/* cig_ast_builtin.h
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - Builtin Types.
 */
#ifndef CIG_AST_BUILTIN_H_INCLUDED
#define CIG_AST_BUILTIN_H_INCLUDED

#include "cig_ast_node.h"


namespace cig {

namespace ast {

// Basic types for initial setup of global namespace
enum ast_builtin_type : int8_t {
    AST_BUILTIN_INVALID   = -1, // never used, sentinel value
    AST_BUILTIN_BOOL      =  0, // 0..COMPOUND-1 are builtin types of CIG
    AST_BUILTIN_U8        =  1,
    AST_BUILTIN_U16       =  2,
    AST_BUILTIN_U32       =  3,
    AST_BUILTIN_U64       =  4,
    AST_BUILTIN_I8        =  5,
    AST_BUILTIN_I16       =  6,
    AST_BUILTIN_I32       =  7,
    AST_BUILTIN_I64       =  8,
    // Future float builtin types should go here
    AST_BUILTIN_STR       =  9,
    AST_BUILTIN_BYTES     = 10,

    AST_BUILTIN_COUNT     = 11, // Count of builtins; max is COUNT-1

    // Not really builtin types but expression results
    AST_COMPOUND_TYPE     = 11, // A compound data type (class or enum)
    AST_VOID_TYPE         = 12, // An expression result of assignments
    AST_LIST_TYPE         = 13  // A list expression result
};

using exp_ast_builtin_type = std::expected<ast_builtin_type,cig::err>;


static inline
cig_bool // whether ast_builtin_type is a CIG builtin type
ast_builtin_type_is_builtin(ast_builtin_type t) noexcept {
    return (t >= 0 && t < AST_BUILTIN_COUNT);
}
static inline
cig_bool // whether expression info type is a CIG builtin type
ast_ei_type_is_builtin(ast_ei_type t) noexcept {
    return (t >= 0 && t < AST_BUILTIN_COUNT);
}
static inline
ast_builtin_type // the AST builtin type for the CIG builtin type
ast_ei_type_to_builtin(
    ast_ei_type t // must be a CIG builtin type
    )
{
    cig_assert(ast_ei_type_is_builtin(t));
    return static_cast<ast_builtin_type>(t);
}
char const *builtin_type_name(ast_builtin_type builtin_type) noexcept;
i64 builtin_type_alignment(ast_builtin_type builtin_type) noexcept;
i64 builtin_type_size(ast_builtin_type builtin_type) noexcept;

exp_none global_add_builtins(ast_node_ptr global,
                             cig::source_location sloc =
                             std::source_location::current()) noexcept;


exp_ast_builtin_type
literal_to_builtin_type(literal::literal_type t) noexcept;

exp_ast_builtin_type
unary_expression_type(
    ast_node_type node_type,
    ast_builtin_type arg,
    cig::source_location sloc
    ) noexcept;

exp_ast_builtin_type
binary_expression_type(
    ast_node_type node_type,
    ast_builtin_type arg1,
    ast_builtin_type arg2,
    cig::source_location sloc
    ) noexcept;

cig_bool
implicit_cast_possible_preserving_sign(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept;

cig_bool
implicit_cast_possible_preserving_integral(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept;

cig_bool
implicit_cast_possible(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept;

cig_bool
explicit_cast_possible(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept;


} // namespace ast

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_AST_BUILTIN_H_INCLUDED) */
