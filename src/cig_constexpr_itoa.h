/* cig_constexpr_itoa.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Compile-time conversion of numbers to strings.
 */
#ifndef CIG_CONSTEXPR_ITOA_H_INCLUDED
#define CIG_CONSTEXPR_ITOA_H_INCLUDED

#include "cig_defines.h"        // CIG_BIG_ENDIAN,CIG_LITTLE_ENDIAN
#include "cig_datatypes.h"

#include <cstddef>              // std::size_t
#include <cstdint>              // uint8_t ...
#include <type_traits>          // std::is_constant_evaluated


namespace cig {


namespace detail {

constexpr
uint64_t                        // eight byte-sized values in correct byte order
itoa_set_eight_digits_from_value(
    uint64_t const v8           // number below 100000000
    )
{
    /* Algorithm according to Chris Hopman

       Three rounds named 4, 2, and 1.

       A round views the 8-byte value as a sequence of (2 * round) byte
       wide numbers below 10^(2*round) and transforms each into two new
       numbers of half the size (round bytes) valued below 10^round.

       The first multiplication and shift is really a division by 10^round,
       with the integral quotient kept and the remainder dismissed.
       The quotient always fits into 1/4 of the width of the result
       numbers (which are still considered of size 2*round here).
       A mask for the other 3/4 bits is used to eliminate bits spilled
       over from adjacent numbers by the shift.

       The multiplication factors and bit shifts are chosen such that
       factor * 10^round / 2^shift is between 1 and (1 + 1/10^round). */

#if defined(CIG_BIG_ENDIAN)

    uint64_t k4 = (v8 * 3518437209ull) >> 45; // = v8 / 10000
    uint64_t v4 = (v8 - k4 * 10000) | (k4 << 32);

    uint64_t k2 = ((v4 * 10486) >> 20) & 0xFF000000FFull; // = v4 / 100 each
    uint64_t v2 = (v4 - k2 * 100) | (k2 << 16);

    uint64_t k1 = ((v2 * 410) >> 12) & 0xF000F000F000Full; // = v2 / 10 each
    uint64_t v1 = (v2 - k1 * 10) | (k1 << 8);

#elif defined(CIG_LITTLE_ENDIAN)

    uint64_t k4 = (v8 * 3518437209ull) >> 45; // = v8 / 10000
    uint64_t v4 = ((v8 - k4 * 10000) << 32) | k4;

    uint64_t k2 = ((v4 * 10486) >> 20) & 0xFF000000FFull; // = v4 / 100 each
    uint64_t v2 = ((v4 - k2 * 100) << 16) | k2;

    uint64_t k1 = ((v2 * 410) >> 12) & 0xF000F000F000Full; // = v2 / 10 each
    uint64_t v1 = ((v2 - k1 * 10) << 8) | k1;

#else
#error byte order not defined
#endif

    return v1;
}


constexpr
std::size_t                     // index of first byte of printed int [0..23]
itoa_u64toa_raw(
    uint64_t u,			// number to print
    uint64_t a[3]               // ascii characters, not '\0'-terminated
    )
{
    constexpr uint64_t EIGHTDIG = 100000000ull;             // 10^8
    constexpr uint64_t ASCIIOFFSET = 0x3030303030303030ull; // "00000000"

    uint64_t v = u;             // value to print

    static_assert(sizeof(a[0]) == 8);

    uint64_t vhigh;             // highest eight-digit group value
    std::size_t p;              // position of first nonzero digit; 0..23
    if (v < EIGHTDIG)           // 1-8 digits
    {
        a[0] = 0;
        a[1] = 0;
        vhigh = itoa_set_eight_digits_from_value(v);
        a[2] = vhigh | ASCIIOFFSET;
        p = 16;
    }
    else
    {
        uint64_t v0 = v % EIGHTDIG;
        uint64_t v1 = v / EIGHTDIG;

        a[2] = itoa_set_eight_digits_from_value(v0) | ASCIIOFFSET;
        if (v1 < EIGHTDIG)
        {
            a[0] = 0;
            vhigh = itoa_set_eight_digits_from_value(v1);
            a[1] = vhigh | ASCIIOFFSET;
            p = 8;
        }
        else
        {
            uint64_t v1x = v1 % EIGHTDIG;
            uint64_t v2x = v1 / EIGHTDIG;

            a[1] = itoa_set_eight_digits_from_value(v1x) | ASCIIOFFSET;
            vhigh = itoa_set_eight_digits_from_value(v2x);
            a[0] = vhigh | ASCIIOFFSET;
            p = 0;
        }
    }
    
#if defined(CIG_BIG_ENDIAN)
    if (vhigh & 0xffffffff00000000ull)
        vhigh >>= 32;
    else
        p += 4;
    if (vhigh & 0xffff0000ull)
        vhigh >>= 16;
    else
        p += 2;
    if (vhigh & 0xff00ull)
        ;
    else
        p += 1;
#elif defined (CIG_LITTLE_ENDIAN)
    if ((vhigh & 0xffffffffull) == 0)
    {
        vhigh >>= 32;
        p += 4;
    }
    if ((vhigh & 0xffffull) == 0)
    {
        vhigh >>= 16;
        p += 2;
    }
    if ((vhigh & 0xffull) == 0)
    {
        p += 1;
    }
#else
#error byte order not defined
#endif

    return p;
}


constexpr
std::size_t                     // index of first byte of printed int [0..23]
itoa_i64toa_raw(
    int64_t i,                  // number to print
    uint64_t a[3]               // ascii characters, not '\0'-terminated
    )
{
    // Absolute value
    uint64_t v = i < 0 ?
        static_cast<uint64_t>(0) - static_cast<uint64_t>(i) :
        static_cast<uint64_t>(i);

    std::size_t p = itoa_u64toa_raw(v,a);

    if (i < 0)
    {
        p = p - 1;
        std::size_t off = p / 8;

#if defined(CIG_BIG_ENDIAN)
        int bits = (7 - p % 8) * 8;
#elif defined (CIG_LITTLE_ENDIAN)
        int bits = (p % 8) * 8;
#else
#error byte order not defined
#endif

        a[off] =
            (a[off] & ~(static_cast<uint64_t>(0xff) << bits)) |
            (static_cast<uint64_t>('-') << bits);
    }
    return p;
}


constexpr str itoa_u64_to_str(uint64_t v)
{
    uint64_t a[3];
    std::size_t p = itoa_u64toa_raw(v,a);

    static_assert(sizeof(a) == 24);
    auto aa = std::bit_cast<std::array<char,24>,uint64_t[3]>(a);

    return str::from_ascii(&aa[p],24-p);
}


constexpr str itoa_i64_to_str(int64_t v)
{
    uint64_t a[3];
    std::size_t p = itoa_i64toa_raw(v,a);

    static_assert(sizeof(a) == 24);
    auto aa = std::bit_cast<std::array<char,24>,uint64_t[3]>(a);

    return str::from_ascii(&aa[p],24-p);
}


} // namespace detail


constexpr str u8::to_str() const {
    return detail::itoa_u64_to_str(_v);
}

constexpr str u16::to_str() const {
    return detail::itoa_u64_to_str(_v);
}

constexpr str u32::to_str() const {
    return detail::itoa_u64_to_str(_v);
}

constexpr str u64::to_str() const {
    return detail::itoa_u64_to_str(_v);
}

constexpr str i8::to_str() const {
    return detail::itoa_i64_to_str(_v);
}

constexpr str i16::to_str() const {
    return detail::itoa_i64_to_str(_v);
}

constexpr str i32::to_str() const {
    return detail::itoa_i64_to_str(_v);
}

constexpr str i64::to_str() const {
    return detail::itoa_i64_to_str(_v);
}



} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_CONSTEXPR_ITOA_H_INCLUDED) */
