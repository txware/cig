/* cig_unicode_conversion.h
 *
 * Copyright 2022-2024 Claus Fischer
 *
 * String conversion (encoding/decoding) of Unicode strings.
 */
#ifndef CIG_UNICODE_CONVERSION_H_INCLUDED
#define CIG_UNICODE_CONVERSION_H_INCLUDED

#include "cig_err.h"            // cig::err

#include <cstddef>              // std::size_t,std::ptrdiff_t
#include <cstdint>              // uint8_t ...
#include <type_traits>          // make_unsigned

namespace cig {


namespace detail {


/* The inner and the outer string
 *
 * This set of conversion functions converts Unicode strings
 * between an 'inner' and an 'outer' format.
 *
 * The inner string is the string representation used by the
 * program in its data structures.
 *
 * The inner string is s for the encode functions,
 * o for the decode functions. The 'inner' string
 * must always be straight UTF-8, UTF-16, or UTF-32.
 *
 * The outer string is the string which the program
 * exchanges with the outer world, by reading or writing
 * files or network sockets.
 *
 * The encoding of the 'outer' string are in the function name
 * and parameters. It may have variants like CESU-8, encoding
 * '\0' as 0xC0 0x80, or JSON-style or C-style escapes like
 * \ooo, \xhh..., \uhhhh, \Uhhhhhhhh.
 */


// An enum class for the encoding of the 'inner' string.
enum class unicode_conversion_encoding
{
    utf8,
    utf16,
    utf32
};

/* Character encodings

   Unicode (ISO/IEC 10646)
     Unicode code points are in the ranges
            U+0 ..   U+D7FF   Basic Multilingual Plane (BMP)
         U+E000 ..   U+FFFF   Basic Multilingual Plane (BMP)

        U+10000 .. U+10FFFF   Supplementary planes (astral planes)

     The gap in the BMP is reserved for UTF-16 surrogate character codes.
     Code points above U+10FFFF will not be assigned.

   UTF-32 (Unicode Standard 5.0.0, chapter 3)
     The direct encoding of the Unicode code points in 32 bits.

   UTF-16 (RFC 2781, ISO/IEC 10646)
     An encoding where the BMP Unicode code points are directly
     encoded in 16 bits.
     The supplementary planes are encoded with surrogate pairs:
     0x10000 is subtracted from the code point, and the resulting
     20-bit value is split in two 10-bit groups.
     The higher 10 bits are added to 0xD800 for the high surrogate half,
     the lower 10 bits are added to 0xDC00 for the low surrogate half.
     The high and low surrogate half are encoded as 16-bit values
     in that order.

   UTF-8 (RFC 3629, ISO/IEC 10646:2012)
     An encoding where the code point value is encoded in up to four
     single code units (bytes):
             U+0 ..       U+7F  single code unit, directly
            U+80 ..      U+7FF  two code units with 5 bit, 6 bit
           U+800 ..     U+FFFF  three code units with 4 bit, 6 bit, 6 bit
         U+10000 ..   U+1FFFFF  four code units with 3 bit, 6 bit, 6 bit, 6 bit
                                (only the values up to U+10FFFF are used)
        U+200000 ..  U+3FFFFFF  five code units with 2 bit, 4 x 6 bit (not used)
       U+4000000 .. U+7FFFFFFF  six code units with 1 bit, 5 x 6 bit (not used)

     The five- and six-byte sequences are not used, since Unicode
     restricts itself to code points up to U+10FFFF.

     The bytes containing six bits add 0x80 to the six-bit value; thus,
     they all start with the two bits 10......;
     the initial byte adds 0xc0 (two bytes), 0xe0 (three bytes),
     0xf0 (four bytes), 0xf8 (five bytes), and 0xfc (six bytes),
     respectively; thus, they all start with the two bits 11......
     and allow to synchronize within an UTF-8 string.

     Since the multibyte encodings contain all the original bits
     of the Unicode code point, this encoding would be ambiguous.
     It is possible to encode the value U+0 as a single byte (0x00),
     or as two bytes (0xC0 0x80), or as three bytes
     (0xE0 0x80 0x80), and so on.
     The UTF-8 standard therefore specifies that the minimum length
     encoding must be used for a code point; the other encodings
     are called overlong encodings and are not allowed.

     Encodings of values outside the valid Unicode code points,
     such as the surrogate pair values 0xD800 through 0xDFFF
     or values above 0x10FFFF via the above scheme are explicitely
     forbidden and must not be tolerated.
     They are not valid UTF-8.

   CESU-8 (Unicode Technical Report #26)
     CESU-8 is a mixture of UTF-16 and UTF-8 where each code point
     is first encoded as UTF-16, which may result in surrogate
     pairs, and the UTF-16 16-bit-value is then encoded by the
     UTF-8 encoding scheme given above.
     The encoding of code points from the BMP will be the same as
     in UTF-8 but values outside the BMP will use six code units
     (bytes; each surrogate half encodes to three bytes), while
     they would take only four bytes with UTF-8.
     The result is not valid UTF-8.

   Modified UTF-8
     Modified UTF-8 is a variant of CESU-8 where the code point
     U+0 is encoded by the overlong sequence of two bytes C0 80.
     This allows processing of such strings by C functions that
     expect strings to be '\0'-terminated.
     It is not valid UTF-8.
     It is used in Java's "UTF8" and in other implementations.

   WTF-8
     This is an encoding where arbitrary sequences of 16-bit
     values that may contain the values from 0xD800 to 0xDFFF
     are encoded in the UTF-8 encoding scheme.
     It is not a valid Unicode encoding scheme.


   The inner string:

   This implementation uses for the 'inner' string a "generic"
   string format of UTF-8, UTF-16, or UTF-32 with separately
   specified length, which may therefore contain embedded
   '\0'-characters encoded as a single code unit with value 0.
   The encoding is a template parameter (senc or oenc).

   While in most cases a code unit will be a byte (for UTF-8),
   a doublebyte unsigned integer (for UTF-16), or a quad-byte
   unsigned integer (for UTF-32), this implementation allows
   for the code units to be stored in bigger or smaller types:
   smaller types may be used e.g. to store UTF-16 values in
   bytes if all such values are below 0xff. That would correspond
   to a Latin-1 encoding (ISO/IEC 8859-1).

   The outer string:

   This implementation allows to encode the 'inner' string into
   or decode it from an 'outer' string, which is either UTF-32,
   UTF-16, UTF-8, CESU-8, or Modified UTF-8, and to quote
   control characters and some special characters in a form
   suitable for JSON or C strings, and to decode these
   quoting methods from these forms.

   The specific variant depends on the template function name
   and its parameters.

   Other formats:

   This implementation does not deal with WTF-8.

   It also does not deal with normalizations of Unicode, i.e.
   with NFC and NFD forms. */


struct unicode_conversion_error {
    enum errortype {
        NO_ERROR,
        ILLEGAL_UTF8_SEQUENCE,
        ILLEGAL_UTF16_SEQUENCE,
        ILLEGAL_UNICODE_CODEPOINT,
        ILLEGAL_UNICODE_CODEPOINT_SURROGATE,
        SURROGATE_PAIR_NOT_COMPLETED,
        LOW_SURROGATE_WITHOUT_PRECEDING_HIGH,
        TWO_HIGH_SURROGATES,
        TRUNCATED_UTF8_SEQUENCE,
        TRUNCATED_UTF16_SEQUENCE,
        OVERLONG_UTF8_ENCODING,
        ALGORITHMIC_UNICODE_CONVERSION,
        MISSING_INITIAL_DELIMITER,
        MISSING_FINAL_DELIMITER,
        CONTROL_CHARACTER_MUST_BE_QUOTED,
        BAD_CHARACTER_AFTER_BACKSLASH,
        END_OF_INPUT_AFTER_BACKSLASH,
        END_OF_INPUT_IN_BACKSLASH_u,
        END_OF_INPUT_IN_BACKSLASH_U,
        END_OF_INPUT_IN_BACKSLASH_x,
        BAD_HEXDIGIT_IN_BACKSLASH_u,
        BAD_HEXDIGIT_IN_BACKSLASH_U,
        BAD_HEXDIGIT_IN_BACKSLASH_x,
        VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH,
        VALUE_IN_BACKSLASH_x_TOO_HIGH
    };

    static constexpr const char * const errormessage[] = {
        "Conversion was successful",
        "Illegal UTF-8 code-unit sequence",
        "Illegal UTF-16 code-unit sequence",
        "Illegal Unicode code point",
        "Illegal Unicode code point (surrogate)",
        "Surrogate pair not completed",
        "Low surrogate without preceding high",
        "High surrogate followed by another high surrogate",
        "Truncated UTF-8 code-unit sequence",
        "Truncated UTF-16 code-unit sequence",
        "Overlong UTF-8 encoding",
        "Internal algorithmic error in Unicode conversion",
        "Missing initial delimiter",
        "Missing final delimiter",
        "Control character must be quoted",
        "Bad character after backslash, not a valid escape sequence",
        "End of input after backslash, not a valid escape sequence",
        "End of input in the backslash-u-sequence",
        "End of input in the backslash-U-sequence",
        "End of input in the backslash-x-sequence",
        "Bad hexdigit in the backslash-u-sequence",
        "Bad hexdigit in the backslash-U-sequence",
        "Bad hexdigit in the backslash-x-sequence",
        "Value in octal backslash sequence too high, only ASCII allowed",
        "Value in backslash-x-sequence too high, only ASCII allowed"
    };

    enum { SQ = 12 };           // Maximum length of code unit sequence

    errortype type { NO_ERROR };
    unsigned long code_unit {}; // Index of code point in input array s
#if 0
    // removed to allow the compiler to optimize away character_count
    unsigned long code_point {};// Number of code points read from s before
#endif
    int sequence_len {};        // Length of sequence of input code units
    unsigned long sequence[SQ] {}; // Erroneous sequence of input code units
};


static inline
err::errorcode
unicode_conversion_map_error(unicode_conversion_error::errortype etype)
{
    switch (etype)
    {
    case unicode_conversion_error::NO_ERROR:
        return err::E_SUCCESS;
    case unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE:
        return err::EC_ILLEGAL_UTF8_SEQUENCE;
    case unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE:
        return err::EC_ILLEGAL_UTF16_SEQUENCE;
    case unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT:
        return err::EC_ILLEGAL_UNICODE_CODEPOINT;
    case unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE:
        return err::EC_ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    case unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED:
        return err::EC_SURROGATE_PAIR_NOT_COMPLETED;
    case unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH:
        return err::EC_LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    case unicode_conversion_error::TWO_HIGH_SURROGATES:
        return err::EC_TWO_HIGH_SURROGATES;
    case unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE:
        return err::EC_TRUNCATED_UTF8_SEQUENCE;
    case unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE:
        return err::EC_TRUNCATED_UTF16_SEQUENCE;
    case unicode_conversion_error::OVERLONG_UTF8_ENCODING:
        return err::EC_OVERLONG_UTF8_ENCODING;
    case unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION:
        return err::EC_ALGORITHMIC_UNICODE_CONVERSION;
    case unicode_conversion_error::MISSING_INITIAL_DELIMITER:
        return err::EC_MISSING_INITIAL_DELIMITER;
    case unicode_conversion_error::MISSING_FINAL_DELIMITER:
        return err::EC_MISSING_FINAL_DELIMITER;
    case unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED:
        return err::EC_CONTROL_CHARACTER_MUST_BE_QUOTED;
    case unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH:
        return err::EC_BAD_CHARACTER_AFTER_BACKSLASH;
    case unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH:
        return err::EC_END_OF_INPUT_AFTER_BACKSLASH;
    case unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u:
        return err::EC_END_OF_INPUT_IN_BACKSLASH_u;
    case unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U:
        return err::EC_END_OF_INPUT_IN_BACKSLASH_U;
    case unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x:
        return err::EC_END_OF_INPUT_IN_BACKSLASH_x;
    case unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u:
        return err::EC_BAD_HEXDIGIT_IN_BACKSLASH_u;
    case unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U:
        return err::EC_BAD_HEXDIGIT_IN_BACKSLASH_U;
    case unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x:
        return err::EC_BAD_HEXDIGIT_IN_BACKSLASH_x;
    case unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH:
        return err::EC_VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    case unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH:
        return err::EC_VALUE_IN_BACKSLASH_x_TOO_HIGH;
    }

    // Default error code
    return err::E_EILSEQ;
}








static constexpr const unsigned char
unicode_conversion_hexchar[] = "0123456789abcdef";


template<class T>
static constexpr
int_fast8_t                     // value of hexadecimal digit, or -1
unicode_conversion_xdigitval(
    T d                         // [i] digit character
    )
{
    typedef typename std::make_unsigned<T>::type UT;
    UT c = static_cast<UT>(d);

    // The following test should be optimized away by the compiler.
    if ('0' + 1 == '1' &&
        '1' + 1 == '2' &&
        '2' + 1 == '3' &&
        '3' + 1 == '4' &&
        '4' + 1 == '5' &&
        '5' + 1 == '6' &&
        '6' + 1 == '7' &&
        '7' + 1 == '8' &&
        '8' + 1 == '9' &&
        'a' + 1 == 'b' &&
        'b' + 1 == 'c' &&
        'c' + 1 == 'd' &&
        'd' + 1 == 'e' &&
        'e' + 1 == 'f' &&
        'A' + 1 == 'B' &&
        'B' + 1 == 'C' &&
        'C' + 1 == 'D' &&
        'D' + 1 == 'E' &&
        'E' + 1 == 'F')
    {
        return 
            c >= '0' && c <= '9'  ?  static_cast<int_fast8_t>(c) - '0' :
            c >= 'a' && c <= 'f'  ?  static_cast<int_fast8_t>(c) - ('a' - 10) :
            c >= 'A' && c <= 'F'  ?  static_cast<int_fast8_t>(c) - ('A' - 10) :
            -1;
    }
    else
    {
        int_fast8_t v = 0;

        if (c > 0x7f)
            return -1;

        switch (static_cast<unsigned char>(c))
        {
        case '0': v =  0; break;
        case '1': v =  1; break;
        case '2': v =  2; break;
        case '3': v =  3; break;
        case '4': v =  4; break;
        case '5': v =  5; break;
        case '6': v =  6; break;
        case '7': v =  7; break;
        case '8': v =  8; break;
        case '9': v =  9; break;
        case 'A':
        case 'a': v = 10; break;
        case 'B':
        case 'b': v = 11; break;
        case 'C':
        case 'c': v = 12; break;
        case 'D':
        case 'd': v = 13; break;
        case 'E':
        case 'e': v = 14; break;
        case 'F':
        case 'f': v = 15; break;
        default:
            v = -1;
            break;
        }
        return v;
    }
}


/* unicode_conversion_encode_utf8()
 *
 * Encode a UTF-8 string into valid UTF-8, CESU-8, or Modified UTF-8
 * raw or JSON or C output.
 *
 *
 *
 * The output character encoding (UTF-8, CESU-8, Modified UTF-8) is
 * determined by create_surrogate_pairs and create_C0_80_for_zero:
 *
 *                              UTF-8     CESU-8    Modified UTF-8
 *                              ----------------------------------
 *   create_surrogate_pairs     0         1         1
 *   create_C0_80_for_zero      0         0         1
 *
 * The create_surrogate_pairs flag causes Unicode code points from
 * U+10000 to U+10FFFF to first be encoded in pair of high and low
 * surrogate 16-bit value, as in UTF-16, then each UTF-16 surrogate
 * value to be encoded into three code units (bytes) according to
 * the UTF-8 encoding scheme. Note that the result it not valid UTF-8,
 * it is called CESU-8. The correct UTF-8 encoding would be to encode
 * the Unicode code points directly into UTF-8 (maximum four code
 * units).
 *
 * The create_C0_80_for_zero flag causes Unicode code point U+0
 * (i.e. the null character) to be encoded not as the '\0' byte,
 * as UTF-8 would mandate, but into the two byte sequence C0 80.
 * These two bytes are a byte sequence that can be decoded according
 * to UTF-8 rules, but they are not the shortest possible sequence
 * and therefore not legal UTF-8.
 * Some systems use this encoding to allow processing strings that contain
 * the code point U+0 with typical C functions which treat '\0' as a
 * string-end character throughout.
 * Note that if quote_control_chars is also set, this flag has no effect.
 *
 *
 *
 * The output form (raw string or JSON or C encoding) is determined by
 * quote_control_chars, quote_escapes, enclose_delimiter, and zero_terminate:
 *
 *                            RAW     JSON    C
 *                            -----------------
 *   quote_control_chars      0       1       1 or 2
 *   quote_escapes            0       1       1
 *   quote_nonascii           0       0 or 3  0, 1, 2, 4, 5
 *   enclose_delimiter        -1      ?       ? (according to needs)
 *   zero_terminate           0       ?       ? (according to needs)
 *
 * The quote_control_chars flag causes control characters (U+0 to U+1F)
 * to be encoded as \uxxxx sequences, where x are hexadecimal digits.
 * This encoding is required by the JSON specification for serialization
 * of JSON objects. If the flag's value is 2, control characters are
 * instead encoded as \ooo, where o are octal digits. That might be
 * preferred for C.
 *
 * The quote_escapes flag causes certain characters ('"', '\\',
 * '\b', '\f', '\n', '\r', '\t') to be encoded in their JSON two-character
 * forms, just as in C strings. Note that we do not encode the forward
 * slash with a preceding backslash; that is not necessary since a
 * forward slash by itself is a perfectly legal character in a string.
 * When both the quote_control_chars and quote_escaped flags are given,
 * the five control characters of the above list are encoded in the
 * two-character form, since that enhances readability and saves space.
 * In other words, quote_escapes has precedence over quote_control_chars.
 *
 * The quote_nonascii flag causes all characters above 0x7f to be encoded
 * with backslash escapes. There is a choice of 1: \U with eight hexdigits,
 * 2: \U with eight hexdigits or \u with four hexdigits for codepoints
 * in BMP, 3: \u with surrogate pairs outside BMP, 4-6: same as 1-3 but
 * with \ooo (three octal characters without a prefix) for the characters
 * from 0x80 to 0xff.
 *
 * The enclose_delimiter character causes double-quotes '"' or other
 * delimiters to be placed before and after the encoded string.
 * The zero_terminate flag causes a null character '\0' to be appended to
 * the encoded (possibly delimited) string.
 * If these characters are requested, they are included in the output
 * code unit count.
 *
 * The number of code units written corresponds to the number returned by
 * unicode_conversion_encode_utf8_count() called with the same flags.
 *
 *
 *
 * The input string s is not '\0'-terminated, its length is specified
 * by slen. This function expects s to be a valid Unicode string with
 * correct UTF-8/UTF-16/UTF-32 code unit sequences, not CESU-8 or
 * Modified UTF-8. Its encoding is specified by senc.
 *
 * If it is a valid Unicode string, the function returns the number of
 * code units written to the output array o, including enclosing
 * delimiters and a terminating '\0'.
 *
 * If it is not a valid Unicode string, the function returns -1 and sets
 * an error message.
 */
template<
    unicode_conversion_encoding senc, // encoding of input code points
    class S,                          // data type of input code units
    class O                           // data type of output code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_encode_utf8(
    S const * const s,          // [i] input code units, not '\0'-terminated
    unsigned long const slen,   // [i] length of (number of code units in) s
    O *o,                       // [o] output code units according to params
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long n_codepoints_max = -1, // [i] max number of codepoints in s to encode
    int create_surrogate_pairs=0,//[i] encode U+10000 through U+10FFFF
                                // [i] as surrogate pairs (CESU-8)
    int create_C0_80_for_zero=0,// [i] encode U+0 as C0 80 (modified UTF-8)
    int quote_control_chars = 0,// [i] whether to quote control chars
    int quote_escapes = 0,      // [i] whether to quote " and \ and ...
    int quote_nonascii = 0,     // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long enclose_delimiter = -1,// [i] if nonnegative enclose in this delimiter
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;

    if (enclose_delimiter != -1)
        o[j++] = static_cast<O>(enclose_delimiter);


    for (i = 0; i < slen; i += n)
    {
        // In case senc is not utf32,
        // we can limit the code points length of s here;
        // otherwise n is the code point limit anyway
        if (n_codepoints_max != -1 &&
            character_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // Decode input encoding
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (i + 1 == slen)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (i + 2 == slen)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (i + 3 == slen)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (i + 4 == slen)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (i + 5 == slen)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (i + 1 == slen)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;

        // Write as an escape, e.g. "\n"
        if (quote_escapes &&    // Note: We never quote the forward slash
            (w == '"' ||
             w == '\\' ||
             w == '\b' ||
             w == '\f' ||
             w == '\n' ||
             w == '\r' ||
             w == '\t'))
        {
            o[j++] = '\\';
            if (w == '"')
                o[j++] = '"';
            if (w == '\\')
                o[j++] = '\\';
            if (w == '\b')
                o[j++] = 'b';
            if (w == '\f')
                o[j++] = 'f';
            if (w == '\n')
                o[j++] = 'n';
            if (w == '\r')
                o[j++] = 'r';
            if (w == '\t')
                o[j++] = 't';
        }

        // Write as a hex sequence, e.g. "\u123a"
        else if (quote_control_chars && w < 0x20)
        {
            if (quote_control_chars == 2) // C only style
            {
                o[j++] = '\\';

                o[j++] = unicode_conversion_hexchar[(w >>  6) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w >>  3) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0x7];
            }
            else                // JSON or C style
            {
                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = '0';
                o[j++] = '0';
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }
        }

        // Write nonascii characters as backslash sequences
        else if (quote_nonascii && w >= 0x80)
        {
            // Write as octal
            if ((quote_nonascii == 4 ||
                 quote_nonascii == 5 ||
                 quote_nonascii == 6) && w <= 0xff)
            {
                o[j++] = '\\';

                o[j++] = unicode_conversion_hexchar[(w >>  6) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w >>  3) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0x7];
            }
            // Write as C-style \Uxxxxxxxx
            else if (quote_nonascii == 1 ||
                     quote_nonascii == 4 ||
                     ((quote_nonascii == 2 ||
                       quote_nonascii == 5) && w >= 0x10000))
            {
                o[j++] = '\\';
                o[j++] = 'U';

                o[j++] = unicode_conversion_hexchar[(w >> 28) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 24) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 20) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 16) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }

            // else quote_nonascii is 2,3,5,6

            // Write as JSON-style \uxxxx\uxxxx for surrogates
            else if (w >= 0x10000)
            {
                /* The maximum value of w is 0x10FFFF (checked above).
                   The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
                uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
                uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));

                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(hi >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi      ) & 0xf];

                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(lo >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo      ) & 0xf];
            }

            // Write as JSON-style \uxxxx for basic multilingual plane
            else
            {
                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(w >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }
        }

        // Write the null character as a double-byte if requested
        else if (w == 0 && create_C0_80_for_zero)
        {
            o[j++] = static_cast<unsigned char>(0xC0);
            o[j++] = static_cast<unsigned char>(0x80);
        }

        /* Write Unicode characters from supplementary planes
           as surrogate pairs if requested */
        else if (w >= 0x10000 && create_surrogate_pairs)
        {
            /* The maximum value of w is 0x10FFFF (checked above).
               The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
            uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
            uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));

            o[j++] =  static_cast<unsigned char>(0xe0 |  hi >> 12);
            o[j++] =  static_cast<unsigned char>(0x80 | (hi >> 6  & 0x3f));
            o[j++] =  static_cast<unsigned char>(0x80 | (hi       & 0x3f));

            o[j++] =  static_cast<unsigned char>(0xe0 |  lo >> 12);
            o[j++] =  static_cast<unsigned char>(0x80 | (lo >> 6  & 0x3f));
            o[j++] =  static_cast<unsigned char>(0x80 | (lo       & 0x3f));
        }

        // Write Unicode characters in UTF-8 encoding
        else
        {
            if (w < 0x80)
            {
                o[j++] = static_cast<unsigned char>(w);
            }
            else if (w < 0x800)
            {
                o[j++] =  static_cast<unsigned char>(0xc0 | w >> 6);
                o[j++] =  static_cast<unsigned char>(0x80 | (w & 0x3f));
            }
            else if (w < 0x10000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xe0 |  w >> 12);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else if (w < 0x200000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf0 |  w >> 18);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf8 |  w >> 24);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else // if (w < 0x80000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xfc |  w >> 30);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 24 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
        }
    }

    if (enclose_delimiter != -1)
        o[j++] = static_cast<O>(enclose_delimiter);

    if (zero_terminate)
        o[j++] = '\0';

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_encode_utf8_count()
 *
 * This function goes together with unicode_conversion_encode_utf8().
 * It computes the number of code units written by the encoding function.
 * The delimiters and the terminating '\0' character are
 * only counted if the respective flags are set on input.
 */
template<
    unicode_conversion_encoding senc, // encoding of input code points
    class S                           // data type of input code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_encode_utf8_count(
    S const *s,                 // [i] input code units, not '\0'-terminated
    unsigned long slen,         // [i] length of (number of code units in) s
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long n_codepoints_max = -1, // [i] max number of codepoints in s to encode
    int create_surrogate_pairs=0,//[i] encode U+10000 through U+10FFFF
                                // [i] as surrogate pairs (CESU-8)
    int create_C0_80_for_zero=0,// [i] encode U+0 as C0 80 (modified UTF-8)
    int quote_control_chars = 0,// [i] whether to quote control chars
    int quote_escapes = 0,      // [i] whether to quote " and \ and ...
    int quote_nonascii = 0,     // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long enclose_delimiter = -1,// [i] if nonnegative enclose in this delimiter
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;

    if (enclose_delimiter != -1)
        j += 2;


    for (i = 0; i < slen; i += n)
    {
        // In case senc is not utf32,
        // we can limit the code points length of s here;
        // otherwise n is the code point limit anyway
        if (n_codepoints_max != -1 &&
            character_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // Decode input encoding
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (i + 1 == slen)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (i + 2 == slen)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (i + 3 == slen)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (i + 4 == slen)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (i + 5 == slen)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (i + 1 == slen)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;

        // Write as an escape, e.g. "\n"
        if (quote_escapes &&    // Note: We never quote the forward slash
            (w == '"' ||
             w == '\\' ||
             w == '\b' ||
             w == '\f' ||
             w == '\n' ||
             w == '\r' ||
             w == '\t'))
        {
            j += 2;
        }

        // Write as a hex sequence, e.g. "\u123a"
        else if (quote_control_chars && w < 0x20)
        {
            if (quote_control_chars == 2) // C only style
                j += 4;
            else                // JSON or C style
                j += 6;
        }

        // Write nonascii characters as backslash sequences
        else if (quote_nonascii && w >= 0x80)
        {
            // Write as octal
            if ((quote_nonascii == 4 ||
                 quote_nonascii == 5 ||
                 quote_nonascii == 6) && w <= 0xff)
            {
                j += 4;
            }
            // Write as C-style \Uxxxxxxxx
            else if (quote_nonascii == 1 ||
                     quote_nonascii == 4 ||
                     ((quote_nonascii == 2 ||
                       quote_nonascii == 5) && w >= 0x10000))
            {
                j += 10;
            }

            // else quote_nonascii is 2,3,5,6

            // Write as JSON-style \uxxxx\uxxxx for surrogates
            else if (w >= 0x10000)
            {
                j += 12;
            }

            // Write as JSON-style \uxxxx for basic multilingual plane
            else
            {
                j += 6;
            }
        }

        // Write the null character as a double-byte if requested
        else if (w == 0 && create_C0_80_for_zero)
        {
            j += 2;
        }

        /* Write Unicode characters from supplementary planes
           as surrogate pairs if requested */
        else if (w >= 0x10000 && create_surrogate_pairs)
        {
            j += 6;
        }

        // Write Unicode characters in UTF-8 encoding
        else
        {
            if (w < 0x80)
            {
                j += 1;
            }
            else if (w < 0x800)
            {
                j += 2;
            }
            else if (w < 0x10000ul)
            {
                j += 3;
            }
            else if (w < 0x200000ul)
            {
                j += 4;
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                j += 5;
            }
            else // if (w < 0x80000000ul)
            {
                j += 6;
            }
        }
    }

    if (zero_terminate)
        j++;

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_decode_utf8()
 *
 * Decode a UTF-8 string from valid UTF-8, CESU-8, or Modified UTF-8
 * raw or JSON or C input.
 *
 * The input string s can be '\0'-terminated; in that case send
 * should be passed as NULL. If send is not NULL, any '\0' characters
 * in the input string will be passed on to the output.
 *
 *
 *
 * The input character encoding (UTF-8, CESU-8, Modified UTF-8) is
 * determined by allow_C0_80_for_zero and allow_surrogate_pairs:
 *
 *                              UTF-8     CESU-8    Modified UTF-8
 *                              ----------------------------------
 *   allow_surrogate_pairs      0         1         1
 *   allow_C0_80_for_zero       0         0         1
 *
 * If both flags are zero, this function validates that the input string
 * is correct UTF-8 with correct UTF-8 code point sequences.
 * Overlong encodings or illegal Unicode code points are not accepted.
 *
 * The allow_surrogate_pairs flag allows surrogate pairs of two
 * code points in the input; the first must be from U+D800 .. U+DBFF
 * and the second from the U+DC00 .. U+DFFF range.
 * These code points in the input string must be directly UTF-8 encoded,
 * as in CESU-8, or they may be encoded as \uxxxx \uxxxx if quoted form
 * is JSON.
 * The surrogate pair is always combined to a single valid Unicode
 * code point in the range of U+10000 to U+10FFFF (a code point from
 * the supplementary planes), which is then UTF-8 encoded (up to four
 * code points).
 * The allow_surrogate_pairs flag does not disallow the standard UTF-8
 * encoding of Unicode characters above U+10000; these are always accepted.
 * When quoted_form is 1 (JSON), surrogate pairs encoded as \uxxxx \uxxxx
 * will always be accepted, irrespective of the allow_surrogate_pairs flag.
 *
 * The allow_C0_80_for_zero flag allows the overlong UTF-8 encoding C0 80
 * (a two-byte encoding of the code point U+0) in the input.
 * Some systems use this encoding to allow processing strings that contain
 * the code point U+0 with typical C functions which treat '\0' as a
 * string-end character throughout.
 * It does not allow any other overlong UTF-8 encodings.
 * The allow_C0_80_for_zero flag does not disallow the standard UTF-8
 * encoding of U+0 as 00 (the '\0' character).
 * However, if send is NULL, the '\0' character indicates end of input,
 * as with typical C functions, and will not be passed to the output.
 *
 *
 *
 * The input form (raw string or JSON or C encoding) is determined by
 * quoted_form and with_delimiters:
 *
 *                            RAW     JSON    C
 *                            -------------------
 *   quoted_form              0       1       2/3
 *   with_delimiters          0       ?       ?   (according to needs)
 *
 * The quoted_form flag equal 1 causes the JSON escape sequences
 * \"   \\   \/   \b   \f   \n   \r   \t   \uxxxx
 * to be correctly decoded into their Unicode equivalents. Note that
 * only Unicode characters of the basic multilingual plane can be
 * encoded with \uxxxx. Codes from the supplementary planes must be
 * given directly (as an UTF-8 character), or as surrogate pairs
 * which is not standard UTF-8 but standard for JSON \u-sequences.
 * The quoted_form flag 1 causes directly encoded (non-backslash-escaped)
 * control characters (U+1F and below) in the UTF-8 input to be not
 * acceptable. It also causes other backslash sequences to be rejected.
 *
 * The quoted_form flag equal 2 or 3 accepts the additional escape sequences
 * of the C language, namely \a, \v, \?, \', \xxx.., \ooo, and \Uxxxxxxxx.
 * It causes surrogate pairs of two \uxxxx encodings to be rejected
 * unless the allow_surrogate_pairs flag is also set.
 * Note that according to C rules, the forms \ooo and \xxx... are not
 * interpreted as Unicode code points but as values to be directly
 * stored in a single code unit.
 * With quoted_form equal 2, this code accepts \xxx... and \ooo only
 * for valid Unicode code points that fit into one code unit.
 * With quoted_form equal 3, \ooo and \xxx... are accepted for all
 * values that fit into one code unit, even if they do not represent
 * a valid code point. In that case, the output of this function is
 * not strictly Unicode, and such illegal code units are not counted
 * in the codepoints output variable. A maximum number of codepoints
 * is illegal for this form.
 *
 * The with_delimiters flag makes the function expect and strip
 * surrounding delimiters from the input string. This flag requires
 * the quoted_form flag.
 * If this flag is not given but a delimiter is set, decoding will
 * stop before the delimiter but not count the delimiter.
 *
 *
 * The output string o will always be correct UTF-8: It will only
 * contain the minimum bytes encoding of correct Unicode code points
 * (U+0 through U+D7FF, U+E000 through U+10FFFF).
 *
 * The zero_terminate flag causes a null byte '\0' to be appended
 * to the decoded output string. This byte will be appended regardless
 * of the occurrence of '\0'-bytes within the output string.
 *
 * The codepoints value is set to the count of Unicode code points
 * in the output string. An appended '\0' byte is not counted,
 * but embedded '\0' characters from the input are all counted.
 * In case of embedded '\0' characters, the negative count is returned.
 *
 * The n_cu_parsed value is set to the number of code units read from
 * the input string s.
 * The n_cp_parsed value is set to the number of UTF-8 code points
 * read from the input string s. Each UTF-8 code point is counted;
 * if the input is CESU-8 encoded, the two surrogate halves are
 * both counted. For escape sequences, each character making up
 * the escape sequence is counted.
 *
 * If the input string is a valid UTF-8 string (or CESU-8 or
 * Modified UTF-8, according to the flags), and if - in case of
 * quoted_form - all control characters and backslashes are correctly
 * escaped and all backslash escape sequences are valid JSON or C, and
 * if - in case of with_delimiters - the string is correctly enclosed
 * in delimiters, the function returns the number of code units
 * written to the output array o, including a terminating '\0' that
 * has been requested.
 *
 * The number of code units written corresponds to the number returned by
 * unicode_conversion_decode_utf8_count() called with the same flags.
 *
 * If the input string is incorrect UTF-8 (or CESU-8 or Modified UTF-8,
 * according to the flags), or if quoted_form is requested and the
 * JSON or C backslash sequences are incorrect or control characters are
 * unquoted, or if with_delimiters is requested and the string is
 * not correctly enclosed in delimiters, the function returns -1
 * and sets an error message.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class S,                          // data type of input code units
    class O                           // data type of output code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_decode_utf8(
    S const * const s,          // [i] input code units in UTF-8/CESU-8/mod.8
    S const * const send,       // [i] end pointer of s (past last code unit);
                                //     if NULL, s is '\0'-terminated
    O *o,                       // [o] output code units UTF-8/UTF-16/UTF-32
    std::ptrdiff_t *codepoints, // [o] number of Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o] no. code units read from s; can be 0
    unsigned long *n_cp_parsed, // [o] no. code points read from s; can be 0
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long delimiter = -1,        // [i] code unit in s to stop at unless quoted
    int stop_if_incomplete = 0, // [i] stop parsing before incomplete codepoint
    long n_codepoints_max = -1, // [i] maximum number of codepoints in o
    int allow_surrogate_pairs=0,// [i] allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero =0,// [i] allow input \0 encoded as hex C0 80
    int quoted_form = 0,        // [i] whether control chars must be quoted
    int with_delimiters = 0,    // [i] require delimiters around input
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    int allow_quoted_surrogates = (quoted_form == 1); // JSON only, \uxxxx only
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    int possible_quoted_surrogate = 0;    // 1 after \uxxxx
    uint_fast32_t i = 0;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    int is_quoted = 0;                    // 1 after '\\'
    US u, u2, u3, u4, u5, u6;
    int_fast8_t d0, d1, d2, d3, d4, d5, d6, d7;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast8_t has_embedded_zeros = 0;
    uint_fast32_t codepoint_count = 0;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;


    // Check the codepoints limit
    if (n_codepoints_max != -1 && quoted_form == 3)
        goto algorithmic_unicode_conversion;

    // Parse the opening delimiter
    if (with_delimiters)
    {
        if (delimiter == -1 || !quoted_form)
            goto algorithmic_unicode_conversion;
        if (stop_if_incomplete && s + i == send)
            goto incomplete_input_success;
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_initial_delimiter;
        i++;
        character_count++;
    }

    for (;                      // i already initialized
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        // Limit the number of code points into o.
        if (n_codepoints_max != -1 &&
            codepoint_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        is_quoted = 0;
        possible_quoted_surrogate = 0;
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0xff)
            goto illegal_utf8_sequence;

        // Found the trailing delimiter
        if (delimiter != -1 && u == static_cast<US>(delimiter))
            break;

        // Control characters must be quoted
        if (quoted_form && u < 0x20)
            goto control_character_must_be_quoted;

        // Found a backslash
        if (quoted_form && u == '\\')
        {
            is_quoted = 1;

            // Decode escaped sequence
            if (s + i + 1 == send)  // '\0' checked below
                goto end_of_input_after_backslash;

            u = static_cast<US>(s[i+1]);
            if (u > 0xff)
                goto bad_character_after_backslash;

            // Unicode escape sequence: \uxxxx
            if (u == 'u')
            {
                possible_quoted_surrogate = allow_quoted_surrogates;
                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_u;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_u;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_u;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_u;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_u;
                w =
                    (1ul << 12) * static_cast<uint8_t>(d0) +
                    (1ul <<  8) * static_cast<uint8_t>(d1) +
                    (1ul <<  4) * static_cast<uint8_t>(d2) +
                    (1ul      ) * static_cast<uint8_t>(d3);
                n = 6;
                character_count += 6;
            }

            // JSON escape sequences
            else if (u == 'n' || u == 't' || u == 'r' ||
                     u == 'f' || u == 'b' || u == '/' ||
                     u == '"' || u == '\\')
            {
                if      (u == 'n')
                    w = '\n';
                else if (u == 't')
                    w = '\t';
                else if (u == 'r')
                    w = '\r';
                else if (u == 'f')
                    w = '\f';
                else if (u == 'b')
                    w = '\b';
                else // \ / "
                    w = u;
                n = 2;
                character_count += 2;
            }

            // End of input via '\0'
            else if (!u && !send)
                goto end_of_input_after_backslash;

            // Any other escape sequence is an error unless in C
            else if (quoted_form != 2 && quoted_form != 3)
                goto bad_character_after_backslash;

            // C escape sequences in addition to JSON
            else if (u == 'a' || u == 'v' || u == '?' ||
                     u == '\'')
            {
                if      (u == 'a')
                    w = '\a';
                else if (u == 'v')
                    w = '\v';
                else // ? '
                    w = u;
                n = 2;
                character_count += 2;
            }

            // Unicode escape sequence: \Uxxxxxxxx (C only)
            else if (u == 'U')
            {
                n = 10;         // For error messages, see return_error

                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_U;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_U;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_U;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_U;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 6 == send)
                    goto end_of_input_in_backslash_U;
                d4 = unicode_conversion_xdigitval(static_cast<US>(s[i+6]));
                if (d4 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 7 == send)
                    goto end_of_input_in_backslash_U;
                d5 = unicode_conversion_xdigitval(static_cast<US>(s[i+7]));
                if (d5 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 8 == send)
                    goto end_of_input_in_backslash_U;
                d6 = unicode_conversion_xdigitval(static_cast<US>(s[i+8]));
                if (d6 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 9 == send)
                    goto end_of_input_in_backslash_U;
                d7 = unicode_conversion_xdigitval(static_cast<US>(s[i+9]));
                if (d7 == -1)
                    goto bad_hexdigit_in_backslash_U;
                w =
                    (1ul << 28) * static_cast<uint8_t>(d0) +
                    (1ul << 24) * static_cast<uint8_t>(d1) +
                    (1ul << 20) * static_cast<uint8_t>(d2) +
                    (1ul << 16) * static_cast<uint8_t>(d3) +
                    (1ul << 12) * static_cast<uint8_t>(d4) +
                    (1ul <<  8) * static_cast<uint8_t>(d5) +
                    (1ul <<  4) * static_cast<uint8_t>(d6) +
                    (1ul      ) * static_cast<uint8_t>(d7);
                n = 10;
                character_count += 10;
            }

            // Hexadecimal escape sequence: \xxxx (C only)
            else if (u == 'x')
            {
                w = 0;
                for (m = 0; m < 10; m++) // read no more than 10 hexdigits
                {
                    if (s + i + 2 + m == send)
                    {
                        if (m == 0)
                            goto end_of_input_in_backslash_x;
                        else
                            break;
                    }
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1)
                    {
                        if (m == 0)
                            goto bad_hexdigit_in_backslash_x;
                        else
                            break;
                    }
                    if (quoted_form == 3 && w > 0xffffffful)
                        goto value_in_backslash_x_too_high;
                    w = (w << 4) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    if (w <= 0xffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    if (w <= 0x10ffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                character_count += n;
            }

            // Octal escape sequence: \ooo (C only)
            else if ( (d0 = unicode_conversion_xdigitval(u)) != -1 && d0 < 7)
            {
                w = static_cast<uint8_t>(d0);
                for (m = 0; m < 2; m++) // read no more than 3 octal digits
                {
                    if (s + i + 2 + m == send)
                        break;
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1 || d0 > 7)
                        break;
                    w = (w << 3) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_octal_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_octal_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                character_count += n;
            }

            // Any other escape sequence is an error
            else
                goto bad_character_after_backslash;
        }


        // Not an escaped sequence but a direct UTF-8 byte sequence
        else
        {
            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (s + i + 1 == send)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (s + i + 2 == send)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (s + i + 3 == send)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (s + i + 4 == send)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (s + i + 5 == send)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m &&
                // Allow modified UTF-8: \0 is encoded as 11000000 10000000
                (!allow_C0_80_for_zero ||
                 w != 0 ||
                 n != 2))
                goto overlong_encoding;
            character_count++;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            /* Surrogate pairs can be allowed by call parameter
               but also by JSON \uxxxx \uxxxx sequences;
               those are detected by possible_quoted_surrogate */
            if ((!is_quoted && !allow_surrogate_pairs) ||
                (is_quoted && !possible_quoted_surrogate))
            {
                character_count--;
                goto illegal_unicode_codepoint_surrogate;
            }

            // Surrogates of JSON and of CESU-8 cannot be mixed
            if (previous_surrogate)
            {
                // Test only if both forms are principally possible
                if (allow_surrogate_pairs && allow_quoted_surrogates)
                {
                    if (static_cast<US>(s[i-1]) <= 0x7f) // previous was \uxxxx
                    {
                        if (!possible_quoted_surrogate) // this one is UTF-8
                        {
                            character_count--;
                            goto surrogate_pair_not_completed;
                        }
                    }
                    else // previous was UTF-8
                    {
                        if (possible_quoted_surrogate) // this one is \uxxxx
                        {
                            character_count -= n;
                            n = 1;
                            goto surrogate_pair_not_completed;
                        }
                    }
                }
            }

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto two_high_surrogates;
                }
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto low_surrogate_without_preceding_high;
                }
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
                // Count the two UTF-8 groups of CESU-8 as just one code point
                if (!possible_quoted_surrogate)
                    character_count--;
            }
        }
        else if (previous_surrogate)
        {
            if (possible_quoted_surrogate)
                character_count -= n;
            else
                character_count--;
            goto surrogate_pair_not_completed;
        }


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the UTF-8 (or CESU-8 or Modified UTF-8) is correct

        // Count the code point
        codepoint_count++;

        // Is this an embedded zero?
        if (!w)
            has_embedded_zeros = 1;

        // Store the resulting character w as UTF-8/UTF-16/UTF-32
        if (oenc == unicode_conversion_encoding::utf8)
        {
            if (w < 0x80)
            {
                o[j++] = static_cast<unsigned char>(w);
            }
            else if (w < 0x800)
            {
                o[j++] =  static_cast<unsigned char>(0xc0 | w >> 6);
                o[j++] =  static_cast<unsigned char>(0x80 | (w & 0x3f));
            }
            else if (w < 0x10000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xe0 |  w >> 12);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else if (w < 0x200000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf0 |  w >> 18);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf8 |  w >> 24);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else // if (w < 0x80000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xfc |  w >> 30);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 24 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
        }
        else if (oenc == unicode_conversion_encoding::utf16)
        {
            if (w > 0xFFFF)
            {
                /* The maximum value of w is 0x10FFFF (checked above).
                   The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
                uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
                uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));
                o[j++] = static_cast<O>(hi);
                o[j++] = static_cast<O>(lo);
            }
            else
                o[j++] = static_cast<O>(w);
        }
        else if (oenc == unicode_conversion_encoding::utf32)
        {
            o[j++] = static_cast<O>(w);
        }
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 code units
        if (stop_if_incomplete)
            goto incomplete_input_success;
        goto surrogate_pair_not_completed;
    }

    // Parse the trailing delimiter
    if (with_delimiters)
    {
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_final_delimiter;
        i++;
        character_count++;
    }

    // Append the trailing '\0'
    if (zero_terminate)
        o[j++] = '\0';

    // Return the results
    if (codepoints)
        *codepoints = (has_embedded_zeros ?
                       - static_cast<std::ptrdiff_t>(codepoint_count) :
                       static_cast<std::ptrdiff_t>(codepoint_count));
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Success after incomplete input

 incomplete_input_success:

    // We get here when stop_if_incomplete is set,
    // but only indirectly from one of the following error labels.
    //
    // The value of i should be the start position of the incomplete
    // UTF-8 sequence or escape sequence.
    //
    // We need to consider if the previous code point or escape sequence
    // was the start of a surrogate, and unwind that in case.
    if (previous_surrogate)
    {
        // Uncount the first surrogate half
        if (static_cast<US>(s[i-1]) <= 0x7f)
        {
            // It was a backslash sequence
            while (s[i-1] != '\\')
                i--, n++, character_count--;
        }
        else
            // It was a direct surrogate character (CESU-8)
            i -= 3, n += 3, character_count--;
    }

    // If with_delimiters, we cannot partially parse
    if (with_delimiters)
    {
        i = 0;
        character_count = 0;
    }

    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Error handling

 algorithmic_unicode_conversion:
    n = 0;
    error_type = unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION;
    goto return_error;

 missing_initial_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_INITIAL_DELIMITER;
    goto return_error;

 missing_final_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_FINAL_DELIMITER;
    goto return_error;

 control_character_must_be_quoted:
    n = 1;
    error_type = unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED;
    goto return_error;

 bad_character_after_backslash:
    n = 2;
    error_type = unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_after_backslash:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 1;
    error_type = unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_in_backslash_u:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u;
    goto return_error;

 bad_hexdigit_in_backslash_u:
    n = 6;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u;
    goto return_error;

 end_of_input_in_backslash_U:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U;
    goto return_error;

 bad_hexdigit_in_backslash_U:
    n = 10;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U;
    goto return_error;

 end_of_input_in_backslash_x:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 2;
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x;
    goto return_error;

 bad_hexdigit_in_backslash_x:
    n = 3;
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x;
    goto return_error;

 value_in_backslash_x_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH;
    goto return_error;

 value_in_backslash_octal_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    goto return_error;

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 surrogate_pair_not_completed:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character (CESU-8)
        i -= 3, n += 3, character_count--;
    error_type = unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED;
    goto return_error;

 low_surrogate_without_preceding_high:
    error_type = unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    goto return_error;

 two_high_surrogates:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character (CESU-8)
        i -= 3, n += 3, character_count--;
    error_type = unicode_conversion_error::TWO_HIGH_SURROGATES;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    if (codepoints)
        *codepoints = 0;        // The code points have no meaning yet
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    return -1;
}


/* unicode_conversion_decode_utf8_count()
 *
 * This function goes together with unicode_conversion_decode_utf8().
 * It computes the number of code units written by the decoding function.
 * The terminating '\0' character is counted if it is appended.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class S                           // data type of input code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_decode_utf8_count(
    S const * const s,          // [i] input code units in UTF-8/CESU-8/mod.8
    S const * const send,       // [i] end pointer of s (past last code unit);
                                //     if NULL, s is '\0'-terminated
    std::ptrdiff_t *codepoints, // [o] number of Unicode CP in o; can be 0
    unsigned long *maxcodepoint,// [o] maximum Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o] no. code units read from s; can be 0
    unsigned long *n_cp_parsed, // [o] no. code points read from s; can be 0
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long delimiter = -1,        // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete = 0, // [i] stop parsing before incomplete codepoint
    long n_codepoints_max = -1, // [i] maximum number of codepoints in o
    int allow_surrogate_pairs=0,// [i] allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero =0,// [i] allow input \0 encoded as hex C0 80
    int quoted_form = 0,        // [i] whether control chars must be quoted
    int with_delimiters = 0,    // [i] require delimiters around input
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    int allow_quoted_surrogates = (quoted_form == 1); // JSON only, \uxxxx only
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    int possible_quoted_surrogate = 0;    // 1 after \uxxxx
    uint_fast32_t i = 0;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    uint_fast32_t wmax = 0;
    int is_quoted = 0;                    // 1 after '\\'
    US u, u2, u3, u4, u5, u6;
    int_fast8_t d0, d1, d2, d3, d4, d5, d6, d7;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast8_t has_embedded_zeros = 0;
    uint_fast32_t codepoint_count = 0;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;


    // Check the codepoints limit
    if (n_codepoints_max != -1 && quoted_form == 3)
        goto algorithmic_unicode_conversion;

    // Parse the opening delimiter
    if (with_delimiters)
    {
        if (delimiter == -1 || !quoted_form)
            goto algorithmic_unicode_conversion;
        if (stop_if_incomplete && s + i == send)
            goto incomplete_input_success;
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_initial_delimiter;
        i++;
        character_count++;
    }

    for (;                      // i already initialized
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        // Limit the number of code points into o.
        if (n_codepoints_max != -1 &&
            codepoint_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        is_quoted = 0;
        possible_quoted_surrogate = 0;
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0xff)
            goto illegal_utf8_sequence;

        // Found the trailing delimiter
        if (delimiter != -1 && u == static_cast<US>(delimiter))
            break;

        // Control characters must be quoted
        if (quoted_form && u < 0x20)
            goto control_character_must_be_quoted;

        // Found a backslash
        if (quoted_form && u == '\\')
        {
            is_quoted = 1;

            // Decode escaped sequence
            if (s + i + 1 == send)  // '\0' checked below
                goto end_of_input_after_backslash;

            u = static_cast<US>(s[i+1]);
            if (u > 0xff)
                goto bad_character_after_backslash;

            // Unicode escape sequence: \uxxxx
            if (u == 'u')
            {
                possible_quoted_surrogate = allow_quoted_surrogates;
                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_u;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_u;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_u;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_u;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_u;
                w =
                    (1ul << 12) * static_cast<uint8_t>(d0) +
                    (1ul <<  8) * static_cast<uint8_t>(d1) +
                    (1ul <<  4) * static_cast<uint8_t>(d2) +
                    (1ul      ) * static_cast<uint8_t>(d3);
                n = 6;
                character_count += 6;
            }

            // JSON escape sequences
            else if (u == 'n' || u == 't' || u == 'r' ||
                     u == 'f' || u == 'b' || u == '/' ||
                     u == '"' || u == '\\')
            {
                if      (u == 'n')
                    w = '\n';
                else if (u == 't')
                    w = '\t';
                else if (u == 'r')
                    w = '\r';
                else if (u == 'f')
                    w = '\f';
                else if (u == 'b')
                    w = '\b';
                else // \ / "
                    w = u;
                n = 2;
                character_count += 2;
            }

            // End of input via '\0'
            else if (!u && !send)
                goto end_of_input_after_backslash;

            // Any other escape sequence is an error unless in C
            else if (quoted_form != 2 && quoted_form != 3)
                goto bad_character_after_backslash;

            // C escape sequences in addition to JSON
            else if (u == 'a' || u == 'v' || u == '?' ||
                     u == '\'')
            {
                if      (u == 'a')
                    w = '\a';
                else if (u == 'v')
                    w = '\v';
                else // ? '
                    w = u;
                n = 2;
                character_count += 2;
            }

            // Unicode escape sequence: \Uxxxxxxxx (C only)
            else if (u == 'U')
            {
                n = 10;         // For error messages, see return_error

                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_U;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_U;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_U;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_U;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 6 == send)
                    goto end_of_input_in_backslash_U;
                d4 = unicode_conversion_xdigitval(static_cast<US>(s[i+6]));
                if (d4 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 7 == send)
                    goto end_of_input_in_backslash_U;
                d5 = unicode_conversion_xdigitval(static_cast<US>(s[i+7]));
                if (d5 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 8 == send)
                    goto end_of_input_in_backslash_U;
                d6 = unicode_conversion_xdigitval(static_cast<US>(s[i+8]));
                if (d6 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 9 == send)
                    goto end_of_input_in_backslash_U;
                d7 = unicode_conversion_xdigitval(static_cast<US>(s[i+9]));
                if (d7 == -1)
                    goto bad_hexdigit_in_backslash_U;
                w =
                    (1ul << 28) * static_cast<uint8_t>(d0) +
                    (1ul << 24) * static_cast<uint8_t>(d1) +
                    (1ul << 20) * static_cast<uint8_t>(d2) +
                    (1ul << 16) * static_cast<uint8_t>(d3) +
                    (1ul << 12) * static_cast<uint8_t>(d4) +
                    (1ul <<  8) * static_cast<uint8_t>(d5) +
                    (1ul <<  4) * static_cast<uint8_t>(d6) +
                    (1ul      ) * static_cast<uint8_t>(d7);
                n = 10;
                character_count += 10;
            }

            // Hexadecimal escape sequence: \xxxx (C only)
            else if (u == 'x')
            {
                w = 0;
                for (m = 0; m < 10; m++) // read no more than 10 hexdigits
                {
                    if (s + i + 2 + m == send)
                    {
                        if (m == 0)
                            goto end_of_input_in_backslash_x;
                        else
                            break;
                    }
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1)
                    {
                        if (m == 0)
                            goto bad_hexdigit_in_backslash_x;
                        else
                            break;
                    }
                    if (quoted_form == 3 && w > 0xffffffful)
                        goto value_in_backslash_x_too_high;
                    w = (w << 4) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    if (w <= 0xffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    if (w <= 0x10ffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                character_count += n;
            }

            // Octal escape sequence: \ooo (C only)
            else if ( (d0 = unicode_conversion_xdigitval(u)) != -1 && d0 < 7)
            {
                w = static_cast<uint8_t>(d0);
                for (m = 0; m < 2; m++) // read no more than 3 octal digits
                {
                    if (s + i + 2 + m == send)
                        break;
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1 || d0 > 7)
                        break;
                    w = (w << 3) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_octal_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                character_count += n;
            }

            // Any other escape sequence is an error
            else
                goto bad_character_after_backslash;
        }


        // Not an escaped sequence but a direct UTF-8 byte sequence
        else
        {
            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (s + i + 1 == send)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (s + i + 2 == send)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (s + i + 3 == send)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (s + i + 4 == send)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (s + i + 5 == send)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m &&
                // Allow modified UTF-8: \0 is encoded as 11000000 10000000
                (!allow_C0_80_for_zero ||
                 w != 0 ||
                 n != 2))
                goto overlong_encoding;
            character_count++;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            /* Surrogate pairs can be allowed by call parameter
               but also by JSON \uxxxx \uxxxx sequences;
               those are detected by possible_quoted_surrogate */
            if ((!is_quoted && !allow_surrogate_pairs) ||
                (is_quoted && !possible_quoted_surrogate))
            {
                character_count--;
                goto illegal_unicode_codepoint_surrogate;
            }

            // Surrogates of JSON and of CESU-8 cannot be mixed
            if (previous_surrogate)
            {
                // Test only if both forms are principally possible
                if (allow_surrogate_pairs && allow_quoted_surrogates)
                {
                    if (static_cast<US>(s[i-1]) <= 0x7f) // previous was \uxxxx
                    {
                        if (!possible_quoted_surrogate) // this one is UTF-8
                        {
                            character_count--;
                            goto surrogate_pair_not_completed;
                        }
                    }
                    else // previous was UTF-8
                    {
                        if (possible_quoted_surrogate) // this one is \uxxxx
                        {
                            character_count -= n;
                            n = 1;
                            goto surrogate_pair_not_completed;
                        }
                    }
                }
            }

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto two_high_surrogates;
                }
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto low_surrogate_without_preceding_high;
                }
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
                // Count the two UTF-8 groups of CESU-8 as just one code point
                if (!possible_quoted_surrogate)
                    character_count--;
            }
        }
        else if (previous_surrogate)
        {
            if (possible_quoted_surrogate)
                character_count -= n;
            else
                character_count--;
            goto surrogate_pair_not_completed;
        }


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the UTF-8 (or CESU-8 or Modified UTF-8) is correct

        // Count the code point
        codepoint_count++;

        // Is this an embedded zero?
        if (!w)
            has_embedded_zeros = 1;

        // Store the resulting character w as UTF-8/UTF-16/UTF-32
        if (oenc == unicode_conversion_encoding::utf8)
        {
            if (w < 0x80)
            {
                j += 1;
            }
            else if (w < 0x800)
            {
                j += 2;
            }
            else if (w < 0x10000ul)
            {
                j += 3;
            }
            else if (w < 0x200000ul)
            {
                j += 4;
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                j += 5;
            }
            else // if (w < 0x80000000ul)
            {
                j += 6;
            }
        }
        else if (oenc == unicode_conversion_encoding::utf16)
        {
            if (w > 0xFFFF)
            {
                j += 2;
            }
            else
            {
                j += 1;
            }
        }
        else if (oenc == unicode_conversion_encoding::utf32)
        {
            j += 1;
        }

        // Compute the maximum codepoint
        if (wmax < w)
            wmax = w;
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 code units
        if (stop_if_incomplete)
            goto incomplete_input_success;
        goto surrogate_pair_not_completed;
    }

    // Parse the trailing delimiter
    if (with_delimiters)
    {
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_final_delimiter;
        i++;
        character_count++;
    }

    // Append the trailing '\0'
    if (zero_terminate)
        j++;

    // Return the results
    if (codepoints)
        *codepoints = (has_embedded_zeros ?
                       - static_cast<std::ptrdiff_t>(codepoint_count) :
                       static_cast<std::ptrdiff_t>(codepoint_count));
    if (maxcodepoint)
        *maxcodepoint = wmax;
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Success after incomplete input

 incomplete_input_success:

    // We get here when stop_if_incomplete is set,
    // but only indirectly from one of the following error labels.
    //
    // The value of i should be the start position of the incomplete
    // UTF-8 sequence or escape sequence.
    //
    // We need to consider if the previous code point or escape sequence
    // was the start of a surrogate, and unwind that in case.
    if (previous_surrogate)
    {
        // Uncount the first surrogate half
        if (static_cast<US>(s[i-1]) <= 0x7f)
        {
            // It was a backslash sequence
            while (s[i-1] != '\\')
                i--, n++, character_count--;
        }
        else
            // It was a direct surrogate character (CESU-8)
            i -= 3, n += 3, character_count--;
    }

    // If with_delimiters, we cannot partially parse
    if (with_delimiters)
    {
        i = 0;
        character_count = 0;
    }

    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Error handling

 algorithmic_unicode_conversion:
    n = 0;
    error_type = unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION;
    goto return_error;

 missing_initial_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_INITIAL_DELIMITER;
    goto return_error;

 missing_final_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_FINAL_DELIMITER;
    goto return_error;

 control_character_must_be_quoted:
    n = 1;
    error_type = unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED;
    goto return_error;

 bad_character_after_backslash:
    n = 2;
    error_type = unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_after_backslash:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 1;
    error_type = unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_in_backslash_u:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u;
    goto return_error;

 bad_hexdigit_in_backslash_u:
    n = 6;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u;
    goto return_error;

 end_of_input_in_backslash_U:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U;
    goto return_error;

 bad_hexdigit_in_backslash_U:
    n = 10;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U;
    goto return_error;

 end_of_input_in_backslash_x:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 2;
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x;
    goto return_error;

 bad_hexdigit_in_backslash_x:
    n = 3;
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x;
    goto return_error;

 value_in_backslash_x_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH;
    goto return_error;

 value_in_backslash_octal_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    goto return_error;

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 surrogate_pair_not_completed:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character (CESU-8)
        i -= 3, n += 3, character_count--;
    error_type = unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED;
    goto return_error;

 low_surrogate_without_preceding_high:
    error_type = unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    goto return_error;

 two_high_surrogates:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character (CESU-8)
        i -= 3, n += 3, character_count--;
    error_type = unicode_conversion_error::TWO_HIGH_SURROGATES;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    if (codepoints)
        *codepoints = 0;        // The code points have no meaning yet
    if (maxcodepoint)
        *maxcodepoint = 0;
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    return -1;
}


/* unicode_conversion_verify_unicode()
 *
 * This function verifies that the input string is valid UTF-8/UTF-16/UTF-32.
 */
template<unicode_conversion_encoding senc,
         class S>
inline
int                             // 1 on success, 0 on invalid Unicode
unicode_conversion_verify_unicode(
    S const * const s,          // [i]  input code units in UTF-8/-16/-32
    S const * const send,       // [i]  end pointer of s (past last code unit);
                                //      if NULL, s is '\0'-terminated
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    [[maybe_unused]] uint_fast32_t character_count = 0; // Code points of s
    unicode_conversion_error::errortype error_type;


    for (i = 0;
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (s + i + 1 == send)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (s + i + 2 == send)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (s + i + 3 == send)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (s + i + 4 == send)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (s + i + 5 == send)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (s + i + 1 == send)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
            goto illegal_unicode_codepoint_surrogate;

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF) // Above UNICODE table
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;
    }

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return 1;

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return 0;
}


/* unicode_conversion_decode_char()
 *
 * Decode a Unicode code point from UTF-8/UTF-16/UTF-32 represenation in 'si'
 * into an unsigned long value.
 *
 * Up to four bytes will be read from si, and the return value
 * will be the resulting legal Unicode code point, or the
 * value -1 indicating an illegal codepoint.
 *
 * The five- and six-byte sequences are not used, since Unicode
 * restricts itself to code points up to U+10FFFF.
 *
 * Surrogate pairs: Values between U+D800 and U+DFFF are not
 * legal Unicode code points, they are reserved for surrogate
 * pairs in UTF-16, and are illegal in UTF-8.
 */
template<
    class S,                    // data type of input code units
    int allow_surrogate_pairs,  // [i]  allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero,   // [i]  allow input \0 encoded as hex C0 80
    int quoted_form,            // [i]  whether control chars must be quoted
    long enclose_delimiter = -1,// [i]  if nonnegative enclose in this delimiter
    int zero_terminate          // [i]  whether to append a trailing '\0'
    >
inline
long                            // legal code point if valid, -1 on failure
unicode_conversion_decode_char(
    S const * const s,          // [i] input UTF-8 encoded character
    int *ns,                    // [o] number of code units read; 0 on failure
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    unicode_conversion_error::errortype error_type;


    if (1)
    {
        u = static_cast<US>(s[0]);
        n = 6;                  // For error messages, see return_error
        if (u > 0xff)
            goto illegal_utf8_sequence;

        // Decode the UTF-8 sequence and check against illegal UTF-8
        if (u < 0x80)
            // ASCII character
            w = u, n = 1;
        else if (u < 0xc0)
            goto illegal_utf8_sequence;
        else
        {
            u2 = static_cast<US>(s[1]);
            if (u2 > 0xff)
                goto illegal_utf8_sequence;
            if ((u2 & 0xc0) != 0x80)
                goto illegal_utf8_sequence;
            if (u < 0xe0)
            {
                n = 2;
                w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                    (u2 & 0x3f);
            }
            else
            {
                u3 = static_cast<US>(s[2]);
                if (u3 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u3 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xf0)
                {
                    n = 3;
                    w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                        static_cast<uint32_t>(u2 & 0x3f) << 6 |
                        (u3 & 0x3f);
                }
                else
                {
                    u4 = static_cast<US>(s[3]);
                    if (u4 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u4 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf8)
                    {
                        n = 4;
                        w = static_cast<uint32_t>(u  & 0x07) << 18 |
                            static_cast<uint32_t>(u2 & 0x3f) << 12 |
                            static_cast<uint32_t>(u3 & 0x3f) << 6 |
                            (u4 & 0x3f);
                    }
                    else
                    {
                        u5 = static_cast<US>(s[4]);
                        if (u5 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u5 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xfc)
                        {
                            n = 5;
                            w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                (u5 & 0x3f);
                        }
                        else
                        {
                            u6 = static_cast<US>(s[5]);
                            if (u6 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u6 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfe)
                            {
                                n = 6;
                                w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                    (u6 & 0x3f);
                            }
                            else
                                goto illegal_utf8_sequence;
                        }
                    }
                }
            }
        }

        // Check against the minimum length encoding requirement
        if (w < 0x80)
            m = 1;
        else if (w < 0x800)
            m = 2;
        else if (w < 0x10000ul)
            m = 3;
        else if (w < 0x200000ul)
            m = 4;
        else if (w < 0x4000000ul)
            m = 5;
        else // if (w < 0x80000000ul)
            m = 6;
        if (n != m)
            goto overlong_encoding;

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
            goto illegal_unicode_codepoint_surrogate;

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF) // Above UNICODE table
            goto illegal_unicode_codepoint;

        // All right, the UTF-8 is correct
    }

    if (ns)
        *ns = n;
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<long>(w);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = 0;
        /* error.code_point = 0; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_encode_char()
 *
 * Encode a Unicode code point from an unsigned long value
 * into UTF-8/UTF-16/UTF-32 code units in 'o'.
 * Up to four code units will be written to o.
 *
 * If w is not a valid Unicode code point, the function
 * does nothing and returns 0.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class O                           // data type of output code units
    >
inline
int                             // number of code units used, or 0 on failure
unicode_conversion_encode_char(
    unsigned long w,            // [i] Unicode code point
    O *o                        // [o] output code units UTF-8/UTF-16/UTF-32
    )
{
    int n;

    // Is this part of a surrogate pair?
    if (w >= 0x00D800 &&
        w <= 0x00DFFF)
        return 0;

    // Check that w is a correct UNICODE code point
    if (w > 0x10FFFF) // Above UNICODE table
        return 0;

    if (oenc == unicode_conversion_encoding::utf8)
    {
        if (w < 0x80)
        {
            o[0] = static_cast<unsigned char>(w);
            n = 1;
        }
        else if (w < 0x800)
        {
            o[0] =  static_cast<unsigned char>(0xc0 | w >> 6);
            o[1] =  static_cast<unsigned char>(0x80 | (w & 0x3f));
            n = 2;
        }
        else if (w < 0x10000ul)
        {
            o[0] =  static_cast<unsigned char>(0xe0 |  w >> 12);
            o[1] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
            o[2] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            n = 3;
        }
        else if (w < 0x200000ul)
        {
            o[0] =  static_cast<unsigned char>(0xf0 |  w >> 18);
            o[1] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
            o[2] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
            o[3] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            n = 4;
        }
        /* The 5-byte and 6-byte encodings cannot happen
           because they are outside the correct Unicode
           range that has been checked above. */
        else if (w < 0x4000000ul)
        {
            o[0] =  static_cast<unsigned char>(0xf8 |  w >> 24);
            o[1] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
            o[2] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
            o[3] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
            o[4] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            n = 5;
        }
        else // if (w < 0x80000000ul)
        {
            o[0] =  static_cast<unsigned char>(0xfc |  w >> 30);
            o[1] =  static_cast<unsigned char>(0x80 | (w >> 24 & 0x3f));
            o[2] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
            o[3] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
            o[4] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
            o[5] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            n = 6;
        }
    }
    else if (oenc == unicode_conversion_encoding::utf16)
    {
        if (w > 0xFFFF)
        {
            /* The maximum value of w is 0x10FFFF (checked above).
               The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
            uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
            uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));
            o[0] = static_cast<O>(hi);
            o[1] = static_cast<O>(lo);
            n = 2;
        }
        else
        {
            o[0] = static_cast<O>(w);
            n = 1;
        }
    }
    else if (oenc == unicode_conversion_encoding::utf32)
    {
        o[0] = static_cast<O>(w);
        n = 1;
    }

    return n;
}


/* unicode_conversion_encode_utf16()
 *
 * Encode a UTF-8 string into valid UTF-16 raw or JSON or C output.
 *
 *
 *
 * The output form (raw string or JSON or C encoding) is determined by
 * quote_control_chars, quote_escapes, enclose_delimiter, and zero_terminate:
 *
 *                            RAW     JSON    C
 *                            -----------------
 *   quote_control_chars      0       1       1 or 2
 *   quote_escapes            0       1       1
 *   quote_nonascii           0       0 or 3  0, 1, 2, 4, 5
 *   enclose_delimiter        -1      ?       ? (according to needs)
 *   zero_terminate           0       ?       ? (according to needs)
 *
 * The quote_control_chars flag causes control characters (U+0 to U+1F)
 * to be encoded as \uxxxx sequences, where x are hexadecimal digits.
 * This encoding is required by the JSON specification for serialization
 * of JSON objects. If the flag's value is 2, control characters are
 * instead encoded as \ooo, where o are octal digits. That might be
 * preferred for C.
 *
 * The quote_escapes flag causes certain characters ('"', '\\',
 * '\b', '\f', '\n', '\r', '\t') to be encoded in their JSON two-character
 * forms, just as in C strings. Note that we do not encode the forward
 * slash with a preceding backslash; that is not necessary since a
 * forward slash by itself is a perfectly legal character in a string.
 * When both the quote_control_chars and quote_escaped flags are given,
 * the five control characters of the above list are encoded in the
 * two-character form, since that enhances readability and saves space.
 * In other words, quote_escapes has precedence over quote_control_chars.
 *
 * The quote_nonascii flag causes all characters above 0x7f to be encoded
 * with backslash escapes. There is a choice of 1: \U with eight hexdigits,
 * 2: \U with eight hexdigits or \u with four hexdigits for codepoints
 * in BMP, 3: \u with surrogate pairs outside BMP, 4-6: same as 1-3 but
 * with \ooo (three octal characters without a prefix) for the characters
 * from 0x80 to 0xff.
 *
 * The enclose_delimiter character causes double-quotes '"' or other
 * delimiters to be placed before and after the encoded string.
 * The zero_terminate flag causes a null character '\0' to be appended to
 * the encoded (possibly delimited) string.
 * If these characters are requested, they are included in the output
 * code unit count.
 *
 * The number of code units written corresponds to the number returned by
 * unicode_conversion_encode_utf16_count() called with the same flags.
 *
 *
 *
 * The input string s is not '\0'-terminated, its length is specified
 * by slen. This function expects s to be a valid Unicode string with
 * correct UTF-8/UTF-16/UTF-32 code unit sequences, not CESU-8 or
 * Modified UTF-8. Its encoding is specified by senc.
 *
 * If it is a valid Unicode string, the function returns the number of
 * code units written to the output array o, including enclosing
 * delimiters and a terminating '\0'.
 *
 * If it is not a valid Unicode string, the function returns -1 and sets
 * an error message.
 */
template<
    unicode_conversion_encoding senc, // encoding of input code points
    class S,                          // data type of input code units
    class O                           // data type of output code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_encode_utf16(
    S const * const s,          // [i] input code units, not '\0'-terminated
    unsigned long const slen,   // [i] length of (number of code units in) s
    O *o,                       // [o] output code units according to params
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long n_codepoints_max = -1, // [i] max number of codepoints in s to encode
    int quote_control_chars = 0,// [i] whether to quote control chars
    int quote_escapes = 0,      // [i] whether to quote " and \ and ...
    int quote_nonascii = 0,     // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long enclose_delimiter = -1,// [i] if nonnegative enclose in this delimiter
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;

    if (enclose_delimiter != -1)
        o[j++] = static_cast<O>(enclose_delimiter);


    for (i = 0; i < slen; i += n)
    {
        // In case senc is not utf32,
        // we can limit the code points length of s here;
        // otherwise n is the code point limit anyway
        if (n_codepoints_max != -1 &&
            character_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // Decode input encoding
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (i + 1 == slen)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (i + 2 == slen)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (i + 3 == slen)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (i + 4 == slen)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (i + 5 == slen)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (i + 1 == slen)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;

        // Write as an escape, e.g. "\n"
        if (quote_escapes &&    // Note: We never quote the forward slash
            (w == '"' ||
             w == '\\' ||
             w == '\b' ||
             w == '\f' ||
             w == '\n' ||
             w == '\r' ||
             w == '\t'))
        {
            o[j++] = '\\';
            if (w == '"')
                o[j++] = '"';
            if (w == '\\')
                o[j++] = '\\';
            if (w == '\b')
                o[j++] = 'b';
            if (w == '\f')
                o[j++] = 'f';
            if (w == '\n')
                o[j++] = 'n';
            if (w == '\r')
                o[j++] = 'r';
            if (w == '\t')
                o[j++] = 't';
        }

        // Write as a hex sequence, e.g. "\u123a"
        else if (quote_control_chars && w < 0x20)
        {
            if (quote_control_chars == 2) // C only style
            {
                o[j++] = '\\';

                o[j++] = unicode_conversion_hexchar[(w >>  6) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w >>  3) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0x7];
            }
            else                // JSON or C style
            {
                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = '0';
                o[j++] = '0';
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }
        }

        // Write nonascii characters as backslash sequences
        else if (quote_nonascii && w >= 0x80)
        {
            // Write as octal
            if ((quote_nonascii == 4 ||
                 quote_nonascii == 5 ||
                 quote_nonascii == 6) && w <= 0xff)
            {
                o[j++] = '\\';

                o[j++] = unicode_conversion_hexchar[(w >>  6) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w >>  3) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0x7];
            }
            // Write as C-style \Uxxxxxxxx
            else if (quote_nonascii == 1 ||
                     quote_nonascii == 4 ||
                     ((quote_nonascii == 2 ||
                       quote_nonascii == 5) && w >= 0x10000))
            {
                o[j++] = '\\';
                o[j++] = 'U';

                o[j++] = unicode_conversion_hexchar[(w >> 28) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 24) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 20) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 16) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }

            // else quote_nonascii is 2,3,5,6

            // Write as JSON-style \uxxxx\uxxxx for surrogates
            else if (w >= 0x10000)
            {
                /* The maximum value of w is 0x10FFFF (checked above).
                   The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
                uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
                uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));

                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(hi >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi      ) & 0xf];

                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(lo >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo      ) & 0xf];
            }

            // Write as JSON-style \uxxxx for basic multilingual plane
            else
            {
                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(w >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }
        }

        /* Write Unicode characters from supplementary planes
           as surrogate pairs (required for UTF-16) */
        else if (w >= 0x10000)
        {
            /* The maximum value of w is 0x10FFFF (checked above).
               The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
            uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
            uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));

            o[j++] = static_cast<O>(hi);
            o[j++] = static_cast<O>(lo);
        }

        // Write Unicode characters from BMP directly
        else
        {
            o[j++] = static_cast<O>(w);
        }
    }

    if (enclose_delimiter != -1)
        o[j++] = static_cast<O>(enclose_delimiter);

    if (zero_terminate)
        o[j++] = '\0';

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_encode_utf16_count()
 *
 * This function goes together with unicode_conversion_encode_utf16().
 * It computes the number of code units written by the encoding function.
 * The delimiters and the terminating '\0' character are
 * only counted if the respective flags are set on input.
 */
template<
    unicode_conversion_encoding senc, // encoding of input code points
    class S                           // data type of input code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_encode_utf16_count(
    S const *s,                 // [i] input code units, not '\0'-terminated
    unsigned long slen,         // [i] length of (number of code units in) s
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long n_codepoints_max = -1, // [i] max number of codepoints in s to encode
    int quote_control_chars = 0,// [i] whether to quote control chars
    int quote_escapes = 0,      // [i] whether to quote " and \ and ...
    int quote_nonascii = 0,     // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long enclose_delimiter = -1,// [i] if nonnegative enclose in this delimiter
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;

    if (enclose_delimiter != -1)
        j += 2;


    for (i = 0; i < slen; i += n)
    {
        // In case senc is not utf32,
        // we can limit the code points length of s here;
        // otherwise n is the code point limit anyway
        if (n_codepoints_max != -1 &&
            character_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // Decode input encoding
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (i + 1 == slen)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (i + 2 == slen)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (i + 3 == slen)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (i + 4 == slen)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (i + 5 == slen)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (i + 1 == slen)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;

        // Write as an escape, e.g. "\n"
        if (quote_escapes &&    // Note: We never quote the forward slash
            (w == '"' ||
             w == '\\' ||
             w == '\b' ||
             w == '\f' ||
             w == '\n' ||
             w == '\r' ||
             w == '\t'))
        {
            j += 2;
        }

        // Write as a hex sequence, e.g. "\u123a"
        else if (quote_control_chars && w < 0x20)
        {
            if (quote_control_chars == 2) // C only style
                j += 4;
            else                // JSON or C style
                j += 6;
        }

        // Write nonascii characters as backslash sequences
        else if (quote_nonascii && w >= 0x80)
        {
            // Write as octal
            if ((quote_nonascii == 4 ||
                 quote_nonascii == 5 ||
                 quote_nonascii == 6) && w <= 0xff)
            {
                j += 4;
            }
            // Write as C-style \Uxxxxxxxx
            else if (quote_nonascii == 1 ||
                     quote_nonascii == 4 ||
                     ((quote_nonascii == 2 ||
                       quote_nonascii == 5) && w >= 0x10000))
            {
                j += 10;
            }

            // else quote_nonascii is 2,3,5,6

            // Write as JSON-style \uxxxx\uxxxx for surrogates
            else if (w >= 0x10000)
            {
                j += 12;
            }

            // Write as JSON-style \uxxxx for basic multilingual plane
            else
            {
                j += 6;
            }
        }

        /* Write Unicode characters from supplementary planes
           as surrogate pairs (required for UTF-16) */
        else if (w >= 0x10000)
        {
            j += 2;
        }

        // Write Unicode characters from BMP directly
        else
        {
            j++;
        }
    }

    if (zero_terminate)
        j++;

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_decode_utf16()
 *
 * Decode a UTF-8 string from valid UTF-16 raw or JSON or C input.
 *
 * The input string s can be '\0'-terminated; in that case send
 * should be passed as NULL. If send is not NULL, any '\0' characters
 * in the input string will be passed on to the output.
 *
 *
 *
 * The input form (raw string or JSON or C encoding) is determined by
 * quoted_form and with_delimiters:
 *
 *                            RAW     JSON    C
 *                            -------------------
 *   quoted_form              0       1       2/3
 *   with_delimiters          0       ?       ?   (according to needs)
 *
 * The quoted_form flag equal 1 causes the JSON escape sequences
 * \"   \\   \/   \b   \f   \n   \r   \t   \uxxxx
 * to be correctly decoded into their Unicode equivalents. Note that
 * only Unicode characters of the basic multilingual plane can be
 * encoded with \uxxxx. Codes from the supplementary planes must be
 * given as surrogate pairs of either UTF-16 characters or JSON \u-sequences.
 * The quoted_form flag 1 causes directly encoded (non-backslash-escaped)
 * control characters (U+1F and below) in the UTF-16 input to be not
 * acceptable. It also causes other backslash sequences to be rejected.
 *
 * The quoted_form flag equal 2 or 3 accepts the additional escape sequences
 * of the C language, namely \a, \v, \?, \', \xxx.., \ooo, and \Uxxxxxxxx.
 * It causes surrogate pairs of two \uxxxx encodings to be rejected.
 * Note that according to C rules, the forms \ooo and \xxx... are not
 * interpreted as Unicode code points but as values to be directly
 * stored in a single code unit.
 * With quoted_form equal 2, this code accepts \xxx... and \ooo only
 * for valid Unicode code points that fit into one code unit.
 * With quoted_form equal 3, \ooo and \xxx... are accepted for all
 * values that fit into one code unit, even if they do not represent
 * a valid code point. In that case, the output of this function is
 * not strictly Unicode, and such illegal code units are not counted
 * in the codepoints output variable. A maximum number of codepoints
 * is illegal for this form.
 *
 * The with_delimiters flag makes the function expect and strip
 * surrounding delimiters from the input string. This flag requires
 * the quoted_form flag.
 * If this flag is not given but a delimiter is set, decoding will
 * stop before the delimiter but not count the delimiter.
 *
 *
 * The output string o will always be correct UTF-8: It will only
 * contain the minimum bytes encoding of correct Unicode code points
 * (U+0 through U+D7FF, U+E000 through U+10FFFF).
 *
 * The zero_terminate flag causes a null byte '\0' to be appended
 * to the decoded output string. This byte will be appended regardless
 * of the occurrence of '\0'-bytes within the output string.
 *
 * The codepoints value is set to the count of Unicode code points
 * in the output string. An appended '\0' byte is not counted,
 * but embedded '\0' characters from the input are all counted.
 * In case of embedded '\0' characters, the negative count is returned.
 *
 * The n_cu_parsed value is set to the number of code units read from
 * the input string s.
 * The n_cp_parsed value is set to the number of UTF-16 code points
 * read from the input string s. For escape sequences, each character
 * making up the escape sequence is counted.
 *
 * If the input string is a valid UTF-16 string, and if - in case of
 * quoted_form - all control characters and backslashes are correctly
 * escaped and all backslash escape sequences are valid JSON or C, and
 * if - in case of with_delimiters - the string is correctly enclosed
 * in delimiters, the function returns the number of code units
 * written to the output array o, including a terminating '\0' that
 * has been requested.
 *
 * The number of code units written corresponds to the number returned by
 * unicode_conversion_decode_utf16_count() called with the same flags.
 *
 * If the input string is incorrect UTF-16, or if quoted_form is
 * requested and the JSON or C backslash sequences are incorrect or
 * control characters are unquoted, or if with_delimiters is
 * requested and the string is not correctly enclosed in delimiters,
 * the function returns -1 and sets an error message.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class S,                          // data type of input code units
    class O                           // data type of output code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_decode_utf16(
    S const *s,                 // [i] input code units in UTF-16
    S const *send,              // [i] end pointer of s (past last code unit);
                                //     if NULL, s is '\0'-terminated
    O *o,                       // [o] output code units UTF-8/UTF-16/UTF-32
    std::ptrdiff_t *codepoints, // [o] number of Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o] no. code units read from s; can be 0
    unsigned long *n_cp_parsed, // [o] no. code points read from s; can be 0
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long delimiter = -1,        // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete = 0, // [i] stop parsing before incomplete codepoint
    long n_codepoints_max = -1, // [i] maximum number of codepoints in o
    int quoted_form = 0,        // [i] whether control chars must be quoted
    int with_delimiters = 0,    // [i] require delimiters around input
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    int allow_quoted_surrogates = (quoted_form == 1); // JSON only, \uxxxx only
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    int possible_quoted_surrogate = 0;    // 1 after \uxxxx
    uint_fast32_t i = 0;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    int is_quoted = 0;                    // 1 after '\\'
    US u;
    int_fast8_t d0, d1, d2, d3, d4, d5, d6, d7;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast8_t has_embedded_zeros = 0;
    uint_fast32_t codepoint_count = 0;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;


    // Check the codepoints limit
    if (n_codepoints_max != -1 && quoted_form == 3)
        goto algorithmic_unicode_conversion;

    // Parse the opening delimiter
    if (with_delimiters)
    {
        if (delimiter == -1 || !quoted_form)
            goto algorithmic_unicode_conversion;
        if (stop_if_incomplete && s + i == send)
            goto incomplete_input_success;
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_initial_delimiter;
        i++;
        character_count++;
    }

    for (;                      // i already initialized
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        // Limit the number of code points into o.
        if (n_codepoints_max != -1 &&
            codepoint_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        is_quoted = 0;
        possible_quoted_surrogate = 0;
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0x10FFFF)
        {
            n = 1;
            goto illegal_unicode_codepoint;
        }

        // Found the trailing delimiter
        if (delimiter != -1 && u == static_cast<US>(delimiter))
            break;

        // Control characters must be quoted
        if (quoted_form && u < 0x20)
            goto control_character_must_be_quoted;

        // Found a backslash
        if (quoted_form && u == '\\')
        {
            is_quoted = 1;

            // Decode escaped sequence
            if (s + i + 1 == send)  // '\0' checked below
                goto end_of_input_after_backslash;

            u = static_cast<US>(s[i+1]);
            if (u > 0xff)
                goto bad_character_after_backslash;

            // Unicode escape sequence: \uxxxx
            if (u == 'u')
            {
                possible_quoted_surrogate = allow_quoted_surrogates;
                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_u;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_u;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_u;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_u;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_u;
                w =
                    (1ul << 12) * static_cast<uint8_t>(d0) +
                    (1ul <<  8) * static_cast<uint8_t>(d1) +
                    (1ul <<  4) * static_cast<uint8_t>(d2) +
                    (1ul      ) * static_cast<uint8_t>(d3);
                n = 6;
                character_count += 6;
            }

            // JSON escape sequences
            else if (u == 'n' || u == 't' || u == 'r' ||
                     u == 'f' || u == 'b' || u == '/' ||
                     u == '"' || u == '\\')
            {
                if      (u == 'n')
                    w = '\n';
                else if (u == 't')
                    w = '\t';
                else if (u == 'r')
                    w = '\r';
                else if (u == 'f')
                    w = '\f';
                else if (u == 'b')
                    w = '\b';
                else // \ / "
                    w = u;
                n = 2;
                character_count += 2;
            }

            // End of input via '\0'
            else if (!u && !send)
                goto end_of_input_after_backslash;

            // Any other escape sequence is an error unless in C
            else if (quoted_form != 2 && quoted_form != 3)
                goto bad_character_after_backslash;

            // C escape sequences in addition to JSON
            else if (u == 'a' || u == 'v' || u == '?' ||
                     u == '\'')
            {
                if      (u == 'a')
                    w = '\a';
                else if (u == 'v')
                    w = '\v';
                else // ? '
                    w = u;
                n = 2;
                character_count += 2;
            }

            // Unicode escape sequence: \Uxxxxxxxx (C only)
            else if (u == 'U')
            {
                n = 10;         // For error messages, see return_error

                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_U;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_U;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_U;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_U;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 6 == send)
                    goto end_of_input_in_backslash_U;
                d4 = unicode_conversion_xdigitval(static_cast<US>(s[i+6]));
                if (d4 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 7 == send)
                    goto end_of_input_in_backslash_U;
                d5 = unicode_conversion_xdigitval(static_cast<US>(s[i+7]));
                if (d5 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 8 == send)
                    goto end_of_input_in_backslash_U;
                d6 = unicode_conversion_xdigitval(static_cast<US>(s[i+8]));
                if (d6 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 9 == send)
                    goto end_of_input_in_backslash_U;
                d7 = unicode_conversion_xdigitval(static_cast<US>(s[i+9]));
                if (d7 == -1)
                    goto bad_hexdigit_in_backslash_U;
                w =
                    (1ul << 28) * static_cast<uint8_t>(d0) +
                    (1ul << 24) * static_cast<uint8_t>(d1) +
                    (1ul << 20) * static_cast<uint8_t>(d2) +
                    (1ul << 16) * static_cast<uint8_t>(d3) +
                    (1ul << 12) * static_cast<uint8_t>(d4) +
                    (1ul <<  8) * static_cast<uint8_t>(d5) +
                    (1ul <<  4) * static_cast<uint8_t>(d6) +
                    (1ul      ) * static_cast<uint8_t>(d7);
                n = 10;
                character_count += 10;
            }

            // Hexadecimal escape sequence: \xxxx (C only)
            else if (u == 'x')
            {
                w = 0;
                for (m = 0; m < 10; m++) // read no more than 10 hexdigits
                {
                    if (s + i + 2 + m == send)
                    {
                        if (m == 0)
                            goto end_of_input_in_backslash_x;
                        else
                            break;
                    }
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1)
                    {
                        if (m == 0)
                            goto bad_hexdigit_in_backslash_x;
                        else
                            break;
                    }
                    if (quoted_form == 3 && w > 0xffffffful)
                        goto value_in_backslash_x_too_high;
                    w = (w << 4) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    if (w <= 0xffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    if (w <= 0x10ffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                character_count += n;
            }

            // Octal escape sequence: \ooo (C only)
            else if ( (d0 = unicode_conversion_xdigitval(u)) != -1 && d0 < 7)
            {
                w = static_cast<uint8_t>(d0);
                for (m = 0; m < 2; m++) // read no more than 3 octal digits
                {
                    if (s + i + 2 + m == send)
                        break;
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1 || d0 > 7)
                        break;
                    w = (w << 3) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_octal_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_octal_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        character_count += n;
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                character_count += n;
            }

            // Any other escape sequence is an error
            else
                goto bad_character_after_backslash;
        }


        // Not an escaped sequence but a direct UTF-16 character
        else
        {
            w = u;
            n = 1;
            character_count++;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            // \u-surrogates are not allowed in C, only in JSON
            if (is_quoted && !possible_quoted_surrogate)
            {
                character_count -= n;
                n = 1;
                goto illegal_unicode_codepoint_surrogate;
            }

            // Surrogates of JSON and of UTF-16 cannot be mixed
            if (previous_surrogate)
            {
                // Test only if both forms are principally possible
                if (allow_quoted_surrogates)
                {
                    if (static_cast<US>(s[i-1]) <= 0x7f) // previous was \uxxxx
                    {
                        if (!possible_quoted_surrogate) // this one is UTF-16
                        {
                            character_count--;
                            goto surrogate_pair_not_completed;
                        }
                    }
                    else // previous was UTF-16
                    {
                        if (possible_quoted_surrogate) // this one is \uxxxx
                        {
                            character_count -= n;
                            n = 1;
                            goto surrogate_pair_not_completed;
                        }
                    }
                }
            }

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto two_high_surrogates;
                }
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto low_surrogate_without_preceding_high;
                }
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
                // Count the two UTF-16 units or groups as just one code point
                if (!possible_quoted_surrogate)
                    character_count--;
            }
        }
        else if (previous_surrogate)
        {
            if (possible_quoted_surrogate)
                character_count -= n;
            else
                character_count--;
            goto surrogate_pair_not_completed;
        }


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the UTF-16 is correct

        // Count the code point
        codepoint_count++;

        // Is this an embedded zero?
        if (!w)
            has_embedded_zeros = 1;

        // Store the resulting character w as UTF-8/UTF-16/UTF-32
        if (oenc == unicode_conversion_encoding::utf8)
        {
            if (w < 0x80)
            {
                o[j++] = static_cast<unsigned char>(w);
            }
            else if (w < 0x800)
            {
                o[j++] =  static_cast<unsigned char>(0xc0 | w >> 6);
                o[j++] =  static_cast<unsigned char>(0x80 | (w & 0x3f));
            }
            else if (w < 0x10000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xe0 |  w >> 12);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else if (w < 0x200000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf0 |  w >> 18);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf8 |  w >> 24);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else // if (w < 0x80000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xfc |  w >> 30);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 24 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
        }
        else if (oenc == unicode_conversion_encoding::utf16)
        {
            if (w > 0xFFFF)
            {
                /* The maximum value of w is 0x10FFFF (checked above).
                   The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
                uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
                uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));
                o[j++] = static_cast<O>(hi);
                o[j++] = static_cast<O>(lo);
            }
            else
                o[j++] = static_cast<O>(w);
        }
        else if (oenc == unicode_conversion_encoding::utf32)
        {
            o[j++] = static_cast<O>(w);
        }
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 code units
        if (stop_if_incomplete)
            goto incomplete_input_success;
        goto surrogate_pair_not_completed;
    }

    // Parse the trailing delimiter
    if (with_delimiters)
    {
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_final_delimiter;
        i++;
        character_count++;
    }

    // Append the trailing '\0'
    if (zero_terminate)
        o[j++] = '\0';

    // Return the results
    if (codepoints)
        *codepoints = (has_embedded_zeros ?
                       - static_cast<std::ptrdiff_t>(codepoint_count) :
                       static_cast<std::ptrdiff_t>(codepoint_count));
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Success after incomplete input

 incomplete_input_success:

    // We get here when stop_if_incomplete is set,
    // but only indirectly from one of the following error labels.
    //
    // The value of i should be the start position of the incomplete
    // UTF-8 sequence or escape sequence.
    //
    // We need to consider if the previous code point or escape sequence
    // was the start of a surrogate, and unwind that in case.
    if (previous_surrogate)
    {
        // Uncount the first surrogate half
        if (static_cast<US>(s[i-1]) <= 0x7f)
        {
            // It was a backslash sequence
            while (s[i-1] != '\\')
                i--, n++, character_count--;
        }
        else
            // It was a direct surrogate character
            i--, n++, character_count--;
    }

    // If with_delimiters, we cannot partially parse
    if (with_delimiters)
    {
        i = 0;
        character_count = 0;
    }

    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Error handling

 algorithmic_unicode_conversion:
    n = 0;
    error_type = unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION;
    goto return_error;

 missing_initial_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_INITIAL_DELIMITER;
    goto return_error;

 missing_final_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_FINAL_DELIMITER;
    goto return_error;

 control_character_must_be_quoted:
    n = 1;
    error_type = unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED;
    goto return_error;

 bad_character_after_backslash:
    n = 2;
    error_type = unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_after_backslash:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 1;
    error_type = unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_in_backslash_u:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u;
    goto return_error;

 bad_hexdigit_in_backslash_u:
    n = 6;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u;
    goto return_error;

 end_of_input_in_backslash_U:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U;
    goto return_error;

 bad_hexdigit_in_backslash_U:
    n = 10;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U;
    goto return_error;

 end_of_input_in_backslash_x:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 2;
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x;
    goto return_error;

 bad_hexdigit_in_backslash_x:
    n = 3;
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x;
    goto return_error;

 value_in_backslash_x_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH;
    goto return_error;

 value_in_backslash_octal_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 surrogate_pair_not_completed:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character
        i--, n++, character_count--;
    error_type = unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED;
    goto return_error;

 low_surrogate_without_preceding_high:
    error_type = unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    goto return_error;

 two_high_surrogates:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character
        i--, n++, character_count--;
    error_type = unicode_conversion_error::TWO_HIGH_SURROGATES;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    if (codepoints)
        *codepoints = 0;        // The code points have no meaning yet
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    return -1;
}


/* unicode_conversion_decode_utf16_count()
 *
 * This function goes together with unicode_conversion_decode_utf16().
 * It computes the number of code units written by the decoding function.
 * The terminating '\0' character is counted if it is appended.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class S                           // data type of input code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_decode_utf16_count(
    S const *s,                 // [i] input code units in UTF-16
    S const *send,              // [i] end pointer of s (past last code unit);
                                //     if NULL, s is '\0'-terminated
    std::ptrdiff_t *codepoints, // [o] number of Unicode CP in o; can be 0
    unsigned long *maxcodepoint,// [o] maximum Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o] no. code units read from s; can be 0
    unsigned long *n_cp_parsed, // [o] no. code points read from s; can be 0
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long delimiter = -1,        // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete = 0, // [i] stop parsing before incomplete codepoint
    long n_codepoints_max = -1, // [i] maximum number of codepoints in o
    int quoted_form = 0,        // [i] whether control chars must be quoted
    int with_delimiters = 0,    // [i] require delimiters around input
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    int allow_quoted_surrogates = (quoted_form == 1); // JSON only, \uxxxx only
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    int possible_quoted_surrogate = 0;    // 1 after \uxxxx
    uint_fast32_t i = 0;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    uint_fast32_t wmax = 0;
    int is_quoted = 0;                    // 1 after '\\'
    US u;
    int_fast8_t d0, d1, d2, d3, d4, d5, d6, d7;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast8_t has_embedded_zeros = 0;
    uint_fast32_t codepoint_count = 0;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;


    // Check the codepoints limit
    if (n_codepoints_max != -1 && quoted_form == 3)
        goto algorithmic_unicode_conversion;

    // Parse the opening delimiter
    if (with_delimiters)
    {
        if (delimiter == -1 || !quoted_form)
            goto algorithmic_unicode_conversion;
        if (stop_if_incomplete && s + i == send)
            goto incomplete_input_success;
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_initial_delimiter;
        i++;
        character_count++;
    }

    for (;                      // i already initialized
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        // Limit the number of code points into o.
        if (n_codepoints_max != -1 &&
            codepoint_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        is_quoted = 0;
        possible_quoted_surrogate = 0;
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0x10FFFF)
        {
            n = 1;
            goto illegal_unicode_codepoint;
        }

        // Found the trailing delimiter
        if (delimiter != -1 && u == static_cast<US>(delimiter))
            break;

        // Control characters must be quoted
        if (quoted_form && u < 0x20)
            goto control_character_must_be_quoted;

        // Found a backslash
        if (quoted_form && u == '\\')
        {
            is_quoted = 1;

            // Decode escaped sequence
            if (s + i + 1 == send)  // '\0' checked below
                goto end_of_input_after_backslash;

            u = static_cast<US>(s[i+1]);
            if (u > 0xff)
                goto bad_character_after_backslash;

            // Unicode escape sequence: \uxxxx
            if (u == 'u')
            {
                possible_quoted_surrogate = allow_quoted_surrogates;
                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_u;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_u;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_u;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_u;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_u;
                w =
                    (1ul << 12) * static_cast<uint8_t>(d0) +
                    (1ul <<  8) * static_cast<uint8_t>(d1) +
                    (1ul <<  4) * static_cast<uint8_t>(d2) +
                    (1ul      ) * static_cast<uint8_t>(d3);
                n = 6;
                character_count += 6;
            }

            // JSON escape sequences
            else if (u == 'n' || u == 't' || u == 'r' ||
                     u == 'f' || u == 'b' || u == '/' ||
                     u == '"' || u == '\\')
            {
                if      (u == 'n')
                    w = '\n';
                else if (u == 't')
                    w = '\t';
                else if (u == 'r')
                    w = '\r';
                else if (u == 'f')
                    w = '\f';
                else if (u == 'b')
                    w = '\b';
                else // \ / "
                    w = u;
                n = 2;
                character_count += 2;
            }

            // End of input via '\0'
            else if (!u && !send)
                goto end_of_input_after_backslash;

            // Any other escape sequence is an error unless in C
            else if (quoted_form != 2 && quoted_form != 3)
                goto bad_character_after_backslash;

            // C escape sequences in addition to JSON
            else if (u == 'a' || u == 'v' || u == '?' ||
                     u == '\'')
            {
                if      (u == 'a')
                    w = '\a';
                else if (u == 'v')
                    w = '\v';
                else // ? '
                    w = u;
                n = 2;
                character_count += 2;
            }

            // Unicode escape sequence: \Uxxxxxxxx (C only)
            else if (u == 'U')
            {
                n = 10;         // For error messages, see return_error

                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_U;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_U;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_U;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_U;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 6 == send)
                    goto end_of_input_in_backslash_U;
                d4 = unicode_conversion_xdigitval(static_cast<US>(s[i+6]));
                if (d4 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 7 == send)
                    goto end_of_input_in_backslash_U;
                d5 = unicode_conversion_xdigitval(static_cast<US>(s[i+7]));
                if (d5 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 8 == send)
                    goto end_of_input_in_backslash_U;
                d6 = unicode_conversion_xdigitval(static_cast<US>(s[i+8]));
                if (d6 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 9 == send)
                    goto end_of_input_in_backslash_U;
                d7 = unicode_conversion_xdigitval(static_cast<US>(s[i+9]));
                if (d7 == -1)
                    goto bad_hexdigit_in_backslash_U;
                w =
                    (1ul << 28) * static_cast<uint8_t>(d0) +
                    (1ul << 24) * static_cast<uint8_t>(d1) +
                    (1ul << 20) * static_cast<uint8_t>(d2) +
                    (1ul << 16) * static_cast<uint8_t>(d3) +
                    (1ul << 12) * static_cast<uint8_t>(d4) +
                    (1ul <<  8) * static_cast<uint8_t>(d5) +
                    (1ul <<  4) * static_cast<uint8_t>(d6) +
                    (1ul      ) * static_cast<uint8_t>(d7);
                n = 10;
                character_count += 10;
            }

            // Hexadecimal escape sequence: \xxxx (C only)
            else if (u == 'x')
            {
                w = 0;
                for (m = 0; m < 10; m++) // read no more than 10 hexdigits
                {
                    if (s + i + 2 + m == send)
                    {
                        if (m == 0)
                            goto end_of_input_in_backslash_x;
                        else
                            break;
                    }
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1)
                    {
                        if (m == 0)
                            goto bad_hexdigit_in_backslash_x;
                        else
                            break;
                    }
                    if (quoted_form == 3 && w > 0xffffffful)
                        goto value_in_backslash_x_too_high;
                    w = (w << 4) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    if (w <= 0xffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    if (w <= 0x10ffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                character_count += n;
            }

            // Octal escape sequence: \ooo (C only)
            else if ( (d0 = unicode_conversion_xdigitval(u)) != -1 && d0 < 7)
            {
                w = static_cast<uint8_t>(d0);
                for (m = 0; m < 2; m++) // read no more than 3 octal digits
                {
                    if (s + i + 2 + m == send)
                        break;
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1 || d0 > 7)
                        break;
                    w = (w << 3) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_octal_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        character_count += n;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                character_count += n;
            }

            // Any other escape sequence is an error
            else
                goto bad_character_after_backslash;
        }


        // Not an escaped sequence but a direct UTF-16 character
        else
        {
            w = u;
            n = 1;
            character_count++;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            // \u-surrogates are not allowed in C, only in JSON
            if (is_quoted && !possible_quoted_surrogate)
            {
                character_count -= n;
                n = 1;
                goto illegal_unicode_codepoint_surrogate;
            }

            // Surrogates of JSON and of UTF-16 cannot be mixed
            if (previous_surrogate)
            {
                // Test only if both forms are principally possible
                if (allow_quoted_surrogates)
                {
                    if (static_cast<US>(s[i-1]) <= 0x7f) // previous was \uxxxx
                    {
                        if (!possible_quoted_surrogate) // this one is UTF-16
                        {
                            character_count--;
                            goto surrogate_pair_not_completed;
                        }
                    }
                    else // previous was UTF-16
                    {
                        if (possible_quoted_surrogate) // this one is \uxxxx
                        {
                            character_count -= n;
                            n = 1;
                            goto surrogate_pair_not_completed;
                        }
                    }
                }
            }

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto two_high_surrogates;
                }
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                {
                    if (possible_quoted_surrogate)
                        character_count -= n;
                    else
                        character_count--;
                    goto low_surrogate_without_preceding_high;
                }
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
                // Count the two UTF-16 units or groups as just one code point
                if (!possible_quoted_surrogate)
                    character_count--;
            }
        }
        else if (previous_surrogate)
        {
            if (possible_quoted_surrogate)
                character_count -= n;
            else
                character_count--;
            goto surrogate_pair_not_completed;
        }


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the UTF-16 is correct

        // Count the code point
        codepoint_count++;

        // Is this an embedded zero?
        if (!w)
            has_embedded_zeros = 1;

        // Store the resulting character w as UTF-8/UTF-16/UTF-32
        if (oenc == unicode_conversion_encoding::utf8)
        {
            if (w < 0x80)
            {
                j += 1;
            }
            else if (w < 0x800)
            {
                j += 2;
            }
            else if (w < 0x10000ul)
            {
                j += 3;
            }
            else if (w < 0x200000ul)
            {
                j += 4;
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                j += 5;
            }
            else // if (w < 0x80000000ul)
            {
                j += 6;
            }
        }
        else if (oenc == unicode_conversion_encoding::utf16)
        {
            if (w > 0xFFFF)
            {
                j += 2;
            }
            else
            {
                j += 1;
            }
        }
        else if (oenc == unicode_conversion_encoding::utf32)
        {
            j += 1;
        }

        // Compute the maximum codepoint
        if (wmax < w)
            wmax = w;
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 code units
        if (stop_if_incomplete)
            goto incomplete_input_success;
        goto surrogate_pair_not_completed;
    }

    // Parse the trailing delimiter
    if (with_delimiters)
    {
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_final_delimiter;
        i++;
        character_count++;
    }

    // Append the trailing '\0'
    if (zero_terminate)
        j++;

    // Return the results
    if (codepoints)
        *codepoints = (has_embedded_zeros ?
                       - static_cast<std::ptrdiff_t>(codepoint_count) :
                       static_cast<std::ptrdiff_t>(codepoint_count));
    if (maxcodepoint)
        *maxcodepoint = wmax;
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Success after incomplete input

 incomplete_input_success:

    // We get here when stop_if_incomplete is set,
    // but only indirectly from one of the following error labels.
    //
    // The value of i should be the start position of the incomplete
    // UTF-8 sequence or escape sequence.
    //
    // We need to consider if the previous code point or escape sequence
    // was the start of a surrogate, and unwind that in case.
    if (previous_surrogate)
    {
        // Uncount the first surrogate half
        if (static_cast<US>(s[i-1]) <= 0x7f)
        {
            // It was a backslash sequence
            while (s[i-1] != '\\')
                i--, n++, character_count--;
        }
        else
            // It was a direct surrogate character (CESU-8)
            i--, n++, character_count--;
    }

    // If with_delimiters, we cannot partially parse
    if (with_delimiters)
    {
        i = 0;
        character_count = 0;
    }

    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Error handling

 algorithmic_unicode_conversion:
    n = 0;
    error_type = unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION;
    goto return_error;

 missing_initial_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_INITIAL_DELIMITER;
    goto return_error;

 missing_final_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_FINAL_DELIMITER;
    goto return_error;

 control_character_must_be_quoted:
    n = 1;
    error_type = unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED;
    goto return_error;

 bad_character_after_backslash:
    n = 2;
    error_type = unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_after_backslash:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 1;
    error_type = unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_in_backslash_u:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u;
    goto return_error;

 bad_hexdigit_in_backslash_u:
    n = 6;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u;
    goto return_error;

 end_of_input_in_backslash_U:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U;
    goto return_error;

 bad_hexdigit_in_backslash_U:
    n = 10;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U;
    goto return_error;

 end_of_input_in_backslash_x:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 2;
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x;
    goto return_error;

 bad_hexdigit_in_backslash_x:
    n = 3;
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x;
    goto return_error;

 value_in_backslash_x_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH;
    goto return_error;

 value_in_backslash_octal_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 surrogate_pair_not_completed:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character
        i--, n++, character_count--;
    error_type = unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED;
    goto return_error;

 low_surrogate_without_preceding_high:
    error_type = unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    goto return_error;

 two_high_surrogates:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f)
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++, character_count--;
    }
    else
        // It was a direct surrogate character
        i--, n++, character_count--;
    error_type = unicode_conversion_error::TWO_HIGH_SURROGATES;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    if (codepoints)
        *codepoints = 0;        // The code points have no meaning yet
    if (maxcodepoint)
        *maxcodepoint = 0;
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (n_cp_parsed)
        *n_cp_parsed = character_count; // Code points of s, not of o
    return -1;
}


/* unicode_conversion_encode_utf32()
 *
 * Encode a UTF-8 string into valid UTF-32 raw or JSON or C output.
 *
 *
 *
 * The output form (raw string or JSON or C encoding) is determined by
 * quote_control_chars, quote_escapes, enclose_delimiter, and zero_terminate:
 *
 *                            RAW     JSON    C
 *                            -----------------
 *   quote_control_chars      0       1       1 or 2
 *   quote_escapes            0       1       1
 *   quote_nonascii           0       0 or 3  0, 1, 2, 4, 5
 *   enclose_delimiter        -1      ?       ? (according to needs)
 *   zero_terminate           0       ?       ? (according to needs)
 *
 * The quote_control_chars flag causes control characters (U+0 to U+1F)
 * to be encoded as \uxxxx sequences, where x are hexadecimal digits.
 * This encoding is required by the JSON specification for serialization
 * of JSON objects. If the flag's value is 2, control characters are
 * instead encoded as \ooo, where o are octal digits. That might be
 * preferred for C.
 *
 * The quote_escapes flag causes certain characters ('"', '\\',
 * '\b', '\f', '\n', '\r', '\t') to be encoded in their JSON two-character
 * forms, just as in C strings. Note that we do not encode the forward
 * slash with a preceding backslash; that is not necessary since a
 * forward slash by itself is a perfectly legal character in a string.
 * When both the quote_control_chars and quote_escaped flags are given,
 * the five control characters of the above list are encoded in the
 * two-character form, since that enhances readability and saves space.
 * In other words, quote_escapes has precedence over quote_control_chars.
 *
 * The quote_nonascii flag causes all characters above 0x7f to be encoded
 * with backslash escapes. There is a choice of 1: \U with eight hexdigits,
 * 2: \U with eight hexdigits or \u with four hexdigits for codepoints
 * in BMP, 3: \u with surrogate pairs outside BMP, 4-6: same as 1-3 but
 * with \ooo (three octal characters without a prefix) for the characters
 * from 0x80 to 0xff.
 *
 * The enclose_delimiter character causes double-quotes '"' or other
 * delimiters to be placed before and after the encoded string.
 * The zero_terminate flag causes a null character '\0' to be appended to
 * the encoded (possibly delimited) string.
 * If these characters are requested, they are included in the output
 * code unit count.
 *
 * The number of code units written corresponds to the number returned by
 * unicode_conversion_encode_utf32_count() called with the same flags.
 *
 *
 *
 * The input string s is not '\0'-terminated, its length is specified
 * by slen. This function expects s to be a valid Unicode string with
 * correct UTF-8/UTF-16/UTF-32 code unit sequences, not CESU-8 or
 * Modified UTF-8. Its encoding is specified by senc.
 *
 * If it is a valid Unicode string, the function returns the number of
 * code units written to the output array o, including enclosing
 * delimiters and a terminating '\0'.
 *
 * If it is not a valid Unicode string, the function returns -1 and sets
 * an error message.
 */
template<
    unicode_conversion_encoding senc, // encoding of input code points
    class S,                          // data type of input code units
    class O                           // data type of output code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_encode_utf32(
    S const * const s,          // [i] input code units, not '\0'-terminated
    unsigned long const slen,   // [i] length of (number of code units in) s
    O *o,                       // [o] output code units according to params
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long n_codepoints_max = -1, // [i] max number of codepoints in s to encode
    int quote_control_chars = 0,// [i] whether to quote control chars
    int quote_escapes = 0,      // [i] whether to quote " and \ and ...
    int quote_nonascii = 0,     // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long enclose_delimiter = -1,// [i] if nonnegative enclose in this delimiter
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;

    if (enclose_delimiter != -1)
        o[j++] = static_cast<O>(enclose_delimiter);


    for (i = 0; i < slen; i += n)
    {
        // In case senc is not utf32,
        // we can limit the code points length of s here;
        // otherwise n is the code point limit anyway
        if (n_codepoints_max != -1 &&
            character_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // Decode input encoding
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (i + 1 == slen)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (i + 2 == slen)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (i + 3 == slen)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (i + 4 == slen)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (i + 5 == slen)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (i + 1 == slen)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;

        // Write as an escape, e.g. "\n"
        if (quote_escapes &&    // Note: We never quote the forward slash
            (w == '"' ||
             w == '\\' ||
             w == '\b' ||
             w == '\f' ||
             w == '\n' ||
             w == '\r' ||
             w == '\t'))
        {
            o[j++] = '\\';
            if (w == '"')
                o[j++] = '"';
            if (w == '\\')
                o[j++] = '\\';
            if (w == '\b')
                o[j++] = 'b';
            if (w == '\f')
                o[j++] = 'f';
            if (w == '\n')
                o[j++] = 'n';
            if (w == '\r')
                o[j++] = 'r';
            if (w == '\t')
                o[j++] = 't';
        }

        // Write as a hex sequence, e.g. "\u123a"
        else if (quote_control_chars && w < 0x20)
        {
            if (quote_control_chars == 2) // C only style
            {
                o[j++] = '\\';

                o[j++] = unicode_conversion_hexchar[(w >>  6) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w >>  3) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0x7];
            }
            else                // JSON or C style
            {
                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = '0';
                o[j++] = '0';
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }
        }

        // Write nonascii characters as backslash sequences
        else if (quote_nonascii && w >= 0x80)
        {
            // Write as octal
            if ((quote_nonascii == 4 ||
                 quote_nonascii == 5 ||
                 quote_nonascii == 6) && w <= 0xff)
            {
                o[j++] = '\\';

                o[j++] = unicode_conversion_hexchar[(w >>  6) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w >>  3) & 0x7];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0x7];
            }
            // Write as C-style \Uxxxxxxxx
            else if (quote_nonascii == 1 ||
                     quote_nonascii == 4 ||
                     ((quote_nonascii == 2 ||
                       quote_nonascii == 5) && w >= 0x10000))
            {
                o[j++] = '\\';
                o[j++] = 'U';

                o[j++] = unicode_conversion_hexchar[(w >> 28) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 24) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 20) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 16) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }

            // else quote_nonascii is 2,3,5,6

            // Write as JSON-style \uxxxx\uxxxx for surrogates
            else if (w >= 0x10000)
            {
                /* The maximum value of w is 0x10FFFF (checked above).
                   The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
                uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
                uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));

                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(hi >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(hi      ) & 0xf];

                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(lo >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(lo      ) & 0xf];
            }

            // Write as JSON-style \uxxxx for basic multilingual plane
            else
            {
                o[j++] = '\\';
                o[j++] = 'u';

                o[j++] = unicode_conversion_hexchar[(w >> 12) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  8) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w >>  4) & 0xf];
                o[j++] = unicode_conversion_hexchar[(w      ) & 0xf];
            }
        }

        // Write other Unicode characters directly
        else
        {
            o[j++] = static_cast<O>(w);
        }
    }

    if (enclose_delimiter != -1)
        o[j++] = static_cast<O>(enclose_delimiter);

    if (zero_terminate)
        o[j++] = '\0';

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_encode_utf32_count()
 *
 * This function goes together with unicode_conversion_encode_utf32().
 * It computes the number of code units written by the encoding function.
 * The delimiters and the terminating '\0' character are
 * only counted if the respective flags are set on input.
 */
template<
    unicode_conversion_encoding senc, // encoding of input code points
    class S                           // data type of input code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_encode_utf32_count(
    S const *s,                 // [i] input code units, not '\0'-terminated
    unsigned long slen,         // [i] length of (number of code units in) s
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long n_codepoints_max = -1, // [i] max number of codepoints in s to encode
    int quote_control_chars = 0,// [i] whether to quote control chars
    int quote_escapes = 0,      // [i] whether to quote " and \ and ...
    int quote_nonascii = 0,     // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long enclose_delimiter = -1,// [i] if nonnegative enclose in this delimiter
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    uint_fast32_t i;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast32_t character_count = 0; // Code points of s, not of o
    unicode_conversion_error::errortype error_type;

    if (enclose_delimiter != -1)
        j += 2;


    for (i = 0; i < slen; i += n)
    {
        // In case senc is not utf32,
        // we can limit the code points length of s here;
        // otherwise n is the code point limit anyway
        if (n_codepoints_max != -1 &&
            character_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // Decode input encoding
        if (senc == unicode_conversion_encoding::utf8)
        {
            u = static_cast<US>(s[i]);
            n = 6;              // For error messages, see return_error
            if (u > 0xff)
                goto illegal_utf8_sequence;

            // Decode the UTF-8 sequence and check against illegal UTF-8
            if (u < 0x80)
                // ASCII character
                w = u, n = 1;
            else if (u < 0xc0)
                goto illegal_utf8_sequence;
            else
            {
                if (i + 1 == slen)
                    goto truncated_utf8_sequence;
                u2 = static_cast<US>(s[i+1]);
                if (u2 > 0xff)
                    goto illegal_utf8_sequence;
                if ((u2 & 0xc0) != 0x80)
                    goto illegal_utf8_sequence;
                if (u < 0xe0)
                {
                    n = 2;
                    w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                        (u2 & 0x3f);
                }
                else
                {
                    if (i + 2 == slen)
                        goto truncated_utf8_sequence;
                    u3 = static_cast<US>(s[i+2]);
                    if (u3 > 0xff)
                        goto illegal_utf8_sequence;
                    if ((u3 & 0xc0) != 0x80)
                        goto illegal_utf8_sequence;
                    if (u < 0xf0)
                    {
                        n = 3;
                        w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                            static_cast<uint32_t>(u2 & 0x3f) << 6 |
                            (u3 & 0x3f);
                    }
                    else
                    {
                        if (i + 3 == slen)
                            goto truncated_utf8_sequence;
                        u4 = static_cast<US>(s[i+3]);
                        if (u4 > 0xff)
                            goto illegal_utf8_sequence;
                        if ((u4 & 0xc0) != 0x80)
                            goto illegal_utf8_sequence;
                        if (u < 0xf8)
                        {
                            n = 4;
                            w = static_cast<uint32_t>(u  & 0x07) << 18 |
                                static_cast<uint32_t>(u2 & 0x3f) << 12 |
                                static_cast<uint32_t>(u3 & 0x3f) << 6 |
                                (u4 & 0x3f);
                        }
                        else
                        {
                            if (i + 4 == slen)
                                goto truncated_utf8_sequence;
                            u5 = static_cast<US>(s[i+4]);
                            if (u5 > 0xff)
                                goto illegal_utf8_sequence;
                            if ((u5 & 0xc0) != 0x80)
                                goto illegal_utf8_sequence;
                            if (u < 0xfc)
                            {
                                n = 5;
                                w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                    (u5 & 0x3f);
                            }
                            else
                            {
                                if (i + 5 == slen)
                                    goto truncated_utf8_sequence;
                                u6 = static_cast<US>(s[i+5]);
                                if (u6 > 0xff)
                                    goto illegal_utf8_sequence;
                                if ((u6 & 0xc0) != 0x80)
                                    goto illegal_utf8_sequence;
                                if (u < 0xfe)
                                {
                                    n = 6;
                                    w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                        static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                        static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                        static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                        static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                        (u6 & 0x3f);
                                }
                                else
                                    goto illegal_utf8_sequence;
                            }
                        }
                    }
                }
            }

            // Check against the minimum length encoding requirement
            if (w < 0x80)
                m = 1;
            else if (w < 0x800)
                m = 2;
            else if (w < 0x10000ul)
                m = 3;
            else if (w < 0x200000ul)
                m = 4;
            else if (w < 0x4000000ul)
                m = 5;
            else // if (w < 0x80000000ul)
                m = 6;
            if (n != m)
                goto overlong_encoding;
        }
        else if (senc == unicode_conversion_encoding::utf16)
        {
            u = static_cast<US>(s[i]);
            n = 1;

            if (u > 0xffff)
                goto illegal_utf16_sequence;

            // A low half surrogate cannot be the first in a pair
            if (u >= 0x00DC00 && u <= 0x00DFFF)
                goto illegal_utf16_sequence;

            // A high half surrogate must be followed by a low surrogate
            if (u >= 0x00D800 && u <  0x00DC00)
            {
                if (i + 1 == slen)
                    goto truncated_utf16_sequence;
                u2 = static_cast<US>(s[i+1]);

                // u2 must be a low surrogate
                n = 2;
                if (u2 < 0x00DC00 || u2 > 0x00DFFF)
                    goto illegal_utf16_sequence;

                w = ((static_cast<uint32_t>(u)  & 0x3ff) << 10 |
                     (static_cast<uint32_t>(u2) & 0x3ff)) +
                    0x10000;
            }
            else
                w = u;
        }
        else if (senc == unicode_conversion_encoding::utf32)
        {
            u = static_cast<US>(s[i]);
            w = u;
            n = 1;
        }

        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the input Unicode is correct
        character_count++;

        // Write as an escape, e.g. "\n"
        if (quote_escapes &&    // Note: We never quote the forward slash
            (w == '"' ||
             w == '\\' ||
             w == '\b' ||
             w == '\f' ||
             w == '\n' ||
             w == '\r' ||
             w == '\t'))
        {
            j += 2;
        }

        // Write as a hex sequence, e.g. "\u123a"
        else if (quote_control_chars && w < 0x20)
        {
            if (quote_control_chars == 2) // C only style
                j += 4;
            else                // JSON or C style
                j += 6;
        }

        // Write nonascii characters as backslash sequences
        else if (quote_nonascii && w >= 0x80)
        {
            // Write as octal
            if ((quote_nonascii == 4 ||
                 quote_nonascii == 5 ||
                 quote_nonascii == 6) && w <= 0xff)
            {
                j += 4;
            }
            // Write as C-style \Uxxxxxxxx
            else if (quote_nonascii == 1 ||
                     quote_nonascii == 4 ||
                     ((quote_nonascii == 2 ||
                       quote_nonascii == 5) && w >= 0x10000))
            {
                j += 10;
            }

            // else quote_nonascii is 2,3,5,6

            // Write as JSON-style \uxxxx\uxxxx for surrogates
            else if (w >= 0x10000)
            {
                j += 12;
            }

            // Write as JSON-style \uxxxx for basic multilingual plane
            else
            {
                j += 6;
            }
        }

        // Write other Unicode characters directly
        else
        {
            j++;
        }
    }

    if (zero_terminate)
        j++;

    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);

 illegal_utf8_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF8_SEQUENCE;
    goto return_error;

 truncated_utf8_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF8_SEQUENCE;
    goto return_error;

 illegal_utf16_sequence:
    error_type = unicode_conversion_error::ILLEGAL_UTF16_SEQUENCE;
    goto return_error;

 truncated_utf16_sequence:
    error_type = unicode_conversion_error::TRUNCATED_UTF16_SEQUENCE;
    goto return_error;

 overlong_encoding:
    error_type = unicode_conversion_error::OVERLONG_UTF8_ENCODING;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = character_count; */
        *e = error;
    }
    return -1;
}


/* unicode_conversion_decode_utf32()
 *
 * Decode a UTF-8 string from valid UTF-32 raw or JSON or C input.
 *
 * The input string s can be '\0'-terminated; in that case send
 * should be passed as NULL. If send is not NULL, any '\0' characters
 * in the input string will be passed on to the output.
 *
 *
 *
 * The input form (raw string or JSON or C encoding) is determined by
 * quoted_form and with_delimiters:
 *
 *                            RAW     JSON    C
 *                            -------------------
 *   quoted_form              0       1       2/3
 *   with_delimiters          0       ?       ?   (according to needs)
 *
 * The quoted_form flag equal 1 causes the JSON escape sequences
 * \"   \\   \/   \b   \f   \n   \r   \t   \uxxxx
 * to be correctly decoded into their Unicode equivalents. Note that
 * only Unicode characters of the basic multilingual plane can be
 * encoded with \uxxxx. Codes from the supplementary planes must be
 * given directly (as an UTF-32 character) or as surrogate pairs
 * of JSON \u-sequences.
 * The quoted_form flag 1 causes directly encoded (non-backslash-escaped)
 * control characters (U+1F and below) in the UTF-32 input to be not
 * acceptable. It also causes other backslash sequences to be rejected.
 *
 * The quoted_form flag equal 2 or 3 accepts the additional escape sequences
 * of the C language, namely \a, \v, \?, \', \xxx.., \ooo, and \Uxxxxxxxx.
 * It causes surrogate pairs of two \uxxxx encodings to be rejected.
 * Note that according to C rules, the forms \ooo and \xxx... are not
 * interpreted as Unicode code points but as values to be directly
 * stored in a single code unit.
 * With quoted_form equal 2, this code accepts \xxx... and \ooo only
 * for valid Unicode code points that fit into one code unit.
 * With quoted_form equal 3, \ooo and \xxx... are accepted for all
 * values that fit into one code unit, even if they do not represent
 * a valid code point. In that case, the output of this function is
 * not strictly Unicode, and such illegal code units are not counted
 * in the codepoints output variable. A maximum number of codepoints
 * is illegal for this form.
 *
 * The with_delimiters flag makes the function expect and strip
 * surrounding delimiters from the input string. This flag requires
 * the quoted_form flag.
 * If this flag is not given but a delimiter is set, decoding will
 * stop before the delimiter but not count the delimiter.
 *
 *
 * The output string o will always be correct UTF-8: It will only
 * contain the minimum bytes encoding of correct Unicode code points
 * (U+0 through U+D7FF, U+E000 through U+10FFFF).
 *
 * The zero_terminate flag causes a null byte '\0' to be appended
 * to the decoded output string. This byte will be appended regardless
 * of the occurrence of '\0'-bytes within the output string.
 *
 * The codepoints value is set to the count of Unicode code points
 * in the output string. An appended '\0' byte is not counted,
 * but embedded '\0' characters from the input are all counted.
 * In case of embedded '\0' characters, the negative count is returned.
 *
 * The n_cu_parsed value is set to the number of code units read from
 * the input string s. These are also the input code points.
 *
 * If the input string is a valid UTF-32 string, and if - in case of
 * quoted_form - all control characters and backslashes are correctly
 * escaped and all backslash escape sequences are valid JSON or C, and
 * if - in case of with_delimiters - the string is correctly enclosed
 * in delimiters, the function returns the number of code units
 * written to the output array o, including a terminating '\0' that
 * has been requested.
 *
 * The number of code units written corresponds to the number returned by
 * unicode_conversion_decode_utf32_count() called with the same flags.
 *
 * If the input string is incorrect UTF-32, or if quoted_form is
 * requested and the JSON or C backslash sequences are incorrect or
 * control characters are unquoted, or if with_delimiters is
 * requested and the string is not correctly enclosed in delimiters,
 * the function returns -1 and sets an error message.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class S,                          // data type of input code units
    class O                           // data type of output code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_decode_utf32(
    S const *s,                 // [i] input code units in UTF-32
    S const *send,              // [i] end pointer of s (past last code unit);
                                //     if NULL, s is '\0'-terminated
    O *o,                       // [o] output code units UTF-8/UTF-16/UTF-32
    std::ptrdiff_t *codepoints, // [o] number of Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o] no. code units read from s; can be 0
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long delimiter = -1,        // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete = 0, // [i] stop parsing before incomplete codepoint
    long n_codepoints_max = -1, // [i] maximum number of codepoints in o
    int quoted_form = 0,        // [i] whether control chars must be quoted
    int with_delimiters = 0,    // [i] require delimiters around input
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    int allow_quoted_surrogates = (quoted_form == 1); // JSON only, \uxxxx only
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    int possible_quoted_surrogate = 0;    // 1 after \uxxxx
    uint_fast32_t i = 0;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    // int is_quoted = 0;                    // not needed for UTF-32
    US u;
    int_fast8_t d0, d1, d2, d3, d4, d5, d6, d7;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast8_t has_embedded_zeros = 0;
    uint_fast32_t codepoint_count = 0;
    unicode_conversion_error::errortype error_type;


    // Check the codepoints limit
    if (n_codepoints_max != -1 && quoted_form == 3)
        goto algorithmic_unicode_conversion;

    // Parse the opening delimiter
    if (with_delimiters)
    {
        if (delimiter == -1 || !quoted_form)
            goto algorithmic_unicode_conversion;
        if (stop_if_incomplete && s + i == send)
            goto incomplete_input_success;
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_initial_delimiter;
        i++;
    }

    for (;                      // i already initialized
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        // Limit the number of code points into o.
        if (n_codepoints_max != -1 &&
            codepoint_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // is_quoted = 0;
        possible_quoted_surrogate = 0;
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0x10FFFF)
        {
            n = 1;
            w = u;
            goto illegal_unicode_codepoint;
        }

        // Found the trailing delimiter
        if (delimiter != -1 && u == static_cast<US>(delimiter))
            break;

        // Control characters must be quoted
        if (quoted_form && u < 0x20)
            goto control_character_must_be_quoted;

        // Found a backslash
        if (quoted_form && u == '\\')
        {
            // is_quoted = 1;

            // Decode escaped sequence
            if (s + i + 1 == send)  // '\0' checked below
                goto end_of_input_after_backslash;

            u = static_cast<US>(s[i+1]);
            if (u > 0xff)
                goto bad_character_after_backslash;

            // Unicode escape sequence: \uxxxx
            if (u == 'u')
            {
                possible_quoted_surrogate = allow_quoted_surrogates;
                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_u;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_u;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_u;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_u;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_u;
                w =
                    (1ul << 12) * static_cast<uint8_t>(d0) +
                    (1ul <<  8) * static_cast<uint8_t>(d1) +
                    (1ul <<  4) * static_cast<uint8_t>(d2) +
                    (1ul      ) * static_cast<uint8_t>(d3);
                n = 6;
            }

            // JSON escape sequences
            else if (u == 'n' || u == 't' || u == 'r' ||
                     u == 'f' || u == 'b' || u == '/' ||
                     u == '"' || u == '\\')
            {
                if      (u == 'n')
                    w = '\n';
                else if (u == 't')
                    w = '\t';
                else if (u == 'r')
                    w = '\r';
                else if (u == 'f')
                    w = '\f';
                else if (u == 'b')
                    w = '\b';
                else // \ / "
                    w = u;
                n = 2;
            }

            // End of input via '\0'
            else if (!u && !send)
                goto end_of_input_after_backslash;

            // Any other escape sequence is an error unless in C
            else if (quoted_form != 2 && quoted_form != 3)
                goto bad_character_after_backslash;

            // C escape sequences in addition to JSON
            else if (u == 'a' || u == 'v' || u == '?' ||
                     u == '\'')
            {
                if      (u == 'a')
                    w = '\a';
                else if (u == 'v')
                    w = '\v';
                else // ? '
                    w = u;
                n = 2;
            }

            // Unicode escape sequence: \Uxxxxxxxx (C only)
            else if (u == 'U')
            {
                n = 10;         // For error messages, see return_error

                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_U;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_U;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_U;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_U;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 6 == send)
                    goto end_of_input_in_backslash_U;
                d4 = unicode_conversion_xdigitval(static_cast<US>(s[i+6]));
                if (d4 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 7 == send)
                    goto end_of_input_in_backslash_U;
                d5 = unicode_conversion_xdigitval(static_cast<US>(s[i+7]));
                if (d5 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 8 == send)
                    goto end_of_input_in_backslash_U;
                d6 = unicode_conversion_xdigitval(static_cast<US>(s[i+8]));
                if (d6 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 9 == send)
                    goto end_of_input_in_backslash_U;
                d7 = unicode_conversion_xdigitval(static_cast<US>(s[i+9]));
                if (d7 == -1)
                    goto bad_hexdigit_in_backslash_U;
                w =
                    (1ul << 28) * static_cast<uint8_t>(d0) +
                    (1ul << 24) * static_cast<uint8_t>(d1) +
                    (1ul << 20) * static_cast<uint8_t>(d2) +
                    (1ul << 16) * static_cast<uint8_t>(d3) +
                    (1ul << 12) * static_cast<uint8_t>(d4) +
                    (1ul <<  8) * static_cast<uint8_t>(d5) +
                    (1ul <<  4) * static_cast<uint8_t>(d6) +
                    (1ul      ) * static_cast<uint8_t>(d7);
                n = 10;
            }

            // Hexadecimal escape sequence: \xxxx (C only)
            else if (u == 'x')
            {
                w = 0;
                for (m = 0; m < 10; m++) // read no more than 10 hexdigits
                {
                    if (s + i + 2 + m == send)
                    {
                        if (m == 0)
                            goto end_of_input_in_backslash_x;
                        else
                            break;
                    }
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1)
                    {
                        if (m == 0)
                            goto bad_hexdigit_in_backslash_x;
                        else
                            break;
                    }
                    if (quoted_form == 3 && w > 0xffffffful)
                        goto value_in_backslash_x_too_high;
                    w = (w << 4) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    if (w <= 0xffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    if (w <= 0x10ffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_x_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
            }

            // Octal escape sequence: \ooo (C only)
            else if ( (d0 = unicode_conversion_xdigitval(u)) != -1 && d0 < 7)
            {
                w = static_cast<uint8_t>(d0);
                for (m = 0; m < 2; m++) // read no more than 3 octal digits
                {
                    if (s + i + 2 + m == send)
                        break;
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1 || d0 > 7)
                        break;
                    w = (w << 3) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_octal_too_high;
                    }
                    else // quoted_form == 3
                    {
                        if (sizeof(O) < sizeof(w) &&
                            (w >> (8 * sizeof(O))) != 0)
                            goto value_in_backslash_octal_too_high;

                        /* Store the escape value directly
                           and do not count it as a code point */
                        o[j++] = static_cast<O>(w);
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
            }

            // Any other escape sequence is an error
            else
                goto bad_character_after_backslash;
        }


        // Not an escaped sequence but a direct UTF-32 character
        else
        {
            w = u;
            n = 1;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            /* Surrogate pairs are not allowed in the raw UTF-32 input
               but can be quoted as JSON \uxxxx \uxxxx sequences;
               those are detected by possible_quoted_surrogate */
            if (!possible_quoted_surrogate)
                goto illegal_unicode_codepoint_surrogate;

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                    goto two_high_surrogates;
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                    goto low_surrogate_without_preceding_high;
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
            }
        }
        else if (previous_surrogate)
            goto surrogate_pair_not_completed;


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the UTF-32 is correct

        // Count the code point
        codepoint_count++;

        // Is this an embedded zero?
        if (!w)
            has_embedded_zeros = 1;

        // Store the resulting character w as UTF-8/UTF-16/UTF-32
        if (oenc == unicode_conversion_encoding::utf8)
        {
            if (w < 0x80)
            {
                o[j++] = static_cast<unsigned char>(w);
            }
            else if (w < 0x800)
            {
                o[j++] =  static_cast<unsigned char>(0xc0 | w >> 6);
                o[j++] =  static_cast<unsigned char>(0x80 | (w & 0x3f));
            }
            else if (w < 0x10000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xe0 |  w >> 12);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else if (w < 0x200000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf0 |  w >> 18);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xf8 |  w >> 24);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
            else // if (w < 0x80000000ul)
            {
                o[j++] =  static_cast<unsigned char>(0xfc |  w >> 30);
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 24 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 18 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 12 & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w >> 6  & 0x3f));
                o[j++] =  static_cast<unsigned char>(0x80 | (w       & 0x3f));
            }
        }
        else if (oenc == unicode_conversion_encoding::utf16)
        {
            if (w > 0xFFFF)
            {
                /* The maximum value of w is 0x10FFFF (checked above).
                   The maximum value of (w - 0x10000) is 0xFFFFF (20 bits). */
                uint_fast16_t hi = (0xD800 | ((w - 0x10000) >> 10 & 0x3ff));
                uint_fast16_t lo = (0xDC00 | ((w - 0x10000)       & 0x3ff));
                o[j++] = static_cast<O>(hi);
                o[j++] = static_cast<O>(lo);
            }
            else
                o[j++] = static_cast<O>(w);
        }
        else if (oenc == unicode_conversion_encoding::utf32)
        {
            o[j++] = static_cast<O>(w);
        }
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 UTF-32-chars
        if (stop_if_incomplete)
            goto incomplete_input_success;
        goto surrogate_pair_not_completed;
    }

    // Parse the trailing delimiter
    if (with_delimiters)
    {
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_final_delimiter;
        i++;
    }

    // Append the trailing '\0'
    if (zero_terminate)
        o[j++] = '\0';

    // Return the results
    if (codepoints)
        *codepoints = (has_embedded_zeros ?
                       - static_cast<std::ptrdiff_t>(codepoint_count) :
                       static_cast<std::ptrdiff_t>(codepoint_count));
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Success after incomplete input

 incomplete_input_success:

    // We get here when stop_if_incomplete is set,
    // but only indirectly from one of the following error labels.
    //
    // The value of i should be the start position of the incomplete
    // UTF-8 sequence or escape sequence.
    //
    // We need to consider if the previous code point or escape sequence
    // was the start of a surrogate, and unwind that in case.
    if (previous_surrogate)
    {
        // Uncount the first surrogate half
        if (static_cast<US>(s[i-1]) <= 0x7f) // always true
        {
            // It was a backslash sequence
            while (s[i-1] != '\\')
                i--, n++;
        }
        else
            // It was a direct surrogate character
            i--, n++;
    }

    // If with_delimiters, we cannot partially parse
    if (with_delimiters)
    {
        i = 0;
    }

    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Error handling

 algorithmic_unicode_conversion:
    n = 0;
    error_type = unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION;
    goto return_error;

 missing_initial_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_INITIAL_DELIMITER;
    goto return_error;

 missing_final_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_FINAL_DELIMITER;
    goto return_error;

 control_character_must_be_quoted:
    n = 1;
    error_type = unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED;
    goto return_error;

 bad_character_after_backslash:
    n = 2;
    error_type = unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_after_backslash:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 1;
    error_type = unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_in_backslash_u:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u;
    goto return_error;

 bad_hexdigit_in_backslash_u:
    n = 6;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u;
    goto return_error;

 end_of_input_in_backslash_U:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U;
    goto return_error;

 bad_hexdigit_in_backslash_U:
    n = 10;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U;
    goto return_error;

 end_of_input_in_backslash_x:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 2;
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x;
    goto return_error;

 bad_hexdigit_in_backslash_x:
    n = 3;
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x;
    goto return_error;

 value_in_backslash_x_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH;
    goto return_error;

 value_in_backslash_octal_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 surrogate_pair_not_completed:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f) // always true
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++;
    }
    else
        // It was a direct surrogate character
        i--, n++;
    error_type = unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED;
    goto return_error;

 low_surrogate_without_preceding_high:
    error_type = unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    goto return_error;

 two_high_surrogates:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f) // always true
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++;
    }
    else
        // It was a direct surrogate character
        i--, n++;
    error_type = unicode_conversion_error::TWO_HIGH_SURROGATES;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = i; */
        *e = error;
    }
    if (codepoints)
        *codepoints = 0;        // The code points have no meaning yet
    if (n_cu_parsed)
        *n_cu_parsed = i;
    return -1;
}


/* unicode_conversion_decode_utf32_count()
 *
 * This function goes together with unicode_conversion_decode_utf32().
 * It computes the number of code units written by the decoding function.
 * The terminating '\0' character is counted if it is appended.
 */
template<
    unicode_conversion_encoding oenc, // encoding of output code points
    class S                           // data type of input code units
    >
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
unicode_conversion_decode_utf32_count(
    S const *s,                 // [i] input code units in UTF-32
    S const *send,              // [i] end pointer of s (past last code unit);
                                //     if NULL, s is '\0'-terminated
    std::ptrdiff_t *codepoints, // [o] number of Unicode CP in o; can be 0
    unsigned long *maxcodepoint,// [o] maximum Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o] no. code units read from s; can be 0
    unicode_conversion_error *e,// [o] error description in case of failure
    // Behavior configuration:
    long delimiter = -1,        // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete = 0, // [i] stop parsing before incomplete codepoint
    long n_codepoints_max = -1, // [i] maximum number of codepoints in o
    int quoted_form = 0,        // [i] whether control chars must be quoted
    int with_delimiters = 0,    // [i] require delimiters around input
    int zero_terminate = 0      // [i] whether to append a trailing '\0'
    )
{
    typedef typename std::make_unsigned<S>::type US;
    int allow_quoted_surrogates = (quoted_form == 1); // JSON only, \uxxxx only
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    int possible_quoted_surrogate = 0;    // 1 after \uxxxx
    uint_fast32_t i = 0;
    uint_fast32_t j = 0;
    uint_fast32_t w;
    uint_fast32_t wmax = 0;
    // int is_quoted = 0;                    // not needed for UTF-32
    US u;
    int_fast8_t d0, d1, d2, d3, d4, d5, d6, d7;
    uint_fast8_t n;
    uint_fast8_t m;
    uint_fast8_t has_embedded_zeros = 0;
    uint_fast32_t codepoint_count = 0;
    unicode_conversion_error::errortype error_type;


    // Check the codepoints limit
    if (n_codepoints_max != -1 && quoted_form == 3)
        goto algorithmic_unicode_conversion;

    // Parse the opening delimiter
    if (with_delimiters)
    {
        if (delimiter == -1 || !quoted_form)
            goto algorithmic_unicode_conversion;
        if (stop_if_incomplete && s + i == send)
            goto incomplete_input_success;
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_initial_delimiter;
        i++;
    }


    for (;                      // i already initialized
         s + i != send &&
             (s[i] || send);    // '\0' terminates only if send == NULL
         i += n)
    {
        // Limit the number of code points into o.
        if (n_codepoints_max != -1 &&
            codepoint_count == static_cast<uint_fast32_t>(n_codepoints_max))
            break;

        // is_quoted = 0;
        possible_quoted_surrogate = 0;
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0x10FFFF)
        {
            n = 1;
            w = u;
            goto illegal_unicode_codepoint;
        }

        // Found the trailing delimiter
        if (delimiter != -1 && u == static_cast<US>(delimiter))
            break;

        // Control characters must be quoted
        if (quoted_form && u < 0x20)
            goto control_character_must_be_quoted;

        // Found a backslash
        if (quoted_form && u == '\\')
        {
            // is_quoted = 1;

            // Decode escaped sequence
            if (s + i + 1 == send)  // '\0' checked below
                goto end_of_input_after_backslash;

            u = static_cast<US>(s[i+1]);
            if (u > 0xff)
                goto bad_character_after_backslash;

            // Unicode escape sequence: \uxxxx
            if (u == 'u')
            {
                possible_quoted_surrogate = allow_quoted_surrogates;
                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_u;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_u;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_u;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_u;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_u;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_u;
                w =
                    (1ul << 12) * static_cast<uint8_t>(d0) +
                    (1ul <<  8) * static_cast<uint8_t>(d1) +
                    (1ul <<  4) * static_cast<uint8_t>(d2) +
                    (1ul      ) * static_cast<uint8_t>(d3);
                n = 6;
            }

            // JSON escape sequences
            else if (u == 'n' || u == 't' || u == 'r' ||
                     u == 'f' || u == 'b' || u == '/' ||
                     u == '"' || u == '\\')
            {
                if      (u == 'n')
                    w = '\n';
                else if (u == 't')
                    w = '\t';
                else if (u == 'r')
                    w = '\r';
                else if (u == 'f')
                    w = '\f';
                else if (u == 'b')
                    w = '\b';
                else // \ / "
                    w = u;
                n = 2;
            }

            // End of input via '\0'
            else if (!u && !send)
                goto end_of_input_after_backslash;

            // Any other escape sequence is an error unless in C
            else if (quoted_form != 2 && quoted_form != 3)
                goto bad_character_after_backslash;

            // C escape sequences in addition to JSON
            else if (u == 'a' || u == 'v' || u == '?' ||
                     u == '\'')
            {
                if      (u == 'a')
                    w = '\a';
                else if (u == 'v')
                    w = '\v';
                else // ? '
                    w = u;
                n = 2;
            }

            // Unicode escape sequence: \Uxxxxxxxx (C only)
            else if (u == 'U')
            {
                n = 10;         // For error messages, see return_error

                if (s + i + 2 == send)
                    goto end_of_input_in_backslash_U;
                d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2]));
                if (d0 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 3 == send)
                    goto end_of_input_in_backslash_U;
                d1 = unicode_conversion_xdigitval(static_cast<US>(s[i+3]));
                if (d1 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 4 == send)
                    goto end_of_input_in_backslash_U;
                d2 = unicode_conversion_xdigitval(static_cast<US>(s[i+4]));
                if (d2 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 5 == send)
                    goto end_of_input_in_backslash_U;
                d3 = unicode_conversion_xdigitval(static_cast<US>(s[i+5]));
                if (d3 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 6 == send)
                    goto end_of_input_in_backslash_U;
                d4 = unicode_conversion_xdigitval(static_cast<US>(s[i+6]));
                if (d4 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 7 == send)
                    goto end_of_input_in_backslash_U;
                d5 = unicode_conversion_xdigitval(static_cast<US>(s[i+7]));
                if (d5 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 8 == send)
                    goto end_of_input_in_backslash_U;
                d6 = unicode_conversion_xdigitval(static_cast<US>(s[i+8]));
                if (d6 == -1)
                    goto bad_hexdigit_in_backslash_U;
                if (s + i + 9 == send)
                    goto end_of_input_in_backslash_U;
                d7 = unicode_conversion_xdigitval(static_cast<US>(s[i+9]));
                if (d7 == -1)
                    goto bad_hexdigit_in_backslash_U;
                w =
                    (1ul << 28) * static_cast<uint8_t>(d0) +
                    (1ul << 24) * static_cast<uint8_t>(d1) +
                    (1ul << 20) * static_cast<uint8_t>(d2) +
                    (1ul << 16) * static_cast<uint8_t>(d3) +
                    (1ul << 12) * static_cast<uint8_t>(d4) +
                    (1ul <<  8) * static_cast<uint8_t>(d5) +
                    (1ul <<  4) * static_cast<uint8_t>(d6) +
                    (1ul      ) * static_cast<uint8_t>(d7);
                n = 10;
            }

            // Hexadecimal escape sequence: \xxxx (C only)
            else if (u == 'x')
            {
                w = 0;
                for (m = 0; m < 10; m++) // read no more than 10 hexdigits
                {
                    if (s + i + 2 + m == send)
                    {
                        if (m == 0)
                            goto end_of_input_in_backslash_x;
                        else
                            break;
                    }
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1)
                    {
                        if (m == 0)
                            goto bad_hexdigit_in_backslash_x;
                        else
                            break;
                    }
                    if (quoted_form == 3 && w > 0xffffffful)
                        goto value_in_backslash_x_too_high;
                    w = (w << 4) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    if (w <= 0xffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    if (w <= 0x10ffff && (w < 0xB800 || w > 0xBFFF))
                    {
                        // Continue processing as a Unicode code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-Unicode values are forbidden
                        goto value_in_backslash_x_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        j++;
                        continue;
                    }
                }
            }

            // Octal escape sequence: \ooo (C only)
            else if ( (d0 = unicode_conversion_xdigitval(u)) != -1 && d0 < 7)
            {
                w = static_cast<uint8_t>(d0);
                for (m = 0; m < 2; m++) // read no more than 3 octal digits
                {
                    if (s + i + 2 + m == send)
                        break;
                    d0 = unicode_conversion_xdigitval(static_cast<US>(s[i+2+m]));
                    if (d0 == -1 || d0 > 7)
                        break;
                    w = (w << 3) | static_cast<uint8_t>(d0);
                }
                n = 2 + m;

                if (oenc == unicode_conversion_encoding::utf8)
                {
                    if (w <= 0x7f)
                    {
                        // Continue processing as a Unicde code point
                    }
                    else if (quoted_form == 2)
                    {
                        // All non-ASCII values are forbidden
                        goto value_in_backslash_octal_too_high;
                    }
                    else // quoted_form == 3
                    {
                        /* Store the escape value directly
                           and do not count it as a code point */
                        if (wmax < w)
                            wmax = w;
                        j++;
                        continue;
                    }
                }
                else if (oenc == unicode_conversion_encoding::utf16)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
                else if (oenc == unicode_conversion_encoding::utf32)
                {
                    /* Continue processing as a Unicode code point.
                       The value of w is below 0x200. */
                }
            }

            // Any other escape sequence is an error
            else
                goto bad_character_after_backslash;
        }


        // Not an escaped sequence but a direct UTF-32 character
        else
        {
            w = u;
            n = 1;
        }

        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            /* Surrogate pairs are not allowed in the raw UTF-32 input
               but can be quoted as JSON \uxxxx \uxxxx sequences;
               those are detected by possible_quoted_surrogate */
            if (!possible_quoted_surrogate)
                goto illegal_unicode_codepoint_surrogate;

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                    goto two_high_surrogates;
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                    goto low_surrogate_without_preceding_high;
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
            }
        }
        else if (previous_surrogate)
            goto surrogate_pair_not_completed;


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            goto illegal_unicode_codepoint;

        // All right, the UTF-32 is correct

        // Count the code point
        codepoint_count++;

        // Is this an embedded zero?
        if (!w)
            has_embedded_zeros = 1;

        // Store the resulting character w as UTF-8/UTF-16/UTF-32
        if (oenc == unicode_conversion_encoding::utf8)
        {
            if (w < 0x80)
            {
                j += 1;
            }
            else if (w < 0x800)
            {
                j += 2;
            }
            else if (w < 0x10000ul)
            {
                j += 3;
            }
            else if (w < 0x200000ul)
            {
                j += 4;
            }
            /* The 5-byte and 6-byte encodings cannot happen
               because they are outside the correct Unicode
               range that has been checked above. */
            else if (w < 0x4000000ul)
            {
                j += 5;
            }
            else // if (w < 0x80000000ul)
            {
                j += 6;
            }
        }
        else if (oenc == unicode_conversion_encoding::utf16)
        {
            if (w > 0xFFFF)
            {
                j += 2;
            }
            else
            {
                j += 1;
            }
        }
        else if (oenc == unicode_conversion_encoding::utf32)
        {
            j += 1;
        }

        // Compute the maximum codepoint
        if (wmax < w)
            wmax = w;
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 code units
        if (stop_if_incomplete)
            goto incomplete_input_success;
        goto surrogate_pair_not_completed;
    }

    // Parse the trailing delimiter
    if (with_delimiters)
    {
        if (s + i == send ||
            static_cast<US>(s[i]) != static_cast<US>(delimiter))
            goto missing_final_delimiter;
        i++;
    }

    // Append the trailing '\0'
    if (zero_terminate)
        j++;

    // Return the results
    if (codepoints)
        *codepoints = (has_embedded_zeros ?
                       - static_cast<std::ptrdiff_t>(codepoint_count) :
                       static_cast<std::ptrdiff_t>(codepoint_count));
    if (maxcodepoint)
        *maxcodepoint = wmax;
    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Success after incomplete input

 incomplete_input_success:

    // We get here when stop_if_incomplete is set,
    // but only indirectly from one of the following error labels.
    //
    // The value of i should be the start position of the incomplete
    // UTF-8 sequence or escape sequence.
    //
    // We need to consider if the previous code point or escape sequence
    // was the start of a surrogate, and unwind that in case.
    if (previous_surrogate)
    {
        // Uncount the first surrogate half
        if (static_cast<US>(s[i-1]) <= 0x7f) // always true
        {
            // It was a backslash sequence
            while (s[i-1] != '\\')
                i--, n++;
        }
        else
            // It was a direct surrogate character (CESU-8)
            i--, n++;
    }

    // If with_delimiters, we cannot partially parse
    if (with_delimiters)
    {
        i = 0;
    }

    if (n_cu_parsed)
        *n_cu_parsed = i;
    if (e)
    {
        unicode_conversion_error error;
        error.type = unicode_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(j);


    // Error handling

 algorithmic_unicode_conversion:
    n = 0;
    error_type = unicode_conversion_error::ALGORITHMIC_UNICODE_CONVERSION;
    goto return_error;

 missing_initial_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_INITIAL_DELIMITER;
    goto return_error;

 missing_final_delimiter:
    n = 0;
    error_type = unicode_conversion_error::MISSING_FINAL_DELIMITER;
    goto return_error;

 control_character_must_be_quoted:
    n = 1;
    error_type = unicode_conversion_error::CONTROL_CHARACTER_MUST_BE_QUOTED;
    goto return_error;

 bad_character_after_backslash:
    n = 2;
    error_type = unicode_conversion_error::BAD_CHARACTER_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_after_backslash:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 1;
    error_type = unicode_conversion_error::END_OF_INPUT_AFTER_BACKSLASH;
    goto return_error;

 end_of_input_in_backslash_u:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_u;
    goto return_error;

 bad_hexdigit_in_backslash_u:
    n = 6;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_u;
    goto return_error;

 end_of_input_in_backslash_U:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = static_cast<uint8_t>((send - (s + i)));
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_U;
    goto return_error;

 bad_hexdigit_in_backslash_U:
    n = 10;
    if (n > static_cast<uint8_t>(send - (s + i)))// note n is only 8 bit
        n = static_cast<uint8_t>(send - (s + i));
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_U;
    goto return_error;

 end_of_input_in_backslash_x:
    if (stop_if_incomplete)
        goto incomplete_input_success;
    n = 2;
    error_type = unicode_conversion_error::END_OF_INPUT_IN_BACKSLASH_x;
    goto return_error;

 bad_hexdigit_in_backslash_x:
    n = 3;
    error_type = unicode_conversion_error::BAD_HEXDIGIT_IN_BACKSLASH_x;
    goto return_error;

 value_in_backslash_x_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_x_TOO_HIGH;
    goto return_error;

 value_in_backslash_octal_too_high:
    error_type = unicode_conversion_error::VALUE_IN_BACKSLASH_OCTAL_TOO_HIGH;
    goto return_error;

 illegal_unicode_codepoint:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT;
    goto return_error;

 illegal_unicode_codepoint_surrogate:
    error_type = unicode_conversion_error::ILLEGAL_UNICODE_CODEPOINT_SURROGATE;
    goto return_error;

 surrogate_pair_not_completed:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f) // always true
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++;
    }
    else
        // It was a direct surrogate character
        i--, n++;
    error_type = unicode_conversion_error::SURROGATE_PAIR_NOT_COMPLETED;
    goto return_error;

 low_surrogate_without_preceding_high:
    error_type = unicode_conversion_error::LOW_SURROGATE_WITHOUT_PRECEDING_HIGH;
    goto return_error;

 two_high_surrogates:
    // Uncount the first surrogate half
    if (static_cast<US>(s[i-1]) <= 0x7f) // always true
    {
        // It was a backslash sequence
        while (s[i-1] != '\\')
            i--, n++;
    }
    else
        // It was a direct surrogate character
        i--, n++;
    error_type = unicode_conversion_error::TWO_HIGH_SURROGATES;
    goto return_error;

 return_error:
    if (e)
    {
        unicode_conversion_error error;
        error.type = error_type;
        if (n > unicode_conversion_error::SQ)
            n = unicode_conversion_error::SQ;
        for (m = 0; m < n; m++)
            error.sequence[m] = static_cast<US>(s[i + m]);
        for (; m < unicode_conversion_error::SQ; m++)
            error.sequence[m] = 0;
        error.sequence_len = n;
        error.code_unit = i;
        /* error.code_point = i; */
        *e = error;
    }
    if (codepoints)
        *codepoints = 0;        // The code points have no meaning yet
    if (maxcodepoint)
        *maxcodepoint = 0;
    if (n_cu_parsed)
        *n_cu_parsed = i;
    return -1;
}


// UTF-8 encoding function for str
template<class S,class O>
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
str_encode_utf8(
    S const * const s,          // [i]  input code units, not '\0'-terminated
    unsigned long const slen,   // [i]  length of (number of code units in) s
    O *o,                       // [o]  output code units according to params
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    // Note: with these template parameters,
    // e can only be the following error:
    //  ILLEGAL_UNICODE_CODEPOINT,
    
    return
        unicode_conversion_encode_utf8<
            unicode_conversion_encoding::utf32>(
                s,slen,o,e);
}

template<class S>
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
str_encode_utf8_count(
    S const * const s,          // [i]  input code units, not '\0'-terminated
    unsigned long const slen,   // [i]  length of (number of code units in) s
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    // Note: with these template parameters,
    // e can only be the following error:
    //  ILLEGAL_UNICODE_CODEPOINT,
    
    return
        unicode_conversion_encode_utf8_count<
            unicode_conversion_encoding::utf32>(
                s,slen,e);
}


// UTF-8 decoding function for str
template<class S,class O>
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
str_decode_utf8(
    S const * const s,          // [i]  input code units in UTF-8
    S const * const send,       // [i]  end pointer of s (past last code unit);
                                //      if NULL, s is '\0'-terminated
    O *o,                       // [o]  output code units UTF-32
    unsigned long *n_cu_parsed, // [o]  no. code units read from s; can be 0
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    // Note: with these template parameters,
    // e can be one of the following errors:
    //  ILLEGAL_UTF8_SEQUENCE,
    //  ILLEGAL_UNICODE_CODEPOINT,
    //  ILLEGAL_UNICODE_CODEPOINT_SURROGATE,
    //  TRUNCATED_UTF8_SEQUENCE,
    //  OVERLONG_UTF8_ENCODING,
    
    return
        unicode_conversion_decode_utf8<
            unicode_conversion_encoding::utf32>(
                s,send,o,0,n_cu_parsed,0,e);
}

template<class S>
inline
std::ptrdiff_t                  // code units in o on success, -1 on failure
str_decode_utf8_count(
    S const * const s,          // [i]  input code units in UTF-8
    S const * const send,       // [i]  end pointer of s (past last code unit);
                                //      if NULL, s is '\0'-terminated
    unsigned long *maxcodepoint,// [o]  maximum Unicode CP in o; can be 0
    unsigned long *n_cu_parsed, // [o]  no. code units read from s; can be 0
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    // Note: with these template parameters,
    // e can be one of the following errors:
    //  ILLEGAL_UTF8_SEQUENCE,
    //  ILLEGAL_UNICODE_CODEPOINT,
    //  ILLEGAL_UNICODE_CODEPOINT_SURROGATE,
    //  TRUNCATED_UTF8_SEQUENCE,
    //  OVERLONG_UTF8_ENCODING,
    
    return
        unicode_conversion_decode_utf8_count<
            unicode_conversion_encoding::utf32>(
                s,send,0,maxcodepoint,n_cu_parsed,0,e);
}


// UTF-8 verification function for str
template<class S>
inline
bool                            // true if s is pure UTF-8
str_verify_utf8(
    S const * const s,          // [i]  input code units in UTF-8
    S const * const send,       // [i]  end pointer of s (past last code unit);
                                //      if NULL, s is '\0'-terminated
    unicode_conversion_error *e // [o]  error description in case of failure
    )
{
    return
        unicode_conversion_verify_unicode<
            unicode_conversion_encoding::utf8>(
                s,send,e);
}



} // namespace detail


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_UNICODE_CONVERSION_H_INCLUDED) */
