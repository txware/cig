/* cig_sysdep.c
 *
 * Copyright 2023 Claus Fischer
 *
 * System dependent low-level file operations.
 */
#include "cig_sysdep.h"

/* Auxiliary includes */
#include <stdint.h>		/* int64_t,uint64_t */

#include "cig_sysdep_file.h"



/* cig_sys_selftest()
 *
 * Check some elementary requirements for this code.
 */
char const *			/* static error string or 0 on success */
cig_sys_selftest(void)
{
    char const *selftest_error = 0;

    selftest_error = cig_sys_file_selftest();
    if (selftest_error)
	return selftest_error;

    return 0;
}






/* ** EMACS **
 * Local Variables:
 * mode: C
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * indent-tabs-mode: nil
 * End:
 */
#include <stdio.h>
