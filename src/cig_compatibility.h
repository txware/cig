/* cig_compatibility.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Compiler compatibility for cig.
 */
#ifndef CIG_COMPATIBILITY_H_INCLUDED
#define CIG_COMPATIBILITY_H_INCLUDED


#include <version>


// Implementation of std::bit_cast for pre-C++20.
#ifndef __cpp_lib_bit_cast
namespace std {
template <typename To, typename From>
constexpr auto bit_cast(const From& from) -> To {
    static_assert(sizeof(To) == sizeof(From), "size mismatch");
#   if defined(__APPLE__)
    auto to = __builtin_bit_cast(To,from);
#   else
    auto to = To();
    std::memcpy(&to, &from, sizeof(to));
#endif
    // to = bit_cast<To,From>(from);
    return to;
}
} /* namespace std */
#endif


// CIG_RESTRICT similar to C99 restrict
#if defined(__GNUC__) && ((__GNUC__ > 3) || (__GNUC__ == 3 && __GNUC_MINOR__ >= 1))
#   define CIG_RESTRICT __restrict
#elif defined(_MSC_VER) && _MSC_VER >= 1400
#   define CIG_RESTRICT __restrict
#else
#   define CIG_RESTRICT
#endif


// CIG_CONSTEXPR
#if defined (__clang__) && __clang_major__ < 15
#define CIG_HAVE_CONSTEXPR 0
#define CIG_CONSTEXPR
#else
#define CIG_HAVE_CONSTEXPR 1
#define CIG_CONSTEXPR constexpr
#endif



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_COMPATIBILITY_H_INCLUDED) */
