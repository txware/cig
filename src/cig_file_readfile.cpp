/* cig_file_readfile.c
 *
 * Copyright 2023 Claus Fischer
 *
 * Read a file into a string.
 */
#include "cig_file.h"

#include "cig_exception.h"
#include "cig_sysdep_file.h"
#include "cig_file_descriptor.h"
#include "cig_bytes_inline.h"
#include "cig_str_proxy.h"

namespace cig {

namespace file {

exp_bytes
exp_read_file_into_bytes(str const &filename) noexcept
{
    /* Open the file */
    auto expfd = descriptor::exp_open(filename,true);
    if (!expfd)
        return forwarderr(expfd);
    descriptor fd = std::move(expfd.value());

    /* Get the file size */
    auto expsz = fd.exp_get_size();
    if (!expsz)
        return forwarderr(expsz);
    i64 sz = expsz.value();

    /* Allocate the bytes */
    bytes b;

    /* Read the bytes */
    auto exprd = fd.exp_read(b,sz); // allow resize of b
    if (!exprd)
        return forwarderr(exprd);

    /* Close the file */
    auto expclosed = fd.exp_close();
    if (!expclosed)
        return forwarderr(expclosed);

    return b;
}


exp_str
exp_read_file_into_str(str const &filename) noexcept
{
    auto expb = exp_read_file_into_bytes(filename);
    if (!expb)
        return forwarderr(expb);
    bytes b = std::move(expb.value());

    auto exps = b.decode_utf8();
    if (!exps)
        return forwarderr(exps);
    str s = std::move(exps.value());

    return s;
}


#ifdef CIG_HAVE_EXCEPTIONS


/* with exception */
bytes
read_file_into_bytes(str const &filename)
{
    auto exp = exp_read_file_into_bytes(filename);
    Exception::check(exp);
    return std::move(exp.value());
}


/* with exception */
str
read_file_into_str(str const &filename)
{
    auto exp = exp_read_file_into_str(filename);
    Exception::check(exp);
    return std::move(exp.value());
}


#endif


} // namespace file

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
