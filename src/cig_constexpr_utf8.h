/* cig_constexpr_utf8.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Compile-time conversion templates for UTF-8.
 */
#ifndef CIG_CONSTEXPR_UTF8_H_INCLUDED
#define CIG_CONSTEXPR_UTF8_H_INCLUDED

#include <cstddef>              // std::size_t,std::ptrdiff_t
#include <cstdint>              // uint8_t ...
#include <type_traits>          // make_unsigned
#include <array>		// std::array


namespace cig {


//
// Count code points
//

// Error return values
enum class cig_constexpr_utf8_error {
    NO_ERROR = 0,
    ILLEGAL_UTF8_SEQUENCE = -1,
    ILLEGAL_UNICODE_CODEPOINT = -2,
    ILLEGAL_UNICODE_CODEPOINT_SURROGATE = -3,
    SURROGATE_PAIR_NOT_COMPLETED = -4,
    LOW_SURROGATE_WITHOUT_PRECEDING_HIGH = -5,
    TWO_HIGH_SURROGATES = -6,
    TRUNCATED_UTF8_SEQUENCE = -7,
    OVERLONG_UTF8_ENCODING = -8,
    ILLEGAL_EMBEDDED_ZERO = -9,
    ILLEGAL_FINAL_ZERO = -10,
    MISSING_FINAL_ZERO = -11
};


namespace detail {

template<
    typename S,                 // data type of input code units
    size_t NS,                  // length of input array
    typename append_output,     // append codepoint to output array
    typename make_error         // handle error and prepare return
    >
constexpr auto
cig_constexpr_decode_utf8(
    std::array<S,NS> const s,   // input code units in UTF-8/CESU-8/mod.8
    append_output &output,      // output handler functional class
    make_error &error,          // error handler functional class
    int embedded_zeros,         // 0 keep 1 remove 2 error
    int final_zero,             // 0 allow 1 require 2 error
    int strip_final_zero,       // 0 keep 1 remove
    int append_zero,            // 0 not 1 always 2 if not in input
    int allow_surrogate_pairs,  // 0 not 1 allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero    // 0 not 1 allow input \0 encoded as hex C0 80
    ) -> decltype(error({}))
{
    typedef typename std::make_unsigned<S>::type US;
    std::size_t const have_final_zero = (NS > 0 && s[NS-1] == 0);
    std::size_t const remove_final_zero = (have_final_zero && strip_final_zero);
    std::size_t const ns = NS - remove_final_zero;
    uint_fast32_t i = 0;
    uint_fast32_t w;
    uint_fast32_t previous_surrogate = 0; // High surrogate of a pair, or 0
    US u, u2, u3, u4, u5, u6;
    uint_fast8_t n;
    uint_fast8_t m;

    using enum cig_constexpr_utf8_error;

    // Check the final zero
    if (have_final_zero && final_zero == 2)
        return error(ILLEGAL_FINAL_ZERO);
    if (!have_final_zero && final_zero == 1)
        return error(MISSING_FINAL_ZERO);
    

    for (i = 0; i < ns; i += n)
    {
        u = static_cast<US>(s[i]);
        n = 6;                  // For error messages, see return_error
        if (u > 0xff)
            return error(ILLEGAL_UTF8_SEQUENCE);

        // Decode the UTF-8 sequence and check against illegal UTF-8
        if (u < 0x80)
            // ASCII character
            w = u, n = 1;
        else if (u < 0xc0)
            return error(ILLEGAL_UTF8_SEQUENCE);
        else
        {
            if (i + 1 == ns)
                return error(TRUNCATED_UTF8_SEQUENCE);
            u2 = static_cast<US>(s[i+1]);
            if (u2 > 0xff)
                return error(ILLEGAL_UTF8_SEQUENCE);
            if ((u2 & 0xc0) != 0x80)
                return error(ILLEGAL_UTF8_SEQUENCE);
            if (u < 0xe0)
            {
                n = 2;
                w = static_cast<uint32_t>(u  & 0x1f) << 6 |
                    (u2 & 0x3f);
            }
            else
            {
                if (i + 2 == ns)
                    return error(TRUNCATED_UTF8_SEQUENCE);
                u3 = static_cast<US>(s[i+2]);
                if (u3 > 0xff)
                    return error(ILLEGAL_UTF8_SEQUENCE);
                if ((u3 & 0xc0) != 0x80)
                    return error(ILLEGAL_UTF8_SEQUENCE);
                if (u < 0xf0)
                {
                    n = 3;
                    w = static_cast<uint32_t>(u  & 0x0f) << 12 |
                        static_cast<uint32_t>(u2 & 0x3f) << 6 |
                        (u3 & 0x3f);
                }
                else
                {
                    if (i + 3 == ns)
                        return error(TRUNCATED_UTF8_SEQUENCE);
                    u4 = static_cast<US>(s[i+3]);
                    if (u4 > 0xff)
                        return error(ILLEGAL_UTF8_SEQUENCE);
                    if ((u4 & 0xc0) != 0x80)
                        return error(ILLEGAL_UTF8_SEQUENCE);
                    if (u < 0xf8)
                    {
                        n = 4;
                        w = static_cast<uint32_t>(u  & 0x07) << 18 |
                            static_cast<uint32_t>(u2 & 0x3f) << 12 |
                            static_cast<uint32_t>(u3 & 0x3f) << 6 |
                            (u4 & 0x3f);
                    }
                    else
                    {
                        if (i + 4 == ns)
                            return error(TRUNCATED_UTF8_SEQUENCE);
                        u5 = static_cast<US>(s[i+4]);
                        if (u5 > 0xff)
                            return error(ILLEGAL_UTF8_SEQUENCE);
                        if ((u5 & 0xc0) != 0x80)
                            return error(ILLEGAL_UTF8_SEQUENCE);
                        if (u < 0xfc)
                        {
                            n = 5;
                            w = static_cast<uint32_t>(u  & 0x03) << 24 |
                                static_cast<uint32_t>(u2 & 0x3f) << 18 |
                                static_cast<uint32_t>(u3 & 0x3f) << 12 |
                                static_cast<uint32_t>(u4 & 0x3f) << 6 |
                                (u5 & 0x3f);
                        }
                        else
                        {
                            if (i + 5 == ns)
                                return error(TRUNCATED_UTF8_SEQUENCE);
                            u6 = static_cast<US>(s[i+5]);
                            if (u6 > 0xff)
                                return error(ILLEGAL_UTF8_SEQUENCE);
                            if ((u6 & 0xc0) != 0x80)
                                return error(ILLEGAL_UTF8_SEQUENCE);
                            if (u < 0xfe)
                            {
                                n = 6;
                                w = static_cast<uint32_t>(u  & 0x01) << 30 |
                                    static_cast<uint32_t>(u2 & 0x3f) << 24 |
                                    static_cast<uint32_t>(u3 & 0x3f) << 18 |
                                    static_cast<uint32_t>(u4 & 0x3f) << 12 |
                                    static_cast<uint32_t>(u5 & 0x3f) << 6 |
                                    (u6 & 0x3f);
                            }
                            else
                                return error(ILLEGAL_UTF8_SEQUENCE);
                        }
                    }
                }
            }
        }

        // Check against the minimum length encoding requirement
        if (w < 0x80)
            m = 1;
        else if (w < 0x800)
            m = 2;
        else if (w < 0x10000ul)
            m = 3;
        else if (w < 0x200000ul)
            m = 4;
        else if (w < 0x4000000ul)
            m = 5;
        else // if (w < 0x80000000ul)
            m = 6;
        if (n != m &&
            // Allow modified UTF-8: \0 is encoded as 11000000 10000000
            (!allow_C0_80_for_zero ||
             w != 0 ||
             n != 2))
            return error(OVERLONG_UTF8_ENCODING);


        // Is this part of a surrogate pair?
        if (w >= 0x00D800 &&
            w <= 0x00DFFF)
        {
            // Surrogate pairs can be allowed by commandline
            if (!allow_surrogate_pairs)
                return error(ILLEGAL_UNICODE_CODEPOINT_SURROGATE);

            // High half of surrogate pair
            if (w < 0x00DC00)
            {
                // Start a new surrogate pair
                if (previous_surrogate)
                    return error(TWO_HIGH_SURROGATES);
                previous_surrogate = w;
                continue;
            }
            // Low half of surrogate pair
            else
            {
                // Complete the surrogate pair
                if (!previous_surrogate)
                    return error(LOW_SURROGATE_WITHOUT_PRECEDING_HIGH);
                w = ((previous_surrogate & 0x3ff) << 10 |
                     (                 w & 0x3ff)) +
                    0x10000;
                previous_surrogate = 0;
            }
        }
        else if (previous_surrogate)
            return error(SURROGATE_PAIR_NOT_COMPLETED);


        // Check that the code is a correct UNICODE code point
        if (w > 0x10FFFF || // Above UNICODE table
            (w >= 0xD800 && // Surrogate halves
             w <= 0xDFFF))  // Surrogate halves
            return error(ILLEGAL_UNICODE_CODEPOINT);

        // All right, the UTF-8 (or CESU-8 or Modified UTF-8) is correct

        // Is this an embedded zero?
        if (!w && i + 1 != NS)
        {
            if (embedded_zeros == 0)
            {
                ;               // keep
            }
            else if (embedded_zeros == 1)
                continue;       // remove
            else
                return error(ILLEGAL_EMBEDDED_ZERO);
        }

        // Store the resulting character w
        output(w);
    }


    // Leftover started surrogate pair?
    if (previous_surrogate)
    {
        n = 0;                  // This character has 0 code units
        return error(SURROGATE_PAIR_NOT_COMPLETED);
    }

    // Append the trailing '\0'
    if (append_zero == 1)
        output('\0');
    else if (append_zero == 2 && !have_final_zero)
        output('\0');

    // Return the count of code points
    return {};
}

}; // namespace detail

template<
    class S,                    // data type of input code units
    size_t NS                   // length of input array
    >
constexpr
std::ptrdiff_t                  // code units count or cig_constexpr_utf8_error
cig_constexpr_decode_utf8_count(
    std::array<S,NS> const s,   // input code units in UTF-8/CESU-8/mod.8
    int embedded_zeros=0,       // 0 keep 1 remove 2 error
    int final_zero=0,           // 0 allow 1 require 2 error
    int strip_final_zero=0,     // 0 keep 1 remove
    int append_zero=0,          // 0 not 1 always 2 if not in input
    int allow_surrogate_pairs=0,// 0 not 1 allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero=0  // 0 not 1 allow input \0 encoded as hex C0 80
    )
{
    class error_t
    {
    public:
        [[nodiscard]] constexpr ptrdiff_t
        operator()(cig_constexpr_utf8_error e) const noexcept
        {
            return static_cast<ptrdiff_t>(e);
        }
    };

    class output_t
    {
        uint_fast32_t j_ = 0;

    public:
        constexpr void
        operator()(uint_fast32_t /* c */) noexcept
        {
            j_++;
        }

        [[nodiscard]] constexpr ptrdiff_t
        count() const noexcept {
            return static_cast<ptrdiff_t>(j_);
        }
    };

    error_t error{};
    output_t output{};

    auto r = detail::cig_constexpr_decode_utf8<S,NS,output_t,error_t>(
        s,output,error,
        embedded_zeros,         // 0 keep 1 remove 2 error
        final_zero,             // 0 allow 1 require 2 error
        strip_final_zero,       // 0 keep 1 remove
        append_zero,            // 0 not 1 always 2 if not in input
        allow_surrogate_pairs,  // 0 not 1 allow input surrogate pairs (CESU-8)
        allow_C0_80_for_zero    // 0 not 1 allow input \0 encoded as hex C0 80
        );

    return r < 0 ? r : output.count();
}


//
// Maximum code point
//

// Error return value
constexpr uint32_t cig_constexpr_utf8_badmax = 0xffffffff;

template<
    class S,                    // data type of input code units
    size_t NS                   // length of input array
    >
constexpr
std::uint32_t                   // max code point or cig_constexpr_utf8_error
cig_constexpr_decode_utf8_maxcp(
    std::array<S,NS> const s,   // input code units in UTF-8/CESU-8/mod.8
    int embedded_zeros=0,       // 0 keep 1 remove 2 error
    int final_zero=0,           // 0 allow 1 require 2 error
    int strip_final_zero=0,     // 0 keep 1 remove
    int append_zero=0,          // 0 not 1 always 2 if not in input
    int allow_surrogate_pairs=0,// 0 not 1 allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero=0  // 0 not 1 allow input \0 encoded as hex C0 80
    )
{
    class error_t
    {
    public:
        [[nodiscard]] constexpr uint32_t
        operator()(cig_constexpr_utf8_error) const noexcept
        {
            return static_cast<uint32_t>(cig_constexpr_utf8_badmax);
        }
    };

    class output_t
    {
        uint_fast32_t maxcp_ = 0;

    public:
        constexpr void
        operator()(uint_fast32_t c) noexcept
        {
            if (c > maxcp_)
                maxcp_ = c;
        }

        [[nodiscard]] constexpr uint32_t
        maxcp() const noexcept {
            return static_cast<uint32_t>(maxcp_);
        }
    };

    error_t error{};
    output_t output{};

    auto r = detail::cig_constexpr_decode_utf8<S,NS,output_t,error_t>(
        s,output,error,
        embedded_zeros,         // 0 keep 1 remove 2 error
        final_zero,             // 0 allow 1 require 2 error
        strip_final_zero,       // 0 keep 1 remove
        append_zero,            // 0 not 1 always 2 if not in input
        allow_surrogate_pairs,  // 0 not 1 allow input surrogate pairs (CESU-8)
        allow_C0_80_for_zero    // 0 not 1 allow input \0 encoded as hex C0 80
        );

    // Return error code or the maximum code point
    return r < 0 ? r : output.maxcp();
}


//
// Decode into code point array
//

// O  should be determined by calling maxcp()
// NO should be determined by calling count()

template<
    class O,                    // data type of output code units
    size_t NO,                  // length of output array
    class S,                    // data type of input code units
    size_t NS                   // length of input array
    >
constexpr
std::array<O,NO>                // array of Unicode code points
cig_constexpr_decode_utf8(
    std::array<S,NS> const s,   // input code units in UTF-8/CESU-8/mod.8
    int embedded_zeros=0,       // 0 keep 1 remove 2 error
    int final_zero=0,           // 0 allow 1 require 2 error
    int strip_final_zero=0,     // 0 keep 1 remove
    int append_zero=0,          // 0 not 1 always 2 if not in input
    int allow_surrogate_pairs=0,// 0 not 1 allow input surrogate pairs (CESU-8)
    int allow_C0_80_for_zero=0  // 0 not 1 allow input \0 encoded as hex C0 80
    )
{
    std::array<O,NO> o = { };

    class error_t
    {
    public:
        [[nodiscard]] constexpr uint32_t
        operator()(cig_constexpr_utf8_error) const noexcept
        {
            return static_cast<uint32_t>(cig_constexpr_utf8_badmax);
        }
    };

    class output_t
    {
        std::array<O,NO> &o_ref_;
        uint_fast32_t j_ = 0;

    public:
        constexpr explicit output_t(std::array<O,NO> &o) : o_ref_{o} {};
        constexpr void
        operator()(uint_fast32_t c) noexcept
        {
            o_ref_[j_++] = static_cast<O>(c);
        }
    };

    error_t error{};
    output_t output{o};

    detail::cig_constexpr_decode_utf8<S,NS,output_t,error_t>(
        s,output,error,
        embedded_zeros,         // 0 keep 1 remove 2 error
        final_zero,             // 0 allow 1 require 2 error
        strip_final_zero,       // 0 keep 1 remove
        append_zero,            // 0 not 1 always 2 if not in input
        allow_surrogate_pairs,  // 0 not 1 allow input surrogate pairs (CESU-8)
        allow_C0_80_for_zero    // 0 not 1 allow input \0 encoded as hex C0 80
        );

    // Return code point array
    return o;
}


template<
    class O,                    // data type of output bytes
    size_t NO,                  // length of output array
    class S,                    // data type of input bytes
    size_t NS                   // length of input array
    >
constexpr
std::array<O,NO>                // bytes count or cig_constexpr_utf8_error
cig_constexpr_copy_bytes(
    std::array<S,NS> const s    // input code units in UTF-8/CESU-8/mod.8
    )
{
    typedef typename std::make_unsigned<S>::type US;
    std::array<O,NO> o = { };
    std::size_t i;
    std::size_t N = (NO < NS ? NO : NS);

    for (i = 0; i < N; i++)
        o[i] = static_cast<O>(static_cast<US>(s[i]));

    // Return bytes array
    return o;
}


} /* namespace cig */


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_CONSTEXPR_UTF8_H_INCLUDED) */
