/* cig_datatypes.h
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Runtime type definitions for cig.
 */
#ifndef CIG_DATATYPES_H_INCLUDED
#define CIG_DATATYPES_H_INCLUDED

#include <cstddef>              // std::size_t
#include <cstdint>              // uint8_t ...
#include <cstring>              // strlen
#include <optional>             // std::optional,std::nullopt
#include <utility>              // std::pair

#include "cig_defines.h"        // generated defines for this system
#include "cig_err.h"            // type err


// Note: Class members starting with underscore
//       are not intended for use in the CIG language



namespace cig {


/* CIG integral data types:
 *     bool    (in C++ cig_bool)
 *     u8
 *     u16
 *     u32
 *     u64
 *     i8
 *     i16
 *     i32
 *     i64
 *
 * CIG string and types data types:
 *     str     (in C++ strval, strlit, str, strexpr)
 *     bytes   (in C++ bytesval, byteslit, bytes, bytesexpr)
 */


// Primary data types
struct cig_bool;
struct u8;
struct u16;
struct u32;
struct u64;
struct i8;
struct i16;
struct i32;
struct i64;
struct str;
struct bytes;

// Expected versions
using exp_none  = std::expected<void,err>; // No value on success
using exp_bool  = std::expected<cig_bool,err>;
using exp_u8    = std::expected<u8,err>;
using exp_u16   = std::expected<u16,err>;
using exp_u32   = std::expected<u32,err>;
using exp_u64   = std::expected<u64,err>;
using exp_i8    = std::expected<i8,err>;
using exp_i16   = std::expected<i16,err>;
using exp_i32   = std::expected<i32,err>;
using exp_i64   = std::expected<i64,err>;
using exp_str   = std::expected<str,err>;
using exp_bytes = std::expected<bytes,err>;

// For internal use only
namespace detail {
struct strval;
struct bytesval;
struct strexpr;
struct bytesexpr;
} // namespace detail


/**********************************************************************/
/*                                                                    */
/*                           INTEGRAL TYPES                           */
/*                                                                    */
/**********************************************************************/


struct cig_bool {
    bool _v;

    static constexpr const std::size_t type_alignment = alignof(bool);
    static constexpr const std::size_t type_size = sizeof(bool);
    static constexpr const bool max_value = true;
    static constexpr const bool min_value = false;

    constexpr cig_bool() noexcept : _v{false} {};
    constexpr cig_bool(bool x) noexcept : _v{x} {};

    constexpr           cig_bool(const cig_bool &x) noexcept;
    explicit constexpr  cig_bool(const  u8 &x) noexcept;
    explicit constexpr  cig_bool(const u16 &x) noexcept;
    explicit constexpr  cig_bool(const u32 &x) noexcept;
    explicit constexpr  cig_bool(const u64 &x) noexcept;
    explicit constexpr  cig_bool(const  i8 &x) noexcept;
    explicit constexpr  cig_bool(const i16 &x) noexcept;
    explicit constexpr  cig_bool(const i32 &x) noexcept;
    explicit constexpr  cig_bool(const i64 &x) noexcept;

    cig_bool& operator = (const cig_bool &) = default;
    cig_bool& operator &= (const cig_bool &) noexcept;
    cig_bool& operator ^= (const cig_bool &) noexcept;
    cig_bool& operator |= (const cig_bool &) noexcept;

    // Purposely no destructor. The default destructor does nothing.
    // The value remains in place. This is required by the interpreter.

    constexpr operator bool() const noexcept { return _v; };
    constexpr str   to_str() const;         // false,true
    constexpr bytes to_bytes_utf8() const;  // false,true
};

constexpr cig_bool cig_false(false);
constexpr cig_bool cig_true(true);



struct u8 {
    uint8_t _v;

    static constexpr const std::size_t type_alignment = alignof(uint8_t);
    static constexpr const std::size_t type_size = sizeof(uint8_t);
    static constexpr const uint8_t max_value = 255;
    static constexpr const uint8_t min_value = 0;

    constexpr u8() noexcept : _v{0} {};
    constexpr u8(uint8_t x) noexcept : _v{x} {};

    constexpr           u8(const cig_bool &x) noexcept;
    constexpr           u8(const  u8 &x) = default;
    explicit constexpr  u8(const u16 &x) noexcept;
    explicit constexpr  u8(const u32 &x) noexcept;
    explicit constexpr  u8(const u64 &x) noexcept;
    constexpr           u8(const  i8 &x) noexcept;
    explicit constexpr  u8(const i16 &x) noexcept;
    explicit constexpr  u8(const i32 &x) noexcept;
    explicit constexpr  u8(const i64 &x) noexcept;

    u8& operator = (const u8 &) = default;
    u8& operator += (const u8 &) noexcept;
    u8& operator -= (const u8 &) noexcept;
    u8& operator *= (const u8 &) noexcept;
    u8& operator /= (const u8 &) noexcept;
    u8& operator %= (const u8 &) noexcept;
    u8& operator &= (const u8 &) noexcept;
    u8& operator ^= (const u8 &) noexcept;
    u8& operator |= (const u8 &) noexcept;
    u8& operator <<= (const u8 &) noexcept;
    u8& operator >>= (const u8 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct u16 {
    uint16_t _v;

    static constexpr const std::size_t type_alignment = alignof(uint16_t);
    static constexpr const std::size_t type_size = sizeof(uint16_t);
    static constexpr const uint16_t max_value = 65535u;
    static constexpr const uint16_t min_value = 0;

    constexpr u16() noexcept : _v{0} {};
    constexpr u16(uint16_t x) noexcept : _v{x} {};

    constexpr          u16(const cig_bool &x) noexcept;
    constexpr          u16(const  u8 &x) noexcept;
    constexpr          u16(const u16 &x) = default;
    explicit constexpr u16(const u32 &x) noexcept;
    explicit constexpr u16(const u64 &x) noexcept;
    constexpr          u16(const  i8 &x) noexcept;
    constexpr          u16(const i16 &x) noexcept;
    explicit constexpr u16(const i32 &x) noexcept;
    explicit constexpr u16(const i64 &x) noexcept;

    u16& operator = (const u16 &) = default;
    u16& operator += (const u16 &) noexcept;
    u16& operator -= (const u16 &) noexcept;
    u16& operator *= (const u16 &) noexcept;
    u16& operator /= (const u16 &) noexcept;
    u16& operator %= (const u16 &) noexcept;
    u16& operator &= (const u16 &) noexcept;
    u16& operator ^= (const u16 &) noexcept;
    u16& operator |= (const u16 &) noexcept;
    u16& operator <<= (const u16 &) noexcept;
    u16& operator >>= (const u16 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct u32 {
    uint32_t _v;

    static constexpr const std::size_t type_alignment = alignof(uint32_t);
    static constexpr const std::size_t type_size = sizeof(uint32_t);
    static constexpr const uint32_t max_value = 4294967295ul;
    static constexpr const uint32_t min_value = 0;

    constexpr u32() noexcept : _v{0} {};
    constexpr u32(uint32_t x) noexcept : _v{x} {};

    constexpr          u32(const cig_bool &x) noexcept;
    constexpr          u32(const  u8 &x) noexcept;
    constexpr          u32(const u16 &x) noexcept;
    constexpr          u32(const u32 &x) = default;
    explicit constexpr u32(const u64 &x) noexcept;
    constexpr          u32(const  i8 &x) noexcept;
    constexpr          u32(const i16 &x) noexcept;
    constexpr          u32(const i32 &x) noexcept;
    explicit constexpr u32(const i64 &x) noexcept;

    u32& operator = (const u32 &) = default;
    u32& operator += (const u32 &) noexcept;
    u32& operator -= (const u32 &) noexcept;
    u32& operator *= (const u32 &) noexcept;
    u32& operator /= (const u32 &) noexcept;
    u32& operator %= (const u32 &) noexcept;
    u32& operator &= (const u32 &) noexcept;
    u32& operator ^= (const u32 &) noexcept;
    u32& operator |= (const u32 &) noexcept;
    u32& operator <<= (const u32 &) noexcept;
    u32& operator >>= (const u32 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct u64 {
    uint64_t _v;

    static constexpr const std::size_t type_alignment = alignof(uint64_t);
    static constexpr const std::size_t type_size = sizeof(uint64_t);
    static constexpr const uint64_t max_value = 18446744073709551615ull;
    static constexpr const uint64_t min_value = 0;

    constexpr u64() noexcept : _v{0} {};
    constexpr u64(uint64_t x) noexcept : _v{x} {};

    constexpr          u64(const cig_bool &x) noexcept;
    constexpr          u64(const  u8 &x) noexcept;
    constexpr          u64(const u16 &x) noexcept;
    constexpr          u64(const u32 &x) noexcept;
    constexpr          u64(const u64 &x) = default;
    constexpr          u64(const  i8 &x) noexcept;
    constexpr          u64(const i16 &x) noexcept;
    constexpr          u64(const i32 &x) noexcept;
    constexpr          u64(const i64 &x) noexcept;

    u64& operator = (const u64 &) = default;
    u64& operator += (const u64 &) noexcept;
    u64& operator -= (const u64 &) noexcept;
    u64& operator *= (const u64 &) noexcept;
    u64& operator /= (const u64 &) noexcept;
    u64& operator %= (const u64 &) noexcept;
    u64& operator &= (const u64 &) noexcept;
    u64& operator ^= (const u64 &) noexcept;
    u64& operator |= (const u64 &) noexcept;
    u64& operator <<= (const u64 &) noexcept;
    u64& operator >>= (const u64 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct i8 {
    int8_t _v;

    static constexpr const std::size_t type_alignment = alignof(int8_t);
    static constexpr const std::size_t type_size = sizeof(int8_t);
    static constexpr const int8_t max_value = 127;
    static constexpr const int8_t min_value = -128;

    constexpr i8() noexcept : _v{0} {};
    constexpr i8(int8_t x) noexcept : _v{x} {};

    constexpr           i8(const cig_bool &x) noexcept;
    explicit constexpr  i8(const  u8 &x) noexcept;
    explicit constexpr  i8(const u16 &x) noexcept;
    explicit constexpr  i8(const u32 &x) noexcept;
    explicit constexpr  i8(const u64 &x) noexcept;
    constexpr           i8(const  i8 &x) = default;
    explicit constexpr  i8(const i16 &x) noexcept;
    explicit constexpr  i8(const i32 &x) noexcept;
    explicit constexpr  i8(const i64 &x) noexcept;

    i8& operator = (const i8 &) = default;
    i8& operator += (const i8 &) noexcept;
    i8& operator -= (const i8 &) noexcept;
    i8& operator *= (const i8 &) noexcept;
    i8& operator /= (const i8 &) noexcept;
    i8& operator %= (const i8 &) noexcept;
    i8& operator &= (const i8 &) noexcept;
    i8& operator ^= (const i8 &) noexcept;
    i8& operator |= (const i8 &) noexcept;
    i8& operator <<= (const i8 &) noexcept;
    i8& operator >>= (const i8 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct i16 {
    int16_t _v;

    static constexpr const std::size_t type_alignment = alignof(int16_t);
    static constexpr const std::size_t type_size = sizeof(int16_t);
    static constexpr const int16_t max_value = 32767;
    static constexpr const int16_t min_value = -32767-1;

    constexpr i16() noexcept : _v{0} {};
    constexpr i16(int16_t x) noexcept : _v{x} {};

    constexpr          i16(const cig_bool &x) noexcept;
    constexpr          i16(const  u8 &x) noexcept;
    explicit constexpr i16(const u16 &x) noexcept;
    explicit constexpr i16(const u32 &x) noexcept;
    explicit constexpr i16(const u64 &x) noexcept;
    constexpr          i16(const  i8 &x) noexcept;
    constexpr          i16(const i16 &x) = default;
    explicit constexpr i16(const i32 &x) noexcept;
    explicit constexpr i16(const i64 &x) noexcept;

    i16& operator = (const i16 &) = default;
    i16& operator += (const i16 &) noexcept;
    i16& operator -= (const i16 &) noexcept;
    i16& operator *= (const i16 &) noexcept;
    i16& operator /= (const i16 &) noexcept;
    i16& operator %= (const i16 &) noexcept;
    i16& operator &= (const i16 &) noexcept;
    i16& operator ^= (const i16 &) noexcept;
    i16& operator |= (const i16 &) noexcept;
    i16& operator <<= (const i16 &) noexcept;
    i16& operator >>= (const i16 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct i32 {
    int32_t _v;

    static constexpr const std::size_t type_alignment = alignof(int32_t);
    static constexpr const std::size_t type_size = sizeof(int32_t);
    static constexpr const int32_t max_value = 2147483647l;
    static constexpr const int32_t min_value = -2147483647l-1;

    constexpr i32() noexcept : _v{0} {};
    constexpr i32(int32_t x) noexcept : _v{x} {};

    constexpr          i32(const cig_bool &x) noexcept;
    constexpr          i32(const  u8 &x) noexcept;
    constexpr          i32(const u16 &x) noexcept;
    explicit constexpr i32(const u32 &x) noexcept;
    explicit constexpr i32(const u64 &x) noexcept;
    constexpr          i32(const  i8 &x) noexcept;
    constexpr          i32(const i16 &x) noexcept;
    constexpr          i32(const i32 &x) = default;
    explicit constexpr i32(const i64 &x) noexcept;

    i32& operator = (const i32 &) = default;
    i32& operator += (const i32 &) noexcept;
    i32& operator -= (const i32 &) noexcept;
    i32& operator *= (const i32 &) noexcept;
    i32& operator /= (const i32 &) noexcept;
    i32& operator %= (const i32 &) noexcept;
    i32& operator &= (const i32 &) noexcept;
    i32& operator ^= (const i32 &) noexcept;
    i32& operator |= (const i32 &) noexcept;
    i32& operator <<= (const i32 &) noexcept;
    i32& operator >>= (const i32 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};

struct i64 {
    int64_t _v;

    static constexpr const std::size_t type_alignment = alignof(int64_t);
    static constexpr const std::size_t type_size = sizeof(int64_t);
    static constexpr const int64_t max_value = 9223372036854775807ll;
    static constexpr const int64_t min_value = -9223372036854775807ll-1;

    constexpr i64() noexcept : _v{0} {};
    constexpr i64(int64_t x) noexcept : _v{x} {};

    constexpr          i64(const cig_bool &x) noexcept;
    constexpr          i64(const  u8 &x) noexcept;
    constexpr          i64(const u16 &x) noexcept;
    constexpr          i64(const u32 &x) noexcept;
    explicit constexpr i64(const u64 &x) noexcept;
    constexpr          i64(const  i8 &x) noexcept;
    constexpr          i64(const i16 &x) noexcept;
    constexpr          i64(const i32 &x) noexcept;
    constexpr          i64(const i64 &x) = default;

    i64& operator = (const i64 &) = default;
    i64& operator += (const i64 &) noexcept;
    i64& operator -= (const i64 &) noexcept;
    i64& operator *= (const i64 &) noexcept;
    i64& operator /= (const i64 &) noexcept;
    i64& operator %= (const i64 &) noexcept;
    i64& operator &= (const i64 &) noexcept;
    i64& operator ^= (const i64 &) noexcept;
    i64& operator |= (const i64 &) noexcept;
    i64& operator <<= (const i64 &) noexcept;
    i64& operator >>= (const i64 &) noexcept;

    constexpr str to_str() const;          // itoa
    constexpr bytes to_bytes_utf8() const; // itoa
};



/**********************************************************************/
/*                                                                    */
/*                            STRING TYPE                             */
/*                                                                    */
/**********************************************************************/


namespace detail {


// String value class
struct strval {

    /* This structure is inspired by the std::string type in llvm's libc++.

       The least significant bit of cap indicates:
       0 ... the string is stored immediately (a short string)
       1 ... the string uses pointers (a long string); allocated or constant

       The next two bits indicate the character type:
       ascii ... code points below 128, i.e. the characters in ASCII
       UCS-1 ... code points below 256, i.e. the characters in ISO/IEC 8859-1
       UCS-2 ... code points below 0x10000, the basic multilingual plane BMP
       UCS-4 ... all Unicode code points up to 0x10FFFF

       If the string is long, the _long structure defines its content:
       cap            ... (_cap & _capacity_mask) is the allocated length;
                          if zero, the memory is not allocated but points
                          to constant and must not be changed or freed
       size           ... the length of the string in characters, not bytes
       ucs1/ucs2/ucs4 ... the pointer to the allocated string

       If the string is short, the upper five bits of the least significant
       byte indicate the length of the string (shifted by 3 bits).
       On 64 bit little-endian systems, the maximum value is 22.
       The characters are stored in c8[1..22] or c16[1..10] or c32[1..4].
       On 32 bit little-endian systems, the maximum value is 10.
       The characters are stored in c8[1..10] or c16[1..10] or c32[1..4].
       On big-endian systems, first and last offset must be reduced by 1.

       The last character is always followed by a '\0'.

       Strings are always allocated in multiples of 16 characters,
       therefore the lowest four bits are always zero. In those bits,
       the short/long indicator and the character type are stored.

       Note that an all-zero value of str is a valid empty string. */



    // TYPES AND CONSTANTS

    static constexpr const std::size_t type_alignment = alignof(std::size_t);
    static constexpr const std::size_t type_size = 3 * sizeof(std::size_t);

    typedef uint8_t  ucs1_t;    // To hold ascii and latin1 code points
    typedef uint16_t ucs2_t;    // To hold BMP code points
    typedef uint32_t ucs4_t;    // To hold all code points of Unicode

    enum { _long_mask = 0x01 };
    enum  _chartype { _chartype_ascii = 0x00,
                      _chartype_ucs1  = 0x02,
                      _chartype_ucs2  = 0x04,
                      _chartype_ucs4  = 0x06 };
    enum {            _chartype_mask  = 0x06 };
    enum { _capbits_mask  = (static_cast<uint8_t>(_long_mask) |
                             static_cast<uint8_t>(_chartype_mask)) };
    static constexpr std::size_t _capacity_mask =
        ~static_cast<std::size_t>(0x0f);

#   if defined(CIG_BIG_ENDIAN)
    struct _long {
        // Avoid pointers and unions since they are not constexpr-safe
        // union {
        //     ucs1_t *_ucs1;
        //     ucs2_t *_ucs2;
        //     ucs4_t *_ucs4;
        // } _data;
        std::size_t _data;      // size validated in selftest
        std::size_t _size;
        std::size_t _cap;       // allocated_size | chartype | long_bit
    };
    enum {  _c8_len = sizeof(struct _long) / sizeof(ucs1_t) };
    enum { _c16_len = sizeof(struct _long) / sizeof(ucs2_t) };
    enum { _c32_len = sizeof(struct _long) / sizeof(ucs4_t) };

    enum {  _c8_length_byte = _c8_len - 1 };
    enum {  _c8_len_net = _c8_len - 2 };
    enum {  _c8_offset = 0 };

    enum {  _c16_length_byte = _c16_len - 1 };
    enum {  _c16_len_net = _c16_len - 2 };
    enum {  _c16_offset = 0 };

    enum {  _c32_length_byte = _c32_len - 1 };
    enum {  _c32_len_net = _c32_len - 2 };
    enum {  _c32_offset = 0 };
#   elif defined(CIG_LITTLE_ENDIAN)
    struct _long {
        std::size_t _cap;       // allocated_size | chartype | long_bit
        std::size_t _size;
        // Avoid pointers and unions since they are not constexpr-safe
        // union {
        //     ucs1_t *_ucs1;
        //     ucs2_t *_ucs2;
        //     ucs4_t *_ucs4;
        // } _data;
        std::size_t _data;      // size validated in selftest
    };
    enum {  _c8_len = sizeof(struct _long) / sizeof(ucs1_t) };
    enum { _c16_len = sizeof(struct _long) / sizeof(ucs2_t) };
    enum { _c32_len = sizeof(struct _long) / sizeof(ucs4_t) };

    enum {  _c8_length_byte = 0 };
    enum {  _c8_len_net = _c8_len - 2 };
    enum {  _c8_offset = 1 };

    enum {  _c16_length_byte = 0 };
    enum {  _c16_len_net = _c16_len - 2 };
    enum {  _c16_offset = 1 };

    enum {  _c32_length_byte = 0 };
    enum {  _c32_len_net = _c32_len - 2 };
    enum {  _c32_offset = 1 };
#   else
#   error byte order not defined
#   endif

    // Avoid pointers and unions since they are not constexpr-safe
    struct _short_u8 {
        ucs1_t c8[ _c8_len];
    };
    struct _short_u16 {
        ucs2_t c16[_c16_len];
    };
    struct _short_u32 {
        ucs4_t c32[_c32_len];
    };


    // DATA


    _long _l;                   // use std::bit_cast to cast appropriately


    // FUNCTIONS


    static constexpr std::size_t _intcapacity_by_chartype(_chartype t) noexcept;
    static constexpr bool _is_pointered_by_cap(std::size_t cap) noexcept;
    static constexpr bool _is_allocated_by_cap(std::size_t cap) noexcept;
    static constexpr _chartype _chartype_by_cap(std::size_t cap) noexcept;
    static constexpr bool _is_ucs1_by_cap(std::size_t cap) noexcept; // +ascii
    static constexpr bool _is_ucs2_by_cap(std::size_t cap) noexcept;
    static constexpr bool _is_ucs4_by_cap(std::size_t cap) noexcept;
    static constexpr bool _is_ascii_by_cap(std::size_t cap) noexcept;
    static constexpr std::size_t _capacity_by_cap(std::size_t cap) noexcept;
    static constexpr std::size_t _cap_by_components(
        std::size_t capacity,_chartype chartype,bool is_pointered) noexcept;
    static constexpr std::size_t _size_to_allocate(
        std::size_t size, bool duplicate = false) noexcept;
    static constexpr _chartype _chartype_by_max_codepoint(
        uint32_t maxcp) noexcept;
    constexpr bool _is_pointered() const noexcept;
    constexpr bool _is_allocated() const noexcept;
    constexpr _chartype _get_chartype() const noexcept;
    constexpr bool _is_ucs1() const noexcept; // chartype ucs1 or ascii
    constexpr bool _is_ucs2() const noexcept;
    constexpr bool _is_ucs4() const noexcept;
    constexpr bool _is_ascii() const noexcept;
    constexpr ucs1_t *_data_ucs1() noexcept;
    constexpr ucs2_t *_data_ucs2() noexcept;
    constexpr ucs4_t *_data_ucs4() noexcept;
    constexpr ucs1_t const *_data_ucs1() const noexcept;
    constexpr ucs2_t const *_data_ucs2() const noexcept;
    constexpr ucs4_t const *_data_ucs4() const noexcept;

    constexpr std::size_t _get_capacity() const noexcept;// for current chartype
    constexpr std::size_t _get_size() const noexcept;
    constexpr i64 capacity() const noexcept;
    constexpr i64 size() const noexcept;
    constexpr cig_bool is_empty() const noexcept;
    constexpr i64 char_at(i64 idx) const noexcept;     // -1 if idx out of range
    constexpr i64 operator[] (i64 idx) const noexcept; // -1 if idx out of range

    constexpr i64 min() const noexcept; // -1 if empty
    constexpr i64 max() const noexcept; // -1 if empty
    constexpr std::pair<i64,i64> minmax() const noexcept; // both

    constexpr bool is_pure_ascii() const noexcept; // checks chartype,chars


    // Search functions

    // In the following search functions, pos and endpos define a
    // substring of *this wherein the string/single character must
    // be found, i.e. the first possible value returned is pos
    // and the last possible value returned is endpos - substr.size().
    // A single character is considered a substr of size 1,
    // and so are the single characters in the charlist argument.
    // If no place is found, the functions return -1.

    // Find a substring or a single-character string in *this
    i64 find(strval const &sub,i64 pos = 0) const noexcept;
    i64 find(strval const &sub,i64 pos,i64 endpos) const noexcept;
    i64 find(i64 singlechar, i64 pos = 0) const noexcept;
    i64 find(i64 singlechar, i64 pos,i64 endpos) const noexcept;

    // The same but start the search from the back
    i64 rfind(strval const &sub,i64 lastpos = i64::max_value) const noexcept;
    i64 rfind(strval const &sub,i64 lastpos,i64 endpos) const noexcept;
    i64 rfind(i64 singlechar, i64 lastpos = i64::max_value) const noexcept;
    i64 rfind(i64 singlechar, i64 lastpos,i64 endpos) const noexcept;

    // Find one of a list of char in *this
    i64 find_first_of(strval const &charlist,i64 pos = 0) const noexcept;
    i64 find_first_of(strval const &charlist,i64 pos,i64 endpos) const noexcept;
    i64 find_first_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_first_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;
    i64 find_last_of(strval const &charlist,i64 pos = 0) const noexcept;
    i64 find_last_of(strval const &charlist,i64 pos,i64 endpos) const noexcept;
    i64 find_last_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_last_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;
    i64 find_first_not_of(strval const &charlist,i64 pos = 0) const noexcept;
    i64 find_first_not_of(strval const &charlist,i64 pos,i64 endpos) const noexcept;
    i64 find_first_not_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_first_not_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;
    i64 find_last_not_of(strval const &charlist,i64 pos = 0) const noexcept;
    i64 find_last_not_of(strval const &charlist,i64 pos,i64 endpos) const noexcept;
    i64 find_last_not_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_last_not_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;

    // Find by ASCII character class mask, see cig_asciicharclass.h
    i64 find_first_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_first_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;
    i64 find_last_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_last_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;
    i64 find_first_not_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_first_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;
    i64 find_last_not_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_last_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;

    // Specializations
    bool contains(strval const &sub) const noexcept;
    bool starts_with(strval const &sub) const noexcept;
    bool ends_with(strval const &sub) const noexcept;

    // Slicing
    // A slice will always reduce the chartype, slice(0) is the reduced string
    strexpr slice(std::optional<i64> start, // Python like semantics
                  std::optional<i64> end = std::nullopt,
                  std::optional<i64> step = std::nullopt) const noexcept;
    constexpr str ascii_tolower() const; // convert ASCII chars to lowercase
    constexpr str ascii_toupper() const; // convert ASCII chars to uppercase

    exp_bytes encode_utf8() const;

protected:
    // constructor for empty string
    constexpr strval() noexcept : _l { 0, 0, 0 }
    {
        static_assert(sizeof(ucs4_t *) == sizeof(_l._data));
    };
    // auxiliary constructors for str
    constexpr void _construct_clear(_chartype chartype = _chartype_ascii);
    constexpr void _construct_by_move(strval &&s) noexcept;

    friend struct toolbox;
};


constexpr
strval::_chartype
strval::_chartype_by_max_codepoint(uint32_t maxcp) noexcept
{
    if (maxcp < 128)
	return strval::_chartype_ascii;
    else if (maxcp < 256)
	return strval::_chartype_ucs1;
    else if (maxcp < 0x10000)
	return strval::_chartype_ucs2;
    else
	return strval::_chartype_ucs4;
}


} // namespace detail


// String class
struct str : public detail::strval {

    // INTERNALS
private:
    static ucs1_t *_allocate_ucs1(std::size_t capacity);
    static ucs2_t *_allocate_ucs2(std::size_t capacity);
    static ucs4_t *_allocate_ucs4(std::size_t capacity);
    static void _delete_ucs1(ucs1_t *ptr,std::size_t capacity);
    static void _delete_ucs2(ucs2_t *ptr,std::size_t capacity);
    static void _delete_ucs4(ucs4_t *ptr,std::size_t capacity);
    constexpr void _delete_by_cap(std::size_t cap);
    constexpr void _destruct();
    constexpr void _reserve(std::size_t size,_chartype chartype,
                            bool duplicate = false);
    constexpr void _set_size(std::size_t size,bool zero_initialize = false);
    constexpr void _construct_clear(_chartype chartype = _chartype_ascii);
    template <typename Char>
    constexpr void _construct_from_ascii(Char const *data, std::size_t size);
    template <typename Char>
    constexpr void _construct_from_ucs1(Char const *data, std::size_t size);
    template <typename Char>
    constexpr void _construct_from_ucs2(Char const *data, std::size_t size);
    template <typename Char>
    constexpr void _construct_from_ucs4(Char const *data, std::size_t size);
    constexpr void _construct_a_copy(const strval &s);
    constexpr void _construct_by_move(str &&s) noexcept;
    constexpr void _construct_by_move(strval &&s) noexcept;

    // EXTERNALS
public:
    constexpr ~str();
    constexpr str() noexcept {};
    constexpr str(const strval&);
    constexpr str(strval&&) noexcept;
    constexpr str(const str&);
    constexpr str(str&&) noexcept;
    constexpr str &operator = (const strval &);
    constexpr str &operator = (const str &);
    constexpr str &operator = (str&&);
    inline str &operator += (const strval &);
    constexpr str &operator *= (i64 m);
    inline str(const detail::strexpr &);
    inline str &operator = (const detail::strexpr &);
    inline str &operator += (const detail::strexpr &);

    // auxiliary constructors for str
    template <typename Char>
    static constexpr str from_ascii(Char const *data, std::size_t size);
    template <typename Char>
    static constexpr str from_ascii_c_str(Char const *data);
    template <typename Char>
    static constexpr str from_ucs1(Char const *data, std::size_t size);
    template <typename Char>
    static constexpr str from_ucs2(Char const *data, std::size_t size);
    template <typename Char>
    static constexpr str from_ucs4(Char const *data, std::size_t size);
    template <typename Char>
    static exp_str from_utf8(Char const *data);
    template <typename Char>
    static exp_str from_utf8(Char const *data, std::size_t size);

    constexpr void reserve(i64,cig_bool duplicate = cig_false,
                           _chartype = _chartype_ascii);
    // after reserve + external fill, must set_size even on partial fill
    constexpr void set_size(i64,cig_bool zero_initialize = cig_true);

    friend struct detail::strval;
    friend struct detail::bytesval;
    friend struct toolbox;
};


/**********************************************************************/
/*                                                                    */
/*                             BYTES TYPE                             */
/*                                                                    */
/**********************************************************************/


namespace detail {


// Bytes value class
struct bytesval {


    /* This structure is inspired by the std::string type in llvm's libc++.

       The least significant bit of cap indicates:
       0 ... the array is stored immediately (a short array)
       1 ... the array uses pointers (a long array); allocated or constant

       If the array is long, the _long structure defines its content:
       cap            ... (_cap & ~_long_mask) is the allocated length; if zero,
                          the memory is not allocated but points to constant
                          and must not be changed or freed
       size           ... the length of the array in bytes
       data           ... the pointer to the allocated array

       If the array is short, the upper five bits of the least significant byte
       indicate the length of the array (shifted by 3 bits).
       On 64 bit systems, the maximum value is 22.
       The bytes are stored in _data[1..22] for little endian systems,
       _data[0..21] for big endian systems.
       On 32 bit systems, the maximum value is 10.
       The bytes are stored in _data[1..10] for little endian systems,
       _data[0..9] for big endian systems.

       The last byte is always followed by a '\0'.

       Bytes arrays are always allocated in multiples of 16 characters,
       therefore the lowest four bits are always zero. In those bits,
       the short/long indicator is stored.

       Note that an all-zero value of bytes is a valid empty array. */



    // TYPES AND CONSTANTS


    static constexpr const std::size_t type_alignment = alignof(std::size_t);
    static constexpr const std::size_t type_size = 3 * sizeof(std::size_t);

    typedef uint8_t  byte_t;    // Do not use std::byte which is not arithmetic

    enum { _long_mask = 0x01 };
    enum { _capbits_mask  = static_cast<uint8_t>(_long_mask) };
    static constexpr std::size_t _capacity_mask =
        ~static_cast<std::size_t>(0x0f);

#   if defined(CIG_BIG_ENDIAN)
    struct _long {
        std::size_t _data;
        std::size_t _size;
        std::size_t _cap;       // allocated_size | long_bit
    };
    enum {  _intlen = sizeof(struct _long) };
    enum {  _length_byte = _intlen - 1 };
    enum {  _intlen_net = _intlen - 2 };
    enum {  _int_offset = 0 };
#   elif defined(CIG_LITTLE_ENDIAN)
    struct _long {
        std::size_t _cap;
        std::size_t _size;
        std::size_t _data;
    };
    enum {  _intlen = sizeof(struct _long) };
    enum {  _length_byte = 0 };
    enum {  _intlen_net = _intlen - 2 };
    enum {  _int_offset = 1 };
#   else
#   error byte order not defined
#   endif

    // Avoid pointers and unions since they are not constexpr-safe
    struct _short {
        byte_t _data[_intlen];
    };


    // DATA


    _long _l;                   // use std::bit_cast to cast appropriately


    // FUNCTIONS


    static constexpr bool _is_pointered_by_cap(std::size_t cap) noexcept;
    static constexpr bool _is_allocated_by_cap(std::size_t cap) noexcept;
    static constexpr std::size_t _capacity_by_cap(std::size_t cap) noexcept;
    static constexpr std::size_t _cap_by_components(
        std::size_t capacity,bool is_pointered) noexcept;
    static constexpr std::size_t _size_to_allocate(
        std::size_t size, bool duplicate = false) noexcept;
    constexpr bool _is_pointered() const noexcept;
    constexpr bool _is_allocated() const noexcept;
    constexpr byte_t *_data() noexcept;
    constexpr byte_t const *_data() const noexcept;

    constexpr std::size_t _get_capacity() const noexcept;
    constexpr std::size_t _get_size() const noexcept;
    constexpr i64 capacity() const noexcept;
    constexpr i64 size() const noexcept;
    constexpr cig_bool is_empty() const noexcept;
    constexpr i64 byte_at(i64 idx) const noexcept;     // -1 if idx out of range
    constexpr i64 operator[] (i64 idx) const noexcept; // -1 if idx out of range

    constexpr i64 min() const noexcept; // -1 if empty
    constexpr i64 max() const noexcept; // -1 if empty
    constexpr std::pair<i64,i64> minmax() const noexcept; // both

    constexpr bool is_pure_ascii() const noexcept; // checks all bytes <= 0x7f


    // Search functions

    // In the following search functions, pos and endpos define a
    // subsequence of *this wherein the bytes sequence/single byte must
    // be found, i.e. the first possible value returned is pos
    // and the last possible value returned is endpos - subbytes.size().
    // A single byte is considered a subbytes of size 1,
    // and so are the single bytes in the bytelist argument.
    // If no place is found, the functions return -1.

    // Find a subsequence or a single-byte sequence in *this
    i64 find(bytesval const &sub,i64 pos = 0) const noexcept;
    i64 find(bytesval const &sub,i64 pos,i64 endpos) const noexcept;
    i64 find(i64 singlechar, i64 pos = 0) const noexcept;
    i64 find(i64 singlechar, i64 pos,i64 endpos) const noexcept;

    // The same but start the search from the back
    i64 rfind(bytesval const &sub,i64 lastpos = i64::max_value) const noexcept;
    i64 rfind(bytesval const &sub,i64 lastpos,i64 endpos) const noexcept;
    i64 rfind(i64 singlechar, i64 lastpos = i64::max_value) const noexcept;
    i64 rfind(i64 singlechar, i64 lastpos,i64 endpos) const noexcept;

    // Find one of a list of char in *this
    i64 find_first_of(bytesval const &bytelist,i64 pos = 0) const noexcept;
    i64 find_first_of(bytesval const &bytelist,i64 pos,i64 endpos) const noexcept;
    i64 find_first_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_first_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;
    i64 find_last_of(bytesval const &bytelist,i64 pos = 0) const noexcept;
    i64 find_last_of(bytesval const &bytelist,i64 pos,i64 endpos) const noexcept;
    i64 find_last_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_last_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;
    i64 find_first_not_of(bytesval const &bytelist,i64 pos = 0) const noexcept;
    i64 find_first_not_of(bytesval const &bytelist,i64 pos,i64 endpos) const noexcept;
    i64 find_first_not_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_first_not_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;
    i64 find_last_not_of(bytesval const &bytelist,i64 pos = 0) const noexcept;
    i64 find_last_not_of(bytesval const &bytelist,i64 pos,i64 endpos) const noexcept;
    i64 find_last_not_of(i64 singlechar,i64 pos = 0) const noexcept;
    i64 find_last_not_of(i64 singlechar,i64 pos,i64 endpos) const noexcept;

    // Find by ASCII character class mask, see cig_asciicharclass.h
    i64 find_first_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_first_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;
    i64 find_last_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_last_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;
    i64 find_first_not_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_first_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;
    i64 find_last_not_in_acc(i64 accmask,i64 pos = 0) const noexcept;
    i64 find_last_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept;

    // Specializations
    bool contains(bytesval const &sub) const noexcept;
    bool starts_with(bytesval const &sub) const noexcept;
    bool ends_with(bytesval const &sub) const noexcept;

    // Slicing
    bytesexpr slice(std::optional<i64> start, // Python like semantics
                    std::optional<i64> end = std::nullopt,
                    std::optional<i64> step = std::nullopt) const noexcept;
    constexpr bytes ascii_tolower() const; // convert ASCII bytes to lowercase
    constexpr bytes ascii_toupper() const; // convert ASCII bytes to uppercase

    cig_bool is_pure_utf8() const noexcept; // utf8, no overlong encodings
    exp_str decode_utf8() const;

protected:
    // constructor for empty bytes
    constexpr bytesval() noexcept : _l { 0, 0, 0 }
    {
        static_assert(sizeof(byte_t *) == sizeof(_l._data));
    };
    // auxiliary constructors for bytes
    constexpr void _construct_clear();
    constexpr void _construct_by_move(bytesval &&s) noexcept;

    friend struct toolbox;
};


} // namespace detail


// Bytes class
struct bytes : public detail::bytesval {

    // INTERNALS
private:
    static byte_t *_allocate(std::size_t capacity);
    static void _delete(byte_t *ptr,std::size_t capacity);
    constexpr void _delete_by_cap(std::size_t cap);
    constexpr void _destruct();
    constexpr void _reserve(std::size_t size, bool duplicate = false);
    constexpr void _set_size(std::size_t size, bool zero_initialize = false);
    constexpr void _construct_clear();
    template <typename Byte>
    constexpr void _construct_from_bytes(Byte const *data, std::size_t size);
    constexpr void _construct_a_copy(const bytesval &s);
    constexpr void _construct_by_move(bytes &&s) noexcept;
    constexpr void _construct_by_move(bytesval &&s) noexcept;

    // EXTERNALS
public:
    constexpr ~bytes();
    constexpr bytes() noexcept {};
    constexpr bytes(const bytesval&);
    constexpr bytes(bytesval&&) noexcept;
    constexpr bytes(const bytes&);
    constexpr bytes(bytes&&) noexcept;
    constexpr bytes &operator = (const bytesval &);
    constexpr bytes &operator = (const bytes &);
    constexpr bytes &operator = (bytes&&);
    inline bytes &operator += (const bytesval &);
    constexpr bytes &operator *= (i64 m);
    inline bytes(const detail::bytesexpr &);
    inline bytes &operator = (const detail::bytesexpr &);
    inline bytes &operator += (const detail::bytesexpr &);

    // auxiliary constructors for bytes
    template <typename Byte>
    static constexpr bytes from_c_bytes(Byte const *data, std::size_t size);

    constexpr void reserve(i64,cig_bool duplicate = cig_false);
    // after reserve + external fill, must set_size even on partial fill
    constexpr void set_size(i64,cig_bool zero_initialize = cig_true);

    friend struct detail::bytesval;
    friend struct detail::strval;
    friend struct toolbox;
};


} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_DATATYPES_H_INCLUDED) */
