/* cig_assert.cpp
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Assert failure function.
 */
#ifndef CIG_ASSERT_H_INCLUDED
#define CIG_ASSERT_H_INCLUDED

#include "cig_assert.h"
#include "cig_startup_filepointer.h"

#include <cstdio>               // std::fprintf,std::fflush
#include <cstdlib>              // std::abort

namespace cig {

[[noreturn]] void cig_assert_fail(char const *assertion,char const *file,
                                  int line,char const *function)
{
    // Flush all log files (allocation, recorder)
    cig::startup::flush_all_filepointer_wrappers();
    cig::startup::close_all_filepointer_wrappers();
    std::fflush(NULL);
    std::fprintf(stderr,"%s:%u: %s%sAssertion `%s' failed.\n",
		 file,line,
		 function ? function : "",
		 function ? ": " : "",
		 assertion);
    std::fflush(stderr);
    std::abort();
}

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_ASSERT_H_INCLUDED) */
