/* cig_exception.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Exception type with internal err value.
 */
#ifndef CIG_EXCEPTION_H_INCLUDED
#define CIG_EXCEPTION_H_INCLUDED


#include "cig_err.h"            // cig::err, CIG_HAVE_EXCEPTIONS


#ifdef CIG_HAVE_EXCEPTIONS
namespace cig {


// The base class for more specific Error classes
class Exception {
public:
    err e;
    cig::source_location throwloc;

    // Constructors
    constexpr Exception(        // from void
        cig::source_location sloc = std::source_location::current()) :
        e(0), throwloc(sloc) { }
    constexpr Exception(        // from cig::err::errorcode enum
        cig::err::errorcode error,
        cig::source_location sloc = std::source_location::current()) :
        e(error), throwloc(sloc) { }
    constexpr Exception(        // from int
        int error,
        cig::source_location sloc = std::source_location::current()) :
        e(error), throwloc(sloc) { }
    constexpr Exception(        // from cig::err
        cig::err error,
        cig::source_location sloc = std::source_location::current()) :
        e(error), throwloc(sloc) { }
    constexpr Exception(        // from Exception
        Exception const &excpt,
        cig::source_location sloc = std::source_location::current()) :
        e(excpt.e), throwloc(sloc) { }

    template<typename T>
    constexpr Exception(        // from a failed std::expected
        std::expected<T,cig::err> && exp,
        cig::source_location sloc = std::source_location::current()) :
        e(exp.error()), throwloc(sloc) { }

    template<typename T>
    constexpr Exception(        // from a failed std::expected
        std::expected<T,cig::err> const & exp,
        cig::source_location sloc = std::source_location::current()) :
        e(exp.error()), throwloc(sloc) { }

    template<typename T>
    static constexpr void check( // from a std::expected only if it failed
        std::expected<T,cig::err> && exp,
        cig::source_location sloc = std::source_location::current())
    {
        if (!exp.has_value())
            throw Exception(exp,sloc);
    }

    template<typename T>
    static constexpr void check( // from a std::expected only if it failed
        std::expected<T,cig::err> const & exp,
        cig::source_location sloc = std::source_location::current())
    {
        if (!exp.has_value())
            throw Exception(exp,sloc);
    }
};


} // namespace cig

#endif  // CIG_HAVE_EXCEPTIONS



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_EXCEPTION_H_INCLUDED) */
