/* cig_sysdep_file.h
 *
 * Copyright 2023 Claus Fischer
 *
 * System dependent low-level file operations.
 */
#ifndef CIG_SYSDEP_FILE_H_INCLUDED
#define CIG_SYSDEP_FILE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif


#define CIG_FD_INVALID -1       /* invalid file descriptor */


char const *			/* static error string or 0 on success */
cig_sys_file_selftest(void);

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_open(
    char const *filename,       /* [i] filename */
    int read_only,              /* [i] open for read only */
    int *filedes                /* [o] file descriptor, or CIG_FD_INVALID */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_close(
    int filedes                 /* [i] file descriptor */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_syncdata(
    int filedes                 /* [i] file descriptor */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_syncdata_name(
    char const *filename        /* [i] filename */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_getlen(
    int filedes,                /* [i] file descriptor */
    int64_t *len                /* [o] length, or -1 */
    );    

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_getpos(
    int filedes,                /* [i] file descriptor */
    int64_t *pos                /* [o] position, or -1 */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_setpos(
    int filedes,                /* [i] file descriptor */
    int64_t pos                 /* [i] position */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_truncate(
    int filedes                 /* [i] file descriptor */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_write(
    int filedes,                /* [i] file descriptor */
    void const *buf,            /* [i] buffer with data to write */
    uint64_t buflen             /* [i] length of data in bytes */
    );

int                             /* 0 on success, cig_errno on failure */
cig_sys_file_read(
    int filedes,                /* [i] file descriptor */
    void *buf,                  /* [o] buffer with data to read */
    uint64_t buflen             /* [i] length of data to read in bytes */
    );


#ifdef __cplusplus
} /* extern "C" */
#endif

/* ** EMACS **
 * Local Variables:
 * mode: C
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_SYSDEP_FILE_H_INCLUDED) */
