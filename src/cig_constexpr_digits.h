/* cig_constexpr_digits.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Compile-time evaluation of numbers.
 */
#ifndef CIG_CONSTEXPR_DIGITS_H_INCLUDED
#define CIG_CONSTEXPR_DIGITS_H_INCLUDED

#include <cstddef>              // std::size_t,std::ptrdiff_t
#include <cstdint>              // uint8_t ...
#include <array>		// std::array


namespace cig {


namespace detail {


template<typename Char>
constexpr bool isdigit(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7' &&
                  '7' + 1 == '8' &&
                  '8' + 1 == '9')
    {
        return (digit >= '0' && digit <= '9');
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        switch (c)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9': return true;
        default:  return false;
        }
    }
}


template<typename Char>
constexpr bool isodigit(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7')
    {
        return (digit >= '0' && digit <= '7');
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        switch (c)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7': return true;
        default:  return false;
        }
    }
}


template<typename Char>
constexpr bool isxdigit(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7' &&
                  '7' + 1 == '8' &&
                  '8' + 1 == '9' &&
                  'a' + 1 == 'b' &&
                  'b' + 1 == 'c' &&
                  'c' + 1 == 'd' &&
                  'd' + 1 == 'e' &&
                  'e' + 1 == 'f' &&
                  'A' + 1 == 'B' &&
                  'B' + 1 == 'C' &&
                  'C' + 1 == 'D' &&
                  'D' + 1 == 'E' &&
                  'E' + 1 == 'F')
    {
        return ((digit >= '0' && digit <= '9') ||
                (digit >= 'a' && digit <= 'f') ||
                (digit >= 'A' && digit <= 'F'));
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        switch (c)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'A':
        case 'a':
        case 'B':
        case 'b':
        case 'C':
        case 'c':
        case 'D':
        case 'd':
        case 'E':
        case 'e':
        case 'F':
        case 'f': return true;
        default:  return false;
        }
    }
}


template<typename Char>
constexpr bool isalpha(Char digit)
{
    if constexpr ('a' + 1 == 'b' &&
                  'b' + 1 == 'c' &&
                  'c' + 1 == 'd' &&
                  'd' + 1 == 'e' &&
                  'e' + 1 == 'f' &&
                  'f' + 1 == 'g' &&
                  'g' + 1 == 'h' &&
                  'h' + 1 == 'i' &&
                  'i' + 1 == 'j' &&
                  'j' + 1 == 'k' &&
                  'k' + 1 == 'l' &&
                  'l' + 1 == 'm' &&
                  'm' + 1 == 'n' &&
                  'n' + 1 == 'o' &&
                  'o' + 1 == 'p' &&
                  'p' + 1 == 'q' &&
                  'q' + 1 == 'r' &&
                  'r' + 1 == 's' &&
                  's' + 1 == 't' &&
                  't' + 1 == 'u' &&
                  'u' + 1 == 'v' &&
                  'v' + 1 == 'w' &&
                  'w' + 1 == 'x' &&
                  'x' + 1 == 'y' &&
                  'y' + 1 == 'z' &&
                  'A' + 1 == 'B' &&
                  'B' + 1 == 'C' &&
                  'C' + 1 == 'D' &&
                  'D' + 1 == 'E' &&
                  'E' + 1 == 'F' &&
                  'F' + 1 == 'G' &&
                  'G' + 1 == 'H' &&
                  'H' + 1 == 'I' &&
                  'I' + 1 == 'J' &&
                  'J' + 1 == 'K' &&
                  'K' + 1 == 'L' &&
                  'L' + 1 == 'M' &&
                  'M' + 1 == 'N' &&
                  'N' + 1 == 'O' &&
                  'O' + 1 == 'P' &&
                  'P' + 1 == 'Q' &&
                  'Q' + 1 == 'R' &&
                  'R' + 1 == 'S' &&
                  'S' + 1 == 'T' &&
                  'T' + 1 == 'U' &&
                  'U' + 1 == 'V' &&
                  'V' + 1 == 'W' &&
                  'W' + 1 == 'X' &&
                  'X' + 1 == 'Y' &&
                  'Y' + 1 == 'Z')
    {
        return ((digit >= 'a' && digit <= 'z') ||
                (digit >= 'A' && digit <= 'Z'));
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        switch (c)
        {
        case 'A':
        case 'a':
        case 'B':
        case 'b':
        case 'C':
        case 'c':
        case 'D':
        case 'd':
        case 'E':
        case 'e':
        case 'F':
        case 'f':
        case 'G':
        case 'g':
        case 'H':
        case 'h':
        case 'I':
        case 'i':
        case 'J':
        case 'j':
        case 'K':
        case 'k':
        case 'L':
        case 'l':
        case 'M':
        case 'm':
        case 'N':
        case 'n':
        case 'O':
        case 'o':
        case 'P':
        case 'p':
        case 'Q':
        case 'q':
        case 'R':
        case 'r':
        case 'S':
        case 's':
        case 'T':
        case 't':
        case 'U':
        case 'u':
        case 'V':
        case 'v':
        case 'W':
        case 'w':
        case 'X':
        case 'x':
        case 'Y':
        case 'y':
        case 'Z':
        case 'z': return true;
        default:  return false;
        }
    }
}


template<typename Char>
constexpr bool isalnum(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7' &&
                  '7' + 1 == '8' &&
                  '8' + 1 == '9' &&
                  'a' + 1 == 'b' &&
                  'b' + 1 == 'c' &&
                  'c' + 1 == 'd' &&
                  'd' + 1 == 'e' &&
                  'e' + 1 == 'f' &&
                  'f' + 1 == 'g' &&
                  'g' + 1 == 'h' &&
                  'h' + 1 == 'i' &&
                  'i' + 1 == 'j' &&
                  'j' + 1 == 'k' &&
                  'k' + 1 == 'l' &&
                  'l' + 1 == 'm' &&
                  'm' + 1 == 'n' &&
                  'n' + 1 == 'o' &&
                  'o' + 1 == 'p' &&
                  'p' + 1 == 'q' &&
                  'q' + 1 == 'r' &&
                  'r' + 1 == 's' &&
                  's' + 1 == 't' &&
                  't' + 1 == 'u' &&
                  'u' + 1 == 'v' &&
                  'v' + 1 == 'w' &&
                  'w' + 1 == 'x' &&
                  'x' + 1 == 'y' &&
                  'y' + 1 == 'z' &&
                  'A' + 1 == 'B' &&
                  'B' + 1 == 'C' &&
                  'C' + 1 == 'D' &&
                  'D' + 1 == 'E' &&
                  'E' + 1 == 'F' &&
                  'F' + 1 == 'G' &&
                  'G' + 1 == 'H' &&
                  'H' + 1 == 'I' &&
                  'I' + 1 == 'J' &&
                  'J' + 1 == 'K' &&
                  'K' + 1 == 'L' &&
                  'L' + 1 == 'M' &&
                  'M' + 1 == 'N' &&
                  'N' + 1 == 'O' &&
                  'O' + 1 == 'P' &&
                  'P' + 1 == 'Q' &&
                  'Q' + 1 == 'R' &&
                  'R' + 1 == 'S' &&
                  'S' + 1 == 'T' &&
                  'T' + 1 == 'U' &&
                  'U' + 1 == 'V' &&
                  'V' + 1 == 'W' &&
                  'W' + 1 == 'X' &&
                  'X' + 1 == 'Y' &&
                  'Y' + 1 == 'Z')
    {
        return ((digit >= '0' && digit <= '9') ||
                (digit >= 'a' && digit <= 'z') ||
                (digit >= 'A' && digit <= 'Z'));
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        switch (c)
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'A':
        case 'a':
        case 'B':
        case 'b':
        case 'C':
        case 'c':
        case 'D':
        case 'd':
        case 'E':
        case 'e':
        case 'F':
        case 'f':
        case 'G':
        case 'g':
        case 'H':
        case 'h':
        case 'I':
        case 'i':
        case 'J':
        case 'j':
        case 'K':
        case 'k':
        case 'L':
        case 'l':
        case 'M':
        case 'm':
        case 'N':
        case 'n':
        case 'O':
        case 'o':
        case 'P':
        case 'p':
        case 'Q':
        case 'q':
        case 'R':
        case 'r':
        case 'S':
        case 's':
        case 'T':
        case 't':
        case 'U':
        case 'u':
        case 'V':
        case 'v':
        case 'W':
        case 'w':
        case 'X':
        case 'x':
        case 'Y':
        case 'y':
        case 'Z':
        case 'z': return true;
        default:  return false;
        }
    }
}


template<typename Char>
constexpr int			// value 0..9, or -1
digitval(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7' &&
                  '7' + 1 == '8' &&
                  '8' + 1 == '9')
    {
        return (digit >= '0' && digit <= '9'  ?  static_cast<int>(digit) - '0' :
                -1);
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        int v;

        switch (c)
        {
        case '0': v =  0; break;
        case '1': v =  1; break;
        case '2': v =  2; break;
        case '3': v =  3; break;
        case '4': v =  4; break;
        case '5': v =  5; break;
        case '6': v =  6; break;
        case '7': v =  7; break;
        case '8': v =  8; break;
        case '9': v =  9; break;
        default:
            v = -1;
            break;
        }
        return v;
    }
}


template<typename Char>
constexpr int			// value 0..7, or -1
odigitval(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7')
    {
        return (digit >= '0' && digit <= '7'  ?  static_cast<int>(digit) - '0' :
                -1);
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        int v;

        switch (c)
        {
        case '0': v =  0; break;
        case '1': v =  1; break;
        case '2': v =  2; break;
        case '3': v =  3; break;
        case '4': v =  4; break;
        case '5': v =  5; break;
        case '6': v =  6; break;
        case '7': v =  7; break;
        default:
            v = -1;
            break;
        }
        return v;
    }
}


template<typename Char>
constexpr int			// value 0..35, or -1
xdigitval(Char digit)
{
    if constexpr ('0' + 1 == '1' &&
                  '1' + 1 == '2' &&
                  '2' + 1 == '3' &&
                  '3' + 1 == '4' &&
                  '4' + 1 == '5' &&
                  '5' + 1 == '6' &&
                  '6' + 1 == '7' &&
                  '7' + 1 == '8' &&
                  '8' + 1 == '9' &&
                  'a' + 1 == 'b' &&
                  'b' + 1 == 'c' &&
                  'c' + 1 == 'd' &&
                  'd' + 1 == 'e' &&
                  'e' + 1 == 'f' &&
                  'f' + 1 == 'g' &&
                  'g' + 1 == 'h' &&
                  'h' + 1 == 'i' &&
                  'i' + 1 == 'j' &&
                  'j' + 1 == 'k' &&
                  'k' + 1 == 'l' &&
                  'l' + 1 == 'm' &&
                  'm' + 1 == 'n' &&
                  'n' + 1 == 'o' &&
                  'o' + 1 == 'p' &&
                  'p' + 1 == 'q' &&
                  'q' + 1 == 'r' &&
                  'r' + 1 == 's' &&
                  's' + 1 == 't' &&
                  't' + 1 == 'u' &&
                  'u' + 1 == 'v' &&
                  'v' + 1 == 'w' &&
                  'w' + 1 == 'x' &&
                  'x' + 1 == 'y' &&
                  'y' + 1 == 'z' &&
                  'A' + 1 == 'B' &&
                  'B' + 1 == 'C' &&
                  'C' + 1 == 'D' &&
                  'D' + 1 == 'E' &&
                  'E' + 1 == 'F' &&
                  'F' + 1 == 'G' &&
                  'G' + 1 == 'H' &&
                  'H' + 1 == 'I' &&
                  'I' + 1 == 'J' &&
                  'J' + 1 == 'K' &&
                  'K' + 1 == 'L' &&
                  'L' + 1 == 'M' &&
                  'M' + 1 == 'N' &&
                  'N' + 1 == 'O' &&
                  'O' + 1 == 'P' &&
                  'P' + 1 == 'Q' &&
                  'Q' + 1 == 'R' &&
                  'R' + 1 == 'S' &&
                  'S' + 1 == 'T' &&
                  'T' + 1 == 'U' &&
                  'U' + 1 == 'V' &&
                  'V' + 1 == 'W' &&
                  'W' + 1 == 'X' &&
                  'X' + 1 == 'Y' &&
                  'Y' + 1 == 'Z')
    {
        return (digit >= '0' && digit <= '9'  ?  static_cast<int>(digit) - '0' :
                digit >= 'a' && digit <= 'z'  ?  (static_cast<int>(digit) -
                                                  ('a' - 10)) :
                digit >= 'A' && digit <= 'Z'  ?  (static_cast<int>(digit) -
                                                  ('A' - 10)) :
                -1);
    }
    else
    {
        if constexpr (sizeof(Char) > 1)
        {
            if (digit <= 0 || digit > 0xff)
                return false;
        }
        unsigned char c = static_cast<unsigned char>(digit);

        int v;

        switch (c)
        {
        case '0': v =  0; break;
        case '1': v =  1; break;
        case '2': v =  2; break;
        case '3': v =  3; break;
        case '4': v =  4; break;
        case '5': v =  5; break;
        case '6': v =  6; break;
        case '7': v =  7; break;
        case '8': v =  8; break;
        case '9': v =  9; break;
        case 'A':
        case 'a': v = 10; break;
        case 'B':
        case 'b': v = 11; break;
        case 'C':
        case 'c': v = 12; break;
        case 'D':
        case 'd': v = 13; break;
        case 'E':
        case 'e': v = 14; break;
        case 'F':
        case 'f': v = 15; break;
        case 'G':
        case 'g': v = 16; break;
        case 'H':
        case 'h': v = 17; break;
        case 'I':
        case 'i': v = 18; break;
        case 'J':
        case 'j': v = 19; break;
        case 'K':
        case 'k': v = 20; break;
        case 'L':
        case 'l': v = 21; break;
        case 'M':
        case 'm': v = 22; break;
        case 'N':
        case 'n': v = 23; break;
        case 'O':
        case 'o': v = 24; break;
        case 'P':
        case 'p': v = 25; break;
        case 'Q':
        case 'q': v = 26; break;
        case 'R':
        case 'r': v = 27; break;
        case 'S':
        case 's': v = 28; break;
        case 'T':
        case 't': v = 29; break;
        case 'U':
        case 'u': v = 30; break;
        case 'V':
        case 'v': v = 31; break;
        case 'W':
        case 'w': v = 32; break;
        case 'X':
        case 'x': v = 33; break;
        case 'Y':
        case 'y': v = 34; break;
        case 'Z':
        case 'z': v = 35; break;
        default:
            v = -1;
            break;
        }
        return v;
    }
}


} // namespace detail


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_CONSTEXPR_DIGITS_H_INCLUDED) */
