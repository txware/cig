/* cig_vector.h
 *
 * Copyright 2024 Claus Fischer
 *
 * Vector type for cig.
 */
#ifndef CIG_VECTOR_H_INCLUDED
#define CIG_VECTOR_H_INCLUDED

#include "cig_allocation.h"

#include <cstddef>


namespace cig {


// Note that all CIG types, even those constructed by the interpreter,
// can be trivially relocated in the sense of trivial relocatability
// as defined in P1144: A move constructor followed by a destructor
// of the original object can be replaced by a memcpy().



// Both the interpreted code and the compiled template
// need to use the same data layout.

template<class Allocator=cig::baseallocator<"vector"> >
class vector_base {

protected:

    static constexpr const std::size_t type_alignment = alignof(vector_base);
    static constexpr const std::size_t type_size = sizeof(vector_base);

    // This class is a bit strange since its information is incomplete.
    // The basic object type of the vector elements is not defined.
    // That makes the definition of the pointer and the size useless
    // unless the information about the basic object type is somehow
    // added. This is either done by the vector template below or by
    // runtime information of the interpreter about the object type.

    void *value_bytes;
    std::size_t v_size;         // Number of objects, not bytes
    std::size_t v_capacity;     // Number of objects, not bytes

    void base_construct() noexcept {
        value_bytes = 0;
        v_capacity = 0;
        v_size = 0;
    }
    void base_construct_reserve(std::size_t element_size, std::size_t cap)
    {
        if (element_size < 1)
        {
            base_construct();
            return;
        }
        void *new_bytes =
            Allocator::template alloc<unsigned char>(element_size * cap);
        value_bytes = new_bytes;
        v_capacity = cap;
        v_size = 0;
    }
    void base_reserve(std::size_t element_size, std::size_t new_cap)
    {
        if (v_capacity >= new_cap)
            return;
        if (element_size < 1)
            return;
        // If the vector already had elements,
        // enlarge the capacity to a power of 2
        if (v_capacity)
        {
            std::size_t c = 4;
            while (c < new_cap)
                c *= 2;
            new_cap = c;
        }
        void *new_bytes =
            Allocator::template alloc<unsigned char>(element_size * new_cap);
        if (v_size)
        {
            cig_assert(value_bytes);
            cig_assert(v_size <= v_capacity);
            std::memcpy(new_bytes,value_bytes,v_size * element_size);
        }
        if (value_bytes)
        {
            cig_assert(v_capacity > 0);
            Allocator::template free<unsigned char>(
                static_cast<unsigned char *>(value_bytes),
                v_capacity * element_size);
        }
        value_bytes = new_bytes;
        v_capacity = new_cap;
    }
    void base_free(std::size_t element_size)
    {
        if (value_bytes)
        {
            cig_assert(v_capacity > 0);
            Allocator::template free<unsigned char>(
                static_cast<unsigned char *>(value_bytes),
                v_capacity * element_size);
        }
        base_construct();
    }
};



template<class T>
class vector : public vector_base< cig::baseallocator<"vector"> >
{
private:
    T *values() { return static_cast<T*>(this->value_bytes); }
    const T *values() const { return static_cast<T*>(this->value_bytes); }

    // Auxiliary functions
    constexpr void _clear() noexcept {
        value_type * CIG_RESTRICT v = values();

        for (std::size_t i = this->v_size; i > 0; i--)
            v[i-1].~value_type();
        this->v_size = 0;
    }
    constexpr void _free() noexcept {
        this->base_free(sizeof(value_type));
    }
    constexpr void _destruct() {
        _clear();
        _free();
    }
    constexpr void _construct_clear() noexcept {
        this->base_construct();
    }
    constexpr void _construct_by_move(vector &&v) noexcept {
        this->value_bytes = v.value_bytes;
        this->v_size = v.v_size;
        this->v_capacity = v.v_capacity;
        v._construct_clear();
    }
    constexpr void _construct_a_copy(const vector &v) {
        std::size_t sz = v.v_size;
        base_construct_reserve(sizeof(value_type),sz);
        value_type const * CIG_RESTRICT src = v.values();
        value_type * CIG_RESTRICT dst = values();
        
        for (std::size_t i = 0; i < sz; i++)
            new(&dst[i]) value_type(src[i]);
    }

public:
    typedef T value_type;
    typedef cig::baseallocator<"vector"> allocator_type;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef T* pointer;
    typedef const T* const_pointer;

    constexpr i64 i64_size() const noexcept {
        std::size_t sz = this->v_size;
        return static_cast<int64_t>(sz);
    }

    constexpr std::size_t size() const noexcept {
        std::size_t sz = this->v_size;
        return sz;
    }

    constexpr void reserve(size_type new_cap) {
        size_type cap = this->v_capacity;
        if (cap >= new_cap)
            return;
        this->base_reserve(sizeof(value_type),new_cap);
    }
    constexpr void reserve(i64 new_cap) {
        if (new_cap > 0)
            reserve(static_cast<size_type>(new_cap._v));
    }

    void resize(size_type count) {
        size_type sz = this->v_size;
        size_type cap = this->v_capacity;
        if (sz < count)
        {
            reserve(count);

            value_type * CIG_RESTRICT v = values();

            for (std::size_t i = sz; i < count; i++)
                new(&v[i]) value_type();
            this->v_size = count;
        }
        else if (sz > count)
        {
            value_type * CIG_RESTRICT v = values();
            for (std::size_t i = sz; i > count; i--)
                v[i-1].~value_type();
            this->v_size = count;
        }
    }
    void resize(i64 count)
    {
        if (count > 0)
            resize(static_cast<size_type>(count._v));
        else
            resize(0);
    }

    void resize(size_type count, const value_type &value) {
        size_type sz = this->v_size;
        if (sz < count)
        {
            reserve(count);

            value_type * CIG_RESTRICT v = values();

            for (std::size_t i = sz; i < count; i++)
                new(&v[i]) value_type(value);
            this->v_size = count;
        }
        else if (sz > count)
        {
            value_type * CIG_RESTRICT v = values();
            for (std::size_t i = sz; i > count; i--)
                v[i-1].~value_type();
            this->v_size = count;
        }
    }
    void resize(i64 count, const value_type &value)
    {
        if (count > 0)
            resize(static_cast<size_type>(count._v),value);
        else
            resize(0,value);
    }


    void clear() noexcept { _clear(); }

    void push_back(const T &value) {
        size_type sz = this->v_size;
        size_type cap = this->v_capacity;
        if (sz >= cap)
            reserve(sz+1);

        value_type * CIG_RESTRICT v = values();
        new(&v[sz]) value_type(value);
        this->v_size = sz+1;
    }

    void push_back(T &&value) {
        size_type sz = this->v_size;
        size_type cap = this->v_capacity;
        if (sz >= cap)
            reserve(sz+1);

        value_type * CIG_RESTRICT v = values();
        new(&v[sz]) value_type(std::move(value));
        this->v_size = sz+1;
    }

    reference operator[] (size_type pos)
    {
        return values()[pos];
    }
    reference operator[] (i64 pos)
    {
        return values()[pos >= 0 ? pos._v : 0];
    }
    const_reference operator[] (size_type pos) const
    {
        return values()[pos];
    }
    const_reference operator[] (i64 pos) const
    {
        return values()[pos >= 0 ? pos._v : 0];
    }

    vector() { _construct_clear(); }
    ~vector() { _destruct(); }

    vector(const vector &v) { _construct_a_copy(v); }
    vector(vector &&v) { _construct_by_move(std::move(v)); }
    vector & operator = (const vector &v) {
        if (this != &v)
        {
            _destruct();
            _construct_a_copy(v);
        }
        return *this;
    }
    vector & operator = (vector && v) {
        if (this != &v)
        {
            _destruct();
            _construct_by_move();
        }
        return *this;
    }
};


} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_VECTOR_H_INCLUDED) */
