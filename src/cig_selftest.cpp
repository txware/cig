/* cig_selftest.cpp
 *
 * Copyright 2022-2024 Claus Fischer
 *
 * Selftest function for cig.
 */
#include "cig_selftest.h"

#include "cig_datatypes.h"
#include "cig_int_inline.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_literals.h"
#include "cig_constexpr_itoa.h"
#include "cig_sysdep.h"
#include "cig_interpreter_ops_and_types.h"

// Macros for file and line number
#define TOSTR(x) #x
#define TOLINENUM(x) TOSTR(x)
#define E(s)                                                            \
    do {                                                                \
        return (__FILE__  ":" TOLINENUM(__LINE__) ": error: " s);       \
    } while (0)


namespace cig {

// Template to check whether an expression is constexpr
// via Mark de Wever/Arthur O'Dwyer
// does not work on clang < 15
template<class F, int = (F{}(), 0)>
constexpr bool is_constexpr_friendly(F) { return true; }
constexpr bool is_constexpr_friendly(...) { return false; }
#if defined(__clang__) && defined(__clang_major__) && __clang_major__ < 15
#  define ASSERT_EXPR_IS_CONSTEXPR(expr)
#  define ASSERT_EXPR_IS_NOT_CONSTEXPR(expr)
#else
#  define ASSERT_EXPR_IS_CONSTEXPR(expr) \
    static_assert(is_constexpr_friendly([] { expr; }))
#  define ASSERT_EXPR_IS_NOT_CONSTEXPR(expr) \
    static_assert(!is_constexpr_friendly([] { expr; }))
#endif


char const * selftest_basic_types() // static error string or 0 on success
{
    static_assert(sizeof(cig_bool) == cig_bool::type_size);
    static_assert(sizeof(u8)  == u8::type_size);
    static_assert(sizeof(u16) == u16::type_size);
    static_assert(sizeof(u32) == u32::type_size);
    static_assert(sizeof(u64) == u64::type_size);
    static_assert(sizeof(i8)  == i8::type_size);
    static_assert(sizeof(i16) == i16::type_size);
    static_assert(sizeof(i32) == i32::type_size);
    static_assert(sizeof(i64) == i64::type_size);
    static_assert(sizeof(str) == str::type_size);
    static_assert(sizeof(bytes) == bytes::type_size);

    static_assert(alignof(cig_bool) == cig_bool::type_alignment);
    static_assert(alignof(u8)  == u8::type_alignment);
    static_assert(alignof(u16) == u16::type_alignment);
    static_assert(alignof(u32) == u32::type_alignment);
    static_assert(alignof(u64) == u64::type_alignment);
    static_assert(alignof(i8)  == i8::type_alignment);
    static_assert(alignof(i16) == i16::type_alignment);
    static_assert(alignof(i32) == i32::type_alignment);
    static_assert(alignof(i64) == i64::type_alignment);
    static_assert(alignof(str) == str::type_alignment);
    static_assert(alignof(bytes) == bytes::type_alignment);

    return 0;
}


#if defined(__clang__) && __clang_major__ < 17
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-value"
#endif


char const * selftest_errorstring() // static error string or 0 on success
{
    using namespace detail;
    char const *selftest_error;

    // Selftest of sysdep layer
    selftest_error = cig_sys_selftest();
    if (selftest_error)
        return selftest_error;

    // Test sizes of pointers and structures in strval
    static_assert(sizeof(strval::ucs1_t *) == sizeof(std::size_t));
    static_assert(sizeof(strval::ucs2_t *) == sizeof(std::size_t));
    static_assert(sizeof(strval::ucs4_t *) == sizeof(std::size_t));
    static_assert(sizeof(strval::_l._data) == sizeof(std::size_t));
    static_assert(sizeof(strval::_short_u8) == sizeof(strval::_long));
    static_assert(sizeof(strval::_short_u16) == sizeof(strval::_long));
    static_assert(sizeof(strval::_short_u32) == sizeof(strval::_long));

    // Test size and alignment of basic types
    selftest_error = selftest_basic_types();
    if (selftest_error)
        return selftest_error;

    // Selftest of interpreter
    selftest_error = cig_interpreter_selftest();
    if (selftest_error)
        return selftest_error;

    // Test addition of strlitholders
    CIG_CONSTEXPR auto strlitholder_add = "abc"_str + "def"_str;
#if CIG_HAVE_CONSTEXPR
    static_assert(strlitholder_add.buf[4] == 'e');
#else
    if (strlitholder_add.buf[4] != 'e')
        E("bad strlitholder_add letter e");
#endif

    // Test constexpr of short string literals
    CIG_CONSTEXPR strlit strlit_test1 = strlit("abcdefghi"_str);
#if CIG_HAVE_CONSTEXPR
    static_assert((strlit_test1._l._cap & 0xff) ==
                  ((9 << 3) | strval::_chartype_ascii));
#else
    if ((strlit_test1._l._cap & 0xff) != ((9 << 3) | strval::_chartype_ascii))
        E("bad strlit_test1 cap byte");
#endif
    ASSERT_EXPR_IS_CONSTEXPR(strlit("abc"_str));

    // Test constexpr of long string literals
    ASSERT_EXPR_IS_NOT_CONSTEXPR(strlit("abc😀def"_str));

    // Test constexpr of str from short literal
    CIG_CONSTEXPR str abc = "abc"_str;
    CIG_CONSTEXPR str def = "def"_str;
    CIG_CONSTEXPR str abc_smiley = "abc😀"_str;
    str abc_def = "abc😀def"_str; // not constexpr from long literal

    // Test constexpr of to_str() for small numbers
    constexpr i64 tendigits = 8001503897_i64;
    constexpr str tenchars = tendigits.to_str();
    constexpr str alt_tenchars = 1234567890_i64 .to_str(); // mind the gap

    // Test constexpr of short bytes literals
    CIG_CONSTEXPR byteslit byteslit_test1 = byteslit("abcdefghi"_bytes);
#if CIG_HAVE_CONSTEXPR
    static_assert((byteslit_test1._l._cap & 0xff) == (9 << 3));
    ASSERT_EXPR_IS_CONSTEXPR(byteslit("abc"_bytes));
#else
    if ((byteslit_test1._l._cap & 0xff) != (9 << 3))
        E("byteslit_test1 bad cap byte");
#endif

    // Test constexpr of long string literals
    ASSERT_EXPR_IS_NOT_CONSTEXPR(byteslit("abcdefghijklmnopqrstuvw"_bytes));

    // Test constexpr of simple numbers
    constexpr i8 five(5);
    constexpr i16 thousand(1000);
    constexpr i32 billion(1000000000);
    constexpr i64 tera(1000000000000);
    constexpr u8 fiveu(5);
    constexpr u16 thousandu(1000);
    constexpr u32 billionu(1000000000);
    constexpr u64 trillionu(1000000000000);

    // Test constexpr of comparisons
    constexpr auto numcomp = (five == five &&
                              thousand == thousand &&
                              billion == billion &&
                              tera == tera &&
                              fiveu == fiveu &&
                              thousandu == thousandu &&
                              billionu == billionu &&
                              trillionu == trillionu &&

                              five != thousand &&
                              thousand != billion &&
                              billion != tera &&
                              fiveu != thousandu &&
                              thousandu != billionu &&
                              billionu != trillionu &&

                              !(five != five) &&
                              !(thousand != thousand) &&
                              !(billion != billion) &&
                              !(tera != tera) &&
                              !(fiveu != fiveu) &&
                              !(thousandu != thousandu) &&
                              !(billionu != billionu) &&
                              !(trillionu != trillionu) &&
                              
                              !(five == thousand) &&
                              !(thousand == billion) &&
                              !(billion == tera) &&
                              !(fiveu == thousandu) &&
                              !(thousandu == billionu) &&
                              !(billionu == trillionu) &&

                              five <= five &&
                              thousand <= thousand &&
                              billion <= billion &&
                              tera <= tera &&
                              fiveu <= fiveu &&
                              thousandu <= thousandu &&
                              billionu <= billionu &&
                              trillionu <= trillionu &&

                              !(five < five) &&
                              !(thousand < thousand) &&
                              !(billion < billion) &&
                              !(tera < tera) &&
                              !(fiveu < fiveu) &&
                              !(thousandu < thousandu) &&
                              !(billionu < billionu) &&
                              !(trillionu < trillionu) &&

                              five >= five &&
                              thousand >= thousand &&
                              billion >= billion &&
                              tera >= tera &&
                              fiveu >= fiveu &&
                              thousandu >= thousandu &&
                              billionu >= billionu &&
                              trillionu >= trillionu &&

                              !(five > five) &&
                              !(thousand > thousand) &&
                              !(billion > billion) &&
                              !(tera > tera) &&
                              !(fiveu > fiveu) &&
                              !(thousandu > thousandu) &&
                              !(billionu > billionu) &&
                              !(trillionu > trillionu) &&

                              five < thousand &&
                              thousand < billion &&
                              billion < tera &&
                              fiveu < thousandu &&
                              thousandu < billionu &&
                              billionu < trillionu &&

                              !(five > thousand) &&
                              !(thousand > billion) &&
                              !(billion > tera) &&
                              !(fiveu > thousandu) &&
                              !(thousandu > billionu) &&
                              !(billionu > trillionu) &&

                              five <= thousand &&
                              thousand <= billion &&
                              billion <= tera &&
                              fiveu <= thousandu &&
                              thousandu <= billionu &&
                              billionu <= trillionu &&

                              !(five >= thousand) &&
                              !(thousand >= billion) &&
                              !(billion >= tera) &&
                              !(fiveu >= thousandu) &&
                              !(thousandu >= billionu) &&
                              !(billionu >= trillionu) &&

                              thousand > five &&
                              billion > thousand &&
                              tera > billion &&
                              thousandu > fiveu &&
                              billionu > thousandu &&
                              trillionu > billionu &&

                              !(thousand < five) &&
                              !(billion < thousand) &&
                              !(tera < billion) &&
                              !(thousandu < fiveu) &&
                              !(billionu < thousandu) &&
                              !(trillionu < billionu) &&

                              thousand >= five &&
                              billion >= thousand &&
                              tera >= billion &&
                              thousandu >= fiveu &&
                              billionu >= thousandu &&
                              trillionu >= billionu &&

                              !(thousand <= five) &&
                              !(billion <= thousand) &&
                              !(tera <= billion) &&
                              !(thousandu <= fiveu) &&
                              !(billionu <= thousandu) &&
                              !(trillionu <= billionu));
    static_assert(numcomp == true);

    CIG_CONSTEXPR auto strcmpval = (abc == abc &&
                                    def == def &&
                                    abc_smiley == abc_smiley &&
                                    !(abc != abc) &&
                                    !(def != def) &&
                                    !(abc_smiley != abc_smiley) &&
                                    abc < def &&
                                    abc < abc_smiley &&
                                    abc_smiley < def &&
                                    abc <= def &&
                                    abc <= abc_smiley &&
                                    abc_smiley <= def &&
                                    def > abc &&
                                    abc_smiley > abc &&
                                    def > abc_smiley &&
                                    def >= abc &&
                                    abc_smiley >= abc &&
                                    def >= abc_smiley &&
                                    !(abc > def) &&
                                    !(abc > abc_smiley) &&
                                    !(abc_smiley > def) &&
                                    !(abc >= def) &&
                                    !(abc >= abc_smiley) &&
                                    !(abc_smiley >= def) &&
                                    !(def < abc) &&
                                    !(abc_smiley < abc) &&
                                    !(def < abc_smiley) &&
                                    !(def <= abc) &&
                                    !(abc_smiley <= abc) &&
                                    !(def <= abc_smiley));
#if CIG_HAVE_CONSTEXPR
    static_assert(strcmpval == true);
#else
    if (strcmpval != true)
        E("bad strcmpval");
#endif
                             

    auto strcmp_notconste = (abc_def == abc_def &&
                             !(abc_def != abc_def) &&
                             abc < abc_def &&
                             abc_smiley < abc_def &&
                             abc_def < def &&
                             abc <= abc_def &&
                             abc_smiley <= abc_def &&
                             abc_def <= def &&
                             abc_def > abc &&
                             abc_def > abc_smiley &&
                             def > abc_def &&
                             abc_def >= abc &&
                             def >= abc_def &&
                             !(abc > abc_def) &&
                             !(abc_smiley > abc_def) &&
                             !(abc_def > def) &&
                             !(abc >= abc_def) &&
                             !(abc_def >= def) &&
                             !(abc_def < abc) &&
                             !(abc_def < abc_smiley) &&
                             !(def < abc_def) &&
                             !(abc_def <= abc) &&
                             !(def <= abc_def));
    if (strcmp_notconste != true)
        E("selftest error in non-constexpr string comparisons");

    str slicebase = "slicebase"_str;
    str myslice = slicebase.slice(1_i64).slice(1_i64).slice(1_i64);
    if (myslice._get_size() != 6 || myslice != "cebase"_str)
        E("selftest error in simple stacked slice");
                              
    return 0;
}

} /* namespace cig */

/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
