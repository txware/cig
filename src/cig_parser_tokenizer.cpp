/* cig_parser_tokenizer.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Tokenizer for the CIG language.
 */
#include "cig_parser.h"
#include "cig_parser_tokenizer.h"
#include "cig_str_inline.h"

#include "cig_asciicharclass.h"
#include "cig_str_proxy.h"
#include "cig_toolbox.h"

#include <cstring>              // std::strlen



/* Note on tokenizing strings and bytes:
   
   A) Source is UTF-8
   
      The source file must be strictly UTF-8, no non-conforming
      or overlength byte sequences are allowed.
      However, in comments, string and bytes literals all
      Unicode values represented correctly in UTF-8 are allowed.

   B) Tokenizer guarantees correct str

      This tokenizer will only read correct str and bytes values.

      All str literal tokens will have correct sequences
      of Unicode code points (0 through 0x10ffff except
      0xB800 through 0xBFFF).

      While bytes literals will be obviously always correct as such,
      they may represent valid UTF-8 encoded Unicode strings or not.

   D) JSON str literals  "..."

      An unprefixed "..." str literal must conform to JSON syntax,
      i.e. Unicode characters above the BMP must be entered directly
      or as a pair of surrogate \uxxxx \uxxxx escapes.
      The tokenizer ensures that the resulting code points are valid.
      The JSON str literal is the only place where \uxxxx escapes
      may form surrogates, since other escaped forms are not available.

   E) C style str literals  u"..."

      A prefixed str literal with prefix u will allow C-style escapes
      of the form \ooo (up to three octal digits), \xxxxxx (x followed
      by any sequence of hexadecimal digits), or \Uxxxxxxxx (capital U
      followed by exactly 8 hexadecimal digits).
      These are in addition to JSON-style \uxxxx (lowercase u)
      escape sequences; however, the JSON-style sequences must not
      form surrogate pairs.
      Each escaped sequence will be translated into a single code
      point according to its value; if its value is not in the range
      of legitimate Unicode code points, the string will be rejected.
      (This is achieved by quoted_form == 2 in the Unicode conversion
      functions.)

   F) UTF-8 bytes literals  u8"..."

      UTF-8 bytes literals are UTF-8 strings with prefix u8 in the
      source code. These create bytes tokens with correct UTF-8
      byte sequences. All four styles of escape sequences (\ooo, \uxxxx,
      \Uxxxxxxxx, \xxxx...) are allowed; however the \ooo and \xxxx...
      style sequences must create value below 0x80 (ASCII values)
      to be accepted (quoted_form == 2).
      The \u and \U escape sequences are encoded as UTF-8 just as
      the regular UTF-8 characters in the input are passed through.
      The \u sequences must not form surrogate pairs.

   G) Non-UTF8 bytes literals  b"..."

      Non-UTF8 bytes literals are UTF-8 strings with prefix b in the
      source code. These create bytes tokens with correct UTF-8
      byte sequences for original UTF-8 input characters in the source
      code and for \uxxxx and \Uxxxxxxxx escape sequences, and for
      ASCII-valued \ooo and \xxxx... sequences.
      For non-ASCII-valued \ooo and \xxxx... sequences, the respective
      values are directly inserted into the byte stream.
      If the values are higher than 0xff, the token is rejected.
      The \u sequences must not form surrogate pairs.

   H) UTF-8 single character literals  '...'

      An unprefixed '...' character literal will allow C-style escapes
      of all forms, \ooo, \xxxx...., \uxxxx, \Uxxxxxxxx. However the
      resulting character must be a valid Unicode code point, otherwise
      the literal will be rejected.
      Since a single character literal is of type i64 anyway, all
      non-Unicode-values can easily be produced as numbers.
      The quoted form is convenient for direct input of UTF-8 characters
      though.

   To summarize, all correctly encoded UTF-8 characters in the source
   file can be direct accepted for str and bytes constants.

   The escaped forms \ooo and \xxxx... are accepted for bytes literals
   and for C-style str literals; however, they are always stored in
   a single code unit. For strings, therefore, the \xxxx... value must
   represent a correct Unicode code point; the \ooo does so anyway.
   For UTF-8 bytes, they must represent values below 0x80, whereas
   for Non-UTF-8 bytes, they must represent values up to 0xff.

   The escaped form \Uxxxxxxxx is accepted for C-style str literals and
   for bytes literals. The value must represent a correct Unicode code
   point and is represented as code point in str or as UTF-8 sequence
   in bytes.

   The escaped form \uxxxx is accepted for all str and bytes literals.




   Examples of literals:

   "abc"                   str
   u"abc"                  str
   "Österreich"            str
   u"Österreich"           str
   "\u00d6sterreich"       str
   u"\u00d6sterreich"      str
   "\326sterreich"         invalid // JSON does not permit \ooo escapes
   u"\326sterreich"        str
   "\U000000d6sterreich"   invalid // JSON does not permit \U escapes
   u"\U000000d6sterreich"  str
   "😀"                    str
   u"😀"                   str
   "\x1f600"               invalid // JSON does not permit \x escapes
   u"\x1f600"              str
   "\U0001f600"            invalid // JSON does not permit \U escapes
   u"\U0001f600"           str
   "\uD83D\uDE00"          str     // the only JSON way to quote the smiley
   u"\uD83D\uDE00"         invalid // C style does not join surrogates
   

   b"abc"                  bytes    // valid UTF-8
   u8"abc"                 bytes    // valid UTF-8
   b"Österreich"           bytes    // valid UTF-8
   u8"Österreich"          bytes    // valid UTF-8
   b"\u00d6sterreich"      bytes    // valid UTF-8
   u8"\u00d6sterreich"     bytes    // valid UTF-8
   b"\326sterreich"        bytes    // but not UTF-8; \326 is a single byte
   u8"\326sterreich"       invalid  // \ooo is not a single-code-unit code point
   b"\U000000d6sterreich"  bytes    // valid UTF-8
   u8"\U000000d6sterreich" bytes    // valid UTF-8
   b"\xd6sterreich"        bytes    // but not UTF-8; 0xd6 is a single byte
   u8"\xd6sterreich"       invalid  // \xd6 is not a single-code-unit code point
   b"😀"                   bytes    // valid UTF-8
   uu"😀"                  bytes    // valid UTF-8
   b"\x1f600"              invalid  // \x1f600 not a single-code-unit code point
   u8"\x1f600"             invalid  // \x1f600 not a single-code-unit code point
   b"\U0001f600"           bytes
   u8"\U0001f600"          bytes
   b"\uD83D\uDE00"         invalid  // \uD83D not a single-code-unit code pt ...
   u8"\uD83D\uDE00"        invalid  // ... and C style does not join surrogates

   'Ö'                     i64      // single-char value
   '\u00d6'                i64      // single-char value
   '\U000000d6'            i64      // single-char value
   '\xd6'                  i64      // single-char value
   '\326'                  i64      // single-char value
   '😀'                    i64      // single-char value
   '\x1f600'               i64      // single-char value
   '\U0001f600'            i64      // single-char value
   '\uD83D\uDE00'          invalid  // C style does not join surrogates


 */ 




namespace cig {

namespace parser {


char const *tokentype_name(tokentype ttype)
{
    using enum tokentype;
    switch (ttype)
    {
    case INVALID:          return "INVALID";
    case KEYWORDS:         return "KEYWORDS";
    case FUNCTION:         return "FUNCTION";
    case RETURN:           return "RETURN";
    case NAMESPACE:        return "NAMESPACE";
    case CLASS:            return "CLASS";
    case ENUM:             return "ENUM";
    case NATIVE:           return "NATIVE";
    case STATIC:           return "STATIC";
    case NONSTATIC:        return "NONSTATIC";
    case KWD_AND:          return "KWD_AND";
    case KWD_OR:           return "KWD_OR";
    case KWD_NOT:          return "KWD_NOT";
    case KWD_IN:           return "KWD_IN";
    case KWD_IF:           return "KWD_IF";
    case KWD_ELSE:         return "KWD_ELSE";
    case KWD_FOR:          return "KWD_FOR";
    case KWD_WHILE:        return "KWD_WHILE";
    case KWD_DO:           return "KWD_DO";
    case KWD_IS:           return "KWD_IS";
    case KWD_OF:           return "KWD_OF";
    case GOTO:             return "GOTO";
    case LABEL:            return "LABEL";
    case BREAK:            return "BREAK";
    case CONTINUE:         return "CONTINUE";
    case KEYWORD_MAX:      return "KEYWORD_MAX";
    case SYMBOL_MIN:       return "SYMBOL_MIN";
    case SHL_ASSIGN:       return "SHL_ASSIGN";
    case SHR_ASSIGN:       return "SHR_ASSIGN";
    case EQUAL:            return "EQUAL";
    case NOT_EQUAL:        return "NOT_EQUAL";
    case LESS_EQUAL:       return "LESS_EQUAL";
    case GREATER_EQUAL:    return "GREATER_EQUAL";
    case SHIFT_LEFT:       return "SHIFT_LEFT";
    case SHIFT_RIGHT:      return "SHIFT_RIGHT";
    case LESS:             return "LESS";
    case GREATER:          return "GREATER";
    case ARROW:            return "ARROW";
    case DOT_ASSIGN:       return "DOT_ASSIGN";
    case PLUS_ASSIGN:      return "PLUS_ASSIGN";
    case MINUS_ASSIGN:     return "MINUS_ASSIGN";
    case MULT_ASSIGN:      return "MULT_ASSIGN";
    case DIV_ASSIGN:       return "DIV_ASSIGN";
    case MOD_ASSIGN:       return "MOD_ASSIGN";
    case AND_ASSIGN:       return "AND_ASSIGN";
    case XOR_ASSIGN:       return "XOR_ASSIGN";
    case OR_ASSIGN:        return "OR_ASSIGN";
    case ASSIGN:           return "ASSIGN";
    case PLUS:             return "PLUS";
    case MINUS:            return "MINUS";
    case MULT:             return "MULT";
    case DIV:              return "DIV";
    case MOD:              return "MOD";
    case LOGICAL_AND:      return "LOGICAL_AND";
    case LOGICAL_OR:       return "LOGICAL_OR";
    case BIT_AND:          return "BIT_AND";
    case BIT_XOR:          return "BIT_XOR";
    case BIT_OR:           return "BIT_OR";
    case LOGICAL_NOT:      return "LOGICAL_NOT";
    case BIT_NOT:          return "BIT_NOT";
    case OPENPAREN:        return "OPENPAREN";
    case CLOSEPAREN:       return "CLOSEPAREN";
    case OPENBRACE:        return "OPENBRACE";
    case CLOSEBRACE:       return "CLOSEBRACE";
    case OPENBRACKET:      return "OPENBRACKET";
    case CLOSEBRACKET:     return "CLOSEBRACKET";
    case OPENTMPL:         return "OPENTMPL";
    case CLOSETMPL:        return "CLOSETMPL";
    case DOT:              return "DOT";
    case COMMA:            return "COMMA";
    case SEMICOLON:        return "SEMICOLON";
    case COLON:            return "COLON";
    case QUEST:            return "QUEST";
    case REFPTR:           return "REFPTR";
    case WEAKPTR:          return "WEAKPTR";
    case SHAREDPTR:        return "SHAREDPTR";
    case NONNULLPTR:       return "NONNULLPTR";
    case ATSIGN:           return "ATSIGN";
    case DOLLAR:           return "DOLLAR";
    case SYMBOL_MAX:       return "SYMBOL_MAX";
    case LITERAL_MIN:      return "LITERAL_MIN";
    case LITERAL_STR:      return "LITERAL_STR";
    case LITERAL_BYTES:    return "LITERAL_BYTES";
    case LITERAL_CHAR:     return "LITERAL_CHAR";
    case LITERAL_I64:      return "LITERAL_I64";
    case LITERAL_I32:      return "LITERAL_I32";
    case LITERAL_I16:      return "LITERAL_I16";
    case LITERAL_I8:       return "LITERAL_I8";
    case LITERAL_U64:      return "LITERAL_U64";
    case LITERAL_U32:      return "LITERAL_U32";
    case LITERAL_U16:      return "LITERAL_U16";
    case LITERAL_U8:       return "LITERAL_U8";
    case LITERAL_BOOL:     return "LITERAL_BOOL";
    case LITERAL_MAX:      return "LITERAL_MAX";
    case IDENTIFIER:       return "IDENTIFIER";
    case FILEEND:          return "FILEEND";
    case ERROR:            return "ERROR";
    case MAX:              return "MAX";
    }
    return "";
}

char const *tokentype_symbol(tokentype ttype)
{
    using enum tokentype;
    switch (ttype)
    {
    case INVALID:          return "";
    case KEYWORDS:         return "keywords";
    case FUNCTION:         return "function";
    case RETURN:           return "return";
    case NAMESPACE:        return "namespace";
    case CLASS:            return "class";
    case ENUM:             return "enum";
    case NATIVE:           return "native";
    case STATIC:           return "static";
    case NONSTATIC:        return "nonstatic";
    case KWD_AND:          return "and";
    case KWD_OR:           return "or";
    case KWD_NOT:          return "not";
    case KWD_IN:           return "in";
    case KWD_IF:           return "if";
    case KWD_ELSE:         return "else";
    case KWD_FOR:          return "for";
    case KWD_WHILE:        return "while";
    case KWD_DO:           return "do";
    case KWD_IS:           return "is";
    case KWD_OF:           return "of";
    case GOTO:             return "goto";
    case LABEL:            return "label";
    case BREAK:            return "break";
    case CONTINUE:         return "continue";
    case KEYWORD_MAX:      return "";
    case SYMBOL_MIN:       return "";
    case SHL_ASSIGN:       return "<<=";
    case SHR_ASSIGN:       return ">>=";
    case EQUAL:            return "==";
    case NOT_EQUAL:        return "!=";
    case LESS_EQUAL:       return "<=";
    case GREATER_EQUAL:    return ">=";
    case SHIFT_LEFT:       return "<<";
    case SHIFT_RIGHT:      return ">>";
    case LESS:             return "<";
    case GREATER:          return ">";
    case ARROW:            return "->";
    case DOT_ASSIGN:       return ".=";
    case PLUS_ASSIGN:      return "+=";
    case MINUS_ASSIGN:     return "-=";
    case MULT_ASSIGN:      return "*=";
    case DIV_ASSIGN:       return "/=";
    case MOD_ASSIGN:       return "%=";
    case AND_ASSIGN:       return "&=";
    case XOR_ASSIGN:       return "^=";
    case OR_ASSIGN:        return "|=";
    case ASSIGN:           return "=";
    case PLUS:             return "+";
    case MINUS:            return "-";
    case MULT:             return "*";
    case DIV:              return "/";
    case MOD:              return "%";
    case LOGICAL_AND:      return "&&";
    case LOGICAL_OR:       return "||";
    case BIT_AND:          return "&";
    case BIT_XOR:          return "^";
    case BIT_OR:           return "|";
    case LOGICAL_NOT:      return "!";
    case BIT_NOT:          return "~";
    case OPENPAREN:        return "(";
    case CLOSEPAREN:       return ")";
    case OPENBRACE:        return "{";
    case CLOSEBRACE:       return "}";
    case OPENBRACKET:      return "[";
    case CLOSEBRACKET:     return "]";
    case OPENTMPL:         return "<:";
    case CLOSETMPL:        return ":>";
    case DOT:              return ".";
    case COMMA:            return ",";
    case SEMICOLON:        return ";";
    case COLON:            return ":";
    case QUEST:            return "?";
    case REFPTR:           return "<&>";
    case WEAKPTR:          return "<***>";
    case SHAREDPTR:        return "<**>";
    case NONNULLPTR:       return "<*>";
    case ATSIGN:           return "@";
    case DOLLAR:           return "$";
    case SYMBOL_MAX:       return "";
    case LITERAL_MIN:
    case LITERAL_STR:
    case LITERAL_BYTES:
    case LITERAL_CHAR:
    case LITERAL_I64:
    case LITERAL_I32:
    case LITERAL_I16:
    case LITERAL_I8:
    case LITERAL_U64:
    case LITERAL_U32:
    case LITERAL_U16:
    case LITERAL_U8:
    case LITERAL_BOOL:
    case LITERAL_MAX:      return "";
    case IDENTIFIER:       return "";
    case FILEEND:          return "";
    case ERROR:            return "";
    case MAX:              return "";
    }
    return "";
}

str const tokentype_symbolstr(tokentype ttype)
{
    using enum tokentype;
    switch (ttype)
    {
    case INVALID:          return ""_str;
    case KEYWORDS:         return "keywords"_str;
    case FUNCTION:         return "function"_str;
    case RETURN:           return "return"_str;
    case NAMESPACE:        return "namespace"_str;
    case CLASS:            return "class"_str;
    case ENUM:             return "enum"_str;
    case NATIVE:           return "native"_str;
    case STATIC:           return "static"_str;
    case NONSTATIC:        return "nonstatic"_str;
    case KWD_AND:          return "and"_str;
    case KWD_OR:           return "or"_str;
    case KWD_NOT:          return "not"_str;
    case KWD_IN:           return "in"_str;
    case KWD_IF:           return "if"_str;
    case KWD_ELSE:         return "else"_str;
    case KWD_FOR:          return "for"_str;
    case KWD_WHILE:        return "while"_str;
    case KWD_DO:           return "do"_str;
    case KWD_IS:           return "is"_str;
    case KWD_OF:           return "of"_str;
    case GOTO:             return "goto"_str;
    case LABEL:            return "label"_str;
    case BREAK:            return "break"_str;
    case CONTINUE:         return "continue"_str;
    case KEYWORD_MAX:      return ""_str;
    case SYMBOL_MIN:       return ""_str;
    case SHL_ASSIGN:       return "<<="_str;
    case SHR_ASSIGN:       return ">>="_str;
    case EQUAL:            return "=="_str;
    case NOT_EQUAL:        return "!="_str;
    case LESS_EQUAL:       return "<="_str;
    case GREATER_EQUAL:    return ">="_str;
    case SHIFT_LEFT:       return "<<"_str;
    case SHIFT_RIGHT:      return ">>"_str;
    case LESS:             return "<"_str;
    case GREATER:          return ">"_str;
    case ARROW:            return "->"_str;
    case DOT_ASSIGN:       return ".="_str;
    case PLUS_ASSIGN:      return "+="_str;
    case MINUS_ASSIGN:     return "-="_str;
    case MULT_ASSIGN:      return "*="_str;
    case DIV_ASSIGN:       return "/="_str;
    case MOD_ASSIGN:       return "%="_str;
    case AND_ASSIGN:       return "&="_str;
    case XOR_ASSIGN:       return "^="_str;
    case OR_ASSIGN:        return "|="_str;
    case ASSIGN:           return "="_str;
    case PLUS:             return "+"_str;
    case MINUS:            return "-"_str;
    case MULT:             return "*"_str;
    case DIV:              return "/"_str;
    case MOD:              return "%"_str;
    case LOGICAL_AND:      return "&&"_str;
    case LOGICAL_OR:       return "||"_str;
    case BIT_AND:          return "&"_str;
    case BIT_XOR:          return "^"_str;
    case BIT_OR:           return "|"_str;
    case LOGICAL_NOT:      return "!"_str;
    case BIT_NOT:          return "~"_str;
    case OPENPAREN:        return "("_str;
    case CLOSEPAREN:       return ")"_str;
    case OPENBRACE:        return "{"_str;
    case CLOSEBRACE:       return "}"_str;
    case OPENBRACKET:      return "["_str;
    case CLOSEBRACKET:     return "]"_str;
    case OPENTMPL:         return "<:"_str;
    case CLOSETMPL:        return ":>"_str;
    case DOT:              return "."_str;
    case COMMA:            return ","_str;
    case SEMICOLON:        return "'"_str;
    case COLON:            return ":"_str;
    case QUEST:            return "?"_str;
    case REFPTR:           return "<&>"_str;
    case WEAKPTR:          return "<***>"_str;
    case SHAREDPTR:        return "<**>"_str;
    case NONNULLPTR:       return "<*>"_str;
    case ATSIGN:           return "@"_str;
    case DOLLAR:           return "$"_str;
    case SYMBOL_MAX:       return ""_str;
    case LITERAL_MIN:
    case LITERAL_STR:
    case LITERAL_BYTES:
    case LITERAL_CHAR:
    case LITERAL_I64:
    case LITERAL_I32:
    case LITERAL_I16:
    case LITERAL_I8:
    case LITERAL_U64:
    case LITERAL_U32:
    case LITERAL_U16:
    case LITERAL_U8:
    case LITERAL_BOOL:
    case LITERAL_MAX:      return ""_str;
    case IDENTIFIER:       return ""_str;
    case FILEEND:          return ""_str;
    case ERROR:            return ""_str;
    case MAX:              return ""_str;
    }
    return ""_str;
}


bool tokentype_is_keyword(tokentype ttype)
{
    using enum tokentype;
    return (ttype > INVALID && ttype < KEYWORD_MAX);
}


bool tokentype_is_literal(tokentype ttype)
{
    using enum tokentype;
    return (ttype > LITERAL_MIN && ttype < LITERAL_MAX);
}


token::token(cig::source_location const &sloc,tokentype t)
{
    sourceloc = sloc;
    ttype = t;
}


token::token(cig::source_location const &sloc,tokentype t,i64 len)
{
    sourceloc = sloc;
    sourcelen = uint32_t(len._v);
    ttype = t;
}


static inline
tokentype
literal_type_to_tokentype(cig::ast::literal::literal_type t)
{
    using enum cig::ast::literal::literal_type;
    switch (t)
    {
    case LIT_UNDEFINED: return tokentype::INVALID;
    case LIT_STR: return tokentype::LITERAL_STR;
    case LIT_BYTES: return tokentype::LITERAL_BYTES;
    case LIT_I8: return tokentype::LITERAL_I8;
    case LIT_I16: return tokentype::LITERAL_I16;
    case LIT_I32: return tokentype::LITERAL_I32;
    case LIT_I64: return tokentype::LITERAL_I64;
    case LIT_U8: return tokentype::LITERAL_U8;
    case LIT_U16: return tokentype::LITERAL_U16;
    case LIT_U32: return tokentype::LITERAL_U32;
    case LIT_U64: return tokentype::LITERAL_U64;
    case LIT_BOOL: return tokentype::LITERAL_BOOL;
    }
    return tokentype::INVALID;
}



cig::source_location token::startloc() const noexcept
{
    return sourceloc;
}

cig::source_location token::endloc() const noexcept
{
    cig::source_location sloc(int64_t(sourceloc.line()),
                              int64_t(sourceloc.column() + sourcelen),
                              sourceloc.file_name(),
                              sourceloc.function_name());
    return sloc;
}



// The current location in the source
#define L()                                     \
    cig::source_location(                       \
    static_cast<int64_t>(line._v + 1),          \
    static_cast<int64_t>(column._v + 1),        \
    filename,                                   \
    0)

// Return a parse error message as std::unexpected
#define E(e) std::unexpected(parse_error(L(),e))
#define EE(e,s) std::unexpected(parse_error(L(),e,s))

// Return an algorithmic error with a C literal string as description
#define ALGORITHMIC(text) std::unexpected(parse_error(err::algorithmic(text)))


// Create a token from current source location
#define AUXTOKEN(ttype)                         \
token(cig::source_location(                     \
    static_cast<int64_t>(line._v + 1),          \
    static_cast<int64_t>(column._v + 1),        \
    filename,                                   \
    0),                                         \
    ttype)
#define LEN_TOKEN(ttype,len)                    \
token(cig::source_location(                     \
    static_cast<int64_t>(line._v + 1),          \
    static_cast<int64_t>(column._v + 1),        \
    filename,                                   \
    0),                                         \
    ttype,                                      \
    len)

// Create and a symbol token and add to the list
#define TTYPESTRAUX(x) #x
#define TTYPESTR(x) TTYPESTRAUX(x)
#define SYMBOL_TOKEN(ttype)                                     \
    do {                                                        \
        token newtoken = AUXTOKEN(ttype);                       \
        char const *sym = tokentype_symbol(ttype);              \
        uint32_t len = 1;                                       \
        /* Symbols have three chars maximum */                  \
        if (c != sym[0] ||                                      \
            (sym[1] &&                                          \
             (nextc != sym[1] ||                                \
              (sym[2] &&                                        \
               (s[pos+2] != sym[2] ||                           \
                sym[3])))))                                     \
            return EE(err::EC_INCONSISTENT_TOKEN_DEFINITION,    \
                      TTYPESTR(ttype));                         \
        if (!sym[0])                                            \
            len = 0;                                            \
        else if (!sym[1])                                       \
            len = 1;                                            \
        else if (!sym[2])                                       \
            len = 2;                                            \
        else                                                    \
            len = 3;                                            \
        newtoken.sourcelen = len;                               \
        tokenlist.push_back(newtoken);                          \
        pos = pos + len;                                        \
        column = column + static_cast<uint64_t>(len);           \
    } while (0)


std::expected<cig::vector<token>,parse_error>
exp_tokenize_file_str(str const &s,char const *filename,i64 tabsize)
{
    i64 filelen = s.size();
    i64 pos;
    u64 line = 0;
    u64 column = 0;
    cig::vector<token> tokenlist;
    using enum tokentype;

    // Empty file
    if (!filelen)
    {
        token tok = LEN_TOKEN(FILEEND,0);
        tokenlist.push_back(tok);
        return tokenlist;
    }

    // The last character must be a newline,
    // i.e. the last line must be terminated.
    if (!s.ends_with("\n"_str))
    {
        toolbox::count_line_and_column(s,0,filelen,line,column,tabsize);
        return E(err::EC_NO_NEWLINE_AT_END_OF_FILE);
    }

    for (pos = 0; pos < filelen; )
    {
        i64 c = s[pos];
        acctag acc = codepoint_to_acctag(c);
        i64 nextc = s[pos+1];   // next code point, or -1 at end

        // We are at the beginning or end of a tag
        switch (acc)
        {
        case acctag::NUL:
            return E(err::EC_EMBEDDED_NUL_CHAR_IN_FILE);
        case acctag::CONTROL:
            return E(err::EC_EMBEDDED_ASCII_CONTROL_CHAR_IN_FILE);

            // Skip whitespace
        case acctag::HT:        // horizontal tab
        case acctag::LF:        // line feed
        case acctag::CR:        // carriage return
        case acctag::SPACE:
            if (true)
            {
                i64 newpos = s.find_first_not_of("\t\n\r "_str,pos);
                if (newpos == -1)
                {
                    // We reached the end of file
                    toolbox::count_line_and_column(
                        s,pos,filelen,line,column,tabsize);
                    pos = filelen;
                    // Leave the for loop
                    goto regular_return;
                }
                // Skip the whitespace
                toolbox::count_line_and_column(
                    s,pos,newpos,line,column,tabsize);
                // No token to create
                pos = newpos;
            }
            continue;
        case acctag::EXCLAM:    // ! exclamation mark
            if (nextc == '=')
                SYMBOL_TOKEN(NOT_EQUAL);
            else
                SYMBOL_TOKEN(LOGICAL_NOT);
            continue;
        case acctag::DQUOTE:    // " double quote
            if (true)           // A JSON-style quoted string literal
            {
                i64 endpos;
                auto exp = toolbox::exp_decode_json_str(s,pos,endpos);
                if (exp)
                {
                    i64 len = endpos - pos;
                    token tok = LEN_TOKEN(LITERAL_STR,len);
                    tok.value = std::move(exp.value());
                    tokenlist.push_back(tok);
                    toolbox::count_line_and_column(s,pos,endpos,line,column,
                                                   tabsize);
                    pos = endpos;
                }
                else
                {
                    // Return the string decoder error
                    // but with the current source location
                    return E(exp.error().get_error_code());
                }
            }
            continue;
        case acctag::HASH:      // # hash sign
            if (true)
            {
                i64 newpos = s.find('\n',pos);
                if (newpos == -1)
                    E(err::EC_UNTERMINATED_HASH_COMMENT);
                i64 endpos = newpos + 1;
                toolbox::count_line_and_column(s,pos,endpos,line,column,
                                               tabsize);
                pos = endpos;
            }
            continue;
        case acctag::DOLLAR:    // $ dollar sign
            SYMBOL_TOKEN(DOLLAR);
            continue;
        case acctag::PERCNT:
            if (nextc == '=')
                SYMBOL_TOKEN(MOD_ASSIGN);
            else
                SYMBOL_TOKEN(MOD);
            continue;
        case acctag::AMPAND:    // & ampersand
            if (nextc == '=')
                SYMBOL_TOKEN(AND_ASSIGN);
            else if (nextc == '&')
                SYMBOL_TOKEN(LOGICAL_AND);
            else
                SYMBOL_TOKEN(BIT_AND);
            continue;
        case acctag::SQUOTE:    // ' single quote
            if (true)
            {
                i64 endpos;
                auto exp = toolbox::exp_decode_quoted_codepoint(s,pos,endpos);
                if (exp)
                {
                    i64 len = endpos - pos;
                    token tok = LEN_TOKEN(LITERAL_CHAR,len);
                    i64 ch = exp.value();
                    tok.value = ch; // single character literal is an i64
                    tokenlist.push_back(tok);
                    toolbox::count_line_and_column(s,pos,endpos,line,column,
                                                   tabsize);
                    pos = endpos;
                }
                else
                {
                    // Return the string decoder error
                    // but with the current source location
                    return E(exp.error().get_error_code());
                }
                continue;
            }
            continue;
        case acctag::OPAREN:    // ( opening parenthesis
            SYMBOL_TOKEN(OPENPAREN);
            continue;
        case acctag::CPAREN:    // ) closing parenthesis
            SYMBOL_TOKEN(CLOSEPAREN);
            continue;
        case acctag::AST:       // * asterisk
            if (nextc == '=')
                SYMBOL_TOKEN(MULT_ASSIGN);
            else
                SYMBOL_TOKEN(MULT);
            continue;
        case acctag::PLUS:      // + plus sign
            if (nextc == '=')
                SYMBOL_TOKEN(PLUS_ASSIGN);
            else
                SYMBOL_TOKEN(PLUS);
            continue;
        case acctag::COMMA:     // , comma
            SYMBOL_TOKEN(COMMA);
            continue;
        case acctag::MINUS:     // - minus sign
            if (nextc == '>')
                SYMBOL_TOKEN(ARROW);
            else if (nextc == '=')
                SYMBOL_TOKEN(MINUS_ASSIGN);
            else
                SYMBOL_TOKEN(MINUS);
            continue;
        case acctag::DOT:       // . dot
            if (nextc == '=')
                SYMBOL_TOKEN(DOT_ASSIGN);
            else
                SYMBOL_TOKEN(DOT);
            continue;
        case acctag::SLASH:     // / forward slash
            if (nextc == '*')
            {
                i64 newpos = s.find("*/"_str,pos);
                if (newpos == -1)
                    E(err::EC_UNTERMINATED_C_COMMENT);
                i64 endpos = newpos + 2;
                toolbox::count_line_and_column(s,pos,endpos,line,column,
                                               tabsize);
                pos = endpos;
            }
            else if (nextc == '/')
            {
                i64 newpos = s.find('\n',pos);
                if (newpos == -1)
                    E(err::EC_UNTERMINATED_CXX_COMMENT);
                i64 endpos = newpos + 1;
                toolbox::count_line_and_column(s,pos,endpos,line,column,
                                               tabsize);
                pos = endpos;
            }
            else if (nextc == '=')
                SYMBOL_TOKEN(DIV_ASSIGN);
            else
                SYMBOL_TOKEN(DIV);
            continue;
        case acctag::DIGIT:
            if (true)
            {
                i64 endpos;
                auto exp = toolbox::exp_decode_int_literal(s,pos,endpos);
                if (exp)
                {
                    i64 len = endpos - pos;
                    token tok = LEN_TOKEN(literal_type_to_tokentype(
                                          exp.value().get_literal_type()),
                                          len);
                    tok.value = exp.value();
                    tokenlist.push_back(tok);
                    toolbox::count_line_and_column(s,pos,endpos,line,column,
                                                   tabsize);
                    pos = endpos;
                }
                else
                {
                    // Return the integer decoder error
                    // but with the current source location
                    // Return the string decoder error
                    // but with the current source location
                    return E(exp.error().get_error_code());
                }
            }
            continue;
        case acctag::COLON:     // : colon
            if (nextc == '>')
                SYMBOL_TOKEN(CLOSETMPL);
            else
                SYMBOL_TOKEN(COLON);
            continue;
        case acctag::SEMICOL:   // ; semicolon
            SYMBOL_TOKEN(SEMICOLON);
            continue;
        case acctag::LESS:      // < less sign/left angular bracket
            if (nextc == ':')
                SYMBOL_TOKEN(OPENTMPL);
            else if (nextc == '=')
                SYMBOL_TOKEN(LESS_EQUAL);
            else if (nextc == '<')
            {
                if (s[pos+2] == '=')
                    SYMBOL_TOKEN(SHL_ASSIGN);
                else
                    SYMBOL_TOKEN(SHIFT_LEFT);
            }
            else
                SYMBOL_TOKEN(LESS);
            continue;
        case acctag::EQUAL:     // = equal sign
            if (nextc == '=')
                SYMBOL_TOKEN(EQUAL);
            else
                SYMBOL_TOKEN(ASSIGN);
            continue;
        case acctag::GREATER:   // > greater sign/right angular bracket
            if (nextc == '=')
                SYMBOL_TOKEN(GREATER_EQUAL);
            else if (nextc == '>')
            {
                if (s[pos+2] == '=')
                    SYMBOL_TOKEN(SHR_ASSIGN);
                else
                    SYMBOL_TOKEN(SHIFT_RIGHT);
            }
            else
                SYMBOL_TOKEN(GREATER);
            continue;
        case acctag::QUEST:     // ? question mark
            SYMBOL_TOKEN(QUEST);
            continue;
        case acctag::ATSIGN:    // @ at sign
            if (nextc == '@')
            {
                if (s[pos+2] == '@')
                    SYMBOL_TOKEN(WEAKPTR);
                else
                    SYMBOL_TOKEN(SHAREDPTR);
            }
            else if (nextc == '&')
            {
                SYMBOL_TOKEN(REFPTR);
            }
            else
                SYMBOL_TOKEN(NONNULLPTR);
            continue;
        case acctag::UPPER:
        case acctag::LOWER:
        case acctag::USCORE:
            if (c == 'u' && nextc == '"') // A C-style quoted str literal
            {
                i64 endpos;
                auto exp = toolbox::exp_decode_C_str(s,pos+1,endpos);
                if (exp)
                {
                    i64 len = endpos - pos;
                    token tok = LEN_TOKEN(LITERAL_STR,len);
                    tok.value = std::move(exp.value());
                    tokenlist.push_back(tok);
                    toolbox::count_line_and_column(s,pos,endpos,line,column,
                                                   tabsize);
                    pos = endpos;
                }
                else
                {
                    // Return the string decoder error
                    // but with the current source location
                    return E(exp.error().get_error_code());
                }
            }
            else if (c == 'u' &&     // A C-style quoted bytes literal
                     nextc == '8' && // with valid UTF-8 content
                     s[pos+2] == '"')
            {
                i64 endpos;
                auto exp = toolbox::exp_decode_C_bytes_u8(s,pos+2,endpos);
                if (exp)
                {
                    i64 len = endpos - pos;
                    token tok = LEN_TOKEN(LITERAL_BYTES,len);
                    tok.value = std::move(exp.value());
                    tokenlist.push_back(tok);
                    toolbox::count_line_and_column(s,pos,endpos,line,column,
                                                   tabsize);
                    pos = endpos;
                }
                else
                {
                    // Return the bytes decoder error
                    // but with the current source location
                    return E(exp.error().get_error_code());
                }
            }
            else if (c == 'b' &&   // A C-style quoted bytes literal
                     nextc == '"') // with any bytes, not UTF-8 conforming
            {
                i64 endpos;
                auto exp = toolbox::exp_decode_C_bytes(s,pos+1,endpos);
                if (exp)
                {
                    i64 len = endpos - pos;
                    token tok = LEN_TOKEN(LITERAL_BYTES,len);
                    tok.value = std::move(exp.value());
                    tokenlist.push_back(tok);
                    toolbox::count_line_and_column(s,pos,endpos,line,column,
                                                   tabsize);
                    pos = endpos;
                }
                else
                {
                    // Return the bytes decoder error
                    // but with the current source location
                    return E(exp.error().get_error_code());
                }
            }
            else                // A keyword or identifier
            {
                i64 newpos =
                    s.find_first_not_in_acc(
                        cig::accmask::DIGIT |
                        cig::accmask::LOWER | // hex chars or exponent
                        cig::accmask::UPPER | // hex chars or exponent
                        cig::accmask::USCORE,
                        pos);
                if (newpos == -1)
                {
                    return ALGORITHMIC(
                        "File ends within keyword or identifier");
                }
                str id = s.slice(pos,newpos);
                tokentype tk = INVALID;
                for (;
                     tk < KEYWORD_MAX;
                     tk = static_cast<tokentype>(static_cast<int>(tk) + 1))
                {
                    if (tk == INVALID)
                        continue;
                    // fixme only consider switched-on keywords
                    if (id == tokentype_symbolstr(tk))
                        break;
                }
                i64 len = newpos - pos;
                if (tk < KEYWORD_MAX)
                {
                    token tok = LEN_TOKEN(tk,len);
                    tok.value = std::move(id);
                    tokenlist.push_back(tok);
                }
                else
                {
                    token tok = LEN_TOKEN(IDENTIFIER,len);
                    tok.value = std::move(id);
                    tokenlist.push_back(tok);
                }
                toolbox::count_line_and_column(s,pos,newpos,line,column,
                                               tabsize);
                pos = newpos;
            }
            continue;
        case acctag::OBRACK:
            SYMBOL_TOKEN(OPENBRACKET);
            continue;
        case acctag::BSLASH:
            return E(err::EC_EMBEDDED_BACKSLASH_CHAR_IN_FILE);
        case acctag::CBRACK:
            SYMBOL_TOKEN(CLOSEBRACKET);
            continue;
        case acctag::CARET:
            if (nextc == '=')
                SYMBOL_TOKEN(XOR_ASSIGN);
            else
                SYMBOL_TOKEN(BIT_XOR);
            continue;
        case acctag::GRAVE:
            return E(err::EC_UNKNOWN_CHARACTER);
        case acctag::OBRACE:
            SYMBOL_TOKEN(OPENBRACE);
            continue;
        case acctag::VBAR:
            if (nextc == '=')
                SYMBOL_TOKEN(OR_ASSIGN);
            else if (nextc == '|')
                SYMBOL_TOKEN(LOGICAL_OR);
            else
                SYMBOL_TOKEN(BIT_OR);
            continue;
        case acctag::CBRACE:
            SYMBOL_TOKEN(CLOSEBRACE);
            continue;
        case acctag::TILDE:
            SYMBOL_TOKEN(BIT_NOT);
            continue;
        case acctag::DEL:
            E(err::EC_EMBEDDED_DEL_CHAR_IN_FILE);
            continue;
        case acctag::NOASCII:
            return E(err::EC_EMBEDDED_NONASCII_CHAR_IN_FILE);
        }

        return ALGORITHMIC("Unrecognized ASCII character class code");
    }

    // We can come here from a final to-end-of-line comment
    // or from whitespace
 regular_return:
    token tok = LEN_TOKEN(FILEEND,0);
    tokenlist.push_back(tok);
    return tokenlist;
}

#undef E


// New definition of E() for selftest
#define TOSTR(x) #x
#define TOLINENUM(x) TOSTR(x)
#define E(s)                                                            \
    do {                                                                \
        return (__FILE__  ":" TOLINENUM(__LINE__) ": error: " s);       \
    } while (0)


// Selftest
char const *parser_tokenizer_selftest_errorstring()
{
    using namespace cig::parser;
    using enum tokentype;
    str teststr = "    /* bla \\ */  //  \\ \n  #  \n"_str;
    static char auxerrormsg[200];

    for (tokentype tk = INVALID;
         tk < MAX;
         tk = static_cast<tokentype>(static_cast<int>(tk) + 1))
    {
        if (tk == LITERAL_STR)
        {
            teststr += " u\"\\u00d6sterreich\" "_str;
            continue;
        }
        else if (tk == LITERAL_BYTES)
        {
            teststr += " u8\"\\u00d6sterreich\" "_str;
            continue;
        }
        else if (tk == LITERAL_CHAR)
        {
            teststr += " '\\326' "_str;
            continue;
        }
        else if (tk == INVALID ||
            tk == KEYWORD_MAX ||
            tk == SYMBOL_MIN ||
            tk == SYMBOL_MAX ||
            (static_cast<int64_t>(tk) >= static_cast<int64_t>(LITERAL_MIN) &&
             static_cast<int64_t>(tk) <= static_cast<int64_t>(LITERAL_MAX)) ||
            tk == IDENTIFIER ||
            tk == FILEEND ||
            tk == ERROR)
            continue;

        teststr += " "_str;
        teststr += str::from_ascii(tokentype_symbol(tk),
                                   std::strlen(tokentype_symbol(tk)));

    }

    teststr += "\n"_str;

    auto exp_tokenlist = exp_tokenize_file_str(teststr,"selftest");
    if (!exp_tokenlist.has_value())
    {
        auto error = exp_tokenlist.error();
        detail::utf8proxy u(error.name);
        if (u.size())
        {
            snprintf(auxerrormsg,sizeof(auxerrormsg),"%s:%lu: %s: %s",
                     __FILE__,static_cast<unsigned long>(__LINE__),
                     error.strerror_c(),
                     u.c_str());
        }
        else
        {
            snprintf(auxerrormsg,sizeof(auxerrormsg),"%s:%lu: %s",
                     __FILE__,static_cast<unsigned long>(__LINE__),
                     error.strerror_c());
        }
        return auxerrormsg;
    }

    auto tokenlist = std::move(exp_tokenlist.value());

    unsigned int i = 0;
    for (tokentype tk = INVALID;
         tk < MAX;
         tk = static_cast<tokentype>(static_cast<int>(tk) + 1))
    {
        if (tk == INVALID ||
            tk == KEYWORD_MAX ||
            tk == SYMBOL_MIN ||
            tk == SYMBOL_MAX ||
            tk == SYMBOL_MAX ||
            tk == LITERAL_MIN ||
            (static_cast<int64_t>(tk) > static_cast<int64_t>(LITERAL_CHAR) &&
             static_cast<int64_t>(tk) < static_cast<int64_t>(LITERAL_MAX)) ||
            tk == LITERAL_MAX ||
            tk == IDENTIFIER ||
            tk == FILEEND ||
            tk == ERROR)
            continue;

        token tok = tokenlist[i];
        if (tok.ttype != tk)
        {
            snprintf(auxerrormsg,sizeof(auxerrormsg),"%s:%lu: "
                     "tk = %d, tok.ttype = %d",
                     __FILE__,static_cast<unsigned long>(__LINE__),
                     static_cast<int>(tk),
                     static_cast<int>(tok.ttype));
        }
        i++;
    }
    return 0;
}




} // namespace parser

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
