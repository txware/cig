/* cig_sysdep.h
 *
 * Copyright 2023 Claus Fischer
 *
 * System dependent low-level functions.
 */
#ifndef CIG_SYSDEP_H_INCLUDED
#define CIG_SYSDEP_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif


char const *			/* static error string or 0 on success */
cig_sys_selftest(void);


#ifdef __cplusplus
} /* extern "C" */
#endif

/* ** EMACS **
 * Local Variables:
 * mode: C
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_SYSDEP_H_INCLUDED) */
