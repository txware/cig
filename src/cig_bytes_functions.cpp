/* cig_bytes_functions.cpp
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Runtime non-inline bytes sequence functions.
 */
#include "cig_datatypes.h"

#include "cig_unicode_conversion.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_asciicharclass.h"


#include <cstdint>              // int64_t ...
#include <cstddef>              // std::size_t

namespace cig {

namespace detail {


//
// auxiliary
//


template<typename S1,typename S2>
static inline
bool
bytes_equal(
    S1 const * CIG_RESTRICT s1, // [i] input code points of first sequence
    S2 const * CIG_RESTRICT s2, // [i] input code points of second sequence
    int64_t n                   // [i] length of both strings
    )
{
    typedef typename std::make_unsigned<S1>::type US1;
    typedef typename std::make_unsigned<S2>::type US2;
    int64_t i;

    for (i = 0; i < n; i++)
        if (static_cast<US1>(s1[i]) != static_cast<US2>(s2[i]))
            return false;
    return true;
}


template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_sequence(S1 const * CIG_RESTRICT s1,
            S2 const * CIG_RESTRICT s2,
            int64_t sz1,        // length of s1
            int64_t sz2,        // length of s2
            i64 pos,            // start of s1 region
            i64 endpos)         // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos + sz2 > endpos)
        return -1;
    if (sz2 == 0)
        return pos;
    for (int64_t off = pos._v; off <= endpos._v - sz2; off++)
    {
        if (s1[off] == s2[0] && bytes_equal(s1+off,s2,sz2))
            return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
rfind_sequence(S1 const * CIG_RESTRICT s1,
             S2 const * CIG_RESTRICT s2,
             int64_t sz1,       // length of s1
             int64_t sz2,       // length of s2
             i64 pos,           // start of s1 region
             i64 endpos)        // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos + sz2 > endpos)
        return -1;
    if (sz2 == 0)
        return pos;
    for (int64_t off = endpos._v - sz2; off >= pos._v; off--)
    {
        if (s1[off] == s2[0] && bytes_equal(s1+off,s2,sz2))
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_byte(S1 const * CIG_RESTRICT s1,
          int64_t sz1,          // length of s1
          i64 ch,               // byte to find
          i64 pos,              // start of s1 region
          i64 endpos)           // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if (s1[off] == ch)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
rfind_byte(S1 const * CIG_RESTRICT s1,
           int64_t sz1,         // length of s1
           i64 ch,              // byte to find
           i64 pos,             // start of s1 region
           i64 endpos)          // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if (s1[off] == ch)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_notbyte(S1 const * CIG_RESTRICT s1,
             int64_t sz1,       // length of s1
             i64 ch,            // byte to find
             i64 pos,           // start of s1 region
             i64 endpos)        // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if (s1[off] != ch)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
rfind_notbyte(S1 const * CIG_RESTRICT s1,
              int64_t sz1,      // length of s1
              i64 ch,           // byte to find
              i64 pos,          // start of s1 region
              i64 endpos)       // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if (s1[off] != ch)
            return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_firstof_bytelist(S1 const * CIG_RESTRICT s1,
                      S2 const * CIG_RESTRICT s2,
                      int64_t sz1, // length of s1
                      std::size_t sz2, // length of s2
                      i64 pos,     // start of s1 region
                      i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        for (std::size_t i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_lastof_bytelist(S1 const * CIG_RESTRICT s1,
                     S2 const * CIG_RESTRICT s2,
                     int64_t sz1, // length of s1
                     std::size_t sz2, // length of s2
                     i64 pos,     // start of s1 region
                     i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        for (std::size_t i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_firstnotof_bytelist(S1 const * CIG_RESTRICT s1,
                         S2 const * CIG_RESTRICT s2,
                         int64_t sz1, // length of s1
                         std::size_t sz2, // length of s2
                         i64 pos,     // start of s1 region
                         i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        std::size_t i;
        for (i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                break;
        if (i == sz2)
            return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_lastnotof_bytelist(S1 const * CIG_RESTRICT s1,
                        S2 const * CIG_RESTRICT s2,
                        int64_t sz1, // length of s1
                        std::size_t sz2, // length of s2
                        i64 pos,     // start of s1 region
                        i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        std::size_t i;
        for (i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                break;
        if (i == sz2)
            return off;
    }
    return -1;
}


//
// auxiliary for ascii character class
//


template<typename S1>
static inline
i64                             // position found, or -1
find_firstof_acc(S1 const * CIG_RESTRICT s1,
                 int64_t sz1,   // length of s1
                 i64 charclass, // ASCII char class flags, ored
                 i64 pos,       // start of s1 region
                 i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) != 0)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_lastof_acc(S1 const * CIG_RESTRICT s1,
                int64_t sz1,   // length of s1
                i64 charclass, // ASCII char class flags, ored
                i64 pos,       // start of s1 region
                i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) != 0)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_firstnotof_acc(S1 const * CIG_RESTRICT s1,
                    int64_t sz1,   // length of s1
                    i64 charclass, // ASCII char class flags, ored
                    i64 pos,       // start of s1 region
                    i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) == 0)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_lastnotof_acc(S1 const * CIG_RESTRICT s1,
                   int64_t sz1,   // length of s1
                   i64 charclass, // ASCII char class flags, ored
                   i64 pos,       // start of s1 region
                   i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) == 0)
            return off;
    }
    return -1;
}


//
// find
//


i64 bytesval::find(bytesval const &sub,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    return detail::find_sequence(_data(),sub._data(),
                                 sz1,sz2,
                                 pos,sz1);
}

i64 bytesval::find(bytesval const &sub,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    return detail::find_sequence(_data(),sub._data(),
                                 sz1,sz2,
                                 pos,endpos);
}

i64 bytesval::find(i64 singlebyte,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_byte(_data(),sz1,singlebyte,
                             pos,sz1);
}

i64 bytesval::find(i64 singlebyte,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_byte(_data(),sz1,singlebyte,
                             pos,endpos);
}


//
// rfind
//


i64 bytesval::rfind(bytesval const &sub,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    return detail::rfind_sequence(_data(),sub._data(),
                                  sz1,sz2,
                                  pos,sz1);
}

i64 bytesval::rfind(bytesval const &sub,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    return detail::rfind_sequence(_data(),sub._data(),
                                  sz1,sz2,
                                  pos,endpos);
}

i64 bytesval::rfind(i64 singlebyte,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::rfind_byte(_data(),sz1,singlebyte,
                              pos,sz1);
}

i64 bytesval::rfind(i64 singlebyte,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::rfind_byte(_data(),sz1,singlebyte,
                              pos,endpos);
}


//
// find_first_of
//


i64 bytesval::find_first_of(bytesval const &bytelist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_firstof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,sz1);
}

i64 bytesval::find_first_of(bytesval const &bytelist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_firstof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,endpos);
}

i64 bytesval::find_first_of(i64 singlebyte,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_byte(_data(),sz1,singlebyte,
                             pos,sz1);
}

i64 bytesval::find_first_of(i64 singlebyte,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_byte(_data(),sz1,singlebyte,
                             pos,endpos);
}


//
// find_last_of
//


i64 bytesval::find_last_of(bytesval const &bytelist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_lastof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,sz1);
}

i64 bytesval::find_last_of(bytesval const &bytelist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_lastof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,endpos);
}

i64 bytesval::find_last_of(i64 singlebyte,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::rfind_byte(_data(),sz1,singlebyte,
                              pos,sz1);
}

i64 bytesval::find_last_of(i64 singlebyte,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::rfind_byte(_data(),sz1,singlebyte,
                              pos,endpos);
}


//
// find_first_not_of
//


i64 bytesval::find_first_not_of(bytesval const &bytelist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_firstnotof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,sz1);
}

i64 bytesval::find_first_not_of(bytesval const &bytelist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_firstnotof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,endpos);
}

i64 bytesval::find_first_not_of(i64 singlebyte,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_notbyte(_data(),sz1,singlebyte,
                                pos,sz1);
}

i64 bytesval::find_first_not_of(i64 singlebyte,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_notbyte(_data(),sz1,singlebyte,
                                pos,endpos);
}


//
// find_last_not_of
//


i64 bytesval::find_last_not_of(bytesval const &bytelist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_lastnotof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,sz1);
}

i64 bytesval::find_last_not_of(bytesval const &bytelist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =bytelist._get_size();
    return detail::find_lastnotof_bytelist(
        _data(),bytelist._data(),
        sz1,sz2,
        pos,endpos);
}

i64 bytesval::find_last_not_of(i64 singlebyte,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::rfind_notbyte(_data(),sz1,singlebyte,
                                 pos,sz1);
}

i64 bytesval::find_last_not_of(i64 singlebyte,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::rfind_notbyte(_data(),sz1,singlebyte,
                                 pos,endpos);
}


//
// find by ascii character class
//


i64 bytesval::find_first_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_firstof_acc(_data(),sz1,accmask,
                                    pos,sz1);
}

i64 bytesval::find_first_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_firstof_acc(_data(),sz1,accmask,
                                    pos,endpos);
}

i64 bytesval::find_last_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_lastof_acc(_data(),sz1,accmask,
                                   pos,sz1);
}

i64 bytesval::find_last_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_lastof_acc(_data(),sz1,accmask,
                                   pos,endpos);
}

i64 bytesval::find_first_not_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_firstnotof_acc(_data(),sz1,accmask,
                                       pos,sz1);
}

i64 bytesval::find_first_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_firstnotof_acc(_data(),sz1,accmask,
                                       pos,endpos);
}

i64 bytesval::find_last_not_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_lastnotof_acc(_data(),sz1,accmask,
                                      pos,sz1);
}

i64 bytesval::find_last_not_in_acc(i64 accmask,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    return detail::find_lastnotof_acc(_data(),sz1,accmask,
                                      pos,endpos);
}


//
// Specializations
//


bool bytesval::contains(bytesval const &sub) const noexcept
{
    return find(sub) != -1;
}

bool bytesval::starts_with(bytesval const &sub) const noexcept
{
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    
    return find(sub,0,sz2) != -1;
}

bool bytesval::ends_with(bytesval const &sub) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    
    return find(sub,sz1-sz2) != -1;
}



} // namespace detail

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
