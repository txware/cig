/* cig_ast_node.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - AST node
 */
#include "cig_ast_node.h"
#include "cig_ast_literal.h"

#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_vector.h"
#include "cig_map.h"


#define EXPRESSIONS_HAVE_BACKREFS 0


// Without a static or a nonstatic specification,
// what is the default?
// Do not change.
#define DEFAULT_CLASS_IS_STATIC true      // C++ knows only static classes
#define DEFAULT_VARIABLE_IS_STATIC false  // C++ member variables are nonstatic
#define DEFAULT_FUNCTION_IS_STATIC false  // C++ member functions are nonstatic



namespace cig {

namespace ast {





/**********************************************************************/
/*                                                                    */
/*                    CLASSIFICATION OF NODE TYPES                    */
/*                                                                    */
/**********************************************************************/





#define AST_NODE_UNARY_EXPR_CASES               \
         AST_EXPR_SIGNPLUS:                     \
    case AST_EXPR_SIGNMINUS:                    \
    case AST_EXPR_BIT_NOT:                      \
    case AST_EXPR_LOGICAL_NOT

#define AST_NODE_BINARY_EXPR_CASES              \
         AST_EXPR_MULT:                         \
    case AST_EXPR_DIV:                          \
    case AST_EXPR_MOD:                          \
    case AST_EXPR_ADD:                          \
    case AST_EXPR_SUB:                          \
    case AST_EXPR_SHIFT_LEFT:                   \
    case AST_EXPR_SHIFT_RIGHT:                  \
    case AST_EXPR_LESS:                         \
    case AST_EXPR_GREATER:                      \
    case AST_EXPR_LESS_EQUAL:                   \
    case AST_EXPR_GREATER_EQUAL:                \
    case AST_EXPR_EQUAL:                        \
    case AST_EXPR_NOT_EQUAL:                    \
    case AST_EXPR_IN:                           \
    case AST_EXPR_AND:                          \
    case AST_EXPR_XOR:                          \
    case AST_EXPR_OR:                           \
    case AST_EXPR_LOGICAL_AND:                  \
    case AST_EXPR_LOGICAL_OR:                   \
    case AST_EXPR_ADD_ASSIGN:                   \
    case AST_EXPR_SUB_ASSIGN:                   \
    case AST_EXPR_MULT_ASSIGN:                  \
    case AST_EXPR_DIV_ASSIGN:                   \
    case AST_EXPR_MOD_ASSIGN:                   \
    case AST_EXPR_SHL_ASSIGN:                   \
    case AST_EXPR_SHR_ASSIGN:                   \
    case AST_EXPR_AND_ASSIGN:                   \
    case AST_EXPR_XOR_ASSIGN:                   \
    case AST_EXPR_OR_ASSIGN:                    \
    case AST_EXPR_DOT_ASSIGN:                   \
    case AST_EXPR_ASSIGN

#define AST_NODE_TERNARY_EXPR_CASES             \
         AST_EXPR_CONDITION

#define AST_NODE_EXPR_CASES                     \
         AST_EXPRMIN:                           \
    case AST_EXPR_LITERAL:                      \
    case AST_EXPR_ENTITYLINK:                   \
    case AST_EXPR_MEMBERLINK:                   \
    case AST_EXPR_UNQUALNAME:                   \
    case AST_EXPR_MEMBER:                       \
    case AST_EXPR_ARRAYDEREF:                   \
    case AST_EXPR_SLICE:                        \
    case AST_EXPR_FUNCALL:                      \
    case AST_NODE_UNARY_EXPR_CASES:             \
    case AST_NODE_BINARY_EXPR_CASES:            \
    case AST_NODE_TERNARY_EXPR_CASES:           \
    case AST_EXPR_LIST:                         \
    case AST_EXPRMAX


#define AST_NODE_STMT_CASES                     \
         AST_STMTMIN:                           \
    case AST_COMPOUNDSTMT:                      \
    case AST_RETURNSTMT:                        \
    case AST_EXPRSTMT:                          \
    case AST_IFSTMT:                            \
    case AST_WHILESTMT:                         \
    case AST_DOWHILESTMT:                       \
    case AST_FORSTMT:                           \
    case AST_FOROFSTMT:                         \
    case AST_CONTINUESTMT:                      \
    case AST_BREAKSTMT:                         \
    case AST_STMTMAX



cig_bool ast_node::ast_node_type_is_expr(ast_node_type t) noexcept {
    using enum ast::ast_node_type;

    return (t > AST_EXPRMIN && t < AST_EXPRMAX) || t == AST_VARIABLE;
}

cig_bool ast_node::ast_node_type_is_assign_expr(ast_node_type t) noexcept {
    using enum ast::ast_node_type;

    return t >= AST_EXPR_ADD_ASSIGN && t <= AST_EXPR_ASSIGN;
}

cig_bool ast_node::ast_node_type_is_stmt(ast_node_type t) noexcept {
    using enum ast::ast_node_type;

    return t > AST_STMTMIN && t < AST_STMTMAX;
}

cig_bool ast_node::ast_node_type_has_backref(ast_node_type t) noexcept
{
    using enum ast::ast_node_type;

    switch (t)
    {
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_COMPOUNDSTMT:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    case AST_FOROFSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
        return true;

    case AST_INVALID:
    case AST_GLOBAL:
    case AST_TYPEREF:
    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_STMTMIN:
    case AST_STMTMAX:
    case AST_MAX:
        return false;
    }
    return false;
}


str ast_node::ast_node_type_tag(ast_node_type t) noexcept
{
    using enum ast::ast_node_type;

    switch (t)
    {
    case AST_INVALID:                  return "AST_INVALID"_str;
    case AST_GLOBAL:                   return "AST_GLOBAL"_str;
    case AST_NAMESPACE:                return "AST_NAMESPACE"_str;
    case AST_CLASSDECL:                return "AST_CLASSDECL"_str;
    case AST_CLASSDEF:                 return "AST_CLASSDEF"_str;
    case AST_ENUMDECL:                 return "AST_ENUMDECL"_str;
    case AST_ENUMDEF:                  return "AST_ENUMDEF"_str;
    case AST_ENUMVALUE:                return "AST_ENUMVALUE"_str;
    case AST_VARIABLE:                 return "AST_VARIABLE"_str;
    case AST_OVERLOADSET:              return "AST_OVERLOADSET"_str;
    case AST_FUNDECL:                  return "AST_FUNDECL"_str;
    case AST_FUNDEF:                   return "AST_FUNDEF"_str;
    case AST_LAMBDA:                   return "AST_LAMBDA"_str;
    case AST_TYPEREF:                  return "AST_TYPEREF"_str;
    case AST_FUNPAR:                   return "AST_FUNPAR"_str;
    case AST_FUNPARLIST:               return "AST_FUNPARLIST"_str;
    case AST_FUNRET:                   return "AST_FUNRET"_str;
    case AST_FUNRETLIST:               return "AST_FUNRETLIST"_str;
    case AST_FUNBODY:                  return "AST_FUNBODY"_str;
    case AST_EXPRMIN:                  return "AST_EXPRMIN"_str;
    case AST_EXPR_LITERAL:             return "AST_EXPR_LITERAL"_str;
    case AST_EXPR_ENTITYLINK:          return "AST_EXPR_ENTITYLINK"_str;
    case AST_EXPR_MEMBERLINK:          return "AST_EXPR_MEMBERLINK"_str;
    case AST_EXPR_UNQUALNAME:          return "AST_EXPR_UNQUALNAME"_str;
    case AST_EXPR_MEMBER:              return "AST_EXPR_MEMBER"_str;
    case AST_EXPR_ARRAYDEREF:          return "AST_EXPR_ARRAYDEREF"_str;
    case AST_EXPR_SLICE:               return "AST_EXPR_SLICE"_str;
    case AST_EXPR_FUNCALL:             return "AST_EXPR_FUNCALL"_str;
    case AST_EXPR_SIGNPLUS:            return "AST_EXPR_SIGNPLUS"_str;
    case AST_EXPR_SIGNMINUS:           return "AST_EXPR_SIGNMINUS"_str;
    case AST_EXPR_BIT_NOT:             return "AST_EXPR_BIT_NOT"_str;
    case AST_EXPR_LOGICAL_NOT:         return "AST_EXPR_LOGICAL_NOT"_str;
    case AST_EXPR_MULT:                return "AST_EXPR_MULT"_str;
    case AST_EXPR_DIV:                 return "AST_EXPR_DIV"_str;
    case AST_EXPR_MOD:                 return "AST_EXPR_MOD"_str;
    case AST_EXPR_ADD:                 return "AST_EXPR_ADD"_str;
    case AST_EXPR_SUB:                 return "AST_EXPR_SUB"_str;
    case AST_EXPR_SHIFT_LEFT:          return "AST_EXPR_SHIFT_LEFT"_str;
    case AST_EXPR_SHIFT_RIGHT:         return "AST_EXPR_SHIFT_RIGHT"_str;
    case AST_EXPR_LESS:                return "AST_EXPR_LESS"_str;
    case AST_EXPR_GREATER:             return "AST_EXPR_GREATER"_str;
    case AST_EXPR_LESS_EQUAL:          return "AST_EXPR_LESS_EQUAL"_str;
    case AST_EXPR_GREATER_EQUAL:       return "AST_EXPR_GREATER_EQUAL"_str;
    case AST_EXPR_EQUAL:               return "AST_EXPR_EQUAL"_str;
    case AST_EXPR_NOT_EQUAL:           return "AST_EXPR_NOT_EQUAL"_str;
    case AST_EXPR_IN:                  return "AST_EXPR_IN"_str;
    case AST_EXPR_AND:                 return "AST_EXPR_AND"_str;
    case AST_EXPR_XOR:                 return "AST_EXPR_XOR"_str;
    case AST_EXPR_OR:                  return "AST_EXPR_OR"_str;
    case AST_EXPR_LOGICAL_AND:         return "AST_EXPR_LOGICAL_AND"_str;
    case AST_EXPR_LOGICAL_OR:          return "AST_EXPR_LOGICAL_OR"_str;
    case AST_EXPR_CONDITION:           return "AST_EXPR_CONDITION"_str;
    case AST_EXPR_LIST:                return "AST_EXPR_LIST"_str;
    case AST_EXPR_ADD_ASSIGN:          return "AST_EXPR_ADD_ASSIGN"_str;
    case AST_EXPR_SUB_ASSIGN:          return "AST_EXPR_SUB_ASSIGN"_str;
    case AST_EXPR_MULT_ASSIGN:         return "AST_EXPR_MULT_ASSIGN"_str;
    case AST_EXPR_DIV_ASSIGN:          return "AST_EXPR_DIV_ASSIGN"_str;
    case AST_EXPR_MOD_ASSIGN:          return "AST_EXPR_MOD_ASSIGN"_str;
    case AST_EXPR_SHL_ASSIGN:          return "AST_EXPR_SHL_ASSIGN"_str;
    case AST_EXPR_SHR_ASSIGN:          return "AST_EXPR_SHR_ASSIGN"_str;
    case AST_EXPR_AND_ASSIGN:          return "AST_EXPR_AND_ASSIGN"_str;
    case AST_EXPR_XOR_ASSIGN:          return "AST_EXPR_XOR_ASSIGN"_str;
    case AST_EXPR_OR_ASSIGN:           return "AST_EXPR_OR_ASSIGN"_str;
    case AST_EXPR_DOT_ASSIGN:          return "AST_EXPR_DOT_ASSIGN"_str;
    case AST_EXPR_ASSIGN:              return "AST_EXPR_ASSIGN"_str;
    case AST_EXPRMAX:                  return "AST_EXPRMAX"_str;
    case AST_STMTMIN:                  return "AST_STMTMIN"_str;
    case AST_COMPOUNDSTMT:             return "AST_COMPOUNDSTMT"_str;
    case AST_RETURNSTMT:               return "AST_RETURNSTMT"_str;
    case AST_EXPRSTMT:                 return "AST_EXPRSTMT"_str;
    case AST_IFSTMT:                   return "AST_IFSTMT"_str;
    case AST_WHILESTMT:                return "AST_WHILESTMT"_str;
    case AST_DOWHILESTMT:              return "AST_DOWHILESTMT"_str;
    case AST_FORSTMT:                  return "AST_FORSTMT"_str;
    case AST_FOROFSTMT:                return "AST_FOROFSTMT"_str;
    case AST_BREAKSTMT:                return "AST_BREAKSTMT"_str;
    case AST_CONTINUESTMT:             return "AST_CONTINUESTMT"_str;
    case AST_STMTMAX:                  return "AST_STMTMAX"_str;
    case AST_MAX:                      return "AST_MAX"_str;
    }
    return ""_str;
}



/**********************************************************************/
/*                                                                    */
/*              AUXILIARY CLASSES FOR AST NODE CONTENTS               */
/*                                                                    */
/**********************************************************************/




namespace detail {


//
// Simple back reference
//
struct simple_backref {

    // The back reference is a weak pointer pointing back to the
    // node (co-)owning this node.
    // There can be only one such owning node for a backref.
    // A node that is co-owned by multiple owners needs to have
    // multiple backref structures inside, one for each kind
    // of ownership that needs to be backtracked.

    std::weak_ptr<ast_node> ptr; // pointer to node with subnode_list, or 0


    // A function to check whether the ptr is zero
    // (not set, as opposed to expired)
    bool is_empty() const {
        using wt = std::weak_ptr<ast_node>;
        return !ptr.owner_before(wt{}) && !wt{}.owner_before(ptr);
    }
    void reset() {
        ptr.reset();
    }
    exp_none set(ast_node_ptr const &p) {

        // Check argument
        if (p == nullptr)
            return makeerr(err::EC_ASTNODE_BACKREF_BADPOINTER);

        // Cannot set two back-references
        if (!is_empty())
            return makeerr(err::EC_ASTNODE_BACKREF_AMBIGUOUS);

        ptr = p;
        return exp_none();
    }
    ast_node_ptr get() const {
        return ptr.lock();
    }
};


//
// Back reference with index
//
struct indexed_backref {

    // The back reference points back to the subnode list
    // owning this node.
    // There can be only one such occurrence.
    // The 

    simple_backref ptr;         // backref without index
    i64 index = -1;             // index into subnode_list, or -1

    indexed_backref() : index(-1) { }
    bool is_empty() const {
        cig_assert((index != -1) == (index >= 0));
        cig_assert(bool((index == -1)) == ptr.is_empty());
        return index == -1;
    }
    void reset() { ptr.reset(); index = -1; }
    exp_none set(ast_node_ptr const &p, i64 i) {

        // Check argument
        if (i == -1)            // codes < -1 have special meaning
            return makeerr(err::EC_ASTNODE_BACKREF_BADINDEX);
        if (p == nullptr)
            return makeerr(err::EC_ASTNODE_BACKREF_BADPOINTER);

        // Cannot set two back-references
        if (index >= 0)
            return makeerr(err::EC_ASTNODE_BACKREF_AMBIGUOUS);
        cig_assert(ptr.set(p)); // must fail if index >= 0
        index = i;
        return exp_none();
    }
    ast_node_backref get() const {
        ast_node_ptr p = ptr.get();
        return { p, index };
    }
};


//
// Dictionary for name lookup
//
struct name_dict : cig::map<str,i64> {

    i64 lookup_name(str const &nm) const noexcept {
        auto entry = find(nm);
        if (entry != end())
            return entry->second;
        else
            return -1;
    }

    exp_none add_lookup_name(str const &nm,i64 index) {
        auto entry = find(nm);
        if (entry != end())
            return makeerr(err::EC_DUPLICATE_NAME);
        insert(std::pair(nm,index));
        return exp_none();
    }
};


//
// Subnode list
//
struct subnode_list : cig::vector<ast_node_ptr> {

    // Defined below ast_node_br
    exp_i64 add_subnode(ast_node_ptr nd,ast_node_ptr const &thisnode);

    exp_i64 add_subnode_without_backref(ast_node_ptr nd);

    i64 get_count() const noexcept {
        std::size_t sz = size();
        i64 szret = static_cast<int64_t>(sz);
        return szret;
    }

    ast_node_ptr get_subnode(i64 index) const noexcept {
        std::size_t sz = size();
        
        if (index < 0)
            return nullptr;
        std::size_t i = static_cast<std::size_t>(index._v);
        if (i >= sz)
            return nullptr;
        return operator[](i);
    }
};


//
// Global subnode list
//
struct global_list : cig::vector<ast_node_ptr> {

    i64 get_count() const noexcept {
        std::size_t sz = size();
        i64 szret = static_cast<int64_t>(sz);
        return szret;
    }

    exp_i64 add_subnode(ast_node_ptr nd)
    {
        i64 szret = get_count();
        push_back(nd);
        return szret;
    }

    ast_node_ptr get_subnode(i64 index) const noexcept {
        std::size_t sz = size();
        
        if (index < 0)
            return nullptr;
        std::size_t i = static_cast<std::size_t>(index._v);
        if (i >= sz)
            return nullptr;
        return operator[](i);
    }
};


//
// Overload list
//
struct overload_list : cig::vector<ast_node_ptr> {

    exp_i64 add_overload(ast_node_ptr nd,ast_node_ptr const &thisnode);

    i64 get_count() const noexcept {
        std::size_t sz = size();
        i64 szret = static_cast<int64_t>(sz);
        return szret;
    }

    ast_node_ptr get_function(i64 index) const noexcept {
        std::size_t sz = size();
        
        if (index < 0)
            return nullptr;
        std::size_t i = static_cast<std::size_t>(index._v);
        if (i >= sz)
            return nullptr;
        return operator[](i);
    }
};


//
// Resolved entity structure for storage inside AST nodes
//
struct resolved_entity {
    using enum ast::ast_resolve_quality;

    // The resolved entity is a weak pointer.

    std::weak_ptr<ast_node> ptr;
    ast_resolve_quality quality = AST_RQ_INVALID;

    // A function to check whether the ptr is zero
    // (not set, as opposed to expired)
    bool is_empty() const {
        using wt = std::weak_ptr<ast_node>;
        return !ptr.owner_before(wt{}) && !wt{}.owner_before(ptr);
    }
    void reset() {
        ptr.reset();
    }
    // Set can be repeated with the same pointer
    // but for replacement, replace must be set
    exp_none set(ast_node_ptr const &p,
                 ast_resolve_quality qu,
                 cig::source_location const &loc,
                 bool replace = false) {

        // Check argument
        if (p == nullptr)
            return makeerr(err::EC_RESOLVED_ENTITY_IS_NULL,loc);

        // Cannot set two conflicting locations except for replace
        if (!is_empty() && !replace)
        {
            if (ptr.lock() != p)
                return makeerr(err::EC_RESOLVED_ENTITY_ALREADY_SET,loc);
        }

        ptr = p;
        quality = qu;
        return exp_none();
    }
    ast_node_ptr get_node() const {
        return ptr.lock();
    }
    ast_resolve_quality get_quality() const noexcept {
        return quality;
    }
};



static const str empty_string = ""_str;
static const literal empty_literal;
static const ast_type_info empty_type_info;
static const ast_variable_info empty_variable_info;
static const ast_function_info empty_function_info;
static const ast_expression_info empty_expression_info;
static const ast_global_info empty_global_info;


// Function prototypes
exp_none
ast_node_recursive_variable_registration(
    ast_node_ptr scope,         // scope where to add the variable declarations
    ast_node_ptr expr           // expression with subexpressions; can be null
    );


} // namespace detail






/**********************************************************************/
/*                                                                    */
/*                   BASE CLASSES OF AST NODE TYPES                   */
/*                                                                    */
/**********************************************************************/



struct ast_node_global;


//
// AST node with extra elements, hidden from public interface
//
struct ast_node_x : ast_node {
public:

    // X-ored constants for x

    // Ownership.
    //
    // The bits indicate various types of ownership (shared_ptr linkage)
    // in the AST tree.


    // X_OWNED:
    //
    // This flag indicates that the primary ownership of an AST node has
    // been set, i.e. the node is linked by its owner.
    //
    // Namespace, class, enum:
    // The primary ownership is the link from a owning entity, typically
    // the encapsulating scope of a namespace, class, enum, variable,
    // or function. For class and enum this is true for both declaration
    // and definition.
    //
    // Function:
    // For functions, the fundef and fundecl nodes are also owned by a
    // class or namespace scope; however, for member function definitions
    // outside the class definition, the 'owning' scope of the definition
    // is the scope and place where the function is defined.
    // This is the place that sets the X_OWNED flag and that holds the
    // subnode link to the fundef.
    // The various components of a function definition or declaration are
    // owned by the fundef or fundecl AST node.
    // The 'overloadset' AST node is a special AST node that contains the
    // set of all overloads of a function of that name. It is owned by the
    // enclosing scope of the declaration.
    // (The links from the overloadset to the fundecl or fundef are not
    // primary ownership, see below.)
    //
    // Variable:
    // Variables defined in a class are owned by the class scope.
    // However, a variable declaration within a function happens in an
    // expression context, and the primary ownership is held by the
    // owning expression or statement.
    // The link from the compound statement to the variable is not the
    // primary owner, it is just created for convenience (to have a
    // strict sorting of the variables in that scope).
    //
    // Expression:
    // For expressions, primary ownership is held by the superexpression
    // or the statement that immediately contains the expression.
    //
    // Statement:
    // For statements, primary ownership is held by the encapsulating
    // compound statement. The topmost compound statement is owned by the
    // function body AST node.
    //
    // X_LINKED_BY_OVERLOAD:
    //
    // This flag indicates that the link from the overload set to the
    // fundecl or fundef is set.
    // Note that the overload set links only to the first declaration
    // or definition of a function. If a declaration is followed by a
    // later definition, the definition is not linked directly from the
    // overload set. It also has no backref to the overload set.
    // The overload set can only be reached via the fundecl.
    //
    // X_LINKED_BY_DECL
    // 
    // This flag indicates that a link from a declaration is set to the
    // definition.
    // Additionally, as a special case, for a variable declaration
    // inside a function, it indicates that the variable declaration
    // is also directly linked from the enclosing compound statement.
    

    static constexpr u64 X_OWNED =              0x1_u64;
    static constexpr u64 X_LINKED_BY_OVERLOAD = 0x2_u64;
    static constexpr u64 X_LINKED_BY_DECL =     0x4_u64;

#define CHECK_SLOT(testptr,testnd)                              \
    do {                                                        \
        if (testptr != nullptr)                                 \
        {                                                       \
            return makeerr(err::EC_SLOT_TO_ADD_NOT_FREE,        \
                           testnd->effectloc);                  \
        }                                                       \
    } while (0)

#define SET_SLOT(testptr,testnd)                                \
    do {                                                        \
        ast_node_ptr &ptrx = testptr;                           \
        if (testptr != nullptr)                                 \
        {                                                       \
            return makeerr(err::EC_SLOT_TO_ADD_NOT_FREE,        \
                           testnd->effectloc);                  \
        }                                                       \
        ptrx = testnd;                                          \
    } while (0)

#define ENSURE_NOT_YET_OWNED(testnd)                                    \
    do {                                                                \
        ast_node_x const *ndx =                                         \
            static_cast<ast_node_x const *>(testnd.get());              \
        if ((ndx->x & ast_node_x::X_OWNED) != 0)                        \
        {                                                               \
            return makeerr(err::EC_NODE_TO_ADD_ALREADY_OWNED,           \
                           testnd->effectloc);                          \
        }                                                               \
    } while(0)
#define ENSURE_NOT_YET_LINKED_FROM_OVERLOADSET(testnd)                  \
    do {                                                                \
        ast_node_x const *ndx =                                         \
            static_cast<ast_node_x const *>(testnd.get());              \
        if ((ndx->x & ast_node_x::X_LINKED_BY_OVERLOAD) != 0)                        \
        {                                                               \
            return makeerr(err::EC_NODE_TO_ADD_ALREADY_IN_OVERLOADSET,  \
                           testnd->effectloc);                          \
        }                                                               \
    } while(0)
#define ENSURE_NOT_YET_LINKED_FROM_DECL(testnd)                         \
    do {                                                                \
        ast_node_x const *ndx =                                         \
            static_cast<ast_node_x const *>(testnd.get());              \
        if ((ndx->x & ast_node_x::X_LINKED_BY_DECL) != 0)               \
        {                                                               \
            return makeerr(err::EC_NODE_TO_ADD_ALREADY_LINKED_BY_DECL,  \
                           testnd->effectloc);                          \
        }                                                               \
    } while(0)
#define SET_OWNED(testnd)                                               \
    do {                                                                \
        ast_node_x *ndx =                                               \
            static_cast<ast_node_x *>(testnd.get());                    \
        ndx->x |= ast_node_x::X_OWNED;                                  \
    } while(0)
#define SET_LINKED_FROM_OVERLOADSET(testnd)                             \
    do {                                                                \
        ast_node_x *ndx =                                               \
            static_cast<ast_node_x *>(testnd.get());                    \
        ndx->x |= ast_node_x::X_LINKED_BY_OVERLOAD;                     \
    } while(0)
#define SET_LINKED_FROM_DECL(testnd)                                    \
    do {                                                                \
        ast_node_x *ndx =                                               \
            static_cast<ast_node_x *>(testnd.get());                    \
        ndx->x |= ast_node_x::X_LINKED_BY_DECL;                         \
    } while(0)


    u64 x;                      // Bits for internal use in this source file

    ast_node_x(ast_node_type t) : ast_node(t) { }
    exp_i64 add_named_entity_aux(ast_node_ptr entity,bool with_var)
    {
        return ast_node::add_named_entity_aux(entity,with_var);
    }


    // Value type.
    //
    // The bits indicate various types of expression values that
    // cannot be easily mixed:
    // Lists cannot be nested. One flat list is the maximum.
    // Uninitialized lvalues and rvalues cannot be mixed in a list.
    // An uninitialized lvalue is a freshly declared variable.
    // An expression with an rvalue cannot be on the lefthand side
    // of an assignment operator.
    // An expression with an uninitialized lvalue cannot be on the
    // right hand side of an assignment, or in a function argument list,
    // or in a return statement, or in an array dereference or
    // function call or most arithmetic operations.
    // An expression with an uninitialized lvalue can also not be
    // mixed with a conditional.

    // X_HAS_LISTVALUE   ... expression subtree has list values
    // X_HAS_ULVALUE     ... expression subtree has uninitialized lvaluds (vars)
    // X_HAS_RVALUE      ... expression subtree has an rvalue
    // X_HAS_CONDITIONAL ... expression contains a conditional ? :

    static constexpr u64 X_HAS_LISTVALUE =   0x08_u64;
    static constexpr u64 X_HAS_ULVALUE =     0x10_u64;
    static constexpr u64 X_HAS_RVALUE =      0x20_u64;
    static constexpr u64 X_HAS_CONDITIONAL = 0x40_u64;

#define SET_LISTVALUE(testnd)                           \
    do {                                                \
        ast_node_x *ndx =                               \
            static_cast<ast_node_x *>(testnd.get());    \
        ndx->x |= ast_node_x::X_HAS_LISTVALUE;          \
    } while(0)
#define SET_ULVALUE(testnd)                             \
    do {                                                \
        ast_node_x *ndx =                               \
            static_cast<ast_node_x *>(testnd.get());    \
        ndx->x |= ast_node_x::X_HAS_ULVALUE;            \
    } while(0)
#define SET_RVALUE(testnd)                              \
    do {                                                \
        ast_node_x *ndx =                               \
            static_cast<ast_node_x *>(testnd.get());    \
        ndx->x |= ast_node_x::X_HAS_RVALUE;             \
    } while(0)
#define SET_CONDITIONAL(testnd)                         \
    do {                                                \
        ast_node_x *ndx =                               \
            static_cast<ast_node_x *>(testnd.get());    \
        ndx->x |= ast_node_x::X_HAS_CONDITIONAL;        \
    } while(0)
#define TRANSFER_VALUEKIND(testnd)                      \
    do {                                                \
        ast_node_x *thisdst =                           \
            static_cast<ast_node_x *>(this);            \
        ast_node_x const *ndx =                         \
            static_cast<ast_node_x *>(testnd.get());    \
        thisdst->x |= (ndx->x &                         \
                      (ast_node_x::X_HAS_LISTVALUE |    \
                       ast_node_x::X_HAS_ULVALUE |      \
                       ast_node_x::X_HAS_RVALUE |       \
                       ast_node_x::X_HAS_CONDITIONAL)); \
    } while(0)

#define NODE_IS_LISTVALUE(testnd)                               \
    ((static_cast<ast_node_x const *>(testnd.get())->x &        \
      ast_node_x::X_HAS_LISTVALUE)                              \
     != 0)
#define NODE_IS_ULVALUE(testnd)                                 \
    ((static_cast<ast_node_x const *>(testnd.get())->x &        \
      ast_node_x::X_HAS_ULVALUE)                                \
     != 0)
#define NODE_IS_RVALUE(testnd)                                  \
    ((static_cast<ast_node_x const *>(testnd.get())->x &        \
      ast_node_x::X_HAS_RVALUE)                                 \
     != 0)
#define NODE_IS_CONDITIONAL(testnd)                             \
    ((static_cast<ast_node_x const *>(testnd.get())->x &        \
      ast_node_x::X_HAS_CONDITIONAL)                            \
     != 0)

};


//
// AST node with backref as base class for types with an indexed_backref
//
struct ast_node_br : ast_node_x {

protected:
    detail::indexed_backref ibr;

public:
    ast_node_br(ast_node_type t) : ast_node_x(t) { }
    exp_none set_backref(ast_node_ptr nd,i64 index) noexcept {
        return ibr.set(nd,index);
    }

    // An auxiliary function to trace namespaces back to global
    ast_node_global *get_global_from_namespace() noexcept
    {
        using enum ast::ast_node_type;

        ast_node_br *nmsp = this;
        for (;;)
        {
            ast_node_type nodetype = nmsp->node_type;
            if (nodetype == AST_GLOBAL)
                return reinterpret_cast<ast_node_global*>(nmsp);
            if (nodetype != AST_NAMESPACE)
                return nullptr;

            auto backref = nmsp->ibr.get();
            ast_node_ptr nd = std::get<0>(backref);

            if (nd == nullptr)
                return nullptr;
            nmsp = static_cast<ast_node_br *>(nd.get());
            if (nmsp == nullptr)
                return nullptr;
        }
    }

    friend struct ast_node;
};

// Define add_subnode of previous class
exp_i64 detail::subnode_list::add_subnode(ast_node_ptr nd,
                                          ast_node_ptr const &thisnode) {
    if (!ast_node::ast_node_type_has_backref(nd->get_node_type()))
        return makeerr(err::EC_NODE_HAS_NO_BACKREF);
    i64 szret = get_count();
    auto exp = static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,szret);
    if (!exp)
        return forwarderr(exp);
    push_back(nd);
    return szret;
}

// Define add_subnode of previous class
exp_i64 detail::subnode_list::add_subnode_without_backref(ast_node_ptr nd) {
    i64 szret = get_count();
    push_back(nd);
    return szret;
}





//
// Lambda base: Base class for lambda, fundef, fundecl
//
struct ast_node_lambdabase : ast_node_br {
    // A function with the same signature may be declared
    // multiple times. We keep the first declaration in a fundecl
    // if it comes before the definition, and discard the other
    // declarations after checking their congruence.
    // We also keep the function definition but no declaration
    // after the definition.
    // Declaration and definition must be congruent in their
    // parameter and return type lists.
    //
    // The backref from ast_node_br always references back to the
    // place where the declaration/definition/lambda has been seen.
    //
    // The other back-references (fundef to fundecl, both to overloadset)
    // are added in the functionbase class below.

    ast_node_ptr funparlist;
    ast_node_ptr funretlist;
    ast_node_ptr funbody;       // For a function declaration, empty
    ast_node_lambdabase(ast_node_type t) : ast_node_br(t) { }
};



//
// Function base: Base class for fundef, fundecl
//
struct ast_node_functionbase : ast_node_lambdabase {
    i64 flags;                  // Flags as in ast_node::ast_node_flags
    str name;                   // Function name
    detail::indexed_backref global_backref; // Global backref if in a namespace
    detail::indexed_backref overloadset_backref;
    ast_node_functionbase(ast_node_type t) : ast_node_lambdabase(t) { }
    exp_none set_overloadset_backref(ast_node_ptr nd,i64 index) noexcept;
};


//
// Expression base: Base class for expressions
//
struct ast_node_expressionbase : ast_node_x {
public:
    ast_expression_info ei;      // Expression info
    detail::resolved_entity rve; // Resolved entity
    ast_node_expressionbase(ast_node_type t) : ast_node_x(t) { }
};



/**********************************************************************/
/*                                                                    */
/*                           AST NODE TYPES                           */
/*                                                                    */
/**********************************************************************/



struct ast_node_global : ast_node_br {
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    detail::global_list globallist;   // List of subnodes in all namespaces
    ast_global_info gi;               // Global info
    ast_node_global() : ast_node_br(ast_node_type::AST_GLOBAL) { }

    exp_i64 add_global_subnode(ast_node_ptr nd);
};


struct ast_node_namespace : ast_node_br {
    str name;                         // Name in enclosing namespace
    detail::indexed_backref global_backref; // Global namespace
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_namespace() : ast_node_br(ast_node_type::AST_NAMESPACE) { }
};


struct ast_node_classdecl : ast_node_br {
    i64 flags;                        // Flags as in ast_node::ast_node_flags
    str name;                         // Name in enclosing class or namespace
    detail::indexed_backref global_backref; // Global backref if in a namespace
    ast_node_ptr classdef;            // Co-owning pointer to class definition
    ast_node_classdecl() : ast_node_br(ast_node_type::AST_CLASSDECL) { }
};


struct ast_node_classdef : ast_node_br {
    i64 flags;                        // Flags as in ast_node::ast_node_flags
    str name;                         // Name in enclosing class or namespace
    detail::indexed_backref global_backref; // Global backref if in a namespace
    detail::simple_backref classdecl; // Back-reference to class declaration
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_type_info type_info;          // Type info for class
    i64 typeidx = -1;                 // Datatype index for interpreter, or -1
    ast_node_classdef() : ast_node_br(ast_node_type::AST_CLASSDEF) { }
};


struct ast_node_enumdecl : ast_node_br {
    str name;                         // Name in enclosing class or namespace
    detail::indexed_backref global_backref; // Global backref if in a namespace
    ast_node_ptr enumdef;             // Co-owning pointer to enum definition
    ast_node_enumdecl() : ast_node_br(ast_node_type::AST_ENUMDECL) { }
};


struct ast_node_enumdef : ast_node_br {
    str name;                         // Name in enclosing class or namespace
    detail::indexed_backref global_backref; // Global backref if in a namespace
    detail::simple_backref enumdecl;  // Back-reference to enum declaration
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_type_info type_info;          // Type info for class
    i64 typeidx = -1;                 // Datatype index for interpreter, or -1
    ast_node_enumdef() : ast_node_br(ast_node_type::AST_ENUMDEF) { }
};


struct ast_node_enumvalue : ast_node_br {
    str name;                   // Name of enum constant in enclosing enum
    literal litvalue;           // Literal value of enum constant
    ast_variable_info variable_info;  // Variable info for literal value
    ast_node_enumvalue() : ast_node_br(ast_node_type::AST_ENUMVALUE) { }
};


struct ast_node_variable : ast_node_br {
    ast_node_ptr typeref;       // Reference to a data type
    i64 flags;                  // Flags as in ast_node::ast_node_flags
    str name;                   // Name in enclosing class/namespace/block
    detail::indexed_backref global_backref; // Global backref if in a namespace
    detail::resolved_entity rve; // Resolved entity
    ast_variable_info variable_info; // Variable info for variable
    ast_node_variable() : ast_node_br(ast_node_type::AST_VARIABLE) {
        // In expression context, a variable
        // is an ULVALUE (uninitialized lvalue)
        x = X_HAS_ULVALUE;
    }
};


struct ast_node_overloadset : ast_node_br {
    str name;                   // Name in enclosing class or namespace
    detail::overload_list fnlist; // List of functions (overloads)
    ast_node_overloadset() : ast_node_br(ast_node_type::AST_OVERLOADSET) { }
};


struct ast_node_fundecl : ast_node_functionbase {
    ast_node_fundecl() : ast_node_functionbase(ast_node_type::AST_FUNDECL) { }
    ast_node_ptr fundef;        // Co-owning pointer to function definition
};


struct ast_node_fundef : ast_node_functionbase {
    ast_node_fundef() : ast_node_functionbase(ast_node_type::AST_FUNDEF) { }
    detail::simple_backref fundecl; // Back-reference to function declaration
    ast_node_ptr qualified_name;    // Qualified name as parsed
    ast_function_info function_info; // Function info for function
    i64 fnidx = -1;             // Function index for interpreter, or -1
};


struct ast_node_lambda : ast_node_lambdabase {
    ast_node_lambda() : ast_node_lambdabase(ast_node_type::AST_LAMBDA) { }
};


struct ast_node_typeref : ast_node_x {
    ast_node_ptr ref;           // Qualified name or direct entity link
    detail::resolved_entity rve; // Resolved entity
    ast_node_typeref() : ast_node_x(ast_node_type::AST_TYPEREF) { }
};


struct ast_node_funpar : ast_node_br {
    ast_node_ptr typeref;       // Reference to a data type
    str name;                   // Name in parameter list, or empty string
    ast_variable_info variable_info; // Variable info for variable
    ast_node_funpar() : ast_node_br(ast_node_type::AST_FUNPAR) { }
};


struct ast_node_funparlist : ast_node_x {
    detail::simple_backref lambdabr;
    detail::name_dict lookup;         // Lookup of subnode (funpar) by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_funparlist() : ast_node_x(ast_node_type::AST_FUNPARLIST) { }

    exp_none set_lambdabackref(ast_node_ptr nd) noexcept {
        return lambdabr.set(nd);
    }
};


struct ast_node_funret : ast_node_br {
    ast_node_ptr typeref;       // Reference to a data type
    ast_variable_info variable_info; // Variable info for variable
    ast_node_funret() : ast_node_br(ast_node_type::AST_FUNRET) { }
};


struct ast_node_funretlist : ast_node_x {
    detail::simple_backref lambdabr;
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_funretlist() : ast_node_x(ast_node_type::AST_FUNRETLIST) { }

    exp_none set_lambdabackref(ast_node_ptr nd) noexcept {
        return lambdabr.set(nd);
    }
};


struct ast_node_funbody : ast_node_br {
    detail::simple_backref lambdabr;
    detail::subnode_list subnodelist; // List of subnodes: one compound stmt
    ast_node_funbody() : ast_node_br(ast_node_type::AST_FUNBODY) { }

    exp_none set_lambdabackref(ast_node_ptr nd) noexcept {
        return lambdabr.set(nd);
    }
};


struct ast_node_literal_expression : ast_node_x {
    literal litvalue;                 // Literal value of enum constant
    ast_variable_info variable_info;  // Variable info in global constants
    ast_expression_info ei;           // Expression info for place of use
    ast_node_literal_expression() :
        ast_node_x(ast_node_type::AST_EXPR_LITERAL) {
        x = X_HAS_RVALUE;
    }
};


struct ast_node_entitylink : ast_node_expressionbase {
    // Note that an entity link cannot come directly from source code,
    // it must be created programmatically
    detail::simple_backref link; // Direkt link to entity (not from source code)
    ast_node_entitylink() :
        ast_node_expressionbase(ast_node_type::AST_EXPR_ENTITYLINK) { }
};


struct ast_node_unqualified_name_expression : ast_node_expressionbase {
    str unqualified_name;       // Unqualified name
    ast_node_unqualified_name_expression() :
        ast_node_expressionbase(ast_node_type::AST_EXPR_UNQUALNAME) { }
};


struct ast_node_unary_expression : ast_node_expressionbase {
    ast_node_ptr subexpr1;
    ast_node_unary_expression(ast_node_type t) : ast_node_expressionbase(t) {
        using enum ast::ast_node_type;

        // In expression context, an unary expression is an RVALUE,
        // except for a member dereference, where the flag must be
        // transferred from the expr containing the member.
        if (t != AST_EXPR_MEMBER && t != AST_EXPR_MEMBERLINK)
            x = X_HAS_RVALUE;
    }
};


struct ast_node_memberlink : ast_node_unary_expression {
    // Note that a member link cannot come directly from source code,
    // it must be created programmatically
    detail::simple_backref link; // Direkt link to entity (not from source code)
    ast_node_memberlink() :
        ast_node_unary_expression(ast_node_type::AST_EXPR_MEMBERLINK) { }
};


struct ast_node_member_expression : ast_node_unary_expression {
    str member_name;
    ast_node_member_expression() :
        ast_node_unary_expression(ast_node_type::AST_EXPR_MEMBER) { }
};


struct ast_node_binary_expression : ast_node_expressionbase {
    ast_node_ptr subexpr1;
    ast_node_ptr subexpr2;
    ast_node_binary_expression(ast_node_type t) : ast_node_expressionbase(t) {
        using enum ast::ast_node_type;

        // In expression context, a binary expression is an RVALUE,
        // except for an array dereference, where the flag must be
        // transferred from the expr that is dereferenced.
        if (t != AST_EXPR_ARRAYDEREF &&
            !ast_node_type_is_assign_expr(t))
            x = X_HAS_RVALUE;
    }
};


struct ast_node_arrayderef_expression : ast_node_binary_expression {
    ast_node_arrayderef_expression() :
        ast_node_binary_expression(ast_node_type::AST_EXPR_ARRAYDEREF) { }
};


struct ast_node_slice_expression : ast_node_expressionbase {
    ast_node_ptr subexpr1;
    ast_node_ptr subexpr2;
    ast_node_ptr subexpr3;
    ast_node_ptr subexpr4;
    ast_node_slice_expression() :
        ast_node_expressionbase(ast_node_type::AST_EXPR_SLICE) {
        // In expression context, a slice is an RVALUE.
        x = X_HAS_RVALUE;
    }
};


struct ast_node_funcall_expression : ast_node_binary_expression {
    ast_node_funcall_expression() :
        ast_node_binary_expression(ast_node_type::AST_EXPR_FUNCALL) {
        // In expression context, a function call is an RVALUE.
        // Nothing to do here.
    }
};


struct ast_node_ternary_expression : ast_node_expressionbase {
    ast_node_ptr subexpr1;
    ast_node_ptr subexpr2;
    ast_node_ptr subexpr3;
    cig::source_location effectloc2;
    ast_node_ternary_expression(ast_node_type t) : ast_node_expressionbase(t) {
        using enum ast::ast_node_type;

        // In expression context, the RVALUE status of a conditional
        // expression is derived from its both alternatives.

        // The CONDITIONAL status must be set here.
        if (t == AST_EXPR_CONDITION)
            x = X_HAS_CONDITIONAL;
    }
};


struct ast_node_list_expression : ast_node_expressionbase {
    detail::subnode_list subnodelist; // List of subexpressions
    bool list_is_complete = false;    // List has been set complete
    ast_node_list_expression() :
        ast_node_expressionbase(ast_node_type::AST_EXPR_LIST) {
        // In expression context, the RVALUE or ULVALUE status of a list
        // expression is derived from its elements.
        // Nothing to do here.
    }
};


struct ast_node_return_statement : ast_node_br {
    ast_node_ptr expr;
    ast_node_return_statement() : ast_node_br(ast_node_type::AST_RETURNSTMT) { }
};


struct ast_node_expression_statement : ast_node_br {
    ast_node_ptr expr;
    ast_node_expression_statement() : ast_node_br(ast_node_type::AST_EXPRSTMT) { }
};


struct ast_node_if_statement : ast_node_br {
    ast_node_ptr initstmt;      // backref index AST_BR_INIT
    ast_node_ptr condexpr;      // backref index AST_BR_COND
    ast_node_ptr thenstmt;      // backref index AST_BR_THEN
    ast_node_ptr elsestmt;      // backref index AST_BR_ELSE
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_if_statement() : ast_node_br(ast_node_type::AST_IFSTMT) { }
};


struct ast_node_while_statement : ast_node_br {
    ast_node_ptr initstmt;      // backref index AST_BR_INIT
    ast_node_ptr condexpr;      // backref index AST_BR_COND
    ast_node_ptr bodystmt;      // backref index AST_BR_BODY
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_while_statement() : ast_node_br(ast_node_type::AST_WHILESTMT) { }
};


struct ast_node_do_while_statement : ast_node_br {
    ast_node_ptr initstmt;      // backref index AST_BR_INIT
    ast_node_ptr condexpr;      // backref index AST_BR_COND
    ast_node_ptr bodystmt;      // backref index AST_BR_BODY
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_do_while_statement() : ast_node_br(ast_node_type::AST_DOWHILESTMT) { }
};


struct ast_node_for_statement : ast_node_br {
    ast_node_ptr initstmt;      // backref index AST_BR_INIT
    ast_node_ptr condexpr;      // backref index AST_BR_COND
    ast_node_ptr iterateexpr;   // backref index AST_BR_ITERATEEXPR
    ast_node_ptr bodystmt;      // backref index AST_BR_BODY
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_for_statement() : ast_node_br(ast_node_type::AST_FORSTMT) { }
};


struct ast_node_forof_statement : ast_node_br {
    ast_node_ptr initstmt;      // backref index AST_BR_INIT
    ast_node_ptr iterator_expr;  // backref index AST_BR_ITERATOR
    ast_node_ptr generator_expr; // backref index AST_BR_GENERATOR
    ast_node_ptr bodystmt;       // backref index AST_BR_BODY
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    i64 n_subnode_initstmt;      // Number of subnodes from init statement
    ast_node_forof_statement() : ast_node_br(ast_node_type::AST_FOROFSTMT) { }
};


struct ast_node_break_statement : ast_node_br {
    ast_node_break_statement() : ast_node_br(ast_node_type::AST_BREAKSTMT) { }
};


struct ast_node_continue_statement : ast_node_br {
    ast_node_continue_statement() :
        ast_node_br(ast_node_type::AST_CONTINUESTMT) { }
};


struct ast_node_compound_statement : ast_node_br {
    detail::name_dict lookup;         // Lookup of subnode by name
    detail::subnode_list subnodelist; // List of subnodes
    ast_node_compound_statement() :
        ast_node_br(ast_node_type::AST_COMPOUNDSTMT) { }
};







/**********************************************************************/
/*                                                                    */
/*                     AST NODE ACCESS FUNCTIONS                      */
/*                                                                    */
/**********************************************************************/


//
// Auxiliary internal functions
//


exp_i64 ast_node_global::add_global_subnode(ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    ast_node_type ndtype = nd->get_node_type();

    auto exp1 = globallist.add_subnode(nd);
    if (!exp1)
        return exp1;
    i64 index = exp1.value();
    cig_assert(index != -1);

    switch (ndtype)
    {
    case AST_NAMESPACE:
        static_cast<ast_node_namespace *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_CLASSDECL:
        static_cast<ast_node_classdecl *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_CLASSDEF:
        static_cast<ast_node_classdef *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_ENUMDECL:
        static_cast<ast_node_enumdecl *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_ENUMDEF:
        static_cast<ast_node_enumdef *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_VARIABLE:
        static_cast<ast_node_variable *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_FUNDECL:
        static_cast<ast_node_fundecl *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;
    case AST_FUNDEF:
        static_cast<ast_node_fundef *>(nd.get())->
            global_backref.set(thisnode,index);
        return index;

    case AST_INVALID:
    case AST_GLOBAL:
    case AST_FUNPARLIST:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        // Algorithmic error:
        // The node type of nd must have been checked by the caller
        cig_assert(false);
        return -1;
    }

    // Algorithmic error:
    // The node type of nd must have been checked by the caller
    return -1;
}

exp_none ast_node_functionbase::set_overloadset_backref(ast_node_ptr nd,i64 i)
    noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF ||
        node_type == AST_FUNDECL)
        return static_cast<ast_node_functionbase*>(this)->
            overloadset_backref.set(nd,i);
    else
        return makeerr(err::EC_NODE_HAS_NO_OVERLOADSETBACKREF);
}

exp_i64 detail::overload_list::add_overload(ast_node_ptr nd,
                                            ast_node_ptr const &thisnode) {
    using enum ast::ast_node_type;

    ast_node_type nt = nd->get_node_type();
    if (nt != AST_FUNDECL &&
        nt != AST_FUNDEF)
        return makeerr(err::EC_NODE_HAS_NO_OVERLOADSETBACKREF);
    i64 szret = get_count();
    auto exp = static_cast<ast_node_functionbase*>(nd.get())->
        set_overloadset_backref(thisnode,szret);
    if (!exp)
        return forwarderr(exp);
    push_back(nd);
    return szret;
}


//
// Creation
//


// Get node type
ast_node_type ast_node::get_node_type() const noexcept
{
    return node_type;
}

// Constructor by enum tag
ast_node_ptr ast_node::make_ast_node(ast_node_type t) noexcept
{
    using enum ast::ast_node_type;
    using all = cig::allocator<ast_node,"ast_node">;

    switch (t)
    {
    case AST_INVALID:
        return nullptr;
    case AST_GLOBAL:
        return std::allocate_shared<ast_node_global>(all());
    case AST_NAMESPACE:
        return std::allocate_shared<ast_node_namespace>(all());
    case AST_CLASSDECL:
        return std::allocate_shared<ast_node_classdecl>(all());
    case AST_CLASSDEF:
        return std::allocate_shared<ast_node_classdef>(all());
    case AST_ENUMDECL:
        return std::allocate_shared<ast_node_enumdecl>(all());
    case AST_ENUMDEF:
        return std::allocate_shared<ast_node_enumdef>(all());
    case AST_ENUMVALUE:
        return std::allocate_shared<ast_node_enumvalue>(all());
    case AST_VARIABLE:
        return std::allocate_shared<ast_node_variable>(all());
    case AST_OVERLOADSET:
        return std::allocate_shared<ast_node_overloadset>(all());
    case AST_FUNDECL:
        return std::allocate_shared<ast_node_fundecl>(all());
    case AST_FUNDEF:
        return std::allocate_shared<ast_node_fundef>(all());
    case AST_LAMBDA:
        return std::allocate_shared<ast_node_lambda>(all());
    case AST_TYPEREF:
        return std::allocate_shared<ast_node_typeref>(all());
    case AST_FUNPAR:
        return std::allocate_shared<ast_node_funpar>(all());
    case AST_FUNPARLIST:
        return std::allocate_shared<ast_node_funparlist>(all());
    case AST_FUNRET:
        return std::allocate_shared<ast_node_funret>(all());
    case AST_FUNRETLIST:
        return std::allocate_shared<ast_node_funretlist>(all());
    case AST_FUNBODY:
        return std::allocate_shared<ast_node_funbody>(all());
    case AST_EXPRMIN:
        return nullptr;
    case AST_EXPR_LITERAL:
        return std::allocate_shared<ast_node_literal_expression>(all());
    case AST_EXPR_ENTITYLINK:
        return std::allocate_shared<ast_node_entitylink>(all());
    case AST_EXPR_MEMBERLINK:
        return std::allocate_shared<ast_node_memberlink>(all());
    case AST_EXPR_UNQUALNAME:
        return std::allocate_shared<ast_node_unqualified_name_expression>(
            all());
    case AST_EXPR_MEMBER:
        return std::allocate_shared<ast_node_member_expression>(all());
    case AST_EXPR_ARRAYDEREF:
        return std::allocate_shared<ast_node_arrayderef_expression>(all());
    case AST_EXPR_SLICE:
        return std::allocate_shared<ast_node_slice_expression>(all());
    case AST_EXPR_FUNCALL:
        return std::allocate_shared<ast_node_funcall_expression>(all());
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
        return std::allocate_shared<ast_node_unary_expression>(all(),t);
    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
        return std::allocate_shared<ast_node_binary_expression>(all(),t);
    case AST_EXPR_CONDITION:
        return std::allocate_shared<ast_node_ternary_expression>(all(),t);
    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
        return std::allocate_shared<ast_node_binary_expression>(all(),t);
    case AST_EXPR_LIST:
        return std::allocate_shared<ast_node_list_expression>(all());
    case AST_EXPRMAX:
        return nullptr;
    case AST_STMTMIN:
        return nullptr;
    case AST_COMPOUNDSTMT:
        return std::allocate_shared<ast_node_compound_statement>(all());
    case AST_RETURNSTMT:
        return std::allocate_shared<ast_node_return_statement>(all());
    case AST_EXPRSTMT:
        return std::allocate_shared<ast_node_expression_statement>(all());
    case AST_IFSTMT:
        return std::allocate_shared<ast_node_if_statement>(all());
    case AST_WHILESTMT:
        return std::allocate_shared<ast_node_while_statement>(all());
    case AST_DOWHILESTMT:
        return std::allocate_shared<ast_node_do_while_statement>(all());
    case AST_FORSTMT:
        return std::allocate_shared<ast_node_for_statement>(all());
    case AST_FOROFSTMT:
        return std::allocate_shared<ast_node_forof_statement>(all());
    case AST_BREAKSTMT:
        return std::allocate_shared<ast_node_break_statement>(all());
    case AST_CONTINUESTMT:
        return std::allocate_shared<ast_node_continue_statement>(all());
    case AST_STMTMAX:
        return nullptr;
    case AST_MAX:
        return nullptr;
    }
    return nullptr;
}

// Constructor by enum tag and locations
ast_node_ptr ast_node::make_ast_node(ast_node_type t,
                                     cig::source_location start,
                                     cig::source_location effect,
                                     cig::source_location end) noexcept
{
    ast_node_ptr nd = make_ast_node(t);
    cig_assert(nd);
    ast_node *ndptr = nd.get();
    ndptr->startloc = start;
    ndptr->effectloc = effect;
    ndptr->endloc = end;
    return nd;
}


// Constructor by ast_type_info
ast_node_ptr ast_node::make_builtin_ast_node(ast_type_info type_info) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr nd = make_ast_node(AST_CLASSDEF);
    cig_assert(nd);
    nd->set_type_info(type_info);
    return nd;
}


//
// Location information
//


cig::source_location ast_node::get_startloc() const noexcept
{
    return startloc;
}

cig::source_location ast_node::get_effectloc() const noexcept
{
    return effectloc;
}

cig::source_location ast_node::get_effectloc2() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_CONDITION)
        return static_cast<const ast_node_ternary_expression*>(this)->
            effectloc2;

    return effectloc;
}

cig::source_location ast_node::get_endloc() const noexcept
{
    return endloc;
}

void ast_node::set_startloc(cig::source_location const &sloc) noexcept
{
    startloc = sloc;
}

void ast_node::set_effectloc(cig::source_location const &sloc) noexcept
{
    effectloc = sloc;
}

void ast_node::set_effectloc2(cig::source_location const &sloc) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_CONDITION)
        static_cast<ast_node_ternary_expression*>(this)->effectloc2 = sloc;
    // else ignore
}

void ast_node::set_endloc(cig::source_location const &sloc) noexcept
{
    endloc = sloc;
}


//
// Descendent nodes of global scope
//


ast_node_ptr                    // ast_node_ptr or nullptr
ast_node::get_global_subnode(i64 index) const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_GLOBAL)
    {
        return static_cast<const ast_node_global*>(this)->
            globallist.get_subnode(index);
    }
    return nullptr;
}


i64                             // count of global subnodes or -1 if not global
ast_node::get_global_subnode_count() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_GLOBAL)
    {
        return static_cast<const ast_node_global*>(this)->
            globallist.get_count();
    }
    return -1;
}


//
// Child nodes in this scope
//


// Lookup entries in parent scope
i64                             // Index of subnode, or -1
ast_node::lookup_name(str const &nm) const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_GLOBAL:
        return static_cast<const ast_node_global*>(this)->
            lookup.lookup_name(nm);
    case AST_NAMESPACE:
        return static_cast<const ast_node_namespace*>(this)->
            lookup.lookup_name(nm);
    case AST_CLASSDEF:
        return static_cast<const ast_node_classdef*>(this)->
            lookup.lookup_name(nm);
    case AST_ENUMDEF:
        return static_cast<const ast_node_enumdef*>(this)->
            lookup.lookup_name(nm);
    case AST_FUNPARLIST:
        return static_cast<const ast_node_funparlist*>(this)->
            lookup.lookup_name(nm);
    case AST_COMPOUNDSTMT:
        return static_cast<const ast_node_compound_statement*>(this)->
            lookup.lookup_name(nm);
    case AST_IFSTMT:
        return static_cast<const ast_node_if_statement*>(this)->
            lookup.lookup_name(nm);
    case AST_WHILESTMT:
        return static_cast<const ast_node_while_statement*>(this)->
            lookup.lookup_name(nm);
    case AST_DOWHILESTMT:
        return static_cast<const ast_node_do_while_statement*>(this)->
            lookup.lookup_name(nm);
    case AST_FORSTMT:
        return static_cast<const ast_node_for_statement*>(this)->
            lookup.lookup_name(nm);
    case AST_FOROFSTMT:
        return static_cast<const ast_node_forof_statement*>(this)->
            lookup.lookup_name(nm);

        // Not available for:
    case AST_INVALID:
    case AST_CLASSDECL:
    case AST_ENUMDECL:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL: // Not looked up directly but via overload set
    case AST_FUNDEF:  // Not looked up directly but via overload set
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_STMTMIN:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_CONTINUESTMT:
    case AST_BREAKSTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return -1;
    }
    return -1;
}

cig::source_location            // Source location of name
ast_node::lookup_name_location(str const &nm) const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDEF:
    case AST_ENUMDEF:
    case AST_FUNPARLIST:
    case AST_COMPOUNDSTMT:
    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    case AST_FOROFSTMT:
    {
        i64 index = lookup_name(nm);
        if (index == -1)
            return std::source_location::current(); // an algorithmic error
        ast_node_ptr nd = get_subnode(index);
        if (nd == nullptr)
            return std::source_location::current(); // an algorithmic error
        return nd->get_effectloc();
    }

        // Not available for:
    case AST_INVALID:
    case AST_CLASSDECL:
    case AST_ENUMDECL:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_STMTMIN:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_CONTINUESTMT:
    case AST_BREAKSTMT:
    case AST_STMTMAX:
    case AST_MAX:
        // Algorithmic error
        return std::source_location::current();
    }
    // Algorithmic error
    return std::source_location::current();
}

// Add a new lookup entry in parent scope
exp_none ast_node::add_lookup_name(str const &nm, // as per child.get_name()
                                   i64 index)     // as per add_subnode()
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_GLOBAL:
        return static_cast<ast_node_global*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_NAMESPACE:
        return static_cast<ast_node_namespace*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_CLASSDEF:
        return static_cast<ast_node_classdef*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_ENUMDEF:
        return static_cast<ast_node_enumdef*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_FUNPARLIST:
        return static_cast<ast_node_funparlist*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_COMPOUNDSTMT:
        return static_cast<ast_node_compound_statement*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_IFSTMT:
        return static_cast<ast_node_if_statement*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_WHILESTMT:
        return static_cast<ast_node_while_statement*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_DOWHILESTMT:
        return static_cast<ast_node_do_while_statement*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_FORSTMT:
        return static_cast<ast_node_for_statement*>(this)->
            lookup.add_lookup_name(nm,index);
    case AST_FOROFSTMT:
        return static_cast<ast_node_forof_statement*>(this)->
            lookup.add_lookup_name(nm,index);

        // Not available for:
    case AST_INVALID:
    case AST_CLASSDECL:
    case AST_ENUMDECL:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL: // Not looked up directly but via overload set
    case AST_FUNDEF:  // Not looked up directly but via overload set
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_STMTMIN:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_CONTINUESTMT:
    case AST_BREAKSTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return makeerr(err::EC_ASTNODE_BADTYPE);
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Add and retrieve subnodes in parent scope
inline
exp_i64 ast_node::add_subnode(ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();

    ast_node_type nt = nd->get_node_type();

    // Do not allow to add functions;
    // this must be done with add_function().
    if (nt == AST_FUNDECL ||
        nt == AST_FUNDEF)
        return makeerr(err::EC_ASTNODE_BADTYPE);

    switch (node_type)
    {
    case AST_GLOBAL:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_global*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_NAMESPACE:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_namespace*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_CLASSDEF:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_classdef*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_ENUMDEF:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_enumdef*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_FUNPARLIST:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_funparlist*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_FUNRETLIST:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_funretlist*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_FUNBODY:
        ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_funbody*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_COMPOUNDSTMT:
        if (nt == AST_VARIABLE)
            ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        else
            ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_compound_statement*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_IFSTMT:
        if (nt == AST_VARIABLE)
            ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        else
            ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_if_statement*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_WHILESTMT:
        if (nt == AST_VARIABLE)
            ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        else
            ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_while_statement*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_DOWHILESTMT:
        if (nt == AST_VARIABLE)
            ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        else
            ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_do_while_statement*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_FORSTMT:
        if (nt == AST_VARIABLE)
            ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        else
            ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_for_statement*>(this)->
            subnodelist.add_subnode(nd,thisnode);
    case AST_FOROFSTMT:
        if (nt == AST_VARIABLE)
            ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        else
            ENSURE_NOT_YET_OWNED(nd);
        return static_cast<ast_node_forof_statement*>(this)->
            subnodelist.add_subnode(nd,thisnode);

        // Not available for:
    case AST_INVALID:
    case AST_CLASSDECL:
    case AST_ENUMDECL:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_NODE_EXPR_CASES:
    case AST_STMTMIN:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return makeerr(err::EC_NODE_HAS_NO_SUBNODELIST);
    }
    return makeerr(err::EC_NODE_HAS_NO_SUBNODELIST);
}


i64                             // count of subnodes or -1 if bad node type
ast_node::get_subnode_count() const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_GLOBAL:
        return static_cast<const ast_node_global*>(this)->
            subnodelist.get_count();
    case AST_NAMESPACE:
        return static_cast<const ast_node_namespace*>(this)->
            subnodelist.get_count();
    case AST_CLASSDEF:
        return static_cast<const ast_node_classdef*>(this)->
            subnodelist.get_count();
    case AST_ENUMDEF:
        return static_cast<const ast_node_enumdef*>(this)->
            subnodelist.get_count();
    case AST_FUNPARLIST:
        return static_cast<const ast_node_funparlist*>(this)->
            subnodelist.get_count();
    case AST_FUNRETLIST:
        return static_cast<const ast_node_funretlist*>(this)->
            subnodelist.get_count();
    case AST_FUNBODY:
        return static_cast<const ast_node_funbody*>(this)->
            subnodelist.get_count();
    case AST_EXPR_LIST:
        return static_cast<const ast_node_list_expression*>(this)->
            subnodelist.get_count();
    case AST_COMPOUNDSTMT:
        return static_cast<const ast_node_compound_statement*>(this)->
            subnodelist.get_count();
    case AST_IFSTMT:
        return static_cast<const ast_node_if_statement*>(this)->
            subnodelist.get_count();
    case AST_WHILESTMT:
        return static_cast<const ast_node_while_statement*>(this)->
            subnodelist.get_count();
    case AST_DOWHILESTMT:
        return static_cast<const ast_node_do_while_statement*>(this)->
            subnodelist.get_count();
    case AST_FORSTMT:
        return static_cast<const ast_node_for_statement*>(this)->
            subnodelist.get_count();
    case AST_FOROFSTMT:
        return static_cast<const ast_node_forof_statement*>(this)->
            subnodelist.get_count();

        // Not available for:
    case AST_INVALID:
    case AST_CLASSDECL:
    case AST_ENUMDECL:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_SLICE:
    case AST_EXPR_FUNCALL:
    case AST_NODE_UNARY_EXPR_CASES:
    case AST_NODE_BINARY_EXPR_CASES:
    case AST_NODE_TERNARY_EXPR_CASES:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return -1;
    }
    return -1;
}


inline
ast_node_ptr                    // ast_node_ptr or nullptr
ast_node::get_subnode(i64 index) const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_GLOBAL:
        return static_cast<const ast_node_global*>(this)->
            subnodelist.get_subnode(index);
    case AST_NAMESPACE:
        return static_cast<const ast_node_namespace*>(this)->
            subnodelist.get_subnode(index);
    case AST_CLASSDEF:
        return static_cast<const ast_node_classdef*>(this)->
            subnodelist.get_subnode(index);
    case AST_ENUMDEF:
        return static_cast<const ast_node_enumdef*>(this)->
            subnodelist.get_subnode(index);
    case AST_FUNPARLIST:
        return static_cast<const ast_node_funparlist*>(this)->
            subnodelist.get_subnode(index);
    case AST_FUNRETLIST:
        return static_cast<const ast_node_funretlist*>(this)->
            subnodelist.get_subnode(index);
    case AST_FUNBODY:
        return static_cast<const ast_node_funbody*>(this)->
            subnodelist.get_subnode(index);
    case AST_EXPR_LIST:
        return static_cast<const ast_node_list_expression*>(this)->
            subnodelist.get_subnode(index);
    case AST_COMPOUNDSTMT:
        return static_cast<const ast_node_compound_statement*>(this)->
            subnodelist.get_subnode(index);
    case AST_IFSTMT:
        return static_cast<const ast_node_if_statement*>(this)->
            subnodelist.get_subnode(index);
    case AST_WHILESTMT:
        return static_cast<const ast_node_while_statement*>(this)->
            subnodelist.get_subnode(index);
    case AST_DOWHILESTMT:
        return static_cast<const ast_node_do_while_statement*>(this)->
            subnodelist.get_subnode(index);
    case AST_FORSTMT:
        return static_cast<const ast_node_for_statement*>(this)->
            subnodelist.get_subnode(index);
    case AST_FOROFSTMT:
        return static_cast<const ast_node_forof_statement*>(this)->
            subnodelist.get_subnode(index);

        // Not available for:
    case AST_INVALID:
    case AST_CLASSDECL:
    case AST_ENUMDECL:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_SLICE:
    case AST_EXPR_FUNCALL:
    case AST_NODE_UNARY_EXPR_CASES:
    case AST_NODE_BINARY_EXPR_CASES:
    case AST_NODE_TERNARY_EXPR_CASES:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return nullptr;
    }
    return nullptr;
}

// Back-reference from subnode to parent scope
ast_node_backref ast_node::get_backref() const noexcept
{
    if (ast_node_type_has_backref(node_type))
        return static_cast<ast_node_br const *>(this)->ibr.get();
    else
        return ast_node_backref(nullptr,-1);
}

// Back-reference from subnode to global scope
ast_node_backref ast_node::get_global_backref() const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_NAMESPACE:
        return static_cast<ast_node_namespace const *>(this)->
            global_backref.get();
    case AST_CLASSDECL:
        return static_cast<ast_node_classdecl const *>(this)->
            global_backref.get();
    case AST_CLASSDEF:
        return static_cast<ast_node_classdef const *>(this)->
            global_backref.get();
    case AST_ENUMDECL:
        return static_cast<ast_node_enumdecl const *>(this)->
            global_backref.get();
    case AST_ENUMDEF:
        return static_cast<ast_node_enumdef const *>(this)->
            global_backref.get();
    case AST_VARIABLE:
        return static_cast<ast_node_variable const *>(this)->
            global_backref.get();
    case AST_FUNDECL:
        return static_cast<ast_node_fundecl const *>(this)->
            global_backref.get();
    case AST_FUNDEF:
        return static_cast<ast_node_fundef const *>(this)->
            global_backref.get();

    case AST_INVALID:
    case AST_GLOBAL:
    case AST_FUNPARLIST:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return ast_node_backref(nullptr,-1);
    }
    return ast_node_backref(nullptr,-1);
}


// Add a namespace, enumvalue, variable, funpar, or funret
// This function combines add_subnode with add_lookup_name
exp_i64 ast_node::add_named_entity_aux(ast_node_ptr entity,bool with_var)
{
    using namespace ast;
    using enum ast::ast_node_type;

    // Argument check
    if (entity == nullptr)
        return makeerr(err::EC_AST_NODE_ENTITY_TO_ADD_IS_NULL,
                       effectloc);

    ast_node_type entity_type = entity->get_node_type();
    ast_node_type scope_type = node_type;
    err::errorcode ecode = err::E_SUCCESS;
    bool name_required = true;
    ast_node_global *global = nullptr;

    switch (entity_type)
    {
    case AST_NAMESPACE:
        if (scope_type != AST_GLOBAL &&
            scope_type != AST_NAMESPACE)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        ENSURE_NOT_YET_OWNED(entity);
        ecode = err::EC_NAMESPACE_NAME_ALREADY_USED;
        name_required = true;
        goto do_add;

    case AST_ENUMVALUE:
        if (scope_type != AST_ENUMDEF)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        ENSURE_NOT_YET_OWNED(entity);
        ecode = err::EC_ENUMCONST_NAME_ALREADY_USED;
        name_required = true;
        goto do_add;

    case AST_VARIABLE:
        if (scope_type != AST_GLOBAL &&
            scope_type != AST_NAMESPACE &&
            scope_type != AST_CLASSDEF &&
            scope_type != AST_COMPOUNDSTMT &&
            scope_type != AST_IFSTMT &&
            scope_type != AST_WHILESTMT &&
            scope_type != AST_DOWHILESTMT &&
            scope_type != AST_FORSTMT &&
            scope_type != AST_FOROFSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (ast_node_type_is_stmt(node_type))
        {
            if (!with_var)
                return makeerr(err::EC_ASTNODE_BADADDTYPE);
            ENSURE_NOT_YET_LINKED_FROM_DECL(entity);
        }
        else
            ENSURE_NOT_YET_OWNED(entity);
        ecode = err::EC_VARIABLE_NAME_ALREADY_USED;
        name_required = true;
        // Check static/nonstatic consistency
        if (true)
        {
            auto expflags = entity->get_flags();
            if (!expflags)
                return makealgorithmic();
            auto newflags = expflags.value();
            if ((newflags & AST_FLAG_STATIC) != 0 &&
                (newflags & AST_FLAG_NONSTATIC) != 0)
                return makeerr(err::EC_CONTRADICTING_STATIC_SPECIFICATION,
                               entity->get_effectloc());
        }
        goto do_add;

    case AST_FUNPAR:
        if (scope_type != AST_FUNPARLIST)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        ENSURE_NOT_YET_OWNED(entity);
        ecode = err::EC_FUNPAR_NAME_ALREADY_USED;
        name_required = false;
        goto do_add;

    case AST_FUNRET:
        if (scope_type != AST_FUNRETLIST)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        ENSURE_NOT_YET_OWNED(entity);
        ecode = err::EC_ALGORITHMIC; // a return type never has a name
        name_required = false;
        goto do_add;


        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return makeerr(err::EC_ASTNODE_BADADDTYPE);
    }
    return makeerr(err::EC_ASTNODE_BADADDTYPE);

 do_add:

    // Get the global namespace
    if (scope_type == AST_GLOBAL ||
        scope_type == AST_NAMESPACE)
    {
        global = static_cast<ast_node_br*>(this)->get_global_from_namespace();
        if (global == nullptr)
            return makeerr(err::EC_NAMESPACE_NOT_IN_GLOBAL,
                           entity->get_effectloc());
    }

    // Get the entity name
    str entityname = entity->get_name();
    bool emptyname = (entityname == ""_str);
    if (emptyname && name_required)
        return makeerr(err::EC_ENTITY_NAME_NOT_AVAILABLE,
                       entity->get_effectloc());

    if (!emptyname)
    {
        // Check if the name is already used
        i64 dupindex = lookup_name(entityname);
        if (dupindex != -1)
        {
            return makeerr(ecode,
                           entity->get_effectloc(),
                           lookup_name_location(entityname));
        }
    }

    // Add the entity to the scope node
    auto expindex = add_subnode(entity);
    if (!expindex)
        return expindex;
    i64 index = expindex.value();
    cig_assert(index != -1);
    if (entity_type == AST_VARIABLE &&
        ast_node_type_is_stmt(node_type))
        SET_LINKED_FROM_DECL(entity);
    else
        SET_OWNED(entity);

    // Add the entity name
    if (!emptyname)
    {
        auto explookup = add_lookup_name(entityname,index);
        cig_assert(explookup);
    }

    // Add the entity to the global node
    if (global)
    {
        auto expindexg = global->add_global_subnode(entity);
        if (!expindexg)
            return expindexg;
    }

    return index;
}


exp_i64 ast_node::add_named_entity(ast_node_ptr entity)
{
    return add_named_entity_aux(entity,false);
}


//
// Cross-tree connectivity
//


// Add and retrieve the entity link (typeref,varref,...)
// The entity link is a reference to an entity that is not
// created by parsing, i.e. by interpreting source code
// directly, but by setting it computationally in a
// code generating function.
// The entity link can be supplemented by following
// member lookups that can be generated by source code
// or programmatically as well, to form a reference
// that can be passed to set_ref().
exp_none ast_node::set_link(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_LINK_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_EXPR_ENTITYLINK)
    {
        // fixme check that nd is a proper entity
        auto exp = static_cast<ast_node_entitylink*>(this)->link.set(nd);
        if (!exp)
            return exp;
        return exp_none();
    }

    if (node_type == AST_EXPR_MEMBERLINK)
    {
        // fixme check that nd is a proper entity
        auto exp = static_cast<ast_node_memberlink*>(this)->link.set(nd);
        if (!exp)
            return exp;
        return exp_none();
    }

    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_link() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_ENTITYLINK)
        return static_cast<const ast_node_entitylink*>(this)->link.get();
    if (node_type == AST_EXPR_MEMBERLINK)
        return static_cast<const ast_node_memberlink*>(this)->link.get();

    return nullptr;
}


// Add and retrieve a reference to an entity
// Such a reference can be a qualified name (unqualified name
// or keyword 'global' or similar, followed by member expressions)
// or it can be an entity link (a link created by calling set_link()
// in a code generating function for an AST_ENTITYLINK node,
// and that node can also be followed by member expressions).
exp_none ast_node::set_ref(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_REF_TO_ADD_IS_NULL,
                       effectloc);
    if (!nd->is_complete())
        return makeerr(err::EC_AST_TREXPRESSION_IS_NOT_COMPLETE,
                       effectloc);

    if (node_type == AST_TYPEREF)
    {
        // fixme check that nd is a qualified name or an entitylink
        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_typeref*>(this)->ref,nd);
        SET_OWNED(nd);
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_ref() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_TYPEREF)
        return static_cast<const ast_node_typeref*>(this)->ref;

    return nullptr;
}

// Add and retrieve a type reference
// A type reference is a special AST_TYPEREF node that contains a
// qualified name or an entity link, which can be set and
// retrieved with set_ref() and get_ref().
// The type reference node is a required intermediary node
// between that reference and a node requiring a data type,
// such as a variable declaration, function parameter or
// function return type.
exp_none ast_node::set_typeref(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_TYPEREF_TO_ADD_IS_NULL,
                       effectloc);

    if (nd->node_type != AST_TYPEREF)
        return makeerr(err::EC_ASTNODE_BADADDTYPE);
    if (!nd->is_complete())
        return makeerr(err::EC_AST_TYPEREF_IS_NOT_COMPLETE,
                       effectloc);

    switch (node_type)
    {
    case AST_VARIABLE:
        // fixme check that nd is a proper typeref
        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_variable*>(this)->typeref,nd);
        SET_OWNED(nd);
        return exp_none();
    case AST_FUNPAR:
        // fixme check that nd is a proper typeref
        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_funpar*>(this)->typeref,nd);
        SET_OWNED(nd);
        return exp_none();
    case AST_FUNRET:
        // fixme check that nd is a proper typeref
        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_funret*>(this)->typeref,nd);
        SET_OWNED(nd);
        return exp_none();

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return makeerr(err::EC_ASTNODE_BADTYPE);
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_typeref() const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_VARIABLE:
        return static_cast<const ast_node_variable*>(this)->typeref;
    case AST_FUNPAR:
        return static_cast<const ast_node_funpar*>(this)->typeref;
    case AST_FUNRET:
        return static_cast<const ast_node_funret*>(this)->typeref;

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return nullptr;
    }
    return nullptr;
}


//
// Class declaration and definition
//


exp_none ast_node::set_classdef(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDECL)
    {
        ast_node_ptr thisnode = shared_from_this();
        ast_node_classdecl *classdecl =
            static_cast<ast_node_classdecl *>(this);

        // Check that nd is a proper class definition
        if (nd->node_type != AST_CLASSDEF)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // Set the backref
        ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        CHECK_SLOT(classdecl->classdef,nd);
        ast_node_classdef *classdef =
            static_cast<ast_node_classdef *>(nd.get());
        auto exp = classdef->classdecl.set(thisnode);
        if (!exp)
            return forwarderr(exp);

        // Set the forward ref
        classdecl->classdef = nd;
        SET_LINKED_FROM_DECL(nd);

        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_classdef() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDECL)
        return static_cast<const ast_node_classdecl*>(this)->classdef;

    return nullptr;
}

// Set and get the class definition in a class declaration
ast_node_ptr ast_node::get_classdecl_backref() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDEF)
        return static_cast<ast_node_classdef const *>(this)->classdecl.get();

    return nullptr;
}


// Add a class as a subnode and link it into the tree
exp_i64 ast_node::add_class(ast_node_ptr nd)
{
    using namespace ast;
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    ast_node_global *global = nullptr;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_CLASS_TO_ADD_IS_NULL,
                       effectloc);

    // Check the scope where declared/defined
    if (node_type != AST_GLOBAL &&
        node_type != AST_NAMESPACE &&
        node_type != AST_CLASSDEF)
        return makeerr(err::EC_ASTNODE_BADTYPE);

    // Get the global namespace
    if (node_type == AST_GLOBAL ||
        node_type == AST_NAMESPACE)
    {
        global = static_cast<ast_node_br*>(this)->get_global_from_namespace();
        if (global == nullptr)
            return makeerr(err::EC_NAMESPACE_NOT_IN_GLOBAL,
                           nd->get_effectloc());
    }

    // Check the class node
    ast_node_type classtype = nd->get_node_type();
    if (classtype != AST_CLASSDECL &&
        classtype != AST_CLASSDEF)
        return makeerr(err::EC_ASTNODE_BADADDTYPE);

    // Check that the class is not already owned
    ENSURE_NOT_YET_OWNED(nd);
    ENSURE_NOT_YET_LINKED_FROM_DECL(nd);

    // Get the class name
    str classname = nd->get_name();
    bool emptyname = (classname == ""_str);
    if (emptyname)
        return makeerr(err::EC_ENTITY_NAME_NOT_AVAILABLE,
                       nd->get_effectloc());

    // Check static/nonstatic consistency
    auto expflags = nd->get_flags();
    if (!expflags)
        return makealgorithmic();
    auto newflags = expflags.value();
    if ((newflags & AST_FLAG_STATIC) != 0 &&
        (newflags & AST_FLAG_NONSTATIC) != 0)
        return makeerr(err::EC_CONTRADICTING_STATIC_SPECIFICATION,
                       nd->get_effectloc());

    // Check if a previous declaration  or definition
    // is present in the scope
    ast_node_ptr classdecl = nullptr;
    i64 declindex = lookup_name(classname);
    ast_node_type oldclasstype = AST_INVALID;
    if (declindex != -1)
    {
        classdecl = get_subnode(declindex);
        if (classdecl == nullptr)
            return makealgorithmic();
        oldclasstype = classdecl->get_node_type();
        if (oldclasstype != AST_CLASSDECL &&
            oldclasstype != AST_CLASSDEF)
            return makeerr(err::EC_CLASS_NAME_ALREADY_USED,
                           nd->get_effectloc(),
                           classdecl->get_effectloc());
    }

    // Here, classdecl is the previous declaration or definition

    if (classdecl)
    {
        // Compare the flags
        auto exp2 = classdecl->get_flags();
        if (!exp2)
            return makealgorithmic();
        auto oldflags = exp2.value();
        // Mismatch in static/nonstatic flags
        if (DEFAULT_CLASS_IS_STATIC)
        {
            if (!(oldflags & AST_FLAG_NONSTATIC))
                oldflags |= AST_FLAG_STATIC;
            if (!(newflags & AST_FLAG_NONSTATIC))
                newflags |= AST_FLAG_STATIC;
        }
        else
        {
            if (!(oldflags & AST_FLAG_STATIC))
                oldflags |= AST_FLAG_NONSTATIC;
            if (!(newflags & AST_FLAG_STATIC))
                newflags |= AST_FLAG_NONSTATIC;
        }
        if ((oldflags & AST_FLAG_STATIC) != (newflags & AST_FLAG_STATIC) ||
            (oldflags & AST_FLAG_NONSTATIC) != (newflags & AST_FLAG_NONSTATIC))
            return makeerr(err::EC_CONTRADICTING_STATIC_SPECIFICATION,
                           nd->get_effectloc(),
                           classdecl->get_effectloc());
        // Mismatch in other flags
        if (oldflags != newflags)
            return makeerr(err::EC_FLAGS_MISMATCH,
                           nd->get_effectloc(),
                           classdecl->get_effectloc());

        // Both cannot be definitions
        if (oldclasstype == AST_CLASSDEF &&
            classtype == AST_CLASSDEF)
            return makeerr(err::EC_CLASS_ALREADY_DEFINED,
                           nd->get_effectloc(),
                           classdecl->get_effectloc());

        // If the new nd is a declaration, discard it as a duplicate
        // Return an index of -1 but success
        if (classtype == AST_CLASSDECL)
            return -1;

        cig_assert(classtype == AST_CLASSDEF);
        cig_assert(oldclasstype == AST_CLASSDECL);

        // Now add the class definition to the class declaration
        auto exp4 = classdecl->set_classdef(nd);
        if (!exp4)
            return makeerr(exp4.error().get_error_code(),
                           nd->get_effectloc(),
                           classdecl->get_effectloc());

        // And add the class definition as a subnode without name lookup
        auto expindex = add_subnode(nd);
        if (!expindex)
            return expindex;
        i64 index = expindex.value();
        cig_assert(index != -1);
        SET_OWNED(nd);

        // Add the class definition to the global node
        if (global)
        {
            auto expindexg = global->add_global_subnode(nd);
            if (!expindexg)
                return expindexg;
        }

        return index;
    }
    else
    {
        // Add the class to the scope node
        auto expindex = add_subnode(nd);
        if (!expindex)
            return expindex;
        i64 index = expindex.value();
        cig_assert(index != -1);
        SET_OWNED(nd);

        // Add the class name
        auto explookup = add_lookup_name(classname,index);
        cig_assert(explookup);

        // Add the class to the global node
        if (global)
        {
            auto expindexg = global->add_global_subnode(nd);
            if (!expindexg)
                return expindexg;
        }

        return index;
    }
}


//
// Enum declaration and definition
//


exp_none ast_node::set_enumdef(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMDECL)
    {
        ast_node_ptr thisnode = shared_from_this();
        ast_node_enumdecl *enumdecl =
            static_cast<ast_node_enumdecl *>(this);

        // Check that nd is a proper enum definition
        if (nd->node_type != AST_ENUMDEF)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // Set the backref
        ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        CHECK_SLOT(enumdecl->enumdef,nd);
        ast_node_enumdef *enumdef =
            static_cast<ast_node_enumdef *>(nd.get());
        auto exp = enumdef->enumdecl.set(thisnode);
        if (!exp)
            return forwarderr(exp);

        // Set the forward ref
        enumdecl->enumdef = nd;
        SET_LINKED_FROM_DECL(nd);

        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_enumdef() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMDECL)
        return static_cast<const ast_node_enumdecl*>(this)->enumdef;

    return nullptr;
}

// Set and get the enum definition in a enum declaration
ast_node_ptr ast_node::get_enumdecl_backref() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMDEF)
        return static_cast<ast_node_enumdef const *>(this)->enumdecl.get();

    return nullptr;
}


// Add an enum as a subnode and link it into the tree
exp_i64 ast_node::add_enum(ast_node_ptr nd)
{
    using namespace ast;
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    ast_node_global *global = nullptr;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_ENUM_TO_ADD_IS_NULL,
                       effectloc);

    // Check the scope where declared/defined
    if (node_type != AST_GLOBAL &&
        node_type != AST_NAMESPACE &&
        node_type != AST_CLASSDEF)
        return makeerr(err::EC_ASTNODE_BADTYPE);

    // Get the global namespace
    if (node_type == AST_GLOBAL ||
        node_type == AST_NAMESPACE)
    {
        global = static_cast<ast_node_br*>(this)->get_global_from_namespace();
        if (global == nullptr)
            return makeerr(err::EC_NAMESPACE_NOT_IN_GLOBAL,
                           nd->get_effectloc());
    }

    // Check the enum node
    ast_node_type enumtype = nd->get_node_type();
    if (enumtype != AST_ENUMDECL &&
        enumtype != AST_ENUMDEF)
        return makeerr(err::EC_ASTNODE_BADADDTYPE);

    // Check that the enum is not already owned
    ENSURE_NOT_YET_OWNED(nd);
    ENSURE_NOT_YET_LINKED_FROM_DECL(nd);

    // Get the enum name
    str enumname = nd->get_name();
    bool emptyname = (enumname == ""_str);
    if (emptyname)
        return makeerr(err::EC_ENTITY_NAME_NOT_AVAILABLE,
                       nd->get_effectloc());

    // Check if a previous declaration  or definition
    // is present in the scope
    ast_node_ptr enumdecl = nullptr;
    i64 declindex = lookup_name(enumname);
    ast_node_type oldenumtype = AST_INVALID;
    if (declindex != -1)
    {
        enumdecl = get_subnode(declindex);
        if (enumdecl == nullptr)
            return makealgorithmic();
        oldenumtype = enumdecl->get_node_type();
        if (oldenumtype != AST_ENUMDECL &&
            oldenumtype != AST_ENUMDEF)
            return makeerr(err::EC_ENUM_NAME_ALREADY_USED,
                           nd->get_effectloc(),
                           enumdecl->get_effectloc());
    }

    // Here, enumdecl is the previous declaration or definition

    if (enumdecl)
    {
        // Both cannot be definitions
        if (oldenumtype == AST_ENUMDEF &&
            enumtype == AST_ENUMDEF)
            return makeerr(err::EC_ENUM_ALREADY_DEFINED,
                           nd->get_effectloc(),
                           enumdecl->get_effectloc());

        // If the new nd is a declaration, discard it as a duplicate
        // Return an index of -1 but success
        if (enumtype == AST_ENUMDECL)
            return -1;

        cig_assert(enumtype == AST_ENUMDEF);
        cig_assert(oldenumtype == AST_ENUMDECL);

        // Now add the enum definition to the enum declaration
        auto exp4 = enumdecl->set_enumdef(nd);
        if (!exp4)
            return makeerr(exp4.error().get_error_code(),
                           nd->get_effectloc(),
                           enumdecl->get_effectloc());

        // And add the enum definition as a subnode without name lookup
        auto expindex = add_subnode(nd);
        if (!expindex)
            return expindex;
        i64 index = expindex.value();
        cig_assert(index != -1);
        SET_OWNED(nd);

        // Add the enum definition to the global node
        if (global)
        {
            auto expindexg = global->add_global_subnode(nd);
            if (!expindexg)
                return expindexg;
        }

        return index;
    }
    else
    {
        // Add the enum to the scope node
        auto expindex = add_subnode(nd);
        if (!expindex)
            return expindex;
        i64 index = expindex.value();
        cig_assert(index != -1);
        SET_OWNED(nd);

        // Add the enum name
        auto explookup = add_lookup_name(enumname,index);
        cig_assert(explookup);

        // Add the enum to the global node
        if (global)
        {
            auto expindexg = global->add_global_subnode(nd);
            if (!expindexg)
                return expindexg;
        }

        return index;
    }
}


//
// Function declaration and definition
//


// Add and retrieve lambdas
exp_i64 ast_node::add_overload(ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    if (node_type == AST_OVERLOADSET)
    {
        ast_node_ptr thisnode = shared_from_this();
        return static_cast<ast_node_overloadset*>(this)->
            fnlist.add_overload(nd,thisnode);
    }
    return makeerr(err::EC_NODE_HAS_NO_FUNCTIONLIST);
}

i64                             // count of overloads or -1 if not overload set
ast_node::get_overload_count() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_OVERLOADSET)
        return static_cast<const ast_node_overloadset*>(this)->
            fnlist.get_count();

    return -1;
}

ast_node_ptr ast_node::get_overload(i64 index) const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_OVERLOADSET)
        return static_cast<const ast_node_overloadset*>(this)->
            fnlist.get_function(index);

    return nullptr;
}

// Add an overload and get an overload backref
ast_node_backref ast_node::get_overloadset_backref() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF)
        return static_cast<ast_node_functionbase const *>(this)->
            overloadset_backref.get();

    return ast_node_backref(nullptr,-1);
}

// Add and retrieve the qualified name of the function at definition point
exp_none ast_node::set_fundef_qualified_name(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_QUNAME_TO_ADD_IS_NULL,
                       effectloc);
    if (!nd->is_complete())
        return makeerr(err::EC_AST_QUALIFIED_NAME_IS_NOT_COMPLETE,
                       effectloc);

    if (node_type == AST_FUNDEF)
    {
        // fixme check that nd is a proper qualified_name
        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_fundef *>(this)->qualified_name,nd);
        SET_OWNED(nd);
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_fundef_qualified_name() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF)
        return static_cast<const ast_node_fundef *>(this)->qualified_name;

    return nullptr;
}

// Add and retrieve the function parameter list
exp_none ast_node::set_funparlist(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_FUNPARLIST_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF ||
        node_type == AST_LAMBDA)
    {
        ast_node_ptr thisnode = shared_from_this();
        ast_node_lambdabase *lambda =
            static_cast<ast_node_lambdabase*>(thisnode.get());

        // Check that nd is a proper funparlist
        if (nd->node_type != AST_FUNPARLIST)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // Add the backref
        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(lambda->funparlist,nd);
        auto exp = static_cast<ast_node_funparlist*>(nd.get())->
            set_lambdabackref(thisnode);
        if (!exp)
            return forwarderr(exp);
        lambda->funparlist = nd;
        SET_OWNED(nd);
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_funparlist() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF ||
        node_type == AST_LAMBDA)
        return static_cast<const ast_node_lambdabase*>(this)->funparlist;

    return nullptr;
}

// Add and retrieve the function return parameter list
exp_none ast_node::set_funretlist(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_FUNRETLIST_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF ||
        node_type == AST_LAMBDA)
    {
        ast_node_ptr thisnode = shared_from_this();
        ast_node_lambdabase *lambda =
            static_cast<ast_node_lambdabase*>(thisnode.get());

        // Check that nd is a proper funretlist
        if (nd->node_type != AST_FUNRETLIST)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // Add the backref
        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(lambda->funretlist,nd);
        auto exp = static_cast<ast_node_funretlist*>(nd.get())->
            set_lambdabackref(thisnode);
        if (!exp)
            return forwarderr(exp);
        lambda->funretlist = nd;
        SET_OWNED(nd);
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_funretlist() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF ||
        node_type == AST_LAMBDA)
        return static_cast<const ast_node_lambdabase*>(this)->funretlist;

    return nullptr;
}

// Add and retrieve the function body
exp_none ast_node::set_funbody(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_FUNBODY_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FUNDEF ||
        node_type == AST_LAMBDA)
    {
        ast_node_ptr thisnode = shared_from_this();
        ast_node_lambdabase *lambda =
            static_cast<ast_node_lambdabase*>(thisnode.get());

        // Check that nd is a proper function body
        if (nd->node_type != AST_FUNBODY)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // Add the backref
        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(lambda->funbody,nd);
        auto exp = static_cast<ast_node_funbody*>(nd.get())->
            set_lambdabackref(thisnode);
        if (!exp)
            return forwarderr(exp);

        lambda->funbody = nd;
        SET_OWNED(nd);
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_funbody() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF ||
        node_type == AST_LAMBDA)
        return static_cast<const ast_node_lambdabase*>(this)->funbody;

    return nullptr;
}

// Lambda-backref for funparlist, funretlist, funbody
ast_node_ptr ast_node::get_lambda_backref() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNPARLIST)
        return static_cast<ast_node_funparlist const *>(this)->lambdabr.get();
    else if (node_type == AST_FUNRETLIST)
        return static_cast<ast_node_funretlist const *>(this)->lambdabr.get();
    else if (node_type == AST_FUNBODY)
        return static_cast<ast_node_funbody const *>(this)->lambdabr.get();
    else
        return nullptr;
}

exp_none ast_node::set_fundef(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_FUNDEF_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FUNDECL)
    {
        ast_node_ptr thisnode = shared_from_this();
        ast_node_fundecl *fundecl =
            static_cast<ast_node_fundecl *>(this);

        // Check that nd is a proper function definition
        if (nd->node_type != AST_FUNDEF)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // Set the backref
        ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
        CHECK_SLOT(fundecl->fundef,nd);
        ast_node_fundef *fundef =
            static_cast<ast_node_fundef *>(nd.get());
        auto exp = fundef->fundecl.set(thisnode);
        if (!exp)
            return forwarderr(exp);

        // Set the forward ref
        fundecl->fundef = nd;
        SET_LINKED_FROM_DECL(nd);

        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_fundef() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDECL)
        return static_cast<const ast_node_fundecl*>(this)->fundef;

    return nullptr;
}

// Set and get the function definition in a function declaration
ast_node_ptr ast_node::get_fundecl_backref() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF)
        return static_cast<ast_node_fundef const *>(this)->fundecl.get();

    return nullptr;
}

// Recursively resolve a function name.
// The qualified name must omit the last (function name) component.
// The start_namespace must be a namespace.
ast_node_ptr recursively_resolve_functionname(ast_node_ptr qualified_name,
                                              ast_node_ptr start_namespace,
                                              err &e)
{
    using enum ast::ast_node_type;

    ast_node_ptr outer_scope;
    str name;

    ast_node_type qunament = qualified_name->get_node_type();
    if (qunament == AST_EXPR_MEMBER)
    {
        ast_node_ptr prefixname = qualified_name->get_subexpr1();
        if (prefixname == nullptr)
        {
            // An algorithmic error: a MEMBER should have a subexpr1
            e = err(err::EC_CANNOT_RESOLVE_FUNCTION_NAME);
            return nullptr;
        }
        outer_scope = recursively_resolve_functionname(prefixname,
                                                       start_namespace,
                                                       e);
        if (outer_scope == nullptr)
            // e is already set from the call
            return nullptr;
        name = qualified_name->get_member_name();
    }
    else if (qunament == AST_EXPR_UNQUALNAME)
    {
        // The function must be called with a namespace
        auto starttype = start_namespace->get_node_type();
        if (starttype != AST_GLOBAL &&
            starttype != AST_NAMESPACE)
        {
            e = err(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                    qualified_name->get_effectloc());
            return nullptr;
        }
        outer_scope = start_namespace;
        name = qualified_name->get_unqualified_name();
    }
    else
    {
        e = err(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                qualified_name->get_effectloc());
        return nullptr;
    }

    // Look up the member name in outer
    i64 index = outer_scope->lookup_name(name);
    if (index == -1)
    {
        e = err(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                qualified_name->get_effectloc());
        return nullptr;
    }

    ast_node_ptr inner_scope = outer_scope->get_subnode(index);
    if (inner_scope == nullptr)
    {
        e = err(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                qualified_name->get_effectloc());
    }

    // Check that inner_scope is a namespace or class
    ast_node_type tt = inner_scope->get_node_type();
    if (tt == AST_CLASSDECL)
    {
        ast_node_ptr classdef = inner_scope->get_classdef();
        if (classdef == nullptr)
        {
            e = err(err::EC_CLASS_NOT_YET_DEFINED,
                    qualified_name->get_effectloc());
            return nullptr;
        }
        if (classdef->get_node_type() != AST_CLASSDEF)
        {
            e = err(err::EC_CLASS_NOT_YET_DEFINED,
                    qualified_name->get_effectloc());
            return nullptr;
        }
        return classdef;
    }
    else if (tt != AST_NAMESPACE &&
             tt != AST_CLASSDEF)
    {
        e = err(err::EC_FUNCTION_NAME_COMPONENT_ILLEGAL,
                qualified_name->get_effectloc());
        return nullptr;
    }
    return inner_scope;
}


// Add a function as a subnode and link it into the tree
exp_i64 ast_node::add_function(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    ast_node_global *global = nullptr;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_FUNCTION_TO_ADD_IS_NULL,
                       effectloc);

    // Check the scope where declared/defined
    if (node_type != AST_GLOBAL &&
        node_type != AST_NAMESPACE &&
        node_type != AST_CLASSDEF)
        return makeerr(err::EC_NODE_HAS_NO_OVERLOADSET,
                       nd->get_startloc());

    // Get the global namespace
    if (node_type == AST_GLOBAL ||
        node_type == AST_NAMESPACE)
    {
        global = static_cast<ast_node_br*>(this)->get_global_from_namespace();
        if (global == nullptr)
            return makeerr(err::EC_NAMESPACE_NOT_IN_GLOBAL,
                           nd->get_effectloc());
    }

    // Check the function node
    ast_node_type fntype = nd->get_node_type();
    if (fntype != AST_FUNDECL &&
        fntype != AST_FUNDEF)
        return makeerr(err::EC_ASTNODE_BADADDTYPE);

    // Check that the fundecl/fundef is not already owned
    ENSURE_NOT_YET_OWNED(nd);
    ENSURE_NOT_YET_LINKED_FROM_DECL(nd);
    ENSURE_NOT_YET_LINKED_FROM_OVERLOADSET(nd);

    // Get the function name
    str funcname = nd->get_name();

    // Check static/nonstatic consistency
    auto expflags = nd->get_flags();
    if (!expflags)
        return makealgorithmic();
    auto newflags = expflags.value();
    if ((newflags & AST_FLAG_STATIC) != 0 &&
        (newflags & AST_FLAG_NONSTATIC) != 0)
        return makeerr(err::EC_CONTRADICTING_STATIC_SPECIFICATION,
                       nd->get_effectloc());

    // Get the function parameter list
    ast_node_ptr parlist = nd->get_funparlist();
    if (parlist == nullptr)
        return makeerr(err::EC_FUNCTIONDATA_WITHOUT_PARLIST);
    ast_node_ptr retlist = nd->get_funretlist();
    if (retlist == nullptr)
        return makeerr(err::EC_FUNCTIONDATA_WITHOUT_RETLIST);
    if (fntype == AST_FUNDEF)
    {
        if (nd->get_funbody() == nullptr)
            return makeerr(err::EC_FUNCTIONDATA_WITHOUT_FUNBODY);
    }


    // Find the insert scope (namespace or class) that holds the overload set
    // to which the function shall be added

    ast_node_ptr insert_scope = thisnode; // Start at current scope
    bool definition_outside_scope = false;

    // A qualified name is not allowed with a declaration
    ast_node_ptr qualified_name = nd->get_fundef_qualified_name();
    if (qualified_name != nullptr && fntype == AST_FUNDECL)
        return makeerr(err::EC_FUNCTION_DECLARATION_NEEDS_SIMPLE_NAME,
                       qualified_name->get_startloc());

    // If a function definition has a non-simple qualified name,
    // adjust insert_scope
    if (fntype == AST_FUNDEF)
    {
        if (qualified_name != nullptr)
        {
            ast_node_type qutype = qualified_name->get_node_type();
            ast_node_ptr outer_name = qualified_name->get_subexpr1();
            str compare_name;
            if (outer_name != nullptr)
            {
                if (qutype != AST_EXPR_MEMBER)
                    return makeerr(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                                   qualified_name->get_startloc());
                compare_name = qualified_name->get_member_name();

                // Any insert from an outer scope must happen in a namespace
                if (node_type != AST_GLOBAL &&
                    node_type != AST_NAMESPACE)
                    return makeerr(err::EC_FUNCTION_DEFINITION_NOT_IN_NAMESPACE,
                                   qualified_name->get_startloc());
                cig::err e(err::E_SUCCESS);
                insert_scope = recursively_resolve_functionname(outer_name,
                                                                thisnode,
                                                                e);
                if (insert_scope == nullptr)
                    return std::unexpected(e);
                definition_outside_scope = true;
            }
            else
            {
                // Keep insert_scope
                if (qutype != AST_EXPR_UNQUALNAME)
                    return makeerr(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                                   qualified_name->get_startloc());
                compare_name = qualified_name->get_unqualified_name();
            }
            if (compare_name != funcname)
                return makeerr(err::EC_CANNOT_RESOLVE_FUNCTION_NAME,
                               qualified_name->get_startloc());
        }
    }
    // We got the final insert_scope that holds the overload set,
    // and it is a namespace or class.


    // Get the overload set
    i64 overload_set_index = insert_scope->lookup_name(funcname);
    ast_node_ptr overload_set;
    if (overload_set_index != -1)
    {
        // Get an existing overload set
        overload_set = insert_scope->get_subnode(overload_set_index);
        if (overload_set == nullptr)
            return makeerr(err::EC_ALGORITHMIC);
        if (overload_set->get_node_type() != AST_OVERLOADSET)
        {
            return makeerr(err::EC_FUNCTION_NAME_ALREADY_USED,
                           nd->get_effectloc(),
                           overload_set->get_effectloc());
        }
    }
    else if (!definition_outside_scope)
    {
        // Create an overload set
        overload_set = ast_node::make_ast_node(AST_OVERLOADSET,
                                               nd->get_startloc(),
                                               nd->get_effectloc(),
                                               nd->get_endloc());
        cig_assert(overload_set);
        auto exp1 = overload_set->set_name(funcname);
        cig_assert(exp1);
        auto exp8 = insert_scope->add_subnode(overload_set);
        cig_assert(exp8);
        SET_OWNED(overload_set);
        overload_set_index = exp8.value();
        auto exp9 = insert_scope->add_lookup_name(funcname,overload_set_index);
        cig_assert(exp9);
    }
    else
        return makeerr(err::EC_FUNCTION_DEFINITION_WITHOUT_DECLARATION,
                       nd->get_effectloc());

    // Check if a declaration is present in the overload set
    ast_node_ptr fundecl = nullptr;
    for (i64 ifn = 0; ; ifn = ifn + 1)
    {
        fundecl = overload_set->get_overload(ifn);
        if (fundecl == nullptr) // end of list reached
            break;

        auto oldparlist = fundecl->get_funparlist();
        if (oldparlist == nullptr)
            return makeerr(err::EC_FUNCTIONDATA_WITHOUT_PARLIST);

        // Compare the oldparlist to the parlist
        if (funparlists_have_same_typerefs(oldparlist,parlist))
            break;
    }
    // Here, fundecl is the previous declaration or definition

    if (fundecl)
    {
        // Compare the function return type lists
        auto oldretlist = fundecl->get_funretlist();
        if (oldretlist == nullptr)
            return makeerr(err::EC_FUNCTIONDATA_WITHOUT_RETLIST);
        ast_node_type oldfntype = fundecl->get_node_type();
        if (oldfntype != AST_FUNDECL &&
            oldfntype != AST_FUNDEF)
            return makeerr(err::EC_FUNCTIONDATA_WITH_BAD_NODE_TYPE);

        if (!funretlists_have_same_typerefs(oldretlist,retlist))
            return makeerr(err::EC_RETLIST_MISMATCH,
                           retlist->get_effectloc(),
                           oldretlist->get_effectloc());

        // Compare the flags
        auto exp2 = fundecl->get_flags();
        if (!exp2)
            return makealgorithmic();
        auto oldflags = exp2.value();
        // Mismatch in static/nonstatic flags
        if (DEFAULT_FUNCTION_IS_STATIC)
        {
            if (!(oldflags & AST_FLAG_NONSTATIC))
                oldflags |= AST_FLAG_STATIC;
            if (!(newflags & AST_FLAG_NONSTATIC))
                newflags |= AST_FLAG_STATIC;
        }
        else
        {
            if (!(oldflags & AST_FLAG_STATIC))
                oldflags |= AST_FLAG_NONSTATIC;
            if (!(newflags & AST_FLAG_STATIC))
                newflags |= AST_FLAG_NONSTATIC;
        }
        if ((oldflags & AST_FLAG_STATIC) != (newflags & AST_FLAG_STATIC) ||
            (oldflags & AST_FLAG_NONSTATIC) != (newflags & AST_FLAG_NONSTATIC))
            return makeerr(err::EC_CONTRADICTING_STATIC_SPECIFICATION,
                           nd->get_effectloc(),
                           fundecl->get_effectloc());
        // Mismatch in other flags
        if (oldflags != newflags)
            return makeerr(err::EC_FLAGS_MISMATCH,
                           nd->get_effectloc(),
                           fundecl->get_effectloc());

        // Both cannot be definitions
        if (oldfntype == AST_FUNDEF &&
            fntype == AST_FUNDEF)
            return makeerr(err::EC_FUNCTION_ALREADY_DEFINED,
                           nd->get_effectloc(),
                           fundecl->get_effectloc());

        // If the new nd is a declaration, discard it as a duplicate
        // Return an index of -1 but success
        if (fntype == AST_FUNDECL)
            return -1;

        cig_assert(fntype == AST_FUNDEF);
        cig_assert(oldfntype == AST_FUNDECL);

        // Now add the function definition to the function declaration
        auto exp4 = fundecl->set_fundef(nd);
        if (!exp4)
            return makeerr(exp4.error().get_error_code(),
                           nd->get_effectloc(),
                           fundecl->get_effectloc());
    }
    else
    {
        // Add the function definition or declaration to the overload set
        auto exp5 = overload_set->add_overload(nd);
        if (!exp5)
            return makeerr(exp5.error().get_error_code(),
                           nd->get_effectloc(),
                           overload_set->get_effectloc());
        SET_LINKED_FROM_OVERLOADSET(nd);
    }
    

    // Finally, add the function definition or declaration
    // at the point where it occurs in the source code
    // (at the end of 'this').


    if (node_type == AST_GLOBAL)
    {
        auto exp6 = static_cast<ast_node_global*>(this)->
            subnodelist.add_subnode(nd,thisnode);
        if (!exp6)
            return makeerr(exp6.error().get_error_code(),
                           nd->get_effectloc());
        i64 index = exp6.value();
        SET_OWNED(nd);

        // Add the function to the global node
        if (global)
        {
            auto expindexg = global->add_global_subnode(nd);
            if (!expindexg)
                return expindexg;
        }

        return index;
    }
    if (node_type == AST_NAMESPACE)
    {
        auto exp6 = static_cast<ast_node_namespace*>(this)->
            subnodelist.add_subnode(nd,thisnode);
        if (!exp6)
            return makeerr(exp6.error().get_error_code(),
                           nd->get_effectloc());
        i64 index = exp6.value();
        SET_OWNED(nd);

        // Add the function to the global node
        if (global)
        {
            auto expindexg = global->add_global_subnode(nd);
            if (!expindexg)
                return expindexg;
        }

        return index;
    }
    if (node_type == AST_CLASSDEF)
    {
        auto exp7 = static_cast<ast_node_classdef*>(this)->
            subnodelist.add_subnode(nd,thisnode);
        if (!exp7)
            return makeerr(exp7.error().get_error_code(),
                           nd->get_effectloc());
        i64 index = exp7.value();
        SET_OWNED(nd);
        return index;
    }

    // This cannot happen, the node_type has been checked above
    return makealgorithmic();
}



//
// Auxiliary functions for ULVALUE and RVALUE error messages
//


namespace detail {

static
cig::source_location
ast_node_defining_rvalue_loc(
    ast_node const &nd
    )
{
    using enum ast::ast_node_type;

    // For conditional expressions, return the location
    // of the first alternative that is an rvalue
    if (nd.get_node_type() == AST_EXPR_CONDITION)
    {
        auto expr2ptr = nd.get_subexpr2();
        if (expr2ptr)
        {
            ast_node_x const &expr2 =
                *static_cast<ast_node_x const *>(expr2ptr.get());
            if ((expr2.x & ast_node_x::X_HAS_RVALUE) != 0)
                return ast_node_defining_rvalue_loc(expr2);
        }
        auto expr3ptr = nd.get_subexpr3();
        if (expr3ptr)
        {
            ast_node_x const &expr3 =
                *static_cast<ast_node_x const *>(expr3ptr.get());
            if ((expr3.x & ast_node_x::X_HAS_RVALUE) != 0)
                return ast_node_defining_rvalue_loc(expr3);
        }
    }
    // For list expressions, return the location
    // of the first element that is an rvalue
    else if (nd.get_node_type() == AST_EXPR_LIST)
    {
        i64 n = nd.get_listsubexpr_count();
        for (i64 i = 0; i < n; i += 1)
        {
            auto exprptr = nd.get_listsubexpr(i);
            if (exprptr)
            {
                ast_node_x const &expr =
                    *static_cast<ast_node_x const *>(exprptr.get());
                if ((expr.x & ast_node_x::X_HAS_RVALUE) != 0)
                    return ast_node_defining_rvalue_loc(expr);
            }
        }
    }
    return nd.get_startloc();
}


static
cig::source_location
ast_node_defining_ulvalue_loc(
    ast_node const &nd
    )
{
    using enum ast::ast_node_type;

    // For conditional expressions, return the location
    // of the first alternative that is an ulvalue
    if (nd.get_node_type() == AST_EXPR_CONDITION)
    {
        auto expr2ptr = nd.get_subexpr2();
        if (expr2ptr)
        {
            ast_node_x const &expr2 =
                *static_cast<ast_node_x const *>(expr2ptr.get());
            if ((expr2.x & ast_node_x::X_HAS_ULVALUE) != 0)
                return ast_node_defining_ulvalue_loc(expr2);
        }
        auto expr3ptr = nd.get_subexpr3();
        if (expr3ptr)
        {
            ast_node_x const &expr3 =
                *static_cast<ast_node_x const *>(expr3ptr.get());
            if ((expr3.x & ast_node_x::X_HAS_ULVALUE) != 0)
                return ast_node_defining_ulvalue_loc(expr3);
        }
    }
    // For list expressions, return the location
    // of the first element that is an ulvalue
    else if (nd.get_node_type() == AST_EXPR_LIST)
    {
        i64 n = nd.get_listsubexpr_count();
        for (i64 i = 0; i < n; i += 1)
        {
            auto exprptr = nd.get_listsubexpr(i);
            if (exprptr)
            {
                ast_node_x const &expr =
                    *static_cast<ast_node_x const *>(exprptr.get());
                if ((expr.x & ast_node_x::X_HAS_ULVALUE) != 0)
                    return ast_node_defining_ulvalue_loc(expr);
            }
        }
    }
    return nd.get_startloc();
}


static
cig::source_location
ast_node_defining_conditional_loc_aux(
    ast_node_ptr nd             // neither list nor assignment
    )
{
    using enum ast::ast_node_type;

    if (nd->get_node_type() == AST_EXPR_CONDITION)
        return nd->get_effectloc();
    auto sub1 = nd->get_subexpr1();
    if (sub1 && sub1->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub1);
    auto sub2 = nd->get_subexpr2();
    if (sub2 && sub2->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub2);
    auto sub3 = nd->get_subexpr3();
    if (sub3 && sub3->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub3);
    auto sub4 = nd->get_subexpr4();
    if (sub4 && sub4->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub4);
    return cig::source_location();
}


static
cig::source_location
ast_node_defining_conditional_loc(
    ast_node const &nd
    )
{
    using enum ast::ast_node_type;

    if (nd.get_node_type() == AST_EXPR_CONDITION)
    {
        return nd.get_effectloc();
    }
    if (nd.get_node_type() == AST_EXPR_LIST)
    {
        i64 n = nd.get_listsubexpr_count();
        for (i64 i = 0; i < n; i += 1)
        {
            auto exprptr = nd.get_listsubexpr(i);
            if (exprptr && exprptr->is_conditional())
                return ast_node_defining_conditional_loc_aux(exprptr);
        }
        return cig::source_location();
    }
    auto sub1 = nd.get_subexpr1();
    if (sub1 && sub1->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub1);
    auto sub2 = nd.get_subexpr2();
    if (sub2 && sub2->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub2);
    auto sub3 = nd.get_subexpr3();
    if (sub3 && sub3->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub3);
    auto sub4 = nd.get_subexpr4();
    if (sub4 && sub4->is_conditional())
        return ast_node_defining_conditional_loc_aux(sub4);
    return cig::source_location();
}


static
cig::source_location
ast_node_ptr_defining_rvalue_loc(
    ast_node_ptr nd             // the node which must be an rvalue
    )
{
    if (!nd)
        return cig::source_location();
    return ast_node_defining_rvalue_loc(*nd.get());
}


static
cig::source_location
ast_node_ptr_defining_ulvalue_loc(
    ast_node_ptr nd             // the node which must be an ulvalue
    )
{
    if (!nd)
        return cig::source_location();
    return ast_node_defining_ulvalue_loc(*nd.get());
}


static
cig::source_location
ast_node_ptr_defining_conditional_loc(
    ast_node_ptr nd             // the node which must be an ulvalue
    )
{
    if (!nd)
        return cig::source_location();
    return ast_node_defining_conditional_loc(*nd.get());
}

}


//
// Statements
//


// Add a statement to a scope or function body, return the index
i64                             // count of substatements or -1 if bad node type
ast_node::get_substmt_count() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_COMPOUNDSTMT)
    {
        return static_cast<const ast_node_compound_statement*>(this)->
            subnodelist.get_count();
    }
    if (node_type == AST_FUNBODY)
    {
        return static_cast<const ast_node_funbody*>(this)->
            subnodelist.get_count();
    }
    return -1;
}


exp_i64 ast_node::add_substmt(ast_node_ptr stmt)
{
    using enum ast::ast_node_type;

    // Argument check
    if (stmt == nullptr)
        return makeerr(err::EC_AST_NODE_SUBSTMT_TO_ADD_IS_NULL,
                       effectloc);

    ast_node_type stmttype = stmt->get_node_type();

    if (node_type == AST_COMPOUNDSTMT)
    {
        if (!ast_node_type_is_stmt(stmttype))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        ENSURE_NOT_YET_OWNED(stmt);

        // Register the variables of the statement in the compound
        if (stmttype == AST_EXPRSTMT)
        {
            if (!stmt->is_complete())
                return makeerr(err::EC_AST_EXPRSTMT_IS_NOT_COMPLETE,
                               effectloc);
            ast_node_ptr thisnode = shared_from_this();
            auto exp = detail::
                ast_node_recursive_variable_registration(thisnode,
                                                         stmt->get_expr());
            if (!exp)
                return forwarderr(exp);
        }

        auto expindex = add_subnode(stmt);
        if (!expindex)
            return expindex;
        SET_OWNED(stmt);
        return expindex;
    }
    if (node_type == AST_FUNBODY)
    {
        if (stmttype != AST_COMPOUNDSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        ENSURE_NOT_YET_OWNED(stmt);
        if (get_subnode_count() != 0)
        {
            return makeerr(err::EC_SLOT_TO_ADD_NOT_FREE,stmt->effectloc);
        }
        auto expindex = add_subnode(stmt);
        if (!expindex)
            return expindex;
        SET_OWNED(stmt);
        return expindex;
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}


ast_node_ptr                    // ast_node_ptr or nullptr
ast_node::get_substmt(i64 index) const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNBODY ||
        node_type == AST_COMPOUNDSTMT)
        return get_subnode(index);
    return nullptr;
}


// Set and get the statement's expression (return stmt and expression stmt)
exp_none ast_node::set_expr(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_STMTEXPR_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_RETURNSTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_EXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // The return statement must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_RETURN_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_return_statement*>(this)->expr,nd);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_EXPRSTMT)
    {
        // Check that nd is an expression
        if (!ast_node_type_is_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_EXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // no checks of VALUE required

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_expression_statement*>(this)->expr,nd);
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_expr() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_RETURNSTMT)
        return static_cast<const ast_node_return_statement*>(this)->expr;

    if (node_type == AST_EXPRSTMT)
        return static_cast<const ast_node_expression_statement*>(this)->expr;

    return nullptr;
}


// Set and get the statement's condition
exp_none ast_node::set_condexpr(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

#if EXPRESSIONS_HAVE_BACKREFS
    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_COND;
#endif

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_CONDEXPR_TO_ADD_IS_NULL,
                       effectloc);
    if (!nd->is_complete())
        return makeerr(err::EC_AST_CONDEXPRESSION_IS_NOT_COMPLETE,
                       effectloc);

    if (node_type == AST_IFSTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // The if condition must not be list-valued
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_IF_CONDITION_DISALLOWS_LISTVAL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        // The if condition must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_IF_CONDITION_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_if_statement*>(this)->condexpr,nd);
#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_WHILESTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // The while condition must not be list-valued
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_WHILE_CONDITION_DISALLOWS_LISTVAL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        // The while condition must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_WHILE_CONDITION_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_while_statement*>(this)->condexpr,nd);
#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_DOWHILESTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // The do..while condition must not be list-valued
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_DOWHILE_CONDITION_DISALLOWS_LISTVAL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        // The do..while condition must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_DOWHILE_CONDITION_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_do_while_statement*>(this)->condexpr,nd);
#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_FORSTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // The for condition must not be list-valued
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_FOR_CONDITION_DISALLOWS_LISTVAL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        // The for condition must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_FOR_CONDITION_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_for_statement*>(this)->condexpr,nd);
#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_condexpr() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_IFSTMT)
        return static_cast<const ast_node_if_statement*>(this)->condexpr;
    if (node_type == AST_WHILESTMT)
        return static_cast<const ast_node_while_statement*>(this)->condexpr;
    if (node_type == AST_DOWHILESTMT)
        return static_cast<const ast_node_do_while_statement*>(this)->condexpr;
    if (node_type == AST_FORSTMT)
        return static_cast<const ast_node_for_statement*>(this)->condexpr;

    return nullptr;
}


// Set and get the statement's body statement
exp_none ast_node::set_bodystmt(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_BODY;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_BODY_TO_ADD_IS_NULL,
                       effectloc);
    if (!nd->is_complete())
        return makeerr(err::EC_AST_BODYSTMT_IS_NOT_COMPLETE,
                       effectloc);

    if (node_type == AST_WHILESTMT)
    {
        // Check that nd is a statement
        if (!ast_node_type_is_stmt(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // If the body is an expression statement,
        // it must not contain variable declarations
        if (nd->node_type == AST_EXPRSTMT)
        {
            ast_node_ptr expr = nd->get_expr();
            if (expr && NODE_IS_ULVALUE(expr))
            {
                return makeerr(
                    err::EC_WHILE_BODY_VARDECL_NEEDS_COMPOUND,
                    detail::ast_node_ptr_defining_ulvalue_loc(expr));
            }
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_while_statement*>(this)->bodystmt,nd);
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_DOWHILESTMT)
    {
        // Check that nd is a statement
        if (!ast_node_type_is_stmt(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // If the body is an expression statement,
        // it must not contain variable declarations
        if (nd->node_type == AST_EXPRSTMT)
        {
            ast_node_ptr expr = nd->get_expr();
            if (expr && NODE_IS_ULVALUE(expr))
            {
                return makeerr(
                    err::EC_DOWHILE_BODY_VARDECL_NEEDS_COMPOUND,
                    detail::ast_node_ptr_defining_ulvalue_loc(expr));
            }
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_do_while_statement*>(this)->bodystmt,nd);
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_FORSTMT)
    {
        // Check that nd is a statement
        if (!ast_node_type_is_stmt(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // If the body is an expression statement,
        // it must not contain variable declarations
        if (nd->node_type == AST_EXPRSTMT)
        {
            ast_node_ptr expr = nd->get_expr();
            if (expr && NODE_IS_ULVALUE(expr))
            {
                return makeerr(
                    err::EC_FOR_BODY_VARDECL_NEEDS_COMPOUND,
                    detail::ast_node_ptr_defining_ulvalue_loc(expr));
            }
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_for_statement*>(this)->bodystmt,nd);
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_FOROFSTMT)
    {
        // Check that nd is a statement
        if (!ast_node_type_is_stmt(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        // If the body is an expression statement,
        // it must not contain variable declarations
        if (nd->node_type == AST_EXPRSTMT)
        {
            ast_node_ptr expr = nd->get_expr();
            if (expr && NODE_IS_ULVALUE(expr))
            {
                return makeerr(
                    err::EC_FOROF_BODY_VARDECL_NEEDS_COMPOUND,
                    detail::ast_node_ptr_defining_ulvalue_loc(expr));
            }
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_forof_statement*>(this)->bodystmt,nd);
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_bodystmt() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_WHILESTMT)
        return static_cast<const ast_node_while_statement*>(this)->bodystmt;
    if (node_type == AST_DOWHILESTMT)
        return static_cast<const ast_node_do_while_statement*>(this)->bodystmt;
    if (node_type == AST_FORSTMT)
        return static_cast<const ast_node_for_statement*>(this)->bodystmt;
    if (node_type == AST_FOROFSTMT)
        return static_cast<const ast_node_forof_statement*>(this)->bodystmt;

    return nullptr;
}


// Set and get the if statement's then statement
exp_none ast_node::set_thenstmt(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_THEN;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_THEN_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_IFSTMT)
    {
        // Check that nd is a statement
        if (!ast_node_type_is_stmt(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_THENSTMT_IS_NOT_COMPLETE,
                           effectloc);

        // If the then statement is an expression statement,
        // it must not contain variable declarations
        if (nd->node_type == AST_EXPRSTMT)
        {
            ast_node_ptr expr = nd->get_expr();
            if (expr && NODE_IS_ULVALUE(expr))
            {
                return makeerr(
                    err::EC_IF_THEN_VARDECL_NEEDS_COMPOUND,
                    detail::ast_node_ptr_defining_ulvalue_loc(expr));
            }
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_if_statement*>(this)->thenstmt,nd);
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_thenstmt() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_IFSTMT)
        return static_cast<const ast_node_if_statement*>(this)->thenstmt;

    return nullptr;
}


// Set and get the if statement's else statement
exp_none ast_node::set_elsestmt(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_ELSE;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_ELSE_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_IFSTMT)
    {
        // Check that nd is a statement
        if (!ast_node_type_is_stmt(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_ELSESTMT_IS_NOT_COMPLETE,
                           effectloc);

        // If the else statement is an expression statement,
        // it must not contain variable declarations
        if (nd->node_type == AST_EXPRSTMT)
        {
            ast_node_ptr expr = nd->get_expr();
            if (expr && NODE_IS_ULVALUE(expr))
            {
                return makeerr(
                    err::EC_IF_ELSE_VARDECL_NEEDS_COMPOUND,
                    detail::ast_node_ptr_defining_ulvalue_loc(expr));
            }
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_if_statement*>(this)->elsestmt,nd);
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_elsestmt() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_IFSTMT)
        return static_cast<const ast_node_if_statement*>(this)->elsestmt;

    return nullptr;
}


// Set and get the for statement's iterate expression
exp_none ast_node::set_iterateexpr(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

#if EXPRESSIONS_HAVE_BACKREFS
    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_ITERATE;
#endif

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_ITERATE_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FORSTMT)
    {
        // Check that nd is an expression
        if (!ast_node_type_is_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_ITERATEEXPR_IS_NOT_COMPLETE,
                           effectloc);

        // The iterate expression must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_ITERATEEXPR_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_for_statement*>(this)->iterateexpr,nd);
#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_iterateexpr() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FORSTMT)
        return static_cast<const ast_node_for_statement*>(this)->iterateexpr;

    return nullptr;
}


// Set and get the init statement, an expression statement
exp_none ast_node::set_initstmt(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_INIT;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_INITSTMT_TO_ADD_IS_NULL,
                       effectloc);
    if (!nd->is_complete())
        return makeerr(err::EC_AST_INITSTMT_IS_NOT_COMPLETE,
                       effectloc);

    if (node_type == AST_IFSTMT)
    {
        // Check that nd is an expression statement
        if (nd->node_type != AST_EXPRSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        auto thisastnd = static_cast<ast_node_if_statement*>(this);

        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(thisastnd->initstmt,nd);

        // Register the variables of the statement in the subnodelist
        auto exp = detail::
            ast_node_recursive_variable_registration(thisnode,
                                                     nd->get_expr());
        if (!exp)
            return forwarderr(exp);
        
        thisastnd->initstmt = nd;
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_WHILESTMT)
    {
        // Check that nd is an expression statement
        if (nd->node_type != AST_EXPRSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        auto thisastnd = static_cast<ast_node_while_statement*>(this);

        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(thisastnd->initstmt,nd);

        // Register the variables of the statement in the subnodelist
        auto exp = detail::
            ast_node_recursive_variable_registration(thisnode,
                                                     nd->get_expr());
        if (!exp)
            return forwarderr(exp);
        
        thisastnd->initstmt = nd;
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_DOWHILESTMT)
    {
        // Check that nd is an expression statement
        if (nd->node_type != AST_EXPRSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        auto thisastnd = static_cast<ast_node_do_while_statement*>(this);

        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(thisastnd->initstmt,nd);

        // Register the variables of the statement in the subnodelist
        auto exp = detail::
            ast_node_recursive_variable_registration(thisnode,
                                                     nd->get_expr());
        if (!exp)
            return forwarderr(exp);
        
        thisastnd->initstmt = nd;
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_FORSTMT)
    {
        // Check that nd is an expression statement
        if (nd->node_type != AST_EXPRSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        auto thisastnd = static_cast<ast_node_for_statement*>(this);

        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(thisastnd->initstmt,nd);

        // Register the variables of the statement in the subnodelist
        auto exp = detail::
            ast_node_recursive_variable_registration(thisnode,
                                                     nd->get_expr());
        if (!exp)
            return forwarderr(exp);
        
        thisastnd->initstmt = nd;
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    if (node_type == AST_FOROFSTMT)
    {
        // Check that nd is an expression statement
        if (nd->node_type != AST_EXPRSTMT)
            return makeerr(err::EC_ASTNODE_BADADDTYPE);

        auto thisastnd = static_cast<ast_node_forof_statement*>(this);

        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(thisastnd->initstmt,nd);

        // The init statement must be set before the iterator expression
        if (thisastnd->iterator_expr)
        {
            return makeerr(err::EC_MUST_SET_INIT_STMT_BEFORE_ITERATOR,
                           nd->get_startloc(),
                           thisastnd->iterator_expr->get_startloc());
        }

        // Register the variables of the statement in the subnodelist
        auto exp = detail::
            ast_node_recursive_variable_registration(thisnode,
                                                     nd->get_expr());
        if (!exp)
            return forwarderr(exp);
        // Remember the variables in the init statement
        // because more variables might be added in the iterator statement
        thisastnd->n_subnode_initstmt = thisastnd->subnodelist.get_count();
        
        thisastnd->initstmt = nd;
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_initstmt() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_IFSTMT)
        return static_cast<const ast_node_if_statement*>(this)->initstmt;
    if (node_type == AST_WHILESTMT)
        return static_cast<const ast_node_while_statement*>(this)->initstmt;
    if (node_type == AST_DOWHILESTMT)
        return static_cast<const ast_node_do_while_statement*>(this)->initstmt;
    if (node_type == AST_FORSTMT)
        return static_cast<const ast_node_for_statement*>(this)->initstmt;
    if (node_type == AST_FOROFSTMT)
        return static_cast<const ast_node_forof_statement*>(this)->initstmt;

    return nullptr;
}


i64                             // count of variables in init statement or 0
ast_node::get_initstmt_variables() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FOROFSTMT)
        return static_cast<const ast_node_forof_statement*>(this)->
            n_subnode_initstmt;
    else if (node_type == AST_IFSTMT ||
             node_type == AST_WHILESTMT ||
             node_type == AST_DOWHILESTMT ||
             node_type == AST_FORSTMT)
        return get_subnode_count();
    else
        return 0;
}


// Set and get the for-in statement's iterator expression
exp_none ast_node::set_iteratorexpr(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    ast_node_ptr thisnode = shared_from_this();
#if EXPRESSIONS_HAVE_BACKREFS
    constexpr int backref = AST_BR_ITERATOR;
#endif

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_ITERATOR_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FOROFSTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_ITERATOREXPR_IS_NOT_COMPLETE,
                           effectloc);

        // The iterator expression must not contain rvalues
        if (NODE_IS_RVALUE(nd))
        {
            return makeerr(err::EC_RVALUE_IN_FOROF_ITERATOREXPR,
                           detail::ast_node_ptr_defining_rvalue_loc(nd));
        }

        auto thisastnd = static_cast<ast_node_forof_statement*>(this);

        ENSURE_NOT_YET_OWNED(nd);
        CHECK_SLOT(thisastnd->iterator_expr,nd);

        // Register the variables of the expression in the subnodelist
        auto exp = detail::
            ast_node_recursive_variable_registration(thisnode,
                                                     nd);
        if (!exp)
            return forwarderr(exp);
        
        thisastnd->iterator_expr = nd;

#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_iteratorexpr() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FOROFSTMT)
        return static_cast<const ast_node_forof_statement*>(this)->
            iterator_expr;

    return nullptr;
}


// Set and get the for-in statement's generator expression
exp_none ast_node::set_generatorexpr(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

#if EXPRESSIONS_HAVE_BACKREFS
    ast_node_ptr thisnode = shared_from_this();
    constexpr int backref = AST_BR_GENERATOR;
#endif

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_GENERATOR_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_FOROFSTMT)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_GENERATOREXPR_IS_NOT_COMPLETE,
                           effectloc);

        // The generator expression must not contain variable declarations
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_FOROF_GENERATOR_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_forof_statement*>(this)->generator_expr,
                 nd);
#if EXPRESSIONS_HAVE_BACKREFS
        static_cast<ast_node_br*>(nd.get())->set_backref(thisnode,backref);
#endif
        SET_OWNED(nd);
        return exp_none();
    }
    
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_generatorexpr() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FOROFSTMT)
        return static_cast<const ast_node_forof_statement*>(this)->
            generator_expr;

    return nullptr;
}


//
// Expressions
//


cig_bool
ast_node::is_list() const noexcept
{
    ast_node_x const *ndx = static_cast<ast_node_x const *>(this);
    return (ndx->x & ast_node_x::X_HAS_LISTVALUE) != 0;
}


cig_bool
ast_node::has_ulvalue() const noexcept
{
    ast_node_x const *ndx = static_cast<ast_node_x const *>(this);
    return (ndx->x & ast_node_x::X_HAS_ULVALUE) != 0;
}


cig_bool
ast_node::has_rvalue() const noexcept
{
    ast_node_x const *ndx = static_cast<ast_node_x const *>(this);
    return (ndx->x & ast_node_x::X_HAS_RVALUE) != 0;
}


cig_bool
ast_node::is_conditional() const noexcept
{
    ast_node_x const *ndx = static_cast<ast_node_x const *>(this);
    return (ndx->x & ast_node_x::X_HAS_CONDITIONAL) != 0;
}


cig_bool
ast_node::is_complete() const noexcept
{
    using enum ast::ast_node_type;

    // A node 'is complete' if all its subnodes that lead to
    // 'spillover effects', i.e. to value type flags of the
    // node being set or to variables being exported, are
    // already set.
    // Only a complete node can be added to another node.
    // In other words, expressions must be built from the
    // leaves up to the toplevel combining expression.

    switch (node_type)
    {
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
    case AST_EXPR_MEMBER:
    case AST_EXPR_MEMBERLINK:
    {
        if (!static_cast<ast_node_unary_expression const *>(this)->subexpr1)
            return false;
        return true;
    }

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:
    {
        if (!static_cast<ast_node_binary_expression const *>(this)->subexpr1)
            return false;
        if (node_type != AST_EXPR_ARRAYDEREF &&
            node_type != AST_EXPR_FUNCALL)
        {
            // For arrays and functions, the index will not lead to
            // spillover effects
            // For functions, the argument list determines overload
            // resolution, which determines the list-valuedness of
            // the function return list. However that is checked
            // separately in the resolution phase.
            if (!static_cast<ast_node_binary_expression const*>(this)->subexpr2)
                return false;
        }
        return true;
    }

    case AST_EXPR_SLICE:
    {
        if (!static_cast<ast_node_slice_expression const *>(this)->subexpr1)
            return false;
        // Slice indices do not lead to spillover effects
        // if (!static_cast<ast_node_slice_expression const *>(this)->subexpr2)
        //     return false;
        // if (!static_cast<ast_node_slice_expression const *>(this)->subexpr3)
        //     return false;
        // if (!static_cast<ast_node_slice_expression const *>(this)->subexpr4)
        //     return false;
        return true;
    }

    case AST_EXPR_CONDITION:
    {
        if (!static_cast<ast_node_ternary_expression const *>(this)->subexpr1)
            return false;
        if (!static_cast<ast_node_ternary_expression const *>(this)->subexpr2)
            return false;
        if (!static_cast<ast_node_ternary_expression const *>(this)->subexpr3)
            return false;
        return true;
    }

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
    {
        if (!static_cast<ast_node_binary_expression const *>(this)->subexpr1)
            return false;
        if (!static_cast<ast_node_binary_expression const *>(this)->subexpr2)
            return false;
        return true;
    }

    case AST_EXPR_LIST:
    {
        if (!static_cast<ast_node_list_expression const *>(this)->
            list_is_complete)
            return false;
        return true;
    }

    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_UNQUALNAME:
        // These expressions are always 'complete'
        return true;

    case AST_VARIABLE:
    {
        if (!static_cast<ast_node_variable const *>(this)->typeref)
            return false;
        return true;
    }

    case AST_FUNPAR:
    {
        if (!static_cast<ast_node_funpar const *>(this)->typeref)
            return false;
        return true;
    }

    case AST_FUNRET:
    {
        if (!static_cast<ast_node_funret const *>(this)->typeref)
            return false;
        return true;
    }

    case AST_TYPEREF:
    {
        if (!static_cast<ast_node_typeref const *>(this)->ref)
            return false;
        return true;
    }

    case AST_FUNBODY:
    {
        if (!static_cast<ast_node_funbody const *>(this)->
            subnodelist.get_subnode(0))
            return false;
        return true;
    }

    case AST_EXPRSTMT:

        // This statement is complete when the expression is set
        if (!static_cast<const ast_node_expression_statement*>(this)->expr)
            return false;
        return true;

    case AST_COMPOUNDSTMT:
    case AST_RETURNSTMT:
    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    case AST_FOROFSTMT:
        // These statements never have any spillover variables,
        // so they are always complete
        return true;

    case AST_CONTINUESTMT:
    case AST_BREAKSTMT:
        // Always complete
        return true;

    // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    case AST_EXPRMIN:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_STMTMAX:
    case AST_MAX:
        return false;
    }
    return false;
}


i64                             // count of list elements or -1 if not a list
ast_node::get_listsubexpr_count() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_LIST)
    {
        return static_cast<ast_node_list_expression const *>(this)->
            subnodelist.get_count();
    }
    return -1;
}


// Add a subexpression to a list
exp_i64 ast_node::add_listsubexpr(ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_LISTEXPR_TO_ADD_IS_NULL,
                       effectloc);

    ast_node_type ndtype = nd->get_node_type();

    if (node_type == AST_EXPR_LIST)
    {
        if (!ast_node_type_is_expr(ndtype))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        ast_node_list_expression *thislist =
            static_cast<ast_node_list_expression*>(this);

        if (thislist->list_is_complete)
            return makeerr(err::EC_AST_LIST_COMPLETE_CANNOT_ADD,
                           effectloc);

        // Do not allow lists in lists
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_LISTS_IN_LISTS_NOT_ALLOWED,
                           nd->get_startloc(),
                           get_startloc());
        }

        // Do not mix ULVALUE and RVALUE in the same list

        if ((thislist->x & ast_node_x::X_HAS_RVALUE) != 0 &&
            NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_LIST_CANNOT_MIX_ULVALUE_AND_RVALUE,
                           nd->get_startloc(),
                           detail::ast_node_defining_rvalue_loc(*this));
        }
        if ((thislist->x & ast_node_x::X_HAS_ULVALUE) != 0 &&
            NODE_IS_RVALUE(nd))
        {
            return makeerr(err::EC_LIST_CANNOT_MIX_ULVALUE_AND_RVALUE,
                           nd->get_startloc(),
                           detail::ast_node_defining_ulvalue_loc(*this));
        }

        // Transfer the ULVALUE and RVALUE and CONDITIONAL properties
        TRANSFER_VALUEKIND(nd);
        thislist->x |= ast_node_x::X_HAS_LISTVALUE;

        ENSURE_NOT_YET_OWNED(nd);
        auto expindex = thislist->subnodelist.add_subnode_without_backref(nd);
        if (!expindex)
            return expindex;
        SET_OWNED(nd);
        return expindex;
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}


exp_none ast_node::set_list_complete() noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_LIST)
    {
        ast_node_list_expression *thislist =
            static_cast<ast_node_list_expression*>(this);

        thislist->list_is_complete = true;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}


// Get a subexpression from a list
ast_node_ptr                    // ast_node_ptr or nullptr
ast_node::get_listsubexpr(i64 index) const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_LIST)
        return get_subnode(index);
    return nullptr;
}


// Set and get the first operand expression
exp_none ast_node::set_subexpr1(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_EXPR1_TO_ADD_IS_NULL,
                       effectloc);

    switch (node_type)
    {
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
    case AST_EXPR_MEMBER:
    case AST_EXPR_MEMBERLINK:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            if (node_type == AST_EXPR_MEMBER ||
                node_type == AST_EXPR_MEMBERLINK)
                return makeerr(err::EC_LIST_MEMBER_IMPOSSIBLE,
                               get_effectloc());
            else
                return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                               get_effectloc());
        }

        // Member operation not possible on conditional values
        if (NODE_IS_CONDITIONAL(nd) &&
            (node_type == AST_EXPR_MEMBER ||
             node_type == AST_EXPR_MEMBERLINK))
            return makeerr(err::EC_CONDITIONAL_MEMBER_IMPOSSIBLE,
                           get_effectloc());

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_NEW_VARIABLE_OPERATION_IMPOSSIBLE,
                           get_effectloc(),
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_unary_expression*>(this)->subexpr1,nd);
        SET_OWNED(nd);

        // For member operation, transfer RVALUE from nd
        if (node_type == AST_EXPR_MEMBER ||
            node_type == AST_EXPR_MEMBERLINK)
        {
            TRANSFER_VALUEKIND(nd);
        }

        return exp_none();

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            if (node_type == AST_EXPR_ARRAYDEREF)
                return makeerr(err::EC_LIST_ARRAYDEREF_IMPOSSIBLE,
                               get_effectloc(),
                               nd->get_startloc());
            else if (node_type == AST_EXPR_FUNCALL)
                return makeerr(err::EC_LIST_FUNCALL_IMPOSSIBLE,
                               get_effectloc(),
                               nd->get_startloc());
            else
                return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                               get_effectloc(),
                               nd->get_startloc());
        }

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            if (node_type == AST_EXPR_ARRAYDEREF)
                return makeerr(err::EC_NEW_VARIABLE_ARRAYDEREF_IMPOSSIBLE,
                               get_effectloc(),
                               detail::ast_node_ptr_defining_ulvalue_loc(nd));
            else if (node_type == AST_EXPR_FUNCALL)
                return makeerr(err::EC_NEW_VARIABLE_FUNCALL_IMPOSSIBLE,
                               get_effectloc(),
                               detail::ast_node_ptr_defining_ulvalue_loc(nd));
            else
                return makeerr(err::EC_NEW_VARIABLE_OPERATION_IMPOSSIBLE,
                               get_effectloc(),
                               detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        // Operation not possible on a conditional expression
        if (NODE_IS_CONDITIONAL(nd))
        {
            if (node_type == AST_EXPR_ARRAYDEREF)
                return makeerr(err::EC_CONDITIONAL_ARRAY_DEREF_IMPOSSIBLE,
                               get_effectloc(),
                               detail::
                               ast_node_ptr_defining_conditional_loc(nd));
            else if (node_type == AST_EXPR_FUNCALL)
                return makeerr(err::EC_CONDITIONAL_FUN_CALL_IMPOSSIBLE,
                               get_effectloc(),
                               detail::
                               ast_node_ptr_defining_conditional_loc(nd));
            // all other operations possible on conditional first argument
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_binary_expression*>(this)->subexpr1,nd);
        SET_OWNED(nd);

        // For array deref operation, transfer RVALUE from nd
        if (node_type == AST_EXPR_ARRAYDEREF)
        {
            TRANSFER_VALUEKIND(nd);
        }

        // rvalue is set from the operation except for arrayderef

        return exp_none();

    case AST_EXPR_SLICE:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_LIST_SLICE_IMPOSSIBLE,
                           get_effectloc(),
                           nd->get_startloc());
        }

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_NEW_VARIABLE_SLICE_IMPOSSIBLE,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd),
                           get_effectloc());
        }

        // Operation not possible on a conditional variable
        if (NODE_IS_CONDITIONAL(nd))
        {
            return makeerr(err::EC_CONDITIONAL_SLICE_IMPOSSIBLE,
                           detail::ast_node_ptr_defining_conditional_loc(nd),
                           get_effectloc());
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_slice_expression*>(this)->subexpr1,nd);
        SET_OWNED(nd);
        return exp_none();

    case AST_EXPR_CONDITION:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_CONDEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_COND_CONDITION_DISALLOWS_LISTVAL,
                           get_effectloc(),
                           nd->get_startloc());
        }

        // Operation not possible on new variables
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_COND_CONDITION_DISALLOWS_VARDECL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd),
                           get_effectloc());
        }

        // conditional is set from the operation

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_ternary_expression*>(this)->subexpr1,nd);
        SET_OWNED(nd);
        return exp_none();

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_LHSEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd) &&
            node_type != AST_EXPR_ASSIGN &&
            node_type != AST_EXPR_DOT_ASSIGN)
        {
            return makeerr(err::EC_LIST_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                           get_effectloc(),
                           nd->get_startloc());
        }

        // New variables can be assigned to but not modify-assigned
        if (NODE_IS_ULVALUE(nd) &&
            node_type != AST_EXPR_ASSIGN &&
            node_type != AST_EXPR_DOT_ASSIGN)
        {
            return makeerr(err::EC_NEW_VARIABLE_OPERATION_IMPOSSIBLE,
                           get_effectloc(),
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        // Rvalues cannot be modified
        if (NODE_IS_RVALUE(nd))
        {
            return makeerr(err::EC_RVALUE_CANNOT_BE_ASSIGNED_TO,
                           get_effectloc(),
                           detail::ast_node_ptr_defining_rvalue_loc(nd));
        }

        // Conditionals cannot be assigned
        if (NODE_IS_CONDITIONAL(nd))
        {
            return makeerr(err::EC_CONDITIONAL_CANNOT_BE_ASSIGNED_TO,
                           get_effectloc(),
                           detail::ast_node_ptr_defining_conditional_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_binary_expression*>(this)->subexpr1,nd);
        SET_OWNED(nd);

        // Transfer ULVALUE from nd
        TRANSFER_VALUEKIND(nd);

        return exp_none();

    case AST_EXPR_LIST:
        // first list subexpr is set via add_listsubexpr
        return makeerr(err::EC_ASTNODE_BADTYPE);

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPRMAX:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return makeerr(err::EC_ASTNODE_BADTYPE);
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_subexpr1() const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
    case AST_EXPR_MEMBER:
    case AST_EXPR_MEMBERLINK:
        return static_cast<const ast_node_unary_expression*>(this)->subexpr1;

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:
        return static_cast<const ast_node_binary_expression*>(this)->subexpr1;

    case AST_EXPR_SLICE:
        return static_cast<const ast_node_slice_expression*>(this)->subexpr1;

    case AST_EXPR_CONDITION:
        return static_cast<const ast_node_ternary_expression*>(this)->subexpr1;

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
        return static_cast<const ast_node_binary_expression*>(this)->subexpr1;

    case AST_EXPR_LIST:
        // first list subexpr is obtained via get_listsubexpr
        return nullptr;

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPRMAX:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return nullptr;
    }
    return nullptr;
}


// Set and get the second operand expression
exp_none ast_node::set_subexpr2(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_EXPR2_TO_ADD_IS_NULL,
                       effectloc);

    switch (node_type)
    {
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
        return makeerr(err::EC_ASTNODE_BADTYPE);

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        // except for function call
        if (NODE_IS_LISTVALUE(nd) && node_type != AST_EXPR_FUNCALL)
        {
            if (node_type == AST_EXPR_ARRAYDEREF)
                return makeerr(err::EC_ARRAY_INDEX_CANNOT_BE_LIST,
                               nd->get_startloc(),
                               get_effectloc());
            else
                return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                               get_effectloc());
        }

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            if (node_type == AST_EXPR_ARRAYDEREF)
                return makeerr(err::EC_ARRAY_INDEX_CANNOT_BE_NEW_VARIABLE,
                               detail::ast_node_ptr_defining_ulvalue_loc(nd),
                               get_effectloc());
            else if (node_type == AST_EXPR_FUNCALL)
                return makeerr(err::EC_FUNCTION_ARGUMENT_CANNOT_BE_NEW_VARIABLE,
                               detail::ast_node_ptr_defining_ulvalue_loc(nd),
                               get_effectloc());
            else
                return makeerr(err::EC_NEW_VARIABLE_OPERATION_IMPOSSIBLE,
                               get_effectloc(),
                               detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_binary_expression*>(this)->subexpr2,nd);
        SET_OWNED(nd);

        // Do not transfer array index valuekind or function argument valuekind
        if (node_type != AST_EXPR_ARRAYDEREF &&
            node_type != AST_EXPR_FUNCALL)
            TRANSFER_VALUEKIND(nd);

        return exp_none();

    case AST_EXPR_SLICE:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_SLICE_POSITION_CANNOT_BE_LIST,
                           nd->get_startloc());
        }

        // Operation not possible on new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_SLICE_POSITION_CANNOT_BE_NEW_VARIABLE,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_slice_expression*>(this)->subexpr2,nd);
        SET_OWNED(nd);

        // Do not transfer slice position valuekind

        return exp_none();

    case AST_EXPR_CONDITION:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_NEW_VARIABLE_CANNOT_BE_CONDITIONAL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd),
                           get_effectloc());
        }

        // Lists and rvalues are allowed

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_ternary_expression*>(this)->subexpr2,nd);
        SET_OWNED(nd);

        // Transfer RVALUE and LISTVALUE from nd
        TRANSFER_VALUEKIND(nd);

        return exp_none();

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:

        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_RHSEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd) &&
            node_type != AST_EXPR_ASSIGN &&
            node_type != AST_EXPR_DOT_ASSIGN)
        {
            return makeerr(err::EC_LIST_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                           get_effectloc(),
                           nd->get_startloc());
        }

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_NEW_VARIABLE_NOT_ALLOWED_ON_RHS,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd),
                           get_effectloc());
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_binary_expression*>(this)->subexpr2,nd);
        SET_OWNED(nd);

        // Do not transfer modifying valuekind

        return exp_none();

    case AST_EXPR_LIST:
        // second list subexpr is set via add_listsubexpr
        return makeerr(err::EC_ASTNODE_BADTYPE);

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPRMAX:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return makeerr(err::EC_ASTNODE_BADTYPE);
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_subexpr2() const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
        return nullptr;

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:
        return static_cast<const ast_node_binary_expression*>(this)->subexpr2;

    case AST_EXPR_SLICE:
        return static_cast<const ast_node_slice_expression*>(this)->subexpr2;

    case AST_EXPR_CONDITION:
        return static_cast<const ast_node_ternary_expression*>(this)->subexpr2;

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
        return static_cast<const ast_node_binary_expression*>(this)->subexpr2;

    case AST_EXPR_LIST:
        // second list subexpr is obtained via get_listsubexpr
        return nullptr;

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPRMAX:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return nullptr;
    }
    return nullptr;
}


// Set and get the third operand expression
exp_none ast_node::set_subexpr3(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_EXPR3_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_EXPR_CONDITION)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on a new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_NEW_VARIABLE_CANNOT_BE_CONDITIONAL,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd),
                           get_effectloc());
        }

        // Lists and rvalues are allowed

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_ternary_expression*>(this)->subexpr3,nd);
        SET_OWNED(nd);

        // Transfer RVALUE and LISTVALUE from nd
        TRANSFER_VALUEKIND(nd);

        return exp_none();
    }
    if (node_type == AST_EXPR_SLICE)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_SLICE_POSITION_CANNOT_BE_LIST,
                           nd->get_startloc());
        }

        // Operation not possible on new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_SLICE_POSITION_CANNOT_BE_NEW_VARIABLE,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_slice_expression*>(this)->subexpr3,nd);
        SET_OWNED(nd);

        // Do not transfer slice position valuekind

        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_subexpr3() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_CONDITION)
        return static_cast<const ast_node_ternary_expression*>(this)->subexpr3;
    if (node_type == AST_EXPR_SLICE)
        return static_cast<const ast_node_slice_expression*>(this)->subexpr3;

    return nullptr;
}


// Set and get the fourth operand expression
exp_none ast_node::set_subexpr4(ast_node_ptr nd) noexcept
{
    using enum ast::ast_node_type;

    // Argument check
    if (nd == nullptr)
        return makeerr(err::EC_AST_NODE_EXPR4_TO_ADD_IS_NULL,
                       effectloc);

    if (node_type == AST_EXPR_SLICE)
    {
        // Check that nd is an expression but not an assignment
        if (!ast_node_type_is_expr(nd->node_type) ||
            ast_node_type_is_assign_expr(nd->node_type))
            return makeerr(err::EC_ASTNODE_BADADDTYPE);
        if (!nd->is_complete())
            return makeerr(err::EC_AST_SUBEXPRESSION_IS_NOT_COMPLETE,
                           effectloc);

        // Operation not possible on list values
        if (NODE_IS_LISTVALUE(nd))
        {
            return makeerr(err::EC_SLICE_POSITION_CANNOT_BE_LIST,
                           nd->get_startloc());
        }

        // Operation not possible on new variable
        if (NODE_IS_ULVALUE(nd))
        {
            return makeerr(err::EC_SLICE_POSITION_CANNOT_BE_NEW_VARIABLE,
                           detail::ast_node_ptr_defining_ulvalue_loc(nd));
        }

        ENSURE_NOT_YET_OWNED(nd);
        SET_SLOT(static_cast<ast_node_slice_expression*>(this)->subexpr4,nd);
        SET_OWNED(nd);

        // Do not transfer slice position valuekind

        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

ast_node_ptr ast_node::get_subexpr4() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_SLICE)
        return static_cast<const ast_node_slice_expression*>(this)->subexpr4;

    return nullptr;
}


//
// Various attributes
//


// Get and set literal
literal const & ast_node::get_literal() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMVALUE)
        return static_cast<const ast_node_enumvalue*>(this)->litvalue;

    if (node_type == AST_EXPR_LITERAL)
        return static_cast<const ast_node_literal_expression*>(this)->litvalue;

    return detail::empty_literal;
}

exp_none ast_node::set_literal(literal const &lit) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMVALUE)
    {
        static_cast<ast_node_enumvalue*>(this)->litvalue = lit;
        return exp_none();
    }

    if (node_type == AST_EXPR_LITERAL)
    {
        static_cast<ast_node_literal_expression*>(this)->litvalue = lit;
        return exp_none();
    }

    return makeerr(err::EC_ASTNODE_BADTYPE);
}

exp_none ast_node::set_literal(literal &&lit) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMVALUE)
    {
        static_cast<ast_node_enumvalue*>(this)->litvalue = std::move(lit);
        return exp_none();
    }

    if (node_type == AST_EXPR_LITERAL)
    {
        static_cast<ast_node_literal_expression*>(this)->litvalue =
            std::move(lit);
        return exp_none();
    }

    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set qualified name components
str const & ast_node::get_unqualified_name() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_UNQUALNAME)
    {
        return static_cast<const ast_node_unqualified_name_expression*>(this)->
            unqualified_name;
    }
    return detail::empty_string;
}

exp_none ast_node::set_unqualified_name(str const &nm) noexcept
{
    using enum ast::ast_node_type;

    if (nm.is_empty())
        return makeerr(err::EC_AST_NODE_UNQUNAME_TO_ADD_IS_EMPTY,
                       effectloc);
    if (node_type == AST_EXPR_UNQUALNAME)
    {
        static_cast<ast_node_unqualified_name_expression*>(this)->
            unqualified_name = nm;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set member name components
str const & ast_node::get_member_name() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_MEMBER)
    {
        return static_cast<const ast_node_member_expression*>(this)->
            member_name;
    }
    return detail::empty_string;
}

exp_none ast_node::set_member_name(str const &nm) noexcept
{
    using enum ast::ast_node_type;

    if (nm.is_empty())
        return makeerr(err::EC_AST_NODE_MBNAME_TO_ADD_IS_EMPTY,
                       effectloc);
    if (node_type == AST_EXPR_MEMBER)
    {
        static_cast<ast_node_member_expression*>(this)->
            member_name = nm;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set name in enclosing scope
// (For function definitions out of class scope with a qualified name,
// this is the last component of the function name only.)
str const & ast_node::get_name() const noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_NAMESPACE:
        return static_cast<const ast_node_namespace*>(this)->name;
    case AST_CLASSDECL:
        return static_cast<const ast_node_classdecl*>(this)->name;
    case AST_CLASSDEF:
        return static_cast<const ast_node_classdef*>(this)->name;
    case AST_ENUMDECL:
        return static_cast<const ast_node_enumdecl*>(this)->name;
    case AST_ENUMDEF:
        return static_cast<const ast_node_enumdef*>(this)->name;
    case AST_ENUMVALUE:
        return static_cast<const ast_node_enumvalue*>(this)->name;
    case AST_VARIABLE:
        return static_cast<const ast_node_variable*>(this)->name;
    case AST_OVERLOADSET:
        return static_cast<const ast_node_overloadset*>(this)->name;
    case AST_FUNPAR:
        return static_cast<const ast_node_funpar*>(this)->name;
    case AST_FUNDECL:
    case AST_FUNDEF:
        return static_cast<const ast_node_functionbase*>(this)->name;

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return detail::empty_string;
    }
    return detail::empty_string;
}

exp_none ast_node::set_name(str const &nm) noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_NAMESPACE:
        static_cast<ast_node_namespace*>(this)->name = nm;
        return exp_none();
    case AST_CLASSDECL:
        static_cast<ast_node_classdecl*>(this)->name = nm;
        return exp_none();
    case AST_CLASSDEF:
        static_cast<ast_node_classdef*>(this)->name = nm;
        return exp_none();
    case AST_ENUMDECL:
        static_cast<ast_node_enumdecl*>(this)->name = nm;
        return exp_none();
    case AST_ENUMDEF:
        static_cast<ast_node_enumdef*>(this)->name = nm;
        return exp_none();
    case AST_ENUMVALUE:
        static_cast<ast_node_enumvalue*>(this)->name = nm;
        return exp_none();
    case AST_VARIABLE:
        static_cast<ast_node_variable*>(this)->name = nm;
        return exp_none();
    case AST_OVERLOADSET:
        static_cast<ast_node_overloadset*>(this)->name = nm;
        return exp_none();
    case AST_FUNPAR:
        static_cast<ast_node_funpar*>(this)->name = nm;
        return exp_none();
    case AST_FUNDECL:
    case AST_FUNDEF:
        static_cast<ast_node_functionbase*>(this)->name = nm;
        return exp_none();

        // Not available for:
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_NODE_EXPR_CASES:
    case AST_NODE_STMT_CASES:
    case AST_MAX:
        return makeerr(err::EC_ASTNODE_BADTYPE);
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set flags
exp_i64 ast_node::get_flags() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDECL)
        return static_cast<ast_node_classdecl const *>(this)->flags;
    if (node_type == AST_CLASSDEF)
        return static_cast<ast_node_classdef const *>(this)->flags;
    if (node_type == AST_VARIABLE)
        return static_cast<ast_node_variable const *>(this)->flags;
    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF)
        return static_cast<ast_node_functionbase const *>(this)->flags;
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

exp_none ast_node::set_flags(i64 flags) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDECL)
    {
        static_cast<ast_node_classdecl*>(this)->flags = flags;
        return exp_none();
    }
    if (node_type == AST_CLASSDEF)
    {
        static_cast<ast_node_classdef*>(this)->flags = flags;
        return exp_none();
    }
    if (node_type == AST_VARIABLE)
    {
        static_cast<ast_node_variable*>(this)->flags = flags;
        return exp_none();
    }
    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF)
    {
        static_cast<ast_node_functionbase*>(this)->flags = flags;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

cig_bool ast_node::is_static() const noexcept
{
    using enum ast::ast_node_type;

    i64 flags;
    bool default_is_static;
    if (node_type == AST_CLASSDECL)
    {
        flags = static_cast<ast_node_classdecl const *>(this)->flags;
        default_is_static = DEFAULT_CLASS_IS_STATIC;
    }
    else if (node_type == AST_CLASSDEF)
    {
        flags = static_cast<ast_node_classdef const *>(this)->flags;
        default_is_static = DEFAULT_CLASS_IS_STATIC;
    }
    else if (node_type == AST_VARIABLE)
    {
        flags = static_cast<ast_node_variable const *>(this)->flags;
        default_is_static = DEFAULT_VARIABLE_IS_STATIC;
    }
    else if (node_type == AST_FUNDECL ||
             node_type == AST_FUNDEF)
    {
        flags = static_cast<ast_node_functionbase const *>(this)->flags;
        default_is_static = DEFAULT_FUNCTION_IS_STATIC;
    }
    else
        return false;

    if (default_is_static)
        return (flags & AST_FLAG_NONSTATIC) == 0;
    else
        return (flags & AST_FLAG_STATIC) != 0;
}

// Get and set type information
ast_type_info ast_node::get_type_info() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDEF)
        return static_cast<ast_node_classdef const *>(this)->type_info;
    if (node_type == AST_ENUMDEF)
        return static_cast<ast_node_enumdef const *>(this)->type_info;
    return detail::empty_type_info;
}

exp_none ast_node::set_type_info(ast_type_info const &ti) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_CLASSDEF)
    {
        static_cast<ast_node_classdef*>(this)->type_info = ti;
        return exp_none();
    }
    if (node_type == AST_ENUMDEF)
    {
        static_cast<ast_node_enumdef*>(this)->type_info = ti;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set variable information
ast_variable_info ast_node::get_variable_info() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_VARIABLE)
        return static_cast<ast_node_variable const *>(this)->variable_info;
    if (node_type == AST_FUNPAR)
        return static_cast<ast_node_funpar const *>(this)->variable_info;
    if (node_type == AST_FUNRET)
        return static_cast<ast_node_funret const *>(this)->variable_info;
    if (node_type == AST_EXPR_LITERAL)
        return static_cast<ast_node_literal_expression const *>(this)->
            variable_info;
    if (node_type == AST_ENUMVALUE)
        return static_cast<ast_node_enumvalue const *>(this)->
            variable_info;
    return detail::empty_variable_info;
}

exp_none ast_node::set_variable_info(ast_variable_info const &vi) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_VARIABLE)
    {
        static_cast<ast_node_variable*>(this)->variable_info = vi;
        return exp_none();
    }
    if (node_type == AST_FUNPAR)
    {
        static_cast<ast_node_funpar*>(this)->variable_info = vi;
        return exp_none();
    }
    if (node_type == AST_FUNRET)
    {
        static_cast<ast_node_funret*>(this)->variable_info = vi;
        return exp_none();
    }
    if (node_type == AST_EXPR_LITERAL)
    {
        static_cast<ast_node_literal_expression*>(this)->variable_info = vi;
        return exp_none();
    }
    if (node_type == AST_ENUMVALUE)
    {
        static_cast<ast_node_enumvalue*>(this)->variable_info = vi;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set function information
ast_function_info ast_node::get_function_info() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF)
        return static_cast<ast_node_fundef const *>(this)->function_info;
    return detail::empty_function_info;
}

exp_none ast_node::set_function_info(ast_function_info const &fi) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF)
    {
        static_cast<ast_node_fundef*>(this)->function_info = fi;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set expression information
ast_expression_info ast_node::get_expression_info() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_LITERAL)
        return static_cast<ast_node_literal_expression const *>(this)->ei;
    // All expressions except are derived from ast_node_expressionbase
    if (ast_node_type_is_expr(node_type))
        return static_cast<ast_node_expressionbase const *>(this)->ei;
    return detail::empty_expression_info;
}

exp_none ast_node::set_expression_info(ast_expression_info const &ei) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_EXPR_LITERAL)
    {
        static_cast<ast_node_literal_expression*>(this)->ei = ei;
        return exp_none();
    }
    // All expressions except are derived from ast_node_expressionbase
    if (ast_node_type_is_expr(node_type))
    {
        static_cast<ast_node_expressionbase*>(this)->ei = ei;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set global information
ast_global_info ast_node::get_global_info() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_GLOBAL)
        return static_cast<ast_node_global const *>(this)->gi;
    return detail::empty_global_info;
}

exp_none ast_node::set_global_info(ast_global_info const &gi) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_GLOBAL)
    {
        static_cast<ast_node_global*>(this)->gi = gi;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set function index for interpreter
i64 ast_node::get_interpreter_function_index() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF)
        return static_cast<ast_node_fundef const *>(this)->fnidx;
    return -1;
}

exp_none ast_node::set_interpreter_function_index(i64 fnidx) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_FUNDEF)
    {
        static_cast<ast_node_fundef*>(this)->fnidx = fnidx;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Get and set datatype index for interpreter
i64 ast_node::get_interpreter_datatype_index() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMDEF)
        return static_cast<ast_node_enumdef const *>(this)->typeidx;
    if (node_type == AST_CLASSDEF)
        return static_cast<ast_node_classdef const *>(this)->typeidx;
    return -1;
}

exp_none ast_node::set_interpreter_datatype_index(i64 typeidx) noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_ENUMDEF)
    {
        static_cast<ast_node_enumdef*>(this)->typeidx = typeidx;
        return exp_none();
    }
    if (node_type == AST_CLASSDEF)
    {
        static_cast<ast_node_classdef*>(this)->typeidx = typeidx;
        return exp_none();
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}

// Node declares variables visible from outside
cig_bool ast_node::declares_variables_visible_from_outside() const noexcept
{
    using enum ast::ast_node_type;

    if (ast_node_type_is_expr(node_type))
        return (static_cast<ast_node_x const *>(this)->x &
                ast_node_x::X_HAS_ULVALUE) != 0;
    else if (node_type == AST_EXPRSTMT)
    {
        ast_node_ptr expr =
            static_cast<ast_node_expression_statement const *>(this)->expr;
        if (!expr)
            return false;
        auto ndx = static_cast<ast_node_x const *>(expr.get());
        if (!ndx)
            return false;
        return (ndx->x & ast_node_x::X_HAS_ULVALUE) != 0;
    }
    else
        return false;
}


str
ast_node::create_fqname(cig_bool with_global) const noexcept {
    using enum ast::ast_node_type;

    if (node_type != AST_ENUMDECL &&
        node_type != AST_ENUMDEF &&
        node_type != AST_ENUMVALUE &&
        node_type != AST_CLASSDECL &&
        node_type != AST_CLASSDEF &&
        node_type != AST_FUNDECL &&
        node_type != AST_FUNDEF &&
        node_type != AST_OVERLOADSET &&
        node_type != AST_NAMESPACE &&
        node_type != AST_GLOBAL)
        return ""_str;

    if (node_type == AST_GLOBAL)
        return "global"_str;

    str s = get_name();
    ast_node_backref br = get_backref();
    if (node_type == AST_FUNDEF)
    {
        // This might be a later out-of-class definition
        // of a previously in-class declared function
        auto fundecl = get_fundecl_backref();
        if (fundecl)
            br = fundecl->get_backref();
    }
    ast_node_ptr nd = std::get<ast_node_ptr>(br);
    while (nd)
    {
        if (nd->node_type == AST_GLOBAL)
        {
            if (with_global)
                return "global."_str + s;
            else
                return s;
        }

        s = nd->get_name() + "."_str + s;
        br = nd->get_backref();
        nd = std::get<ast_node_ptr>(br);
    }
    return s;
}


//
// Name resolution
//

exp_none ast_node::set_resolved_entity(ast_resolved_entity re,
                                       cig_bool replace) noexcept
{
    using enum ast::ast_node_type;

    auto nd = std::get<ast_node_ptr>(re);
    if (!nd)
        return makeerr(err::EC_RESOLVED_ENTITY_IS_NULL,
                       effectloc);
    auto qu = std::get<ast_resolve_quality>(re);

    if (node_type == AST_VARIABLE)
    {
        return static_cast<ast_node_variable*>(this)->
            rve.set(nd,qu,effectloc,replace);
    }
    else if (node_type == AST_TYPEREF)
    {
        return static_cast<ast_node_typeref*>(this)->
            rve.set(nd,qu,effectloc,replace);
    }
    else if (node_type == AST_EXPR_LITERAL)
    {
        return makeerr(err::EC_ASTNODE_BADTYPE);
    }
    else if (ast_node_type_is_expr(node_type))
    {
        return static_cast<ast_node_expressionbase*>(this)->
            rve.set(nd,qu,effectloc,replace);
    }
    return makeerr(err::EC_ASTNODE_BADTYPE);
}


ast_resolved_entity ast_node::get_resolved_entity() const noexcept
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    if (node_type == AST_VARIABLE)
    {
        auto nd =
            static_cast<ast_node_variable const *>(this)->
            rve.get_node();
        auto qu =
            static_cast<ast_node_variable const *>(this)->
            rve.get_quality();
        return { nd, qu };
    }
    else if (node_type == AST_TYPEREF)
    {
        auto nd =
            static_cast<ast_node_typeref const *>(this)->
            rve.get_node();
        auto qu =
            static_cast<ast_node_typeref const *>(this)->
            rve.get_quality();
        return { nd, qu };
    }
    else if (node_type == AST_EXPR_LITERAL)
    {
        return { nullptr, AST_RQ_INVALID };
    }
    else if (ast_node_type_is_expr(node_type))
    {
        auto nd =
            static_cast<ast_node_expressionbase const *>(this)->
            rve.get_node();
        auto qu =
            static_cast<ast_node_expressionbase const *>(this)->
            rve.get_quality();
        return { nd, qu };
    }
    return { nullptr, AST_RQ_INVALID };
}


ast_node_ptr ast_node::get_resolved_node() const noexcept
{
    using enum ast::ast_node_type;

    if (node_type == AST_VARIABLE)
    {
        return static_cast<ast_node_variable const *>(this)->
            rve.get_node();
    }
    else if (node_type == AST_TYPEREF)
    {
        return static_cast<ast_node_typeref const *>(this)->
            rve.get_node();
    }
    else if (node_type == AST_EXPR_LITERAL)
    {
        return nullptr;
    }
    else if (ast_node_type_is_expr(node_type))
    {
        return static_cast<ast_node_expressionbase const *>(this)->
            rve.get_node();
    }
    return nullptr;
}


ast_resolve_quality ast_node::get_resolved_quality() const noexcept
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    if (node_type == AST_VARIABLE)
    {
        return static_cast<ast_node_variable const *>(this)->
            rve.get_quality();
    }
    else if (node_type == AST_TYPEREF)
    {
        return static_cast<ast_node_typeref const *>(this)->
            rve.get_quality();
    }
    else if (ast_node_type_is_expr(node_type))
    {
        return static_cast<ast_node_expressionbase const *>(this)->
            rve.get_quality();
    }
    return AST_RQ_NOTFOUND;
}


/**********************************************************************/
/*                                                                    */
/*                    RECURSIVE INTERNAL FUNCTIONS                    */
/*                                                                    */
/**********************************************************************/


namespace detail {

// Function prototypes
exp_none
ast_node_recursive_variable_registration(
    ast_node_ptr scope,         // scope where to add the variable declarations
    ast_node_ptr expr           // expression with subexpressions; can be null
    )
{
    using enum ast::ast_node_type;

    if (!expr)
        return exp_none();

    if (expr->get_node_type() == AST_VARIABLE)
    {
        ast_node_x *scopex = static_cast<ast_node_x *>(scope.get());
        // fixme check that variable is not already referenced
        auto exp1 = scopex->add_named_entity_aux(expr,true);
        if (!exp1)
            return forwarderr(exp1);
        return exp_none();
    }

    // Recursion over subexpressions
    ast_node_type node_type = expr->get_node_type();
    switch (node_type)
    {
        // Non-expression nodes: Do nothing
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_COMPOUNDSTMT:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    case AST_FOROFSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return exp_none();

        // Unary expressions
    case AST_NODE_UNARY_EXPR_CASES:
    {
        auto exp1 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr1());
        if (!exp1)
            return forwarderr(exp1);
        return exp_none();
    }

    case AST_NODE_BINARY_EXPR_CASES:
    {
        auto exp1 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr1());
        if (!exp1)
            return forwarderr(exp1);
        auto exp2 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr2());
        if (!exp2)
            return forwarderr(exp2);
        return exp_none();
    }

    case AST_NODE_TERNARY_EXPR_CASES:
    {
        auto exp1 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr1());
        if (!exp1)
            return forwarderr(exp1);
        auto exp2 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr2());
        if (!exp2)
            return forwarderr(exp2);
        auto exp3 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr3());
        if (!exp3)
            return forwarderr(exp3);
        return exp_none();
    }

    case AST_EXPR_SLICE:
    {
        auto exp1 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr1());
        if (!exp1)
            return forwarderr(exp1);
        auto exp2 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr2());
        if (!exp2)
            return forwarderr(exp2);
        auto exp3 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr3());
        if (!exp3)
            return forwarderr(exp3);
        auto exp4 =
            ast_node_recursive_variable_registration(scope,
                                                     expr->get_subexpr4());
        if (!exp4)
            return forwarderr(exp4);
        return exp_none();
    }

    case AST_EXPR_LIST:
    {
        i64 n = expr->get_listsubexpr_count();
        for (i64 i = 0; i < n; i += 1)
        {
            auto exp =
                ast_node_recursive_variable_registration(
                    scope,
                    expr->get_listsubexpr(i));
            if (!exp)
                return forwarderr(exp);
        }
        return exp_none();
    }

    case AST_VARIABLE:
        // No need to proceed down; variables cannot be nested in the typeref
        return exp_none();

    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_UNQUALNAME:
        // These node types have no expression subnodes
        return exp_none();

    case AST_EXPR_MEMBER:
    case AST_EXPR_MEMBERLINK:
        // Variables are not allowed as base of member dereference
        return exp_none();

    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:
        // These node types have no expression subnodes
        return exp_none();
    }

    return exp_none();
}



}


/**********************************************************************/
/*                                                                    */
/*         COMPARISON FUNCTIONS FOR FUNCTION PARAMETER LISTS          */
/*                                                                    */
/**********************************************************************/



// Compare two qualified names; structure and components must be identical
static
bool                            // true if qualified names are identical
qualified_names_are_equal(ast::ast_node_ptr q1,
                          ast::ast_node_ptr q2)
{
    using enum ast::ast_node_type;

    auto p1 = q1.get();
    auto p2 = q2.get();
    if (!p1)
        return false;
    if (!p2)
        return false;

    // Compare unqualified names
    if (p1->get_node_type() == AST_EXPR_UNQUALNAME)
    {
        if (p2->get_node_type() != AST_EXPR_UNQUALNAME)
            return false;
        auto n1 = static_cast<const ast_node_unqualified_name_expression*>(p1);
        auto n2 = static_cast<const ast_node_unqualified_name_expression*>(p2);

        return n1->unqualified_name == n2->unqualified_name;
    }

    // fixme compare global, etc.

    if (p1->get_node_type() != AST_EXPR_MEMBER ||
        p2->get_node_type() != AST_EXPR_MEMBER)
        return false;
    if (p1->get_member_name() != p2->get_member_name())
        return false;
    return qualified_names_are_equal(p1->get_subexpr1(),p2->get_subexpr1());
}


static
bool                            // true if typeref qualified names are identical
typerefs_are_equal(ast::ast_node_ptr t1,
                   ast::ast_node_ptr t2)
{
    using enum ast::ast_node_type;

    auto p1 = t1.get();
    auto p2 = t2.get();
    if (!p1)
        return false;
    if (!p2)
        return false;
    if (p1->get_node_type() != AST_TYPEREF)
        return false;
    if (p2->get_node_type() != AST_TYPEREF)
        return false;
    return qualified_names_are_equal(t1->get_ref(),t2->get_ref());
}


bool                            // true if all parameter type qualfd.names match
funparlists_have_same_typerefs(ast::ast_node_ptr pl1,
                               ast::ast_node_ptr pl2)
{
    using enum ast::ast_node_type;

    auto p1 = pl1.get();
    auto p2 = pl2.get();
    if (!p1)
        return false;
    if (!p2)
        return false;
    if (p1->get_node_type() != AST_FUNPARLIST)
        return false;
    if (p2->get_node_type() != AST_FUNPARLIST)
        return false;
    for (i64 index = 0; ; index = index + 1)
    {
        auto funpar1 = p1->get_subnode(index);
        auto funpar2 = p2->get_subnode(index);
        if (funpar1 == nullptr && funpar2 == nullptr)
            break;              // at end of both lists
        if (funpar1 == nullptr || funpar2 == nullptr)
            return false;       // at end of one paramter list only
        auto t1 = funpar1->get_typeref();
        auto t2 = funpar2->get_typeref();
        if (t1 == nullptr || t2 == nullptr)
            return false;
        if (!typerefs_are_equal(t1,t2))
            return false;
    }
    return true;
}


bool                            // true if all return type qualfied names match
funretlists_have_same_typerefs(ast::ast_node_ptr pl1,
                               ast::ast_node_ptr pl2)
{
    using enum ast::ast_node_type;

    auto p1 = pl1.get();
    auto p2 = pl2.get();
    if (!p1)
        return false;
    if (!p2)
        return false;
    if (p1->get_node_type() != AST_FUNRETLIST)
        return false;
    if (p2->get_node_type() != AST_FUNRETLIST)
        return false;
    for (i64 index = 0; ; index = index + 1)
    {
        auto funret1 = p1->get_subnode(index);
        auto funret2 = p2->get_subnode(index);
        if (funret1 == nullptr && funret2 == nullptr)
            break;              // at end of both lists
        if (funret1 == nullptr || funret2 == nullptr)
            return false;       // at end of one return type list only
        auto t1 = funret1->get_typeref();
        auto t2 = funret2->get_typeref();
        if (t1 == nullptr || t2 == nullptr)
            return false;
        if (!typerefs_are_equal(t1,t2))
            return false;
    }
    return true;
}



} // namespace ast

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
