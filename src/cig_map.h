/* cig_map.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Map (dictionary) type for cig.
 */
#ifndef CIG_MAP_H_INCLUDED
#define CIG_MAP_H_INCLUDED

#include "cig_allocation.h"

#include <map>			/* std::map */
#include <utility>		/* std::pair */


namespace cig {


template<
    class Key,
    class T,
    class Compare = std::less<Key>,
    class Allocator=cig::allocator< std::pair<const Key,T>,"map"> >
class map : public std::map<Key,T,Compare,Allocator>
{
};


} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_MAP_H_INCLUDED) */
