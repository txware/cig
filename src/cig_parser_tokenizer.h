/* cig_parser_tokenizer.h
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Parser for the CIG language.
 */
#ifndef CIG_PARSER_TOKENIZER_H_INCLUDED
#define CIG_PARSER_TOKENIZER_H_INCLUDED

#include "cig_err.h"
#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_vector.h"
#include "cig_ast_literal.h"

namespace cig {

namespace parser {

enum class tokentype {
    INVALID,                    // invalid token
    KEYWORDS,                   // keyword keywords
    FUNCTION,                   // keyword function
    RETURN,                     // keyword return
    NAMESPACE,                  // keyword namespace
    CLASS,                      // keyword class
    ENUM,                       // keyword enum
    NATIVE,                     // keyword native
    STATIC,                     // keyword static
    NONSTATIC,                  // keyword nonstatic
    KWD_AND,                    // keyword and
    KWD_OR,                     // keyword or
    KWD_NOT,                    // keyword not
    KWD_IN,                     // keyword in
    KWD_IF,                     // keyword if
    KWD_ELSE,                   // keyword else
    KWD_FOR,                    // keyword for
    KWD_WHILE,                  // keyword while
    KWD_DO,                     // keyword do
    KWD_IS,                     // keyword is
    KWD_OF,                     // keyworf of
    GOTO,                       // keyword goto, reserved for later
    LABEL,                      // keyword label, reserved for later
    BREAK,                      // keyword break
    CONTINUE,                   // keyword continue
    KEYWORD_MAX,                // maximum keyword token (invalid)
    SYMBOL_MIN,                 // minimum symbolic token (invalid)
    SHL_ASSIGN,                 // '<<='
    SHR_ASSIGN,                 // '>>='
    EQUAL,                      // '=='
    NOT_EQUAL,                  // '!='
    LESS_EQUAL,                 // '<='
    GREATER_EQUAL,              // '>='
    SHIFT_LEFT,                 // '<<'
    SHIFT_RIGHT,                // '>>'
    LESS,                       // '<'
    GREATER,                    // '>'
    ARROW,                      // '->'
    DOT_ASSIGN,                 // '.='
    PLUS_ASSIGN,                // '+='
    MINUS_ASSIGN,               // '-='
    MULT_ASSIGN,                // '*='
    DIV_ASSIGN,                 // '/='
    MOD_ASSIGN,                 // '%='
    AND_ASSIGN,                 // '&='
    XOR_ASSIGN,                 // '^='
    OR_ASSIGN,                  // '|='
    ASSIGN,                     // '='
    PLUS,                       // '+'
    MINUS,                      // '-'
    MULT,                       // '*'
    DIV,                        // '/'
    MOD,                        // '%'
    LOGICAL_AND,                // '&&'
    LOGICAL_OR,                 // '||'
    BIT_AND,                    // '&'
    BIT_XOR,                    // '^'
    BIT_OR,                     // '|'
    LOGICAL_NOT,                // '!'
    BIT_NOT,                    // '~'
    OPENPAREN,                  // '('
    CLOSEPAREN,                 // ')'
    OPENBRACE,                  // '{'
    CLOSEBRACE,                 // '}'
    OPENBRACKET,                // '['
    CLOSEBRACKET,               // ']'
    OPENTMPL,                   // '<:'
    CLOSETMPL,                  // ':>'
    DOT,                        // '.'
    COMMA,                      // ','
    SEMICOLON,                  // ';'
    COLON,                      // ':'
    QUEST,                      // '?'
    REFPTR,                     // '<&>'
    WEAKPTR,                    // '<***>'
    SHAREDPTR,                  // '<**>'
    NONNULLPTR,                 // '<*>'
    ATSIGN,                     // '@'
    DOLLAR,                     // '$'
    SYMBOL_MAX,                 // maximum symbolic token (invalid)
    LITERAL_MIN,                // minimum literal token (invalid)
    LITERAL_STR,                // a literal str value
    LITERAL_BYTES,              // a literal bytes value
    LITERAL_CHAR,               // a literal i64 single Unicode code point value
    LITERAL_I64,                // a literal i64 value
    LITERAL_I32,                // a literal i32 value
    LITERAL_I16,                // a literal i16 value
    LITERAL_I8,                 // a literal i8 value
    LITERAL_U64,                // a literal u64 value
    LITERAL_U32,                // a literal u32 value
    LITERAL_U16,                // a literal u16 value
    LITERAL_U8,                 // a literal u8 value
    LITERAL_BOOL,               // a literal cig_bool value is a keyword
    LITERAL_MAX,                // maximum literal token (invalid)
    IDENTIFIER,                 // an identifier
    FILEEND,                    // end of source file
    ERROR,                      // token for errors without a specific token
    MAX
};


char const *tokentype_name(tokentype);
char const *tokentype_symbol(tokentype);
str const tokentype_symbolstr(tokentype);
bool tokentype_is_keyword(tokentype);
bool tokentype_is_literal(tokentype);

struct token {
    tokentype ttype;
    cig::source_location sourceloc;
    uint32_t sourcelen;
    ast::literal value;         // identifier str or literal value

    token() : sourcelen(0) { };
    token(cig::source_location const &,tokentype t);
    token(cig::source_location const &,tokentype t,i64 len);

    cig::source_location startloc() const noexcept;
    cig::source_location endloc() const noexcept;
};

using token_list = cig::vector<token>;

std::expected<token_list,parse_error>
exp_tokenize_file_str(str const &,char const *filename,i64 tabsize = 8);

char const *parser_tokenizer_selftest_errorstring();

} // namespace parser

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_PARSER_TOKENIZER_H_INCLUDED) */
