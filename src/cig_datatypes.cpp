/* cig_datatypes.cpp
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Runtime type implementation for cig.
 */
#include "cig_datatypes.h"

#include "cig_unicode_conversion.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"

#include <cstddef>              // std::size_t,std::ptrdiff_t
#include <cstdint>              // uint64_t ...

namespace cig {


template<typename Char>
exp_str str::from_utf8(Char const *data, std::size_t sz)
{
    using namespace detail;
    unicode_conversion_error e;
    unsigned long maxcodepoint;

    std::ptrdiff_t retval = str_decode_utf8_count(data,data+sz,
                                                  &maxcodepoint,0,&e);

    if (retval == -1)
    {
        // Illegal UTF-8 sequence
        // Truncated UTF-8 sequence
        // Overlong encoding
        // Illegal surrogate
        // Illegal Unicode code point
        return makeerr(detail::unicode_conversion_map_error(e.type));
    }

    std::size_t len = static_cast<unsigned long>(retval);
    strval::_chartype chartype =
        strval::_chartype_by_max_codepoint(static_cast<uint32_t>(maxcodepoint));

    switch (chartype)
    {
    case strval::_chartype_ucs4:
        if (true)
        {
            str s;
            s._reserve(len,chartype);
            str_decode_utf8(data,data+sz,s._data_ucs4(),0,&e);
            s._set_size(len);
            return exp_str(std::move(s));
        }
        break;
    case strval::_chartype_ucs2:
        if (true)
        {
            str s;
            s._reserve(len,chartype);
            str_decode_utf8(data,data+sz,s._data_ucs2(),0,&e);
            s._set_size(len);
            return exp_str(std::move(s));
        }
        break;
    case strval::_chartype_ucs1:
        if (true)
        {
            str s;
            s._reserve(len,chartype);
            str_decode_utf8(data,data+sz,s._data_ucs1(),0,&e);
            s._set_size(len);
            return exp_str(std::move(s));
        }
        break;
    case strval::_chartype_ascii:
        if (true)
        {
            str s;
            s._reserve(len,chartype);
            str_decode_utf8(data,data+sz,s._data_ucs1(),0,&e);
            s._set_size(len);
            return exp_str(std::move(s));
        }
        break;
    }

    /* This should never happen */
    return str();
}


template<typename Char>
exp_str str::from_utf8(Char const *data)
{
    std::size_t sz = detail::str_strlen(data);
    return std::move(from_utf8(data,sz));
}

// Specialization for external users
template
exp_str str::from_utf8(char const *,std::size_t);
template
exp_str str::from_utf8(char const *);
template
exp_str str::from_utf8(unsigned char const *,std::size_t);
template
exp_str str::from_utf8(unsigned char const *);


exp_bytes detail::strval::encode_utf8() const
{
    strval::_chartype chartype = _get_chartype();

    switch (chartype)
    {
    case _chartype_ascii:
        return bytes::from_c_bytes(_data_ucs1(),_get_size());
    case _chartype_ucs1:
        if (true)
        {
            ucs1_t const *data = _data_ucs1();
            std::size_t sz = _get_size();

            unicode_conversion_error e;
            std::ptrdiff_t retval = str_encode_utf8_count(data,sz,&e);
            if (retval == -1)
            {
                // Illegal Unicode Codepoint
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<std::size_t>(retval);
            bytes b;
            b._reserve(len);
            str_encode_utf8(data,sz,b._data(),&e);
            b._set_size(len);
            return exp_bytes(std::move(b));
        }
        break;
    case _chartype_ucs2:
        if (true)
        {
            ucs2_t const *data = _data_ucs2();
            std::size_t sz = _get_size();

            unicode_conversion_error e;
            std::ptrdiff_t retval = str_encode_utf8_count(data,sz,&e);
            if (retval == -1)
            {
                // Illegal Unicode Codepoint
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<std::size_t>(retval);
            bytes b;
            b._reserve(len);
            str_encode_utf8(data,sz,b._data(),&e);
            b._set_size(len);
            return exp_bytes(std::move(b));
        }
        break;
    case _chartype_ucs4:
        if (true)
        {
            ucs4_t const *data = _data_ucs4();
            std::size_t sz = _get_size();

            unicode_conversion_error e;
            std::ptrdiff_t retval = str_encode_utf8_count(data,sz,&e);
            if (retval == -1)
            {
                // Illegal Unicode Codepoint
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<std::size_t>(retval);
            bytes b;
            b._reserve(len);
            str_encode_utf8(data,sz,b._data(),&e);
            b._set_size(len);
            return exp_bytes(std::move(b));
        }
        break;
    }

    /* This should never happen */
    return bytes();
}


cig_bool detail::bytesval::is_pure_utf8() const noexcept
{
    byte_t const *data = _data();
    std::size_t sz = _get_size();
    return str_verify_utf8(data,data+sz,0);
}

exp_str detail::bytesval::decode_utf8() const
{
    byte_t const *data = _data();
    std::size_t sz = _get_size();
    return str::from_utf8(data,sz);
}


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
