/* cig_ast_resolver.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Abstract Syntax Tree - Reference Resolver
 */
#ifndef CIG_AST_RESOLVER_H_INCLUDED
#define CIG_AST_RESOLVER_H_INCLUDED

#include "cig_ast_node.h"

namespace cig {

namespace ast {

exp_none resolve_ast_tree(ast_node_ptr nd) noexcept;


} // namespace ast

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_AST_RESOLVER_H_INCLUDED) */
