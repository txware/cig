/* cig_ast_dump.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - AST Tree Dumper
 */
#include "cig_ast_dump.h"

#include "cig_constexpr_itoa.h"
#include "cig_ast_iterator.h"
#include "cig_str_proxy.h"
#include "cig_fmt_formatter.h"
#include "cig_toolbox.h"


namespace cig {

namespace ast {


str
dump_ast_node_full_name(bool dump_with_index,
                        ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    str name = nd->get_name();
    if (name.is_empty())
        return name;

    if (nd->get_node_type() == AST_FUNDEF)
    {
        auto fundecl = nd->get_fundecl_backref();
        if (fundecl)
            nd = fundecl;
    }

    auto br = nd->get_backref();
    auto scope = std::get<ast_node_ptr>(br);
    if (scope)
    {
        auto scopename = dump_ast_node_full_name(dump_with_index,
                                                 scope);
        if (!scopename.is_empty())
            return scopename + "."_str + name;
    }
    return name;
}

void
dump_ast_node_tag(fmt::ostream &out,
                  str indent,
                  char const *slotname,
                  bool dump_with_index,
                  ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    if (!nd)
        return;
    ast_node_type node_type = nd->get_node_type();
    str type_tag = ast_node::ast_node_type_tag(node_type);
    str name = dump_ast_node_full_name(dump_with_index,
                                       nd);
    char ovldindex[100] = "";
    char dump_number_prefix[20] = "";
    char dump_number_suffix[20] = "";
    cig::source_location sloc = nd->get_effectloc();

    if (node_type == AST_EXPR_UNQUALNAME)
        name = nd->get_unqualified_name();
    else if (node_type == AST_EXPR_MEMBER)
        name = nd->get_member_name();
    else if (node_type == AST_EXPR_LITERAL)
        name = nd->get_literal().to_str();

    if (node_type == AST_FUNDECL ||
        node_type == AST_FUNDEF)
    {
        auto obr = nd->get_overloadset_backref();
        i64 oi = std::get<i64>(obr);
        if (oi == -1 && node_type == AST_FUNDEF)
        {
            auto fundecl = nd->get_fundecl_backref();
            if (fundecl)
            {
                auto obrdecl = fundecl->get_overloadset_backref();
                oi = std::get<i64>(obrdecl);
            }
        }
        snprintf(ovldindex,sizeof(ovldindex)," #%lld",
                 static_cast<long long>(oi._v));
    }

    if (dump_with_index)
    {
        if (slotname && slotname[0])
        {
            snprintf(dump_number_prefix,sizeof(dump_number_prefix),
                     "%4s","");
            snprintf(dump_number_suffix,sizeof(dump_number_suffix),
                     " ->%lld",
                     static_cast<long long>(nd->dump_index._v));
        }
        else
        {
            snprintf(dump_number_prefix,sizeof(dump_number_prefix),
                     "%4lld",
                     static_cast<long long>(nd->dump_index._v));
            snprintf(dump_number_suffix,sizeof(dump_number_suffix),
                     "%s","");
        }
    }
    else
    {
        snprintf(dump_number_prefix,sizeof(dump_number_prefix),"%4s","");
        snprintf(dump_number_suffix,sizeof(dump_number_suffix),"%s","");
    }
    

    out.print("{}{}{}{}{}{}{}{}{} # at {}:{}\n",
              dump_number_prefix,
              indent,
              slotname ? slotname : "",
              slotname && slotname[0] ? " " : "",
              type_tag,
              name.is_empty() ? "" : " ",
              name,
              ovldindex,
              dump_number_suffix,
              sloc.line(),
              sloc.column());
}

static
void
dump_ast_node(fmt::ostream &out,
              detail::ast_node_level_info level,
              bool var_is_in_expression,
              bool dump_with_index,
              ast_node_ptr nd)
{
    using enum ast::ast_node_type;

    ast_node_type node_type = nd->get_node_type();
    str dump_number_prefix = "    "_str;
    str dump_number_prefix_m1 = "   "_str;
    str indent = str("    "_str) * level.total_level;
    str type_tag = ast_node::ast_node_type_tag(node_type);
    str indenttag = indent + " "_str * (type_tag.size() + 1);
    dump_ast_node_tag(out,indent,"",dump_with_index,nd);
    if (ast_node::ast_node_type_is_expr(node_type) &&
        (node_type != AST_VARIABLE ||
         var_is_in_expression))
    {
        if (nd->is_list() ||
            nd->has_ulvalue() ||
            nd->has_rvalue() ||
            nd->is_conditional())
            out.print("{}{}{}{}{}{}\n",
                      dump_number_prefix_m1,
                      indenttag,
                      nd->is_list() ? " list-valued" : "",
                      nd->has_ulvalue() ? " ulvalue" : "",
                      nd->has_rvalue() ? " rvalue" : "",
                      nd->is_conditional() ? " conditional" : "");
    }
    if (node_type == AST_VARIABLE &&
        var_is_in_expression)
        return;

    if (ast_node::ast_node_type_has_backref(node_type))
    {
        auto br = nd->get_backref();
        auto brnd = std::get<ast_node_ptr>(br);
        auto bridx = std::get<i64>(br);
        if (brnd || bridx != -1)
            out.print("{}{}backref {} {}\n",
                      dump_number_prefix,
                      indenttag,brnd ? "yes" : "no",
                      bridx);
        auto gbr = nd->get_global_backref();
        auto gbrnd = std::get<ast_node_ptr>(gbr);
        auto gbridx = std::get<i64>(gbr);
        if (gbrnd || gbridx != -1)
            out.print("{}{}global_backref {} {}\n",
                      dump_number_prefix,
                      indenttag,gbrnd ? "yes" : "no",
                      gbridx);
    }

    if ((ast_node::ast_node_type_is_expr(node_type) &&
         node_type != AST_EXPR_LITERAL) ||
        node_type == AST_EXPR_ENTITYLINK ||
        node_type == AST_TYPEREF)
    {
        ast_node_ptr resolved_entity = nd->get_resolved_node();
        if (resolved_entity)
        {
            dump_ast_node_tag(out,indenttag,"resolved",
                              dump_with_index,
                              resolved_entity);
            return;
#if 0
            out.print("{}{}resolved ->\n",
                      dump_number_prefix,
                      indenttag);
#endif
        }
        else
            out.print("{}{}unresolved\n",
                      dump_number_prefix,
                      indenttag);
    }


    switch (node_type)
    {
    case AST_CLASSDECL:
    {
        dump_ast_node_tag(out,indenttag,"classdef",
                          dump_with_index,
                          nd->get_classdef());
        out.print("{}{}{}\n",
                  dump_number_prefix,indenttag,
                  nd->is_static() ? "static" : "nonstatic");
        return;
    }
    case AST_ENUMDECL:
    {
        dump_ast_node_tag(out,indenttag,"enumdef",
                          dump_with_index,
                          nd->get_enumdef());
        return;
    }
    case AST_CLASSDEF:
    {
        auto classdeclbr = nd->get_classdecl_backref();
        if (classdeclbr)
            out.print("{}{}classdecl_backref {}\n",
                      dump_number_prefix,
                      indenttag,classdeclbr ? "yes" : "no");
        out.print("{}{}{}\n",
                  dump_number_prefix,indenttag,
                  nd->is_static() ? "static" : "nonstatic");
        auto ti = nd->get_type_info();
        if (ti.type_alignment == -1 &&
            ti.type_size == -1)
            out.print("{}{}no type info\n",
                      dump_number_prefix,
                      indenttag);
        else
            out.print("{}{}size {} align {} tag {}\n",
                      dump_number_prefix,
                      indenttag,
                      ti.type_size,
                      ti.type_alignment,
                      ti.bitype);
        return;
    }
    case AST_ENUMDEF:
    {
        auto enumdeclbr = nd->get_enumdecl_backref();
        if (enumdeclbr)
            out.print("{}{}enumdecl_backref {}\n",
                      dump_number_prefix,
                      indenttag,enumdeclbr ? "yes" : "no");
        return;
    }

    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
        // The fundef is dumped with the lambda
        goto dump_lambda;
    case AST_OVERLOADSET:
    {
        i64 count = nd->get_overload_count();
        for (i64 i = 0; i < count; i += 1)
        {
            char a[100];
            std::snprintf(a,sizeof(a),"[%lld]",static_cast<long long>(i._v));
            dump_ast_node_tag(out,indenttag,a,
                              dump_with_index,
                              nd->get_overload(i));
        }
        return;
    }
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
        return;

    case AST_EXPR_ENTITYLINK:
        // fixme print linked entity
        return;

    case AST_TYPEREF:
        // fixme print linked type
        return;

    case AST_FUNBODY:
    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    {
        auto lambdabr = nd->get_lambda_backref();
        if (lambdabr)
            out.print("{}{}lambda_backref {}\n",
                      dump_number_prefix,
                      indenttag,lambdabr ? "yes" : "no");
        return;
    }

    case AST_VARIABLE:
    case AST_FUNPAR:
    case AST_FUNRET:
    case AST_EXPR_LITERAL:
    case AST_ENUMVALUE:
    {
        if (node_type == AST_VARIABLE)
            out.print("{}{}{}\n",
                      dump_number_prefix,indenttag,
                      nd->is_static() ? "static" : "nonstatic");
        if (node_type == AST_ENUMVALUE)
        {
            auto value = nd->get_literal().to_str();
            out.print("{}{}value {}\n",
                      dump_number_prefix,indenttag,value);
        }             
        ast_variable_info vi = nd->get_variable_info();
        if (vi.ti.type_size == -1 &&
            vi.ti.type_alignment == -1 &&
            vi.offset == -1)
            out.print("{}{}no size/alignment/offset info\n",
                      dump_number_prefix,
                      indenttag);
        else
            out.print("{}{}size {} align {} offset {}\n",
                      dump_number_prefix,
                      indenttag,vi.ti.type_size,vi.ti.type_alignment,vi.offset);
        return;
    }

    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    {
        i64 totalvars = nd->get_subnode_count();
        out.print("{}{}init vars {}\n",
                  dump_number_prefix,
                  indenttag,totalvars);
        return;
    }

    case AST_FOROFSTMT:
    {
        i64 initvars = nd->get_initstmt_variables();
        i64 totalvars = nd->get_subnode_count();
        out.print("{}{}init vars {} iterator vars {}\n",
                  dump_number_prefix,
                  indenttag,initvars,totalvars-initvars);
        return;
    }

    case AST_EXPRMIN:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_SLICE:
    case AST_EXPR_FUNCALL:
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_CONDITION:
    case AST_EXPR_LIST:
    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_COMPOUNDSTMT:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return;
    }
    return;

 dump_lambda:
    if (1)
    {
        auto obr = nd->get_overloadset_backref();
        auto obrnd = std::get<ast_node_ptr>(obr);
        auto obridx = std::get<i64>(obr);
        if (obrnd || obridx != -1)
            out.print("{}{}overloadset_backref {} {}\n",
                      dump_number_prefix,
                      indenttag,obrnd ? "yes" : "no",
                      obridx);
        else
            out.print("{}{}overloadset_backref no\n",
                      dump_number_prefix,
                      indenttag);
        auto fdecl = nd->get_fundecl_backref();
        if (fdecl)
            out.print("{}{}fundecl_backref {}\n",
                      dump_number_prefix,
                      indenttag,fdecl ? "yes" : "no");
        auto fnidx = nd->get_interpreter_function_index();
        if (fnidx != -1)
            out.print("{}{}interpreter fnidx {}\n",
                      dump_number_prefix,
                      indenttag,fnidx);
        if (node_type == AST_FUNDECL ||
            node_type == AST_FUNDEF)
            out.print("{}{}{}\n",
                      dump_number_prefix,indenttag,
                      nd->is_static() ? "static" : "nonstatic");
        ast_function_info fi = nd->get_function_info();
        if (fi.runtime_data_alignment == -1 &&
            fi.runtime_data_size == -1)
            out.print("{}{}no size/alignment info\n",
                      dump_number_prefix,
                      indenttag);
        else
            out.print("{}{}size {} align {}\n",
                      dump_number_prefix,
                      indenttag,fi.runtime_data_size,fi.runtime_data_alignment);
    }
    dump_ast_node_tag(out,indenttag,"fundef",
                      dump_with_index,
                      nd->get_fundef());
    return;
}

void
dump_ast_tree(str filename,ast_node_ptr ast_tree,bool dump_with_index)
{
    using enum ast::ast_node_type;
    using enum ast::ast_node_br_special;

    if (ast_tree == nullptr)
        return;

    // Open the output file
    cig::detail::utf8proxy filename_utf8(filename);
    auto out = fmt::output_file(filename_utf8.c_str());

    i64 last_dump_index = -1;
    last_dump_index += 1;
    ast_tree->dump_index = last_dump_index;


    // Set all dump indices
    std::function<exp_none(ast_node_ptr nd)> numfunc =
        [&] (ast_node_ptr nd) -> exp_none {
            last_dump_index += 1;
            nd->dump_index = last_dump_index;
            return exp_none();
        };
    auto expnum = detail::ast_node_iterate_once(ast_tree,numfunc);
    if (!expnum)
        return;

    // out.print("# maximum node index after numbering: {}\n",
    //           last_dump_index);


    // Create the mapfunc
    std::function<exp_bool(ast_node_type parent_type,
                           ast_node_ptr parent,
                           detail::ast_node_level_info parent_level,
                           ast_node_type child_type,
                           ast_node_ptr child,
                           detail::ast_node_level_info child_level,
                           i64 child_index)> mapfunc =
        [&] ([[maybe_unused]] ast_node_type parent_type,
             [[maybe_unused]] ast_node_ptr parent,
             [[maybe_unused]] detail::ast_node_level_info parent_level,
             [[maybe_unused]] ast_node_type child_type,
             [[maybe_unused]] ast_node_ptr child,
             [[maybe_unused]] detail::ast_node_level_info child_level,
             [[maybe_unused]] i64 child_index) {
            if (parent_type == AST_CLASSDECL &&
                child_type == AST_CLASSDEF)
                return false;
            if (parent_type == AST_ENUMDECL &&
                child_type == AST_ENUMDEF)
                return false;
            if (parent_type == AST_FUNDECL &&
                child_type == AST_FUNDEF)
                return false;
            if (parent_type == AST_OVERLOADSET)
                return false;
            bool var_is_in_expression = false;
            if (child_type == AST_VARIABLE)
            {
                // This is the variable being defined in an expression
                if (ast_node::ast_node_type_is_expr(parent_type) ||
                    parent_type == AST_EXPRSTMT ||
                    // The for..of iteratorexpr is not a statement of its own,
                    // therefore we need to check the child index
                    (parent_type == AST_FOROFSTMT &&
                     child_index == AST_BR_ITERATOR))
                {
                    var_is_in_expression = true;
                }
                // This is the variable being defined
                // in the scope before the expression
                else if (ast_node::ast_node_type_is_stmt(parent_type))
                {
                    cig_assert(parent_type != AST_RETURNSTMT);
                    var_is_in_expression = false;
                }
                // This is the variable being defined
                // in a class or namespace
                else
                {
                    cig_assert(parent_type == AST_GLOBAL ||
                               parent_type == AST_NAMESPACE ||
                               parent_type == AST_CLASSDEF);
                    var_is_in_expression = false;
                }
            }

            dump_ast_node(out,child_level,
                          var_is_in_expression,
                          dump_with_index,
                          child);
            if (var_is_in_expression)
                return false;   // Do not iterate
            return true;
        };

    auto exp = detail::ast_node_recursive_iterator(ast_tree,true,mapfunc);
    if (!exp)
        return;
}


void
ast_tree_set_dump_indices(ast_node_ptr ast_tree)
{
    if (ast_tree == nullptr)
        return;

    i64 last_dump_index = -1;

    last_dump_index += 1;
    ast_tree->dump_index = last_dump_index;

    // Set all dump indices
    std::function<exp_none(ast_node_ptr nd)> numfunc =
        [&] (ast_node_ptr nd) -> exp_none {
            last_dump_index += 1;
            nd->dump_index = last_dump_index;
            return exp_none();
        };
    auto expnum = detail::ast_node_iterate_once(ast_tree,numfunc);
    if (!expnum)
        return;
}


} // namespace ast

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
