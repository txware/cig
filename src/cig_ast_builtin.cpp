/* cig_ast_builtin.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - Builtin Types.
 */
#include "cig_ast_builtin.h"


namespace cig {

namespace ast {



#define BUILTIN_TYPE(x,lower,upper)                                     \
    do {                                                                \
        if (global->get_global_subnode_count() !=                       \
            AST_BUILTIN_ ## upper)                                      \
            return makealgorithmic("bad index before " #x);             \
                                                                        \
        ast_type_info ti = { .type_alignment = x::type_alignment,       \
                             .type_size = x::type_size,                 \
                             .bitype = AST_BUILTIN_ ## upper };         \
        ast_node_ptr nd = ast_node::make_builtin_ast_node(ti);          \
        cig_assert(nd);                                                 \
        auto exp2 = nd->set_name(str::from_ascii_c_str(#lower));        \
        cig_assert(exp2);                                               \
        auto exp3 = nd->set_flags(ast_node::AST_FLAG_STATIC |           \
                                  ast_node::AST_FLAG_NATIVE);           \
        cig_assert(exp3);                                               \
        auto exp1 = global->add_class(nd);                              \
        if (!exp1)                                                      \
        {                                                               \
            cig::err e = std::move(exp1.error());                       \
            return makeerr(e.ecode);                                    \
        }                                                               \
    } while (0)


exp_none
global_add_builtins(ast_node_ptr global,cig::source_location sloc) noexcept
{
    using enum ast::ast_node_type;

    if (global == nullptr)
	return makeerr(err::EC_GLOBAL_NOT_EMPTY,sloc);
    if (global->get_node_type() != AST_GLOBAL)
	return makeerr(err::EC_GLOBAL_NOT_EMPTY,sloc);
    if (global->get_global_subnode_count() != 0)
	return makeerr(err::EC_GLOBAL_NOT_EMPTY,sloc);
    if (global->get_subnode_count() != 0)
	return makeerr(err::EC_GLOBAL_NOT_EMPTY,sloc);


    // These macros check that indices correspond to ast_builtin_type values.
    // That is required in cig_ast_resolver.cpp.

    BUILTIN_TYPE(cig_bool,bool,BOOL);
    BUILTIN_TYPE(u8,u8,U8);
    BUILTIN_TYPE(u16,u16,U16);
    BUILTIN_TYPE(u32,u32,U32);
    BUILTIN_TYPE(u64,u64,U64);
    BUILTIN_TYPE(i8,i8,I8);
    BUILTIN_TYPE(i16,i16,I16);
    BUILTIN_TYPE(i32,i32,I32);
    BUILTIN_TYPE(i64,i64,I64);
    BUILTIN_TYPE(str,str,STR);
    BUILTIN_TYPE(bytes,bytes,BYTES);

    i64 builtin_count = global->get_global_subnode_count();
    cig_assert(builtin_count == AST_BUILTIN_COUNT);

    return exp_none();
}


char const *builtin_type_name(ast_builtin_type bitype) noexcept
{
    switch (bitype)
    {
    case AST_BUILTIN_INVALID: return "";

    case AST_BUILTIN_BOOL: return "bool";
    case AST_BUILTIN_U8:   return "u8";
    case AST_BUILTIN_U16:  return "u16";
    case AST_BUILTIN_U32:  return "u32";
    case AST_BUILTIN_U64:  return "u64";
    case AST_BUILTIN_I8:   return "i8";
    case AST_BUILTIN_I16:  return "i16";
    case AST_BUILTIN_I32:  return "i32";
    case AST_BUILTIN_I64:  return "i64";
    case AST_BUILTIN_STR:  return "str";
    case AST_BUILTIN_BYTES:return "bytes";

    case AST_COMPOUND_TYPE: return "compound";
    case AST_VOID_TYPE: return "void";
    case AST_LIST_TYPE: return "list";
    }
    return "";
}


i64 builtin_type_alignment(ast_builtin_type builtin_type) noexcept
{
    switch (builtin_type)
    {
    case AST_BUILTIN_INVALID: return -1;

    case AST_BUILTIN_BOOL: return cig_bool::type_alignment;
    case AST_BUILTIN_U8:   return u8::type_alignment;
    case AST_BUILTIN_U16:  return u16::type_alignment;
    case AST_BUILTIN_U32:  return u32::type_alignment;
    case AST_BUILTIN_U64:  return u64::type_alignment;
    case AST_BUILTIN_I8:   return i8::type_alignment;
    case AST_BUILTIN_I16:  return i16::type_alignment;
    case AST_BUILTIN_I32:  return i32::type_alignment;
    case AST_BUILTIN_I64:  return i64::type_alignment;
    case AST_BUILTIN_STR:  return str::type_alignment;
    case AST_BUILTIN_BYTES:return bytes::type_alignment;

    case AST_COMPOUND_TYPE: return -1;
    case AST_VOID_TYPE:  return -1;
    case AST_LIST_TYPE:  return -1;
    }
    return -1;
}


i64 builtin_type_size(ast_builtin_type builtin_type) noexcept
{
    switch (builtin_type)
    {
    case AST_BUILTIN_INVALID: return -1;

    case AST_BUILTIN_BOOL: return cig_bool::type_size;
    case AST_BUILTIN_U8:   return u8::type_size;
    case AST_BUILTIN_U16:  return u16::type_size;
    case AST_BUILTIN_U32:  return u32::type_size;
    case AST_BUILTIN_U64:  return u64::type_size;
    case AST_BUILTIN_I8:   return i8::type_size;
    case AST_BUILTIN_I16:  return i16::type_size;
    case AST_BUILTIN_I32:  return i32::type_size;
    case AST_BUILTIN_I64:  return i64::type_size;
    case AST_BUILTIN_STR:  return str::type_size;
    case AST_BUILTIN_BYTES:return bytes::type_size;

    case AST_COMPOUND_TYPE: return -1;
    case AST_VOID_TYPE:  return -1;
    case AST_LIST_TYPE:  return -1;
    }
    return -1;
}


exp_ast_builtin_type
literal_to_builtin_type(literal::literal_type t) noexcept
{
    switch (t)
    {
    case literal::LIT_UNDEFINED: return makeerr(err::EC_BAD_LITERAL_TYPE);
    case literal::LIT_STR:       return AST_BUILTIN_STR;
    case literal::LIT_BYTES:     return AST_BUILTIN_BYTES;
    case literal::LIT_I64:       return AST_BUILTIN_I64;
    case literal::LIT_I32:       return AST_BUILTIN_I32;
    case literal::LIT_I16:       return AST_BUILTIN_I16;
    case literal::LIT_I8:        return AST_BUILTIN_I8;
    case literal::LIT_U64:       return AST_BUILTIN_U64;
    case literal::LIT_U32:       return AST_BUILTIN_U32;
    case literal::LIT_U16:       return AST_BUILTIN_U16;
    case literal::LIT_U8:        return AST_BUILTIN_U8;
    case literal::LIT_BOOL:      return AST_BUILTIN_BOOL;
    }
    return makeerr(err::EC_BAD_LITERAL_TYPE);
}


enum type_class {
    TPCLASS_NONE,
    TPCLASS_BOOL,
    TPCLASS_INTEGRAL,
    TPCLASS_FLOAT,              // Prepared for future float builtin types
    TPCLASS_STR,
    TPCLASS_BYTES,
    TPCLASS_OTHER
};


struct builtin_type_tpinfo {
    type_class tpclass = TPCLASS_NONE;
    int is_signed = false;      // Only for integral types
    int bitsize = 0;            // Only for integral and float types
    bool is_trivial = false;    // Trivially copyable, no destructor required
};



static
builtin_type_tpinfo
builtin_type_to_tpinfo(
    ast_builtin_type t
    ) noexcept
{
    switch (t)
    {
    case AST_BUILTIN_INVALID:
        return { };

    case AST_BUILTIN_BOOL:  return { TPCLASS_BOOL,     false,  1, true };

    case AST_BUILTIN_U8:    return { TPCLASS_INTEGRAL, false,  8, true };
    case AST_BUILTIN_U16:   return { TPCLASS_INTEGRAL, false, 16, true };
    case AST_BUILTIN_U32:   return { TPCLASS_INTEGRAL, false, 32, true };
    case AST_BUILTIN_U64:   return { TPCLASS_INTEGRAL, false, 64, true };
    case AST_BUILTIN_I8:    return { TPCLASS_INTEGRAL, true,   8, true };
    case AST_BUILTIN_I16:   return { TPCLASS_INTEGRAL, true,  16, true };
    case AST_BUILTIN_I32:   return { TPCLASS_INTEGRAL, true,  32, true };
    case AST_BUILTIN_I64:   return { TPCLASS_INTEGRAL, true,  64, true };

#if 0 // Floats prepared for later
    case AST_BUILTIN_F32:   return { TPCLASS_FLOAT,    false, 32, true };
    case AST_BUILTIN_F64:   return { TPCLASS_FLOAT,    false, 64, true };
#endif

    case AST_BUILTIN_STR:   return { TPCLASS_STR,      false,  0, false };
    case AST_BUILTIN_BYTES: return { TPCLASS_BYTES,    false,  0, false };


    case AST_COMPOUND_TYPE: return { TPCLASS_OTHER,    false,  0, false };
    case AST_VOID_TYPE:     return { TPCLASS_OTHER,    false,  0, false };
    case AST_LIST_TYPE:     return { TPCLASS_OTHER,    false,  0, false };
    }

    return { };
}


enum operation_class {
    OPCLASS_NONE,
    OPCLASS_ARITHMETIC,
    OPCLASS_BITWISE,
    OPCLASS_LOGICAL,
    OPCLASS_OTHER
};


struct node_type_opinfo {
    operation_class opclass = OPCLASS_NONE;
    int n_args = 0;
    bool is_assign = false;
    bool bool_result = false;
};


static
node_type_opinfo
ast_node_type_to_opinfo(
    ast_node_type node_type
    ) noexcept
{
    using enum ast::ast_node_type;

    switch (node_type)
    {
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_COMPOUNDSTMT:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    case AST_FOROFSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return { };

    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_SLICE:
    case AST_EXPR_FUNCALL:
        return { };

    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
        return { OPCLASS_ARITHMETIC, 1, false, false };

    case AST_EXPR_BIT_NOT:
        return { OPCLASS_BITWISE, 1, false, false };

    case AST_EXPR_LOGICAL_NOT:
        return { OPCLASS_LOGICAL, 1, false, true };

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
        return { OPCLASS_ARITHMETIC, 2, false, false };

    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
        return { OPCLASS_ARITHMETIC, 2, false, true };

    case AST_EXPR_IN:
        return { OPCLASS_OTHER, 2, false, true };

    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
        return { OPCLASS_BITWISE, 2, false, false };

    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
        return { OPCLASS_LOGICAL, 2, false, true };

    case AST_EXPR_CONDITION:
    case AST_EXPR_LIST:
        return { };

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
        return { OPCLASS_ARITHMETIC, 2, true, false };

    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
        return { OPCLASS_BITWISE, 2, true, false };

    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
        return { OPCLASS_OTHER, 2, true, false };


    }
    return { };
}


exp_ast_builtin_type
unary_expression_type(
    ast_node_type node_type,
    ast_builtin_type arg,
    cig::source_location sloc
    ) noexcept
{
    using enum ast::ast_node_type;

    auto oi = ast_node_type_to_opinfo(node_type);
    auto ti = builtin_type_to_tpinfo(arg);

    // Range check
    if (static_cast<int>(arg) < 0 ||
        static_cast<int>(arg) >= AST_BUILTIN_COUNT)
        return makeerr(err::EC_ALGORITHMIC_BAD_EXPRESSION_INPUT_TYPE,sloc);

    // No arithmetic minus sign or bitwise not operation allowed
    // with the 'bool' type
    if (arg == AST_BUILTIN_BOOL)
    {
        // Bitwise and logical operations are allowed,
        // all others are not (except arithmetic plus sign)
        if ((oi.opclass != OPCLASS_ARITHMETIC ||
             node_type != AST_EXPR_SIGNPLUS) &&
            (oi.opclass != OPCLASS_BITWISE ||
             node_type == AST_EXPR_BIT_NOT) &&
            oi.opclass != OPCLASS_LOGICAL)
            return makeerr(err::EC_OPERATION_NOT_AVAILABLE_FOR_BOOL_TYPE,sloc);

        return AST_BUILTIN_BOOL;
    }

    // Logical operations need bool type
    // except logical not ! which is allowed for all builtin data types
    if (oi.opclass == OPCLASS_LOGICAL &&
        node_type != AST_EXPR_LOGICAL_NOT)
        return makeerr(err::EC_LOGICAL_OPERATION_NEEDS_BOOL_ARGUMENTS,sloc);

    // Operations on integral types
    if (ti.tpclass == TPCLASS_INTEGRAL)
    {
        // Arithmetic and bitwise operations and plus sign are allowed,
        // all others are not
        if (oi.opclass != OPCLASS_ARITHMETIC &&
            oi.opclass != OPCLASS_BITWISE &&
            node_type != AST_EXPR_LOGICAL_NOT)
            return makeerr(err::EC_OPERATION_NOT_AVAILABLE_FOR_INT_TYPE,sloc);

        // Pick the correct result

        // If result is bool, we are done
        if (oi.bool_result)
            return AST_BUILTIN_BOOL;

        return arg;
    }

    // Bitwise operations need integral type
    if (oi.opclass == OPCLASS_BITWISE)
        return makeerr(err::EC_BITWISE_OPERATION_NEEDS_INT_ARGUMENTS,sloc);

    // Operations on float types
    if (ti.tpclass == TPCLASS_FLOAT)
    {
        // Arithmetic operations are allowed, all others are not
        if (oi.opclass != OPCLASS_ARITHMETIC)
            return makeerr(err::EC_OPERATION_NOT_AVAILABLE_FOR_FLOAT_TYPE,sloc);

        // Pick the correct result

        // If result is bool, we are done
        if (oi.bool_result)
            return AST_BUILTIN_BOOL;

        return arg;
    }


    // Operations with strings
    if (arg == AST_BUILTIN_STR)
    {
        // Unary + (plus sign)
        if (node_type == AST_EXPR_SIGNPLUS)
            return AST_BUILTIN_STR;

        // Unary ! (logical not -> is_empty())
        if (node_type == AST_EXPR_LOGICAL_NOT)
            return AST_BUILTIN_BOOL;
    }


    // Operations with byte sequences
    if (arg == AST_BUILTIN_BYTES)
    {
        // Unary + (plus sign)
        if (node_type == AST_EXPR_SIGNPLUS)
            return AST_BUILTIN_BYTES;

        // Unary ! (logical not -> is_empty())
        if (node_type == AST_EXPR_LOGICAL_NOT)
            return AST_BUILTIN_BOOL;
    }

    return makeerr(err::EC_EXPRESSION_NOT_POSSIBLE);
}


exp_ast_builtin_type
binary_expression_type(
    ast_node_type node_type,
    ast_builtin_type arg1,
    ast_builtin_type arg2,
    cig::source_location sloc
    ) noexcept
{
    using enum ast::ast_node_type;

    auto oi = ast_node_type_to_opinfo(node_type);
    auto ti1 = builtin_type_to_tpinfo(arg1);
    auto ti2 = builtin_type_to_tpinfo(arg2);

    // Range check
    if (static_cast<int>(arg1) < 0 ||
        static_cast<int>(arg1) >= AST_BUILTIN_COUNT)
        return makeerr(err::EC_ALGORITHMIC_BAD_EXPRESSION_INPUT_TYPE,sloc);
    if (static_cast<int>(arg2) < 0 ||
        static_cast<int>(arg2) >= AST_BUILTIN_COUNT)
        return makeerr(err::EC_ALGORITHMIC_BAD_EXPRESSION_INPUT_TYPE,sloc);

    // Not yet implemented: operator 'in'
    if (node_type == AST_EXPR_IN)
        return makeerr(err::EC_NOT_YET_IMPLEMENTED,sloc);
    // Not yet implemented: operator '.='
    if (node_type == AST_EXPR_IN)
        return makeerr(err::EC_NOT_YET_IMPLEMENTED,sloc);

    // No arithmetic operations allowed with the 'bool' type
    if (arg1 == AST_BUILTIN_BOOL ||
        arg2 == AST_BUILTIN_BOOL)
    {
        // Assignments
        if (oi.is_assign)
        {
            // Arithmetic assignment is not allowed
            if (oi.opclass == OPCLASS_ARITHMETIC)
                return makeerr(err::EC_NO_ARITHMETIC_ASSIGNMENT_WITH_BOOL_TYPE,
                               sloc);
            // Logical-assign, dot-assign and simple assign are allowed


            // Other assignments are allowed if assigned is bool
            // and assigned-to is bool,integer,float type
            if (arg2 != AST_BUILTIN_BOOL)
                return makeerr(err::EC_CANNOT_ASSIGN_TO_BOOL,sloc);

            // Can assign to bool,integer,float
            if (ti1.tpclass == TPCLASS_BOOL ||
                ti1.tpclass == TPCLASS_FLOAT ||
                ti1.tpclass == TPCLASS_INTEGRAL)
                return AST_VOID_TYPE;

            // To all other types, cannot assign a bool
            return makeerr(err::EC_CANNOT_ASSIGN_BOOL,sloc);
        }

        // Bitwise and logical operations are allowed,
        // all others are not
        if (oi.opclass != OPCLASS_BITWISE &&
            oi.opclass != OPCLASS_LOGICAL)
            return makeerr(err::EC_OPERATION_NOT_AVAILABLE_FOR_BOOL_TYPE,sloc);

        // For non-assignment, both types must be bool
        if (arg1 != AST_BUILTIN_BOOL ||
            arg2 != AST_BUILTIN_BOOL)
            return makeerr(err::EC_CANNOT_COMBINE_BOOL_AND_NONBOOL,sloc);

        return AST_BUILTIN_BOOL;
    }

    // Logical operations need bool type
    if (oi.opclass == OPCLASS_LOGICAL)
        return makeerr(err::EC_LOGICAL_OPERATION_NEEDS_BOOL_ARGUMENTS,sloc);

    // Operations between two integral types
    if (ti1.tpclass == TPCLASS_INTEGRAL && ti2.tpclass == TPCLASS_INTEGRAL)
    {
        // Assignments
        if (oi.is_assign)
        {
            // Assignment to smaller type not allowed
            if (ti1.bitsize < ti2.bitsize)
                return makeerr(err::EC_CANNOT_ASSIGN_TO_SMALLER_TYPE,sloc);
            if (ti1.bitsize == ti2.bitsize &&
                ti1.is_signed && !ti2.is_signed)
                return makeerr(err::EC_CANNOT_ASSIGN_TO_SIGNED_TYPE,sloc);

            // All other assignments allowed
            return AST_VOID_TYPE;
        }

        // Arithmetic and bitwise operations are allowed, all others are not
        if (oi.opclass != OPCLASS_ARITHMETIC &&
            oi.opclass != OPCLASS_BITWISE)
            return makeerr(err::EC_OPERATION_NOT_AVAILABLE_FOR_INT_TYPE,sloc);

        // Pick the correct result

        // If result is bool, we are done
        if (oi.bool_result)
            return AST_BUILTIN_BOOL;

        // Choose the greater bitsize
        if (ti1.bitsize > ti2.bitsize)
            return arg1;
        if (ti1.bitsize < ti2.bitsize)
            return arg2;

        // Choose the unsigned
        if (!ti1.is_signed)
            return arg1;
        if (!ti2.is_signed)
            return arg2;

        // The types must be the same
        if (arg1 != arg2)       // This cannot be
            return makealgorithmic("Types must be identical");
        return arg1;
    }

    // Bitwise operations need integral type
    if (oi.opclass == OPCLASS_BITWISE)
        return makeerr(err::EC_BITWISE_OPERATION_NEEDS_INT_ARGUMENTS,sloc);

    // Operations between integral or float types
    if ((ti1.tpclass == TPCLASS_INTEGRAL || ti1.tpclass == TPCLASS_FLOAT) &&
        (ti2.tpclass == TPCLASS_INTEGRAL || ti2.tpclass == TPCLASS_FLOAT))
    {
        // The int-int pairing has been handled above
        if (ti1.tpclass != TPCLASS_FLOAT &&
            ti2.tpclass != TPCLASS_FLOAT)
            return makealgorithmic("One type must be float");

        // Assignments
        if (oi.is_assign)
        {
            // Assignment of float to integral not allowed
            if (ti1.tpclass == TPCLASS_INTEGRAL)
                return makeerr(err::EC_CANNOT_ASSIGN_FLOAT_TO_INT_TYPE,sloc);

            // Assignment to smaller type not allowed
            if (ti2.tpclass == TPCLASS_FLOAT)
            {
                if (ti1.bitsize < ti2.bitsize)
                    return makeerr(err::EC_CANNOT_ASSIGN_TO_SMALLER_TYPE,sloc);
            }
            // else arg2 is an int, can be assigned to any float

            // All other assignments allowed
            return AST_VOID_TYPE;
        }

        // Arithmetic operations are allowed, all others are not
        if (oi.opclass != OPCLASS_ARITHMETIC)
            return makeerr(err::EC_OPERATION_NOT_AVAILABLE_FOR_FLOAT_TYPE,sloc);

        // Pick the correct result

        // If result is bool, we are done
        if (oi.bool_result)
            return AST_BUILTIN_BOOL;

        // Choose the greater bitsize
        if (ti1.bitsize > ti2.bitsize)
            return arg1;
        if (ti1.bitsize < ti2.bitsize)
            return arg2;

        // The types must be the same
        if (arg1 != arg2)       // This cannot be
            return makealgorithmic("Types must be identical");
        return arg1;
    }


    // Operations with strings
    if (arg1 == AST_BUILTIN_STR || arg2 == AST_BUILTIN_STR)
    {
        // Note that this is different from what the actual interpreter
        // or the C++ compiled code do:
        // These create a strexpr.

        // Only a few operations are possible with strings
        if (node_type == AST_EXPR_ADD)
        {
            if (arg1 != AST_BUILTIN_STR || arg2 != AST_BUILTIN_STR)
                return makeerr(err::EC_CANNOT_ADD_STR_AND_NONSTR,sloc);

            return AST_BUILTIN_STR;
        }
        else if (node_type == AST_EXPR_MULT)
        {
            // The other operand must be an int but not u64
            if ((ti1.tpclass == TPCLASS_INTEGRAL && arg1 != AST_BUILTIN_U64) ||
                (ti2.tpclass == TPCLASS_INTEGRAL && arg2 != AST_BUILTIN_U64))
                return AST_BUILTIN_STR;
            
            return makeerr(err::EC_CANNOT_MULT_STR_AND_NONINT,sloc);
        }
        else if (node_type == AST_EXPR_ADD_ASSIGN ||
                 node_type == AST_EXPR_DOT_ASSIGN ||
                 node_type == AST_EXPR_ASSIGN)
        {
            if (arg1 != AST_BUILTIN_STR)
                return makeerr(err::EC_CANNOT_ASSIGN_STR_TO_NONSTR,sloc);
            if (arg2 != AST_BUILTIN_STR)
                return makeerr(err::EC_CANNOT_ASSIGN_NONSTR_TO_STR,sloc);
            return AST_VOID_TYPE;
        }
        // String comparisions: ==, != , >, >=, <, <=
        else if (oi.opclass == OPCLASS_ARITHMETIC &&
                 !oi.is_assign &&
                 oi.bool_result)
        {
            if (arg1 != AST_BUILTIN_STR || arg2 != AST_BUILTIN_STR)
                return makeerr(err::EC_CANNOT_COMPARE_STR_AND_NONSTR,sloc);

            return AST_BUILTIN_BOOL;
        }
    }


    // Operations with byte sequences
    if (arg1 == AST_BUILTIN_BYTES || arg2 == AST_BUILTIN_BYTES)
    {
        // Note that this is different from what the actual interpreter
        // or the C++ compiled code do:
        // These create a bytesexpr.

        // Only a few operations are possible with byte sequences
        if (node_type == AST_EXPR_ADD)
        {
            if (arg1 != AST_BUILTIN_BYTES || arg2 != AST_BUILTIN_BYTES)
                return makeerr(err::EC_CANNOT_ADD_BYTES_AND_NONBYTES,sloc);

            return AST_BUILTIN_BYTES;
        }
        else if (node_type == AST_EXPR_MULT)
        {
            // The other operand must be an int but not u64
            if ((ti1.tpclass == TPCLASS_INTEGRAL && arg1 != AST_BUILTIN_U64) ||
                (ti2.tpclass == TPCLASS_INTEGRAL && arg2 != AST_BUILTIN_U64))
                return AST_BUILTIN_BYTES;
            
            return makeerr(err::EC_CANNOT_MULT_BYTES_AND_NONINT,sloc);
        }
        else if (node_type == AST_EXPR_ADD_ASSIGN ||
                 node_type == AST_EXPR_DOT_ASSIGN ||
                 node_type == AST_EXPR_ASSIGN)
        {
            if (arg1 != AST_BUILTIN_BYTES)
                return makeerr(err::EC_CANNOT_ASSIGN_BYTES_TO_NONBYTES,sloc);
            if (arg2 != AST_BUILTIN_BYTES)
                return makeerr(err::EC_CANNOT_ASSIGN_NONBYTES_TO_BYTES,sloc);
            return AST_VOID_TYPE;
        }
        // Byte sequence comparisions: ==, != , >, >=, <, <=
        else if (oi.opclass == OPCLASS_ARITHMETIC &&
                 !oi.is_assign &&
                 oi.bool_result)
        {
            if (arg1 != AST_BUILTIN_BYTES || arg2 != AST_BUILTIN_BYTES)
                return makeerr(err::EC_CANNOT_COMPARE_BYTES_AND_NONBYTES,sloc);

            return AST_BUILTIN_BOOL;
        }
    }

    return makeerr(err::EC_EXPRESSION_NOT_POSSIBLE);
}


cig_bool
implicit_cast_possible_preserving_sign(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept
{
    auto fromti = builtin_type_to_tpinfo(fromtype);
    auto toti = builtin_type_to_tpinfo(totype);

    // No void, list, compound type
    if (fromti.tpclass == TPCLASS_OTHER ||
        toti.tpclass == TPCLASS_OTHER)
        return false;

    // Bool can be cast to any integral and float type
    if (fromtype == AST_BUILTIN_BOOL &&
        (toti.tpclass == TPCLASS_INTEGRAL ||
         toti.tpclass == TPCLASS_FLOAT))
        return true;

    // Otherwise the typeclass cannot be changed
    if (fromti.tpclass != toti.tpclass)
        return false;

    // Within the same typeclass, the size cannot be shrunk
    if (fromti.bitsize > toti.bitsize)
        return false;

    // Within the same typeclass, the sign cannot be changed
    if (fromti.is_signed != toti.is_signed)
        return false;

    // All others are possible
    return true;
}


cig_bool
implicit_cast_possible_preserving_integral(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept
{
    auto fromti = builtin_type_to_tpinfo(fromtype);
    auto toti = builtin_type_to_tpinfo(totype);

    // No void, list, compound type
    if (fromti.tpclass == TPCLASS_OTHER ||
        toti.tpclass == TPCLASS_OTHER)
        return false;

    // Bool can be cast to any integral and float type
    if (fromtype == AST_BUILTIN_BOOL &&
        (toti.tpclass == TPCLASS_INTEGRAL ||
         toti.tpclass == TPCLASS_FLOAT))
        return true;

    // Otherwise the typeclass cannot be changed
    if (fromti.tpclass != toti.tpclass)
        return false;

    // Within the same typeclass, the size cannot be shrunk
    if (fromti.bitsize > toti.bitsize)
        return false;

    // Within integral typeclass and size, cannot change from unsigned to signed
    if (fromti.tpclass == TPCLASS_INTEGRAL &&
        toti.tpclass == TPCLASS_INTEGRAL &&
        fromti.bitsize == toti.bitsize)
    {
        if (!fromti.is_signed && toti.is_signed)
            return false;
    }

    // All others are possible
    return true;
}


cig_bool
implicit_cast_possible(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept
{
    auto fromti = builtin_type_to_tpinfo(fromtype);
    auto toti = builtin_type_to_tpinfo(totype);

    // Note: This function should be roughly equivalent
    // to binary_expression_type(AST_EXPR_ASSIGN,totype,fromtype)

    // No void, list, compound type
    if (fromti.tpclass == TPCLASS_OTHER ||
        toti.tpclass == TPCLASS_OTHER)
        return false;

    // Bool can be cast to any integral and float type
    if (fromtype == AST_BUILTIN_BOOL &&
        (toti.tpclass == TPCLASS_INTEGRAL ||
         toti.tpclass == TPCLASS_FLOAT))
        return true;

    // Otherwise the typeclass cannot be changed except for integral->float
    if (fromti.tpclass != toti.tpclass &&
        (fromti.tpclass != TPCLASS_INTEGRAL ||
         toti.tpclass != TPCLASS_FLOAT))
        return false;

    // Within the same typeclass, the size cannot be shrunk
    if (fromti.tpclass == toti.tpclass)
    {
        if (fromti.bitsize > toti.bitsize)
            return false;
    }
    // but int->float allows to shrink the bitsize

    // Within integral typeclass and size, cannot change from unsigned to signed
    if (fromti.tpclass == TPCLASS_INTEGRAL &&
        toti.tpclass == TPCLASS_INTEGRAL &&
        fromti.bitsize == toti.bitsize)
    {
        if (!fromti.is_signed && toti.is_signed)
            return false;
    }

    // All others are possible
    return true;
}


cig_bool
explicit_cast_possible(
    ast_builtin_type fromtype,
    ast_builtin_type totype
    ) noexcept
{
    auto fromti = builtin_type_to_tpinfo(fromtype);
    auto toti = builtin_type_to_tpinfo(totype);

    // No void, list, compound type
    if (fromti.tpclass == TPCLASS_OTHER ||
        toti.tpclass == TPCLASS_OTHER)
        return false;

    // Each type can be cast to bool
    if (totype == AST_BUILTIN_BOOL)
        return true;

    // Bool can be cast to any integral and float type
    if (fromtype == AST_BUILTIN_BOOL &&
        (toti.tpclass == TPCLASS_INTEGRAL ||
         toti.tpclass == TPCLASS_FLOAT))
        return true;

    // Here both types are not bool

    // If both types are numbers, explicit cast is possible
    if ((fromti.tpclass == TPCLASS_INTEGRAL ||
         fromti.tpclass == TPCLASS_FLOAT) &&
        (toti.tpclass == TPCLASS_INTEGRAL ||
         toti.tpclass == TPCLASS_FLOAT))
        return true;

    // If the type class of non-numbers is different, fail
    if (fromti.tpclass != toti.tpclass)
        return false;

    // All others are possible
    return true;
}



} // namespace ast

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
