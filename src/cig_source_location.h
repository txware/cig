/* cig_source_location.h
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Source location compatible with std::source_location.
 */
#ifndef CIG_SOURCE_LOCATION_H_INCLUDED
#define CIG_SOURCE_LOCATION_H_INCLUDED

//
// Enable std::source_location
//

#if defined(__clang__) && (__clang_major__ < 16 || __cpp_concepts < 202002L)
#    include "tl_expected.hpp"  // std::unexpected
#else
#  include <expected>             // std::unexpected
#endif


#if defined(__clang__) && (__clang_major__ < 15)
#    include <cstdint>          // uint_least_32_t
namespace std {
    struct source_location {
        constexpr source_location() noexcept { }
        constexpr source_location( [[ maybe_unused ]] const source_location& other ) { }
	constexpr source_location( [[ maybe_unused ]] source_location&& other ) noexcept { }
        constexpr std::uint_least32_t line() const noexcept
        {
            return 0;
        }
        constexpr std::uint_least32_t column() const noexcept
        {
            return 0;
        }
        constexpr const char* file_name() const noexcept
        {
            return "<unknown filename>";
        }
        constexpr const char* function_name() const noexcept
        {
            return "<unknown functionname>";
        }
        static constexpr source_location current() noexcept
        {
            return source_location();
        }
    };
}
#else
#  include <source_location>      // std::source_location
#endif


#include <cstdint>		/* uint32_t */



namespace cig {

// We need our own source_location
// since std::source_location is read-only.
// We need one that can be filled when our own parser runs.

struct source_location {

private:
    // The internal data may change, just as with std::source_location
    uint32_t aux_line { };
    uint32_t aux_column { };
    char const *aux_file_name { };
    char const *aux_function_name { };

public:

    constexpr source_location(std::source_location loc)
    {
        aux_line = loc.line();
        aux_column = loc.column();
        aux_file_name = loc.file_name();
        aux_function_name = loc.function_name();
    }

    constexpr source_location() = default;
    constexpr source_location(source_location const &) = default;
    constexpr source_location(source_location &&) = default;
    constexpr source_location(int64_t line,
                              int64_t column,
                              char const *file_name,
                              char const *function_name) noexcept :
        aux_line { static_cast<uint32_t>(line) },
        aux_column { static_cast<uint32_t>(column) },
        aux_file_name { file_name },
        aux_function_name { function_name }
        {};
    constexpr source_location & operator =(source_location const &) = default;
    constexpr source_location & operator =(source_location &&) = default;

    constexpr uint_fast32_t line() const noexcept;
    constexpr uint_fast32_t column() const noexcept;
    constexpr const char * file_name() const noexcept;
    constexpr const char * function_name() const noexcept;
};


constexpr uint_fast32_t source_location::line() const noexcept
{
    return aux_line;
}

constexpr uint_fast32_t source_location::column() const noexcept
{
    return aux_column;
}

constexpr const char * source_location::file_name() const noexcept
{
    return aux_file_name;
}

constexpr const char * source_location::function_name() const noexcept
{
    return aux_function_name;
}



} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_SOURCE_LOCATION_H_INCLUDED) */
