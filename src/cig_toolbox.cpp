/* cig_toolbox.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * A class with various tools for CIG datatypes.
 */
#include "cig_toolbox.h"
#include "cig_datatypes.h"

#include "cig_unicode_conversion.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_constexpr_digits.h"
#include "cig_integer_conversion.h"

#include <cstddef>              // std::size_t,std::ptrdiff_t
#include <cstdint>              // uint64_t ...

namespace cig {



// To return an algorithmic error
#define ALGORITHMIC(text) std::unexpected(err::algorithmic(text))



//
// Count lines and columns
//


void toolbox::count_line_and_column(detail::strval const &s,
				    i64 startpos,i64 endpos,
				    u64 &line,u64 &column,
				    u64 tabsize) noexcept
{
    int64_t sz1 = static_cast<int64_t>(s._get_size());
    if (startpos < 0)
        startpos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (startpos > endpos)
        return;
    uint64_t lin = line._v;
    uint64_t col = column._v;
    uint64_t tab = (tabsize > 0 ? tabsize._v : 1);
    using namespace detail;

    switch (s._get_chartype())
    {
    case strval::_chartype_ucs4:
        if (true)
        {
            strval::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            for (int64_t i = startpos._v; i < endpos; i++)
            {
                strval::ucs4_t c = p[i];
                if (c == '\n')
                    lin++, col = 0;
                else if (c == '\t')
                    col += tab - (col % tab);
                else
                    col++;
            }
        }
        break;
    case strval::_chartype_ucs2:
        if (true)
        {
            strval::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            for (int64_t i = startpos._v; i < endpos; i++)
            {
                strval::ucs2_t c = p[i];
                if (c == '\n')
                    lin++, col = 0;
                else if (c == '\t')
                    col += tab - (col % tab);
                else
                    col++;
            }
        }
        break;
    default:
        if (true)
        {
            strval::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            for (int64_t i = startpos._v; i < endpos; i++)
            {
                strval::ucs1_t c = p[i];
                if (c == '\n')
                    lin++, col = 0;
                else if (c == '\t')
                    col += tab - (col % tab);
                else
                    col++;
            }
        }
        break;
    }

    line._v = lin;
    column._v = col;
}


void toolbox::count_line_and_column(detail::bytesval const &b,
				    i64 startpos,i64 endpos,
				    u64 &line,u64 &column,
				    u64 tabsize) noexcept
{
    int64_t sz1 = static_cast<int64_t>(b._get_size());
    if (startpos < 0)
        startpos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (startpos > endpos)
        return;
    uint64_t lin = line._v;
    uint64_t col = column._v;
    uint64_t tab = (tabsize > 0 ? tabsize._v : 1);
    using namespace detail;

    bytesval::byte_t const * CIG_RESTRICT p = b._data();
    for (int64_t i = startpos._v; i < endpos; i++)
    {
        bytesval::byte_t c = p[i];
        if (c == '\n')
            lin++, col = 0;
        else if (c == '\t')
            col += tab - (col % tab);
        else if ((c & 0xc0) != 0x80) // skip UTF-8 chars after the first
            col++;
    }
    line._v = lin;
    column._v = col;
}


//
// Decode strings, bytes, and single character values from quoted source
//


inline
exp_str
toolbox::exp_decode_str_from_str(
    detail::strval const &s,    // [i] input string
    i64 startpos,               // [i] start position in s
    i64 &endpos,                // [o] end position or error position in s
    // Behavior configuration
    long delimiter,             // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete,     // [i] stop parsing before incomplete codepoint
    long n_codepoints_max,      // [i] maximum number of codepoints in o
    int quoted_form,            // [i] whether control chars must be quoted
    int with_delimiters,        // [i] require delimiters around input
    int zero_terminate          // [i] whether to append a trailing '\0'
    ) noexcept
{
    using namespace detail;
    i64 sz = s.size();

    if (startpos < 0)
        startpos = 0;
    if (startpos > sz)
        startpos = sz;

    switch (s._get_chartype())
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            unsigned long maxcp;
            unsigned long n_cu;
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_decode_utf32_count<
                    detail::unicode_conversion_encoding::utf32>(
                        p+startpos._v,  // s
                        p+sz._v,        // send
                        0,              // code points
                        &maxcp,         // maximum code point
                        &n_cu,          // same as number of code points parsed
                        &e,             // error
                        delimiter,stop_if_incomplete,n_codepoints_max,
                        quoted_form,with_delimiters,zero_terminate);
            if (retval == -1)
            {
                endpos = startpos + static_cast<int64_t>(n_cu);
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<unsigned long>(retval);
            str::_chartype chartype =
                str::_chartype_by_max_codepoint(static_cast<uint32_t>(maxcp));
            switch (chartype)
            {
            case str::_chartype_ucs4:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype);
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs4(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            case str::_chartype_ucs2:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype);
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs2(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            default:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype); // ascii or ucs1
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs1(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            }
        }
        break;

    case str::_chartype_ucs2:
        if (true)
        {
            str::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            unsigned long maxcp;
            unsigned long n_cu;
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_decode_utf32_count<
                    detail::unicode_conversion_encoding::utf32>(
                        p+startpos._v,  // s
                        p+sz._v,        // send
                        0,              // code points
                        &maxcp,         // maximum code point
                        &n_cu,          // same as number of code points parsed
                        &e,             // error
                        delimiter,stop_if_incomplete,n_codepoints_max,
                        quoted_form,with_delimiters,zero_terminate);
            if (retval == -1)
            {
                endpos = startpos + static_cast<int64_t>(n_cu);
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<unsigned long>(retval);
            str::_chartype chartype =
                str::_chartype_by_max_codepoint(static_cast<uint32_t>(maxcp));
            switch (chartype)
            {
            case str::_chartype_ucs4:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype);
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs4(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            case str::_chartype_ucs2:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype);
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs2(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            default:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype); // ascii or ucs1
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs1(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            }
        }
        break;

    default:
        if (true)
        {
            str::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            unsigned long maxcp;
            unsigned long n_cu;
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_decode_utf32_count<
                    detail::unicode_conversion_encoding::utf32>(
                        p+startpos._v,  // s
                        p+sz._v,        // send
                        0,              // code points
                        &maxcp,         // maximum code point
                        &n_cu,          // same as number of code points parsed
                        &e,             // error
                        delimiter,stop_if_incomplete,n_codepoints_max,
                        quoted_form,with_delimiters,zero_terminate);
            if (retval == -1)
            {
                endpos = startpos + static_cast<int64_t>(n_cu);
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<unsigned long>(retval);
            str::_chartype chartype =
                str::_chartype_by_max_codepoint(static_cast<uint32_t>(maxcp));
            switch (chartype)
            {
            case str::_chartype_ucs4:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype);
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs4(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            case str::_chartype_ucs2:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype);
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs2(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            default:
                if (true)
                {
                    str o;
                    unsigned long n_cu2;

                    o._reserve(len,chartype); // ascii or ucs1
                    unicode_conversion_decode_utf32<
                        unicode_conversion_encoding::utf32>(
                            p+startpos._v,  // s
                            p+sz._v,        // send
                            o._data_ucs1(), // o
                            0,              // code points
                            &n_cu2,         // number of code units parsed
                            &e,             // error, discarded
                            delimiter,stop_if_incomplete,n_codepoints_max,
                            quoted_form,with_delimiters,zero_terminate);
                    if (n_cu != n_cu2)
                    {
                        endpos = startpos;
                        return ALGORITHMIC("inconsistent n_cu and n_cu2");
                    }
                    o._set_size(len);
                    endpos = startpos + static_cast<int64_t>(n_cu);
                    return exp_str(std::move(o));
                }
                break;
            }
        }
        break;
    }

    /* This should never happen */
    return str();
}


inline
exp_str
toolbox::exp_decode_str_from_bytes(
    detail::bytesval const &b,  // [i] input bytes
    i64 startpos,               // [i] start position in b
    i64 &endpos,                // [o] end position or error position in b
    // Behavior configuration
    long delimiter,             // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete,     // [i] stop parsing before incomplete codepoint
    long n_codepoints_max,      // [i] maximum number of codepoints in o
    int quoted_form,            // [i] whether control chars must be quoted
    int with_delimiters,        // [i] require delimiters around input
    int zero_terminate          // [i] whether to append a trailing '\0'
    ) noexcept
{
    using namespace detail;
    i64 sz = b.size();

    if (startpos < 0)
        startpos = 0;
    if (startpos > sz)
        startpos = sz;

    bytes::byte_t const * CIG_RESTRICT p = b._data();
    unsigned long maxcp;
    unsigned long n_cu;
    unicode_conversion_error e;
    std::ptrdiff_t retval =
        unicode_conversion_decode_utf8_count<
            detail::unicode_conversion_encoding::utf32>(
                p+startpos._v,  // s
                p+sz._v,        // send
                0,              // code points
                &maxcp,         // maximum code point
                &n_cu,          // number of bytes read
                0,              // not interested in code points in b
                &e,             // error
                delimiter,stop_if_incomplete,n_codepoints_max,
                0,0,            // no surrogate pairs, no C0 80 for zero
                quoted_form,with_delimiters,zero_terminate);
    if (retval == -1)
    {
        endpos = startpos + static_cast<int64_t>(n_cu);
        return makeerr(detail::unicode_conversion_map_error(e.type));
    }

    std::size_t len = static_cast<unsigned long>(retval);
    str::_chartype chartype =
        str::_chartype_by_max_codepoint(static_cast<uint32_t>(maxcp));
    switch (chartype)
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str o;
            unsigned long n_cu2;

            o._reserve(len,chartype);
            unicode_conversion_decode_utf8<
                unicode_conversion_encoding::utf32>(
                    p+startpos._v,  // s
                    p+sz._v,        // send
                    o._data_ucs4(), // o
                    0,              // code points
                    &n_cu2,         // number of code units parsed
                    0,              // not interested in code points in b
                    &e,             // error, discarded
                    delimiter,stop_if_incomplete,n_codepoints_max,
                    0,0,            // no surrogate pairs, no C0 80 for zero
                    quoted_form,with_delimiters,zero_terminate);
            if (n_cu != n_cu2)
            {
                endpos = startpos;
                return ALGORITHMIC("inconsistent n_cu and n_cu2");
            }
            o._set_size(len);
            endpos = startpos + static_cast<int64_t>(n_cu);
            return exp_str(std::move(o));
        }
        break;

    case str::_chartype_ucs2:
        if (true)
        {
            str o;
            unsigned long n_cu2;

            o._reserve(len,chartype);
            unicode_conversion_decode_utf8<
                unicode_conversion_encoding::utf32>(
                    p+startpos._v,  // s
                    p+sz._v,        // send
                    o._data_ucs2(), // o
                    0,              // code points
                    &n_cu2,         // number of code units parsed
                    0,              // not interested in code points in b
                    &e,             // error, discarded
                    delimiter,stop_if_incomplete,n_codepoints_max,
                    0,0,            // no surrogate pairs, no C0 80 for zero
                    quoted_form,with_delimiters,zero_terminate);
            if (n_cu != n_cu2)
            {
                endpos = startpos;
                return ALGORITHMIC("inconsistent n_cu and n_cu2");
            }
            o._set_size(len);
            endpos = startpos + static_cast<int64_t>(n_cu);
            return exp_str(std::move(o));
        }
        break;

    default:
        if (true)
        {
            str o;
            unsigned long n_cu2;

            o._reserve(len,chartype);
            unicode_conversion_decode_utf8<
                unicode_conversion_encoding::utf32>(
                    p+startpos._v,  // s
                    p+sz._v,        // send
                    o._data_ucs1(), // o
                    0,              // code points
                    &n_cu2,         // number of code units parsed
                    0,              // not interested in code points in b
                    &e,             // error, discarded
                    delimiter,stop_if_incomplete,n_codepoints_max,
                    0,0,            // no surrogate pairs, no C0 80 for zero
                    quoted_form,with_delimiters,zero_terminate);
            if (n_cu != n_cu2)
            {
                endpos = startpos;
                return ALGORITHMIC("inconsistent n_cu and n_cu2");
            }
            o._set_size(len);
            endpos = startpos + static_cast<int64_t>(n_cu);
            return exp_str(std::move(o));
        }
        break;
    }
 
    /* This should never happen */
    return str();
}


inline
exp_bytes
toolbox::exp_decode_bytes_from_str(
    detail::strval const &s,    // [i] input string
    i64 startpos,               // [i] start position in s
    i64 &endpos,                // [o] end position or error position in s
    // Behavior configuration
    long delimiter,             // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete,     // [i] stop parsing before incomplete codepoint
    long n_codepoints_max,      // [i] maximum number of codepoints in o
    int quoted_form,            // [i] whether control chars must be quoted
    int with_delimiters,        // [i] require delimiters around input
    int zero_terminate          // [i] whether to append a trailing '\0'
    ) noexcept
{
    using namespace detail;
    i64 sz = s.size();

    if (startpos < 0)
        startpos = 0;
    if (startpos > sz)
        startpos = sz;

    switch (s._get_chartype())
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            unsigned long n_cu;
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_decode_utf32_count<
                    detail::unicode_conversion_encoding::utf8>(
                        p+startpos._v,  // s
                        p+sz._v,        // send
                        0,              // code points
                        0,              // maximum code point
                        &n_cu,          // number of bytes parsed
                        &e,             // error
                        delimiter,stop_if_incomplete,n_codepoints_max,
                        quoted_form,with_delimiters,zero_terminate);
            if (retval == -1)
            {
                endpos = startpos + static_cast<int64_t>(n_cu);
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<unsigned long>(retval);
            bytes o;
            unsigned long n_cu2;

            o._reserve(len);
            unicode_conversion_decode_utf32<
                unicode_conversion_encoding::utf8>(
                    p+startpos._v,  // s
                    p+sz._v,        // send
                    o._data(),      // o
                    0,              // code points
                    &n_cu2,         // number of bytes parsed
                    &e,             // error, discarded
                    delimiter,stop_if_incomplete,n_codepoints_max,
                    quoted_form,with_delimiters,zero_terminate);
            if (n_cu != n_cu2)
            {
                endpos = startpos;
                return ALGORITHMIC("inconsistent n_cu and n_cu2");
            }
            o._set_size(len);
            endpos = startpos + static_cast<int64_t>(n_cu);
            return exp_bytes(std::move(o));
        }
        break;

    case str::_chartype_ucs2:
        if (true)
        {
            str::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            unsigned long maxcp;
            unsigned long n_cu;
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_decode_utf32_count<
                    detail::unicode_conversion_encoding::utf8>(
                        p+startpos._v,  // s
                        p+sz._v,        // send
                        0,              // code points
                        &maxcp,         // maximum code point
                        &n_cu,          // number of bytes parsed
                        &e,             // error
                        delimiter,stop_if_incomplete,n_codepoints_max,
                        quoted_form,with_delimiters,zero_terminate);
            if (retval == -1)
            {
                endpos = startpos + static_cast<int64_t>(n_cu);
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<unsigned long>(retval);
            bytes o;
            unsigned long n_cu2;

            o._reserve(len);
            unicode_conversion_decode_utf32<
                unicode_conversion_encoding::utf8>(
                    p+startpos._v,  // s
                    p+sz._v,        // send
                    o._data(),      // o
                    0,              // code points
                    &n_cu2,         // number of bytes parsed
                    &e,             // error, discarded
                    delimiter,stop_if_incomplete,n_codepoints_max,
                    quoted_form,with_delimiters,zero_terminate);
            if (n_cu != n_cu2)
            {
                endpos = startpos;
                return ALGORITHMIC("inconsistent n_cu and n_cu2");
            }
            o._set_size(len);
            endpos = startpos + static_cast<int64_t>(n_cu);
            return exp_bytes(std::move(o));
        }
        break;

    default:
        if (true)
        {
            str::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            unsigned long maxcp;
            unsigned long n_cu;
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_decode_utf32_count<
                    detail::unicode_conversion_encoding::utf8>(
                        p+startpos._v,  // s
                        p+sz._v,        // send
                        0,              // code points
                        &maxcp,         // maximum code point
                        &n_cu,          // number of bytes parsed
                        &e,             // error
                        delimiter,stop_if_incomplete,n_codepoints_max,
                        quoted_form,with_delimiters,zero_terminate);
            if (retval == -1)
            {
                endpos = startpos + static_cast<int64_t>(n_cu);
                return makeerr(detail::unicode_conversion_map_error(e.type));
            }
            std::size_t len = static_cast<unsigned long>(retval);
            bytes o;
            unsigned long n_cu2;

            o._reserve(len);
            unicode_conversion_decode_utf32<
                unicode_conversion_encoding::utf8>(
                    p+startpos._v,  // s
                    p+sz._v,        // send
                    o._data(),      // o
                    0,              // code points
                    &n_cu2,         // number of bytes parsed
                    &e,             // error, discarded
                    delimiter,stop_if_incomplete,n_codepoints_max,
                    quoted_form,with_delimiters,zero_terminate);
            if (n_cu != n_cu2)
            {
                endpos = startpos;
                return ALGORITHMIC("inconsistent n_cu and n_cu2");
            }
            o._set_size(len);
            endpos = startpos + static_cast<int64_t>(n_cu);
            return exp_bytes(std::move(o));
        }
        break;
    }

    /* This should never happen */
    return bytes();
}


inline
exp_bytes
toolbox::exp_decode_bytes_from_bytes(
    detail::bytesval const &b,  // [i] input bytes
    i64 startpos,               // [i] start position in b
    i64 &endpos,                // [o] end position or error position in b
    // Behavior configuration
    long delimiter,             // [i] code unit in s to stop at unless escaped
    int stop_if_incomplete,     // [i] stop parsing before incomplete codepoint
    long n_codepoints_max,      // [i] maximum number of codepoints in o
    int quoted_form,            // [i] whether control chars must be quoted
    int with_delimiters,        // [i] require delimiters around input
    int zero_terminate          // [i] whether to append a trailing '\0'
    ) noexcept
{
    using namespace detail;
    i64 sz = b.size();

    if (startpos < 0)
        startpos = 0;
    if (startpos > sz)
        startpos = sz;

    bytes::byte_t const * CIG_RESTRICT p = b._data();
    unsigned long maxcp;
    unsigned long n_cu;
    unicode_conversion_error e;
    std::ptrdiff_t retval =
        unicode_conversion_decode_utf8_count<
            detail::unicode_conversion_encoding::utf8>(
                p+startpos._v,  // s
                p+sz._v,        // send
                0,              // code points
                &maxcp,         // maximum code point
                &n_cu,          // number of bytes parsed
                0,              // not interested in code points in b
                &e,             // error
                delimiter,stop_if_incomplete,n_codepoints_max,
                0,0,            // no surrogate pairs, no C0 80 for zero
                quoted_form,with_delimiters,zero_terminate);
    if (retval == -1)
    {
        endpos = startpos + static_cast<int64_t>(n_cu);
        return makeerr(detail::unicode_conversion_map_error(e.type));
    }

    std::size_t len = static_cast<unsigned long>(retval);
    bytes o;
    unsigned long n_cu2;

    o._reserve(len);
    unicode_conversion_decode_utf8<
        unicode_conversion_encoding::utf8>(
            p+startpos._v,  // s
            p+sz._v,        // send
            o._data(),      // o
            0,              // code points
            &n_cu2,         // number of bytes parsed
            0,              // not interested in code points in b
            &e,             // error, discarded
            delimiter,stop_if_incomplete,n_codepoints_max,
            0,0,            // no surrogate pairs, no C0 80 for zero
            quoted_form,with_delimiters,zero_terminate);
    if (n_cu != n_cu2)
    {
        endpos = startpos;
        return ALGORITHMIC("inconsistent n_cu and n_cu2");
    }
    o._set_size(len);
    endpos = startpos + static_cast<int64_t>(n_cu);
    return exp_bytes(std::move(o));
}


// Decode a str in JSON syntax from source str or UTF-8 bytes
// On error, endpos marks the error position in s or b
exp_str
toolbox::exp_decode_json_str(detail::strval const &s,
                             i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_str_from_str(
        s,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        1,              // JSON style quoting
        1,              // with delimiters
        0);             // do not zero-terminate
}

exp_str
toolbox::exp_decode_json_str(detail::bytesval const &b,
                             i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_str_from_bytes(
        b,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        1,              // JSON style quoting
        1,              // with delimiters
        0);             // do not zero-terminate
}


// Decode a str in C syntax from source str or UTF-8 bytes
// On error, endpos marks the error position in s or b
exp_str
toolbox::exp_decode_C_str(detail::strval const &s,
                          i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_str_from_str(
        s,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        2,              // C style quoting, only accepting proper Unicode
        1,              // with delimiters
        0);             // do not zero-terminate
}

exp_str
toolbox::exp_decode_C_str(detail::bytesval const &b,
                          i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_str_from_bytes(
        b,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        2,              // C style quoting, only accepting proper Unicode
        1,              // with delimiters
        0);             // do not zero-terminate
}


// Decode a UTF-8 bytes in C syntax from source str or UTF-8 bytes
// On error, endpos marks the error position in s or b
exp_bytes
toolbox::exp_decode_C_bytes_u8(detail::strval const &s,
                               i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_bytes_from_str(
        s,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        2,              // C style quoting, only accepting proper Unicode
        1,              // with delimiters
        0);             // do not zero-terminate
}

exp_bytes
toolbox::exp_decode_C_bytes_u8(detail::bytesval const &b,
                               i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_bytes_from_bytes(
        b,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        2,              // C style quoting, only accepting proper Unicode
        1,              // with delimiters
        0);             // do not zero-terminate
}


// Decode a raw bytes in C syntax from source str or UTF-8 bytes
// On error, endpos marks the error position in s or b
exp_bytes
toolbox::exp_decode_C_bytes(detail::strval const &s,
                            i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_bytes_from_str(
        s,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        3,              // C style quoting, accept non-UTF-8 bytes also
        1,              // with delimiters
        0);             // do not zero-terminate
}

exp_bytes
toolbox::exp_decode_C_bytes(detail::bytesval const &b,
                            i64 startpos,i64 &endpos) noexcept
{
    return exp_decode_bytes_from_bytes(
        b,startpos,endpos,
        '"',            // delimiter is double quote
        0,              // do not stop if incomplete
        -1,             // maximum code points to read
        3,              // C style quoting, accept non-UTF-8 bytes also
        1,              // with delimiters
        0);             // do not zero-terminate
}


// Decode a Unicode code point in C syntax from source str or UTF-8 bytes
// This is a single-character literal
// On error, endpos marks the error position in s or b
exp_i64
toolbox::exp_decode_quoted_codepoint(detail::strval const &s,
                                     i64 startpos,i64 &endpos) noexcept
{
    using namespace detail;
    i64 sz = s.size();
    str::ucs4_t ch;
    std::ptrdiff_t retval;
    unsigned long n_cu;
    unicode_conversion_error e;

    if (startpos < 0)
        startpos = 0;
    if (startpos > sz)
        startpos = sz;

    switch (s._get_chartype())
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            retval =
                unicode_conversion_decode_utf32<
                    detail::unicode_conversion_encoding::utf32>(
                        p+startpos._v,p+sz._v, // Input string: s, send
                        &ch,
                        0,&n_cu,        // same as number of code points parsed
                        &e,             // error
                        '\'',           // delimiter is single quote
                        0,              // do not stop if incomplete
                        1,              // maximum code points to go into &ch
                        2,              // C style enforcing valid code point
                        1,              // with delimiters
                        0               // do not zero-terminate
                        );
        }
        break;
    case str::_chartype_ucs2:
        if (true)
        {
            str::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            retval =
                unicode_conversion_decode_utf32<
                    detail::unicode_conversion_encoding::utf32>(
                        p+startpos._v,p+sz._v, // Input string: s, send
                        &ch,
                        0,&n_cu,        // same as number of code points parsed
                        &e,             // error
                        '\'',           // delimiter is single quote
                        0,              // do not stop if incomplete
                        1,              // maximum code points to go into &ch
                        2,              // C style enforcing valid code point
                        1,              // with delimiters
                        0               // do not zero-terminate
                        );
        }
        break;
    default:
        if (true)
        {
            str::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            retval =
                unicode_conversion_decode_utf32<
                    detail::unicode_conversion_encoding::utf32>(
                        p+startpos._v,p+sz._v, // Input string: s, send
                        &ch,
                        0,&n_cu,        // same as number of code points parsed
                        &e,             // error
                        '\'',           // delimiter is single quote
                        0,              // do not stop if incomplete
                        1,              // maximum code points to go into &ch
                        2,              // C style enforcing valid code point
                        1,              // with delimiters
                        0               // do not zero-terminate
                        );
        }
        break;
    }
    // Decoding error
    if (retval == -1)
    {
        endpos = startpos + static_cast<int64_t>(n_cu);
        return makeerr(detail::unicode_conversion_map_error(e.type));
    }
    // String length 0 means empty char: ''
    else if (retval == 0)
    {
        endpos = startpos + 1;
        return makeerr(err::EC_EMPTY_CHARACTER_LITERAL);
    }
    endpos = startpos + static_cast<int64_t>(n_cu);
    return static_cast<int64_t>(ch);
}

exp_i64
toolbox::exp_decode_quoted_codepoint(detail::bytesval const &b,
                                     i64 startpos,i64 &endpos) noexcept
{
    using namespace detail;
    i64 sz = b.size();
    str::ucs4_t ch;
    std::ptrdiff_t retval;
    unsigned long n_cu;
    unicode_conversion_error e;

    if (startpos < 0)
        startpos = 0;
    if (startpos > sz)
        startpos = sz;

    bytes::byte_t const * CIG_RESTRICT p = b._data();
    retval =
        unicode_conversion_decode_utf8<
            detail::unicode_conversion_encoding::utf32>(
                p+startpos._v,p+sz._v, // Input string: s, send
                &ch,
                0,&n_cu,        // number of bytes parsed
                0,              // no need for number of code points parsed
                &e,             // error
                '\'',           // delimiter is single quote
                0,              // do not stop if incomplete
                1,              // maximum code points to go into &ch
                0,              // no CESU surrogate pairs allowed
                0,              // no C0 80 for zero
                2,              // C style enforcing valid code point
                1,              // with delimiters
                0               // do not zero-terminate
                );

    // Decoding error
    if (retval == -1)
    {
        endpos = startpos + static_cast<int64_t>(n_cu);
        return makeerr(detail::unicode_conversion_map_error(e.type));
    }
    // String length 0 means empty char: ''
    else if (retval == 0)
    {
        endpos = startpos + 1;
        return makeerr(err::EC_EMPTY_CHARACTER_LITERAL);
    }
    endpos = startpos + static_cast<int64_t>(n_cu);
    return static_cast<int64_t>(ch);
}



//
// Decode integer values from source
//


// Decode a number literal from source str or UTF-8 bytes
// On error, endpos marks the error position in s or b
inline
exp_u64
toolbox::exp_decode_u64_with_suffix(
    detail::strval const &s,    // [i] input string
    i64 startpos,               // [i] start position in s
    i64 &endpos,                // [o] end position or error position in s
    int &suffix                 /* [o] suffix:
                                       -1 failure
                                       0 none
                                       1 u
                                       2 i
                                       3 u8
                                       4 i8
                                       5 u16
                                       6 i16
                                       7 u32
                                       8 i32
                                       9 u64
                                       10 i64 */
    ) noexcept
{
    using namespace detail;
    std::size_t sz = s._get_size();
    
    if (startpos < 0)
        startpos = 0;
    if (startpos > static_cast<int64_t>(sz))
        startpos = static_cast<int64_t>(sz);
    std::size_t pos = static_cast<std::size_t>(startpos._v);

    uint64_t x;
    unsigned long n_cu;
    integer_conversion_error e;


    switch (s._get_chartype())
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            int retval;

            retval = decode_uint64_with_suffix(p + pos,
                                               sz - pos,
                                               &x,
                                               &n_cu,
                                               &e);
            endpos = startpos + static_cast<int64_t>(n_cu);
            if (retval == -1)
            {
                suffix = -1;
                return makeerr(integer_conversion_map_error(e.type));
            }
            suffix = retval;
            return x;
        }
        break;

    case str::_chartype_ucs2:
        if (true)
        {
            str::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            int retval;

            retval = decode_uint64_with_suffix(p + pos,
                                               sz - pos,
                                               &x,
                                               &n_cu,
                                               &e);
            endpos = startpos + static_cast<int64_t>(n_cu);
            if (retval == -1)
            {
                suffix = -1;
                return makeerr(integer_conversion_map_error(e.type));
            }
            suffix = retval;
            return x;
        }
        break;

    default:
        if (true)
        {
            str::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            int retval;

            retval = decode_uint64_with_suffix(p + pos,
                                               sz - pos,
                                               &x,
                                               &n_cu,
                                               &e);
            endpos = startpos + static_cast<int64_t>(n_cu);
            if (retval == -1)
            {
                suffix = -1;
                return makeerr(integer_conversion_map_error(e.type));
            }
            suffix = retval;
            return x;
        }
        break;
    }

    /* This should never happen */
    return 0;
}


inline
exp_u64
toolbox::exp_decode_u64_with_suffix(
    detail::bytesval const &b,  // [i] input bytes
    i64 startpos,               // [i] start position in b
    i64 &endpos,                // [o] end position or error position in b
    int &suffix                 /* [o] suffix:
                                       -1 failure
                                       0 none
                                       1 u
                                       2 i
                                       3 u8
                                       4 i8
                                       5 u16
                                       6 i16
                                       7 u32
                                       8 i32
                                       9 u64
                                       10 i64 */
    ) noexcept
{
    using namespace detail;
    std::size_t sz = b._get_size();
    
    if (startpos < 0)
        startpos = 0;
    if (startpos > static_cast<int64_t>(sz))
        startpos = static_cast<int64_t>(sz);
    std::size_t pos = static_cast<std::size_t>(startpos._v);

    uint64_t x;
    unsigned long n_cu;
    integer_conversion_error e;


    bytes::byte_t const * CIG_RESTRICT p = b._data();
    int retval;

    retval = decode_uint64_with_suffix(p + pos,
                                       sz - pos,
                                       &x,
                                       &n_cu,
                                       &e);
    endpos = startpos + static_cast<int64_t>(n_cu);
    if (retval == -1)
    {
        suffix = -1;
        return makeerr(integer_conversion_map_error(e.type));
    }
    suffix = retval;
    return x;
}


ast::exp_literal
toolbox::exp_decode_int_literal(detail::strval const &s,
                                i64 startpos,i64 &endpos) noexcept
{
    int suffix;
    auto exp = exp_decode_u64_with_suffix(s,startpos,endpos,suffix);
    if (!exp)
        return forwarderr(exp);
    u64 v = exp.value();
    switch (suffix)
    {
    case 1:                     // u
    case 9:                     // u64
        return ast::literal(v);
    case 0:                     // none
    case 2:                     // i
    case 10:                    // i64
        if (v._v > i64::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i64(static_cast<int64_t>(v._v)));
    case 3:                     // u8
        if (v._v > u8::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(u8(static_cast<uint8_t>(v._v)));
    case 4:                     // i8
        if (v._v > i8::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i8(static_cast<int8_t>(v._v)));
    case 5:                     // u16
        if (v._v > u16::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(u16(static_cast<uint16_t>(v._v)));
    case 6:                     // i16
        if (v._v > i16::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i16(static_cast<int16_t>(v._v)));
    case 7:                     // u32
        if (v._v > u32::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(u32(static_cast<uint32_t>(v._v)));
    case 8:                     // i32
        if (v._v > i32::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i32(static_cast<int32_t>(v._v)));
    }
    return makeerr(err::EC_UNRECOGNIZED_NUMBER_SUFFIX);
}

ast::exp_literal
toolbox::exp_decode_int_literal(detail::bytesval const &b,
                                i64 startpos,i64 &endpos) noexcept
{
    int suffix;
    auto exp = exp_decode_u64_with_suffix(b,startpos,endpos,suffix);
    if (!exp)
        return forwarderr(exp);
    u64 v = exp.value();
    switch (suffix)
    {
    case 1:                     // u
    case 9:                     // u64
        return ast::literal(v);
    case 0:                     // none
    case 2:                     // i
    case 10:                    // i64
        if (v._v > i64::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i64(static_cast<int64_t>(v._v)));
    case 3:                     // u8
        if (v._v > u8::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(u8(static_cast<uint8_t>(v._v)));
    case 4:                     // i8
        if (v._v > i8::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i8(static_cast<int8_t>(v._v)));
    case 5:                     // u16
        if (v._v > u16::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(u16(static_cast<uint16_t>(v._v)));
    case 6:                     // i16
        if (v._v > i16::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i16(static_cast<int16_t>(v._v)));
    case 7:                     // u32
        if (v._v > u32::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(u32(static_cast<uint32_t>(v._v)));
    case 8:                     // i32
        if (v._v > i32::max_value)
        {
            endpos = startpos;
            return makeerr(err::EC_NUMBER_IS_TOO_BIG);
        }
        return ast::literal(i32(static_cast<int32_t>(v._v)));
    }
    return makeerr(err::EC_UNRECOGNIZED_NUMBER_SUFFIX);
}


//
// Encode strings, bytes into quoted source
//

inline
exp_str
toolbox::exp_encode_str_to_str(
    detail::strval const &s,    // [i] input string
    // Behavior configuration
    int quote_controls,         // [i] 0 not 1 JSON-style \u 2 C-style \ooo
    int quote_escapes,          // [i] whether to quote " and \ and ...
    int quote_nonascii,         // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long delimiter,             // [i] if nonnegative enclose in this delimiter
    int zero_terminate          // [i] whether to append a trailing '\0'
    )
{
    using namespace detail;
    i64 sz = s.size();
    unsigned long szu = static_cast<unsigned long>(sz._v);
    str::_chartype chartype = s._get_chartype();

    switch (chartype)
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_encode_utf32_count<
                    detail::unicode_conversion_encoding::utf32>(
                        p,              // s
                        szu,            // send
                        &e,             // error
                        -1,quote_controls,quote_escapes,quote_nonascii,
                        delimiter,zero_terminate);
            if (retval == -1)
                return makeerr(detail::unicode_conversion_map_error(e.type));
            std::size_t len = static_cast<unsigned long>(retval);
            str o;

            o._reserve(len,chartype);
            unicode_conversion_encode_utf32<
                unicode_conversion_encoding::utf32>(
                    p,              // s
                    szu,            // slen
                    o._data_ucs4(), // o
                    &e,             // error, discarded
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
            o._set_size(len);
            return exp_str(std::move(o));
        }
        break;

    case str::_chartype_ucs2:
        if (true)
        {
            str::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_encode_utf32_count<
                    detail::unicode_conversion_encoding::utf32>(
                        p,              // s
                        szu,            // slen
                        &e,             // error
                        -1,quote_controls,quote_escapes,quote_nonascii,
                        delimiter,zero_terminate);
            if (retval == -1)
                return makeerr(detail::unicode_conversion_map_error(e.type));
            std::size_t len = static_cast<unsigned long>(retval);
            str o;

            o._reserve(len,chartype);
            unicode_conversion_encode_utf32<
                unicode_conversion_encoding::utf32>(
                    p,              // s
                    szu,            // slen
                    o._data_ucs2(), // o
                    &e,             // error, discarded
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
            o._set_size(len);
            return exp_str(std::move(o));
        }
        break;

    default:
        if (true)
        {
            str::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_encode_utf32_count<
                    detail::unicode_conversion_encoding::utf32>(
                        p,              // s
                        szu,            // slen
                        &e,             // error
                        -1,quote_controls,quote_escapes,quote_nonascii,
                        delimiter,zero_terminate);
            if (retval == -1)
                return makeerr(detail::unicode_conversion_map_error(e.type));
            std::size_t len = static_cast<unsigned long>(retval);
            str o;

            o._reserve(len,chartype); // ascii or ucs1
            unicode_conversion_encode_utf32<
                unicode_conversion_encoding::utf32>(
                    p,              // s
                    szu,            // slen
                    o._data_ucs1(), // o
                    &e,             // error, discarded
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
            o._set_size(len);
            return exp_str(std::move(o));
        }
        break;
    }

    /* This should never happen */
    return str();
}


inline
exp_bytes
toolbox::exp_encode_str_to_bytes(
    detail::strval const &s,    // [i] input string
    // Behavior configuration
    int quote_controls,         // [i] 0 not 1 JSON-style \u 2 C-style \ooo
    int quote_escapes,          // [i] whether to quote " and \ and ...
    int quote_nonascii,         // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
    long delimiter,             // [i] if nonnegative enclose in this delimiter
    int zero_terminate          // [i] whether to append a trailing '\0'
    )
{
    using namespace detail;
    i64 sz = s.size();
    unsigned long szu = static_cast<unsigned long>(sz._v);

    switch (s._get_chartype())
    {
    case str::_chartype_ucs4:
        if (true)
        {
            str::ucs4_t const * CIG_RESTRICT p = s._data_ucs4();
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_encode_utf32_count<
                    detail::unicode_conversion_encoding::utf8>(
                        p,              // s
                        szu,            // slen
                        &e,             // error
                        -1,quote_controls,quote_escapes,quote_nonascii,
                        delimiter,zero_terminate);
            if (retval == -1)
                return makeerr(detail::unicode_conversion_map_error(e.type));
            std::size_t len = static_cast<unsigned long>(retval);
            bytes o;

            o._reserve(len);
            unicode_conversion_encode_utf32<
                unicode_conversion_encoding::utf8>(
                    p,              // s
                    szu,            // slen
                    o._data(),      // o
                    &e,             // error, discarded
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
            o._set_size(len);
            return exp_bytes(std::move(o));
        }
        break;

    case str::_chartype_ucs2:
        if (true)
        {
            str::ucs2_t const * CIG_RESTRICT p = s._data_ucs2();
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_encode_utf32_count<
                    detail::unicode_conversion_encoding::utf8>(
                        p,              // s
                        szu,            // slen
                        &e,             // error
                        -1,quote_controls,quote_escapes,quote_nonascii,
                        delimiter,zero_terminate);
            if (retval == -1)
                return makeerr(detail::unicode_conversion_map_error(e.type));
            std::size_t len = static_cast<unsigned long>(retval);
            bytes o;

            o._reserve(len);
            unicode_conversion_encode_utf32<
                unicode_conversion_encoding::utf8>(
                    p,              // s
                    szu,            // slen
                    o._data(),      // o
                    &e,             // error, discarded
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
            o._set_size(len);
            return exp_bytes(std::move(o));
        }
        break;

    default:
        if (true)
        {
            str::ucs1_t const * CIG_RESTRICT p = s._data_ucs1();
            unicode_conversion_error e;
            std::ptrdiff_t retval =
                unicode_conversion_encode_utf32_count<
                    detail::unicode_conversion_encoding::utf8>(
                        p,              // s
                        szu,            // slen
                        &e,             // error
                        -1,quote_controls,quote_escapes,quote_nonascii,
                        delimiter,zero_terminate);
            if (retval == -1)
                return makeerr(detail::unicode_conversion_map_error(e.type));
            std::size_t len = static_cast<unsigned long>(retval);
            bytes o;

            o._reserve(len);
            unicode_conversion_encode_utf32<
                unicode_conversion_encoding::utf8>(
                    p,              // s
                    szu,            // slen
                    o._data(),      // o
                    &e,             // error, discarded
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
            o._set_size(len);
            return exp_bytes(std::move(o));
        }
        break;
    }

    /* This should never happen */
    return bytes();
}


inline
exp_bytes
toolbox::exp_encode_bytes_to_bytes(
    detail::bytesval const &b,  // [i] input bytes
    // Behavior configuration
    int quote_controls,         // [i] 0 not 1 JSON-style \u 2 C-style \ooo
    int quote_escapes,          // [i] whether to quote " and \ and ...
    int quote_nonascii,         // [i] 0 keep 1 \U 2 \u 3 \oct 4 latin1 to utf8 
    long delimiter,             // [i] if nonnegative enclose in this delimiter
    int zero_terminate          // [i] whether to append a trailing '\0'
    )
{
    using namespace detail;
    i64 sz = b.size();
    unsigned long szu = static_cast<unsigned long>(sz._v);

    bytes::byte_t const * CIG_RESTRICT p = b._data();
    unicode_conversion_error e;

    if (quote_nonascii == 4)    // ISO-8859-1 to UTF-8
    {
        std::ptrdiff_t retval =
            unicode_conversion_encode_utf8_count<
                detail::unicode_conversion_encoding::utf32>(
                    p,              // s
                    szu,            // slen
                    &e,             // error
                    -1,0,0,         // no surrogate encoding
                    quote_controls,quote_escapes,0, // encode to UTF-8
                    delimiter,zero_terminate);
        if (retval == -1)
        {
            return makeerr(detail::unicode_conversion_map_error(e.type));
        }

        std::size_t len = static_cast<unsigned long>(retval);
        bytes o;

        o._reserve(len);
        unicode_conversion_encode_utf8<
            unicode_conversion_encoding::utf32>(
                p,              // s
                szu,            // slen
                o._data(),      // o
                &e,             // error, discarded
                -1,0,0,         // no surrogate encoding
                quote_controls,quote_escapes,0, // encode to UTF-8
                delimiter,zero_terminate);
        o._set_size(len);
        return exp_bytes(std::move(o));
    }
    else                        // Keep nonascii bytes or quote them
    {
        std::ptrdiff_t retval =
            unicode_conversion_encode_utf32_count<
                detail::unicode_conversion_encoding::utf32>(
                    p,              // s
                    szu,            // slen
                    &e,             // error
                    -1,quote_controls,quote_escapes,quote_nonascii,
                    delimiter,zero_terminate);
        if (retval == -1)
        {
            return makeerr(detail::unicode_conversion_map_error(e.type));
        }

        std::size_t len = static_cast<unsigned long>(retval);
        bytes o;

        o._reserve(len);
        unicode_conversion_encode_utf32<
            unicode_conversion_encoding::utf32>(
                p,              // s
                szu,            // slen
                o._data(),      // o
                &e,             // error, discarded
                -1,quote_controls,quote_escapes,quote_nonascii,
                delimiter,zero_terminate);
        o._set_size(len);
        return exp_bytes(std::move(o));
    }
}


// Encode a str with JSON syntax into str or UTF-8 bytes
exp_str
toolbox::exp_encode_json_str(detail::strval const &s,
                             cig_bool ensure_ascii) noexcept
{
    return exp_encode_str_to_str(
        s,
        1,                      // quote_controls JSON-style \u
        1,                      // quote_escapes
        ensure_ascii ? 3 : 0,   // quote_nonascii JSON-style \u with surrogates
        '"',                    // delimiter
        0);             // do not zero-terminate
}

exp_bytes
toolbox::exp_encode_json_str_to_bytes(detail::strval const &s,
                                      cig_bool ensure_ascii) noexcept
{
    return exp_encode_str_to_bytes(
        s,
        1,                      // quote_controls JSON-style \u
        1,                      // quote_escapes
        ensure_ascii ? 3 : 0,   // quote_nonascii JSON-style \u with surrogates
        '"',                    // delimiter
        0);             // do not zero-terminate
}


// Encode a str with C syntax into str or UTF-8 bytes
// Use with prefix 'u' in CIG source code.
exp_str
toolbox::exp_encode_C_str(detail::strval const &s,
                          cig_bool ensure_ascii) noexcept
{
    return exp_encode_str_to_str(
        s,
        2,                      // quote_controls C-style
        1,                      // quote_escapes
        ensure_ascii ? 2 : 0,   // quote_nonascii C-style \u or \U
        '"',                    // delimiter
        0);             // do not zero-terminate
}

exp_bytes
toolbox::exp_encode_C_str_to_bytes(detail::strval const &s,
                                   cig_bool ensure_ascii) noexcept
{
    return exp_encode_str_to_bytes(
        s,
        2,                      // quote_controls C-style
        1,                      // quote_escapes
        ensure_ascii ? 2 : 0,   // quote_nonascii 
        '"',                    // delimiter
        0);             // do not zero-terminate
}


// Encode a bytes with C syntax into str or UTF-8 bytes
// Use with prefix 'b' in CIG source code.
exp_str
toolbox::exp_encode_C_bytes(detail::bytesval const &b) noexcept
{
    auto expbquoted = exp_encode_bytes_to_bytes(
        b,
        2,                      // quote_controls C-style \ooo
        1,                      // quote_escapes
        3,                      // quote_nonascii C-style \ooo
        '"',                    // delimiter
        0);                     // do not zero-terminate
    if (!expbquoted)
        return std::unexpected(std::move(expbquoted.error()));
    bytes bquoted = std::move(expbquoted.value());
    return bquoted.decode_utf8();
}

exp_bytes
toolbox::exp_encode_C_bytes_to_bytes(detail::bytesval const &b,
                                     cig_bool ensure_ascii) noexcept
{
    auto expbquoted = exp_encode_bytes_to_bytes(
        b,
        2,                      // quote_controls C-style
        1,                      // quote_escapes
        ensure_ascii ? 3 : 0,   // quote_nonascii 
        '"',                    // delimiter
        0);                     // do not zero-terminate
    return expbquoted;
}


// Encode a bytes with strict UTF-8 content into str or UTF-8 bytes
// Use with prefix 'u8' in CIG source code.
exp_bool
toolbox::exp_verify_u8_bytes(detail::bytesval const &b) noexcept
{
    detail::bytesval::byte_t const * CIG_RESTRICT p = b._data();
    std::size_t sz = b._get_size();
    detail::unicode_conversion_error e;

    if (!unicode_conversion_verify_unicode<
        detail::unicode_conversion_encoding::utf8>(p,p+sz,&e))
        return makeerr(detail::unicode_conversion_map_error(e.type));
    return cig_true;
}


exp_str
toolbox::exp_encode_u8_bytes(detail::bytesval const &b) noexcept
{
    auto expbquoted = exp_encode_bytes_to_bytes(
        b,
        2,                      // quote_controls C-style \ooo
        1,                      // quote_escapes
        0,                      // do not quote_nonascii
        '"',                    // delimiter
        0);                     // do not zero-terminate
    if (!expbquoted)
        return std::unexpected(std::move(expbquoted.error()));
    bytes bquoted = std::move(expbquoted.value());
    auto expisutf = exp_verify_u8_bytes(bquoted);
    if (!expisutf)
        return forwarderr(expisutf);
    return bquoted.decode_utf8();
}

exp_bytes
toolbox::exp_encode_u8_bytes_to_bytes(detail::bytesval const &b) noexcept
{
    auto expbquoted = exp_encode_bytes_to_bytes(
        b,
        2,                      // quote_controls C-style
        1,                      // quote_escapes
        0,                      // do not quote nonascii
        '"',                    // delimiter
        0);                     // do not zero-terminate
    return expbquoted;
    bytes bquoted = std::move(expbquoted.value());
    auto expisutf = exp_verify_u8_bytes(bquoted);
    if (!expisutf)
        return forwarderr(expisutf);
    return bquoted;
}


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
