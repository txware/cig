/* cig_file_descriptor.h
 *
 * Copyright 2023 Claus Fischer
 *
 * File descriptor class with destructor.
 */
#ifndef CIG_FILE_DESCRIPTOR_H_INCLUDED
#define CIG_FILE_DESCRIPTOR_H_INCLUDED

#include <utility>              // std::move

#include "cig_datatypes.h"      // str,err
#include "cig_int_inline.h"     // i64 operations
#include "cig_exception.h"      // Exception

namespace cig {

namespace file {

class descriptor;

using exp_descriptor = std::expected<descriptor,err>;

class descriptor {
    static constexpr int invalid = -1;
    int filedes;                // Unix file descriptor or Windows HANDLE

    // This function leaves 'uninitialized memory'
    void _destruct() noexcept;  // failing to close will abort from cig_assert

public:

    // Rule of five
    // Destructor
    ~descriptor() noexcept { _destruct(); }
    // Constructor
    constexpr descriptor() noexcept : filedes(invalid) { }
    // Move constructor
    constexpr descriptor(descriptor && other) noexcept;
    // Move assignment
    constexpr descriptor & operator = (descriptor && other) noexcept;
    // No dupliction or copying
    descriptor(const descriptor &) = delete;
    descriptor & operator = (const descriptor &) = delete;

    // Sneak in and out
    explicit constexpr descriptor(int fd) noexcept : filedes(fd) {}
    int steal() noexcept { int fd = filedes; filedes = invalid; return fd; }


    // Basic operations
    static exp_descriptor exp_open(str const &filepath,bool read_only) noexcept;
    exp_none exp_close() noexcept;
    exp_none exp_syncdata() const noexcept;
    static exp_none exp_syncdata_name(str const &filepath) noexcept;
    exp_i64 exp_get_size() const noexcept;
    exp_i64 exp_get_position() const noexcept;
    exp_none exp_set_position(i64) const noexcept;
    exp_none exp_truncate() const noexcept;
    err sysdep_read(void *data,i64 length) const noexcept;
    err sysdep_write(void const *data,i64 length) const noexcept;
    exp_none exp_read(bytes &buffer,
                      std::optional<i64> size = std::nullopt,
                      std::optional<i64> start = std::nullopt,
                      bool allow_resize = true) const noexcept;
    exp_none exp_write(bytes const &buffer,
                       std::optional<i64> size = std::nullopt,
                       std::optional<i64> start = std::nullopt) const noexcept;

#ifdef CIG_HAVE_EXCEPTIONS
    static descriptor open(str const &filepath,bool read_only) noexcept(false);
    descriptor(str const &filepath,bool read_only) noexcept(false);
    void close() noexcept(false);
    void syncdata() const noexcept(false);
    static void syncdata_name(str const &filepath) noexcept(false);
    i64 get_size() const noexcept(false);
    i64 get_position() const noexcept(false);
    void set_position(i64) const noexcept(false);
    void truncate() const noexcept(false);
    void read(bytes &buffer,
              std::optional<i64> size = std::nullopt,
              std::optional<i64> start = std::nullopt,
              bool allow_resize = true) const noexcept(false);
    void write(bytes const &buffer,
               std::optional<i64> size = std::nullopt,
               std::optional<i64> start = std::nullopt) const noexcept(false);
#endif  // CIG_HAVE_EXCEPTIONS
};


// Move constructor
constexpr descriptor::descriptor(descriptor && other) noexcept :
    filedes(std::move(other.filedes))
{
    other.filedes = invalid;
}


// Move assignment
constexpr descriptor & descriptor::operator = (descriptor && other) noexcept
{
    if (this != &other)
    {
        _destruct();
        filedes = other.filedes;
        other.filedes = invalid;
    }
    return *this;
}


inline exp_none
descriptor::exp_read(bytes &buffer,
                     std::optional<i64> size,
                     std::optional<i64> start,
                     bool allow_resize) const noexcept
{
    i64 off = start.value_or(0);
    i64 bufsz = buffer.size();
    i64 sz = size.value_or(bufsz - off); // cannot use capacity
    if (off < 0 || off > bufsz /* avoid leaving holes */ || sz < 0)
        return makeerr(err::E_EINVAL);
    if (off + sz > bufsz)
    {
        if (!allow_resize)
            return makeerr(err::E_ENOBUFS);
        buffer.reserve(off + sz);
    }
    err e = sysdep_read(buffer._data()+off._v,sz);
    // since the read may have overwritten the terminating '\0' byte,
    // we need to set_size() even on failure
    if (e.is_error())
    {
        buffer.set_size(bufsz);
        return makeerr(e.get_error_code());
    }
    if (off + sz > bufsz)   // Not if all read was within bufsz
        buffer.set_size(off+sz,cig_false);
    return exp_none();      // Or omit the return statement
}


inline exp_none
descriptor::exp_write(bytes const &buffer,
                      std::optional<i64> size,
                      std::optional<i64> start) const noexcept
{
    i64 off = start.value_or(0);
    i64 bufsz = buffer.size();
    i64 sz = size.value_or(bufsz - off);
    if (off < 0 || off > bufsz || sz < 0)
        return makeerr(err::E_EINVAL);
    if (off + sz >bufsz)
        return makeerr(err::E_ENOBUFS);
    err e = sysdep_write(buffer._data()+off._v,sz);
    if (e.is_error())
        return makeerr(e.get_error_code());
    return exp_none();          // Or omit the return statement
}


#ifdef CIG_HAVE_EXCEPTIONS


inline
descriptor                      // valid descriptor or exception
descriptor::open(str const &filepath,bool read_only)
{
    auto expectedfd = exp_open(filepath,read_only);
    Exception::check(expectedfd);
    return std::move(std::move(expectedfd).value());
}


// with exception
inline descriptor::descriptor(str const &filepath,bool read_only)
{
    descriptor aux = open(filepath,read_only); // may 'throw'
    filedes = aux.steal();
}


// with exception
inline void descriptor::close()
{
    auto exp = exp_close();
    Exception::check(exp);
}


// with exception
inline void descriptor::syncdata() const
{
    auto exp = exp_syncdata();
    Exception::check(exp);
}


// with exception
inline void descriptor::syncdata_name(str const &filepath)
{
    auto exp = exp_syncdata_name(filepath);
    Exception::check(exp);
}


// with exception
inline i64 descriptor::get_size() const
{
    auto exp = exp_get_size();
    Exception::check(exp);
    return exp.value();
}


// with exception
inline i64 descriptor::get_position() const
{
    auto exp = exp_get_position();
    Exception::check(exp);
    return exp.value();
}


// with exception
inline void descriptor::set_position(i64 pos) const
{
    auto exp = exp_set_position(pos);
    Exception::check(exp);
}


// with exception
inline void descriptor::truncate() const
{
    auto exp = exp_truncate();
    Exception::check(exp);
}


// with exception
inline void
descriptor::read(bytes &buffer,
                 std::optional<i64> size,
                 std::optional<i64> start,
                 bool allow_resize) const
{
    auto exp = exp_read(buffer,size,start,allow_resize);
    Exception::check(exp);
}


// with exception
inline void
descriptor::write(bytes const &buffer,
                  std::optional<i64> size,
                  std::optional<i64> start) const
{
    auto exp = exp_write(buffer,size,start);
    Exception::check(exp);
}


#endif  // CIG_HAVE_EXCEPTIONS


} // namespace file


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_FILE_DESCRIPTOR_H_INCLUDED) */
