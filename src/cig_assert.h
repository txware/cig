/* cig_assert.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Implementation of assert for cig.
 */
#ifndef CIG_ASSERT_H_INCLUDED
#define CIG_ASSERT_H_INCLUDED

namespace cig {

/* Stringification of argument */
#define CIG_ASSERT_STRING(x) #x

#define cig_assert(expr)                        \
  ((void)(static_cast<bool>(expr)  ||           \
   (cig_assert_fail(CIG_ASSERT_STRING(expr),    \
                    __FILE__,                   \
                    __LINE__,                   \
                    __func__),0)))

[[noreturn]] void cig_assert_fail(char const *assertion,char const *file,
                                  int line,char const *function);

//
// Auxiliary quasi static assert
//
constexpr void cig_static_assert([[ maybe_unused ]] bool condition,
                                 [[ maybe_unused ]] char const *message)
{
#if defined(__EXCEPTIONS) || defined(_HAS_EXCEPTIONS)
    // This form is chosen since a dynamic assert cannot be used
    // in constexpr context and a static_assert will not work.
    if (!condition)
        throw message;          // 'throw' never throws, a static assert
#endif
}



} /* namespace cig */


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_ASSERT_H_INCLUDED) */
