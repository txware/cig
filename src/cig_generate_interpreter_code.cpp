/* cig_generate_interpreter_code.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Code generator for interpreter code
 */
#include <cstdio>               // FILE,fopen,...
#include <cerrno>               // errno
#include <cstring>              // strerror
#include <cstdlib>              // malloc
#include <cctype>               // isspace,tolower
#include <fmt/format.h>
#include <fmt/os.h>
#include <bit>
#include <string_view>
#include <vector>

#include "cig_compatibility.h"

#include "cig_interpreter_ops_and_types.h"

using namespace cig::interpreter;


// Output files
#define CIG_INTERPRETER_CODE_H        "cig_interpreter_code.h"
#define CIG_INTERPRETER_CODE_CPP      "cig_interpreter_code.cpp"


static
char const *
oparg_cppname(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return "cig_bool";
    case OPARG_U8: return "u8";
    case OPARG_U16: return "u16";
    case OPARG_U32: return "u32";
    case OPARG_U64: return "u64";
    case OPARG_I8: return "i8";
    case OPARG_I16: return "i16";
    case OPARG_I32: return "i32";
    case OPARG_I64: return "i64";
    case OPARG_STR: return "str";
    case OPARG_BYTES: return "bytes";
    case OPARG_STREXPR: return "strexpr";
    case OPARG_BYTESEXPR: return "bytesexpr";
    case OPARG_COMPOUND: return "compound";
    case OPARG_VOID: return "void";
    case OPARG_LIST: return "list";
    }
    return "";
}

static
char const *
oparg_cppnamequal(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return "cig_bool";
    case OPARG_U8: return "u8";
    case OPARG_U16: return "u16";
    case OPARG_U32: return "u32";
    case OPARG_U64: return "u64";
    case OPARG_I8: return "i8";
    case OPARG_I16: return "i16";
    case OPARG_I32: return "i32";
    case OPARG_I64: return "i64";
    case OPARG_STR: return "str";
    case OPARG_BYTES: return "bytes";
    case OPARG_STREXPR: return "detail::strexpr";
    case OPARG_BYTESEXPR: return "detail::bytesexpr";
    case OPARG_COMPOUND: return "compound";
    case OPARG_VOID: return "void";
    case OPARG_LIST: return "list";
    }
    return "";
}


#define NAME_LEN 300




// The opcode table is built while creating the cpp file
// and is used in several places in the cpp and h file.

#define N_OPCODE_INFO 10000

struct opcode_info {
    char symbol[NAME_LEN];
    char opshort[NAME_LEN];
    opstem stem = OPSTEM_NOOP;
    int nargs = 0;
    oparg a1;
    oparg a2;
    char arg1[NAME_LEN];
    char arg2[NAME_LEN];
    char oplong[3*NAME_LEN+5];
    char oplong_lower[3*NAME_LEN+5];
};

static opcode_info opinfo[N_OPCODE_INFO];

static int nopinfo = 0;


#define ADD_OPCODE_INFO(xsymbol,xopstem,xnargs,xarg1,xarg2)             \
    do {                                                                \
        char const *aoi_sy = xsymbol;                                   \
        char const *aoi_op = opstem_name(xopstem);                      \
        char const *aoi_a1 = (xarg1 == OPARG_COMPOUND ?                 \
                              "" : oparg_name(xarg1));                  \
        char const *aoi_a2 = (xarg2 == OPARG_COMPOUND ?                 \
                              "" : oparg_name(xarg2));                  \
        if (nopinfo >= N_OPCODE_INFO)                                   \
        {                                                               \
            fmt::print(stderr,"{0}: error: too many opinfo entries, "   \
                       "please increase N_OPCODE_INFO\n",               \
                       __FILE__);                                       \
            return 1;                                                   \
        }                                                               \
        snprintf(opinfo[nopinfo].symbol,                                \
                 sizeof(opinfo[nopinfo].symbol),                        \
                 "%s",aoi_sy);                                          \
        snprintf(opinfo[nopinfo].opshort,                               \
                 sizeof(opinfo[nopinfo].opshort),                       \
                 "%s",aoi_op);                                          \
        opinfo[nopinfo].stem = xopstem;                                 \
        opinfo[nopinfo].nargs = xnargs;                                 \
        opinfo[nopinfo].a1 = xarg1;                                     \
        opinfo[nopinfo].a2 = xarg2;                                     \
        snprintf(opinfo[nopinfo].arg1,                                  \
                 sizeof(opinfo[nopinfo].arg1),                          \
                 "%s",aoi_a1);                                          \
        snprintf(opinfo[nopinfo].arg2,                                  \
                 sizeof(opinfo[nopinfo].arg2),                          \
                 "%s",aoi_a2);                                          \
        if (aoi_a2 && aoi_a2[0])                                        \
            snprintf(opinfo[nopinfo].oplong,                            \
                     sizeof(opinfo[nopinfo].oplong),                    \
                     "%s_%s_%s",aoi_op,aoi_a1,aoi_a2);                  \
        else if (aoi_a1 && aoi_a1[0])                                   \
            snprintf(opinfo[nopinfo].oplong,                            \
                     sizeof(opinfo[nopinfo].oplong),                    \
                     "%s_%s",aoi_op,aoi_a1);                            \
        else                                                            \
            snprintf(opinfo[nopinfo].oplong,                            \
                     sizeof(opinfo[nopinfo].oplong),                    \
                     "%s",aoi_op);                                      \
        for (int pos = 0; opinfo[nopinfo].oplong[pos]; pos++)           \
        {                                                               \
            opinfo[nopinfo].oplong_lower[pos] =                         \
                static_cast<char>(                                      \
                    std::tolower(opinfo[nopinfo].oplong[pos]));         \
            opinfo[nopinfo].oplong_lower[pos+1] = '\0';                 \
        }                                                               \
        i = nopinfo;                                                    \
        nopinfo++;                                                      \
    } while (0)


#define ADD0(xstem) ADD_OPCODE_INFO("",xstem,0,OPARG_COMPOUND,OPARG_COMPOUND)
#define ADD1(xstem,arg) ADD_OPCODE_INFO("",xstem,1,arg,OPARG_COMPOUND)
#define ADD2(xstem,arg1,arg2) ADD_OPCODE_INFO("",xstem,2,arg1,arg2)
#define ADD1S(symbol,xstem,arg) ADD_OPCODE_INFO(symbol,xstem,1,arg,OPARG_COMPOUND)
#define ADD2S(symbol,xstem,arg1,arg2) ADD_OPCODE_INFO(symbol,xstem,2,arg1,arg2)



// Arrays to fill and dump into generated cpp file
char const *
opstem_to_unary_opcode[OPSTEM_END][OPARG_COMPOUND] = {};
char const *
opstem_to_binary_opcode[OPSTEM_END][OPARG_COMPOUND][OPARG_COMPOUND] = {};




static
int                             // 0 on success, 1 on failure
generate_interpreter_code_cpp(char const *programname)
{
    auto out = fmt::output_file(CIG_INTERPRETER_CODE_CPP);
    int i;                      // Index of last opinfo, set by macros
    

    /* Header */
    out.print("/* cig_interpreter_code.cpp\n");
    out.print(" *\n");
    out.print(" * Autogenerated by cig_generate_interpreter_code.\n");
    out.print(" *\n");
    out.print(" * Generated by {} version {}\n",
              PROJECT_NAME, PROJECT_VERSION);
    out.print(" */\n");
    out.print("#include \"cig_interpreter.h\"\n");
    out.print("#include \"cig_interpreter_code.h\"\n");
    out.print("#include \"cig_str_proxy.h\"\n");
    out.print("#include \"cig_constexpr_itoa.h\"\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");


    out.print("namespace cig {{\n");
    out.print("\n");
    out.print("namespace interpreter {{\n");
    out.print("\n");



    // First generate the interpreter function
    // and add the opinfo entries

    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("void run_function(unsigned char * CIG_RESTRICT stackmem)\n");
    out.print("{{\n");
    out.print("    using enum cig::interpreter::unwind_flags;\n");
    out.print("\n");
    out.print("#define fnr (*fnruntime::from_stackmem(stackmem))\n");
    out.print("#define fnc (*fnr.fn)\n");
    out.print("#define fnrcalled (*fnr.called)\n");
    out.print("#define stackmem_called (fnr.called->to_stackmem())\n");
    out.print("#define globalconst (fnr.globalconst)\n");
    out.print("#define globalvar (fnr.globalvar)\n");
    out.print("    std::size_t n = static_cast<std::size_t>(fnc.n_instr._v);\n");
    out.print("    const instruction * CIG_RESTRICT const instr = fnc.instr;\n");
    out.print("    std::size_t ip = 0;         // instruction pointer\n");
    out.print("\n");
    out.print("    for (;;) {{\n");
    out.print("        cig_assert(ip < n);\n");
    out.print("        opcode op = instr[ip].op;\n");
    out.print("\n");
    out.print("        switch (op)\n");
    out.print("        {{\n");



    out.print("\n");
    out.print("        // No-Op\n");
    ADD0(OPSTEM_NOOP);
    out.print("        case OP_{}: break;\n",
              opinfo[i].oplong);

    out.print("\n");
    out.print("        // Trap: This code should never be reached\n");
    ADD0(OPSTEM_TRAP);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            str trapmsg = \"Interpreter trapped in function \"_str +\n");
    out.print("                          fnc.fqname +\n");
    out.print("                          \" at ip \"_str + u64(ip).to_str();\n");
    out.print("            \n");
    out.print("            if (!instr[ip].comment.is_empty())\n");
    out.print("                trapmsg += \" \"_str + instr[ip].comment;\n");
    out.print("            \n");
    out.print("            auto trapmsg_utf8 = cig::detail::utf8proxy(trapmsg);\n");
    out.print("            cig_assert_fail(trapmsg_utf8.c_str(),__FILE__,__LINE__,__func__);\n");
    out.print("            \n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Label\n");
    ADD0(OPSTEM_LABEL);
    out.print("        case OP_{}: break;\n",
              opinfo[i].oplong);


    // Jumps


    out.print("\n");
    out.print("        // Jump\n");
    ADD0(OPSTEM_JMP);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("            continue;\n");

    out.print("\n");
    out.print("        // Jump if true\n");
    ADD0(OPSTEM_JMPTRUE);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
              "cig_bool");
    out.print("                            stackmem + instr[ip].arg1._v);\n");
    out.print("            if (*arg1)\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg2._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Jump if false\n");
    ADD0(OPSTEM_JMPFALSE);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
              "cig_bool");
    out.print("                            stackmem + instr[ip].arg1._v);\n");
    out.print("            if (!*arg1)\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg2._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");


    // Jump if value is zero
    out.print("\n");
    out.print("        // Jump if builtin type is zero\n");
    for (oparg arg = static_cast<oparg>(0);
         arg < OPARG_COMPOUND;
         arg = static_cast<oparg>(arg + 1))
    {
        // No simple zero test for str and bytes
        if (arg == OPARG_BOOL ||
            arg == OPARG_STR || arg == OPARG_STREXPR ||
            arg == OPARG_BYTES || arg == OPARG_BYTESEXPR)
            continue;

        ADD1(OPSTEM_JMPZERO,arg);
        out.print("        case OP_{}: // jump if {} is zero\n",
                  opinfo[i].oplong,oparg_cigname(arg));
        out.print("        {{\n");
        out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
                  oparg_cppnamequal(arg));
        out.print("                            stackmem + instr[ip].arg1._v);\n");
        out.print("            if (*arg1 == 0)\n");
        out.print("            {{\n");
        out.print("                ip = static_cast<std::size_t>(instr[ip].arg2._v);\n");
        out.print("                continue;\n");
        out.print("            }}\n");
        out.print("            break;\n");
        out.print("        }}\n");
    }

    // Jump if value is nonzero
    out.print("\n");
    out.print("        // Jump if builtin type is nonzero\n");
    for (oparg arg = static_cast<oparg>(0);
         arg < OPARG_COMPOUND;
         arg = static_cast<oparg>(arg + 1))
    {
        // No simple zero test for str and bytes
        if (arg == OPARG_BOOL ||
            arg == OPARG_STR || arg == OPARG_STREXPR ||
            arg == OPARG_BYTES || arg == OPARG_BYTESEXPR)
            continue;

        ADD1(OPSTEM_JMPNONZERO,arg);
        out.print("        case OP_{}: // jump if {} is not zero\n",
                  opinfo[i].oplong,oparg_cigname(arg));
        out.print("        {{\n");
        out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
                  oparg_cppnamequal(arg));
        out.print("                            "
                  "stackmem + instr[ip].arg1._v);\n");
        out.print("            if (*arg1 != 0)\n");
        out.print("            {{\n");
        out.print("                ip = static_cast<std::size_t>(instr[ip].arg2._v);\n");
        out.print("                continue;\n");
        out.print("            }}\n");
        out.print("            break;\n");
        out.print("        }}\n");
    }



    // Jumps on stack unwinder flags


    out.print("\n");
    out.print("        // Jump if the stack unwinder 'break' flag is set\n");
    ADD0(OPSTEM_JMPBREAK);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            if (fnr.uwflags &\n");
    out.print("                static_cast<uint64_t>(UW_BREAK))\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Jump if the stack unwinder 'continue' flag is set\n");
    ADD0(OPSTEM_JMPCONTINUE);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            if (fnr.uwflags &\n");
    out.print("                static_cast<uint64_t>(UW_CONTINUE))\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Jump if the stack unwinder 'return' flag is set\n");
    ADD0(OPSTEM_JMPRETURN);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            if (fnr.uwflags &\n");
    out.print("                static_cast<uint64_t>(UW_RETURN))\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Jump if the stack unwinder 'exception' flag is set\n");
    ADD0(OPSTEM_JMPEXCEPT);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            if (fnr.uwflags &\n");
    out.print("                static_cast<uint64_t>(UW_EXCEPTION))\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Jump if any stack unwinder flag is set\n");
    ADD0(OPSTEM_JMPANYUWFLAG);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            if (fnr.uwflags)\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Jump if no stack unwinder flag is set\n");
    ADD0(OPSTEM_JMPNOUWFLAG);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("        {{\n");
    out.print("            if (!fnr.uwflags)\n");
    out.print("            {{\n");
    out.print("                ip = static_cast<std::size_t>(instr[ip].arg1._v);\n");
    out.print("                continue;\n");
    out.print("            }}\n");
    out.print("            break;\n");
    out.print("        }}\n");

    out.print("\n");
    out.print("        // Clear the stack unwinder flags\n");
    ADD0(OPSTEM_CLEARUW);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags = 0;\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Set the stack unwinder 'break' flag\n");
    ADD0(OPSTEM_SETBREAK);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_BREAK);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Set the stack unwinder 'continue' flag\n");
    ADD0(OPSTEM_SETCONTINUE);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_CONTINUE);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Set the stack unwinder 'return' flag\n");
    ADD0(OPSTEM_SETRETURN);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_RETURN);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Set the stack unwinder 'exception' flag\n");
    ADD0(OPSTEM_SETEXCEPT);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_EXCEPTION);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Clear the stack unwinder 'break' flag\n");
    ADD0(OPSTEM_CLEARBREAK);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_BREAK);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Clear the stack unwinder 'continue' flag\n");
    ADD0(OPSTEM_CLEARCONTINUE);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_CONTINUE);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Clear the stack unwinder 'return' flag\n");
    ADD0(OPSTEM_CLEARRETURN);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_RETURN);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Clear the stack unwinder 'exception' flag\n");
    ADD0(OPSTEM_CLEAREXCEPT);
    out.print("        case OP_{}:\n",
              opinfo[i].oplong);
    out.print("            fnr.uwflags |= static_cast<uint64_t>(UW_EXCEPTION);\n");
    out.print("            break;\n");

    out.print("\n");
    out.print("        // Destructors\n");
    for (oparg arg = static_cast<oparg>(0);
         arg < OPARG_COMPOUND;
         arg = static_cast<oparg>(arg + 1))
    {
        ADD1(OPSTEM_DESTRUCT,arg);
        out.print("        case OP_{}: // destruct {}\n",
                  opinfo[i].oplong,oparg_cigname(arg));
        out.print("            reinterpret_cast<{}*>(\n",
                  oparg_cppnamequal(arg));
        out.print("                "
                  "stackmem + instr[ip].arg1._v)->~{}();\n",
                  oparg_cppname(arg));
        out.print("            break;\n");
    }




    out.print("\n");
    out.print("        // Default constructors\n");
    for (oparg arg = static_cast<oparg>(0);
         arg < OPARG_COMPOUND;
         arg = static_cast<oparg>(arg + 1))
    {
        // No default constructor for strexpr,bytesexpr
        if (arg == OPARG_STREXPR || arg == OPARG_BYTESEXPR)
            continue;

        ADD1(OPSTEM_CONSTRUCT,arg);
        out.print("        case OP_{}: // construct {}\n",
                  opinfo[i].oplong,oparg_cigname(arg));
        out.print("            new"
                  "(stackmem + instr[ip].arg1._v) {}();\n",
                  oparg_cppname(arg));
        out.print("            break;\n");
    }



    out.print("\n");
    out.print("        // Creating (copying from globalconst) literals\n");
    for (oparg arg = static_cast<oparg>(0);
         arg < OPARG_COMPOUND;
         arg = static_cast<oparg>(arg + 1))
    {
        // No literals for strexpr,bytesexpr
        if (arg == OPARG_STREXPR || arg == OPARG_BYTESEXPR)
            continue;

        ADD1(OPSTEM_COPYLIT,arg);
        out.print("        case OP_{}: // create literal {}\n",
                  opinfo[i].oplong,oparg_cigname(arg));
        out.print("        {{\n");
        out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
                  oparg_cppnamequal(arg));
        out.print("                            "
                  "globalconst + instr[ip].arg1._v);\n");
        out.print("            auto arg2 = reinterpret_cast<{}*>(\n",
                  oparg_cppnamequal(arg));
        out.print("                            "
                  "stackmem + instr[ip].arg2._v);\n");

        // Trivial copy
        if (oparg_is_trivial(arg))
            out.print("            *arg2 = *arg1;\n");

        // Special cases of non-trivial copy
        else if (arg == OPARG_STR || arg == OPARG_BYTES)
        {
            char const *mytype = (arg == OPARG_STR ? "str" : "bytes");

            // Copy-Construct
            out.print("            new"
                      "(arg2) {}(*arg1);\n",
                      mytype);
        }

        // Unhandled copy
        else
        {
            fmt::print(stderr,
                       "{0}: error: unhandled literal copy of {1}\n",
                       programname,oparg_cigname(arg));
            return 1;
        }

        out.print("            break;\n");
        out.print("        }}\n");
    }



    // Copy and move operations for local memory
    out.print(
        "\n"
        "\n"
        "\n"
        "\n"
        "// Copy and move operations for local memory\n"
        "//\n"
        "// There are six operations\n"
        "// for each combination of source and destination type.\n"
        "//\n"
        "// The keyword COPY indicates that the source object remains\n"
        "// a valid object and its contents is fully duplicated.\n"
        "// The keyword MOVE indicates that its source is emptied\n"
        "// as in a typical C++ move scenario; however, it is left as\n"
        "// a valid empty C++ object and its destructor may be called\n"
        "// afterwards (and should have no effect).\n"
        "// The kewyrd TRSF (transfer) indicates that the raw memory\n"
        "// is copied to the destination. After this operation, the\n"
        "// source must not be treated as an object, in particular it\n"
        "// is illegal to call its destructor afterwards.\n"
        "//\n"
        "// The interpreter code generator must carefully pick which\n"
        "// of these keywords it uses.\n"
        "// \n"
        "// All operations will work correctly if source and destination\n"
        "// are the same place in memory.\n"
        "//\n"
        "// Operation  "
        "Description/C++ equivalent\n"
        "// ---------  "
        "-------------------------------------------------------------\n"
        "// COPYMEM    "
        "copy an object onto raw memory (copy constructor)\n"
        "// COPYOBJ    "
        "copy an object onto an object (copy assignment)\n"
        "// MOVEMEM    "
        "move an object onto raw mem, leave deleted (move constructor)\n"
        "// MOVEOBJ    "
        "move an object onto an object, leave deleted (move assignment)\n"
        "// TRSFMEM    "
        "transfer an object onto raw memory (pure memcpy)\n"
        "// TRSFOBJ    "
        "transfer an object onto an object (destructor then memcopy)\n"
        "\n"
        "\n");

    out.print("\n");
    out.print("        // Copy and move operations\n");
    for (oparg arg1 = static_cast<oparg>(0);
         arg1 < OPARG_COMPOUND;
         arg1 = static_cast<oparg>(arg1 + 1))
    {
        for (oparg arg2 = static_cast<oparg>(0);
             arg2 < OPARG_COMPOUND;
             arg2 = static_cast<oparg>(arg2 + 1))
        {
            if (arg1 == OPARG_BOOL && arg2 == OPARG_BOOL)
                ;               // Accept bool to bool
            else if (oparg_to_class(arg1) == OPARGCL_INTEGRAL &&
                     oparg_to_class(arg2) == OPARGCL_INTEGRAL)
                ;               // Accept int to int
            else if (arg1 == OPARG_STR && arg2 == OPARG_STR)
                ;               // Accept str to str
            else if (arg1 == OPARG_BYTES && arg2 == OPARG_BYTES)
                ;               // Accept bytes to bytes
            else
                continue;

            for (int imv = 0; imv < 6; imv++)
            {
                opstem stem =
                    (imv == 0 ? OPSTEM_COPYMEM :
                     imv == 1 ? OPSTEM_COPYOBJ :
                     imv == 2 ? OPSTEM_MOVEMEM :
                     imv == 3 ? OPSTEM_MOVEOBJ :
                     imv == 4 ? OPSTEM_TRSFMEM :
                     OPSTEM_TRSFOBJ);
                
                int cpmv = imv / 2;
                int dstisobj = imv % 2;

                ADD2(stem,arg1,arg2);

                out.print("        case OP_{}: // {} {} to {}{}\n",
                          opinfo[i].oplong,
                          cpmv == 0 ? "copy" :
                          cpmv == 1 ? "move" :
                          "transfer",
                          oparg_cigname(arg1),
                          oparg_cigname(arg2),
                          dstisobj ? " (already an object)" :
                          " (and construct it)");
                out.print("        {{\n");
                out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
                          oparg_cppnamequal(arg1));
                out.print("                            "
                          "stackmem + instr[ip].arg1._v);\n");
                out.print("            auto arg2 = reinterpret_cast<{}*>(\n",
                          oparg_cppnamequal(arg2));
                out.print("                            "
                          "stackmem + instr[ip].arg2._v);\n");

                // Trivial copy between trivial types
                if (oparg_is_trivial(arg1) && oparg_is_trivial(arg2))
                {
                    if (arg1 == arg2)
                        out.print("            *arg2 = *arg1;\n");
                    else
                        out.print("            *arg2 = {}(*arg1);\n",
                                  oparg_cppname(arg2));
                    out.print("            break;\n");
                    out.print("        }}\n");
                    continue;
                }
                else if (oparg_is_trivial(arg1) || oparg_is_trivial(arg2))
                {
                    // Cannot copy/move/transfer non-trival and trivial type
                    fmt::print(stderr,
                               "{0}: error: cannot copy {1} to {2}\n",
                               programname,
                               oparg_cppname(arg1),oparg_cppname(arg2));
                    return 1;
                }
                
                // Special cases of non-trivial copy
                if ((arg1 == OPARG_STR && arg2 == OPARG_STR) ||
                    (arg1 == OPARG_BYTES && arg2 == OPARG_BYTES))
                {
                    char const *mytype = (arg1 == OPARG_STR ? "str" : "bytes");

                    // Construct
                    if (!dstisobj)
                    {
                        if (cpmv == 0) // copy-construct
                        {
                            out.print("            new"
                                      "(arg2) {}(*arg1);\n",
                                      mytype);
                        }
                        else if (cpmv == 1) // move-construct
                        {
                            out.print("            new"
                                      "(arg2) {}(std::move(*arg1));\n",
                                      mytype);
                        }
                        else    // transfer; old location won't be touched
                        {
                            out.print("            std::memcpy("
                                      "reinterpret_cast<void*>(arg2),\n"
                                      "                        "
                                      "reinterpret_cast<void*>(arg1),"
                                      "sizeof({}));\n",
                                      mytype);
                            out.print("            // code generator ensures"
                                      " *arg1 not touched after here\n");
                        }
                    }

                    // Assign (replace object)
                    else
                    {
                        if (cpmv == 0) // assignment
                        {
                            out.print("            *arg2 = *arg1;\n");
                        }
                        else    // move-assignment for move,trsf
                        {
                            out.print("            "
                                      "*arg2 = std::move(*arg1);\n");
                        }
                    }
                }
                else
                {
                    // Unhandled copy
                    fmt::print(stderr,
                               "{0}: error: unhandled copy {1} to {2}\n",
                               programname,
                               oparg_cppname(arg1),oparg_cppname(arg2));
                    return 1;
                }

                out.print("            break;\n");
                out.print("        }}\n");
            }
        }
    }



    // Move operations for function calls
    out.print("\n");
    out.print("        // Move operations for function calls\n");
    for (oparg arg1 = static_cast<oparg>(0);
         arg1 < OPARG_COMPOUND;
         arg1 = static_cast<oparg>(arg1 + 1))
    {
        for (oparg arg2 = static_cast<oparg>(0);
             arg2 < OPARG_COMPOUND;
             arg2 = static_cast<oparg>(arg2 + 1))
        {
            if (arg1 == OPARG_BOOL && arg2 == OPARG_BOOL)
                ;               // Accept bool to bool
            else if (oparg_to_class(arg1) == OPARGCL_INTEGRAL &&
                     oparg_to_class(arg2) == OPARGCL_INTEGRAL)
                ;               // Accept int to int
            else if (arg1 == OPARG_STR && arg2 == OPARG_STR)
                ;               // Accept str to str
            else if (arg1 == OPARG_BYTES && arg2 == OPARG_BYTES)
                ;               // Accept bytes to bytes
            else
                continue;

            for (int imv = 0; imv < 2; imv++)
            {
                opstem stem =
                    (imv ? OPSTEM_MOVEFROMFN : OPSTEM_MOVETOFN);
                
                ADD2(stem,arg1,arg2);

                out.print("        case OP_{}: // move {}{} to {}{}\n",
                          opinfo[i].oplong,
                          imv ? "funret:" : "",
                          oparg_cigname(arg1),
                          imv ? "" : "funpar:",
                          oparg_cigname(arg2));
                out.print("        {{\n");
                out.print("            auto arg1 = reinterpret_cast<{}*>(\n"
                          "                            "
                          "stackmem{} + instr[ip].arg1._v);\n",
                          oparg_cppnamequal(arg1),
                          imv ? "_called" : "");
                out.print("            auto arg2 = reinterpret_cast<{}*>(\n"
                          "                            "
                          "stackmem{} + instr[ip].arg2._v);\n",
                          oparg_cppnamequal(arg2),
                          imv ? "" : "_called");

                // Trivial move between trivial types
                if (oparg_is_trivial(arg1) && oparg_is_trivial(arg2))
                {
                    if (arg1 == arg2)
                        out.print("            *arg2 = *arg1;\n");
                    else
                        out.print("            *arg2 = {}(*arg1);\n",
                                  oparg_cppname(arg2));
                    out.print("            break;\n");
                    out.print("        }}\n");
                    continue;
                }
                else if (oparg_is_trivial(arg1) || oparg_is_trivial(arg2))
                {
                    // Cannot move non-trival and trivial type
                    fmt::print(stderr,
                               "{0}: error: cannot move {1} to {2}\n",
                               programname,
                               oparg_cppname(arg1),oparg_cppname(arg2));
                    return 1;
                }
                
                // Special cases of non-trivial move
                if ((arg1 == OPARG_STR && arg2 == OPARG_STR) ||
                    (arg1 == OPARG_BYTES && arg2 == OPARG_BYTES))
                {
                    char const *mytype = (arg1 == OPARG_STR ? "str" : "bytes");

                    out.print("            new"
                              "(arg2) {}(std::move(*arg1));\n",
                              mytype);
                }
                else
                {
                    // Unhandled move
                    fmt::print(stderr,
                               "{0}: error: unhandled move {1} to {2}\n",
                               programname,
                               oparg_cppname(arg1),oparg_cppname(arg2));
                    return 1;
                }

                out.print("            break;\n");
                out.print("        }}\n");
            }
        }
    }



    // Unary expression operations
    out.print(
        "\n"
        "\n"
        "\n"
        "\n"
        "// Unary expression operations\n"
        "//\n"
        "// Unary operations use the argument located in arg1\n"
        "// to create a result located in res.\n"
        "// The data type of res is determined by the operation and by the\n"
        "// data type of the operand, which are encoded in op.\n"
        "\n"
        "\n");

    out.print("\n");
    out.print("        // Unary expression operations\n");
    for (opstem stem = OPSTEM_NOOP;
         stem < OPSTEM_END;
         stem = static_cast<opstem>(stem + 1))
    {
        if (opstem_nargs(stem) != 1)
            continue;

        for (oparg arg1 = static_cast<oparg>(0);
             arg1 < OPARG_COMPOUND;
             arg1 = static_cast<oparg>(arg1 + 1))
        {
            oparg res = oparg_unary_result_type(stem,arg1);
            if (res == OPARG_COMPOUND)
                continue;

            ADD1(stem,arg1);

            out.print("        case OP_{}: // {} {}\n",
                      opinfo[i].oplong,
                      opstem_symbol(stem),
                      oparg_cigname(arg1));
            out.print("        {{\n");
            out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
                      oparg_cppnamequal(arg1));
            out.print("                            "
                      "stackmem + instr[ip].arg1._v);\n");
            out.print("            auto res = reinterpret_cast<{}*>(\n",
                      oparg_cppnamequal(res));
            out.print("                            "
                      "stackmem + instr[ip].res._v);\n");

            out.print("            static_assert(std::is_same<\n");
            out.print("                std::remove_reference<decltype(*res)>::type,\n");
            out.print("                std::remove_reference<decltype({} *arg1)>::type>\n",
                      opstem_symbol(stem));
            out.print("                ::value == true);\n");

            // Special cases
            if (0)
                ;
            else if (stem == OPSTEM_SIGNPLUS && // plus sign for string
                     (arg1 == OPARG_STR || arg1 == OPARG_STREXPR))
            {
                char const *a1 = (arg1 == OPARG_STR ? "str" : "expr");

                out.print("            new"
                          "(res) {}(cig::detail::strexpr::mult_{},\n"
                          "               "
                          "      {} *arg1,1);\n",
                          "detail::strexpr",
                          a1,
                          "               ");
            }
            else if (stem == OPSTEM_SIGNPLUS && // plus sign for byte sequence
                     (arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR))
            {
                char const *a1 = (arg1 == OPARG_BYTES ? "bytes" : "expr");

                out.print("            new"
                          "(res) {}(cig::detail::bytesexpr::mult_{},\n"
                          "               "
                          "      {} *arg1,1);\n",
                          "detail::bytesexpr",
                          a1,
                          "                 ");
            }

            // General case
            else
                out.print("            *res = {} *arg1;\n",
                          opstem_symbol(stem));

            out.print("            break;\n");
            out.print("        }}\n");
        }
    }



    // Binary expression operations
    out.print(
        "\n"
        "\n"
        "\n"
        "\n"
        "// Binary expression operations\n"
        "//\n"
        "// Binary operations combine the arguments located in arg1 and arg2\n"
        "// to create a result located in res.\n"
        "// The data type of res is determined by the operation and by the\n"
        "// data types of the operands, which are encoded in op.\n"
        "\n"
        "\n");

    out.print("\n");
    out.print("        // Binary expression operations\n");
    for (opstem stem = OPSTEM_NOOP;
         stem < OPSTEM_END;
         stem = static_cast<opstem>(stem + 1))
    {
        if (opstem_nargs(stem) != 2)
            continue;

        for (oparg arg1 = static_cast<oparg>(0);
             arg1 < OPARG_COMPOUND;
             arg1 = static_cast<oparg>(arg1 + 1))
        {
            for (oparg arg2 = static_cast<oparg>(0);
                 arg2 < OPARG_COMPOUND;
                 arg2 = static_cast<oparg>(arg2 + 1))
            {
                oparg res = oparg_binary_result_type(stem,arg1,arg2);

                if (res == OPARG_COMPOUND)
                    continue;

                ADD2(stem,arg1,arg2);

                out.print("        case OP_{}: // {} {} {}\n",
                          opinfo[i].oplong,
                          oparg_cigname(arg1),
                          opstem_symbol(stem),
                          oparg_cigname(arg2));
                out.print("        {{\n");
                out.print("            auto arg1 = reinterpret_cast<{}*>(\n",
                          oparg_cppnamequal(arg1));
                out.print("                            "
                          "stackmem + instr[ip].arg1._v);\n");
                out.print("            auto arg2 = reinterpret_cast<{}*>(\n",
                          oparg_cppnamequal(arg2));
                out.print("                            "
                          "stackmem + instr[ip].arg2._v);\n");
                
                if (!opstem_assigns(stem))
                {
                    out.print("            auto res = reinterpret_cast<{}*>(\n",
                              oparg_cppnamequal(res));
                    out.print("                            "
                              "stackmem + instr[ip].res._v);\n");
                }

                // Since the overloaded && and || operators in C++
                // will not short-circuit like the built-in operators
                // do, we do not overload in C++ at all.
                // Consequently, we cannot check the result here.
                // In fact, the interpreter's code generator does
                // not use the opcodes generated here but implements
                // short-circuiting.

                if (!opstem_assigns(stem) &&
                    stem != OPSTEM_LOGICAL_AND &&
                    stem != OPSTEM_LOGICAL_OR)
                {
                    out.print("            static_assert(std::is_same<\n");
                    out.print("                std::remove_reference<decltype(*res)>::type,\n");
                    out.print("                std::remove_reference<decltype(*arg1 {} *arg2)>::type>\n",
                              opstem_symbol(stem));
                    out.print("                ::value == true);\n");
                }

                // Special cases
                if (0)
                    ;
                else if (stem == OPSTEM_ADD && // add two strings
                         (arg1 == OPARG_STR || arg1 == OPARG_STREXPR) &&
                         (arg2 == OPARG_STR || arg2 == OPARG_STREXPR))
                {
                    char const *a1 = (arg1 == OPARG_STR ? "str" : "expr");
                    char const *a2 = (arg2 == OPARG_STR ? "str" : "expr");

                    out.print("            new"
                              "(res) {}(cig::detail::strexpr::plus_{}_{},\n"
                              "               "
                              "      {} *arg1,*arg2);\n",
                              "detail::strexpr",
                              a1,a2,
                              "               ");
                    
                }
                else if (stem == OPSTEM_MULT && // multiply string with number
                         ((arg1 == OPARG_STR || arg1 == OPARG_STREXPR) ||
                          (arg2 == OPARG_STR || arg2 == OPARG_STREXPR)))
                {
                    if (arg1 == OPARG_STR || arg1 == OPARG_STREXPR)
                    {
                        char const *a1 = (arg1 == OPARG_STR ? "str" : "expr");
                        
                        out.print("            new"
                                  "(res) {}(cig::detail::strexpr::mult_{},\n"
                                  "               "
                                  "      {} *arg1,{});\n",
                                  "detail::strexpr",
                                  a1,
                                  "               ",
                                  arg2 == OPARG_I64 ? "*arg2" : "i64(*arg2)");
                    }
                    else
                    {
                        char const *a2 = (arg2 == OPARG_STR ? "str" : "expr");
                        
                        out.print("            new"
                                  "(res) {}(cig::detail::strexpr::mult_{},\n"
                                  "               "
                                  "      {} *arg2,{});\n",
                                  "detail::strexpr",
                                  a2,
                                  "               ",
                                  arg1 == OPARG_I64 ? "*arg1" : "i64(*arg1)");
                    }
                }

                else if (stem == OPSTEM_ADD && // add two byte sequences
                         (arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR) &&
                         (arg2 == OPARG_BYTES || arg2 == OPARG_BYTESEXPR))
                {
                    char const *a1 = (arg1 == OPARG_BYTES ? "bytes" : "expr");
                    char const *a2 = (arg2 == OPARG_BYTES ? "bytes" : "expr");

                    out.print("            new"
                              "(res) {}(cig::detail::bytesexpr::plus_{}_{},\n"
                              "               "
                              "      {} *arg1,*arg2);\n",
                              "detail::bytesexpr",
                              a1,a2,
                              "                 ");
                    
                }
                else if (stem == OPSTEM_MULT && // multiply byte squ with number
                         ((arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR) ||
                          (arg2 == OPARG_BYTES || arg2 == OPARG_BYTESEXPR)))
                {
                    if (arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR)
                    {
                        char const *a1 = (arg1 == OPARG_BYTES ?
                                          "bytes" : "expr");
                        
                        out.print("            new"
                                  "(res) {}(cig::detail::bytesexpr::mult_{},\n"
                                  "               "
                                  "      {} *arg1,{});\n",
                                  "detail::bytesexpr",
                                  a1,
                                  "                 ",
                                  arg2 == OPARG_I64 ? "*arg2" : "i64(*arg2)");
                    }
                    else
                    {
                        char const *a2 = (arg2 == OPARG_BYTES ?
                                          "bytes" : "expr");
                        
                        out.print("            new"
                                  "(res) {}(cig::detail::bytesexpr::mult_{},\n"
                                  "               "
                                  "      {} *arg2,{});\n",
                                  "detail::bytesexpr",
                                  a2,
                                  "                 ",
                                  arg1 == OPARG_I64 ? "*arg1" : "i64(*arg1)");
                    }
                }

                // General case
                else if (opstem_assigns(stem))
                    out.print("            *arg1 {} *arg2;\n",
                              opstem_symbol(stem));
                else
                    out.print("            *res = *arg1 {} *arg2;\n",
                              opstem_symbol(stem));
                out.print("            break;\n");
                out.print("        }}\n");
            }
        }
    }




    out.print("\n");
    out.print("        // End instruction\n");
    ADD0(OPSTEM_END);
    out.print("        case OP_{}: return;\n",
              opinfo[i].oplong);

    out.print("        }}\n");
    out.print("        ip++;\n");
    out.print("    }}\n");
    out.print("}}\n");
    out.print("\n");
    out.print("\n");




    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("str opcode_name(opcode op)\n");
    out.print("{{\n");
    out.print("    switch (op)\n");
    out.print("    {{\n");
    for (i = 0; i < nopinfo; i++)
    {
        out.print("    case OP_{}: return \"{}\"_str;\n",
                  opinfo[i].oplong,opinfo[i].oplong);
    }
    out.print("    }}\n");
    out.print("    return \"\"_str;\n");
    out.print("}}\n");
    out.print("\n");




    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("str opcode_name_lower(opcode op)\n"); 
    out.print("{{\n");
    out.print("    switch (op)\n");
    out.print("    {{\n");
    for (i = 0; i < nopinfo; i++)
    {
        out.print("    case OP_{}: return \"{}\"_str;\n",
                  opinfo[i].oplong,opinfo[i].oplong_lower);
    }
    out.print("    }}\n");
    out.print("    return \"\"_str;\n");
    out.print("}}\n");
    out.print("\n");


    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("opstem opcode_to_opstem(opcode op)\n");
    out.print("{{\n");
    out.print("    switch (op)\n");
    out.print("    {{\n");
    for (i = 0; i < nopinfo; i++)
    {
        out.print("    case OP_{}: return OPSTEM_{};\n",
                  opinfo[i].oplong,opstem_name(opinfo[i].stem));
    }
    out.print("    }}\n");
    out.print("    return OPSTEM_NOOP;\n");
    out.print("}}\n");
    out.print("\n");


    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("oparg opcode_to_oparg1(opcode op)\n");
    out.print("{{\n");
    out.print("    switch (op)\n");
    out.print("    {{\n");
    for (i = 0; i < nopinfo; i++)
    {
        if (opinfo[i].nargs == 1 ||
            opinfo[i].nargs == 2)
            out.print("    case OP_{}: return OPARG_{};\n",
                      opinfo[i].oplong,oparg_name(opinfo[i].a1));
        else
            out.print("    case OP_{}: return OPARG_COMPOUND;\n",
                      opinfo[i].oplong);
    }
    out.print("    }}\n");
    out.print("    return OPARG_COMPOUND;\n");
    out.print("}}\n");
    out.print("\n");




    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("oparg opcode_to_oparg2(opcode op)\n");
    out.print("{{\n");
    out.print("    switch (op)\n");
    out.print("    {{\n");
    for (i = 0; i < nopinfo; i++)
    {
        if (opinfo[i].nargs == 2)
            out.print("    case OP_{}: return OPARG_{};\n",
                      opinfo[i].oplong,oparg_name(opinfo[i].a2));
        else
            out.print("    case OP_{}: return OPARG_COMPOUND;\n",
                      opinfo[i].oplong);
    }
    out.print("    }}\n");
    out.print("    return OPARG_COMPOUND;\n");
    out.print("}}\n");
    out.print("\n");




    // Write the opstem-to-opcode functions.

    // Fill the arrays
    for (i = 0; i < nopinfo; i++)
    {
        if (opinfo[i].nargs == 1)
        {
            opstem_to_unary_opcode[opinfo[i].stem][opinfo[i].a1] =
                opinfo[i].oplong;
        }
        if (opinfo[i].nargs == 2)
        {
            opstem_to_binary_opcode
                [opinfo[i].stem][opinfo[i].a1][opinfo[i].a2] =
                opinfo[i].oplong;
        }
    }


    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("opcode // unary opcode or OP_NOOP\n");
    out.print("opstem_to_unary_opcode_table[OPSTEM_END]"
              "[OPARG_COMPOUND] = {{\n");

    for (int stem = 0; stem < OPSTEM_END; stem++)
    {
        out.print("{{ // {}\n",opstem_name(static_cast<opstem>(stem)));
        for (int a1 = 0; a1 < OPARG_COMPOUND; a1++)
        {
            out.print("  OP_{}{}\n",
                      opstem_to_unary_opcode[stem][a1] ?
                      opstem_to_unary_opcode[stem][a1] :
                      "NOOP",
                      a1 + 1 == OPARG_COMPOUND ? "" : ",");
        }
        out.print(" }}{}\n",
                  stem + 1 == OPSTEM_END ? "" : ",");
    }

    out.print("}};\n");
    out.print("\n");
    out.print("opcode // binary opcode or OP_NOOP\n");
    out.print("opstem_to_binary_opcode_table[OPSTEM_END]"
              "[OPARG_COMPOUND][OPARG_COMPOUND] = {{\n");

    for (int stem = 0; stem < OPSTEM_END; stem++)
    {
        out.print("{{ // {}\n",opstem_name(static_cast<opstem>(stem)));
        for (int a1 = 0; a1 < OPARG_COMPOUND; a1++)
        {
            out.print(" {{\n");
            for (int a2 = 0; a2 < OPARG_COMPOUND; a2++)
            {
                out.print("  OP_{}{}\n",
                          opstem_to_binary_opcode[stem][a1][a2] ?
                          opstem_to_binary_opcode[stem][a1][a2] :
                          "NOOP",
                          a2 + 1 == OPARG_COMPOUND ? "" : ",");
            }
            out.print(" }}{}\n",
                      a1 + 1 == OPARG_COMPOUND ? "" : ",");
        }
        out.print(" }}{}\n",
                  stem + 1 == OPSTEM_END ? "" : ",");
    }

    out.print("}};\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");




    out.print("opcode // Opcode or noop\n");
    out.print("opstem_to_unary_opcode(opstem stem,oparg arg)\n");
    out.print("{{\n");
    out.print("    if (stem < 0 || stem >= OPSTEM_END)\n");
    out.print("	return OP_NOOP;\n");
    out.print("    if (arg < 0 || arg >= OPARG_COMPOUND)\n");
    out.print("	return OP_NOOP;\n");
    out.print("    return opstem_to_unary_opcode_table[stem][arg];\n");
    out.print("}}\n");
    out.print("\n");
    out.print("\n");
    out.print("opcode // Opcode or noop\n");
    out.print("opstem_to_binary_opcode(opstem stem,oparg arg1,oparg arg2)\n");
    out.print("{{\n");
    out.print("    if (stem < 0 || stem >= OPSTEM_END)\n");
    out.print("	return OP_NOOP;\n");
    out.print("    if (arg1 < 0 || arg1 >= OPARG_COMPOUND)\n");
    out.print("	return OP_NOOP;\n");
    out.print("    if (arg2 < 0 || arg2 >= OPARG_COMPOUND)\n");
    out.print("	return OP_NOOP;\n");
    out.print("    return opstem_to_binary_opcode_table[stem][arg1][arg2];\n");
    out.print("}}\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");







    out.print("}} // namespace interpreter\n");
    out.print("\n");
    out.print("}} // namespace cig\n");
    out.print("\n");

    /* No footer */
    out.print("\n");
    out.print("\n");
    out.close();
    return 0;
}


static
void
generate_interpreter_code_h()
{
    auto out = fmt::output_file(CIG_INTERPRETER_CODE_H);
    int i;

    /* Header */
    out.print("/* cig_interpreter_code.h\n");
    out.print(" *\n");
    out.print(" * Autogenerated by cig_generate_interpreter_code.\n");
    out.print(" *\n");
    out.print(" * Generated by {} version {}\n",
              PROJECT_NAME, PROJECT_VERSION);
    out.print(" */\n");
    out.print("#ifndef CIG_INTERPRETER_CODE_H_INCLUDED\n");
    out.print("#define CIG_INTERPRETER_CODE_H_INCLUDED\n");
    out.print("\n");
    out.print("\n");
    out.print("\n");

    out.print("namespace cig {{\n");
    out.print("\n");
    out.print("namespace interpreter {{\n");
    out.print("\n");


    out.print("\n");
    out.print("\n");
    out.print("enum opcode : uint16_t {{\n");
    for (i = 0; i < nopinfo; i++)
    {
        out.print("    OP_{}",opinfo[i].oplong);

        if (i + 1 == nopinfo)
            out.print("\n");
        else
            out.print(",\n");
    }
    out.print("}};\n");
    out.print("\n");
    out.print("\n");




    out.print("\n");
    out.print("\n");
    out.print("// Prototypes\n");
    out.print("str opcode_name(opcode op);\n");
    out.print("str opcode_name_lower(opcode op);\n");
    out.print("\n");


    out.print("}} // namespace interpreter\n");
    out.print("\n");
    out.print("}} // namespace cig\n");
    out.print("\n");

    /* Footer */
    out.print("\n");
    out.print("\n");
    out.print("#endif /* !defined(CIG_INTERPRETER_CODE_H_INCLUDED) */\n");
    out.close();
}


int main([[maybe_unused]] int argc, char *argv[])
{
    //
    // Create the output files
    //


    // Note that the C++ file must be created before the header file

    /* Create the source file cig_interpreter_code.cpp */
    int retval = generate_interpreter_code_cpp(argv[0]);
    if (retval)
        return retval;

    /* Create the header file cig_interpreter_code.h */
    generate_interpreter_code_h();

    return 0;
}

/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
