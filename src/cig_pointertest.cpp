/* cig_pointertest.cpp
 *
 * Copyright 2024 Claus Fischer
 *
 * Test program for CIG parser.
 */

#include "cig_startup_main.h"
#include "cig_str_proxy.h"
#include "cig_fmt_formatter.h"


struct C {
    cig::str c;
};

struct D {
    cig::str c;
    cig::str d;
};


#if 0
/* See the following website for an Allocator example:
https://stackoverflow.com/questions/22487267/unable-to-use-custom-allocator-with-allocate-shared-make-shared */
#endif


template<typename T,
         cig::allocation::detail::allocator_rawstring A="type_all">
class type_allocator
{
public:
    int type_index;

    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T value_type;

    template<typename U>
    struct rebind { typedef type_allocator<U,A> other; };

    type_allocator(int ti) noexcept { type_index = ti; }
    type_allocator([[maybe_unused]] const type_allocator& other) noexcept {
        type_index = other.type_index;
    }

    template<typename U>
    type_allocator([[maybe_unused]] const type_allocator<U,A>& other) noexcept {
        type_index = other.type_index;
    }

    template<typename U>
    type_allocator& operator = (const type_allocator<U,A>& other) noexcept {
        type_index = other.type_index;
        return *this;
    }
    type_allocator<T>& operator = (const type_allocator& other) noexcept {
        type_index = other.type_index;
        return *this;
    }
    ~type_allocator() noexcept {}


    [[nodiscard]] pointer
    allocate(size_type n,
             cig::source_location const &loc =
             std::source_location::current()) noexcept
    {
        fprintf(stderr,"allocating type %llu\n",
                static_cast<unsigned long long>(type_index));
        // if (type_index == 5)
        //     n += 100;
        return cig::baseallocator<A>::template alloc<T>(n,loc);
    }

    void
    deallocate(pointer ptr,size_type n,
               cig::source_location const &loc =
               std::source_location::current()) noexcept
    {
        fprintf(stderr,"deallocating type %llu\n",
                static_cast<unsigned long long>(type_index));
        // if (type_index == 5)
        //     n += 100;
        cig::baseallocator<A>::template free<T>(ptr,n,loc);
    }
};


#if 0                           // These might be needed later

template <typename T, typename U,
          cig::allocation::detail::allocator_rawstring A="type_all">
inline bool operator == (const type_allocator<T,A>& a, const type_allocator<U,A>& b)
{
    return b.type_index == b.type_index;
}

template <typename T, typename U,
          cig::allocation::detail::allocator_rawstring A="type_all">
inline bool operator != (const type_allocator<T,A>& a, const type_allocator<U,A>& b)
{
    return !(a == b);
}

#endif





int cig::cig_main([[maybe_unused]] int argc,
                  [[maybe_unused]] char *argv[])
{
    using allc = cig::allocator<C,"C">;
    using alld = cig::allocator<D,"D">;
    using pointerc = std::shared_ptr<C>;
    using pointerd = std::shared_ptr<D>;
    auto typeallc = type_allocator<C,"IntC">(5);
    auto typealld = type_allocator<D,"IntD">(5);

    str program_name = str::from_utf8(argv[0]).value();

    fmt::print(stderr,"Interpreter Object Pointer and Allocator Test:\n");

    // This shows up under "new", on IA64/GCC 56 bytes
    pointerc pclassicc = std::make_shared<C>();
    // This shows up under "new", on IA64/GCC 80 bytes
    pointerd pclassicd = std::make_shared<D>();

    // This shows up under "C", on IA64/GCC 40 bytes
    pointerc pc = std::allocate_shared<C>(allc());
    // This shows up under "D", on IA64/GCC 64 bytes
    pointerd pd = std::allocate_shared<D>(alld());

    // This shows up under "I", on IA64/GCC 48 bytes
    pointerc pic = std::allocate_shared<C>(typeallc);
    pointerd pid = std::allocate_shared<D>(typealld);

    // Check that an allocated string is really deleted
    pic->c = "abcdefghijklmnopqrstuvwxyz"_str * 5;

    return 0;
}



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
