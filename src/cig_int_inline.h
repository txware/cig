/* cig_int_inline.h
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Inline functions for integral types.
 */
#ifndef CIG_INT_INLINE_H_INCLUDED
#define CIG_INT_INLINE_H_INCLUDED

#include "cig_datatypes.h"
#include "cig_literals.h"


namespace cig {


/* CONSTRUCTORS IMPLEMENTING ARITHMETIC CONVERSIONS */

constexpr cig_bool::cig_bool(const cig_bool &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const  u8 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const u16 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const u32 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const u64 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const  i8 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const i16 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const i32 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};
constexpr cig_bool::cig_bool(const i64 &x) noexcept :
    _v{ static_cast<bool>(x._v) } {};

constexpr  u8:: u8(const cig_bool &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const u16 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const u32 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const u64 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const  i8 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const i16 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const i32 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};
constexpr  u8:: u8(const i64 &x) noexcept :
    _v{ static_cast<uint8_t>(x._v) } {};

constexpr u16::u16(const cig_bool &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const  u8 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const u32 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const u64 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const  i8 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const i16 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const i32 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};
constexpr u16::u16(const i64 &x) noexcept :
    _v{ static_cast<uint16_t>(x._v) } {};

constexpr u32::u32(const cig_bool &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const  u8 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const u16 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const u64 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const  i8 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const i16 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const i32 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};
constexpr u32::u32(const i64 &x) noexcept :
    _v{ static_cast<uint32_t>(x._v) } {};

constexpr u64::u64(const cig_bool &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const  u8 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const u16 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const u32 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const  i8 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const i16 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const i32 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};
constexpr u64::u64(const i64 &x) noexcept :
    _v{ static_cast<uint64_t>(x._v) } {};

constexpr  i8:: i8(const cig_bool &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const  u8 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const u16 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const u32 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const u64 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const i16 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const i32 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};
constexpr  i8:: i8(const i64 &x) noexcept :
    _v{ static_cast<int8_t>(x._v) } {};

constexpr i16::i16(const cig_bool &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const  u8 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const u16 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const u32 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const u64 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const  i8 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const i32 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};
constexpr i16::i16(const i64 &x) noexcept :
    _v{ static_cast<int16_t>(x._v) } {};

constexpr i32::i32(const cig_bool &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const  u8 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const u16 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const u32 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const u64 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const  i8 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const i16 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};
constexpr i32::i32(const i64 &x) noexcept :
    _v{ static_cast<int32_t>(x._v) } {};

constexpr i64::i64(const cig_bool &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const  u8 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const u16 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const u32 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const u64 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const  i8 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const i16 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};
constexpr i64::i64(const i32 &x) noexcept :
    _v{ static_cast<int64_t>(x._v) } {};


/* CONVERTING TO STRINGS AND BYTES */


constexpr str cig_bool::to_str() const {
    return _v ? str("true"_str) : str("false"_str);
}



/* UNARY AND BINARY OPERATORS */

#define CIG_BASIC_TYPE_UNARY_OPERATOR(cigtype,ctype,OP) \
[[nodiscard]] constexpr                                 \
cigtype operator OP (cigtype const &x) noexcept         \
{                                                       \
    return cigtype(OP x._v);                            \
}


#define CIG_ALL_BASIC_TYPES_UNARY_OPERATOR(OP)         \
   CIG_BASIC_TYPE_UNARY_OPERATOR( u8, uint8_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR(u16,uint16_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR(u32,uint32_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR(u64,uint64_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR( i8,  int8_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR(i16, int16_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR(i32, int32_t,OP)      \
   CIG_BASIC_TYPE_UNARY_OPERATOR(i64, int64_t,OP)

#define CIG_ALL_BASIC_TYPES_BOOL_UNARY_OPERATOR(OP)     \
   CIG_BASIC_TYPE_UNARY_OPERATOR(cig_bool, bool,OP)     \
   CIG_BASIC_TYPE_UNARY_OPERATOR(      u8, uint8_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(     u16,uint16_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(     u32,uint32_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(     u64,uint64_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(      i8,  int8_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(     i16, int16_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(     i32, int32_t,OP)  \
   CIG_BASIC_TYPE_UNARY_OPERATOR(     i64, int64_t,OP)

#define CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(cigtype,ctype,OP)      \
[[nodiscard]] constexpr                                                 \
cig_bool operator OP (cigtype const &x) noexcept                        \
{                                                                       \
    return OP x._v;                                                     \
}

#define CIG_ALL_BASIC_TYPES_BOOL_UNARY_OPERATOR_BOOLVALUED(OP)          \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(cig_bool, bool, OP)         \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(      u8, uint8_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(     u16,uint16_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(     u32,uint32_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(     u64,uint64_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(      i8,  int8_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(     i16, int16_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(     i32, int32_t,OP)       \
   CIG_BASIC_TYPE_UNARY_OPERATOR_BOOLVALUED(     i64, int64_t,OP)

#define CIG_BASIC_TYPE_BINARY_OPERATOR(cigtype,ctype,OP)                \
[[nodiscard]] constexpr                                                 \
cigtype operator OP (cigtype const &x, cigtype const &y) noexcept       \
{                                                                       \
    return cigtype(ctype(x._v OP y._v));                                \
}

/* Special case cig_bool/bool and &&, ||:
   Only combination of cig_bool and cig_bool yields cig_bool,
   combinations with pure book yield bool. */
#define CIG_BOOL_BOOL_BINARY_OPERATOR(OP)                               \
[[nodiscard]] constexpr                                                 \
cig_bool operator OP (cig_bool const &x, cig_bool const &y) noexcept    \
{                                                                       \
    return cig_bool(bool(x._v OP y._v));                                \
}                                                                       \
[[nodiscard]] constexpr                                                 \
bool operator OP (bool const &x, cig_bool const &y) noexcept            \
{                                                                       \
    return bool(x OP y._v);                                             \
}                                                                       \
[[nodiscard]] constexpr                                                 \
bool operator OP (cig_bool const &x, bool const &y) noexcept            \
{                                                                       \
    return bool(x._v OP y);                                             \
}

#define CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(OP)         \
   CIG_BASIC_TYPE_BINARY_OPERATOR( u8, uint8_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR(u16,uint16_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR(u32,uint32_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR(u64,uint64_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR( i8,  int8_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR(i16, int16_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR(i32, int32_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR(i64, int64_t,OP)

#define CIG_ALL_BASIC_TYPES_BOOL_BINARY_OPERATOR(OP)    \
   CIG_BASIC_TYPE_BINARY_OPERATOR(cig_bool,    bool,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(      u8, uint8_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(     u16,uint16_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(     u32,uint32_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(     u64,uint64_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(      i8,  int8_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(     i16, int16_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(     i32, int32_t,OP) \
   CIG_BASIC_TYPE_BINARY_OPERATOR(     i64, int64_t,OP)

#define CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(cigtype,ctype,OP)     \
[[nodiscard]] constexpr                                                 \
cig_bool operator OP (cigtype const &x, cigtype const &y) noexcept      \
{                                                                       \
    return x._v OP y._v;                                                \
}

#define CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED( u8, uint8_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(u16,uint16_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(u32,uint32_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(u64,uint64_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED( i8,  int8_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(i16, int16_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(i32, int32_t,OP)   \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(i64, int64_t,OP)

#define CIG_ALL_BASIC_TYPES_BOOL_BINARY_OPERATOR_BOOLVALUED(OP)         \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(cig_bool, bool,OP)         \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(      u8, uint8_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(     u16,uint16_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(     u32,uint32_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(     u64,uint64_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(      i8,  int8_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(     i16, int16_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(     i32, int32_t,OP)      \
   CIG_BASIC_TYPE_BINARY_OPERATOR_BOOLVALUED(     i64, int64_t,OP)

#define CIG_BASIC_TYPE_ASSIGN_OPERATOR(cigtype,ctype,OP)        \
inline cigtype &                                                \
cigtype::operator OP (cigtype const &x) noexcept                \
{                                                               \
    _v OP x._v;                                                 \
    return *this;                                               \
}

#define CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(OP)         \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR( u8, uint8_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR(u16,uint16_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR(u32,uint32_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR(u64,uint64_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR( i8,  int8_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR(i16, int16_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR(i32, int32_t,OP)      \
   CIG_BASIC_TYPE_ASSIGN_OPERATOR(i64, int64_t,OP)




CIG_ALL_BASIC_TYPES_BOOL_UNARY_OPERATOR(+)
CIG_ALL_BASIC_TYPES_UNARY_OPERATOR(-)
CIG_ALL_BASIC_TYPES_UNARY_OPERATOR(~)
CIG_ALL_BASIC_TYPES_BOOL_UNARY_OPERATOR_BOOLVALUED(!)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(+)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(-)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(*)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(/)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(%)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(<)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(>)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(<=)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(>=)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(==)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR_BOOLVALUED(!=)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(&)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(^)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(|)
CIG_BOOL_BOOL_BINARY_OPERATOR(<)
CIG_BOOL_BOOL_BINARY_OPERATOR(>)
CIG_BOOL_BOOL_BINARY_OPERATOR(<=)
CIG_BOOL_BOOL_BINARY_OPERATOR(>=)
CIG_BOOL_BOOL_BINARY_OPERATOR(==)
CIG_BOOL_BOOL_BINARY_OPERATOR(!=)
CIG_BOOL_BOOL_BINARY_OPERATOR(&)
CIG_BOOL_BOOL_BINARY_OPERATOR(^)
CIG_BOOL_BOOL_BINARY_OPERATOR(|)
// Since C++ overloaded operators do not short-circuit,
// we do not overload the operators && and || here.
// cig_BOOL_BOOL_BINARY_OPERATOR(&&)
// CIG_BOOL_BOOL_BINARY_OPERATOR(||)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(<<)
CIG_ALL_BASIC_TYPES_BINARY_OPERATOR(>>)

CIG_BASIC_TYPE_ASSIGN_OPERATOR(cig_bool,bool,&=)
CIG_BASIC_TYPE_ASSIGN_OPERATOR(cig_bool,bool,^=)
CIG_BASIC_TYPE_ASSIGN_OPERATOR(cig_bool,bool,|=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(+=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(-=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(*=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(/=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(%=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(&=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(^=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(|=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(<<=)
CIG_ALL_BASIC_TYPES_ASSIGN_OPERATOR(>>=)

#undef CIG_ALL_BASIC_TYPES_BINARY_OPERATOR
#undef CIG_BASIC_TYPE_BINARY_OPERATOR


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_INT_INLINE_H_INCLUDED) */
