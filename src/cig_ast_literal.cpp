/* cig_ast_literal.cpp
 *
 * Copyright 2024 Claus Fischer
 *
 * Abstract Syntax Tree - Literals.
 */
#include "cig_ast_literal.h"
#include "cig_toolbox.h"
#include "cig_constexpr_itoa.h"

namespace cig {

namespace ast {


// Convert a literal to string as in C++ source code
str literal::to_str() const noexcept
{
    auto littype = get_literal_type();

    switch (littype)
    {
    case literal::literal_type::LIT_UNDEFINED:
        return "<undefined literal type>"_str;
    case literal::literal_type::LIT_STR:
    {
        str const &s = std::get<str>(*this);
        auto exps = toolbox::exp_encode_json_str(s,false);
        if (!exps)
        {
            cig::err e = std::move(exps.error());
            return e.to_str();
        }
        return exps.value();
    }
    case literal::literal_type::LIT_BYTES:
    {
        bytes const &b = std::get<bytes>(*this);
        if (b.is_pure_utf8())
        {
            auto expb = toolbox::exp_encode_u8_bytes(b);
            if (!expb)
            {
                cig::err e = std::move(expb.error());
                return e.to_str();
            }
            return "u8"_str + expb.value();
        }
        else
        {
            auto expb = toolbox::exp_encode_C_bytes(b);
            if (!expb)
            {
                cig::err e = std::move(expb.error());
                return e.to_str();
            }
            return "b"_str + expb.value();
        }
        return "<fixme encode bytes>"_str; // "std::get<bytes>(*this);
    }
    case literal::literal_type::LIT_I64:
        return std::get<i64>(*this).to_str(); // i64
    case literal::literal_type::LIT_I32:
        return std::get<i32>(*this).to_str() + "i32"_str;
    case literal::literal_type::LIT_I16:
        return std::get<i16>(*this).to_str() + "i16"_str;
    case literal::literal_type::LIT_I8:
        return std::get<i8>(*this).to_str() + "i8"_str;
    case literal::literal_type::LIT_U64:
        return std::get<u64>(*this).to_str() + "u"_str; // u64
    case literal::literal_type::LIT_U32:
        return std::get<u32>(*this).to_str() + "u32"_str;
    case literal::literal_type::LIT_U16:
        return std::get<u16>(*this).to_str() + "u16"_str;
    case literal::literal_type::LIT_U8:
        return std::get<u8>(*this).to_str() + "u8"_str;
    case literal::literal_type::LIT_BOOL:
        return std::get<cig_bool>(*this).to_str();
    }
    return "<unknown literal type>"_str;
}


} // namespace ast

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
