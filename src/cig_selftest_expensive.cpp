/* cig_selftest_expensive.cpp
 *
 * Copyright 2022 Claus Fischer
 *
 * Expensive selftest function for cig.
 */
#include "cig_selftest_expensive.h"

#include <cstring>
#include "cig_str_inline.h"
#include "cig_literals.h"
#include "cig_int_inline.h"
#include "cig_selftest.h"
#include "cig_err.h"

// Macros for file and line number
#define TOSTR(x) #x
#define TOLINENUM(x) TOSTR(x)
#define E(s)                                                            \
    do {                                                                \
        return (__FILE__  ":" TOLINENUM(__LINE__) ": error: " s);       \
    } while (0)


// This selftest is in the generated file cig_sysdep_errno_selftest.c
extern "C" char const * cig_sysdep_errno_selftest();

// This selftest is in cig_parser.cpp
extern "C" char const * cig_parser_selftest();

namespace cig {

static char const *selftest_errno()
{
    // This selftest is in the generated file cig_sysdep_errno_selftest.c
    return cig_sysdep_errno_selftest();
}

static char const *selftest_str()
{
    // Comparison of string expressions
    str abcdef = "abcdef"_str;

    if (abcdef.slice(2,4) != str("cd"_str) * 1_i64)
        E("bad slice 1");
    if ((2_i64 * abcdef).slice(2_i64,std::nullopt,7) != str("cd"_str) * 1_i64)
        E("bad slice 2");
    str abcdef6 = 6 * abcdef;
    if (abcdef6._get_size() != 36)
        E("bad slice 3");
    if (abcdef6.slice(-6,3,-5) != abcdef)
        E("bad slice 4");
    if ((6 * abcdef).slice(-6,3,-5) != "abcdef"_str)
        E("bad slice 5");
    if ((((2 * abcdef.slice(std::nullopt,std::nullopt,-1)) * 2)
         + "fedcba"_str * 2)
        .slice(5,-2,5) != "abcdef"_str)
        E("bad slice 6");
    

    // Systematic slice test with sliding sieve
    str first = "abcdefghijklmn"_str;
    CIG_CONSTEXPR str second = "1234567"_str;
    str third = "opqrstuvwxyz"_str;

    str full = first + 5 * second + third;
#define FULL (first + 5 * second + third)

    str fullexpanded =
        "abcdefghijklmn12345671234567123456712345671234567opqrstuvwxyz"_str;

    if (full != fullexpanded)
	E("full != fullexpanded");
    if (FULL != fullexpanded)
        E("FULL != fullexpanded");

    for (std::size_t i1 = 0; i1 < 100; i1++)
    {
        for (std::ptrdiff_t step = -10; step < 10; step++)
        {
            if (step == 0)
                continue;
            if (FULL.slice(i1,std::nullopt,step) !=
                fullexpanded.slice(i1,std::nullopt,step))
                E("bad slice in loop");
            if (FULL.slice(i1,std::nullopt,step)
                .slice(std::nullopt,std::nullopt,-1) !=
                fullexpanded.slice(i1,std::nullopt,step)
                .slice(std::nullopt,std::nullopt,-1))
                E("bad slice in loop");
            if (FULL.slice(i1,10,step) !=
                fullexpanded.slice(i1,10,step))
                E("bad slice in loop");
        }
    }


    // A slice must reduce the chartype if possible
    str oe = "Österreich"_str;
    if (oe._get_chartype() != str::_chartype_ucs1)
        E("bad chartype of Oesterreich");
    str sterreich = oe.slice(1);
    if (sterreich._get_chartype() != str::_chartype_ascii)
        E("bad chartype of sterreich");


    // Test the find functions
    str atstart = "abc    abc    abc  "_str;
    str atend = "  abc    abc    abc"_str;

    if (!atstart.starts_with("abc"_str))
        E("error in str::starts_with");
    if (atend.starts_with("abc"_str))
        E("error in str::starts_with");
    if (atstart.ends_with("abc"_str))
        E("error in str::ends_with");
    if (!atend.ends_with("abc"_str))
        E("error in str::ends_with");
    if (atstart.find_first_of("cba"_str,5,14) != i64(7))
        E("error in str::find_first_of(charlist)");
    if (atstart.find_last_of("cba"_str,5,14) != i64(9))
        E("error in str::find_first_of(charlist)");


    return 0;
}

static char const *selftest_cig_error()
{
    cig::err e(err::E_EPERM);

    if (strcmp(e.errortag_c(),"EPERM") != 0)
        E("bad errortag_c for EPERM");
    if (e.errortag() != "EPERM"_str)
        E("bad errortag for EPERM");
    if (e.to_str() != "EPERM (1): Operation not permitted"_str)
        E("bad error.to_str() for EPERM");
    return 0;
}

char const *expensive_selftest_errorstring()
{
    using namespace detail;

    char const *selftest_error = 0;

    // Start with the quick selftest for runtime programs
    selftest_error = selftest_errorstring();
    if (selftest_error)
	return selftest_error;

    // Perform a selftest for errno values
    selftest_error = selftest_errno();
    if (selftest_error)
	return selftest_error;

    // Perform a selftest for strings
    selftest_error = selftest_str();
    if (selftest_error)
	return selftest_error;

    // Perform a selftest for the error class
    selftest_error = selftest_cig_error();
    if (selftest_error)
	return selftest_error;

    // Perform a selftest for the parser
    selftest_error = cig_parser_selftest();
    if (selftest_error)
        return selftest_error;

    return 0;
}

} // namespace cig

/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
