/* cig_startup_filepointer.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Startup and termination code for cig_main; file pointer repository.
 *
 * This include file is only for startup-related code;
 * it should not be used in the general parts of the code.
 */
#ifndef CIG_STARTUP_FILEPOINTER_H_INCLUDED
#define CIG_STARTUP_FILEPOINTER_H_INCLUDED

#include <cstdio>               // FILE

#include "cig_simple_dll.h"     // cig::detail::dll::node


namespace cig {

namespace startup {

namespace detail {

// A globally registered FILE * wrapper,
// to enable flushing and closing these FILE pointers
// when assert fails or the fork() system call for
// daemonization wants to close them.


struct filepointer_wrapper;

// Global function to register a filepointer_wrapper
void register_filepointer_wrapper(filepointer_wrapper &);

struct filepointer_wrapper : cig::detail::dll::node<filepointer_wrapper>
{
    char const *name;
    FILE *file_pointer;

    filepointer_wrapper(char const *nm) :
        name(nm), file_pointer(0)
    {
        // The registrator does not specify a name,
        // all users should do
        if (nm)
            register_filepointer_wrapper(*this);
    }
    ~filepointer_wrapper() noexcept { close(); }

    operator bool() const noexcept { return file_pointer != 0; }
    void flush() const noexcept { if (file_pointer) std::fflush(file_pointer); }
    void close() noexcept
    {
        if (file_pointer)
            std::fclose(file_pointer);
        file_pointer = 0;
    }
    filepointer_wrapper & operator = (FILE *fp) noexcept
    {
        close();
        file_pointer = fp;
        return *this;
    }
};

} // namespace detail


// Global function to flush all filepointer_wrapper objects
void flush_all_filepointer_wrappers();

// Global function to close all filepointer_wrapper objects
void close_all_filepointer_wrappers();


} // namespace startup

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_STARTUP_FILEPOINTER_H_INCLUDED) */
