/* cig_allocation.h
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Allocation functions.
 */
#ifndef CIG_ALLOCATION_H_INCLUDED
#define CIG_ALLOCATION_H_INCLUDED

#include <cstddef>              // std::size_t
#include <utility>              // std::move
#include <array>                // std::array
#include <cstdint>              // int64_t

#include "cig_source_location.h" // std::source_location
#include "cig_simple_dll.h"     // cig::detail::simple_dll

namespace cig {

namespace allocation {

namespace detail {


struct allocator_data;


// Global function to register an allocator
void register_allocator(allocator_data &) noexcept;


// Each allocation is done via an allocator.
// 
// Allocators are identified by a name (an ASCII string)
// and created as allocator<"name">.
// Any name can be chosen, and allocations and deallocations
// for the same allocator will be cumulated.
// 
// At the end of the program, an automatic check will be
// performed to ensure that all allocated objects have
// been freed.
// 
// For this purpose, each allocator registers with a global allocation_tracker
// defined in cig_allocation.cpp.
// Registration works by hooking the allocator_dll of the allocator
// into a double linked list in the allocation tracker.
// While hooking in must be done by calling register_allocator(),
// unhooking the allocator works automatically by calling the destructor
// of either the allocator or the allocation tracker.

struct allocator_data : cig::detail::dll::node<allocator_data>
{
    char const *name;
    int64_t object_alloc;       // Counter of objects allocationd
    int64_t object_free;        // Counter of objects freed
    int64_t object_max;         // Maximum allocated objects at one time
    int64_t byte_alloc;         // Counter of bytes allocated
    int64_t byte_free;          // Counter of bytes freed
    int64_t byte_max;           // Maximum allocated bytes at one time

public:
    allocator_data(char const *nm) noexcept :
        name(nm),
        object_alloc(0), object_free(0), object_max(0),
        byte_alloc(0), byte_free(0), byte_max(0)
    {
        // The allocation tracker does not specify a name,
        // all allocators defined here do
        if (nm)
            register_allocator(*this);
    }
};

// Class allocator_rawstring encapsulates native C/C++-strings as names,
// it is use to convert "name" into a template parameter
template<std::size_t N>
struct allocator_rawstring
{
    std::array<char,N> arr {};
    constexpr allocator_rawstring(char const (&pp)[N]) noexcept
    {
	for (std::size_t i = 0; i < N; i++)
	    arr[i] = pp[i];
    }
};


} // namespace detail; back to cig::allocation


// 
// Globals in cig::allocation
// 

// Allocate bytes (the allocator name is used for logging)
void *
allocate(std::size_t size,
         char const *allocatorname,
         cig::source_location const &loc = std::source_location::current())
    noexcept;

// Free bytes (the allocator name is used for logging)
void
free(void *ptr,
     std::size_t size,
     char const *allocatorname,
     cig::source_location const &loc = std::source_location::current())
    noexcept;

// Allocate bytes (the allocator name is used for logging)
void *
allocate_or_return_zero(
    std::size_t size,
    char const *allocatorname,
    cig::source_location const &loc = std::source_location::current())
    noexcept;

// Check whether memory is still allocated
bool check_still_allocated() noexcept;

// Print an end report about memory still not freed per allocator class
void endreport(void *file_pointer,bool force_print = false) noexcept;

// Flush the allocation logger file (and close, will be automatically reopened)
void flush() noexcept;


} // namespace allocation


// Class baseallocator is the basic allocator specific for the "name"
template<allocation::detail::allocator_rawstring A>
struct baseallocator : allocation::detail::allocator_data
{

    static inline auto namearr = A.arr;
    static inline char const *name = &namearr[0];
    static inline baseallocator object;

    static baseallocator &get_allocator() noexcept // a singleton
    {
        return object;
    }

    template<typename T>
    [[nodiscard]] static T *alloc(
        std::size_t count,
        cig::source_location const &loc = std::source_location::current()
        ) noexcept
    {
        baseallocator &all = get_allocator();
        std::size_t sz = count * sizeof(T);
        void *voidptr = // failing will abort from cig_assert
            allocation::allocate(sz,name,loc);
        T *ptr = reinterpret_cast<T*>(voidptr);
        all.object_alloc++;
        if (all.object_max < all.object_alloc - all.object_free)
            all.object_max = all.object_alloc - all.object_free;
        all.byte_alloc += static_cast<int64_t>(sz);
        if (all.byte_max < all.byte_alloc - all.byte_free)
            all.byte_max = all.byte_alloc - all.byte_free;
        return ptr;
    }

    template<typename T>
    [[nodiscard]] static T *alloc_or_return_zero(
        std::size_t count,
        cig::source_location const &loc = std::source_location::current()
        ) noexcept
    {
        baseallocator &all = get_allocator();
        std::size_t sz = count * sizeof(T);
        void *voidptr = // failing will return 0
            allocation::allocate_or_return_zero(sz,name,loc);
        T *ptr = reinterpret_cast<T*>(voidptr);
        if (ptr)                // Count only if a value has been allocated
        {
            all.object_alloc++;
            if (all.object_max < all.object_alloc - all.object_free)
                all.object_max = all.object_alloc - all.object_free;
            all.byte_alloc += static_cast<int64_t>(sz);
            if (all.byte_max < all.byte_alloc - all.byte_free)
                all.byte_max = all.byte_alloc - all.byte_free;
        }
        return ptr;
    }

    template<typename T>
    static void free(T *ptr,
              std::size_t count,
              cig::source_location const &loc = std::source_location::current()
        ) noexcept
    {
        baseallocator &all = get_allocator();
        std::size_t sz = count * sizeof(T);
        void *voidptr = reinterpret_cast<void*>(ptr);
        allocation::free(voidptr,sz,name,loc);
        all.object_free++;
        all.byte_free += static_cast<int64_t>(sz);
    }

private:
    baseallocator() noexcept :
        allocator_data(&A.arr[0]) { }
};


// Class allocator is a replacement for std::allocator for containers
template<typename T,allocation::detail::allocator_rawstring A="cig::allocator">
class allocator
{
public:
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef T *pointer;
    typedef T const *const_pointer;
    typedef T &reference;
    typedef T const &const_reference;
    typedef T value_type;
    template <class U> 
    struct rebind { typedef allocator<U, A> other; };

    allocator() noexcept {};
    allocator([[maybe_unused]] const allocator& other) noexcept {}

    template<typename U>
    allocator([[maybe_unused]] const allocator<U,A>& other) noexcept {}

    template<typename U>
    allocator& operator = ([[maybe_unused]] const allocator<U,A>& other)
        noexcept {
        return *this; }
    allocator<T>& operator = ([[maybe_unused]] const allocator& other)
        noexcept {
        return *this; }
    ~allocator() noexcept {}


    [[nodiscard]] static pointer
    allocate(size_type n,
             cig::source_location const &loc =
             std::source_location::current()) noexcept
    {
        return cig::baseallocator<A>::template alloc<T>(n,loc);
    }
    static void
    deallocate(pointer ptr,size_type n,
               cig::source_location const &loc =
               std::source_location::current()) noexcept
    {
        cig::baseallocator<A>::template free<T>(ptr,n,loc);
    }

    // Combinations of array allocator and constructor/destructor.
    // These are not required for std::allocator compatibility
    // but are useful in some situations.
    [[nodiscard]] static pointer
    allocate_and_construct(size_type n,
                           cig::source_location const &loc =
                           std::source_location::current()) noexcept
    {
        pointer p = cig::baseallocator<A>::template alloc<T>(n,loc);
        size_type i;
        for (i = 0; i < n; i++)
            new(p+i) T;
        return p;
    }
    static void
    deallocate_and_destruct(pointer ptr,size_type n,
                            cig::source_location const &loc =
                            std::source_location::current()) noexcept
    {
        size_type i;
        for (i = n; i > 0; i--)
            (ptr + (i-1))->~T();
        cig::baseallocator<A>::template free<T>(ptr,n,loc);
    }
};


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_ALLOCATION_H_INCLUDED) */
