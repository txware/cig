/* cig_interpreter_ops_and_types.h
 *
 * Copyright 2024 Claus Fischer
 *
 * Interpreter operations and types for CIG.
 */
#ifndef CIG_OPSTEM_H_INCLUDED
#define CIG_OPSTEM_H_INCLUDED

#include <cstdint>              // uint8_t ...

namespace cig {

namespace interpreter {


// Operation stem (operation without arguments)
enum opstem {
    OPSTEM_NOOP,                // used as a sentinel value
    OPSTEM_TRAP,                // stop program (ip should never come here)
    OPSTEM_LABEL,

    // Jump operations

    OPSTEM_JMP,                 // Jump unconditionally
    OPSTEM_JMPTRUE,             // Jump if condition variable is true
    OPSTEM_JMPFALSE,            // Jump if condition variable is false
    OPSTEM_JMPZERO,             // Jump if builtin type value is zero
    OPSTEM_JMPNONZERO,          // Jump if builtin type value is nonzero

    OPSTEM_JMPBREAK,            // Jump if 'break' flag is set
    OPSTEM_JMPCONTINUE,         // Jump if 'continue' flag is set
    OPSTEM_JMPRETURN,           // Jump if 'return' flag is set
    OPSTEM_JMPEXCEPT,           // Jump if 'exception' flag is set
    OPSTEM_JMPANYUWFLAG,        // Jump if any 'unwind' flag is set
    OPSTEM_JMPNOUWFLAG,         // Jump if no 'unwind' flag is set
    OPSTEM_CLEARUW,             // Clear unwind flags
    OPSTEM_SETBREAK,            // Set the 'break' flag
    OPSTEM_SETCONTINUE,         // Set the 'continue' flag
    OPSTEM_SETRETURN,           // Set the 'return' flag
    OPSTEM_SETEXCEPT,           // Set the 'except' flag
    OPSTEM_CLEARBREAK,          // Clear the 'break' flag
    OPSTEM_CLEARCONTINUE,       // Clear the 'continue' flag
    OPSTEM_CLEARRETURN,         // Clear the 'return' flag
    OPSTEM_CLEAREXCEPT,         // Clear the 'except' flag

    // Construct and destruct an object in a position in local memory

    OPSTEM_DESTRUCT,
    OPSTEM_CONSTRUCT,

    OPSTEM_COPYLIT,

    // Copy and move operations for local memory
    //
    // There are six operations
    // for each combination of source and destination type.
    //
    // The keyword COPY indicates that the source object remains
    // a valid object and its contents is fully duplicated.
    // The keyword MOVE indicates that its source is emptied
    // as in a typical C++ move scenario; however, it is left as
    // a valid empty C++ object and its destructor may be called
    // afterwards (and should have no effect).
    // The kewyrd TRSF (transfer) indicates that the raw memory
    // is copied to the destination. After this operation, the
    // source must not be treated as an object, in particular it
    // is illegal to call its destructor afterwards.
    //
    // The interpreter code generator must carefully pick which
    // of these keywords it uses.
    //
    // All operations will work correctly if source and destination
    // are the same place in memory.
    //
    // Operation  Description/C++ equivalent
    // ---------  -------------------------------------------------------------
    // COPYMEM    copy an object onto raw memory (copy constructor)
    // COPYOBJ    copy an object onto an object (copy assignment)
    // MOVEMEM    move an object onto raw mem, leave deleted (move constructor)
    // MOVEOBJ    move an object onto an object, leave deleted (move assignment)
    // TRSFMEM    transfer an object onto raw memory (pure memcpy)
    // TRSFOBJ    transfer an object onto an object (destructor then memcopy)

    OPSTEM_COPYMEM,
    OPSTEM_COPYOBJ,
    OPSTEM_MOVEMEM,
    OPSTEM_MOVEOBJ,
    OPSTEM_TRSFMEM,
    OPSTEM_TRSFOBJ,

    OPSTEM_MOVETOFN,            // Move to function, C++ move constructor
    OPSTEM_MOVEFROMFN,          // Move from function, C++ move constructor

    // Unary operations

    OPSTEM_SIGNPLUS,
    OPSTEM_SIGNMINUS,
    OPSTEM_BIT_NOT,
    OPSTEM_LOGICAL_NOT,

    // Binary operations

    OPSTEM_MULT,
    OPSTEM_DIV,
    OPSTEM_MOD,
    OPSTEM_ADD,
    OPSTEM_SUB,
    OPSTEM_SHIFT_LEFT,
    OPSTEM_SHIFT_RIGHT,
    OPSTEM_LESS,
    OPSTEM_GREATER,
    OPSTEM_LESS_EQUAL,
    OPSTEM_GREATER_EQUAL,
    OPSTEM_EQUAL,
    OPSTEM_NOT_EQUAL,
    OPSTEM_AND,
    OPSTEM_XOR,
    OPSTEM_OR,
    OPSTEM_LOGICAL_AND,
    OPSTEM_LOGICAL_OR,
    OPSTEM_ADD_ASSIGN,
    OPSTEM_SUB_ASSIGN,
    OPSTEM_MULT_ASSIGN,
    OPSTEM_DIV_ASSIGN,
    OPSTEM_MOD_ASSIGN,
    OPSTEM_SHL_ASSIGN,
    OPSTEM_SHR_ASSIGN,
    OPSTEM_AND_ASSIGN,
    OPSTEM_XOR_ASSIGN,
    OPSTEM_OR_ASSIGN,
    OPSTEM_DOT_ASSIGN,
    OPSTEM_ASSIGN,

    // End operation (function return, must be present)

    OPSTEM_END                  // used as max value
};

// Interpreter basic type
enum oparg : int8_t {
    OPARG_BOOL      =  0,       // Types BOOL..BYTES have the same values
    OPARG_U8        =  1,       // as ast_builtin_type.
    OPARG_U16       =  2,       // Verified in cig_interpreter_selftest.
    OPARG_U32       =  3,
    OPARG_U64       =  4,
    OPARG_I8        =  5,
    OPARG_I16       =  6,
    OPARG_I32       =  7,
    OPARG_I64       =  8,
    OPARG_STR       =  9,
    OPARG_BYTES     = 10,
    OPARG_STREXPR   = 11,
    OPARG_BYTESEXPR = 12,
    OPARG_COMPOUND  = 13,       // used as a sentinel and max value
    OPARG_VOID      = 14,
    OPARG_LIST      = 15
};

// The operation stems are categorized into several classes
enum opstem_class {
    OPSTEMCL_NONE,
    OPSTEMCL_ARITHMETIC,
    OPSTEMCL_BITWISE,
    OPSTEMCL_LOGICAL,
    OPSTEMCL_OTHER
};

// The operation arguments are categorized into several classes
// This is parallel to cig_ast_builtin.cpp
enum oparg_class {
    OPARGCL_NONE,
    OPARGCL_BOOL,
    OPARGCL_INTEGRAL,
    OPARGCL_FLOAT,              // Prepared for future float builtin types
    OPARGCL_STR,
    OPARGCL_BYTES,
    OPARGCL_OTHER
};

char const *opstem_name(opstem stem);
opstem opstem_from_name(char const *name); // opstem or OPSTEM_NOOP
char const *opstem_symbol(opstem stem);
opstem_class opstem_to_class(opstem stem);
int opstem_nargs(opstem stem);    // number of arguments
bool opstem_assigns(opstem stem); // is an assignment: +=, .=, = ...
bool opstem_boolres(opstem stem); // result is always boolean


char const *oparg_name(oparg arg);
oparg oparg_from_name(char const *name); // oparg or OPARG_COMPOUND
char const *oparg_cigname(oparg arg);
oparg_class oparg_to_class(oparg arg);
bool oparg_is_trivial(oparg arg);        // trivially copyable

oparg                           // result type, or OPARG_COMPOUND
oparg_unary_result_type(
    opstem stem,                // The operation
    oparg arg                   // The argument type
    );

oparg                           // result type (or VOID), or OPARG_COMPOUND
oparg_binary_result_type(
    opstem stem,                // The operation
    oparg arg1,                 // Type of argument 1
    oparg arg2                  // Type of argument 2
    );

} // namespace interpreter

} // namespace cig


extern "C" char const *cig_interpreter_selftest();


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_OPSTEM_H_INCLUDED) */
