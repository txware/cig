/* cig_print.h
 *
 * Copyright 2022 Claus Fischer
 *
 * A template print function for cig.
 */
#ifndef CIG_PRINT_H_INCLUDED
#define CIG_PRINT_H_INCLUDED

#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_str_inline.h"

#include <cstdio>               // FILE,fprintf


namespace cig {

struct print
{
    // A quasi named parameter 'print::end("\n")'
    struct end {
        str const _end;
        end(str const &e) : _end(e) {}
    };

    FILE *_fp;                  // FILE to use, default stdout
    cig::str _end;              // line-end to use, default "\n"

    // Basic single-string printer
    template<typename T>
    void print_str(T &&s) {
        fprintf(_fp,"%s",s._data_ucs1()); // TODO: convert to UTF-8
    }

    // Tuple printer
    template<typename... Args, std::size_t... I>
    void print_tuple(std::tuple<Args &...> params, std::index_sequence<I...>)
    {
        ( ([&]() // Set _fp from FILE argument and _end from end class
            {
                constexpr bool is_file =
                    std::is_same<FILE * const,Args>::value;
                if constexpr (is_file) {
                    _fp = std::get<I>(params);
                }
                constexpr bool is_end =
                    std::is_same<const end,Args>::value;
                if constexpr (is_end) {
                    _end = std::get<I>(params)._end;
                }
            }()), ... );
        
        ( ([&]() // Print the printable arguments
            {
                if constexpr (std::is_same<FILE * const,Args>::value) { }
                else if constexpr (std::is_same<const end,Args>::value) {}
                else if constexpr (std::is_base_of<detail::strval,Args>::value)
                {
                    print_str(std::get<I>(params));
                }
                else
                {
                    print_str(std::get<I>(params).to_str());
                }
            }()), ... );

        print_str(_end);
    }

    // Template constructor converts to a tuple
    template<typename... Args>
    print(const Args &... args) : _fp(stdout), _end("\n"_str)
    {
        print_tuple(std::forward_as_tuple(args...),
                    std::make_index_sequence<sizeof...(args)>{});
    }
};


} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_PRINT_H_INCLUDED) */
