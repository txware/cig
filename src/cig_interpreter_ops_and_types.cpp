/* cig_interpreter_ops_and_types.cpp
 *
 * Copyright 2024 Claus Fischer
 *
 * Interpreter operations and types for CIG.
 */
#include <cstring>              // strcmp

#include "cig_interpreter_ops_and_types.h"


namespace cig {

namespace interpreter {


char const *
opstem_name(opstem stem)
{
    switch (stem)
    {
    case OPSTEM_NOOP: return "NOOP";
    case OPSTEM_TRAP: return "TRAP";
    case OPSTEM_LABEL: return "LABEL";
    case OPSTEM_JMP: return "JMP";
    case OPSTEM_JMPTRUE: return "JMPTRUE";
    case OPSTEM_JMPFALSE: return "JMPFALSE";
    case OPSTEM_JMPZERO: return "JMPZERO";
    case OPSTEM_JMPNONZERO: return "JMPNONZERO";
    case OPSTEM_JMPBREAK: return "JMPBREAK";
    case OPSTEM_JMPCONTINUE: return "JMPCONTINUE";
    case OPSTEM_JMPRETURN: return "JMPRETURN";
    case OPSTEM_JMPEXCEPT: return "JMPEXCEPT";
    case OPSTEM_JMPANYUWFLAG: return "JMPANYUWFLAG";
    case OPSTEM_JMPNOUWFLAG: return "JMPNOUWFLAG";
    case OPSTEM_CLEARUW: return "CLEARUW";
    case OPSTEM_SETBREAK: return "SETBREAK";
    case OPSTEM_SETCONTINUE: return "SETCONTINUE";
    case OPSTEM_SETRETURN: return "SETRETURN";
    case OPSTEM_SETEXCEPT: return "SETEXCEPT";
    case OPSTEM_CLEARBREAK: return "CLEARBREAK";
    case OPSTEM_CLEARCONTINUE: return "CLEARCONTINUE";
    case OPSTEM_CLEARRETURN: return "CLEARRETURN";
    case OPSTEM_CLEAREXCEPT: return "CLEAREXCEPT";
    case OPSTEM_DESTRUCT: return "DESTRUCT";
    case OPSTEM_CONSTRUCT: return "CONSTRUCT";
    case OPSTEM_COPYLIT: return "COPYLIT";
    case OPSTEM_COPYMEM: return "COPYMEM";
    case OPSTEM_COPYOBJ: return "COPYOBJ";
    case OPSTEM_MOVEMEM: return "MOVEMEM";
    case OPSTEM_MOVEOBJ: return "MOVEOBJ";
    case OPSTEM_TRSFMEM: return "TRSFMEM";
    case OPSTEM_TRSFOBJ: return "TRSFOBJ";
    case OPSTEM_MOVETOFN: return "MOVETOFN";
    case OPSTEM_MOVEFROMFN: return "MOVEFROMFN";
    case OPSTEM_SIGNPLUS: return "SIGNPLUS";
    case OPSTEM_SIGNMINUS: return "SIGNMINUS";
    case OPSTEM_BIT_NOT: return "BIT_NOT";
    case OPSTEM_LOGICAL_NOT: return "LOGICAL_NOT";
    case OPSTEM_MULT: return "MULT";
    case OPSTEM_DIV: return "DIV";
    case OPSTEM_MOD: return "MOD";
    case OPSTEM_ADD: return "ADD";
    case OPSTEM_SUB: return "SUB";
    case OPSTEM_SHIFT_LEFT: return "SHIFT_LEFT";
    case OPSTEM_SHIFT_RIGHT: return "SHIFT_RIGHT";
    case OPSTEM_LESS: return "LESS";
    case OPSTEM_GREATER: return "GREATER";
    case OPSTEM_LESS_EQUAL: return "LESS_EQUAL";
    case OPSTEM_GREATER_EQUAL: return "GREATER_EQUAL";
    case OPSTEM_EQUAL: return "EQUAL";
    case OPSTEM_NOT_EQUAL: return "NOT_EQUAL";
    // case OPSTEM_IN: return "IN";
    case OPSTEM_AND: return "AND";
    case OPSTEM_XOR: return "XOR";
    case OPSTEM_OR: return "OR";
    case OPSTEM_LOGICAL_AND: return "LOGICAL_AND";
    case OPSTEM_LOGICAL_OR: return "LOGICAL_OR";
    case OPSTEM_ADD_ASSIGN: return "ADD_ASSIGN";
    case OPSTEM_SUB_ASSIGN: return "SUB_ASSIGN";
    case OPSTEM_MULT_ASSIGN: return "MULT_ASSIGN";
    case OPSTEM_DIV_ASSIGN: return "DIV_ASSIGN";
    case OPSTEM_MOD_ASSIGN: return "MOD_ASSIGN";
    case OPSTEM_SHL_ASSIGN: return "SHL_ASSIGN";
    case OPSTEM_SHR_ASSIGN: return "SHR_ASSIGN";
    case OPSTEM_AND_ASSIGN: return "AND_ASSIGN";
    case OPSTEM_XOR_ASSIGN: return "XOR_ASSIGN";
    case OPSTEM_OR_ASSIGN: return "OR_ASSIGN";
    case OPSTEM_DOT_ASSIGN: return "DOT_ASSIGN";
    case OPSTEM_ASSIGN: return "ASSIGN";
    case OPSTEM_END: return "END";
    }
    return "<unknown opstem>";
}


opstem   // opstem or OPSTEM_COMPOUND
opstem_from_name(char const *name)
{
    opstem a;
    for (a = static_cast<opstem>(0);
         a < OPSTEM_END;
         a = static_cast<opstem>(a + 1))
    {
        if (std::strcmp(opstem_name(a),name) == 0)
            return a;
    }
    if (std::strcmp(opstem_name(OPSTEM_END),name) == 0)
        return OPSTEM_END;
    return OPSTEM_NOOP;
}


char const *
opstem_symbol(opstem stem)
{
    switch (stem)
    {
    case OPSTEM_NOOP:           return "";
    case OPSTEM_TRAP:           return "";
    case OPSTEM_LABEL:          return "";
    case OPSTEM_JMP:            return "";
    case OPSTEM_JMPTRUE:        return "";
    case OPSTEM_JMPFALSE:       return "";
    case OPSTEM_JMPZERO:        return "";
    case OPSTEM_JMPNONZERO:     return "";
    case OPSTEM_JMPBREAK:       return "";
    case OPSTEM_JMPCONTINUE:    return "";
    case OPSTEM_JMPRETURN:      return "";
    case OPSTEM_JMPEXCEPT:      return "";
    case OPSTEM_JMPANYUWFLAG:   return "";
    case OPSTEM_JMPNOUWFLAG:    return "";
    case OPSTEM_CLEARUW:        return "";
    case OPSTEM_SETBREAK:       return "";
    case OPSTEM_SETCONTINUE:    return "";
    case OPSTEM_SETRETURN:      return "";
    case OPSTEM_SETEXCEPT:      return "";
    case OPSTEM_CLEARBREAK:     return "";
    case OPSTEM_CLEARCONTINUE:  return "";
    case OPSTEM_CLEARRETURN:    return "";
    case OPSTEM_CLEAREXCEPT:    return "";
    case OPSTEM_DESTRUCT:       return "";
    case OPSTEM_CONSTRUCT:      return "";
    case OPSTEM_COPYLIT:        return "";
    case OPSTEM_COPYMEM:        return "";
    case OPSTEM_COPYOBJ:        return "";
    case OPSTEM_MOVEMEM:        return "";
    case OPSTEM_MOVEOBJ:        return "";
    case OPSTEM_TRSFMEM:        return "";
    case OPSTEM_TRSFOBJ:        return "";
    case OPSTEM_MOVETOFN:       return "";
    case OPSTEM_MOVEFROMFN:     return "";
    case OPSTEM_SIGNPLUS:       return "+";
    case OPSTEM_SIGNMINUS:      return "-";
    case OPSTEM_BIT_NOT:        return "~";
    case OPSTEM_LOGICAL_NOT:    return "!";
    case OPSTEM_MULT:           return "*";
    case OPSTEM_DIV:            return "/";
    case OPSTEM_MOD:            return "%";
    case OPSTEM_ADD:            return "+";
    case OPSTEM_SUB:            return "-";
    case OPSTEM_SHIFT_LEFT:     return "<<";
    case OPSTEM_SHIFT_RIGHT:    return ">>";
    case OPSTEM_LESS:           return "<";
    case OPSTEM_GREATER:        return ">";
    case OPSTEM_LESS_EQUAL:     return "<=";
    case OPSTEM_GREATER_EQUAL:  return ">=";
    case OPSTEM_EQUAL:          return "==";
    case OPSTEM_NOT_EQUAL:      return "!=";
    // case OPSTEM_IN:          return "in";
    case OPSTEM_AND:            return "&";
    case OPSTEM_XOR:            return "^";
    case OPSTEM_OR:             return "|";
    case OPSTEM_LOGICAL_AND:    return "&&";
    case OPSTEM_LOGICAL_OR:     return "||";
    case OPSTEM_ADD_ASSIGN:     return "+=";
    case OPSTEM_SUB_ASSIGN:     return "-=";
    case OPSTEM_MULT_ASSIGN:    return "*=";
    case OPSTEM_DIV_ASSIGN:     return "/=";
    case OPSTEM_MOD_ASSIGN:     return "%=";
    case OPSTEM_SHL_ASSIGN:     return "<<=";
    case OPSTEM_SHR_ASSIGN:     return ">>=";
    case OPSTEM_AND_ASSIGN:     return "&=";
    case OPSTEM_XOR_ASSIGN:     return "^=";
    case OPSTEM_OR_ASSIGN:      return "|=";
    case OPSTEM_DOT_ASSIGN:     return ".=";
    case OPSTEM_ASSIGN:         return "=";
    case OPSTEM_END: return "";
    }
    return "<unknown opstem>";
}


opstem_class                    // a grouping of operations into classes
opstem_to_class(opstem stem)
{
    switch (stem)
    {
    case OPSTEM_NOOP:          return OPSTEMCL_OTHER;
    case OPSTEM_TRAP:          return OPSTEMCL_OTHER;
    case OPSTEM_LABEL:         return OPSTEMCL_OTHER;
    case OPSTEM_JMP:           return OPSTEMCL_OTHER;
    case OPSTEM_JMPTRUE:       return OPSTEMCL_OTHER;
    case OPSTEM_JMPFALSE:      return OPSTEMCL_OTHER;
    case OPSTEM_JMPZERO:       return OPSTEMCL_OTHER;
    case OPSTEM_JMPNONZERO:    return OPSTEMCL_OTHER;
    case OPSTEM_JMPBREAK:      return OPSTEMCL_OTHER;
    case OPSTEM_JMPCONTINUE:   return OPSTEMCL_OTHER;
    case OPSTEM_JMPRETURN:     return OPSTEMCL_OTHER;
    case OPSTEM_JMPEXCEPT:     return OPSTEMCL_OTHER;
    case OPSTEM_JMPANYUWFLAG:  return OPSTEMCL_OTHER;
    case OPSTEM_JMPNOUWFLAG:   return OPSTEMCL_OTHER;
    case OPSTEM_CLEARUW:       return OPSTEMCL_OTHER;
    case OPSTEM_SETBREAK:      return OPSTEMCL_OTHER;
    case OPSTEM_SETCONTINUE:   return OPSTEMCL_OTHER;
    case OPSTEM_SETRETURN:     return OPSTEMCL_OTHER;
    case OPSTEM_SETEXCEPT:     return OPSTEMCL_OTHER;
    case OPSTEM_CLEARBREAK:    return OPSTEMCL_OTHER;
    case OPSTEM_CLEARCONTINUE: return OPSTEMCL_OTHER;
    case OPSTEM_CLEARRETURN:   return OPSTEMCL_OTHER;
    case OPSTEM_CLEAREXCEPT:   return OPSTEMCL_OTHER;
    case OPSTEM_DESTRUCT:      return OPSTEMCL_OTHER;
    case OPSTEM_CONSTRUCT:     return OPSTEMCL_OTHER;
    case OPSTEM_COPYLIT:       return OPSTEMCL_OTHER;
    case OPSTEM_COPYMEM:       return OPSTEMCL_OTHER;
    case OPSTEM_COPYOBJ:       return OPSTEMCL_OTHER;
    case OPSTEM_MOVEMEM:       return OPSTEMCL_OTHER;
    case OPSTEM_MOVEOBJ:       return OPSTEMCL_OTHER;
    case OPSTEM_TRSFMEM:       return OPSTEMCL_OTHER;
    case OPSTEM_TRSFOBJ:       return OPSTEMCL_OTHER;
    case OPSTEM_MOVETOFN:      return OPSTEMCL_OTHER;
    case OPSTEM_MOVEFROMFN:    return OPSTEMCL_OTHER;

    case OPSTEM_SIGNPLUS:        return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SIGNMINUS:       return OPSTEMCL_ARITHMETIC;
    case OPSTEM_BIT_NOT:         return OPSTEMCL_BITWISE;
    case OPSTEM_LOGICAL_NOT:     return OPSTEMCL_LOGICAL;

    case OPSTEM_MULT:            return OPSTEMCL_ARITHMETIC;
    case OPSTEM_DIV:             return OPSTEMCL_ARITHMETIC;
    case OPSTEM_MOD:             return OPSTEMCL_ARITHMETIC;
    case OPSTEM_ADD:             return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SUB:             return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SHIFT_LEFT:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SHIFT_RIGHT:     return OPSTEMCL_ARITHMETIC;
    case OPSTEM_LESS:            return OPSTEMCL_ARITHMETIC;
    case OPSTEM_GREATER:         return OPSTEMCL_ARITHMETIC;
    case OPSTEM_LESS_EQUAL:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_GREATER_EQUAL:   return OPSTEMCL_ARITHMETIC;
    case OPSTEM_EQUAL:           return OPSTEMCL_ARITHMETIC;
    case OPSTEM_NOT_EQUAL:       return OPSTEMCL_ARITHMETIC;
    case OPSTEM_AND:             return OPSTEMCL_BITWISE;
    case OPSTEM_XOR:             return OPSTEMCL_BITWISE;
    case OPSTEM_OR:              return OPSTEMCL_BITWISE;
    case OPSTEM_LOGICAL_AND:     return OPSTEMCL_LOGICAL;
    case OPSTEM_LOGICAL_OR:      return OPSTEMCL_LOGICAL;
    case OPSTEM_ADD_ASSIGN:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SUB_ASSIGN:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_MULT_ASSIGN:     return OPSTEMCL_ARITHMETIC;
    case OPSTEM_DIV_ASSIGN:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_MOD_ASSIGN:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SHL_ASSIGN:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_SHR_ASSIGN:      return OPSTEMCL_ARITHMETIC;
    case OPSTEM_AND_ASSIGN:      return OPSTEMCL_BITWISE;
    case OPSTEM_XOR_ASSIGN:      return OPSTEMCL_BITWISE;
    case OPSTEM_OR_ASSIGN:       return OPSTEMCL_BITWISE;
    case OPSTEM_DOT_ASSIGN:      return OPSTEMCL_OTHER;
    case OPSTEM_ASSIGN:          return OPSTEMCL_OTHER;

    case OPSTEM_END: return OPSTEMCL_OTHER;
    }
    return OPSTEMCL_NONE;
}


bool                            // whether operation assigns
opstem_assigns(opstem stem)
{
    switch (stem)
    {
    case OPSTEM_NOOP:            return false;
    case OPSTEM_TRAP:            return false;
    case OPSTEM_LABEL:           return false;
    case OPSTEM_JMP:             return false;
    case OPSTEM_JMPTRUE:         return false;
    case OPSTEM_JMPFALSE:        return false;
    case OPSTEM_JMPZERO:         return false;
    case OPSTEM_JMPNONZERO:      return false;
    case OPSTEM_JMPBREAK:        return false;
    case OPSTEM_JMPCONTINUE:     return false;
    case OPSTEM_JMPRETURN:       return false;
    case OPSTEM_JMPEXCEPT:       return false;
    case OPSTEM_JMPANYUWFLAG:    return false;
    case OPSTEM_JMPNOUWFLAG:     return false;
    case OPSTEM_CLEARUW:         return false;
    case OPSTEM_SETBREAK:        return false;
    case OPSTEM_SETCONTINUE:     return false;
    case OPSTEM_SETRETURN:       return false;
    case OPSTEM_SETEXCEPT:       return false;
    case OPSTEM_CLEARBREAK:      return false;
    case OPSTEM_CLEARCONTINUE:   return false;
    case OPSTEM_CLEARRETURN:     return false;
    case OPSTEM_CLEAREXCEPT:     return false;
    case OPSTEM_DESTRUCT:        return false;
    case OPSTEM_CONSTRUCT:       return false;
    case OPSTEM_COPYLIT:         return false;
    case OPSTEM_COPYMEM:         return false;
    case OPSTEM_COPYOBJ:         return false;
    case OPSTEM_MOVEMEM:         return false;
    case OPSTEM_MOVEOBJ:         return false;
    case OPSTEM_TRSFMEM:         return false;
    case OPSTEM_TRSFOBJ:         return false;
    case OPSTEM_MOVETOFN:        return false;
    case OPSTEM_MOVEFROMFN:      return false;
    case OPSTEM_SIGNPLUS:        return false;
    case OPSTEM_SIGNMINUS:       return false;
    case OPSTEM_BIT_NOT:         return false;
    case OPSTEM_LOGICAL_NOT:     return false;
    case OPSTEM_MULT:            return false;
    case OPSTEM_DIV:             return false;
    case OPSTEM_MOD:             return false;
    case OPSTEM_ADD:             return false;
    case OPSTEM_SUB:             return false;
    case OPSTEM_SHIFT_LEFT:      return false;
    case OPSTEM_SHIFT_RIGHT:     return false;
    case OPSTEM_LESS:            return false;
    case OPSTEM_GREATER:         return false;
    case OPSTEM_LESS_EQUAL:      return false;
    case OPSTEM_GREATER_EQUAL:   return false;
    case OPSTEM_EQUAL:           return false;
    case OPSTEM_NOT_EQUAL:       return false;
    case OPSTEM_AND:             return false;
    case OPSTEM_XOR:             return false;
    case OPSTEM_OR:              return false;
    case OPSTEM_LOGICAL_AND:     return false;
    case OPSTEM_LOGICAL_OR:      return false;
    case OPSTEM_ADD_ASSIGN:      return true;
    case OPSTEM_SUB_ASSIGN:      return true;
    case OPSTEM_MULT_ASSIGN:     return true;
    case OPSTEM_DIV_ASSIGN:      return true;
    case OPSTEM_MOD_ASSIGN:      return true;
    case OPSTEM_SHL_ASSIGN:      return true;
    case OPSTEM_SHR_ASSIGN:      return true;
    case OPSTEM_AND_ASSIGN:      return true;
    case OPSTEM_XOR_ASSIGN:      return true;
    case OPSTEM_OR_ASSIGN:       return true;
    case OPSTEM_DOT_ASSIGN:      return true;
    case OPSTEM_ASSIGN:          return true;

    case OPSTEM_END: return false;
    }
    return false;
}


int                             // number of arguments of operation
opstem_nargs(opstem stem)
{
    switch (stem)
    {
    case OPSTEM_NOOP:          return 0;
    case OPSTEM_TRAP:          return 0;
    case OPSTEM_LABEL:         return 0;
    case OPSTEM_JMP:           return 0;
    case OPSTEM_JMPTRUE:       return 0;
    case OPSTEM_JMPFALSE:      return 0;
    case OPSTEM_JMPZERO:       return 0;
    case OPSTEM_JMPNONZERO:    return 0;
    case OPSTEM_JMPBREAK:      return 0;
    case OPSTEM_JMPCONTINUE:   return 0;
    case OPSTEM_JMPRETURN:     return 0;
    case OPSTEM_JMPEXCEPT:     return 0;
    case OPSTEM_JMPANYUWFLAG:  return 0;
    case OPSTEM_JMPNOUWFLAG:   return 0;
    case OPSTEM_CLEARUW:       return 0;
    case OPSTEM_SETBREAK:      return 0;
    case OPSTEM_SETCONTINUE:   return 0;
    case OPSTEM_SETRETURN:     return 0;
    case OPSTEM_SETEXCEPT:     return 0;
    case OPSTEM_CLEARBREAK:    return 0;
    case OPSTEM_CLEARCONTINUE: return 0;
    case OPSTEM_CLEARRETURN:   return 0;
    case OPSTEM_CLEAREXCEPT:   return 0;
    case OPSTEM_DESTRUCT:      return 1;
    case OPSTEM_CONSTRUCT:     return 1;
    case OPSTEM_COPYLIT:       return 2;
    case OPSTEM_COPYMEM:       return 2;
    case OPSTEM_COPYOBJ:       return 2;
    case OPSTEM_MOVEMEM:       return 2;
    case OPSTEM_MOVEOBJ:       return 2;
    case OPSTEM_TRSFMEM:       return 2;
    case OPSTEM_TRSFOBJ:       return 2;
    case OPSTEM_MOVETOFN:      return 2;
    case OPSTEM_MOVEFROMFN:    return 2;
    case OPSTEM_SIGNPLUS:        return 1;
    case OPSTEM_SIGNMINUS:       return 1;
    case OPSTEM_BIT_NOT:         return 1;
    case OPSTEM_LOGICAL_NOT:     return 1;
    case OPSTEM_MULT:            return 2;
    case OPSTEM_DIV:             return 2;
    case OPSTEM_MOD:             return 2;
    case OPSTEM_ADD:             return 2;
    case OPSTEM_SUB:             return 2;
    case OPSTEM_SHIFT_LEFT:      return 2;
    case OPSTEM_SHIFT_RIGHT:     return 2;
    case OPSTEM_LESS:            return 2;
    case OPSTEM_GREATER:         return 2;
    case OPSTEM_LESS_EQUAL:      return 2;
    case OPSTEM_GREATER_EQUAL:   return 2;
    case OPSTEM_EQUAL:           return 2;
    case OPSTEM_NOT_EQUAL:       return 2;
    case OPSTEM_AND:             return 2;
    case OPSTEM_XOR:             return 2;
    case OPSTEM_OR:              return 2;
    case OPSTEM_LOGICAL_AND:     return 2;
    case OPSTEM_LOGICAL_OR:      return 2;
    case OPSTEM_ADD_ASSIGN:      return 2;
    case OPSTEM_SUB_ASSIGN:      return 2;
    case OPSTEM_MULT_ASSIGN:     return 2;
    case OPSTEM_DIV_ASSIGN:      return 2;
    case OPSTEM_MOD_ASSIGN:      return 2;
    case OPSTEM_SHL_ASSIGN:      return 2;
    case OPSTEM_SHR_ASSIGN:      return 2;
    case OPSTEM_AND_ASSIGN:      return 2;
    case OPSTEM_XOR_ASSIGN:      return 2;
    case OPSTEM_OR_ASSIGN:       return 2;
    case OPSTEM_DOT_ASSIGN:      return 2;
    case OPSTEM_ASSIGN:          return 2;

    case OPSTEM_END: return false;
    }
    return false;
}


bool                            // whether the result of operation is boolean
opstem_boolres(opstem stem)
{
    switch (stem)
    {
    case OPSTEM_NOOP:          return false;
    case OPSTEM_TRAP:          return false;
    case OPSTEM_LABEL:         return false;
    case OPSTEM_JMP:           return false;
    case OPSTEM_JMPTRUE:       return false;
    case OPSTEM_JMPFALSE:      return false;
    case OPSTEM_JMPZERO:       return false;
    case OPSTEM_JMPNONZERO:    return false;
    case OPSTEM_JMPBREAK:      return false;
    case OPSTEM_JMPCONTINUE:   return false;
    case OPSTEM_JMPRETURN:     return false;
    case OPSTEM_JMPEXCEPT:     return false;
    case OPSTEM_JMPANYUWFLAG:  return false;
    case OPSTEM_JMPNOUWFLAG:   return false;
    case OPSTEM_CLEARUW:       return false;
    case OPSTEM_SETBREAK:      return false;
    case OPSTEM_SETCONTINUE:   return false;
    case OPSTEM_SETRETURN:     return false;
    case OPSTEM_SETEXCEPT:     return false;
    case OPSTEM_CLEARBREAK:    return false;
    case OPSTEM_CLEARCONTINUE: return false;
    case OPSTEM_CLEARRETURN:   return false;
    case OPSTEM_CLEAREXCEPT:   return false;
    case OPSTEM_DESTRUCT:      return false;
    case OPSTEM_CONSTRUCT:     return false;
    case OPSTEM_COPYLIT:       return false;
    case OPSTEM_COPYMEM:       return false;
    case OPSTEM_COPYOBJ:       return false;
    case OPSTEM_MOVEMEM:       return false;
    case OPSTEM_MOVEOBJ:       return false;
    case OPSTEM_TRSFMEM:       return false;
    case OPSTEM_TRSFOBJ:       return false;
    case OPSTEM_MOVETOFN:      return false;
    case OPSTEM_MOVEFROMFN:    return false;

    case OPSTEM_SIGNPLUS:        return false;
    case OPSTEM_SIGNMINUS:       return false;
    case OPSTEM_BIT_NOT:         return false;
    case OPSTEM_LOGICAL_NOT:     return true;
    case OPSTEM_MULT:            return false;
    case OPSTEM_DIV:             return false;
    case OPSTEM_MOD:             return false;
    case OPSTEM_ADD:             return false;
    case OPSTEM_SUB:             return false;
    case OPSTEM_SHIFT_LEFT:      return false;
    case OPSTEM_SHIFT_RIGHT:     return false;
    case OPSTEM_LESS:            return true;
    case OPSTEM_GREATER:         return true;
    case OPSTEM_LESS_EQUAL:      return true;
    case OPSTEM_GREATER_EQUAL:   return true;
    case OPSTEM_EQUAL:           return true;
    case OPSTEM_NOT_EQUAL:       return true;

    case OPSTEM_AND:             return false;
    case OPSTEM_XOR:             return false;
    case OPSTEM_OR:              return false;
    case OPSTEM_LOGICAL_AND:     return true;
    case OPSTEM_LOGICAL_OR:      return true;
    case OPSTEM_ADD_ASSIGN:      return false;
    case OPSTEM_SUB_ASSIGN:      return false;
    case OPSTEM_MULT_ASSIGN:     return false;
    case OPSTEM_DIV_ASSIGN:      return false;
    case OPSTEM_MOD_ASSIGN:      return false;
    case OPSTEM_SHL_ASSIGN:      return false;
    case OPSTEM_SHR_ASSIGN:      return false;
    case OPSTEM_AND_ASSIGN:      return false;
    case OPSTEM_XOR_ASSIGN:      return false;
    case OPSTEM_OR_ASSIGN:       return false;
    case OPSTEM_DOT_ASSIGN:      return false;
    case OPSTEM_ASSIGN:          return false;

    case OPSTEM_END: return false;
    }
    return false;
}


char const *
oparg_cigname(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return "bool";
    case OPARG_U8: return "u8";
    case OPARG_U16: return "u16";
    case OPARG_U32: return "u32";
    case OPARG_U64: return "u64";
    case OPARG_I8: return "i8";
    case OPARG_I16: return "i16";
    case OPARG_I32: return "i32";
    case OPARG_I64: return "i64";
    case OPARG_STR: return "str";
    case OPARG_BYTES: return "bytes";
    case OPARG_STREXPR: return "strexpr";
    case OPARG_BYTESEXPR: return "bytesexpr";
    case OPARG_COMPOUND: return "compound";
    case OPARG_VOID: return "void";
    case OPARG_LIST: return "list";
    }
    return "<unknown oparg>";
}


char const *
oparg_name(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return "BOOL";
    case OPARG_U8: return "U8";
    case OPARG_U16: return "U16";
    case OPARG_U32: return "U32";
    case OPARG_U64: return "U64";
    case OPARG_I8: return "I8";
    case OPARG_I16: return "I16";
    case OPARG_I32: return "I32";
    case OPARG_I64: return "I64";
    case OPARG_STR: return "STR";
    case OPARG_BYTES: return "BYTES";
    case OPARG_STREXPR: return "STREXPR";
    case OPARG_BYTESEXPR: return "BYTESEXPR";
    case OPARG_COMPOUND: return "COMPOUND";
    case OPARG_VOID: return "VOID";
    case OPARG_LIST: return "LIST";
    }
    return "<unknown oparg>";
}


oparg   // oparg or OPARG_COMPOUND
oparg_from_name(char const *name)
{
    oparg a;
    for (a = static_cast<oparg>(0);
         a < OPARG_COMPOUND;
         a = static_cast<oparg>(a + 1))
    {
        if (std::strcmp(oparg_name(a),name) == 0)
            return a;
    }
    return OPARG_COMPOUND;
}


oparg_class
oparg_to_class(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return OPARGCL_BOOL;
    case OPARG_U8: return OPARGCL_INTEGRAL;
    case OPARG_U16: return OPARGCL_INTEGRAL;
    case OPARG_U32: return OPARGCL_INTEGRAL;
    case OPARG_U64: return OPARGCL_INTEGRAL;
    case OPARG_I8: return OPARGCL_INTEGRAL;
    case OPARG_I16: return OPARGCL_INTEGRAL;
    case OPARG_I32: return OPARGCL_INTEGRAL;
    case OPARG_I64: return OPARGCL_INTEGRAL;
    // case OPARG_F32: return OPARGCL_FLOAT;
    // case OPARG_F64: return OPARGCL_FLOAT;
    case OPARG_STR: return OPARGCL_STR;
    case OPARG_BYTES: return OPARGCL_BYTES;
    case OPARG_STREXPR: return OPARGCL_STR;
    case OPARG_BYTESEXPR: return OPARGCL_BYTES;
    case OPARG_COMPOUND: return OPARGCL_NONE;
    case OPARG_VOID: return OPARGCL_NONE;
    case OPARG_LIST: return OPARGCL_NONE;
    }
    return OPARGCL_NONE;
}


bool
oparg_is_signed(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return false;
    case OPARG_U8: return false;
    case OPARG_U16: return false;
    case OPARG_U32: return false;
    case OPARG_U64: return false;
    case OPARG_I8: return true;
    case OPARG_I16: return true;
    case OPARG_I32: return true;
    case OPARG_I64: return true;
    // case OPARG_F32: return false;
    // case OPARG_F64: return false;
    case OPARG_STR: return false;
    case OPARG_BYTES: return false;
    case OPARG_STREXPR: return false;
    case OPARG_BYTESEXPR: return false;
    case OPARG_COMPOUND: return false;
    case OPARG_VOID: return false;
    case OPARG_LIST: return false;
    }
    return false;
}


int
oparg_bitsize(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return 1;
    case OPARG_U8: return 8;
    case OPARG_U16: return 16;
    case OPARG_U32: return 32;
    case OPARG_U64: return 64;
    case OPARG_I8: return 8;
    case OPARG_I16: return 16;
    case OPARG_I32: return 32;
    case OPARG_I64: return 64;
    // case OPARG_F32: return 32;
    // case OPARG_F64: return 64;
    case OPARG_STR: return 0;
    case OPARG_BYTES: return 0;
    case OPARG_STREXPR: return 0;
    case OPARG_BYTESEXPR: return 0;
    case OPARG_COMPOUND: return 0;
    case OPARG_VOID: return 0;
    case OPARG_LIST: return 0;
    }
    return 0;
}


bool                            // whether arg is trivially copyable (memcpy)
oparg_is_trivial(oparg arg)
{
    switch (arg)
    {
    case OPARG_BOOL: return true;
    case OPARG_U8: return true;
    case OPARG_U16: return true;
    case OPARG_U32: return true;
    case OPARG_U64: return true;
    case OPARG_I8: return true;
    case OPARG_I16: return true;
    case OPARG_I32: return true;
    case OPARG_I64: return true;
    // case OPARG_F32: return true;
    // case OPARG_F64: return true;
    case OPARG_STR: return false;
    case OPARG_BYTES: return false;
    case OPARG_STREXPR: return false;
    case OPARG_BYTESEXPR: return false;
    case OPARG_COMPOUND: return false;
    case OPARG_VOID: return false;
    case OPARG_LIST: return false;
    }
    return false;
}






// This function is parallel to cig_ast_builtin.cpp:unary_expression_type.
//
// As a difference, however, this function uses an extended type set
// where strexpr and bytesexpr are used as intermediate types
// for efficiency.
//
// Instead of specific errors, this function returns OPARG_COMPOUND.
oparg                           // result type, or OPARG_COMPOUND
oparg_unary_result_type(
    opstem stem,                // The operation
    oparg arg                   // The argument type
    )
{
    opstem_class opclass = opstem_to_class(stem);

    // Range check
    if (arg < 0 || arg >= OPARG_COMPOUND)
        return OPARG_COMPOUND;
    if (stem < OPSTEM_SIGNPLUS || stem >= OPSTEM_MULT)
        return OPARG_COMPOUND;


    // No arithmetic minus sign or bitwise not operation allowed
    // with the 'bool' type
    if (arg == OPARG_BOOL)
    {
        // Bitwise and logical operations are allowed,
        // all others are not
        if ((opclass != OPSTEMCL_ARITHMETIC ||
             stem != OPSTEM_SIGNPLUS) &&
            (opclass != OPSTEMCL_BITWISE ||
             stem == OPSTEM_BIT_NOT) &&
            opclass != OPSTEMCL_LOGICAL)
            return OPARG_COMPOUND;

        return OPARG_BOOL;
    }

    // Logical operations need bool type
    // except logical not ! which is allowed for all builtint data types
    if (opclass == OPSTEMCL_LOGICAL &&
        stem != OPSTEM_SIGNPLUS)
        return OPARG_COMPOUND;

    // Operations on integral types
    if (oparg_to_class(arg) == OPARGCL_INTEGRAL)
    {
        // Arithmetic and bitwise operations are allowed, all others are not
        if (opclass != OPSTEMCL_ARITHMETIC &&
            opclass != OPSTEMCL_BITWISE &&
            stem != OPSTEM_LOGICAL_NOT)
            return OPARG_COMPOUND;

        // Pick the correct result

        // If result is bool, we are done
        if (opstem_boolres(stem))
            return OPARG_BOOL;

        return arg;
    }

    // Bitwise operations need integral type
    if (opclass == OPSTEMCL_BITWISE)
        return OPARG_COMPOUND;

    // Operations on float types
    if (oparg_to_class(arg) == OPARGCL_FLOAT)
    {
        // Arithmetic operations are allowed, all others are not
        if (opclass != OPSTEMCL_ARITHMETIC)
            return OPARG_COMPOUND;

        // Pick the correct result

        // If result is bool, we are done
        if (opstem_boolres(stem))
            return OPARG_BOOL;

        return arg;
    }


    // Operations with strings
    if (arg == OPARG_STR || arg == OPARG_STREXPR)
    {
        // Unary + (plus sign)
        if (stem == OPSTEM_SIGNPLUS)
            return OPARG_STREXPR;

        // Unary ! (logical not -> is_empty())
        if (stem == OPSTEM_LOGICAL_NOT)
            return OPARG_BOOL;
    }


    // Operations with bytes sequences
    if (arg == OPARG_BYTES || arg == OPARG_BYTESEXPR)
    {
        // Unary + (plus sign)
        if (stem == OPSTEM_SIGNPLUS)
            return OPARG_BYTESEXPR;

        // Unary ! (logical not -> is_empty())
        if (stem == OPSTEM_LOGICAL_NOT)
            return OPARG_BOOL;
    }

    return OPARG_COMPOUND;
}


// This function is parallel to cig_ast_builtin.cpp:binary_expression_type.
//
// As a difference, however, this function uses an extended type set
// where strexpr and bytesexpr are used as intermediate types
// for efficiency.
//
// Instead of specific errors, this function returns OPARG_COMPOUND.
oparg                           // result type (or VOID), or OPARG_COMPOUND
oparg_binary_result_type(
    opstem stem,                // The operation
    oparg arg1,                 // Type of argument 1
    oparg arg2                  // Type of argument 2
    )
{
    opstem_class opclass = opstem_to_class(stem);
    int assigns = opstem_assigns(stem);

    // Range check
    if (arg1 < 0 || arg1 >= OPARG_COMPOUND)
        return OPARG_COMPOUND;
    if (arg2 < 0 || arg2 >= OPARG_COMPOUND)
        return OPARG_COMPOUND;

    // // Not yet implemented: operator 'in'
    // if (stem == OPSTEM_IN)
    //     return OPARG_COMPOUND;
    // Not yet implemented: operator '.='
    if (stem == OPSTEM_DOT_ASSIGN)
        return OPARG_COMPOUND;

    // No arithmetic operations allowed with the 'bool' type
    if (arg1 == OPARG_BOOL ||
        arg2 == OPARG_BOOL)
    {
        // Assignments
        if (assigns)
        {
            // Arithmetic assignment is not allowed
            if (opclass == OPSTEMCL_ARITHMETIC)
                return OPARG_COMPOUND;
            // Logical-assign, dot-assign and simple assign are allowed


            // Other assignments allowed if assigned is bool
            // and assigned-to is bool,integer,float type
            if (arg2 != OPARG_BOOL)
                return OPARG_COMPOUND;

            // Can assign to bool,integer,float
            if (oparg_to_class(arg1) == OPARGCL_BOOL ||
                oparg_to_class(arg1) == OPARGCL_FLOAT ||
                oparg_to_class(arg1) == OPARGCL_INTEGRAL)
                return OPARG_VOID; // allowed

            // To all other types, cannot assign a bool
            return OPARG_COMPOUND;
        }

        // Bitwise and logical operations are allowed,
        // all others are not
        if (opclass != OPSTEMCL_BITWISE &&
            opclass != OPSTEMCL_LOGICAL)
            return OPARG_COMPOUND;

        // For non-assignment, both types must be bool
        if (arg1 != OPARG_BOOL ||
            arg2 != OPARG_BOOL)
            return OPARG_COMPOUND;

        return OPARG_BOOL;
    }

    // Logical operations need bool type
    if (opclass == OPSTEMCL_LOGICAL)
        return OPARG_COMPOUND;

    // Operations between two integral types
    if (oparg_to_class(arg1) == OPARGCL_INTEGRAL &&
        oparg_to_class(arg2) == OPARGCL_INTEGRAL)
    {
        // Assignments
        if (assigns)
        {
            // Assignment to smaller type not allowed
            if (oparg_bitsize(arg1) < oparg_bitsize(arg2))
                return OPARG_COMPOUND;
            if (oparg_bitsize(arg1) == oparg_bitsize(arg2) &&
                oparg_is_signed(arg1) && !oparg_is_signed(arg2))
                return OPARG_COMPOUND;

            // All other assignments allowed
            return OPARG_VOID;
        }

        // Arithmetic and bitwise operations are allowed, all others are not
        if (opclass != OPSTEMCL_ARITHMETIC &&
            opclass != OPSTEMCL_BITWISE)
            return OPARG_COMPOUND;

        // Pick the correct result

        // If result is bool, we are done
        if (opstem_boolres(stem))
            return OPARG_BOOL;

        // Choose the greater bitsize
        if (oparg_bitsize(arg1) > oparg_bitsize(arg2))
            return arg1;
        if (oparg_bitsize(arg1) < oparg_bitsize(arg2))
            return arg2;

        // Choose the unsigned
        if (!oparg_is_signed(arg1))
            return arg1;
        if (!oparg_is_signed(arg2))
            return arg2;

        // The types must be the same
        if (arg1 != arg2)
            return OPARG_COMPOUND;
        return arg1;
    }

    // Bitwise operations need integral type
    if (opclass == OPSTEMCL_BITWISE)
        return OPARG_COMPOUND;

    // Operations between integral or float types
    if ((oparg_to_class(arg1) == OPARGCL_INTEGRAL ||
         oparg_to_class(arg1) == OPARGCL_FLOAT) &&
        (oparg_to_class(arg2) == OPARGCL_INTEGRAL ||
         oparg_to_class(arg2) == OPARGCL_FLOAT))
    {
        // The int-int pairing has been handled above
        if (oparg_to_class(arg1) != OPARGCL_FLOAT &&
            oparg_to_class(arg2) != OPARGCL_FLOAT)
            return OPARG_COMPOUND;

        // Assignments
        if (assigns)
        {
            // Assignment of float to integral not allowed
            if (oparg_to_class(arg1) == OPARGCL_INTEGRAL)
                return OPARG_COMPOUND;

            // Assignment to smaller type not allowed
            if (oparg_to_class(arg2) == OPARGCL_FLOAT)
            {
                if (oparg_bitsize(arg1) < oparg_bitsize(arg2))
                    return OPARG_COMPOUND;
            }
            // else arg2 is an int, can be assigned to any float

            // All other assignments allowed
            return OPARG_VOID;
        }

        // Arithmetic operations are allowed, all others are not
        if (opclass != OPSTEMCL_ARITHMETIC)
            return OPARG_COMPOUND;

        // Pick the correct result

        // If result is bool, we are done
        if (opstem_boolres(stem))
            return OPARG_BOOL;

        // Choose the greater bitsize
        if (oparg_bitsize(arg1) > oparg_bitsize(arg2))
            return arg1;
        if (oparg_bitsize(arg1) < oparg_bitsize(arg2))
            return arg2;

        // The types must be the same
        if (arg1 != arg2)
            return OPARG_COMPOUND;
        return arg1;
    }


    // Operations with strings
    if (arg1 == OPARG_STR || arg1 == OPARG_STREXPR ||
        arg2 == OPARG_STR || arg2 == OPARG_STREXPR)
    {
        // Here is a difference from cig_ast_builtin.cpp:binary_expressino_type
        // in that we also handle strexpr.

        // Only a few operations are possible with strings
        if (stem == OPSTEM_ADD)
        {
            // Both must be str or strexpr
            if (!(arg1 == OPARG_STR || arg1 == OPARG_STREXPR) ||
                !(arg2 == OPARG_STR || arg2 == OPARG_STREXPR))
                return OPARG_COMPOUND;

            return OPARG_STREXPR;
        }
        else if (stem == OPSTEM_MULT)
        {
            // The other operand must be an int but not u64
            if ((oparg_to_class(arg1) == OPARGCL_INTEGRAL && arg1 != OPARG_U64) ||
                (oparg_to_class(arg2) == OPARGCL_INTEGRAL && arg2 != OPARG_U64))
                return OPARG_STREXPR;

            return OPARG_COMPOUND;
        }
        else if (stem == OPSTEM_ADD_ASSIGN ||
                 stem == OPSTEM_DOT_ASSIGN ||
                 stem == OPSTEM_ASSIGN)
        {
            if (arg1 != OPARG_STR)
                return OPARG_COMPOUND;
            if (!(arg2 == OPARG_STR || arg2 == OPARG_STREXPR))
                return OPARG_COMPOUND;
            return OPARG_VOID;
        }
        // String comparisions: ==, != , >, >=, <, <=
        else if (opclass == OPSTEMCL_ARITHMETIC &&
                 assigns &&
                 opstem_boolres(stem))
        {
            // Both must be str or strexpr
            if (!(arg1 == OPARG_STR || arg1 == OPARG_STREXPR) ||
                !(arg2 == OPARG_STR || arg2 == OPARG_STREXPR))
                return OPARG_COMPOUND;

            return OPARG_BOOL;
        }
    }


    // Operations with bytes sequences
    if (arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR ||
        arg2 == OPARG_BYTES || arg2 == OPARG_BYTESEXPR)
    {
        // Here is a difference from cig_ast_builtin.cpp:binary_expressino_type
        // in that we also handle bytesexpr.

        // Only a few operations are possible with bytes sequences
        if (stem == OPSTEM_ADD)
        {
            // Both must be bytes or bytesexpr
            if (!(arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR) ||
                !(arg2 == OPARG_BYTES || arg2 == OPARG_BYTESEXPR))
                return OPARG_COMPOUND;

            return OPARG_BYTESEXPR;
        }
        else if (stem == OPSTEM_MULT)
        {
            // The other operand must be an int but not u64
            if ((oparg_to_class(arg1) == OPARGCL_INTEGRAL && arg1 != OPARG_U64) ||
                (oparg_to_class(arg2) == OPARGCL_INTEGRAL && arg2 != OPARG_U64))
                return OPARG_BYTESEXPR;

            return OPARG_COMPOUND;
        }
        else if (stem == OPSTEM_ADD_ASSIGN ||
                 stem == OPSTEM_DOT_ASSIGN ||
                 stem == OPSTEM_ASSIGN)
        {
            if (arg1 != OPARG_BYTES)
                return OPARG_COMPOUND;
            if (!(arg2 == OPARG_BYTES || arg2 == OPARG_BYTESEXPR))
                return OPARG_COMPOUND;
            return OPARG_VOID;
        }
        // Byte sequence comparisions: ==, != , >, >=, <, <=
        else if (opclass == OPSTEMCL_ARITHMETIC &&
                 assigns &&
                 opstem_boolres(stem))
        {
            // Both must be bytes or bytesexpr
            if (!(arg1 == OPARG_BYTES || arg1 == OPARG_BYTESEXPR) ||
                !(arg2 == OPARG_BYTES || arg2 == OPARG_BYTESEXPR))
                return OPARG_COMPOUND;

            return OPARG_BOOL;
        }
    }

    return OPARG_COMPOUND;
}





} // namespace interpreter

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
