/* cig_selftest_interpreter.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Selftest for interpreter for the CIG language.
 */
#include "cig_interpreter_ops_and_types.h"
#include "cig_ast_builtin.h"


char const *cig_interpreter_selftest()
{
    using namespace cig::interpreter;
    using namespace cig::ast;

    static_assert(OPARG_BOOL  == static_cast<int>(AST_BUILTIN_BOOL ));
    static_assert(OPARG_U8    == static_cast<int>(AST_BUILTIN_U8   ));
    static_assert(OPARG_U16   == static_cast<int>(AST_BUILTIN_U16  ));
    static_assert(OPARG_U32   == static_cast<int>(AST_BUILTIN_U32  ));
    static_assert(OPARG_U64   == static_cast<int>(AST_BUILTIN_U64  ));
    static_assert(OPARG_I8    == static_cast<int>(AST_BUILTIN_I8   ));
    static_assert(OPARG_I16   == static_cast<int>(AST_BUILTIN_I16  ));
    static_assert(OPARG_I32   == static_cast<int>(AST_BUILTIN_I32  ));
    static_assert(OPARG_I64   == static_cast<int>(AST_BUILTIN_I64  ));
    static_assert(OPARG_STR   == static_cast<int>(AST_BUILTIN_STR  ));
    static_assert(OPARG_BYTES == static_cast<int>(AST_BUILTIN_BYTES));

    return 0;
}


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
