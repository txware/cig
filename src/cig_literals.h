/* cig_literals.h
 *
 * Copyright 2022-2024 Claus Fischer
 *
 * Runtime type literals definitions for cig.
 *
 * Note: This file defines the literal operators to embed CIG literals
 *       in C++ code. For the data type to store literals encountered
 *       when parsing CIG source code, see cig_ast_literal.h.
 */
#ifndef CIG_LITERALS_H_INCLUDED
#define CIG_LITERALS_H_INCLUDED

#include "cig_defines.h"        // CIG_BIG_ENDIAN,CIG_LITTLE_ENDIAN
#include "cig_compatibility.h"  // CIG_RESTRICT,std::bit_cast
#include "cig_constexpr_digits.h"
#include "cig_constexpr_utf8.h"
#include "cig_datatypes.h"
#include "cig_assert.h"         // cig_assert,cig_static_assert

#include <algorithm>            // std::copy
#include <cstddef>              // std::size_t
#include <cstdint>              // uint8_t ...
#include <limits>               // std::numeric_limits
#include <stdexcept>            // std::invalid_argument
#include <type_traits>          // std::make_unsigned
#include <bit>                  // std::bit_cast



namespace cig {


namespace detail {


//
// Integer literals
//


template<std::size_t N>
constexpr uint64_t digits_to_uint64(std::array<char,N> a)
{
    constexpr uint64_t maxval = std::numeric_limits<uint64_t>::max();
    constexpr uint64_t max10 = maxval / 10;
    constexpr int rem10 = static_cast<int>(maxval - 10 * max10);
    constexpr uint64_t max16 = maxval / 16;
    constexpr int rem16 = static_cast<int>(maxval - 16 * max16);
    constexpr uint64_t max8 = maxval / 8;
    constexpr int rem8 = static_cast<int>(maxval - 8 * max8);
    constexpr uint64_t max2 = maxval / 2;
    constexpr int rem2 = static_cast<int>(maxval - 2 * max2);
    uint64_t x = 0;

    if (N == 0)
        cig_static_assert(false,"too few digits");
    if (a[0] != '0')            // decimal
    {
        for (std::size_t i = 0; i < N; i++)
        {
            if (!isdigit(a[i]))
                cig_static_assert(false,"not a digit");
            int d = detail::digitval(a[i]);
            if (x >= max10 &&
                (x > max10 || d > rem10))
            {
                cig_static_assert(false,"number too big");
            }
            x = 10 * x + static_cast<uint64_t>(d);
        }
    }
    else if (N == 1)            // just 0
        ;
    else if (a[1] == 'x' ||
             a[1] == 'X')       // hexadecimal
    {
        for (std::size_t i = 2; i < N; i++)
        {
            if (!isxdigit(a[i]))
                cig_static_assert(false,"not a hex digit");
            int d = detail::xdigitval(a[i]);
            if (x >= max16 &&
                (x > max16 || d > rem16))
            {
                cig_static_assert(false,"number too big");
            }
            x = 16 * x + static_cast<uint64_t>(d);
        }
    }
    else if (a[1] == 'b' ||
             a[1] == 'B')       // binary
    {
        for (std::size_t i = 2; i < N; i++)
        {
            if (a[i] != '0' && a[i] != 1)
                cig_static_assert(false,"not a binary digit");
            int d = (a[i] == 1);
            if (x >= max2)
            {
                if (x > max2 || d > rem2)
                    cig_static_assert(false,"number too big");
            }
            x = 2 * x + static_cast<uint64_t>(d);
        }
    }
    else                        // octal
    {
        for (std::size_t i = 1; i < N; i++)
        {
            if (!isodigit(a[i]))
                cig_static_assert(false,"not an octal digit");
            int d = detail::digitval(a[i]);
            if (x >= max8)
            {
                if (x > max8 || d > rem8)
                    cig_static_assert(false,"number too big");
            }
            x = 8 * x + static_cast<uint64_t>(d);
        }
    }
    return x;
}



// Class u8lit stores the value behind the u8
template<uint8_t V>
struct u8lit : u8
{
    constexpr u8lit() : u8(V) {}
};


// Class u16lit stores the value behind the u16
template<uint16_t V>
struct u16lit : u16
{
    constexpr u16lit() : u16(V) {}
};


// Class u32lit stores the value behind the u32
template<uint32_t V>
struct u32lit : u32
{
    constexpr u32lit() : u32(V) {}
};


// Class u64lit stores the value behind the u64
template<uint64_t V>
struct u64lit : u64
{
    constexpr u64lit() : u64(V) {}
};


// Class i8lit stores the value behind the i8
template<int8_t V>
struct i8lit : i8
{
    constexpr i8lit() : i8(V) {}
};


// Class i16lit stores the value behind the i16
template<int16_t V>
struct i16lit : i16
{
    constexpr i16lit() : i16(V) {}
};


// Class i32lit stores the value behind the i32
template<int32_t V>
struct i32lit : i32
{
    constexpr i32lit() : i32(V) {}
};


// Class i64lit stores the value behind the i64
template<int64_t V>
struct i64lit : i64
{
    constexpr i64lit() : i64(V) {}
};


} // namespace detail

template <char ... ch>
constexpr auto operator "" _u8()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= u8::max_value);
    return detail::u8lit<v>();
}

template <char ... ch>
constexpr auto operator "" _u16()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= u16::max_value);
    return detail::u16lit<v>();
}

template <char ... ch>
constexpr auto operator "" _u32()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= u32::max_value);
    return detail::u32lit<v>();
}

template <char ... ch>
constexpr auto operator "" _u64()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= u64::max_value);
    return detail::u64lit<v>();
}

template <char ... ch>
constexpr auto operator "" _i8()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= i8::max_value);
    return detail::i8lit<v>();
}

template <char ... ch>
constexpr auto operator "" _i16()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= i16::max_value);
    return detail::i16lit<v>();
}

template <char ... ch>
constexpr auto operator "" _i32()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= i32::max_value);
    return detail::i32lit<v>();
}

template <char ... ch>
constexpr auto operator "" _i64()
{
    constexpr std::array<char,sizeof...(ch)> digits { ch ... };
    constexpr auto v = detail::digits_to_uint64(digits);
    static_assert(v <= i64::max_value);
    return detail::i64lit<v>();
}



//
// String literals
//


namespace detail {


// Auxiliary function to convert std::array to strval::_long
template<typename Char,std::size_t N>
constexpr strval::_long strlit_array_to_long(std::array<Char,N> s)
{
    constexpr size_t bits = 8 * sizeof(Char);
    constexpr std::size_t count = sizeof(std::size_t) / sizeof(Char);
    strval::_long l { };
    std::size_t i;

    // For systems where std::bit_cast is not working as constexpr

    static_assert(sizeof(Char) * N == sizeof(l));

#   if defined(CIG_BIG_ENDIAN)
    for (i = 0; i < count; i++)
    {
        l._data |= static_cast<std::size_t>(s[        i]) << ((count-1-i)*bits);
        l._size |= static_cast<std::size_t>(s[  count+i]) << ((count-1-i)*bits);
        l._cap  |= static_cast<std::size_t>(s[2*count+i]) << ((count-1-i)*bits);
    }
#   elif defined(CIG_LITTLE_ENDIAN)
    for (i = 0; i < count; i++)
    {
        l._cap  |= static_cast<std::size_t>(s[        i]) << (i * bits);
        l._size |= static_cast<std::size_t>(s[  count+i]) << (i * bits);
        l._data |= static_cast<std::size_t>(s[2*count+i]) << (i * bits);
    }
#   else
#   error byte order not defined
#   endif
    return l;
}


// Class strlit uses an external character array without allocation
struct strlit : public strval {

    // constructor for constexpr literals (short strings only)
    template<typename Char,std::size_t N>
    constexpr strlit(std::array<Char,N> arr, bool isascii)
    {
        constexpr std::size_t size = N - 1;
        static_assert(
            sizeof(Char) == sizeof(ucs1_t) ||
            sizeof(Char) == sizeof(ucs2_t) ||
            sizeof(Char) == sizeof(ucs4_t));
        if (isascii && sizeof(Char) != sizeof(ucs1_t))
            cig_static_assert(false,"isascii must be used with ucs1_t");
        if constexpr (sizeof(Char) == sizeof(ucs4_t))
        {
            static_assert(size <= strval::_c32_len_net);
            std::array<strval::ucs4_t,strval::_c32_len> c32 {};
            std::copy(&arr[0],&arr[0]+size,&c32[strval::_c32_offset]);
            c32[strval::_c32_length_byte] =
                static_cast<strval::ucs4_t>(size << 3) |
                static_cast<strval::ucs4_t>(strval::_chartype_ucs4);
            _l = strlit_array_to_long(c32);
        }
        else if constexpr (sizeof(Char) == sizeof(ucs2_t))
        {
            static_assert(size <= strval::_c16_len_net);
            std::array<strval::ucs2_t,strval::_c16_len> c16 {};
            std::copy(&arr[0],&arr[0]+size,&c16[strval::_c16_offset]);
            c16[strval::_c16_length_byte] =
                static_cast<strval::ucs2_t>(size << 3) |
                static_cast<strval::ucs2_t>(strval::_chartype_ucs2);
            _l = strlit_array_to_long(c16);
        }
        else
        {
            static_assert(size <= strval::_c8_len_net);
            _chartype chartype = (isascii ?
                                  strval::_chartype_ascii :
                                  strval::_chartype_ucs1);
            std::array<strval::ucs1_t,strval::_c8_len> c8 {};
            std::copy(&arr[0],&arr[0]+size,&c8[strval::_c8_offset]);
            c8[strval::_c8_length_byte] =
                static_cast<strval::ucs1_t>(size << 3) |
                static_cast<strval::ucs1_t>(chartype);
            _l = strlit_array_to_long(c8);
        }
    }

    // constructor for runtime literals of chartype ascii or ucs1
    strlit(ucs1_t const *data,  // short or permanent with '\0'
           std::size_t size,    // net size without '\0'
           bool isascii)
    {
        _chartype chartype = (isascii ?
                              strval::_chartype_ascii :
                              strval::_chartype_ucs1);
        if (size <= strval::_c8_len_net)
        {
            // short string
            std::array<strval::ucs1_t,strval::_c8_len> c8 {};
            std::copy(data,data+size,&c8[strval::_c8_offset]);
            c8[strval::_c8_length_byte] =
                static_cast<strval::ucs1_t>(size << 3) |
                static_cast<strval::ucs1_t>(chartype);
            _l = strlit_array_to_long(c8);
        }
        else
        {
            // long string
            if (data[size])
                cig_static_assert(false,"strlit external string "
                                  "not null-terminated");
            _l._cap = _long_mask | static_cast<std::size_t>(chartype);
            _l._size = size;
            _l._data = std::bit_cast<std::size_t,ucs1_t*>(
                const_cast<ucs1_t *>(data));
        }
    }

    // constructor for runtime literals of chartype ucs2
    strlit(ucs2_t const *data,  // short or permanent with '\0'
           std::size_t size)    // net size without '\0'
    {
        _chartype chartype = strval::_chartype_ucs2;
        if (size <= strval::_c16_len_net)
        {
            // short string
            std::array<strval::ucs2_t,strval::_c16_len> c16 {};
            std::copy(data,data+size,&c16[strval::_c16_offset]);
            c16[strval::_c16_length_byte] =
                static_cast<strval::ucs2_t>(size << 3) |
                static_cast<strval::ucs2_t>(strval::_chartype_ucs2);
            _l = strlit_array_to_long(c16);
        }
        else
        {
            // long string
            if (data[size])
                cig_static_assert(false,"strlit external string "
                                  "not null-terminated");
            _l._cap = _long_mask | static_cast<std::size_t>(chartype);
            _l._size = size;
            _l._data = std::bit_cast<std::size_t,ucs2_t*>(
                const_cast<ucs2_t *>(data));
        }
    }

    // constructor for runtime literals of chartype ucs4
    strlit(ucs4_t const *data,  // short or permanent with '\0'
           std::size_t size)    // net size without '\0'
    {
        _chartype chartype = strval::_chartype_ucs4;
        if (size <= strval::_c32_len_net)
        {
            // short string
            std::array<strval::ucs4_t,strval::_c32_len> c32 {};
            std::copy(data,data+size,&c32[strval::_c32_offset]);
            c32[strval::_c32_length_byte] =
                static_cast<strval::ucs4_t>(size << 3) |
                static_cast<strval::ucs4_t>(strval::_chartype_ucs4);
            _l = strlit_array_to_long(c32);
        }
        else
        {
            // long string
            if (data[size])
                cig_static_assert(false,"strlit external string "
                                  "not null-terminated");
            _l._cap = _long_mask | static_cast<std::size_t>(chartype);
            _l._size = size;
            _l._data = std::bit_cast<std::size_t,ucs4_t*>(
                const_cast<ucs4_t *>(data));
        }
    }
};


// Class strlitraw encapsulates native C/C++-strings
template<typename Char,std::size_t N>
struct strlitraw
{
    std::array<Char,N> arr {};
    constexpr strlitraw(Char const (&pp)[N])
    {
	for (std::size_t i = 0; i < N; i++)
	    arr[i] = pp[i];
    };
};


// Class strlitholder stores the std::array behind the strlit
template<typename Char,std::size_t N,bool isascii,std::array<Char,N> arr>
struct strlitholder
{
    static constexpr bool is_ascii { isascii };
#if CIG_HAVE_CONSTEXPR
    static constexpr const std::array<Char,N> buf { arr };
#else
    static const std::array<Char,N> buf;
#endif

    CIG_CONSTEXPR explicit operator strlit()
        const noexcept; // convert to strlit
    // CIG_CONSTEXPR operator strval() const noexcept; // convert to strval
    CIG_CONSTEXPR operator str() const noexcept; // convert to str
    inline str to_str() const noexcept; // convert to str
};

#if ! CIG_HAVE_CONSTEXPR
template<typename Char,std::size_t N,bool isascii,std::array<Char,N> arr>
const std::array<Char,N> strlitholder<Char,N,isascii,arr>::buf = { arr };
#endif


// Function join_strlitholder_arrays concatenates two strlitholders
template<typename A, std::size_t NA,
	 typename B, std::size_t NB,
	 typename C, std::size_t NC>
constexpr auto join_strlitholder_arrays(std::array<A,NA> a,
                                        std::array<B,NB> b)
{
    std::array<C,NC> c { };
    std::size_t i;
    std::size_t j = 0;

    for (i = 0; i < NA-1; i++)
	c[j++] = a[i];
    for (i = 0; i < NB; i++)
	c[j++] = b[i];
    return c;
}


// Operator + concatenates two strlitolders
template<
    typename CharA,
    std::size_t NA,
    bool isasciiA,
    std::array<CharA,NA> arrA,
    typename CharB,
    std::size_t NB,
    bool isasciiB,
    std::array<CharB,NB> arrB>
constexpr auto operator + (
    [[maybe_unused]] strlitholder<CharA,NA,isasciiA,arrA> a,
    [[maybe_unused]] strlitholder<CharB,NB,isasciiB,arrB> b)
{
    constexpr bool isascii = isasciiA && isasciiB;
    constexpr std::size_t N = NA + NB - 1;
    if constexpr (sizeof(CharA) > sizeof(CharB))
    {
	constexpr std::array c =
            join_strlitholder_arrays<CharA,NA,CharB,NB,CharA,N>(arrA,arrB);
	return strlitholder<CharA,N,isascii,c>();
    }
    else
    {
	constexpr std::array c =
            join_strlitholder_arrays<CharA,NA,CharB,NB,CharB,N>(arrA,arrB);
	return strlitholder<CharB,N,isascii,c>();
    }
} // operator +


// Operator strlit converts a strlitholder to strlit
template<typename Char,std::size_t N,bool isascii,std::array<Char,N> arr>
CIG_CONSTEXPR strlitholder<Char,N,isascii,arr>::operator strlit() const noexcept
{
    if constexpr (isascii)
    {
        if constexpr (N-1 <= strval::_c8_len_net)
        {
            return strlit(arr,isascii); // short string
        }
        else
        {
            return strlit(&buf[0],N-1,isascii); // long string
        }
    }
    else if constexpr (sizeof(Char) == sizeof(strval::ucs1_t))
    {
        if constexpr (N-1 <= strval::_c8_len_net)
        {
            return strlit(arr,isascii); // short string
        }
        else
        {
            return strlit(&buf[0],N-1,isascii); // long string
        }
    }
    else if constexpr (sizeof(Char) == sizeof(strval::ucs2_t))
    {
        if constexpr (N-1 <= strval::_c16_len_net)
        {
            return strlit(arr,false); // short string
        }
        else
        {
            return strlit(&buf[0],N-1); // long string
        }
    }
    else
    {
        if constexpr (N-1 <= strval::_c32_len_net)
        {
            return strlit(arr,false); // short string
        }
        else
        {
            static_assert(sizeof(Char) == sizeof(strval::ucs4_t));
            return strlit(&buf[0],N-1); // long string
        }
    }
}


// // Operator strval converts a strlitholder to strval
// template<typename Char,std::size_t N,bool isascii,std::array<Char,N> arr>
// CIG_CONSTEXPR strlitholder<Char,N,isascii,arr>::operator strval() const noexcept
// {
//     return operator strlit();
// }


// Operator str converts a strlitholder to const str
template<typename Char,std::size_t N,bool isascii,std::array<Char,N> arr>
CIG_CONSTEXPR strlitholder<Char,N,isascii,arr>::operator str() const noexcept
{
    return str(operator strlit());
}


// Operator str converts a strlitholder to const str
template<typename Char,std::size_t N,bool isascii,std::array<Char,N> arr>
inline str strlitholder<Char,N,isascii,arr>::to_str() const noexcept
{
    return operator strlit();
}


} // namespace detail


// Operator ""_str creates a strlitholder from the _str suffix
template<detail::strlitraw A>
constexpr auto operator"" _str()
{
    constexpr auto s = A.arr;
    constexpr auto slen = cig_constexpr_decode_utf8_count(s); // ptrdiff_t
    // if < 0, one of cig_constexpr_utf8_error
    static_assert(slen >= 0,"bad UTF-8 input string");
    constexpr auto NO = static_cast<std::size_t>(slen);
    constexpr auto maxcp = cig_constexpr_decode_utf8_maxcp(s); // uint32_t
    constexpr auto chartype = detail::strval::_chartype_by_max_codepoint(maxcp);


    // Version for UCS-4
    if constexpr (chartype == detail::strval::_chartype_ucs4)
    {
	constexpr auto o =
            cig_constexpr_decode_utf8<detail::strval::ucs4_t,NO>(s);
	return detail::strlitholder<detail::strval::ucs4_t,NO,false,o>();
    }
    else if constexpr (chartype == detail::strval::_chartype_ucs2)
    {
	constexpr auto o =
            cig_constexpr_decode_utf8<detail::strval::ucs2_t,NO>(s);
	return detail::strlitholder<detail::strval::ucs2_t,NO,false,o>();
    }
    else if constexpr (chartype == detail::strval::_chartype_ucs1)
    {
	constexpr auto o =
            cig_constexpr_decode_utf8<detail::strval::ucs1_t,NO>(s);
	return detail::strlitholder<detail::strval::ucs1_t,NO,false,o>();
    }
    else
    {
	static_assert(chartype == detail::strval::_chartype_ascii);
	constexpr auto o =
            cig_constexpr_decode_utf8<detail::strval::ucs1_t,NO>(s);
	return detail::strlitholder<detail::strval::ucs1_t,NO,true,o>();
    }
} // operator "" _str()


//
// Bytes literals
//


namespace detail {


// Auxiliary function to convert std::array to bytesval::_long
template<typename Byte,std::size_t N>
constexpr bytesval::_long byteslit_array_to_long(std::array<Byte,N> s)
{
    constexpr size_t bits = 8 * sizeof(Byte);
    constexpr std::size_t count = sizeof(std::size_t) / sizeof(Byte);
    bytesval::_long l { };
    std::size_t i;

    // For systems where std::bit_cast is not working as constexpr

    static_assert(sizeof(Byte) * N == sizeof(l));

#   if defined(CIG_BIG_ENDIAN)
    for (i = 0; i < count; i++)
    {
        l._data |= static_cast<std::size_t>(s[        i]) << ((count-1-i)*bits);
        l._size |= static_cast<std::size_t>(s[  count+i]) << ((count-1-i)*bits);
        l._cap  |= static_cast<std::size_t>(s[2*count+i]) << ((count-1-i)*bits);
    }
#   elif defined(CIG_LITTLE_ENDIAN)
    for (i = 0; i < count; i++)
    {
        l._cap  |= static_cast<std::size_t>(s[        i]) << (i * bits);
        l._size |= static_cast<std::size_t>(s[  count+i]) << (i * bits);
        l._data |= static_cast<std::size_t>(s[2*count+i]) << (i * bits);
    }
#   else
#   error byte order not defined
#   endif
    return l;
}


// Class byteslit uses an external character array without allocation
struct byteslit : public bytesval {

    // constructor for constexpr literals (short arrays only)
    template<typename Byte,std::size_t N>
    CIG_CONSTEXPR byteslit(std::array<Byte,N> arr)
    {
        constexpr std::size_t size = N - 1;
        static_assert(sizeof(Byte) == sizeof(byte_t));
        static_assert(size <= bytesval::_intlen_net);

        std::array<bytesval::byte_t,bytesval::_intlen> c8 {};
        std::copy(&arr[0],&arr[0]+size,&c8[bytesval::_int_offset]);
        c8[bytesval::_length_byte] =
            static_cast<bytesval::byte_t>(size << 3);
        _l = byteslit_array_to_long(c8);
    }

    // constructor for runtime literals
    byteslit(byte_t const *data,  // short or permanent with '\0'
             std::size_t size)  // net size without '\0'
    {
        if (size <= bytesval::_intlen_net)
        {
            // short bytes
            std::array<bytesval::byte_t,bytesval::_intlen> c8 {};
            std::copy(data,data+size,&c8[bytesval::_int_offset]);
            c8[bytesval::_length_byte] =
                static_cast<bytesval::byte_t>(size << 3);
            _l = byteslit_array_to_long(c8);
        }
        else
        {
            // long bytes
            if (data[size])
                cig_static_assert(false,"byteslit external bytes "
                                  "not null-terminated");
            _l._cap = _long_mask;
            _l._size = size;
            _l._data = std::bit_cast<std::size_t,byte_t*>(
                const_cast<byte_t *>(data));
        }
    }
};


// Class byteslitraw encapsulates native C/C++-bytess
template<typename Byte,std::size_t N>
struct byteslitraw
{
    std::array<Byte,N> arr {};
    constexpr byteslitraw(Byte const (&pp)[N])
    {
	for (std::size_t i = 0; i < N; i++)
	    arr[i] = pp[i];
    };
};


// Class byteslitholder stores the std::array behind the byteslit
template<typename Byte,std::size_t N,std::array<Byte,N> arr>
struct byteslitholder
{
#if CIG_HAVE_CONSTEXPR
    static constexpr const std::array<Byte,N> buf { arr };
#else
    static const std::array<Byte,N> buf;
#endif

    CIG_CONSTEXPR explicit operator byteslit()
        const noexcept; // convert to byteslit
    // CIG_CONSTEXPR operator bytesval() const noexcept; // convert to bytesval
    CIG_CONSTEXPR operator bytes() const noexcept; // convert to bytes
    inline bytes to_bytes() const noexcept; // convert to bytes
};

#if ! CIG_HAVE_CONSTEXPR
template<typename Byte,std::size_t N,std::array<Byte,N> arr>
const std::array<Byte,N> byteslitholder<Byte,N,arr>::buf = { arr };
#endif


// Function join_byteslitholder_arrays concatenates two byteslitholders
template<typename A, std::size_t NA,
	 typename B, std::size_t NB,
	 typename C, std::size_t NC>
constexpr auto join_byteslitholder_arrays(std::array<A,NA> a,
                                              std::array<B,NB> b)
{
    std::array<C,NC> c { };
    std::size_t i;
    std::size_t j = 0;

    for (i = 0; i < NA-1; i++)
	c[j++] = a[i];
    for (i = 0; i < NB; i++)
	c[j++] = b[i];
    return c;
}


// Operator + concatenates two byteslitolders
template<
    typename ByteA,
    std::size_t NA,
    std::array<ByteA,NA> arrA,
    typename ByteB,
    std::size_t NB,
    std::array<ByteB,NB> arrB>
constexpr auto operator + (
    [[maybe_unused]] byteslitholder<ByteA,NA,arrA> a,
    [[maybe_unused]] byteslitholder<ByteB,NB,arrB> b)
{
    constexpr std::size_t N = NA + NB - 1;
    if constexpr (sizeof(ByteA) > sizeof(ByteB))
    {
	constexpr std::array c =
            join_byteslitholder_arrays<ByteA,NA,ByteB,NB,ByteA,N>(arrA,arrB);
	return byteslitholder<ByteA,N,c>();
    }
    else
    {
	constexpr std::array c =
            join_byteslitholder_arrays<ByteA,NA,ByteB,NB,ByteB,N>(arrA,arrB);
	return byteslitholder<ByteB,N,c>();
    }
} // operator +


// Operator byteslit converts a byteslitholder to byteslit
template<typename Byte,std::size_t N,std::array<Byte,N> arr>
CIG_CONSTEXPR byteslitholder<Byte,N,arr>::operator byteslit() const noexcept
{
    static_assert(sizeof(Byte) == sizeof(bytesval::byte_t));
    if constexpr (N-1 <= bytesval::_intlen_net)
    {
        return byteslit(arr);
    }
    else
    {
        return byteslit(&buf[0],N-1);
    }
}


// // Operator bytesval converts a byteslitholder to bytesval
// template<typename Byte,std::size_t N,std::array<Byte,N> arr>
// CIG_CONSTEXPR byteslitholder<Byte,N,arr>::operator bytesval() const noexcept
// {
//     return operator byteslit();
// }


// Operator bytes converts a byteslitholder to const bytes
template<typename Byte,std::size_t N,std::array<Byte,N> arr>
CIG_CONSTEXPR byteslitholder<Byte,N,arr>::operator bytes() const noexcept
{
    return bytes(operator byteslit());
}


// Operator bytes converts a byteslitholder to const bytes
template<typename Byte,std::size_t N,std::array<Byte,N> arr>
inline bytes byteslitholder<Byte,N,arr>::to_bytes() const noexcept
{
    return operator byteslit();
}


} // namespace detail


// Operator ""_bytes creates a byteslitholder from the _bytes suffix
template<detail::byteslitraw A>
CIG_CONSTEXPR auto operator"" _bytes()
{
    constexpr auto s = A.arr;
    constexpr std::size_t N = s.size();
    constexpr auto o = cig_constexpr_copy_bytes<bytes::byte_t,N>(s);
    return detail::byteslitholder<bytes::byte_t,N,o>();
} // operator "" _bytes()


} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_LITERALS_H_INCLUDED) */
