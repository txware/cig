/* cig_str_proxy.h
 *
 * Copyright 2023 Claus Fischer
 *
 * String proxy class, only valid during lifetime of original str.
 */
#ifndef CIG_STR_PROXY_H_INCLUDED
#define CIG_STR_PROXY_H_INCLUDED

#include "cig_compatibility.h"  // CIG_RESTRICT,std::bit_cast
#include "cig_datatypes.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"

namespace cig {

namespace detail {

// Intermediary between str and char for calls into the sysdep layer.
class utf8proxy {
    unsigned char const *utf8;  // may point directly into origin
    std::size_t utf8size;       // length in code units (bytes)
    bytes buf;                  // may hold UTF-8 conversion of origin

public:
    utf8proxy(str const &origin) // Only valid during origin's lifetime.
    {
        if (origin._is_ascii())
        {
            utf8 = origin._data_ucs1();
            utf8size = origin._get_size();
        }
        else
        {
            buf = origin.encode_utf8().value();
            utf8 = buf._data();
            utf8size = buf._get_size();
        }
    }

    ~utf8proxy() = default;
    utf8proxy(utf8proxy&&) = delete;
    utf8proxy& operator=(utf8proxy&&) = delete;
    utf8proxy(const utf8proxy&) = delete;
    utf8proxy& operator=(const utf8proxy&) = delete;

    char const *c_str() const
    {
        return reinterpret_cast<char const *>(utf8);
    }

    std::size_t size() const
    {
        return utf8size;
    }

    operator char const *() const
    {
        return c_str();
    }

    operator unsigned char const *() const
    {
        return utf8;
    }
};


} // namespace detail


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_STR_PROXY_H_INCLUDED) */
