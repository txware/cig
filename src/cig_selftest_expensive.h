/* cig_selftest_expensive.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Expensive selftest function for cig.
 */

#ifndef CIG_SELFTEST_EXPENSIVE_H_INCLUDED
#define CIG_SELFTEST_EXPENSIVE_H_INCLUDED

namespace cig {

char const * expensive_selftest_errorstring();

} /* namespace cig */

/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif	/* !defined(CIG_SELFTEST_EXPENSIVE_H_INCLUDED) */
