/* cig_jsonvalue.h
 *
 * Copyright 2023 Claus Fischer
 *
 * JSON value - a class for JSON values in an object/array hierarchy.
 */
#ifndef CIG_JSONVALUE_H_INCLUDED
#define CIG_JSONVALUE_H_INCLUDED

#include "cig_datatypes.h"
#include "cig_vector.h"
#include "cig_map.h"

#include <cstddef>              // std::nullptr_t
#include <variant>              // std::variant,std::monostate






namespace cig {

namespace json {

// This JSON data type hierarchy is modeled after JAVA's
// JSONArray and JSONObject classes.
// It uses the native datatypes for str, i64, and bool.
// It uses a nullptr to hold JSON 'null'.

struct value;                   // A JSON value
struct array;                   // A JSON array
struct object;                  // A JSON object

typedef std::variant<std::nullptr_t,array,object,str,cig_bool,i64> value_base;

struct array : public cig::vector<value>
{
};

struct object : public cig::map<str,value>
{
};

struct value : value_base
{
    enum value_type {
        JSON_NULL = 0,
        JSON_ARRAY = 1,
        JSON_OBJECT = 2,
        JSON_STR = 3,
        JSON_BOOL = 4,
        JSON_I64 = 5
    };

    constexpr value_type get_value_type() const noexcept
    {
        return static_cast<value_type>(index());
    }

    constexpr cig_bool is_null() const noexcept
    {
        return get_value_type() == JSON_NULL;
    }

    constexpr cig_bool is_true() const noexcept
    {
        return get_value_type() == JSON_BOOL && std::get<cig_bool>(*this);
    }

    constexpr cig_bool is_false() const noexcept
    {
        return get_value_type() == JSON_BOOL && !std::get<cig_bool>(*this);
    }

    constexpr cig_bool is_array() const noexcept
    {
        return get_value_type() == JSON_ARRAY;
    }

    constexpr cig_bool is_object() const noexcept
    {
        return get_value_type() == JSON_OBJECT;
    }

    constexpr cig_bool is_bool() const noexcept
    {
        return get_value_type() == JSON_BOOL;
    }

    constexpr array const & get_array() const
    {
        return std::get<array>(*this);
    }

    constexpr object const & get_object() const
    {
        return std::get<object>(*this);
    }

    constexpr str const & get_str() const
    {
        return std::get<str>(*this);
    }

    constexpr cig_bool get_bool() const
    {
        return std::get<cig_bool>(*this);
    }

    constexpr i64 get_i64() const
    {
        return std::get<i64>(*this);
    }

    constexpr array & get_array()
    {
        return std::get<array>(*this);
    }

    constexpr object & get_object()
    {
        return std::get<object>(*this);
    }

    constexpr str & get_str()
    {
        return std::get<str>(*this);
    }

    value() noexcept = default;
    value(value const &) = default;
    value(value &&) = default;
    value & operator = (value const &) = default;
    value & operator = (value &&) = default;

    value(array const &a) noexcept;
    value(array &&a) noexcept;
    value(object const &o) noexcept;
    value(object &&o) noexcept;
    value(str const &s) noexcept : value_base(s) { }
    value(str &&s) noexcept : value_base(std::move(s)) { }
    value(std::nullptr_t) noexcept : value_base(nullptr) { }
    value(cig_bool b) noexcept : value_base(b) { }
    value(i64 const &v) noexcept : value_base(v) { }
};

inline value::value(array const &a) noexcept : value_base(a) { }
inline value::value(array &&a) noexcept : value_base(std::move(a)) { }
inline value::value(object const &o) noexcept : value_base(o) { }
inline value::value(object &&o) noexcept : value_base(std::move(o)) { }

} // namespace json

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_JSONVALUE_H_INCLUDED) */
