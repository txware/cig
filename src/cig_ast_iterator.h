/* cig_ast_iterator.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Abstract Syntax Tree - AST recursive iterator.
 */
#ifndef CIG_AST_ITERATOR_H_INCLUDED
#define CIG_AST_ITERATOR_H_INCLUDED

#include "cig_ast_node.h"

#include <functional>           // std::function


namespace cig {

namespace ast {

namespace detail {


struct ast_node_level_info {
    i64 namespace_level;
    i64 class_level;
    i64 compoundstmt_level;
    i64 expression_level;
    i64 total_level;
};

// Iterate recursively over an AST subtree rooted in nd.
//
// The iterator calls the std::function for each child of nd, with nd
// passed as the 'parent' argument.
//
// If the return value is an cig::err, including the error code E_SUCCESS,
// iteration will stop and the iterator will return the cig::err from the
// toplevel call.
//
// If the std::function returns false, iteration will not proceed into the
// descendants of the child but will proceed with the other children of nd.
//
// If the std::function returns true, iteration will proceed for the
// descendants of child by calling ast_node_recursive_iterator recursively.
// 
exp_none
ast_node_recursive_iterator(ast_node_ptr nd,
                            cig_bool use_global_subnodes,
                            std::function<exp_bool(
                                ast_node_type parent_type,
                                ast_node_ptr parent,
                                ast_node_level_info parent_level,
                                ast_node_type child_type,
                                ast_node_ptr child,
                                ast_node_level_info child_level,
                                i64 child_index)> mapfunc,
                            ast_node_level_info level =
                            ast_node_level_info(),
                            cig::source_location const &sloc =
                            std::source_location::current());


exp_none
ast_node_iterate_once(ast_node_ptr nd,
                      std::function<exp_none(ast_node_ptr nd)> mapfunc,
                      cig_bool use_global_subnodes = true,
                      cig_bool variable_before_expression_statement = true,
                      cig::source_location const &sloc =
                      std::source_location::current());

} // namespace detail

} // namespace ast

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_AST_ITERATOR_H_INCLUDED) */
