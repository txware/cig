/* cig_file_descriptor.c
 *
 * Copyright 2023 Claus Fischer
 *
 * File descriptor class with destructor.
 */
#include "cig_file_descriptor.h"

#include <cstdint>		// int64_t
#include "cig_datatypes.h"
#include "cig_bytes_inline.h"
#include "cig_sysdep_file.h"
#include "cig_assert.h"
#include "cig_str_proxy.h"
#include "cig_int_inline.h"


namespace cig {

namespace file {

void descriptor::_destruct() noexcept
{
    if (filedes != invalid)
    {
	int err = cig_sys_file_close(filedes);
	cig_assert(err == 0);
        filedes = invalid;
    }
}

exp_descriptor descriptor::exp_open(str const &filepath,bool read_only) noexcept
{
    auto path = detail::utf8proxy(filepath);
    int fd;
    int e = cig_sys_file_open(path,read_only,&fd);
    if (e)
        return makeerr(e);
    return descriptor(fd);
}

exp_none descriptor::exp_close() noexcept
{
    if (filedes == invalid)
        return makeerr(err::E_EBADF);
    int e = cig_sys_file_close(filedes);
    if (e)
        return makeerr(e);
    filedes = invalid;
    return exp_none();
}


exp_none descriptor::exp_syncdata() const noexcept
{
    if (filedes == invalid)
        return makeerr(err::E_EBADF);
    int e = cig_sys_file_syncdata(filedes);
    if (e)
        return makeerr(e);
    return exp_none();
}


exp_none descriptor::exp_syncdata_name(str const &filepath) noexcept
{
    auto path = detail::utf8proxy(filepath);
    int e = cig_sys_file_syncdata_name(path);
    if (e)
        return makeerr(e);
    return exp_none();
}


exp_i64 descriptor::exp_get_position() const noexcept
{
    if (filedes == invalid)
        return makeerr(err::E_EBADF);
    int64_t v;
    int e = cig_sys_file_getpos(filedes,&v);
    if (e)
        return makeerr(e);
    return v;
}


exp_i64 descriptor::exp_get_size() const noexcept
{
    if (filedes == invalid)
        return makeerr(err::E_EBADF);
    int64_t v;
    int e = cig_sys_file_getlen(filedes,&v);
    if (e)
        return makeerr(e);
    return v;
}


exp_none descriptor::exp_set_position(i64 pos) const noexcept
{
    if (filedes == invalid)
        return makeerr(err::E_EBADF);
    int e = cig_sys_file_setpos(filedes,pos._v);
    if (e)
        return makeerr(e);
    return exp_none();
}


exp_none descriptor::exp_truncate() const noexcept
{
    if (filedes == invalid)
        return makeerr(err::E_EBADF);
    int e = cig_sys_file_truncate(filedes);
    if (e)
        return makeerr(e);
    return exp_none();
}


err descriptor::sysdep_write(void const *data,i64 length) const noexcept
{
    if (filedes == invalid)
        return err::E_EBADF;
    if (length < 0)
        return err::E_EINVAL;
    int e = cig_sys_file_write(filedes,data,static_cast<uint64_t>(length._v));
    if (e)
        return e;
    return err::E_SUCCESS;
}


err descriptor::sysdep_read(void *data,i64 length) const noexcept
{
    if (filedes == invalid)
        return err::E_EBADF;
    if (length < 0)
        return err::E_EINVAL;
    int e = cig_sys_file_read(filedes,data,static_cast<uint64_t>(length._v));
    if (e)
        return e;
    return err::E_SUCCESS;
}



} // namespace file

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
