/* cig_interpreter_dump.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Abstract Syntax Tree - AST Tree Dumper
 */
#ifndef CIG_INTERPRETER_DUMP_H_INCLUDED
#define CIG_INTERPRETER_DUMP_H_INCLUDED

#include "cig_interpreter.h"

namespace cig {

namespace interpreter {

void
dump_interpreter_code(str filename,ipcode_ptr ip);

} // namespace interpreter

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_INTERPRETER_DUMP_H_INCLUDED) */
