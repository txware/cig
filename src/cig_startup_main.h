/* cig_startup_main.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Startup and termination code for cig_main.
 *
 * This include file is only for startup-related code;
 * it should not be used in the general parts of the code.
 * It should only be used in the file defining cig_main().
 */
#ifndef CIG_STARTUP_MAIN_H_INCLUDED
#define CIG_STARTUP_MAIN_H_INCLUDED

namespace cig {

// A prototype for cig::cig_main.
int cig_main(int argc, char *argv[]);

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_STARTUP_MAIN_H_INCLUDED) */
