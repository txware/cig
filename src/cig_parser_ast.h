/* cig_parser_ast.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Parser for the CIG language.
 */
#ifndef CIG_PARSER_AST_H_INCLUDED
#define CIG_PARSER_AST_H_INCLUDED

#include "cig_ast_node.h"


namespace cig {

namespace parser {

parse_error
parse_ast_file(ast::ast_node_ptr global, // global namespace
               token_list const &);

} // namespace parser

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_PARSER_AST_H_INCLUDED) */
