/* cig_ast_iterator.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - AST recursive iterator
 */
#include "cig_ast_iterator.h"


namespace cig {

namespace ast {

namespace detail {


#define APPLY(child,index)                                              \
    do {                                                                \
        /* Evaluate child only once */                                  \
        ast_node_ptr const &childaux = child;                           \
                                                                        \
        if (childaux != nullptr &&                                      \
            (node_type != AST_NAMESPACE ||                              \
             !use_global_subnodes))                                     \
        {                                                               \
            ast_node_type child_type =                                  \
                child->get_node_type();                                 \
            auto exp = mapfunc(node_type,nd,level,                      \
                               child_type,childaux,childlevel,index);   \
            if (!exp)                                                   \
                return forwarderr(exp);                                 \
                                                                        \
            /* Iterate over children */                                 \
            if (exp.value())                                            \
            {                                                           \
                auto exp2 = ast_node_recursive_iterator_aux(            \
                                childaux,                               \
                                use_global_subnodes,                    \
                                mapfunc,                                \
                                childlevel,                             \
                                sloc);                                  \
                if (!exp2)                                              \
                    return exp2;                                        \
            }                                                           \
        }                                                               \
    } while (0)




static
exp_none
ast_node_recursive_iterator_aux(ast_node_ptr nd,
                                cig_bool use_global_subnodes,
                                std::function<exp_bool(
                                    ast_node_type parent_type,
                                    ast_node_ptr parent,
                                    ast_node_level_info parent_level,
                                    ast_node_type child_type,
                                    ast_node_ptr child,
                                    ast_node_level_info child_level,
                                    i64 child_index)> mapfunc,
                                ast_node_level_info level,
                                cig::source_location const &sloc)
{
    using enum ast::ast_node_type;
    using enum ast::ast_node_br_special;

    if (nd == nullptr)
        return makeerr(err::EC_AST_ITERATOR_BAD_TOPNODE,sloc);

    ast_node_level_info childlevel = level;
    childlevel.total_level += 1;

    ast_node_type node_type = nd->get_node_type();

    switch (node_type)
    {
    case AST_INVALID:
    case AST_EXPRMIN:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_STMTMAX:
    case AST_MAX:
        break;

        // The global node has two iteration options
    case AST_GLOBAL:
        if (use_global_subnodes)
            goto iterate_over_global_subnode_list;
        childlevel.namespace_level += 1;
        goto iterate_over_subnode_list;

        // Nodes with no subnodes
    case AST_ENUMVALUE:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
        return exp_none();

        // Nodes that have just a subnodelist
        // and no subnodes in fixed places:
    case AST_NAMESPACE:
        childlevel.namespace_level += 1;
        goto iterate_over_subnode_list;

    case AST_CLASSDEF:
        childlevel.class_level += 1;
        goto iterate_over_subnode_list;

    case AST_ENUMDEF:
        childlevel.class_level += 1;
        goto iterate_over_subnode_list;

    case AST_FUNPARLIST:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
        goto iterate_over_subnode_list;

    case AST_OVERLOADSET:
        // Keep childlevel
        goto iterate_over_overload_list;

    case AST_EXPR_LIST:
        childlevel.expression_level += 1;
        goto iterate_over_listsubexpr;

    case AST_COMPOUNDSTMT:
        childlevel.compoundstmt_level += 1;
        goto iterate_over_subnode_list;

        // Nodes with subnodes in fixed places only
    case AST_CLASSDECL:
        // Keep childlevel
        APPLY(nd->get_classdef(),AST_BR_CLASSDEF);
        return exp_none();

    case AST_ENUMDECL:
        // Keep childlevel
        APPLY(nd->get_enumdef(),AST_BR_ENUMDEF);
        return exp_none();

    case AST_VARIABLE:
        // Keep childlevel
        APPLY(nd->get_typeref(),AST_BR_TYPEREF);
        return exp_none();

    case AST_FUNDEF:
        // Keep childlevel
        APPLY(nd->get_fundef_qualified_name(),AST_BR_QUNAME);
        goto iterate_over_lambdabase;
        
    case AST_FUNDECL:
    case AST_LAMBDA:
        // Keep childlevel
        goto iterate_over_lambdabase;

    case AST_TYPEREF:
        // Keep childlevel
        APPLY(nd->get_ref(),AST_BR_REF);
        return exp_none();

    case AST_FUNPAR:
    case AST_FUNRET:
        // Keep childlevel
        APPLY(nd->get_typeref(),AST_BR_TYPEREF);
        return exp_none();

    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
        // Keep childlevel
        APPLY(nd->get_expr(),AST_BR_STMTEXPR);
        return exp_none();

        // Unary expressions
    case AST_EXPR_MEMBER:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
        childlevel.expression_level += 1;
        APPLY(nd->get_subexpr1(),AST_BR_SUBEXPR1);
        return exp_none();

        // Binary expressions
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_FUNCALL:
    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
        childlevel.expression_level += 1;
        APPLY(nd->get_subexpr1(),AST_BR_SUBEXPR1);
        APPLY(nd->get_subexpr2(),AST_BR_SUBEXPR2);
        return exp_none();

        // Ternary expressions
    case AST_EXPR_CONDITION:
        childlevel.expression_level += 1;
        APPLY(nd->get_subexpr1(),AST_BR_SUBEXPR1);
        APPLY(nd->get_subexpr2(),AST_BR_SUBEXPR2);
        APPLY(nd->get_subexpr3(),AST_BR_SUBEXPR3);
        return exp_none();

        // Quarternary expressions
    case AST_EXPR_SLICE:
        childlevel.expression_level += 1;
        APPLY(nd->get_subexpr1(),AST_BR_SUBEXPR1);
        APPLY(nd->get_subexpr2(),AST_BR_SUBEXPR2);
        APPLY(nd->get_subexpr3(),AST_BR_SUBEXPR3);
        APPLY(nd->get_subexpr4(),AST_BR_SUBEXPR4);
        return exp_none();

        // Nodes with subnodes in fixed places
        // and additional subnodelists

    case AST_IFSTMT:
        goto iterate_over_subnode_list;

    case AST_WHILESTMT:
        goto iterate_over_subnode_list;

    case AST_DOWHILESTMT:
        goto iterate_over_subnode_list;

    case AST_FORSTMT:
        goto iterate_over_subnode_list;

    case AST_FOROFSTMT:
        goto iterate_over_subnode_list;
    }

    return makeerr(err::EC_AST_ITERATOR_BAD_TOPNODE,sloc);

 iterate_over_lambdabase:
    if (1)
    {
        APPLY(nd->get_funparlist(),AST_BR_FUNPARLIST);
        APPLY(nd->get_funretlist(),AST_BR_FUNRETLIST);
        APPLY(nd->get_funbody(),AST_BR_FUNBODY);
    }
    if (node_type == AST_FUNDECL)
        APPLY(nd->get_fundef(),AST_BR_FUNDEF);
    return exp_none();

 iterate_over_subnode_list:
    if (1)
    {
        i64 subnode_len = nd->get_subnode_count();
        for (i64 i = 0; i < subnode_len; i += 1)
        {
            ast_node_ptr child = nd->get_subnode(i);
            if (child == nullptr)
                return makeerr(
                    err::EC_AST_NULL_IN_SUBNODE_LIST,
                    sloc);
            APPLY(child,i);
        }
    }

    // Specific subnodes to be treated after general subnode list
    if (0)
        ;
    else if (node_type == AST_IFSTMT)
    {
        // Keep childlevel
        APPLY(nd->get_condexpr(),AST_BR_COND);
        APPLY(nd->get_thenstmt(),AST_BR_THEN);
        APPLY(nd->get_elsestmt(),AST_BR_ELSE);
    }
    else if (node_type == AST_WHILESTMT)
    {
        // Keep childlevel
        APPLY(nd->get_condexpr(),AST_BR_COND);
        APPLY(nd->get_bodystmt(),AST_BR_BODY);
    }
    else if (node_type == AST_DOWHILESTMT)
    {
        // Keep childlevel
        APPLY(nd->get_condexpr(),AST_BR_COND);
        APPLY(nd->get_bodystmt(),AST_BR_BODY);
    }
    else if (node_type == AST_FORSTMT)
    {
        // Keep childlevel
        APPLY(nd->get_initstmt(),AST_BR_INIT);
        APPLY(nd->get_condexpr(),AST_BR_COND);
        APPLY(nd->get_iterateexpr(),AST_BR_ITERATE);
        APPLY(nd->get_bodystmt(),AST_BR_BODY);
    }
    else if (node_type == AST_FOROFSTMT)
    {
        // Keep childlevel
        APPLY(nd->get_iteratorexpr(),AST_BR_ITERATOR);
        APPLY(nd->get_generatorexpr(),AST_BR_GENERATOR);
        APPLY(nd->get_bodystmt(),AST_BR_BODY);
    }

    return exp_none();

 iterate_over_global_subnode_list:
    if (1)
    {
        i64 global_subnode_len = nd->get_global_subnode_count();
        for (i64 i = 0; i < global_subnode_len; i += 1)
        {
            ast_node_ptr child = nd->get_global_subnode(i);
            if (child == nullptr)
                return makeerr(
                    err::EC_AST_NULL_IN_SUBNODE_LIST,
                    sloc);

            APPLY(child,i);
        }
    }
    
    return exp_none();

 iterate_over_overload_list:
    if (1)
    {
        i64 overload_len = nd->get_overload_count();
        for (i64 i = 0; i < overload_len; i += 1)
        {
            ast_node_ptr child = nd->get_overload(i);
            if (child == nullptr)
                return makeerr(
                    err::EC_AST_NULL_IN_OVERLOAD_LIST,
                    sloc);
            APPLY(child,i);
        }
    }
    
    return exp_none();

 iterate_over_listsubexpr:
    if (1)
    {
        i64 list_len = nd->get_listsubexpr_count();
        for (i64 i = 0; i < list_len; i += 1)
        {
            ast_node_ptr child = nd->get_listsubexpr(i);
            if (child == nullptr)
                return makeerr(
                    err::EC_AST_NULL_IN_LISTSUBEXPR_LIST,
                    sloc);
            APPLY(child,i);
        }
    }
    
    return exp_none();
}


exp_none
ast_node_recursive_iterator(ast_node_ptr nd,
                            cig_bool use_global_subnodes,
                            std::function<exp_bool(
                                ast_node_type parent_type,
                                ast_node_ptr parent,
                                ast_node_level_info parent_level,
                                ast_node_type child_type,
                                ast_node_ptr child,
                                ast_node_level_info child_level,
                                i64 child_index)> mapfunc,
                            ast_node_level_info level,
                            cig::source_location const &sloc)
{
    using enum ast::ast_node_type;

    if (nd == nullptr)
        return makeerr(err::EC_AST_ITERATOR_BAD_TOPNODE,sloc);

    if (nd->get_node_type() != AST_GLOBAL)
        use_global_subnodes = false;

    return ast_node_recursive_iterator_aux(nd,use_global_subnodes,mapfunc,
                                           level,sloc);
}


// Iterator to process each node exactly once
exp_none
ast_node_iterate_once(ast_node_ptr nd,
                      std::function<exp_none(ast_node_ptr nd)> mapfunc,
                      cig_bool use_global_subnodes,
                      cig_bool variable_before_expression_statement,
                      cig::source_location const &sloc)
{
    using enum ast::ast_node_type;

    std::function<
        exp_bool(ast_node_type parent_type,
                 ast_node_ptr parent,
                 ast_node_level_info parent_level,
                 ast_node_type child_type,
                 ast_node_ptr child,
                 ast_node_level_info child_level,
                 i64 child_index)> mapfuncaux =
        [&] ([[maybe_unused]] ast_node_type parent_type,
             [[maybe_unused]] ast_node_ptr parent,
             [[maybe_unused]] ast::detail::ast_node_level_info parent_level,
             [[maybe_unused]] ast_node_type child_type,
             [[maybe_unused]] ast_node_ptr child,
             [[maybe_unused]] ast::detail::ast_node_level_info child_level,
             [[maybe_unused]] i64 child_index) -> exp_bool {

            // Avoid duplicate processing of classdef,enumdef,fundef
            if (parent_type == AST_CLASSDECL &&
                child_type == AST_CLASSDEF)
                return false;
            if (parent_type == AST_ENUMDECL &&
                child_type == AST_ENUMDEF)
                return false;
            if (parent_type == AST_FUNDECL &&
                child_type == AST_FUNDEF)
                return false;
            if (parent_type == AST_OVERLOADSET)
                return false;
            if (child_type == AST_VARIABLE)
            {
                // This is the variable being defined in an expression
                if (ast_node::ast_node_type_is_expr(parent_type) ||
                    parent_type == AST_EXPRSTMT ||
                    // The for..of iteratorexpr is not a statement of its own,
                    // therefore we need to check the child index
                    (parent_type == AST_FOROFSTMT &&
                     child_index == AST_BR_ITERATOR))
                {
                    if (variable_before_expression_statement)
                        return false;
                }
                // This is the variable being defined
                // in the scope before the expression
                else if (ast_node::ast_node_type_is_stmt(parent_type))
                {
                    cig_assert(parent_type != AST_RETURNSTMT);
                    if (!variable_before_expression_statement)
                        return false;
                }
                // This is the variable being defined
                // in a class or namespace
                else
                {
                    cig_assert(parent_type == AST_GLOBAL ||
                               parent_type == AST_NAMESPACE ||
                               parent_type == AST_CLASSDEF);
                    // Process the variable always
                }
            }
            auto exp = mapfunc(child);
            if (!exp)
                return forwarderr(exp);
            return true;
        };

    detail::ast_node_level_info li = ast_node_level_info();
    return ast_node_recursive_iterator(nd,use_global_subnodes,mapfuncaux,
                                       li,sloc);
}



} // namespace detail

} // namespace ast

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
