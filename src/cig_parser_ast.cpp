/* cig_parser_ast.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Parser for the CIG language.
 */
#include "cig_parser.h"
#include "cig_ast_node.h"
#include "cig_parser_tokenizer.h"
#include "cig_parser_ast.h"
#include "cig_assert.h"


namespace cig {

namespace parser {

using ast_node_or_parse_error = std::expected<ast::ast_node_ptr,parse_error>;

static
ast_node_or_parse_error
parse_function(token_list const &tokenlist,
               i64 startpos,
               i64 &endpos,
               cig_bool is_static = cig_false,
               cig_bool is_nonstatic = cig_false);

static
ast_node_or_parse_error
parse_namespace(ast::ast_node_ptr outer,
                token_list const &tokenlist,
                i64 startpos,
                i64 &endpos);

static
ast_node_or_parse_error
parse_class(token_list const &tokenlist,
            i64 startpos,
            i64 &endpos,
            cig_bool is_static = cig_false,
            cig_bool is_nonstatic = cig_false);

static
ast_node_or_parse_error
parse_enum(token_list const &tokenlist,
           i64 startpos,
           i64 &endpos);

static
ast_node_or_parse_error
parse_expression(token_list const &tokenlist,
                 i64 startpos,
                 i64 &endpos);

static
ast_node_or_parse_error
parse_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos);

static
auto
err_to_parse_error(cig::err const &e)
{
    return std::unexpected(parse_error(e));
}



// Check whether a token can start a qualified name
static
bool                            // true if token can start a qualified name
token_can_start_qualified_name(
    token const &tok            // token to inspect
    )
{
    if (tok.ttype == tokentype::IDENTIFIER)
        return true;

    // fixme keywords such as global can be added
    return false;
}


// Parse a qualified name
static
ast_node_or_parse_error
parse_qualified_name(token_list const &tokenlist,
                     i64 startpos,
                     i64 &endpos)
{
    i64 pos = startpos;
    using namespace ast;
    using enum ast::ast_node_type;

    token const &starttok = tokenlist[pos];
    if (!token_can_start_qualified_name(starttok))
    {
        return std::unexpected(
            parse_error(starttok.sourceloc,
                        err::EC_EXPECTING_QUALIFIED_NAME));
    }

    // Check for member names
    i64 posx = pos + 1;
    for (;;)
    {
        // fixme handle C++ template instantiation
        token const &dottok = tokenlist[posx];
        if (dottok.ttype != tokentype::DOT)
            break;
        token const &idtok = tokenlist[posx + 1];
        if (idtok.ttype != tokentype::IDENTIFIER)
            return std::unexpected(
                parse_error(idtok.sourceloc,
                            err::EC_EXPECTING_MEMBER_NAME));
        posx = posx + 2;
    }

    // If the token that starts the qualified name is not an
    // identifier, then it must be followed by at least one
    // member operation. Otherwise the whole thing is not
    // a qualified name at all.
    if (starttok.ttype != tokentype::IDENTIFIER &&
        posx == pos + 1)
        return std::unexpected(
            parse_error(starttok.sourceloc,
                        err::EC_TOKEN_MUST_BE_FOLLOWED_BY_DOT));

    // All right, we have a qualified name


    // Start with the correct AST node
    ast_node_ptr nd;
    if (starttok.ttype == tokentype::IDENTIFIER)
    {
        str const &idname = std::get<str>(starttok.value);

        // Add the identifier as an unqualified name
        nd = ast_node::make_ast_node(AST_EXPR_UNQUALNAME,
                                     starttok.startloc(),
                                     starttok.startloc(),
                                     starttok.endloc());
        cig_assert(nd != nullptr);
        auto exp1 = nd->set_unqualified_name(idname);
        if (!exp1)
            return err_to_parse_error(exp1.error());
        pos = pos + 2;
    }

    // fixme else add a special token for global, this, outer, super

    else
        // Any type allowed by token_can_start_qualified_name
        // must have been handled here.
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));

    // Build the chain of member access nodes
    // pos starts at the first member name in the chain
    while (pos < posx)
    {
        token const &idtok = tokenlist[pos];
        cig_assert(idtok.ttype == tokentype::IDENTIFIER); // follows from above
        str const &idname = std::get<str>(idtok.value);

        // Create a new expression for the dot operation
        ast_node_ptr oldnd = std::move(nd);
        nd = ast_node::make_ast_node(AST_EXPR_MEMBER,
                                     oldnd->get_startloc(),
                                     idtok.startloc(),
                                     idtok.endloc());
        cig_assert(nd != nullptr);
        auto exp2 = nd->set_subexpr1(oldnd);
        if (!exp2)
            return err_to_parse_error(exp2.error());
        auto exp3 = nd->set_member_name(idname);
        if (!exp3)
            return err_to_parse_error(exp3.error());

        // We do not need to check the dot again
        pos = pos + 2;
    }

    cig_assert(pos == posx + 1);

    endpos = posx;
    return nd;
}


// Parse a reference to a type outside a function body
// (inside the function body see parse_primary_expression).
//
// The intention is that datatypes as in
//   some_function(arguments).membertype
// cannot be used in variable declarations or function parameter lists.
static
ast_node_or_parse_error
parse_type_ref(token_list const &tokenlist,
               i64 startpos,
               i64 &endpos)
{
    i64 pos = startpos;
    using namespace ast;
    using enum ast::ast_node_type;

    auto exp = parse_qualified_name(tokenlist,pos,pos);
    if (!exp)
        return exp;
    ast_node_ptr qualified_name = std::move(exp.value());

    ast_node_ptr nd = ast_node::make_ast_node(AST_TYPEREF,
                                              qualified_name->get_startloc(),
                                              qualified_name->get_effectloc(),
                                              qualified_name->get_endloc());
    cig_assert(nd != nullptr);
    auto exp1 = nd->set_ref(qualified_name);
    if (!exp1)
        return err_to_parse_error(exp1.error());
    endpos = pos;
    return nd;
}


// Parse a variable declaration after the type_ref
static
ast_node_or_parse_error
parse_variable_after_typeref(token_list const &tokenlist,
                             ast::ast_node_ptr const &typeref,
                             cig::source_location &sourceloc,
                             i64 startpos,
                             i64 &endpos,
                             cig_bool is_static = cig_false,
                             cig_bool is_nonstatic = cig_false)
{
    i64 pos = startpos;
    using namespace ast;
    using enum ast::ast_node_type;
    using enum ast_node::ast_node_flags;

    // Check consistency of static specifications
    if (is_static && is_nonstatic)
        return std::unexpected(
            parse_error(sourceloc,
                        err::EC_CONTRADICTING_STATIC_SPECIFICATION));

    ast_node::ast_node_flags flags = AST_FLAG_NOPTR;

    token const &tok = tokenlist[pos];
    if (tok.ttype == tokentype::REFPTR)
    {
        flags = AST_FLAG_REFPTR;
        pos = pos + 1;
    }
    else if (tok.ttype == tokentype::WEAKPTR)
    {
        flags = AST_FLAG_WEAKPTR;
        pos = pos + 1;
    }
    else if (tok.ttype == tokentype::SHAREDPTR)
    {
        flags = AST_FLAG_SHAREDPTR;
        pos = pos + 1;
    }
    else if (tok.ttype == tokentype::NONNULLPTR)
    {
        flags = AST_FLAG_NONNULLPTR;
        pos = pos + 1;
    }

    if (is_static)
        flags = ast_node::ast_node_flags(flags | AST_FLAG_STATIC); 
    if (is_nonstatic)
        flags = ast_node::ast_node_flags(flags | AST_FLAG_NONSTATIC); 

    token const &nametok = tokenlist[pos];
    if (nametok.ttype != tokentype::IDENTIFIER)
    {
        return std::unexpected(
            parse_error(nametok.sourceloc,
                        err::EC_EXPECTING_IDENTIFIER));
    }
    str const &varname = std::get<str>(nametok.value);

    ast_node_ptr nd = ast_node::make_ast_node(AST_VARIABLE,
                                              typeref->get_startloc(),
                                              nametok.startloc(),
                                              nametok.endloc());
    auto exp1 = nd->set_typeref(typeref);
    if (!exp1)
        return err_to_parse_error(exp1.error());
    auto exp2 = nd->set_name(varname);
    if (!exp2)
        return err_to_parse_error(exp2.error());
    auto exp3 = nd->set_flags(flags);
    if (!exp3)
        return err_to_parse_error(exp3.error());

    sourceloc = nametok.sourceloc;
    endpos = pos + 1;
    return nd;
}


// Parse a variable declaration outside a function body (class/global variable)
static
ast_node_or_parse_error
parse_variable(token_list const &tokenlist,
               i64 startpos,
               i64 &endpos,
               cig_bool is_static = cig_false,
               cig_bool is_nonstatic = cig_false)
{
    using namespace ast;

    // Consistency of is_static and is_nonstatic checked
    // inside parse_variable_after_typeref

    i64 pos = startpos;

    // Read the data type
    auto exp1 = parse_type_ref(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr typeref = exp1.value();

    // Parse the variable after the typeref
    cig::source_location sloc;
    auto exp2 = parse_variable_after_typeref(tokenlist,typeref,sloc,pos,pos,
                                             is_static,is_nonstatic);
    if (!exp2)
        return exp2;
    ast_node_ptr variable = exp2.value();

    // fixme parse an initialization expression

    // Read the semicolon
    token const &semitok = tokenlist[pos];
    if (semitok.ttype != tokentype::SEMICOLON)
        return std::unexpected(
            parse_error(semitok.sourceloc,
                        err::EC_EXPECTING_SEMICOLON));
    pos = pos + 1;

    // Adjust the location
    variable->set_endloc(semitok.endloc());

    endpos = pos;
    return variable;
}


// Parse a single function parameter
static
ast_node_or_parse_error
parse_funpar(token_list const &tokenlist,
             i64 startpos,
             i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    // Read the data type
    auto exp1 = parse_type_ref(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr typeref = exp1.value();

    // Create the funpar
    ast_node_ptr funpar = ast_node::make_ast_node(AST_FUNPAR,
                                                  typeref->get_startloc(),
                                                  typeref->get_startloc(),
                                                  typeref->get_endloc());
    auto exp2 = funpar->set_typeref(typeref);
    if (!exp2)
        return err_to_parse_error(exp2.error());

    // fixme qualifiers @

    // Read the parameter name if it exists
    token const &nametok = tokenlist[pos];
    if (nametok.ttype == tokentype::IDENTIFIER)
    {
        str const &parname = std::get<str>(nametok.value);
        auto exp3 = funpar->set_name(parname);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        pos = pos + 1;

        funpar->set_effectloc(nametok.startloc());
        funpar->set_endloc(nametok.endloc());
    }

    endpos = pos;
    return funpar;
}


// Parse a function parameter list
static
ast_node_or_parse_error
parse_function_parlist(token_list const &tokenlist,
                       i64 startpos,
                       i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &opentok = tokenlist[pos];
    if (opentok.ttype != tokentype::OPENPAREN)
        return std::unexpected(parse_error(
                                   opentok.sourceloc,
                                   err::EC_EXPECTING_OPENPAREN));
    pos = pos + 1;

    ast_node_ptr parlist = ast_node::make_ast_node(AST_FUNPARLIST,
                                                   opentok.startloc(),
                                                   opentok.startloc(),
                                                   opentok.endloc());
    cig_assert(parlist != nullptr);

    for (bool first = true;; first = false)
    {
        token const &tok = tokenlist[pos];

        // End parsing at file-end or parlist-end paren
        if (tok.ttype == tokentype::FILEEND ||
            tok.ttype == tokentype::CLOSEPAREN)
            break;

        // Read the item-separating comma
        if (!first)
        {
            if (tok.ttype != tokentype::COMMA)
                return std::unexpected(
                    parse_error(tok.sourceloc,
                                err::EC_EXPECTING_COMMA_OR_END));
            pos = pos + 1;
        }

        token const &typetok = tokenlist[pos];
        if (!token_can_start_qualified_name(typetok))
        {
            return std::unexpected(
                parse_error(tok.sourceloc,
                            err::EC_UNEXPECTED_TOKEN_IN_PARLIST));
        }

        auto expfunpar = parse_funpar(tokenlist,pos,pos);
        if (!expfunpar)
            return expfunpar;
        ast_node_ptr funpar = std::move(expfunpar.value());

        // Add the funpar to the scope
        auto expindex = parlist->add_named_entity(funpar);
        if (!expindex)
            return err_to_parse_error(expindex.error());
    }

    token const &closetok = tokenlist[pos];
    if (closetok.ttype == tokentype::CLOSEPAREN)
    {
        pos = pos + 1;
    }
    else
    {
        cig_assert(closetok.ttype == tokentype::FILEEND); // follows from above
        return std::unexpected(
            parse_error(opentok.sourceloc,
                        err::EC_PARLIST_NOT_CLOSED));
    }

    parlist->set_endloc(closetok.endloc());

    // Successful return
    endpos = pos;
    return parlist;
}


// Parse a single function return type
static
ast_node_or_parse_error
parse_funret(token_list const &tokenlist,
             i64 startpos,
             i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    // Read the data type
    auto exp1 = parse_type_ref(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr typeref = exp1.value();

    // Create the funret
    ast_node_ptr funret = ast_node::make_ast_node(AST_FUNRET,
                                                  typeref->get_startloc(),
                                                  typeref->get_startloc(),
                                                  typeref->get_endloc());
    auto exp2 = funret->set_typeref(typeref);
    if (!exp2)
        return err_to_parse_error(exp2.error());

    // fixme qualifiers @

    // No parameter names for return types
    // // Read the parameter name if it exists
    // token const &nametok = tokenlist[pos];
    // if (nametok.ttype == tokentype::IDENTIFIER)
    // {
    //     str const &parname = std::get<str>(nametok.value);
    //     auto exp3 = funret->set_name(parname);
    //     cig_assert(exp3);
    //     pos = pos + 1;
    // 
    //     funpar->set_effectloc(nametok.startloc());
    //     funpar->set_endloc(nametok.endloc());
    // }

    endpos = pos;
    return funret;
}


// Parse a function return type list
static
ast_node_or_parse_error
parse_function_retlist(token_list const &tokenlist,
                       i64 startpos,
                       i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &opentok = tokenlist[pos];

    ast_node_ptr retlist = ast_node::make_ast_node(AST_FUNRETLIST,
                                                   opentok.startloc(),
                                                   opentok.startloc(),
                                                   opentok.endloc());
    cig_assert(retlist != nullptr);

    if (opentok.ttype == tokentype::OPENPAREN)
    {
        pos = pos + 1;

        for (bool first = true;; first = false)
        {
            token const &tok = tokenlist[pos];

            // End parsing at file-end or retlist-end paren
            if (tok.ttype == tokentype::FILEEND ||
                tok.ttype == tokentype::CLOSEPAREN)
                break;

            // Read the item-separating comma
            if (!first)
            {
                if (tok.ttype != tokentype::COMMA)
                    return std::unexpected(
                        parse_error(tok.sourceloc,
                                    err::EC_EXPECTING_COMMA_OR_END));
                pos = pos + 1;
            }

            token const &typetok = tokenlist[pos];
            if (!token_can_start_qualified_name(typetok))
            {
                return std::unexpected(
                    parse_error(tok.sourceloc,
                                err::EC_UNEXPECTED_TOKEN_IN_RETLIST));
            }

            auto expfunret = parse_funret(tokenlist,pos,pos);
            if (!expfunret)
                return expfunret;
            ast_node_ptr funret = std::move(expfunret.value());

            // Add the funret to the scope
            auto expindex = retlist->add_named_entity(funret);
            if (!expindex)
                return err_to_parse_error(expindex.error());
        }

        token const &closetok = tokenlist[pos];
        if (closetok.ttype == tokentype::CLOSEPAREN)
        {
            pos = pos + 1;
            retlist->set_endloc(closetok.endloc());
        }
        else
        {
            cig_assert(closetok.ttype == tokentype::FILEEND); /* follows
                                                                 from above */
            return std::unexpected(
                parse_error(opentok.sourceloc,
                            err::EC_RETLIST_NOT_CLOSED));
        }
    }
    else if (token_can_start_qualified_name(opentok))
    {
        auto expfunret = parse_funret(tokenlist,pos,pos);
        if (!expfunret)
            return expfunret;
        ast_node_ptr funret = std::move(expfunret.value());
        retlist->set_endloc(funret->get_endloc());

        // Add the funret to the scope
        auto expindex = retlist->add_named_entity(funret);
        if (!expindex)
            return err_to_parse_error(expindex.error());
    }
    else
    {
        return std::unexpected(parse_error(
                                   opentok.sourceloc,
                                   err::EC_EXPECTING_OPENPAREN_OR_TYPE));
    }


    // Successful return
    endpos = pos;
    return retlist;
}


// Parse in an enum and return before file-end or enum closing brace
static
parse_error
parse_in_enum(ast::ast_node_ptr outer,
              token_list const &tokenlist,
              i64 startpos,
              i64 &endpos)
{
    i64 pos = startpos;
    using namespace ast;
    using enum ast::ast_node_type;

    for (bool first = true;; first = false)
    {
        token const &tok = tokenlist[pos];

        // End parsing at file-end or class-end brace
        if (tok.ttype == tokentype::FILEEND ||
            tok.ttype == tokentype::CLOSEBRACE)
            break;

        // Read the item-separating comma
        if (!first)
        {
            if (tok.ttype != tokentype::COMMA)
                return parse_error(tok.sourceloc,
                                   err::EC_EXPECTING_COMMA_OR_END);
            pos = pos + 1;
        }

        token const &nametok = tokenlist[pos];

        // Read the identifier
        if (nametok.ttype == tokentype::IDENTIFIER)
        {
            str const &name = std::get<str>(nametok.value);
            pos = pos + 1;

            // Create the enum value
            ast_node_ptr sub = ast_node::make_ast_node(AST_ENUMVALUE,
                                                       nametok.startloc(),
                                                       nametok.startloc(),
                                                       nametok.endloc());
            cig_assert(sub != nullptr);
            auto exp1 = sub->set_name(name);
            if (!exp1)
                return parse_error(exp1.error());

            // Add the literal value if one is given
            if (tokenlist[pos].ttype == tokentype::ASSIGN)
            {
                token const &littok = tokenlist[pos + 1];
                if (littok.ttype > tokentype::LITERAL_MIN &&
                    littok.ttype < tokentype::LITERAL_MAX)
                {
                    auto exp = sub->set_literal(littok.value);
                    if (!exp)
                        return parse_error(exp.error());
                    pos = pos + 2;

                    sub->set_endloc(littok.endloc());
                }
                else
                {
                    return parse_error(littok.sourceloc,
                                       err::EC_EXPECTING_LITERAL);
                }
            }

            // Add the enum value to the enum node
            auto expindex = outer->add_named_entity(sub);
            if (!expindex)
                return parse_error(expindex.error());
        }

        // Anything else is an error
        else
        {
            return parse_error(nametok.sourceloc,
                               err::EC_EXPECTING_IDENTIFIER);
        }
    }

    // Here, the next token is a closing brace or end-of-file

    // Successful return
    endpos = pos;
    return no_parse_error;
}


// Parse in a class and return before file-end or class closing brace
static
parse_error
parse_in_class(ast::ast_node_ptr outer,
               token_list const &tokenlist,
               i64 startpos,
               i64 &endpos)
{
    i64 pos = startpos;
    using namespace ast;
    bool have_static = false;
    bool have_nonstatic = false;

    for (;;)
    {
        token const &tok = tokenlist[pos];

        // End parsing at file-end or class-end brace
        if (tok.ttype == tokentype::FILEEND ||
            tok.ttype == tokentype::CLOSEBRACE)
        {
            if (have_static || have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_UNEXPECTED_END_OF_CLASS);
            break;
        }

        else if (tok.ttype == tokentype::STATIC)
        {
            if (have_static)
                return parse_error(tok.sourceloc,
                                   err::EC_DUPLICATE_STATIC_SPECIFICATION);
            if (have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_CONTRADICTING_STATIC_SPECIFICATION);
            have_static = true;
            pos += 1;
        }

        else if (tok.ttype == tokentype::NONSTATIC)
        {
            if (have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_DUPLICATE_STATIC_SPECIFICATION);
            if (have_static)
                return parse_error(tok.sourceloc,
                                   err::EC_CONTRADICTING_STATIC_SPECIFICATION);
            have_nonstatic = true;
            pos += 1;
        }

        else if (tok.ttype == tokentype::CLASS)
        {
            auto expclass = parse_class(tokenlist,pos,pos,
                                        have_static,have_nonstatic);
            if (!expclass)
                return expclass.error();
            ast_node_ptr nd = std::move(expclass.value());
            auto expindex = outer->add_class(nd);
            if (!expindex)
                return parse_error(expindex.error());
            have_static = false;
            have_nonstatic = false;
        }
        else if (tok.ttype == tokentype::ENUM)
        {
            if (have_static || have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_ILLEGAL_STATIC_SPECIFICATION);
            auto expenum = parse_enum(tokenlist,pos,pos);
            if (!expenum)
                return expenum.error();
            ast_node_ptr nd = std::move(expenum.value());
            auto expindex = outer->add_enum(nd);
            if (!expindex)
                return parse_error(expindex.error());
        }
        else if (tok.ttype == tokentype::FUNCTION)
        {
            auto expfun = parse_function(tokenlist,pos,pos,
                                         have_static,have_nonstatic);
            if (!expfun)
                return expfun.error();
            ast_node_ptr function = std::move(expfun.value());
            auto expindex = outer->add_function(function);
            if (!expindex)
                return parse_error(expindex.error());
            have_static = false;
            have_nonstatic = false;
        }
        else if (token_can_start_qualified_name(tok))
        {
            auto expvar = parse_variable(tokenlist,pos,pos,
                                         have_static,have_nonstatic);
            if (!expvar)
                return expvar.error();
            ast_node_ptr variable = std::move(expvar.value());

            // Add the variable to the scope
            auto expindex = outer->add_named_entity(variable);
            if (!expindex)
                return parse_error(expindex.error());
            have_static = false;
            have_nonstatic = false;
        }

        // Anything else is an error
        else
        {
            return parse_error(tok.sourceloc,
                               err::EC_UNEXPECTED_TOKEN_IN_CLASS);
        }
    }

    // Here, the next token is a closing brace or end-of-file

    // Successful return
    endpos = pos;
    return no_parse_error;
}


// Parse in a namespace and return before file-end or namespace closing brace
static
parse_error
parse_in_namespace(ast::ast_node_ptr outer,
                   token_list const &tokenlist,
                   i64 startpos,
                   i64 &endpos)
{
    i64 pos = startpos;
    using namespace ast;
    bool have_static = false;
    bool have_nonstatic = false;

    for (;;)
    {
        token const &tok = tokenlist[pos];

        // End parsing at file-end or namespace-end brace
        if (tok.ttype == tokentype::FILEEND ||
            tok.ttype == tokentype::CLOSEBRACE)
        {
            if (have_static || have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_UNEXPECTED_END_OF_CLASS);
            break;
        }

        else if (tok.ttype == tokentype::STATIC)
        {
            if (have_static)
                return parse_error(tok.sourceloc,
                                   err::EC_DUPLICATE_STATIC_SPECIFICATION);
            if (have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_CONTRADICTING_STATIC_SPECIFICATION);
            have_static = true;
            pos += 1;
        }

        else if (tok.ttype == tokentype::NONSTATIC)
        {
            if (have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_DUPLICATE_STATIC_SPECIFICATION);
            if (have_static)
                return parse_error(tok.sourceloc,
                                   err::EC_CONTRADICTING_STATIC_SPECIFICATION);
            have_nonstatic = true;
            pos += 1;
        }

        else if (tok.ttype == tokentype::NAMESPACE)
        {
            if (have_static || have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_ILLEGAL_STATIC_SPECIFICATION);
            auto expnmsp = parse_namespace(outer,tokenlist,pos,endpos);
            if (!expnmsp)
                return expnmsp.error();
            pos = endpos;
        }
        else if (tok.ttype == tokentype::CLASS)
        {
            if (have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_NONSTATIC_TOPLEVEL_CLASS);
            auto expclass = parse_class(tokenlist,pos,pos,
                                        have_static,have_nonstatic);
            if (!expclass)
                return expclass.error();
            ast_node_ptr nd = std::move(expclass.value());
            auto expindex = outer->add_class(nd);
            if (!expindex)
                return parse_error(expindex.error());
            have_static = false;
            have_nonstatic = false;
        }
        else if (tok.ttype == tokentype::ENUM)
        {
            if (have_static || have_nonstatic)
                return parse_error(tok.sourceloc,
                                   err::EC_ILLEGAL_STATIC_SPECIFICATION);
            auto expenum = parse_enum(tokenlist,pos,pos);
            if (!expenum)
                return expenum.error();
            ast_node_ptr nd = std::move(expenum.value());
            auto expindex = outer->add_enum(nd);
            if (!expindex)
                return parse_error(expindex.error());
        }
        else if (tok.ttype == tokentype::FUNCTION)
        {
            auto expfun = parse_function(tokenlist,pos,pos,
                                         have_static,have_nonstatic);
            if (!expfun)
                return expfun.error();
            ast_node_ptr function = std::move(expfun.value());
            auto expindex = outer->add_function(function);
            if (!expindex)
                return parse_error(expindex.error());
            have_static = false;
            have_nonstatic = false;
        }

        // Anything else is an error
        else
        {
            return parse_error(tok.sourceloc,
                               err::EC_UNEXPECTED_TOKEN_IN_NAMESPACE);
        }
    }

    // Here, the next token is a closing brace or end-of-file

    // Successful return
    endpos = pos;
    return no_parse_error;
}


//
// Parse various statements and expressions
//

static
ast_node_or_parse_error
parse_primary_expression(token_list const &tokenlist,
                         i64 startpos,
                         i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;
    token const &tok = tokenlist[pos];
    bool allow_vardecl = true;

    if (tok.ttype == tokentype::OPENPAREN)
    {
        pos = pos + 1;

        auto exp1 = parse_expression(tokenlist,pos,pos);
        if (!exp1)
            return exp1;
        
        token const &endtok = tokenlist[pos];
        if (endtok.ttype != tokentype::CLOSEPAREN)
        {
            return std::unexpected(parse_error(endtok.sourceloc,
                                               err::EC_EXPECTING_CLOSEPAREN));
        }

        pos = pos + 1;
        endpos = pos;
        return exp1;
    }
    else if (tokentype_is_literal(tok.ttype))
    {
        ast_node_ptr nd = ast_node::make_ast_node(AST_EXPR_LITERAL,
                                                  tok.startloc(),
                                                  tok.startloc(),
                                                  tok.endloc());
        cig_assert(nd != nullptr);
        auto exp1 = nd->set_literal(std::move(tok.value));
        if (!exp1)
            return err_to_parse_error(exp1.error());
        endpos = pos + 1;
        return nd;
    }
    else if (token_can_start_qualified_name(tok))
    {
        // This also parses member lookups but only within types or variables,
        // not within function return values.
        auto exp1 = parse_qualified_name(tokenlist,pos,pos);
        if (!exp1)
            return exp1;
        ast_node_ptr qualified_name = std::move(exp1.value());
        token const &nexttok = tokenlist[pos];

        // fixme probably allow_vardecl should be removed altogether,
        // it is not possible to determine whether a variable declaration
        // is allowed during parsing left-to-right without lookahead
        if (allow_vardecl &&
            (nexttok.ttype == tokentype::IDENTIFIER ||
             nexttok.ttype == tokentype::REFPTR ||
             nexttok.ttype == tokentype::WEAKPTR ||
             nexttok.ttype == tokentype::SHAREDPTR ||
             nexttok.ttype == tokentype::NONNULLPTR))
        {
            // Make the qualified name into a typeref as in parse_type_ref
            ast_node_ptr typeref =
                ast_node::make_ast_node(AST_TYPEREF,
                                        qualified_name->get_startloc(),
                                        qualified_name->get_startloc(),
                                        qualified_name->get_endloc());
            cig_assert(typeref != nullptr);
            auto exp2 = typeref->set_ref(qualified_name);
            if (!exp2)
                return err_to_parse_error(exp2.error());

            // Create a variable declaration from the typeref
            cig::source_location sloc;
            auto exp3 = parse_variable_after_typeref(tokenlist,typeref,
                                                     sloc,pos,pos);
            if (!exp3)
                return exp3;
            ast_node_ptr variable = exp3.value();
            endpos = pos;
            return variable;
        }
        else
        {
            // Return the qualified name.
            // We do not know yet whether it is a variable or function.
            endpos = pos;
            return qualified_name;
        }
    }

    return std::unexpected(parse_error(tok.sourceloc,
                                       err::EC_EXPECTING_EXPRESSION));
}

static
ast_node_or_parse_error
parse_postfix_expression(token_list const &tokenlist,
                         i64 startpos,
                         i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_primary_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr nd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        // Member lookup
        if (tok.ttype == tokentype::DOT)
        {
            token const &idtok = tokenlist[pos + 1];
            if (idtok.ttype != tokentype::IDENTIFIER)
                return std::unexpected(
                    parse_error(idtok.sourceloc,
                                err::EC_EXPECTING_MEMBER_NAME));
            str const &idname = std::get<str>(idtok.value);

            // Create a new expression for the dot operation
            ast_node_ptr oldnd = std::move(nd);
            nd = ast_node::make_ast_node(AST_EXPR_MEMBER,
                                         oldnd->get_startloc(),
                                         idtok.startloc(),
                                         idtok.endloc());
            cig_assert(nd != nullptr);
            auto exp2 = nd->set_subexpr1(oldnd);
            if (!exp2)
                return err_to_parse_error(exp2.error());
            auto exp3 = nd->set_member_name(idname);
            if (!exp3)
                return err_to_parse_error(exp3.error());

            pos = pos + 2;
        }

        // Array dereference or slice expression
        else if (tok.ttype == tokentype::OPENBRACKET)
        {
            pos = pos + 1;
            ast_node_ptr firstidx = nullptr;
            if (tokenlist[pos].ttype != tokentype::COLON) // maybe first colon
            {
                // Read the array index or the first slice argument
                auto exp2 = parse_expression(tokenlist,pos,pos);
                if (!exp2)
                    return exp2;
                firstidx = std::move(exp2.value());
            }

            // Slice expression
            token const &firstcol = tokenlist[pos];
            if (firstcol.ttype == tokentype::COLON) // the first colon
            {
                pos = pos + 1;
                ast_node_ptr secondidx = nullptr;
                ast_node_ptr thirdidx = nullptr;
                token const &midtok = tokenlist[pos]; // maybe second colon 
                if (midtok.ttype != tokentype::COLON &&
                    midtok.ttype != tokentype::CLOSEBRACKET)
                {
                    auto exp3 = parse_expression(tokenlist,pos,pos);
                    if (!exp3)
                        return exp3;
                    secondidx = std::move(exp3.value());
                }
                token const &secondcol = tokenlist[pos];
                if (secondcol.ttype == tokentype::COLON) // the second colon
                {
                    pos = pos + 1;
                    if (tokenlist[pos].ttype != tokentype::CLOSEBRACKET)
                    {
                        auto exp4 = parse_expression(tokenlist,pos,pos);
                        if (!exp4)
                            return exp4;
                        thirdidx = std::move(exp4.value());
                    }
                }

                token const &closetok = tokenlist[pos];
                if (closetok.ttype != tokentype::CLOSEBRACKET)
                    return std::unexpected(
                        parse_error(closetok.sourceloc,
                                    err::EC_EXPECTING_CLOSEBRACKET));
                pos = pos + 1;

                ast_node_ptr arrnd = std::move(nd);
                nd = ast_node::make_ast_node(AST_EXPR_SLICE,
                                             arrnd->get_startloc(),
                                             tok.startloc(),
                                             closetok.endloc());
                cig_assert(nd != nullptr);
                auto exp5 = nd->set_subexpr1(arrnd);
                if (!exp5)
                    return err_to_parse_error(exp5.error());
                if (firstidx != nullptr)
                {
                    auto exp6 = nd->set_subexpr2(firstidx);
                    if (!exp6)
                        return err_to_parse_error(exp6.error());
                }
                if (secondidx != nullptr)
                {
                    auto exp7 = nd->set_subexpr3(secondidx);
                    if (!exp7)
                        return err_to_parse_error(exp7.error());
                }
                if (thirdidx != nullptr)
                {
                    auto exp8 = nd->set_subexpr4(thirdidx);
                    if (!exp8)
                        return err_to_parse_error(exp8.error());
                }
            }

            // Array index expression
            else
            {
                token const &closetok = tokenlist[pos];
                if (closetok.ttype != tokentype::CLOSEBRACKET)
                    return std::unexpected(
                        parse_error(closetok.sourceloc,
                                    err::EC_EXPECTING_CLOSEBRACKET));
                pos = pos + 1;

                ast_node_ptr arrnd = std::move(nd);
                nd = ast_node::make_ast_node(AST_EXPR_ARRAYDEREF,
                                             arrnd->get_startloc(),
                                             tok.startloc(),
                                             closetok.endloc());
                cig_assert(nd != nullptr);
                auto exp3 = nd->set_subexpr1(arrnd);
                if (!exp3)
                    return err_to_parse_error(exp3.error());
                auto exp4 = nd->set_subexpr2(firstidx);
                if (!exp4)
                    return err_to_parse_error(exp4.error());
            }
        }

        // Function call
        else if (tok.ttype == tokentype::OPENPAREN)
        {
            // A function call cannot follow an array dereference
            // nor another function call
            ast_node_type nt = nd->get_node_type();
            if (nt == AST_EXPR_ARRAYDEREF)
                return std::unexpected(
                    parse_error(tok.sourceloc,
                                err::EC_FUNCALL_AFTER_ARRAY_NOT_POSSIBLE));
            if (nt == AST_EXPR_FUNCALL)
                return std::unexpected(
                    parse_error(tok.sourceloc,
                                err::EC_FUNCALL_AFTER_FUNCALL_NOT_POSSIBLE));

            pos = pos + 1;
            token const &nexttok = tokenlist[pos];
            ast_node_ptr arglist = nullptr;
            if (nexttok.ttype != tokentype::CLOSEPAREN)
            {
                // fixme named function arguments:
                // make a separate parse_named_funargs function
                // that allows names in function arguments
                auto exp2 = parse_expression(tokenlist,pos,pos);
                if (!exp2)
                    return exp2;
                arglist = std::move(exp2.value());
            }
            token const &closetok = tokenlist[pos];
            if (closetok.ttype != tokentype::CLOSEPAREN)
                return std::unexpected(
                    parse_error(closetok.sourceloc,
                                err::EC_EXPECTING_CLOSEPAREN));
            pos = pos + 1;

            ast_node_ptr funcnd = std::move(nd);
            nd = ast_node::make_ast_node(AST_EXPR_FUNCALL,
                                         funcnd->get_startloc(),
                                         tok.startloc(),
                                         closetok.endloc());
            cig_assert(nd != nullptr);
            auto exp3 = nd->set_subexpr1(funcnd);
            if (!exp3)
                return err_to_parse_error(exp3.error());
            if (arglist != nullptr)
            {
                auto exp4 = nd->set_subexpr2(arglist);
                if (!exp4)
                    return err_to_parse_error(exp4.error());
            }
        }

        else
            break;
    }

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_unary_expression(token_list const &tokenlist,
                       i64 startpos,
                       i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    token const &tok = tokenlist[startpos];

    ast_node_type t = AST_INVALID;
    if (tok.ttype == tokentype::PLUS)
        t = AST_EXPR_SIGNPLUS;
    else if (tok.ttype == tokentype::MINUS)
        t = AST_EXPR_SIGNMINUS;
    else if (tok.ttype == tokentype::BIT_NOT)
        t = AST_EXPR_BIT_NOT;
    else if (tok.ttype == tokentype::LOGICAL_NOT)
        t = AST_EXPR_LOGICAL_NOT;
    else if (tok.ttype == tokentype::KWD_NOT)
        t = AST_EXPR_LOGICAL_NOT;

    if (t != AST_INVALID)
    {
        auto exp1 = parse_unary_expression(tokenlist,startpos + 1,endpos);
        if (!exp1)
            return exp1;
        ast_node_ptr first = std::move(exp1.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  tok.startloc(),
                                                  tok.startloc(),
                                                  first->get_endloc());
        cig_assert(nd != nullptr);
        auto exp2 = nd->set_subexpr1(first);
        if (!exp2)
            return err_to_parse_error(exp2.error());
        // endpos already set
        return nd;
    }
    // fixme type conversion? that is rather a function call
    // fixme operator new?
    else
        return parse_postfix_expression(tokenlist,startpos,endpos);
}


static
ast_node_or_parse_error
parse_cast_expression(token_list const &tokenlist,
                      i64 startpos,
                      i64 &endpos)
{
    // fixme is that even necessary?
    i64 pos = startpos;
    return parse_unary_expression(tokenlist,pos,endpos);
}


static
ast_node_or_parse_error
parse_multiplicative_expression(token_list const &tokenlist,
                                i64 startpos,
                                i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_cast_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::MULT)
            t = AST_EXPR_MULT;
        else if (tok.ttype == tokentype::DIV)
            t = AST_EXPR_DIV;
        else if (tok.ttype == tokentype::MOD)
            t = AST_EXPR_MOD;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_cast_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_additive_expression(token_list const &tokenlist,
                          i64 startpos,
                          i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_multiplicative_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::PLUS)
            t = AST_EXPR_ADD;
        else if (tok.ttype == tokentype::MINUS)
            t = AST_EXPR_SUB;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_multiplicative_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_shift_expression(token_list const &tokenlist,
                       i64 startpos,
                       i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_additive_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::SHIFT_LEFT)
            t = AST_EXPR_SHIFT_LEFT;
        else if (tok.ttype == tokentype::SHIFT_RIGHT)
            t = AST_EXPR_SHIFT_RIGHT;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_additive_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_relational_expression(token_list const &tokenlist,
                            i64 startpos,
                            i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_shift_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::LESS)
            t = AST_EXPR_LESS;
        else if (tok.ttype == tokentype::GREATER)
            t = AST_EXPR_GREATER;
        else if (tok.ttype == tokentype::LESS_EQUAL)
            t = AST_EXPR_LESS_EQUAL;
        else if (tok.ttype == tokentype::GREATER_EQUAL)
            t = AST_EXPR_GREATER_EQUAL;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_shift_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_equality_expression(token_list const &tokenlist,
                          i64 startpos,
                          i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_relational_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::EQUAL)
            t = AST_EXPR_EQUAL;
        else if (tok.ttype == tokentype::NOT_EQUAL)
            t = AST_EXPR_NOT_EQUAL;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_relational_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_in_expression(token_list const &tokenlist,
                     i64 startpos,
                     i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_equality_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::KWD_IN)
            t = AST_EXPR_IN;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_equality_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_and_expression(token_list const &tokenlist,
                     i64 startpos,
                     i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_in_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::BIT_AND)
            t = AST_EXPR_AND;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_in_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_exclusive_or_expression(token_list const &tokenlist,
                              i64 startpos,
                              i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_and_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::BIT_XOR)
            t = AST_EXPR_XOR;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_and_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_inclusive_or_expression(token_list const &tokenlist,
                              i64 startpos,
                              i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_exclusive_or_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::BIT_OR)
            t = AST_EXPR_OR;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_exclusive_or_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_logical_and_expression(token_list const &tokenlist,
                             i64 startpos,
                             i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_inclusive_or_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::LOGICAL_AND ||
            tok.ttype == tokentype::KWD_AND)
            t = AST_EXPR_LOGICAL_AND;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_inclusive_or_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_logical_or_expression(token_list const &tokenlist,
                            i64 startpos,
                            i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_logical_and_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    for (;;)
    {
        token const &tok = tokenlist[pos];

        ast_node_type t = AST_INVALID;
        if (tok.ttype == tokentype::LOGICAL_OR ||
            tok.ttype == tokentype::KWD_OR)
            t = AST_EXPR_LOGICAL_OR;

        if (t == AST_INVALID)
            break;

        auto exp2 = parse_logical_and_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_conditional_expression(token_list const &tokenlist,
                             i64 startpos,
                             i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;
    auto exp1 = parse_logical_or_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    
    token const &tok = tokenlist[pos];

    if (tok.ttype != tokentype::QUEST)
    {
        endpos = pos;
        return exp1;
    }

    // Note: The expression before the colon could be any top-level expression,
    // since it is unambiguously delimited by the colong. It could even be
    // a list expression.
    // However, the expression after the colon must be a conditional expression
    // or lower level expression, otherwise an ambiguity ensues whether the
    // operator (e.g. comma) separates list elements of the second alternative
    // in the conditional expresssion or whether it terminates the conditional
    // expression and starts the following expression.
    // The C syntax allows everything before the colon, even assignments.
    // However, here we prefer to allow the same kind of expression before
    // and after the colon, therefore the expressions on both sides of the
    // colon must be conditional expressions again.
    // Any list intended to be in one of the alternatives of the conditional
    // must therefore be enclosed in parentheses.

    ast_node_ptr first = std::move(exp1.value());

    auto exp2 = parse_conditional_expression(tokenlist,pos+1,pos);
    if (!exp2)
        return exp2;
    ast_node_ptr second = std::move(exp2.value());

    token const &coltok = tokenlist[pos];

    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(
            parse_error(coltok.sourceloc,err::EC_EXPECTING_CONDITIONAL_COLON));

    auto exp3 = parse_conditional_expression(tokenlist,pos+1,pos);
    if (!exp3)
        return exp3;
    ast_node_ptr third = std::move(exp3.value());

    ast_node_ptr nd = ast_node::make_ast_node(AST_EXPR_CONDITION,
                                              first->get_startloc(),
                                              tok.startloc(),
                                              third->get_endloc());
    cig_assert(nd != nullptr);
    nd->set_effectloc2(coltok.startloc());

    auto exp4 = nd->set_subexpr1(first);
    if (!exp4)
        return err_to_parse_error(exp4.error());
    auto exp5 = nd->set_subexpr2(second);
    if (!exp5)
        return err_to_parse_error(exp5.error());
    auto exp6 = nd->set_subexpr3(third);
    if (!exp6)
        return err_to_parse_error(exp6.error());

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_list_expression(token_list const &tokenlist,
                      i64 startpos,
                      i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_conditional_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::COMMA) // No list
    {
        endpos = pos;
        return retnd;
    }

    // List
    ast_node_ptr listnd = ast_node::make_ast_node(AST_EXPR_LIST,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  tok.endloc()); // for now
    cig_assert(listnd != nullptr);
    auto expindex1 = listnd->add_listsubexpr(retnd);
    if (!expindex1)
        return std::unexpected(parse_error(tok.sourceloc,
                                           expindex1.error()));
    i64 index1 = expindex1.value();
    cig_assert(index1 != -1);

    for (;;)
    {
        token const &commatok = tokenlist[pos];
        if (commatok.ttype != tokentype::COMMA)
            break;

        auto exp2 = parse_conditional_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr nextnd = std::move(exp2.value());
        listnd->set_endloc(nextnd->get_endloc());
        auto expindex = listnd->add_listsubexpr(nextnd);
        if (!expindex)
            return std::unexpected(parse_error(commatok.sourceloc,
                                               expindex.error()));
        i64 index = expindex.value();
        cig_assert(index != -1);
    }

    // Set the list end
    auto expcomplete = listnd->set_list_complete();
    if (!expcomplete)
        return err_to_parse_error(expcomplete.error());

    endpos = pos;
    return listnd;
}


static
ast_node_or_parse_error
parse_expression(token_list const &tokenlist,
                 i64 startpos,
                 i64 &endpos)
{
    i64 pos = startpos;

    return parse_list_expression(tokenlist,pos,endpos);
}


static
ast_node_or_parse_error
parse_assignment_expression(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr retnd = std::move(exp1.value());

    token const &tok = tokenlist[pos];

    ast_node_type t = AST_INVALID;
    if (tok.ttype == tokentype::SHL_ASSIGN)
        t = AST_EXPR_SHL_ASSIGN;
    else if (tok.ttype == tokentype::SHR_ASSIGN)
        t = AST_EXPR_SHR_ASSIGN;
    else if (tok.ttype == tokentype::PLUS_ASSIGN)
        t = AST_EXPR_ADD_ASSIGN;
    else if (tok.ttype == tokentype::MINUS_ASSIGN)
        t = AST_EXPR_SUB_ASSIGN;
    else if (tok.ttype == tokentype::MULT_ASSIGN)
        t = AST_EXPR_MULT_ASSIGN;
    else if (tok.ttype == tokentype::DIV_ASSIGN)
        t = AST_EXPR_DIV_ASSIGN;
    else if (tok.ttype == tokentype::MOD_ASSIGN)
        t = AST_EXPR_MOD_ASSIGN;
    else if (tok.ttype == tokentype::AND_ASSIGN)
        t = AST_EXPR_AND_ASSIGN;
    else if (tok.ttype == tokentype::XOR_ASSIGN)
        t = AST_EXPR_XOR_ASSIGN;
    else if (tok.ttype == tokentype::OR_ASSIGN)
        t = AST_EXPR_OR_ASSIGN;
    else if (tok.ttype == tokentype::DOT_ASSIGN)
        t = AST_EXPR_DOT_ASSIGN;
    else if (tok.ttype == tokentype::ASSIGN)
        t = AST_EXPR_ASSIGN;

    if (t != AST_INVALID)
    {
        auto exp2 = parse_list_expression(tokenlist,pos + 1,pos);
        if (!exp2)
            return exp2;
        ast_node_ptr second = std::move(exp2.value());
        ast_node_ptr nd = ast_node::make_ast_node(t,
                                                  retnd->get_startloc(),
                                                  tok.startloc(),
                                                  second->get_endloc());
        cig_assert(nd != nullptr);
        auto exp3 = nd->set_subexpr1(retnd);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = nd->set_subexpr2(second);
        if (!exp4)
            return err_to_parse_error(exp4.error());
        retnd = std::move(nd);
    }

    endpos = pos;
    return retnd;
}


static
ast_node_or_parse_error
parse_expression_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    auto exp1 = parse_assignment_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr first = std::move(exp1.value());

    token const &semitok = tokenlist[pos];
    if (semitok.ttype != tokentype::SEMICOLON)
        return std::unexpected(parse_error(semitok.sourceloc,
                                           err::EC_EXPECTING_SEMICOLON));
    pos = pos + 1;

    ast_node_ptr nd = ast_node::make_ast_node(AST_EXPRSTMT,
                                              first->get_startloc(),
                                              first->get_effectloc(),
                                              semitok.endloc());
    cig_assert(nd != nullptr);
    auto exp2 = nd->set_expr(first);
    if (!exp2)
        return std::unexpected(parse_error(semitok.sourceloc,exp2.error()));


    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_return_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &tok = tokenlist[pos];

    if (tok.ttype != tokentype::RETURN)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));

    pos = pos + 1;

    auto exp1 = parse_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr first = std::move(exp1.value());

    token const &semitok = tokenlist[pos];
    if (semitok.ttype != tokentype::SEMICOLON)
        return std::unexpected(parse_error(semitok.sourceloc,
                                           err::EC_EXPECTING_SEMICOLON));
    pos = pos + 1;

    ast_node_ptr nd = ast_node::make_ast_node(AST_RETURNSTMT,
                                              tok.startloc(),
                                              first->get_startloc(),
                                              semitok.endloc());
    cig_assert(nd != nullptr);

    auto exp2 = nd->set_expr(first);
    if (!exp2)
        return err_to_parse_error(exp2.error());

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_if_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    // Keyword if
    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::KWD_IF)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));
    pos = pos + 1;

    // Parse the first expression
    auto exp1 = parse_assignment_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr first_expr = std::move(exp1.value());

    // The first_expr may be the init expression
    // or the condition of the if statement.

    ast_node_ptr init_stmt;
    ast_node_ptr cond;
    token const &init_semicol = tokenlist[pos];
    if (init_semicol.ttype == tokentype::SEMICOLON)
    {
        // The first expression was the init expression
        ast_node_ptr init_expr = std::move(first_expr);

        init_stmt = ast_node::make_ast_node(
            AST_EXPRSTMT,
            init_expr->get_startloc(),
            init_expr->get_effectloc(),
            init_semicol.endloc());
        cig_assert(init_stmt != nullptr);

        auto exp2 = init_stmt->set_expr(init_expr);
        if (!exp2)
            return err_to_parse_error(exp2.error());


        // We have to parse the second expression
        pos += 1;
        auto exp3 = parse_assignment_expression(tokenlist,pos,pos);
        if (!exp3)
            return exp3;
        cond = std::move(exp3.value());
    }
    else
        cond = std::move(first_expr);

    // Now the first_stmt is either the init expression statement or empty.
    // The cond is set.

    // Colon
    token const &coltok = tokenlist[pos];
    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(parse_error(coltok.sourceloc,
                                           err::EC_EXPECTING_IF_COLON));
    pos = pos + 1;

    // Check the condition
    if (cond->declares_variables_visible_from_outside())
    {
        return std::unexpected(
            parse_error(cond->get_startloc(),
                        err::EC_IF_CONDITION_DISALLOWS_VARDECL));
    }
    if (ast_node::ast_node_type_is_assign_expr(cond->get_node_type()))
    {
        return std::unexpected(
            parse_error(cond->get_effectloc(),
                        err::EC_IF_CONDITION_DISALLOWS_ASSIGNMENT));
    }

    // Then
    auto exp4 = parse_statement(tokenlist,pos,pos);
    if (!exp4)
        return exp4;
    ast_node_ptr thenstmt = std::move(exp4.value());
    cig::source_location endloc = thenstmt->get_endloc();
    // Then statement is checked when inserted

    // Else
    ast_node_ptr elsestmt = nullptr;
    token const &elsetok = tokenlist[pos];
    if (elsetok.ttype == tokentype::KWD_ELSE)
    {
        token const &coltok2 = tokenlist[pos+1];
        if (coltok2.ttype == tokentype::COLON)
        {
            token const &nexttok = tokenlist[pos+2];
            if (nexttok.ttype == tokentype::KWD_IF)
                return std::unexpected(parse_error(
                                           coltok2.sourceloc,
                                           err::EC_NO_COLON_IN_ELSE_IF));
            pos = pos + 2;
        }
        else
        {
            if (coltok2.ttype != tokentype::KWD_IF)
                return std::unexpected(parse_error(
                                           coltok2.sourceloc,
                                           err::EC_EXPECTING_ELSE_COLON));
            pos = pos + 1;
        }
        auto exp5 = parse_statement(tokenlist,pos,pos);
        if (!exp5)
            return exp5;
        elsestmt = std::move(exp5.value());
        endloc = elsestmt->get_endloc();
        // Else statement is checked when inserted
    }

    // Create the statement
    ast_node_ptr nd = ast_node::make_ast_node(AST_IFSTMT,
                                              tok.startloc(),
                                              tok.startloc(),
                                              endloc);
    cig_assert(nd != nullptr);

    // Insert components
    if (init_stmt)
    {
        auto exp6 = nd->set_initstmt(init_stmt);
        if (!exp6)
            return err_to_parse_error(exp6.error());
    }
    auto exp7 = nd->set_condexpr(cond);
    if (!exp7)
        return err_to_parse_error(exp7.error());
    auto exp8 = nd->set_thenstmt(thenstmt);
    if (!exp8)
        return err_to_parse_error(exp8.error());
    if (elsestmt != nullptr)
    {
        auto exp9 = nd->set_elsestmt(elsestmt);
        if (!exp9)
            return err_to_parse_error(exp9.error());
    }

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_while_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    // Keyword while
    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::KWD_WHILE)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));
    pos = pos + 1;

    // Parse the first expression
    auto exp1 = parse_assignment_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr first_expr = std::move(exp1.value());

    // The first_expr may be the init expression
    // or the condition of the while statement.

    ast_node_ptr init_stmt;
    ast_node_ptr cond;
    token const &init_semicol = tokenlist[pos];
    if (init_semicol.ttype == tokentype::SEMICOLON)
    {
        // The first expression was the init expression
        ast_node_ptr init_expr = std::move(first_expr);

        init_stmt = ast_node::make_ast_node(
            AST_EXPRSTMT,
            init_expr->get_startloc(),
            init_expr->get_effectloc(),
            init_semicol.endloc());
        cig_assert(init_stmt != nullptr);

        auto exp2 = init_stmt->set_expr(init_expr);
        if (!exp2)
            return err_to_parse_error(exp2.error());


        // We have to parse the second expression
        pos += 1;
        auto exp3 = parse_assignment_expression(tokenlist,pos,pos);
        if (!exp3)
            return exp3;
        cond = std::move(exp3.value());
    }
    else
        cond = std::move(first_expr);

    // Now the first_stmt is either the init expression statement or empty.
    // The cond is set.

    // Colon
    token const &coltok = tokenlist[pos];
    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(parse_error(coltok.sourceloc,
                                           err::EC_EXPECTING_WHILE_COLON));
    pos = pos + 1;

    // Check the condition
    if (cond->declares_variables_visible_from_outside())
    {
        return std::unexpected(
            parse_error(cond->get_startloc(),
                        err::EC_WHILE_CONDITION_DISALLOWS_VARDECL));
    }
    if (ast_node::ast_node_type_is_assign_expr(cond->get_node_type()))
    {
        return std::unexpected(
            parse_error(cond->get_effectloc(),
                        err::EC_WHILE_CONDITION_DISALLOWS_ASSIGNMENT));
    }

    // Body
    auto exp4 = parse_statement(tokenlist,pos,pos);
    if (!exp4)
        return exp4;
    ast_node_ptr bodystmt = std::move(exp4.value());
    // Body is checked when inserted

    // Create the statement
    ast_node_ptr nd = ast_node::make_ast_node(AST_WHILESTMT,
                                              tok.startloc(),
                                              tok.startloc(),
                                              bodystmt->get_endloc());
    cig_assert(nd != nullptr);

    // Insert components
    if (init_stmt)
    {
        auto exp5 = nd->set_initstmt(init_stmt);
        if (!exp5)
            return err_to_parse_error(exp5.error());
    }
    auto exp6 = nd->set_condexpr(cond);
    if (!exp6)
        return err_to_parse_error(exp6.error());
    auto exp7 = nd->set_bodystmt(bodystmt);
    if (!exp7)
        return err_to_parse_error(exp7.error());

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_do_while_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &tok = tokenlist[pos];

    // Keyword do
    if (tok.ttype != tokentype::KWD_DO)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));
    pos = pos + 1;

    // Colon after do
    token const docoltok = tokenlist[pos];
    if (docoltok.ttype != tokentype::COLON)
        return std::unexpected(parse_error(docoltok.sourceloc,
                                           err::EC_EXPECTING_DO_COLON));
    pos = pos + 1;

    // Body
    auto exp2 = parse_statement(tokenlist,pos,pos);
    if (!exp2)
        return exp2;
    ast_node_ptr bodystmt = std::move(exp2.value());

    // Keyword while
    token const &whiletok = tokenlist[pos];
    if (whiletok.ttype != tokentype::KWD_WHILE)
        return std::unexpected(parse_error(whiletok.sourceloc,
                                           err::EC_EXPECTING_WHILE));
    pos = pos + 1;

    // Condition
    auto exp1 = parse_assignment_expression(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr cond = std::move(exp1.value());

    // Semicolon
    token const &semitok = tokenlist[pos];
    if (semitok.ttype != tokentype::SEMICOLON)
        return std::unexpected(parse_error(semitok.sourceloc,
                                           err::EC_EXPECTING_SEMICOLON));
    pos = pos + 1;

    // Check the condition
    if (cond->declares_variables_visible_from_outside())
    {
        return std::unexpected(
            parse_error(cond->get_startloc(),
                        err::EC_DOWHILE_CONDITION_DISALLOWS_VARDECL));
    }
    if (ast_node::ast_node_type_is_assign_expr(cond->get_node_type()))
    {
        return std::unexpected(
            parse_error(cond->get_effectloc(),
                        err::EC_DOWHILE_CONDITION_DISALLOWS_ASSIGNMENT));
    }

    // Body is checked when inserted

    // Create the statement
    ast_node_ptr nd = ast_node::make_ast_node(AST_WHILESTMT,
                                              tok.startloc(),
                                              tok.startloc(),
                                              semitok.endloc());
    cig_assert(nd != nullptr);

    // Insert components
    auto exp4 = nd->set_condexpr(cond);
    if (!exp4)
        return err_to_parse_error(exp4.error());
    auto exp5 = nd->set_bodystmt(bodystmt);
    if (!exp5)
        return err_to_parse_error(exp5.error());

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_for_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    // Keyword for
    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::KWD_FOR)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));
    pos += 1;


    // Parse the first expression
    ast_node_ptr first_expr;
    token const &empty_semicol = tokenlist[pos];
    if (empty_semicol.ttype != tokentype::SEMICOLON)
    {
        auto exp1 = parse_assignment_expression(tokenlist,pos,pos);
        if (!exp1)
            return exp1;
        first_expr = std::move(exp1.value());
    }

    // The first_expr may be the init expression
    // or the iterator of a for..of statement.

    ast_node_ptr init_stmt;
    ast_node_ptr second_expr;
    token const &init_semicol = tokenlist[pos];
    if (init_semicol.ttype == tokentype::SEMICOLON)
    {
        // The first expression was the init expression
        ast_node_ptr init_expr = std::move(first_expr);

        init_stmt = ast_node::make_ast_node(
            AST_EXPRSTMT,
            init_expr->get_startloc(),
            init_expr->get_effectloc(),
            init_semicol.endloc());
        cig_assert(init_stmt != nullptr);

        auto exp2 = init_stmt->set_expr(init_expr);
        if (!exp2)
            return err_to_parse_error(exp2.error());


        // We have to parse the second expression
        pos += 1;
        token const &empty_semicol2 = tokenlist[pos];
        if (empty_semicol2.ttype != tokentype::SEMICOLON)
        {
            auto exp3 = parse_assignment_expression(tokenlist,pos,pos);
            if (!exp3)
                return exp3;
            second_expr = std::move(exp3.value());
        }
    }
    else if (init_semicol.ttype == tokentype::KWD_OF)
    {
        // There is no init expression
        // and this was already the second expression (the iterator)
        second_expr = std::move(first_expr);
    }
    else
    {
        return std::unexpected(
            parse_error(init_semicol.sourceloc,
                        err::EC_EXPECTING_SEMICOLON_OR_OF));
    }

    // Now the first_stmt is either the init expression statement or empty.
    // The second_expr is set.

    token const &semicol_or_of = tokenlist[pos];
    pos += 1;

    // If we do not have a semicolon or the keyword of,
    // that is an error
    if (semicol_or_of.ttype != tokentype::SEMICOLON &&
        semicol_or_of.ttype != tokentype::KWD_OF)
    {
        return std::unexpected(
            parse_error(semicol_or_of.sourceloc,
                        err::EC_EXPECTING_SEMICOLON_OR_OF));
    }


    // The traditional C-style syntax but without parentheses
    if (semicol_or_of.ttype == tokentype::SEMICOLON)
    {
        // Check the condition (the second expression)
        if (second_expr->declares_variables_visible_from_outside())
        {
            return std::unexpected(
                parse_error(second_expr->get_startloc(),
                            err::EC_FOR_CONDITION_DISALLOWS_VARDECL));
        }
        if (ast_node::ast_node_type_is_assign_expr(
                second_expr->get_node_type()))
        {
            return std::unexpected(
                parse_error(second_expr->get_effectloc(),
                            err::EC_FOR_CONDITION_DISALLOWS_ASSIGNMENT));
        }

        // Iterate expression
        ast_node_ptr itexpr;
        if (tokenlist[pos].ttype != tokentype::COLON)
        {
            auto exp4 = parse_assignment_expression(tokenlist,pos,pos);
            if (!exp4)
                return exp4;
            itexpr = std::move(exp4.value());
        }

        // Colon
        token const &coltok = tokenlist[pos];
        if (coltok.ttype != tokentype::COLON)
            return std::unexpected(parse_error(coltok.sourceloc,
                                               err::EC_EXPECTING_FOR_COLON));
        pos = pos + 1;

        // Check the iterate expression
        if (itexpr->declares_variables_visible_from_outside())
        {
            return std::unexpected(
                parse_error(itexpr->get_startloc(),
                            err::EC_ITERATEEXPR_DISALLOWS_VARDECL));
        }

        // Body
        auto exp5 = parse_statement(tokenlist,pos,pos);
        if (!exp5)
            return exp5;
        ast_node_ptr bodystmt = std::move(exp5.value());
        // Body is checked when inserted

        // Create the statement
        ast_node_ptr nd = ast_node::make_ast_node(AST_FORSTMT,
                                                  tok.startloc(),
                                                  tok.startloc(),
                                                  bodystmt->get_endloc());
        cig_assert(nd != nullptr);

        // Insert components
        if (init_stmt != nullptr)
        {
            auto exp6 = nd->set_initstmt(init_stmt);
            if (!exp6)
                return err_to_parse_error(exp6.error());
        }

        if (second_expr != nullptr)
        {
            // fixme check the expression, it must not have variables
            // and it must not be an assignment
            auto exp7 = nd->set_condexpr(second_expr);
            if (!exp7)
                return err_to_parse_error(exp7.error());
        }

        if (itexpr != nullptr)
        {
            // fixme check the expression, it must not have variables
            auto exp8 = nd->set_iterateexpr(itexpr);
            if (!exp8)
                return err_to_parse_error(exp8.error());
        }

        // The body statement gets checked in the set_body function

        auto exp9 = nd->set_bodystmt(bodystmt);
        if (!exp9)
            return err_to_parse_error(exp9.error());

        endpos = pos;
        return nd;
    }




    // The new for..of syntax
    else
    {
        // Check the iterator
        if (ast_node::ast_node_type_is_assign_expr(
                second_expr->get_node_type()))
        {
            return std::unexpected(
                parse_error(second_expr->get_effectloc(),
                            err::EC_FOROF_ITERATOR_DISALLOWS_ASSIGNMENT));
        }

        // Generator expression
        auto exp10 = parse_expression(tokenlist,pos,pos);
        if (!exp10)
            return exp10;
        ast_node_ptr for_generator = std::move(exp10.value());

        // Colon
        token const &coltok = tokenlist[pos];
        if (coltok.ttype != tokentype::COLON)
            return std::unexpected(parse_error(coltok.sourceloc,
                                               err::EC_EXPECTING_FOR_COLON));
        pos = pos + 1;

        // Check the generator
        if (for_generator->declares_variables_visible_from_outside())
        {
            return std::unexpected(
                parse_error(for_generator->get_startloc(),
                            err::EC_FOROF_GENERATOR_DISALLOWS_VARDECL));
        }
        if (ast_node::ast_node_type_is_assign_expr(
                for_generator->get_node_type()))
        {
            return std::unexpected(
                parse_error(for_generator->get_effectloc(),
                            err::EC_FOROF_GENERATOR_DISALLOWS_ASSIGNMENT));
        }

        // Body
        auto exp11 = parse_statement(tokenlist,pos,pos);
        if (!exp11)
            return exp11;
        ast_node_ptr bodystmt = std::move(exp11.value());
        // Body is checked when inserted

        // Create the statement
        ast_node_ptr nd = ast_node::make_ast_node(AST_FOROFSTMT,
                                                  tok.startloc(),
                                                  tok.startloc(),
                                                  bodystmt->get_endloc());
        cig_assert(nd != nullptr);

        // Insert components
        if (init_stmt != nullptr)
        {
            auto exp12 = nd->set_initstmt(init_stmt);
            if (!exp12)
                return err_to_parse_error(exp12.error());
        }
        auto exp13 = nd->set_iteratorexpr(second_expr);
        if (!exp13)
            return err_to_parse_error(exp13.error());
        auto exp14 = nd->set_generatorexpr(for_generator);
        if (!exp14)
            return err_to_parse_error(exp14.error());
        auto exp15 = nd->set_bodystmt(bodystmt);
        if (!exp15)
            return err_to_parse_error(exp15.error());

        endpos = pos;
        return nd;
    }
}


static
ast_node_or_parse_error
parse_compound_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &bracetok = tokenlist[pos];

    if (bracetok.ttype != tokentype::OPENBRACE)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));

    pos = pos + 1;

    ast_node_ptr nd = ast_node::make_ast_node(AST_COMPOUNDSTMT,
                                              bracetok.startloc(),
                                              bracetok.startloc(),
                                              bracetok.endloc()); // for now
    cig_assert(nd != nullptr);

    for (;;)
    {
        token const &endtok = tokenlist[pos];
        if (endtok.ttype == tokentype::FILEEND ||
            endtok.ttype == tokentype::CLOSEBRACE)
            break;

        auto exp1 = parse_statement(tokenlist,pos,pos);
        if (!exp1)
            return exp1;
        ast_node_ptr stmt = std::move(exp1.value());
        auto exp2 = nd->add_substmt(stmt);
        if (!exp2)
            return err_to_parse_error(exp2.error());
    }

    token const &cbracetok = tokenlist[pos];
    if (cbracetok.ttype != tokentype::CLOSEBRACE)
    {
        cig_assert(cbracetok.ttype == tokentype::FILEEND); // follows from above
        // Report the closing brace
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_COMPOUND_STATEMENT_NOT_CLOSED));
    }

    nd->set_endloc(cbracetok.endloc());

    // Skip the closing brace
    pos = pos + 1;

    endpos = pos;
    return nd;
}


static
ast_node_or_parse_error
parse_statement(
    token_list const &tokenlist,
    i64 startpos,
    i64 &endpos)
{
    i64 pos = startpos;
    using namespace ast;

    token const &tok = tokenlist[pos];

    if (tok.ttype == tokentype::RETURN)
        return parse_return_statement(tokenlist,pos,endpos);
    else if (tok.ttype == tokentype::OPENBRACE)
        return parse_compound_statement(tokenlist,pos,endpos);
    else if (tok.ttype == tokentype::KWD_IF)
        return parse_if_statement(tokenlist,pos,endpos);
    else if (tok.ttype == tokentype::KWD_WHILE)
        return parse_while_statement(tokenlist,pos,endpos);
    else if (tok.ttype == tokentype::KWD_DO)
        return parse_do_while_statement(tokenlist,pos,endpos);
    else if (tok.ttype == tokentype::KWD_FOR)
        return parse_for_statement(tokenlist,pos,endpos);

    else
        return parse_expression_statement(tokenlist,pos,endpos);
}


//
// Parse outside a function body
//


// Parse a function body including the opening and closing brace
static
ast_node_or_parse_error
parse_function_body(token_list const &tokenlist,
                    i64 startpos,
                    i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &tok = tokenlist[pos];

    if (tok.ttype != tokentype::OPENBRACE)
        return std::unexpected(parse_error(err::EC_ALGORITHMIC));

    auto exp1 = parse_compound_statement(tokenlist,pos,pos);
    if (!exp1)
        return exp1;
    ast_node_ptr body = std::move(exp1.value());

    ast_node_ptr funbody = ast_node::make_ast_node(AST_FUNBODY,
                                                   body->get_startloc(),
                                                   body->get_effectloc(),
                                                   body->get_endloc());
    cig_assert(funbody != nullptr);

    auto exp2 = funbody->add_substmt(body);
    if (!exp2)
        return err_to_parse_error(exp2.error());

    // Successful return
    endpos = pos;
    return funbody;
}


static
ast_node_or_parse_error
parse_namespace(ast::ast_node_ptr outer,
                token_list const &tokenlist,
                i64 startpos,
                i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    // A namespace must be in another namespace
    cig_assert(outer->get_node_type() == AST_GLOBAL ||
               outer->get_node_type() == AST_NAMESPACE);

    i64 pos = startpos;
    token const &tok = tokenlist[pos];
    cig_assert(tok.ttype == tokentype::NAMESPACE);

    // Read the name
    token const &nametok = tokenlist[pos+1];
    if (nametok.ttype != tokentype::IDENTIFIER)
        return std::unexpected(
            parse_error(nametok.sourceloc,
                        err::EC_EXPECTING_NAMESPACE_NAME));
    str const &name = std::get<str>(nametok.value);

    // Read the colon
    token const &coltok = tokenlist[pos+2];
    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(
            parse_error(coltok.sourceloc,
                        err::EC_EXPECTING_NAMESPACE_COLON));

    // Read the opening brace
    token const &bracetok = tokenlist[pos+3];
    if (bracetok.ttype != tokentype::OPENBRACE)
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_EXPECTING_NAMESPACE_OPENBRACE));

    // Lookup the namespace
    i64 index = outer->lookup_name(name);
    ast_node_ptr sub;
    if (index == -1)
    {
        // Create the namespace
        sub = ast_node::make_ast_node(AST_NAMESPACE,
                                      tok.startloc(),
                                      nametok.startloc(),
                                      bracetok.endloc()); // for now
        cig_assert(sub != nullptr);
        auto exp1 = sub->set_name(name);
        if (!exp1)
            return err_to_parse_error(exp1.error());
        auto expindex = outer->add_named_entity(sub);
        if (!expindex)
            return std::unexpected(
                parse_error(bracetok.sourceloc,expindex.error()));
        index = expindex.value();
        cig_assert(index != -1);
    }
    else
    {
        // Verify that the ast-subnode is a namespace,
        // to which we will add
        sub = outer->get_subnode(index);
        cig_assert(sub != nullptr); // Named subnode must exist
        if (sub->get_node_type() != AST_NAMESPACE)
        {
            return std::unexpected(
                parse_error(nametok.sourceloc,
                            outer->lookup_name_location(name),
                            err::EC_NAMESPACE_NAME_ALREADY_USED));
        }
    }

    // Parse the namespace
    auto pe = parse_in_namespace(sub,tokenlist,pos + 4,endpos);
    if (pe.is_error())
        return std::unexpected(pe);
    // Upon successful return, the next token ist closebrace or fileend

    // Read the closing brace
    token const &cbracetok = tokenlist[endpos];
    if (cbracetok.ttype != tokentype::CLOSEBRACE)
    {
        // guaranteed by parse_in_namespace: closebrace or fileend
        cig_assert(cbracetok.ttype == tokentype::FILEEND);
        // Report the closing brace
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_NAMESPACE_NOT_CLOSED));
    }
    sub->set_endloc(cbracetok.endloc());

    // Continue after the closing brace
    pos = endpos + 1;

    endpos = pos;
    return sub;
}


static
ast_node_or_parse_error
parse_class(token_list const &tokenlist,
            i64 startpos,
            i64 &endpos,
            cig_bool is_static,
            cig_bool is_nonstatic)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;
    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::CLASS)
        return std::unexpected(parse_error(tok.sourceloc,
                                           err::EC_EXPECTING_KWD_CLASS));

    // Read the name
    token const &nametok = tokenlist[pos+1];
    if (nametok.ttype != tokentype::IDENTIFIER)
        return std::unexpected(
            parse_error(nametok.sourceloc,
                        err::EC_EXPECTING_CLASS_NAME));
    str const &name = std::get<str>(nametok.value);

    // Check consistency of static specifications
    if (is_static && is_nonstatic)
        return std::unexpected(
            parse_error(nametok.sourceloc,
                        err::EC_CONTRADICTING_STATIC_SPECIFICATION));

    // Read a semicolon or colon
    token const &coltok = tokenlist[pos+2];
    pos = pos + 3;
    if (coltok.ttype == tokentype::SEMICOLON)
    {
        // This was just a forward class declaration

        ast_node_ptr sub = ast_node::make_ast_node(AST_CLASSDECL,
                                                   tok.startloc(),
                                                   nametok.startloc(),
                                                   coltok.endloc());
        cig_assert(sub != nullptr);
        auto exp1 = sub->set_name(name);
        if (!exp1)
            return err_to_parse_error(exp1.error());

        auto exp2 = sub->set_flags((is_static ?
                                    ast_node::AST_FLAG_STATIC :
                                    ast_node::AST_FLAG_NONE) |
                                   (is_nonstatic ?
                                    ast_node::AST_FLAG_NONSTATIC :
                                    ast_node::AST_FLAG_NONE));
        if (!exp2)
            return err_to_parse_error(exp2.error());

        endpos = pos;
        return sub;
    }


    // Read the colon
    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(
            parse_error(coltok.sourceloc,
                        err::EC_EXPECTING_CLASS_COLON));

    // Read qualifier keywords
    token kwdtok = tokenlist[pos];
    bool is_native = false;

    while (kwdtok.ttype > tokentype::INVALID &&
           kwdtok.ttype < tokentype::KEYWORD_MAX)
    {
        if (0)
            ;
        else if (kwdtok.ttype == tokentype::NATIVE)
        {
            if (is_native)
            {
                return std::unexpected(
                    parse_error(kwdtok.sourceloc,
                                err::EC_DUPLICATE_NATIVE));
            }
            is_native = true;
        }
        else
        {
            return std::unexpected(
                parse_error(kwdtok.sourceloc,
                            err::EC_EXPECTING_FUNCTION_QUALIFIER));
        }

        // Skip the qualifier keyword
        pos = pos + 1;
        kwdtok = tokenlist[pos];
    }


    // Read a semicolon or opening brace
    token const &bracetok = tokenlist[pos];
    if (bracetok.ttype == tokentype::SEMICOLON)
    {
        // This was probably a native class.
        // Create a declaration.

        ast_node_ptr sub = ast_node::make_ast_node(AST_CLASSDECL,
                                                   tok.startloc(),
                                                   nametok.startloc(),
                                                   bracetok.endloc());
        cig_assert(sub != nullptr);
        auto exp1 = sub->set_name(name);
        if (!exp1)
            return err_to_parse_error(exp1.error());
        auto exp2 = sub->set_flags((is_native ?
                                    ast_node::AST_FLAG_NATIVE :
                                    ast_node::AST_FLAG_NONE) |
                                   (is_static ?
                                    ast_node::AST_FLAG_STATIC :
                                    ast_node::AST_FLAG_NONE) |
                                   (is_nonstatic ?
                                    ast_node::AST_FLAG_NONSTATIC :
                                    ast_node::AST_FLAG_NONE));
        if (!exp2)
            return err_to_parse_error(exp2.error());

        endpos = pos + 1;
        return sub;
    }

    else if (bracetok.ttype != tokentype::OPENBRACE)
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_EXPECTING_CLASS_OPENBRACE));

    // Create the class definition

    // A class can be native and still contain a definition in CIG.
    // That is the case for classes with member functions accessible
    // from CIG.
    // Both for native and non-native classes, we can continue
    // with the definition here.
    ast_node_ptr sub = ast_node::make_ast_node(AST_CLASSDEF,
                                               tok.startloc(),
                                               nametok.startloc(),
                                               coltok.endloc()); // repl later
    cig_assert(sub != nullptr);
    auto exp1 = sub->set_name(name);
    if (!exp1)
        return err_to_parse_error(exp1.error());
    auto exp2 = sub->set_flags((is_native ?
                                ast_node::AST_FLAG_NATIVE :
                                ast_node::AST_FLAG_NONE) |
                               (is_static ?
                                ast_node::AST_FLAG_STATIC :
                                ast_node::AST_FLAG_NONE) |
                               (is_nonstatic ?
                                ast_node::AST_FLAG_NONSTATIC :
                                ast_node::AST_FLAG_NONE));
    if (!exp2)
        return err_to_parse_error(exp2.error());

    // Parse the class
    auto pe = parse_in_class(sub,tokenlist,pos + 1,pos);
    if (pe.is_error())
        return std::unexpected(pe);
    // Upon successful return, the next token ist closebrace or fileend

    // Read the closing brace
    token const &cbracetok = tokenlist[pos];
    if (cbracetok.ttype != tokentype::CLOSEBRACE)
    {
        // guaranteed by parse_in_class: closebrace or fileend
        cig_assert(cbracetok.ttype == tokentype::FILEEND);
        // Report the closing brace
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_CLASS_NOT_CLOSED));
    }

    // Set the end location
    sub->set_endloc(cbracetok.endloc());

    // Continue after the closing brace
    pos = pos + 1;

    endpos = pos;
    return sub;
}            


static
ast_node_or_parse_error
parse_enum(token_list const &tokenlist,
           i64 startpos,
           i64 &endpos)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;
    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::ENUM)
        return std::unexpected(parse_error(tok.sourceloc,
                                           err::EC_EXPECTING_KWD_ENUM));

    // Read the name
    token const &nametok = tokenlist[pos+1];
    if (nametok.ttype != tokentype::IDENTIFIER)
        return std::unexpected(
            parse_error(nametok.sourceloc,
                        err::EC_EXPECTING_ENUM_NAME));
    str const &name = std::get<str>(nametok.value);

    // Read a semicolon or colon
    token const &coltok = tokenlist[pos+2];
    pos = pos + 3;
    if (coltok.ttype == tokentype::SEMICOLON)
    {
        // This was just a forward enum declaration

        ast_node_ptr sub = ast_node::make_ast_node(AST_ENUMDECL,
                                                   tok.startloc(),
                                                   nametok.startloc(),
                                                   coltok.endloc());
        cig_assert(sub != nullptr);
        auto exp1 = sub->set_name(name);
        if (!exp1)
            return err_to_parse_error(exp1.error());

        endpos = pos;
        return sub;
    }


    // Read the colon
    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(
            parse_error(coltok.sourceloc,
                        err::EC_EXPECTING_ENUM_COLON));

    // No qualifier keywords for enum

    // Read a semicolon or opening brace
    token const &bracetok = tokenlist[pos];
    if (bracetok.ttype == tokentype::SEMICOLON)
    {
        // Create a declaration

        ast_node_ptr sub = ast_node::make_ast_node(AST_ENUMDECL,
                                                   tok.startloc(),
                                                   nametok.startloc(),
                                                   bracetok.endloc());
        cig_assert(sub != nullptr);
        auto exp1 = sub->set_name(name);
        if (!exp1)
            return err_to_parse_error(exp1.error());

        endpos = pos + 1;
        return sub;
    }
    else if (bracetok.ttype != tokentype::OPENBRACE)
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_EXPECTING_ENUM_OPENBRACE));

    // Create the enum definition
    ast_node_ptr sub = ast_node::make_ast_node(AST_ENUMDEF,
                                               tok.startloc(),
                                               nametok.startloc(),
                                               coltok.endloc()); // repl later
    cig_assert(sub != nullptr);
    auto exp1 = sub->set_name(name);
    if (!exp1)
        return err_to_parse_error(exp1.error());

    // Parse the enum
    auto pe = parse_in_enum(sub,tokenlist,pos + 1,pos);
    if (pe.is_error())
        return std::unexpected(pe);
    // Upon successful return, the next token ist closebrace or fileend

    // Read the closing brace
    token const &cbracetok = tokenlist[pos];
    if (cbracetok.ttype != tokentype::CLOSEBRACE)
    {
        // guaranteed by parse_in_enum: closebrace or fileend
        cig_assert(cbracetok.ttype == tokentype::FILEEND);
        // Report the closing brace
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_ENUM_NOT_CLOSED));
    }

    // Set the end location
    sub->set_endloc(cbracetok.endloc());

    // Continue after the closing brace
    pos = pos + 1;

    endpos = pos;
    return sub;
}            


static
ast_node_or_parse_error
parse_function(token_list const &tokenlist,
               i64 startpos,
               i64 &endpos,
               cig_bool is_static,
               cig_bool is_nonstatic)
{
    using namespace ast;
    using enum ast::ast_node_type;

    i64 pos = startpos;

    token const &tok = tokenlist[pos];
    if (tok.ttype != tokentype::FUNCTION)
        return std::unexpected(parse_error(tok.sourceloc,
                                           err::EC_EXPECTING_KWD_FUNCTION));
    pos = pos + 1;

    // Read the name. It must start with an identifier.
    token const &nametok = tokenlist[pos];
    if (nametok.ttype != tokentype::IDENTIFIER)
        return std::unexpected(parse_error(nametok.sourceloc,
                                           err::EC_EXPECTING_FUNCTION_NAME));
    auto expquname = parse_qualified_name(tokenlist,pos,pos);
    if (!expquname)
        return expquname;
    ast_node_ptr namend = std::move(expquname.value());
    cig::source_location nameloc = namend->get_effectloc();

    // Check consistency of static specifications
    if (is_static && is_nonstatic)
        return std::unexpected(
            parse_error(nametok.sourceloc,
                        err::EC_CONTRADICTING_STATIC_SPECIFICATION));

    // Extract the last name component
    str funcname;
    if (namend->get_node_type() == AST_EXPR_UNQUALNAME)
    {
        // If the name has only one component, discard the namend
        funcname = namend->get_unqualified_name();
        namend = nullptr;
    }
    else if (namend->get_node_type() == AST_EXPR_MEMBER)
    {
        funcname = namend->get_member_name();
    }

    // Read the function parameter list
    auto expparlist = parse_function_parlist(tokenlist,pos,pos);
    if (!expparlist)
        return expparlist;
    auto parlist = std::move(expparlist.value());

    // Read the arrow
    token const &arrowtok = tokenlist[pos];
    if (arrowtok.ttype != tokentype::ARROW)
        return std::unexpected(parse_error(arrowtok.sourceloc,
                                           err::EC_EXPECTING_ARROW));
    pos = pos + 1;

    // Read the function return type list
    // token const &retlisttok = tokenlist[pos];
    auto expretlist = parse_function_retlist(tokenlist,pos,pos);
    if (!expretlist)
        return expretlist;
    auto retlist = std::move(expretlist.value());

    // If there is a semicolon, we are done
    token const &coltok = tokenlist[pos];
    pos = pos + 1;
    if (coltok.ttype == tokentype::SEMICOLON)
    {
        if (namend != nullptr)
            return std::unexpected(
                parse_error(nametok.sourceloc,
                            err::EC_FUNCTION_DECLARATION_NEEDS_SIMPLE_NAME));

        ast_node_ptr fundecl = ast_node::make_ast_node(AST_FUNDECL,
                                                       tok.startloc(),
                                                       nameloc,
                                                       coltok.endloc());
        cig_assert(fundecl);

        auto exp1 = fundecl->set_funparlist(parlist);
        if (!exp1)
            return err_to_parse_error(exp1.error());
        auto exp2 = fundecl->set_funretlist(retlist);
        if (!exp2)
            return err_to_parse_error(exp2.error());
        auto exp3 = fundecl->set_name(funcname);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = fundecl->set_flags((is_static ?
                                        ast_node::AST_FLAG_STATIC :
                                        ast_node::AST_FLAG_NONE) |
                                       (is_nonstatic ?
                                        ast_node::AST_FLAG_NONSTATIC :
                                        ast_node::AST_FLAG_NONE));
        if (!exp4)
            return err_to_parse_error(exp4.error());

        endpos = pos;
        return fundecl;
    }

    // Read the colon
    if (coltok.ttype != tokentype::COLON)
        return std::unexpected(parse_error(coltok.sourceloc,
                                           err::EC_EXPECTING_FUNCTION_COLON));

    // Read qualifier keywords
    token kwdtok = tokenlist[pos];
    bool is_native = false;

    while (kwdtok.ttype > tokentype::INVALID &&
           kwdtok.ttype < tokentype::KEYWORD_MAX)
    {
        if (0)
            ;
        else if (kwdtok.ttype == tokentype::NATIVE)
        {
            if (is_native)
            {
                return std::unexpected(parse_error(kwdtok.sourceloc,
                                                   err::EC_DUPLICATE_NATIVE));
            }
            is_native = true;
        }
        else
        {
            return std::unexpected(
                parse_error(kwdtok.sourceloc,
                            err::EC_EXPECTING_FUNCTION_QUALIFIER));
        }

        // Skip the qualifier keyword
        pos = pos + 1;
        kwdtok = tokenlist[pos];
    }
    ast_node::ast_node_flags flags = ast_node::AST_FLAG_NONE;
    if (is_native)
        flags = ast_node::ast_node_flags(flags | ast_node::AST_FLAG_NATIVE);
    if (is_static)
        flags = ast_node::ast_node_flags(flags | ast_node::AST_FLAG_STATIC); 
    if (is_nonstatic)
        flags = ast_node::ast_node_flags(flags | ast_node::AST_FLAG_NONSTATIC); 

    // Read the semicolon or opening brace
    token const &bracetok = tokenlist[pos];
    if (bracetok.ttype == tokentype::SEMICOLON)
    {
        if (!is_native)
        {
            return std::unexpected(
                parse_error(bracetok.sourceloc,
                            err::EC_EXPECTING_FUNCTION_OPENBRACE));
        }

        ast_node_ptr fundecl = ast_node::make_ast_node(AST_FUNDECL,
                                                       tok.startloc(),
                                                       nameloc,
                                                       coltok.endloc());
        cig_assert(fundecl);

        auto exp1 = fundecl->set_funparlist(parlist);
        if (!exp1)
            return err_to_parse_error(exp1.error());
        auto exp2 = fundecl->set_funretlist(retlist);
        if (!exp2)
            return err_to_parse_error(exp2.error());
        auto exp3 = fundecl->set_name(funcname);
        if (!exp3)
            return err_to_parse_error(exp3.error());
        auto exp4 = fundecl->set_flags(flags);
        if (!exp4)
            return err_to_parse_error(exp4.error());

        endpos = pos + 1;
        return fundecl;
    }
    else if (bracetok.ttype == tokentype::OPENBRACE)
    {
        if (is_native)
        {
            // A native function must not have a body in CIG.
            return std::unexpected(
                parse_error(bracetok.sourceloc,
                            err::EC_NATIVE_FUNCTION_HAS_NO_BODY));
        }
        // A non-native function will be defined by its body.
    }
    else
        return std::unexpected(
            parse_error(bracetok.sourceloc,
                        err::EC_EXPECTING_FUNCTION_OPENBRACE));


    // Parse the function body
    auto pe = parse_function_body(tokenlist,pos,pos);
    if (!pe)
        return pe;
    ast_node_ptr body = std::move(pe.value());
    // This has parsed everything up to the closing brace

    ast_node_ptr fundef = ast_node::make_ast_node(AST_FUNDEF,
                                                  tok.startloc(),
                                                  nameloc,
                                                  retlist->get_endloc());
    cig_assert(fundef);

    auto exp1 = fundef->set_funparlist(parlist);
    if (!exp1)
        return err_to_parse_error(exp1.error());
    auto exp2 = fundef->set_funretlist(retlist);
    if (!exp2)
        return err_to_parse_error(exp2.error());
    auto exp3 = fundef->set_name(funcname);
    if (!exp3)
        return err_to_parse_error(exp3.error());
    auto exp4 = fundef->set_flags(flags);
    if (!exp4)
        return err_to_parse_error(exp4.error());
    auto exp5 = fundef->set_funbody(body);
    if (!exp5)
        return err_to_parse_error(exp5.error());
    if (namend)
    {
        auto exp6 = fundef->set_fundef_qualified_name(namend);
        if (!exp6)
            return err_to_parse_error(exp6.error());
    }

    endpos = pos;
    return fundef;
}


parse_error
parse_ast_file(ast::ast_node_ptr global, // namespace global
               token_list const &tokenlist)
{
    i64 pos = 0;
    i64 endpos;
    i64 sz = tokenlist.i64_size();

    // All subfunctions expect the tokenlist to end with a FILEEND token
    if (sz <= 0)
        return parse_error(err::EC_EMPTY_TOKENLIST);
    token const &tok = tokenlist[sz-1];
    if (tok.ttype != tokentype::FILEEND)
        return parse_error(tok.sourceloc,
                           err::EC_EMPTY_TOKENLIST);

    return parse_in_namespace(global,tokenlist,pos,endpos);
}


} // namespace parser

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
