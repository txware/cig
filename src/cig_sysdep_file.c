/* cig_sysdep_file.c
 *
 * Copyright 2023 Claus Fischer
 *
 * System dependent low-level file operations.
 */
#include <stdint.h>             /* int64_t,uint64_t */

#include "cig_sysdep_file.h"

/* Auxiliary includes */
#include <errno.h>
#include "cig_sysdep_errno.h"
#include "cig_errno.h"

#ifdef WIN32
#  include <windows.h>          /* FILETIME */
#else
#  
#endif
#  include <fcntl.h>            /* open,fstat */
#  include <unistd.h>           /* close,fsync,fdatasync */
#  include <sys/stat.h>         /* fstat */
// #ifdef HAVE_SYS_TYPES_H
// #  include <sys/types.h>
// #endif
// 
// #include <stddef.h>          /* size_t */
// #include <errno.h>           /* errno */
// #include <stdio.h>           /* snprintf/_snprintf, fprintf */
// #include <string.h>          /* strerror */



#if CIG_WITH_RECORDER
// #  define CIG_RECORDER_REDEFINE_FUNCTIONS
// #  include "cig_recorder.h"
#endif


#define SUPPRESS_UNUSED_MESSAGE(x) ((void) x)
#define TOSTR(x) #x
#define TOLINENUM(x) TOSTR(x)
#define E(s) return (__FILE__  ":" TOLINENUM(__LINE__) ": error: " s)



/* cig_sys_file_selftest()
 *
 * Check some elementary requirements for this code.
 */
char const *                    /* static error string or 0 on success */
cig_sys_file_selftest(void)
{
#ifdef WIN32
    /* Ensure that on Windows the file handle is the same size as an int. */
    if (sizeof(HANDLE) != sizeof(int) ||
        (int)INVALID_HANDLE_VALUE != -1)
        E("WIN32: selftest of HANDLE type failed");
#endif
    return 0;
}


/* cig_sys_file_open()
 *
 * Open a file and set the position to the beginning.
 * The file is not truncated. If it does not exist, it is
 * created unless the read_only flag is given.
 */
int                             /* 0 on success, cig_errno on failure */
cig_sys_file_open(
    char const *filename,       /* [i] filename */
    int read_only,              /* [i] open for read only */
    int *filedes                /* [o] file descriptor, or CIG_FD_INVALID */
    )
{
#ifdef WIN32
    HANDLE handle;
    DWORD access;
    DWORD sharemode;
    DWORD disposition;

    if (read_only)
        access = GENERIC_READ;
    else
        access = GENERIC_READ | GENERIC_WRITE;

    sharemode = FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE;

    if (read_only)
        disposition = OPEN_EXISTING;
    else
        disposition = OPEN_ALWAYS;

    /* Note: For flags and attributes, one might use WRITE_THROUGH. */
    SetLastError(0);
    handle =
        CreateFile(
            filename,           /* name of file to open */
            access,             /* access mode */
            sharemode,          /* sharemode */
            NULL,               /* securityattributes */
            disposition,        /* whether to create/truncate/expect file */
            FILE_ATTRIBUTE_NORMAL,/* flags_and_attributes */
            NULL);              /* no template file */

    *filedes = handle;
    if (handle == INVALID_HANDLE_VALUE)
    {
        int e = GetLastError();
        if (e == ERROR_SHARING_VIOLATION)
            return cig_map_get_last_error(e); /* Fixme or map here */
#if 0
        else if (e == ERROR_ALREADY_EXISTS) /* 183 */
            /* Set upon success when a file is overwritten */
            return cig_map_get_last_error(e);
#endif
        else if (e == ERROR_FILE_EXISTS) /* 80 */
            /* Set if file exists and overwrite not allowed  */
            return CIG_ERRNO_EEXIST;
        else if (e == ERROR_FILE_NOT_FOUND) /* 2 */
            /* File is requested but does not exist */
            return CIG_ERRNO_ENOENT;
        else if (e == ERROR_ACCESS_DENIED)
            /* File is being deleted, no new access */
            /* File is hidden and user has no access */
            return CIG_ERRNO_EACCES;
        else if (e == ERROR_SHARING_VIOLATION)
            /* Other process has file open, does not share */
            return CIG_ERRNO_EDEADLK;

        return cig_map_get_last_error(e);
    }
    return CIG_ERRNO_ESUCCESS;
#else
    int fd;
    int flags;
    mode_t mode;

    if (read_only)
        flags = O_RDONLY;
    else
        flags = O_RDWR | O_CREAT;
    /* This mode will be modified by the umask appropriately */
    mode = (S_IRUSR | S_IWUSR |
            S_IRGRP | S_IWGRP |
            S_IROTH | S_IWOTH);

    fd = open(filename,flags,mode);
    *filedes = fd;
    if (fd == CIG_FD_INVALID)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_close(
    int filedes                 /* [i] file descriptor */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    SetLastError(0);
    if (!CloseHandle((HANDLE)filedes))
    {
        int e = GetLastError();
        if (e == ERROR_INVALID_HANDLE)
            return CIG_ERRNO_EBADF;

        return cig_map_get_last_error(e);
    }
    return CIG_ERRNO_ESUCCESS;
#else
    if (close(filedes) != 0)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_syncdata(
    int filedes                 /* [i] file descriptor */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    SetLastError(0);
    /* _commit() calls FlushFileBuffers() */
    if (!FlushFileBuffers(filedes))
    {
        int e = GetLastError();
        if (e == ERROR_INVALID_HANDLE)
            return CIG_ERRNO_EBADF;

        return cig_map_get_last_error(e);
    }
    return CIG_ERRNO_ESUCCESS;
#elif defined(__linux__)
    if (fdatasync(filedes) == -1)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#elif defined(__APPLE__)        /* Apple has no fdatasync */
    if (fsync(filedes) == -1)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#else
#  error fsync not available
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_syncdata_name(
    char const *filename        /* [i] filename */
    )
{
#ifdef WIN32
    HANDLE handle;
    DWORD access = GENERIC_READ;
    DWORD sharemode = FILE_SHARE_READ;
    DWORD disposition = OPEN_EXISTING;
    DWORD flags_and_attributes = FILE_FLAG_BACKUP_SEMANTICS;

    /* The FILE_FLAG_BACKUP_SEMANTICS is required to open a directory. */

    /* Open the file or directory */
    SetLastError(0);
    handle =
        CreateFile(
            filename,           /* name of file to open */
            access,             /* access mode */
            sharemode,          /* sharemode */
            NULL,               /* securityattributes */
            disposition,        /* whether to create/truncate/expect file */
            flags_and_attributes,/* flags and attributes */
            NULL);              /* no template file */
    if (handle == INVALID_HANDLE_VALUE)
    {
        int e = GetLastError();
        if (e == ERROR_SHARING_VIOLATION)
            return cig_map_get_last_error(e); /* Fixme or map here */
#if 0
        else if (e == ERROR_ALREADY_EXISTS) /* 183 */
            /* Set upon success when a file is overwritten */
            return cig_map_get_last_error(e);
#endif
        else if (e == ERROR_FILE_EXISTS) /* 80 */
            /* Set if file exists and overwrite not allowed  */
            return CIG_ERRNO_EEXIST;
        else if (e == ERROR_FILE_NOT_FOUND)   /* 2 */
            /* File is requested but does not exist */
            return CIG_ERRNO_ENOENT;
        else if (e == ERROR_ACCESS_DENIED)
            /* File is being deleted, no new access */
            /* File is hidden and user has no access */
            return CIG_ERRNO_EACCES;
        else if (e == ERROR_SHARING_VIOLATION)
            /* Other process has file open, does not share */
            return CIG_ERRNO_EDEADLK;

        return cig_map_get_last_error(e);
    }
    if (!FlushFileBuffers(handle))
    {
        int e = GetLastError();

        CloseHandle(handle);

        if (e == ERROR_INVALID_HANDLE)
            return CIG_ERRNO_EBADF;
        return cig_map_get_last_error(e);
    }
    if (!CloseHandle(handle))
    {
        int e = GetLastError();
        if (e == ERROR_INVALID_HANDLE)
            return CIG_ERRNO_EBADF;

        return cig_map_get_last_error(e);
    }

    return CIG_ERRNO_ESUCCESS;
#else
    int fd;
    int flags;

    flags = O_RDONLY;

    fd = open(filename,flags);
    if (fd == CIG_FD_INVALID)
    {
        int e = errno;
        return cig_map_errno(e);
    }

#if defined(__linux__)
    if (fdatasync(fd) == -1)
    {
        int e = errno;
        close(fd);
        return cig_map_errno(e);
    }
#elif defined(__APPLE__)
    if (fsync(fd) == -1)
    {
        int e = errno;
        close(fd);
        return cig_map_errno(e);
    }
#else
#  error fsync not available
#endif

    if (close(fd) != 0)
    {
        int e = errno;
        return cig_map_errno(e);
    }

    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_getlen(
    int filedes,                /* [i] file descriptor */
    int64_t *len                /* [o] length, or -1 */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    LARGE_INTEGER newlen;

    SetLastError(0);
    if (!GetFileSize((HANDLE)filedes,&newlen))
    {
        int e = GetLastError();
        *len = -1;
        return cig_map_get_last_error(e);
    }
    *len = newlen;
    return CIG_ERRNO_ESUCCESS;
#else
    struct stat st;

    if (fstat(filedes,&st) != 0)
    {
        int e = errno;
        *len = -1;
        return cig_map_errno(e);
    }
    *len = (int64_t)st.st_size;
    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_getpos(
    int filedes,                /* [i] file descriptor */
    int64_t *pos                /* [o] position, or -1 */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    LARGE_INTEGER newpos;

    SetLastError(0);
    if (!SetFilePointerEx((HANDLE)filedes,0,&newpos,FILE_CURRENT))
    {
        int e = GetLastError();
        *pos = -1;
        if (e == ERROR_NEGATIVE_SEEK)
            return CIG_ERRNO_EINVAL;
        else if (e == ERROR_INVALID_PARAMTER)
            return CIG_ERRNO_EINVAL;
        return cig_map_get_last_error(e);
    }
    *pos = newpos;
    return CIG_ERRNO_ESUCCESS;
#else
    off_t off;

    off = lseek(filedes,0,SEEK_CUR);
    *pos = off;
    if (off == (off_t)-1)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_setpos(
    int filedes,                /* [i] file descriptor */
    int64_t pos                 /* [i] position */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    SetLastError(0);
    if (!SetFilePointerEx((HANDLE)filedes,pos,0,FILE_BEGIN))
    {
        int e = GetLastError();
        if (e == ERROR_NEGATIVE_SEEK)
            return CIG_ERRNO_EINVAL;
        else if (e == ERROR_INVALID_PARAMTER)
            return CIG_ERRNO_EINVAL;
        return cig_map_get_last_error(e);
    }
    return CIG_ERRNO_ESUCCESS;
#else
    off_t off;
    off = lseek(filedes,pos,SEEK_SET);
    if (off == (off_t)-1)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_truncate(
    int filedes                 /* [i] file descriptor */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    SetLastError(0);
    if (!SetFilePointerEx((HANDLE)filedes,0,0,FILE_BEGIN))
    {
        int e = GetLastError();
        if (e == ERROR_NEGATIVE_SEEK)
            return CIG_ERRNO_EINVAL;
        else if (e == ERROR_INVALID_PARAMTER)
            return CIG_ERRNO_EINVAL;
        return cig_map_get_last_error(e);
    }
    if (!SetEndOfFile((HANDLE)filedes))
    {
        int e = GetLastError();
        return cig_map_get_last_error(e);
    }
    return CIG_ERRNO_ESUCCESS;
#else
    off_t off;

    off = lseek(filedes,0,SEEK_CUR);
    if (off == (off_t)-1)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    if (ftruncate(filedes,off) != 0)
    {
        int e = errno;
        return cig_map_errno(e);
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


int                             /* 0 on success, cig_errno on failure */
cig_sys_file_write(
    int filedes,                /* [i] file descriptor */
    void const *buf,            /* [i] buffer with data to write */
    uint64_t buflen             /* [i] length of data in bytes */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    uint64_t pos;
    DWORD bwritten;

    for (pos = 0; pos < buflen; pos += bwritten)
    {
        uint64_t len = buflen - pos;
        if (len > 0x7fffffff)   /* Truncate to 32 bit */
            len = 0x7fffffff;
        /* WriteFileEx would write asynchronously */
        SetLastError(0);
        if (!WriteFile((HANDLE)filedes,
                       (unsigned char const *)buf+pos,
                       len,&bwritten,NULL))
        {
            int e = GetLastError();
            if (e == ERROR_INVALID_USER_BUFFER)
                return CIG_ERRNO_EFAULT;
            else if (e == ERROR_NOT_ENOUGH_MEMORY)
                return CIG_ERRNO_ENOMEM;
            else if (e == ERROR_OPERATION_ABORTED)
                return CIG_ERRNO_ECANCELED;
            else if (e == ERROR_NOT_ENOUGH_QUOTA)
                return CIG_ERRNO_EDQUOT;
            else if (e == ERROR_IO_PENDING)
                return CIG_ERRNO_EINPROGRESS;
            else if (e == ERROR_BROKEN_PIPE)
                return CIG_ERRNO_EPIPE;

            /* fixme if interrupted, try again */

            return cig_map_get_last_error(e);
        }
    }
    return CIG_ERRNO_ESUCCESS;
#else
    size_t pos;
    ssize_t bwritten;
    
    for (pos = 0; pos < buflen; pos += bwritten)
    {
        bwritten = write(filedes,
                         (unsigned char const *)buf+pos,
                         buflen-pos);
        if (bwritten == -1)
        {
            int e = errno;

            if (e == EAGAIN ||
                e == EINTR)
                bwritten = 0;

            else
            {
                return cig_map_errno(e);
            }
        }
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


/* cig_sys_file_read()
 *
 * Note that this function does not handle EOF gracefully,
 * it will return CIG_ERRNO_NODATA in that instance.
 *
 * The function is intended to read from a file where the
 * length of the file and the length that can be read
 * is previously known.
 */
int                             /* 0 on success, cig_errno on failure */
cig_sys_file_read(
    int filedes,                /* [i] file descriptor */
    void *buf,                  /* [o] buffer with data to read */
    uint64_t buflen             /* [i] length of data to read in bytes */
    )
{
    if (filedes == CIG_FD_INVALID)
        return CIG_ERRNO_EBADF;

#ifdef WIN32
    uint64_t pos;
    DWORD bread;

    for (pos = 0; pos < buflen; pos += bread)
    {
        uint64_t len = buflen - pos;
        if (len > 0x7fffffff)   /* Truncate to 32 bit */
            len = 0x7fffffff;
        /* ReadFileEx would write asynchronously */
        SetLastError(0);
        if (!ReadFile((HANDLE)filedes,
                      (unsigned char *)buf+pos,
                      len,&bread,NULL))
        {
            int e = GetLastError();
            if (e == ERROR_INVALID_USER_BUFFER)
                return CIG_ERRNO_EFAULT;
            else if (e == ERROR_NOT_ENOUGH_MEMORY)
                return CIG_ERRNO_ENOMEM;
            else if (e == ERROR_OPERATION_ABORTED)
                return CIG_ERRNO_ECANCELED;
            else if (e == ERROR_NOT_ENOUGH_QUOTA)
                return CIG_ERRNO_EDQUOT;
            else if (e == ERROR_IO_PENDING)
                return CIG_ERRNO_EINPROGRESS;
            else if (e == ERROR_HANDLE_EOF)
                return CIG_ERRNO_ENODATA;

            /* fixme if interrupted, try again */

            return cig_map_get_last_error(e);
        }
    }
    return CIG_ERRNO_ESUCCESS;
#else
    size_t pos;
    ssize_t bread;
    
    for (pos = 0; pos < buflen; pos += bread)
    {
        bread = read(filedes,(unsigned char *)buf+pos,buflen-pos);
        if (bread == -1)
        {
            int e = errno;

            if (e == EAGAIN ||
                e == EINTR)
                bread = 0;

            else
            {
                return cig_map_errno(e);
            }
        }
        else if (bread == 0)
        {
            return CIG_ERRNO_ENODATA;
        }
    }
    return CIG_ERRNO_ESUCCESS;
#endif
}


/* ** EMACS **
 * Local Variables:
 * mode: C
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * indent-tabs-mode: nil
 * End:
 */
