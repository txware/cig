/* cig_parser.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Parser for the CIG language.
 */
#include "cig_parser.h"
#include "cig_parser_tokenizer.h"
#include "cig_parser_ast.h"
#include "cig_file.h"


namespace cig {

namespace parser {

parse_error
parse_file(ast::ast_node_ptr global, // global namespace
           str const &filename,
           char const *location_file_name,
           i64 tabsize)
{
    auto expcontent = cig::file::exp_read_file_into_str(filename);
    if (!expcontent)
    {
        cig::source_location sloc(0,0,location_file_name,0);
        cig::err e = std::move(expcontent.error());
        return parse_error(sloc,e.get_error_code());
    }
    str content = std::move(expcontent.value());

    auto exptokenlist =
        exp_tokenize_file_str(content,location_file_name,tabsize);
    if (!exptokenlist)
    {
        return std::move(exptokenlist.error());
    }
    auto tokenlist = std::move(exptokenlist.value());

    auto pe = parse_ast_file(global,tokenlist);
    if (pe.is_error())
    {
        return pe;
    }

    return no_parse_error;
}

} // namespace parser

} // namespace cig


char const *cig_parser_selftest()
{
    char const *selftest_error =
	cig::parser::parser_tokenizer_selftest_errorstring();
    if (selftest_error)
        return selftest_error;

    return 0;
}



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
