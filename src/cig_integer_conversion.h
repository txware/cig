/* cig_integer_conversion.h
 *
 * Copyright 2023 Claus Fischer
 *
 * String conversion (encoding/decoding) of integers.
 */
#ifndef CIG_INTEGER_CONVERSION_H_INCLUDED
#define CIG_INTEGER_CONVERSION_H_INCLUDED

#include "cig_err.h"            // cig::err

#include <cstddef>              // std::size_t,std::ptrdiff_t
#include <cstdint>              // uint8_t ...
#include <type_traits>          // make_unsigned

namespace cig {


namespace detail {


struct integer_conversion_error {
    enum errortype {
        NO_ERROR,
        END_OF_INPUT_BEFORE_NUMBER,
        NUMBER_MUST_START_WITH_A_DIGIT,
        NUMBER_IS_TOO_BIG,
        NOT_A_BINARY_DIGIT,
        NOT_AN_OCTAL_DIGIT,
        UNRECOGNIZED_NUMBER_SUFFIX
    };

    static constexpr const char * const errormessage[] = {
        "Conversion was successful",
        "End of input before number",
        "Number must start with a digit",
        "Number is too big",
        "Not a binary digit",
        "Not an octal digit",
        "Unrecognized suffix type"
    };

    errortype type { NO_ERROR };
};


static inline
err::errorcode
integer_conversion_map_error(integer_conversion_error::errortype etype)
{
    switch (etype)
    {
    case integer_conversion_error::NO_ERROR:
        return err::E_SUCCESS;
    case integer_conversion_error::END_OF_INPUT_BEFORE_NUMBER:
        return err::EC_END_OF_INPUT_BEFORE_NUMBER;
    case integer_conversion_error::NUMBER_MUST_START_WITH_A_DIGIT:
        return err::EC_NUMBER_MUST_START_WITH_A_DIGIT;
    case integer_conversion_error::NUMBER_IS_TOO_BIG:
        return err::EC_NUMBER_IS_TOO_BIG;
    case integer_conversion_error::NOT_A_BINARY_DIGIT:
        return err::EC_NOT_A_BINARY_DIGIT;
    case integer_conversion_error::NOT_AN_OCTAL_DIGIT:
        return err::EC_NOT_AN_OCTAL_DIGIT;
    case integer_conversion_error::UNRECOGNIZED_NUMBER_SUFFIX:
        return err::EC_UNRECOGNIZED_NUMBER_SUFFIX;
    }

    // Default error code
    return err::E_EILSEQ;
}



template<typename Char>
CIG_CONSTEXPR inline
std::ptrdiff_t                  // number of characters parsed, or -1
digits_to_uint64(
    Char const * CIG_RESTRICT a,// [i] string to read from
    std::size_t alen,           // [i] maximum length of a
    uint64_t *value,            // [o] value parsed, or 0
    integer_conversion_error *e)// [o] error description in case of failure
{
    constexpr uint64_t maxval = std::numeric_limits<uint64_t>::max();
    constexpr uint64_t max10 = maxval / 10;
    constexpr int rem10 = static_cast<int>(maxval - 10 * max10);
    constexpr uint64_t max16 = maxval / 16;
    constexpr int rem16 = static_cast<int>(maxval - 16 * max16);
    constexpr uint64_t max8 = maxval / 8;
    constexpr int rem8 = static_cast<int>(maxval - 8 * max8);
    constexpr uint64_t max2 = maxval / 2;
    constexpr int rem2 = static_cast<int>(maxval - 2 * max2);
    uint64_t x = 0;
    std::size_t i = 0;
    integer_conversion_error::errortype error_type;

    if (alen == 0)
        goto end_of_input_before_number;

    if (!isdigit(a[0]))
        goto number_must_start_with_a_digit;

    if (a[0] != '0')            // decimal
    {
        for (; i < alen; i++)
        {
            if (!isdigit(a[i]))
                break;
            int d = detail::digitval(a[i]);
            if (x >= max10 &&
                (x > max10 || d > rem10))
            {
                goto number_too_big;
            }
            x = 10 * x + static_cast<uint64_t>(d);
        }
    }
    else if (alen == 1)
    {
        // Nothing to do
        i = 1;
    }
    else if (a[1] == 'x' ||
             a[1] == 'X')       // hexadecimal
    {
        for (i = 2; i < alen; i++)
        {
            if (!isxdigit(a[i]))
                break;
            int d = detail::xdigitval(a[i]);
            if (x >= max16 &&
                (x > max16 || d > rem16))
            {
                goto number_too_big;
            }
            x = 16 * x + static_cast<uint64_t>(d);
        }
    }
    else if (a[1] == 'b' ||
             a[1] == 'B')       // binary
    {
        for (i = 2; i < alen; i++)
        {
            if (a[i] != '0' && a[i] != 1)
            {
                // Since a suffix cannot start with a digit,
                // any digit is an error
                if (isdigit(a[i]))
                    goto not_a_binary_digit;
                break;
            }
            int d = (a[i] == 1);
            if (x >= max2)
            {
                if (x > max2 || d > rem2)
                    goto number_too_big;
            }
            x = 2 * x + static_cast<uint64_t>(d);
        }
    }
    else                        // octal
    {
        for (i = 1; i < alen; i++)
        {
            if (!isodigit(a[i]))
            {
                // Since a suffix cannot start with a digit,
                // any digit is an error
                if (isdigit(a[i])) // '8' or '9'
                    goto not_an_octal_digit;
                break;
            }
            int d = detail::digitval(a[i]);
            if (x >= max8)
            {
                if (x > max8 || d > rem8)
                    goto number_too_big;
            }
            x = 8 * x + static_cast<uint64_t>(d);
        }
    }

    if (value)
        *value = x;
    if (e)
    {
        integer_conversion_error error;
        error.type = integer_conversion_error::NO_ERROR;
        *e = error;
    }
    return static_cast<std::ptrdiff_t>(i);

 end_of_input_before_number:
    error_type = integer_conversion_error::END_OF_INPUT_BEFORE_NUMBER;
    goto return_error;

 number_must_start_with_a_digit:
    error_type = integer_conversion_error::NUMBER_MUST_START_WITH_A_DIGIT;
    goto return_error;

 number_too_big:
    error_type = integer_conversion_error::NUMBER_IS_TOO_BIG;
    goto return_error;

 not_a_binary_digit:
    error_type = integer_conversion_error::NOT_A_BINARY_DIGIT;
    goto return_error;

 not_an_octal_digit:
    error_type = integer_conversion_error::NOT_AN_OCTAL_DIGIT;
    goto return_error;

 return_error:
    if (e)
    {
        integer_conversion_error error;
        error.type = error_type;
        *e = error;
    }
    if (value)
        *value = 0;
    return -1;
}


template<typename Char>
CIG_CONSTEXPR inline
int                             /* Suffix type, or -1; suffix types:
                                   0 none
                                   1 u
                                   2 i
                                   3 u8
                                   4 i8
                                   5 u16
                                   6 i16
                                   7 u32
                                   8 i32
                                   9 u64
                                   10 i64 */
decode_uint64_with_suffix(
    Char const * CIG_RESTRICT a,// [i] string to read from
    std::size_t alen,           // [i] maximum length of a
    uint64_t *value,            // [o] value parsed, or 0
    unsigned long *chars_parsed,// [o] no. chars read from a; can be 0
    integer_conversion_error *e)// [o] error description in case of failure
{
    integer_conversion_error error;
    uint64_t v;
    std::ptrdiff_t r = digits_to_uint64(a,alen,&v,&error);
    if (r == -1)
    {
        if (chars_parsed)
            *chars_parsed = 0;
        if (value)
            *value = v;
        if (e)
            *e = error;
        return -1;
    }

    // Check the suffix; the value had length r
    int suffixtype = 0;
    alen -= static_cast<std::size_t>(r);
    if (alen == 0)
    {
        // Leave suffixtype at 0
    }
    else if (a[r] == 'i')
    {
        if (alen == 1)
        {
            r += 1;
            suffixtype = 2;
        }
        else if (alen >= 2 && a[r+1] == '8')
        {
            if (alen > 2 && isalnum(a[r+2]))
                goto unrecognized_number_suffix;
            r += 2;
            suffixtype = 4;
        }
        else if (alen >= 3 && a[r+1] == '1' && a[r+2] == '6')
        {
            if (alen > 3 && isalnum(a[r+3]))
                goto unrecognized_number_suffix;
            r += 3;
            suffixtype = 6;
        }
        else if (alen >= 3 && a[r+1] == '3' && a[r+2] == '2')
        {
            if (alen > 3 && isalnum(a[r+3]))
                goto unrecognized_number_suffix;
            r += 3;
            suffixtype = 8;
        }
        else if (alen >= 3 && a[r+1] == '6' && a[r+2] == '4')
        {
            if (alen > 3 && isalnum(a[r+3]))
                goto unrecognized_number_suffix;
            r += 3;
            suffixtype = 10;
        }
        else if (alen > 1 && isalnum(a[r+1]))
            goto unrecognized_number_suffix;
        else
        {
            r += 1;
            suffixtype = 2;
        }
    }
    else if (a[r] == 'u')
    {
        if (alen == 1)
        {
            r += 1;
            suffixtype = 1;
        }
        else if (alen >= 2 && a[r+1] == '8')
        {
            if (alen > 2 && isalnum(a[r+2]))
                goto unrecognized_number_suffix;
            r += 2;
            suffixtype = 3;
        }
        else if (alen >= 3 && a[r+1] == '1' && a[r+2] == '6')
        {
            if (alen > 3 && isalnum(a[r+3]))
                goto unrecognized_number_suffix;
            r += 3;
            suffixtype = 5;
        }
        else if (alen >= 3 && a[r+1] == '3' && a[r+2] == '2')
        {
            if (alen > 3 && isalnum(a[r+3]))
                goto unrecognized_number_suffix;
            r += 3;
            suffixtype = 7;
        }
        else if (alen >= 3 && a[r+1] == '6' && a[r+2] == '4')
        {
            if (alen > 3 && isalnum(a[r+3]))
                goto unrecognized_number_suffix;
            r += 3;
            suffixtype = 9;
        }
        else if (alen > 1 && isalnum(a[r+1]))
            goto unrecognized_number_suffix;
        else
        {
            r += 1;
            suffixtype = 1;
        }
    }
    else if (isalpha(a[r]))
        goto unrecognized_number_suffix;

    if (value)
        *value = v;
    if (chars_parsed)
        *chars_parsed = static_cast<std::size_t>(r);
    if (e)
    {
        error.type = integer_conversion_error::NO_ERROR;
        *e = error;
    }
    return suffixtype;

 unrecognized_number_suffix:
    if (value)
        *value = v;
    if (chars_parsed)
        *chars_parsed = static_cast<unsigned long>(r);
    if (e)
    {
        error.type = integer_conversion_error::UNRECOGNIZED_NUMBER_SUFFIX;
        *e = error;
    }
    return -1;
}


} // namespace detail


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_INTEGER_CONVERSION_H_INCLUDED) */
