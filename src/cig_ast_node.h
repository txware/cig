/* cig_ast_node.h
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - AST node.
 */
#ifndef CIG_AST_NODE_H_INCLUDED
#define CIG_AST_NODE_H_INCLUDED

#include "cig_datatypes.h"
#include "cig_ast_literal.h"

#include <memory>               // std::shared_ptr
#include <tuple>                // std::tuple

namespace cig {

namespace ast {


// Common types
struct ast_node;
using ast_node_ptr = std::shared_ptr<ast_node>;
using exp_ast_node_ptr = std::expected<ast_node_ptr,cig::err>;
using ast_node_backref = std::tuple<ast_node_ptr,i64>;

// Builtin types
enum ast_builtin_type : int8_t; // CIG builtin types known in the AST tree
using ast_ei_type = int8_t;     // Expression info type
// For CIG builtin types, ast_ei_type and ast_builtin_type use the same value


//
// Various information structures contained in AST nodes
//

struct ast_global_info
{
    i64 globalconst_size = -1;
    i64 globalconst_alignment = -1;
    i64 globalvar_size = -1;
    i64 globalvar_alignment = -1;
};

struct ast_type_info
{
    i64 type_alignment = -1;
    i64 type_size = -1;
    ast_builtin_type bitype = static_cast<ast_builtin_type>(-1);
};


struct ast_variable_info
{
    ast_type_info ti;
    i64 offset = -1;
};


struct ast_function_info
{
    i64 runtime_data_alignment = -1;
    i64 runtime_data_size = -1;
};


// Kind of access to an expression value
//
// The offset is from the start of the memory area, e.g. from the start
// of the global variables, the start of the function-local stackmem,
// or the start of the class object.
enum class ast_access_kind {
    NOT_ACCESSIBLE,             // Inaccessible variable or object
    NO_ACCESS_TYPE_OR_NMSP,     // Data type (class or enum) or namespace
    ACCESS_CONSTANT,            // Literal or enum constant
    GLOBAL_OBJECT,              // Global variable or class-static variable
    GLOBAL_FUNCTION,            // Global function or class-static function
    LOCAL_OBJECT,               // Fn body variable or temporary in stackmem
    CLASS_OBJECT,               // This class or outer class
    CLASS_FUNCTION,             // Member function of this class or outer
    MEMORY_OBJECT,              // Object somewhere in memory
    MEMORY_FUNCTION             // Member function of object somewhere in memory
};


// Local representation of the expression result
//
// In case the expression result is a non-local object, some information
// about it needs to be stored in local memory (stackmem).
// The local value can be the address, an auxiliary pointer or a local
// rvalue copy of the value.
enum class ast_local_repr : int8_t {
    NO_LOCAL_COPY,              // Original value is already local
    RVALUE_COPY,                // Local copy of non-local lvalue
    LVALUE_ADDRESS,             // Address of a non-local lvalue
    POINTER_TEMPORARY           // A shared pointer temporary
};

struct ast_expression_info
{
    using enum ast_access_kind;
    using enum ast_local_repr;

    ast_access_kind access_kind = NOT_ACCESSIBLE;
    i64 offset = -1;            // For functions -1
    cig_bool combines_sub=false;// Offset is combined with subexpression offset
    i64 outer_level = -1;       // Current class 0, outer 1, outer of outer 2 ..
    ast_builtin_type bitype = static_cast<ast_builtin_type>(-1);
    ast_ei_type eitype = -1;    // Expression info type: oparg, interpreter only
    ast_local_repr local_repr = NO_LOCAL_COPY;
    i64 local_repr_offset = -1; // Local representation offset, or -1
};


//
// Name resolution for entities
//


// Name resolution is dependent on the sequence in which entities
// (classes, enums, variables, functions) are declared and defined
// in the source code.
// This must be upheld to maintain compatibility with C++.
// While a type (class, enum) is incomplete, it cannot be fully used,
// in particular it cannot be used as part (base class or member variable)
// of another type.

// Just like C++, and unlike Java or Python, the types must be defined
// in sequence, because just like in C++ and unlike Java or Python
// a type can be direct part of another type, for which its memory
// layout needs to be known.
// (In Java or Python, a type is only part of another type by implicit
// reference).

// The resolve_quality serves the purpose to inform about the relative
// position of a resolved entity relative to the place where it is
// looked up. This is particularly relevant for classes, where access
// from within the class (but mot within a function body) to types and
// variables is determined by the relative position in the source code.
// Only entities that have been declared or defined before the point
// of use can be correctly used.
// While a class is being defined, its resolution state is CLASSFROMDEF.

enum class ast_resolve_quality {
    AST_RQ_INVALID,             // Resolution invalid
    AST_RQ_NOTFOUND,            // Name/entity not found (node ptr is null)
    AST_RQ_VARIABLE,            // Variable with complete type
    AST_RQ_FUNPAR,              // Function parameter with complete type
    AST_RQ_CLASSDECL,           // Class declared but not yet defined
    AST_RQ_CLASSFROMDEF,        // Class name referenced from inside definition
    AST_RQ_CLASS,               // Class with complete type
    AST_RQ_ENUMDECL,            // Enum declared but not yet defined
    AST_RQ_ENUM,                // Enum type defined
    AST_RQ_ENUMVALUE,           // Enum value
    AST_RQ_FNNAME,              // Function name/entity
    AST_RQ_FUNCALL,             // Resolved function to call
    AST_RQ_NAMESPACE            // Namespace name/entity or global
};

using ast_resolved_entity = std::tuple<ast_node_ptr,ast_resolve_quality>;


//
// AST node
//


// Note:
// The AST node type behaviour emulates a virtual base class.
// However, as AST nodes need to be constructed during parsing and
// interpretation of the source code, it seems easier here to
// just use an enum to identify the AST node type.
enum class ast_node_type {
    AST_INVALID,
    AST_GLOBAL,                 // The global namespace (outermost namespace)
    AST_NAMESPACE,              // A namespace
    AST_CLASSDECL,              // A class declaration
    AST_CLASSDEF,               // A class definition
    AST_ENUMDECL,               // An enum declaration
    AST_ENUMDEF,                // An enum definition
    AST_ENUMVALUE,              // A single enum value
    AST_VARIABLE,               // A variable
    AST_OVERLOADSET,            // A function name with multiple overloaded fns
    AST_FUNDECL,                // A function declaration without body
    AST_FUNDEF,                 // A function definition
    AST_LAMBDA,                 // A lambda, i.e. a function without name
    AST_TYPEREF,                // A reference to a data type
    AST_FUNPAR,                 // A function parameter
    AST_FUNPARLIST,             // A function parameter list
    AST_FUNRET,                 // A function return parameter
    AST_FUNRETLIST,             // A function return parameter list
    AST_FUNBODY,                // A function body
    AST_EXPRMIN,                // Minimum expression value
    AST_EXPR_LITERAL,           // A literal value
    AST_EXPR_ENTITYLINK,        // A direct link to an entity instead of qu.nm
    AST_EXPR_MEMBERLINK,        // Direct link to member instead of mb.expr
    AST_EXPR_UNQUALNAME,        // An identifier (an unqualified name)
    AST_EXPR_MEMBER,            // A member operation
    AST_EXPR_ARRAYDEREF,        // An array dereferencing operation
    AST_EXPR_SLICE,             // An array slicing operation
    AST_EXPR_FUNCALL,           // A function call
    AST_EXPR_SIGNPLUS,          // A unary plus
    AST_EXPR_SIGNMINUS,         // A unary minus
    AST_EXPR_BIT_NOT,           // A bitwise inversion
    AST_EXPR_LOGICAL_NOT,       // A logical not operation
    AST_EXPR_MULT,              // A multiplication
    AST_EXPR_DIV,               // A division
    AST_EXPR_MOD,               // A modulo operation
    AST_EXPR_ADD,               // An addition
    AST_EXPR_SUB,               // A subtraction
    AST_EXPR_SHIFT_LEFT,        // A left-shift operation
    AST_EXPR_SHIFT_RIGHT,       // A right-shift operation
    AST_EXPR_LESS,              // A less-than comparison
    AST_EXPR_GREATER,           // A greater-than comparison
    AST_EXPR_LESS_EQUAL,        // A less-or-equal comparison
    AST_EXPR_GREATER_EQUAL,     // A greater-or-equal comparison
    AST_EXPR_EQUAL,             // An equal comparison
    AST_EXPR_NOT_EQUAL,         // An unequal comparison
    AST_EXPR_IN,                // An 'in' operation
    AST_EXPR_AND,               // A bitwise and operation
    AST_EXPR_XOR,               // A bitwise exclusive-or operation
    AST_EXPR_OR,                // A bitwise or operation
    AST_EXPR_LOGICAL_AND,       // A logical and operation
    AST_EXPR_LOGICAL_OR,        // A logical or operation
    AST_EXPR_CONDITION,         // A condition expression ? :
    AST_EXPR_LIST,              // A list expression
    AST_EXPR_ADD_ASSIGN,        // An add-and-reassign expression
    AST_EXPR_SUB_ASSIGN,        // A subtract-from-and-reassign expression
    AST_EXPR_MULT_ASSIGN,       // A multiply-and-reassign expression
    AST_EXPR_DIV_ASSIGN,        // A divide-by-and-reassign expression
    AST_EXPR_MOD_ASSIGN,        // A modulo-and-reassign expression
    AST_EXPR_SHL_ASSIGN,        // A shift-left-and-reassign expression
    AST_EXPR_SHR_ASSIGN,        // A shift-right-and-reassign expression
    AST_EXPR_AND_ASSIGN,        // A bitwise-and-and-reassign expression
    AST_EXPR_XOR_ASSIGN,        // A bitwise-xor-and-reassign expression
    AST_EXPR_OR_ASSIGN,         // A bitwise-or-and-reassign expression
    AST_EXPR_DOT_ASSIGN,        // An element-wise assignment of an object
    AST_EXPR_ASSIGN,            // A plain assignment expression
    AST_EXPRMAX,                // Maximum expression value
    AST_STMTMIN,                // Minimum statement value
    AST_COMPOUNDSTMT,           // A compound statement
    AST_RETURNSTMT,             // A return statement
    AST_EXPRSTMT,               // An expression statement
    AST_IFSTMT,                 // An if statement
    AST_WHILESTMT,              // A while statement
    AST_DOWHILESTMT,            // A do while statement
    AST_FORSTMT,                // A classical C-style for statement
    AST_FOROFSTMT,              // A new style for-of statement
    AST_BREAKSTMT,              // A break statement
    AST_CONTINUESTMT,           // A continue statement
    AST_STMTMAX,                // Maximum statement value
    AST_MAX
};


// Special backref indices, -1 means no backref, >= 0 is from subnode list
enum ast_node_br_special {
    AST_BR_INIT = -2,           // Init statement in for,if,while
    AST_BR_COND = -3,           // Condition in if, while, for, do..while
    AST_BR_BODY = -4,           // Body of while, do..while, for, for..of
    AST_BR_THEN = -5,           // Then statement in if
    AST_BR_ELSE = -6,           // Else statement in if
    AST_BR_ITERATE   = -7,      // Iterate expression in for
    AST_BR_ITERATOR  = -8,      // Iterator variables expression in for..of
    AST_BR_GENERATOR = -9,      // Generator expression in for..of
    AST_BR_QUNAME =    -10,     // Qualified name of fundef
    AST_BR_FUNPARLIST =-11,     // Function parameter list
    AST_BR_FUNRETLIST =-11,     // Function return list
    AST_BR_FUNBODY =   -12,     // Function body
    AST_BR_FUNDEF =    -13,     // Function definition from declaration
    AST_BR_CLASSDEF =  -14,     // Class definition from declaration
    AST_BR_ENUMDEF =   -15,     // Enum definition from declaration
    AST_BR_TYPEREF =   -16,     // Typeref from var/par/retval declaration
    AST_BR_REF =       -17,     // Entity reference from e.g. typeref
    AST_BR_STMTEXPR =  -18,     // Statement expression for expr stmt, return
    AST_BR_SUBEXPR1 =  -19,     // First subexpression of expression
    AST_BR_SUBEXPR2 =  -20,     // Second subexpression of expression
    AST_BR_SUBEXPR3 =  -21,     // Third subexpression of expression
    AST_BR_SUBEXPR4 =  -22,     // Fourth subexpression of expression
};


struct ast_node : std::enable_shared_from_this<ast_node> {

    enum ast_node_flags {
        AST_FLAG_NONE = 0x0,

        AST_FLAG_PTRMASK      = 0x7,
        AST_FLAG_SMARTPTRMASK = 0x4,
        AST_FLAG_NOPTR        = 0x0, // No pointer at all
        AST_FLAG_REFPTR       = 0x1, // A reference in C++ style
        AST_FLAG_WEAKPTR      = 0x4, // A std::weak_ptr in C++
        AST_FLAG_SHAREDPTR    = 0x6, // A std::shared_ptr in C++
        AST_FLAG_NONNULLPTR   = 0x7, // A guaranteed non-null std::shared_ptr

        AST_FLAG_NATIVE    = 0x08,
        AST_FLAG_STATIC    = 0x10, // Source requested static
        AST_FLAG_NONSTATIC = 0x20, // Source requested nonstatic

        AST_FLAG_MASK   = 0x3f  // All bits that can be flags
    };

public:
    static cig_bool             // true if ast node type is expr or variable
    ast_node_type_is_expr(ast_node_type t) noexcept;
    static cig_bool             // true if ast node type is an assignment type
    ast_node_type_is_assign_expr(ast_node_type t) noexcept;
    static cig_bool             // true if ast node type is a statement type
    ast_node_type_is_stmt(ast_node_type t) noexcept;
    static cig_bool             // true if ast node can be in subnode list
    ast_node_type_has_backref(ast_node_type t) noexcept;
    static str                  // node type tag or ""
    ast_node_type_tag(ast_node_type t) noexcept;

protected:
    ast_node_type node_type;
    cig::source_location startloc;  // first location of code
    cig::source_location effectloc; // between start and end
    cig::source_location endloc;    // after last location of code
    ast_node(ast_node_type t) : node_type(t) { }; // No public constructor

    // Auxiliary functions
    // Call these via add_named_entity, add_class, add_enum, add_function,
    //                add_substmt
    exp_none add_lookup_name(str const &nm, // The nm from child.get_name()
                             i64 index);    // The index from add_subnode()
    exp_i64 add_subnode(ast_node_ptr nd); // Add subnode to scope, return index
    exp_i64 add_named_entity_aux(ast_node_ptr entity,bool); // Auxiliary

    exp_none set_classdef(ast_node_ptr fundef) noexcept; // Add def to decl
    exp_none set_enumdef(ast_node_ptr fundef) noexcept; // Add def to decl
    exp_none set_fundef(ast_node_ptr fundef) noexcept; // Add def to decl
    exp_i64 add_overload(ast_node_ptr nd); // Add new ovld to set, return index

public:
    // Auxiliary information for dumping
    i64 dump_index = -1;


    // Creation
    ast_node_type get_node_type() const noexcept;
    static ast_node_ptr make_ast_node(ast_node_type t) noexcept;
    static ast_node_ptr make_ast_node(ast_node_type t,
                                      cig::source_location start,
                                      cig::source_location effect,
                                      cig::source_location end) noexcept;
    static ast_node_ptr make_builtin_ast_node(ast_type_info type_info) noexcept;


    // Location information
    cig::source_location get_startloc() const noexcept;
    cig::source_location get_effectloc() const noexcept;
    cig::source_location get_effectloc2() const noexcept;
    cig::source_location get_endloc() const noexcept;
    void set_startloc(cig::source_location const &sloc) noexcept;
    void set_effectloc(cig::source_location const &sloc) noexcept;
    void set_effectloc2(cig::source_location const &sloc) noexcept;
    void set_endloc(cig::source_location const &sloc) noexcept;


    // Descendent nodes of global scope
    i64 get_global_subnode_count() const noexcept; // Count of subnodes or -1
    ast_node_ptr get_global_subnode(i64 index) // AST node or nullptr
        const noexcept;


    // Child nodes in this scope
    i64 lookup_name(str const &nm) const noexcept; // Index of subnode, or -1
    cig::source_location lookup_name_location(str const &nm) const noexcept;
    i64 get_subnode_count() const noexcept; // Count of subnodes or -1
    ast_node_ptr get_subnode(i64 index) const noexcept; // AST node or nullptr
    ast_node_backref get_backref() const noexcept; // From subnode to scope
    ast_node_backref get_global_backref() const noexcept; // Subnode to global
    exp_i64 add_named_entity(ast_node_ptr entity); /* Add a: namespace,
                                                      enumvalue, variable,
                                                      funpar, funret;
                                                      see add_class, add_enum,
                                                      add_function below */


    // Cross-tree connectivity
    //
    // Created by program: entity link
    exp_none set_link(ast_node_ptr nd) noexcept; // Set link to entity
    ast_node_ptr get_link() const noexcept; // Link to entity (type,var,...)
    //
    // See also set_unqualified_name, set_member_name below
    //
    // Created by source code (qualified names: unqualified name with members)
    // or created by program: entity link with members
    exp_none set_ref(ast_node_ptr nd) noexcept; // Set qualified name in typeref
    ast_node_ptr get_ref() const noexcept; // Qualified name in typeref
    //
    // Specific type reference in variable, function parameter, return type
    exp_none set_typeref(ast_node_ptr nd) noexcept; // Set reference to datatype
    ast_node_ptr get_typeref() const noexcept; // Reference to data type


    // Class declaration and definition
    ast_node_ptr get_classdef() const noexcept; // Get def from decl
    ast_node_ptr get_classdecl_backref() const noexcept; // Get decl from def
    exp_i64 add_class(ast_node_ptr nd); // Add a class


    // Enum declaration and definition
    ast_node_ptr get_enumdef() const noexcept; // Get def from decl
    ast_node_ptr get_enumdecl_backref() const noexcept; // Get decl from def
    exp_i64 add_enum(ast_node_ptr nd); // Add an enum


    // Function declaration and definition
    i64 get_overload_count() const noexcept; // Count of overloads or -1
    ast_node_ptr get_overload(i64 index) const noexcept; // AST node or nullptr
    ast_node_backref get_overloadset_backref()
        const noexcept; /* Overload set for fundef or fundecl */
    exp_none set_fundef_qualified_name(ast_node_ptr nd) noexcept;
    ast_node_ptr get_fundef_qualified_name() const noexcept; // AST node/nullptr
    exp_none set_funparlist(ast_node_ptr nd) noexcept; // Parameter list
    ast_node_ptr get_funparlist() const noexcept; // AST node or nullptr
    exp_none set_funretlist(ast_node_ptr nd) noexcept; // Return type list
    ast_node_ptr get_funretlist() const noexcept; // AST node or nullptr
    exp_none set_funbody(ast_node_ptr nd) noexcept; // Function body
    ast_node_ptr get_funbody() const noexcept; // AST node or nullptr
    ast_node_ptr get_lambda_backref() const noexcept; // Body/par/ret to lambda
    ast_node_ptr get_fundef() const noexcept; // Get def from decl
    ast_node_ptr get_fundecl_backref() const noexcept; // Get decl from def
    // Note: The add_function method is the only one where non-local
    //       name resolution is done internally. The qualified name of
    //       a function definition outside its class scope needs to be
    //       resolved to find the class where it must be integrated.
    //       The function is called from the outside scope where the
    //       function is defined.
    exp_i64 add_function(ast_node_ptr fn) noexcept; /* Add fundef/fundecl at
                                                       point of code location */


    // Statements
    // Compound statment
    exp_i64 add_substmt(ast_node_ptr stmt); // Add stmt to scope, return index
    i64 get_substmt_count() const noexcept; // Count of substatements or -1
    ast_node_ptr get_substmt(i64 index) const noexcept; // AST node or nullptr
    // Expression statement
    exp_none set_expr(ast_node_ptr nd) noexcept; // Set statement expression
    ast_node_ptr get_expr() const noexcept; // Statement expression
    // If, while, do..while, for; backref index AST_BR_COND
    exp_none set_condexpr(ast_node_ptr nd) noexcept; // Set condition expression
    ast_node_ptr get_condexpr() const noexcept; // Condition expression
    // While, do..while, for, for..of; backref index AST_BR_BODY
    exp_none set_bodystmt(ast_node_ptr nd) noexcept; // Set statement body
    ast_node_ptr get_bodystmt() const noexcept; // Statement body
    // If; backref index AST_BR_THEN
    exp_none set_thenstmt(ast_node_ptr nd) noexcept; // Set then substatement
    ast_node_ptr get_thenstmt() const noexcept; // Then substatement
    // If; backref index AST_BR_ELSE
    exp_none set_elsestmt(ast_node_ptr nd) noexcept; // Set else substatement
    ast_node_ptr get_elsestmt() const noexcept; // Else substatement
    // C-style for; backref index AST_BR_ITERATE
    exp_none set_iterateexpr(ast_node_ptr nd) noexcept; // Set it. of Cstyle for
    ast_node_ptr get_iterateexpr() const noexcept; // Iterate expression of for
    // If, while, do..while, C-style for, for..of; backref index AST_BR_INIT
    exp_none set_initstmt(ast_node_ptr nd) noexcept; // Set init stmt of for
    ast_node_ptr get_initstmt() const noexcept; // Init stmt of for
    i64 get_initstmt_variables() const noexcept; // Count of v in init statement
    // For..in; backref index AST_BR_ITERATOR
    exp_none set_iteratorexpr(ast_node_ptr nd) noexcept; // Set iterators
    ast_node_ptr get_iteratorexpr() const noexcept; // Iterators of for..of
    // For..of; backref index AST_BR_GENERATOR
    exp_none set_generatorexpr(ast_node_ptr nd) noexcept; // Set generator expr
    ast_node_ptr get_generatorexpr() const noexcept; // Generator of for..of

    

    // Expressions
    exp_none set_subexpr1(ast_node_ptr nd) noexcept; // Set first subexpression
    ast_node_ptr get_subexpr1() const noexcept; // Subexpression 1
    exp_none set_subexpr2(ast_node_ptr nd) noexcept; // Set second subexpression
    ast_node_ptr get_subexpr2() const noexcept; // Subexpression 2
    exp_none set_subexpr3(ast_node_ptr nd) noexcept; // Set third subexpression
    ast_node_ptr get_subexpr3() const noexcept; // Subexpression 3
    exp_none set_subexpr4(ast_node_ptr nd) noexcept; // Set fourth subexpression
    ast_node_ptr get_subexpr4() const noexcept; // Subexpression 4

    exp_i64 add_listsubexpr(ast_node_ptr nd); // Add list element expr
    exp_none set_list_complete() noexcept;    // Set all list elements are added
    i64 get_listsubexpr_count() const noexcept; // Count of elements or -1
    ast_node_ptr get_listsubexpr(i64 index) const noexcept; // List element expr
    cig_bool is_list() const noexcept;         // Expression is list-valued
    cig_bool has_ulvalue() const noexcept;     // Expression has uninit. lvalue
    cig_bool has_rvalue() const noexcept;      // Expression has an rvalue
    cig_bool is_conditional() const noexcept;  /* Expression has a ? : in it
                                                  but not in array/slice index
                                                  or function arguments */
    cig_bool is_complete() const noexcept;     /* Node is complete, has all
                                                  necessary subnodes */


    // Various attributes
    literal const & get_literal() const noexcept;
    exp_none set_literal(literal const &lit) noexcept;
    exp_none set_literal(literal &&lit) noexcept;

    str const & get_unqualified_name() const noexcept;
    exp_none set_unqualified_name(str const &nm) noexcept;

    str const & get_member_name() const noexcept;
    exp_none set_member_name(str const &nm) noexcept;

    str const & get_name() const noexcept; // Name for lookup in parent scope
    exp_none set_name(str const &nm) noexcept;

    exp_i64 get_flags() const noexcept;
    exp_none set_flags(i64 flags) noexcept;
    cig_bool is_static() const noexcept;

    exp_none set_type_info(ast_type_info const &ti) noexcept;
    ast_type_info get_type_info() const noexcept;

    exp_none set_variable_info(ast_variable_info const &vi) noexcept;
    ast_variable_info get_variable_info() const noexcept;

    exp_none set_function_info(ast_function_info const &fi) noexcept;
    ast_function_info get_function_info() const noexcept;

    exp_none set_expression_info(ast_expression_info const &ei) noexcept;
    ast_expression_info get_expression_info() const noexcept;

    exp_none set_global_info(ast_global_info const &gi) noexcept;
    ast_global_info get_global_info() const noexcept;

    exp_none set_interpreter_function_index(i64 fnidx) noexcept;
    i64 get_interpreter_function_index() const noexcept;
    exp_none set_interpreter_datatype_index(i64 typeidx) noexcept;
    i64 get_interpreter_datatype_index() const noexcept;

    // Whether stmt declares variables with lifetime beyond stmt
    cig_bool declares_variables_visible_from_outside() const noexcept;

     // Fully qualified name for class,enum,function,variable, or ""
    str create_fqname(cig_bool with_global = cig_true) const noexcept;

    // Name resolution
    exp_none set_resolved_entity(ast_resolved_entity re,
                                 cig_bool replace=cig_false) noexcept;
    ast_resolved_entity get_resolved_entity() const noexcept;
    ast_node_ptr get_resolved_node() const noexcept;
    ast_resolve_quality get_resolved_quality() const noexcept;
};


bool // true if function parameter lists have identical typeref qualified names
funparlists_have_same_typerefs(ast::ast_node_ptr pl1,
                               ast::ast_node_ptr pl2);

bool // true if function return type lists have identical typeref qualifd. names
funretlists_have_same_typerefs(ast::ast_node_ptr pl1,
                               ast::ast_node_ptr pl2);


} // namespace ast

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_AST_NODE_H_INCLUDED) */
