/* cig_simple_dll.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Simple double-linked list that can be destructed in any order.
 */
#ifndef CIG_SIMPLE_DLL_H_INCLUDED
#define CIG_SIMPLE_DLL_H_INCLUDED

namespace cig {

namespace detail {

namespace dll {


// A simple double-linked list


// Each node starts out as a list of its own, a zero-size circle
// A node can be hooked into a list and taken out at any time.

// Each node is the base class of a container object
// that is returned by dll_next and dll_prev.

// The method is called the curiously recurring template pattern.


template<typename most_derived_class>
struct node
{
    // When the object is hooked in the DLL, both pointers are not this
    // Outside the DLL, both are this
    most_derived_class *dll_next; // Next in double linked list, or this
    most_derived_class *dll_prev; // Previous in double linked list, or this


    // Since the other nodes store this node's address, prohibit move or copy
    node(node&&) = delete;
    node& operator=(node&&) = delete;
    node(const node&) = delete;
    node& operator=(const node&) = delete;

private:
    void insert_between_nodes(node &p,node &n);
public:
    void withdraw_from_dll();
    void insert_after_node(node& p);
    void insert_before_node(node& n);
    void prepend_to_dll(node& head);
    void append_to_dll(node& head);
    bool is_in_dll() const;

    node();
    ~node();
};


template<typename most_derived_class>
inline node<most_derived_class>::node()
{
    dll_next = static_cast<most_derived_class*>(this);
    dll_prev = static_cast<most_derived_class*>(this);
}

template<typename most_derived_class>
inline void node<most_derived_class>::withdraw_from_dll()
{
    if (dll_next)
        dll_next->dll_prev = dll_prev;
    if (dll_prev)
        dll_prev->dll_next = dll_next;
    dll_next = static_cast<most_derived_class*>(this);
    dll_prev = static_cast<most_derived_class*>(this);
}

template<typename most_derived_class>
inline node<most_derived_class>::~node()
{
    withdraw_from_dll();
}

template<typename most_derived_class>
inline bool node<most_derived_class>::is_in_dll() const
{
    return 
        dll_next != static_cast<const most_derived_class*>(this) &&
        dll_prev != static_cast<const most_derived_class*>(this);
}

template<typename most_derived_class>
inline void node<most_derived_class>::insert_between_nodes(node &p,node &n)
{
    // Do nothing if already hooked into a DLL
    if ((dll_next && dll_next != static_cast<most_derived_class*>(this)) ||
        (dll_prev && dll_prev != static_cast<most_derived_class*>(this)))
        return;

    p.dll_next = static_cast<most_derived_class*>(this);
    n.dll_prev = static_cast<most_derived_class*>(this);
    dll_next = static_cast<most_derived_class*>(&n);
    dll_prev = static_cast<most_derived_class*>(&p);
}

template<typename most_derived_class>
inline void node<most_derived_class>::insert_after_node(node & p)
{
    // Do nothing if the DLL is broken
    if (!p.dll_next)
        return;
    insert_between_nodes(p,*p.dll_next);
}

template<typename most_derived_class>
inline void node<most_derived_class>::insert_before_node(node & n)
{
    // Do nothing if the DLL is broken
    if (!n.dll_prev)
        return;
    insert_between_nodes(*n.dll_prev,n);
}



template<typename most_derived_class>
inline void node<most_derived_class>::prepend_to_dll(node & head)
{
    insert_after_node(head);
}

template<typename most_derived_class>
inline void node<most_derived_class>::append_to_dll(node & head)
{
    insert_before_node(head);
}


} // namespace dll

} // namespace detail

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_SIMPLE_DLL_H_INCLUDED) */
