/* cig_bytes_inline.h
 *
 * Copyright 2022-2024 Claus Fischer
 *
 * Inline functions for bytes.
 */
#ifndef CIG_BYTES_INLINE_H_INCLUDED
#define CIG_BYTES_INLINE_H_INCLUDED

#include "cig_compatibility.h"  // CIG_RESTRICT,std::bit_cast
#include "cig_datatypes.h"
#include "cig_slicespec.h"      // slicespec
#include "cig_assert.h"         // cig_assert,cig_static_assert
#include "cig_allocation.h"     // cig::allocator

#include <cstddef>              // std::size_t
#include <utility>              // std::move
#include <stdexcept>            // std::invalid_argument
#include <type_traits>          // std::make_unsigned
#include <bit>                  // std::bit_cast
#include <compare>              // std::strong_ordering
#include <optional>             // std::optional,std::nullopt
#include <variant>              // std::variant,std::monostate
#include <tuple>                // std::tie

namespace cig {


namespace detail {


//
// bytesval
//


// Auxiliary functions to compose and decompose the cap value

constexpr bool bytesval::_is_pointered_by_cap(std::size_t cap) noexcept {
    return (cap & _long_mask) != 0;
}

constexpr bool bytesval::_is_allocated_by_cap(std::size_t cap) noexcept {
    // Long bytesings with zero capacity
    // are unallocated external arrays of literals
    return ((cap & _long_mask    ) != 0 &&
            (cap & _capacity_mask) != 0);
}

constexpr std::size_t bytesval::_capacity_by_cap(std::size_t cap) noexcept {
    return
        _is_pointered_by_cap(cap) ? (cap & _capacity_mask) :
        static_cast<std::size_t>(_intlen_net) + 1; // add '\0'
}

constexpr std::size_t bytesval::_cap_by_components(
    std::size_t capacity,bool is_pointered) noexcept
{
    return
        (capacity /* multiple of 16 */ & _capacity_mask) |
        (is_pointered ? static_cast<std::size_t>(_long_mask) : 0);
}



// Auxiliary functions

constexpr bool bytesval::_is_pointered() const noexcept {
    return _is_pointered_by_cap(_l._cap);
}

constexpr bool bytesval::_is_allocated() const noexcept {
    // Long bytesings with zero _cap are unallocated external arrays of literals
    return _is_allocated_by_cap(_l._cap);
}

constexpr bytesval::byte_t *bytesval::_data() noexcept {
    return _is_pointered() ?
        std::bit_cast<byte_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short*>(&_l)->_data + _int_offset;
}

constexpr const bytesval::byte_t *bytesval::_data() const noexcept {
    return _is_pointered() ?
        std::bit_cast<byte_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short const *>(&_l)->_data + _int_offset;
}

constexpr std::size_t bytesval::_size_to_allocate(
    std::size_t size,bool duplicate) noexcept {
    // add at least 1 byte for '\0' then round up to multiple of 16
    if (duplicate)              // used for += operator
    {
        std::size_t sz = 16;
        while (sz <= size)
            sz *= 2;
        return sz;
    }
    else
        return (size + (16 - size % 16));
}

// Function capacity returns the net capacity of the bytesing w/o trailing '\0'
constexpr std::size_t bytesval::_get_capacity() const noexcept {
    return _capacity_by_cap(_l._cap) - 1; // subtract the '\0'
}

// Function size returns the net length of the bytesing w/o trailing '\0'
constexpr std::size_t bytesval::_get_size() const noexcept {
    return _is_pointered() ? _l._size : ((_l._cap & 0xff) >> 3);
}

constexpr void bytesval::_construct_clear()
{
    _l._cap = 0;
    _l._size = 0;
    _l._data = 0;
}

constexpr void bytesval::_construct_by_move(bytesval &&s) noexcept
{
    _l = s._l;
    s._construct_clear();
}


constexpr i64 bytesval::capacity() const noexcept {
    return static_cast<int64_t>(_get_capacity());
}

constexpr i64 bytesval::size() const noexcept {
    return static_cast<int64_t>(_get_size());
}

constexpr cig_bool bytesval::is_empty() const noexcept {
    return static_cast<int64_t>(_get_size()) == 0;
}

// Function byte_at returns the byte at idx, or -1
constexpr i64 bytesval::byte_at(i64 idx) const noexcept {
    std::size_t sz = _get_size();
    if (idx._v < 0 || static_cast<std::size_t>(idx._v) >= sz)
        return -1;
    bytesval::byte_t const * CIG_RESTRICT data = _data();
    return data[idx._v];
}


constexpr i64 bytesval::operator [] (i64 idx) const noexcept {
    return byte_at(idx);
}


constexpr
i64                             // minimum byte value; -1 if bytesval is empty
bytesval::min() const noexcept
{
    int64_t vmin = i64::max_value;

    std::size_t endpos = _get_size();
    bytesval::byte_t const * CIG_RESTRICT data = _data();
    for (std::size_t pos = 0; pos < endpos; pos++)
    {
        bytesval::byte_t v = data[pos];
        if (vmin > v)
            vmin = v;
    }

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return vmin;
}


constexpr
i64                             // maximum byte value; -1 if bytesval is empty
bytesval::max() const noexcept
{
    int64_t vmax = -1;

    std::size_t endpos = _get_size();
    bytesval::byte_t const * CIG_RESTRICT data = _data();
    for (std::size_t pos = 0; pos < endpos; pos++)
    {
        bytesval::byte_t v = data[pos];
        if (vmax < v)
            vmax = v;
    }

    return vmax;
}


constexpr
std::pair<i64,i64>              // min,max; -1,-1 if bytesval is empty
bytesval::minmax() const noexcept
{
    int64_t vmin = i64::max_value;
    int64_t vmax = -1;

    std::size_t endpos = _get_size();
    bytesval::byte_t const * CIG_RESTRICT data = _data();
    for (std::size_t pos = 0; pos < endpos; pos++)
    {
        bytesval::byte_t v = data[pos];
        if (vmin > v)
            vmin = v;
        if (vmax < v)
            vmax = v;
    }

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return { vmin, vmax };
}


constexpr
bool                            // true if min() <= 0x7f
bytesval::is_pure_ascii() const noexcept
{
    i64 v = min();
    return v._v <= 0x7f;
}


// Function bytes_memcpy copies bytes with potentially different base types
template<typename S,typename O,bool do_cast=false>
constexpr void
bytes_memcpy(
    O * CIG_RESTRICT o,         // [o] output bytes
    S const * CIG_RESTRICT s,   // [i] input bytes
    std::size_t n               // [i] number of bytes
    )
{
    typedef typename std::make_unsigned<S>::type US;
    
    // if (sizeof(O) == sizeof(S))
    // {
    //     std::copy(s,s+n,o);
    //     return;
    // }

    if constexpr (do_cast)
        for (std::size_t i = 0; i < n; i++)
            o[i] = static_cast<O>(static_cast<US>(s[i]));
    else
        for (std::size_t i = 0; i < n; i++)
            o[i] = static_cast<US>(s[i]);
}


// Function bytes_memcpy_ascii_tolower copies and converts ASCII to lowercase
template<typename S,typename O,bool do_cast=false>
constexpr void
bytes_memcpy_ascii_tolower(
    O * CIG_RESTRICT o,         // [o] output bytes
    S const * CIG_RESTRICT s,   // [i] input bytes
    std::size_t n               // [i] number of bytes
    )
{
    typedef typename std::make_unsigned<S>::type US;
    
    if constexpr (do_cast)
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = static_cast<O>(u >= 65 && u < 91 ? (u | 0x20) : u);
        }
    }
    else
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = (u >= 65 && u < 91 ? (u | 0x20) : u);
        }
    }
}


// Function bytes_memcpy_ascii_toupper copies and converts ASCII to uppercase
template<typename S,typename O,bool do_cast=false>
constexpr void
bytes_memcpy_ascii_toupper(
    O * CIG_RESTRICT o,         // [o] output bytes
    S const * CIG_RESTRICT s,   // [i] input bytes
    std::size_t n               // [i] number of bytes
    )
{
    typedef typename std::make_unsigned<S>::type US;
    constexpr auto mask = static_cast<US>(~static_cast<US>(0x20));

    if constexpr (do_cast)
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = static_cast<O>(u >= 97 && u < 123 ? (u & mask) : u);
        }
    }
    else
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = (u >= 97 && u < 123 ? (u & mask) : u);
        }
    }
}


// Function bytes_memcmp compares byte arrays with potentially diff. base types
template<typename S1,typename S2>
constexpr std::strong_ordering
bytes_memcmp(
    S1 const * CIG_RESTRICT s1, // [i] input bytes of first array
    S2 const * CIG_RESTRICT s2, // [i] input bytes of second array
    std::size_t n1,             // [i] number of bytes in first array
    std::size_t n2              // [i] number of bytes in second array
    )
{
    typedef typename std::make_unsigned<S1>::type US1;
    typedef typename std::make_unsigned<S2>::type US2;
    std::size_t n = (n1 < n2 ? n1 : n2);
    std::size_t i;

    for (i = 0; i < n; i++)
        if (static_cast<US1>(s1[i]) != static_cast<US2>(s2[i]))
            break;
    if (i < n)
    {
        US1 c1 = static_cast<US1>(s1[i]);
        US2 c2 = static_cast<US2>(s2[i]);
        return
            (c1 < c2 ? std::strong_ordering::less :
             c1 > c2 ? std::strong_ordering::greater :
             std::strong_ordering::equal);
    }
    return
        (n1 < n2 ? std::strong_ordering::less :
         n1 > n2 ? std::strong_ordering::greater :
         std::strong_ordering::equal);
}


// Function bytes_zero fills a bytes array with zero-valued bytes
template<typename O>
constexpr void
bytes_zero(
    O * CIG_RESTRICT o,         // [o] output bytes
    std::size_t n               // [i] number of bytes
    )
{
    std::size_t i;

    for (i = 0; i < n; i++)
        o[i] = 0;
}


} // namespace detail


//
// bytes
//


// Allocation functions (central place of allocation for bytes)
inline detail::bytesval::byte_t *
bytes::_allocate(std::size_t capacity) {
    return baseallocator<"byte_t">::alloc<byte_t>(capacity);
}
inline void bytes::_delete(byte_t *ptr,[[maybe_unused]] std::size_t capacity) {
    baseallocator<"byte_t">::free<byte_t>(ptr,capacity);
}

// Function _delete_by_cap deletes internal pointer by externally provided cap
constexpr void bytes::_delete_by_cap(std::size_t cap)
{
    // This function leaves 'uninitialized memory'

    // Nothing to delete
    if (!_is_allocated_by_cap(cap))
        return;

    // The allocated size is capacity+1
    std::size_t oldcapacity = _capacity_by_cap(cap); // with '\0'

    byte_t *byteptr = std::bit_cast<byte_t*,std::size_t>(_l._data);
    _delete(byteptr,oldcapacity);
}

// Function _destruct is the named destructor
constexpr void bytes::_destruct()
{
    // This function leaves 'uninitialized memory'
    _delete_by_cap(_l._cap);
}

// Official destructor
constexpr bytes::~bytes() {
    _destruct();
}

// Function _construct_clear is a named constructor
constexpr void bytes::_construct_clear()
{
    bytesval::_construct_clear();
}

// Function _construct_by_move is a named move constructor
constexpr void bytes::_construct_by_move(bytesval &&s) noexcept
{
    bytesval::_construct_by_move(std::move(s));
}

// Function _reserve ensures place for capacity
constexpr void bytes::_reserve(std::size_t newsize,bool duplicate) {
    std::size_t oldsize = _get_size();
    std::size_t const oldcap = _l._cap;
    std::size_t oldcapacity = _capacity_by_cap(oldcap); // with '\0'

    // Note that for external literal arrays, oldcapacity is 0

    // Nothing to do?
    if (newsize + 1 <= oldcapacity)
        return;

    // Does it fit in the structure?
    bool old_is_pointered = _is_pointered_by_cap(oldcap);
    if (newsize <= _intlen_net /* net */ &&
        !old_is_pointered)
        return;

    std::size_t newcap = _size_to_allocate(newsize,duplicate);
    byte_t *b = _allocate(newcap);
    if (oldsize)
        detail::bytes_memcpy(b,_data(),oldsize);
    b[oldsize] = '\0';
    _delete_by_cap(oldcap);
    _l._data = std::bit_cast<size_t,byte_t*>(b);
    _l._size = oldsize;
    _l._cap = _cap_by_components(newcap,true);
}

constexpr void bytes::_set_size(std::size_t sz, bool zero_initialize)
{
    std::size_t cap = _l._cap;
    std::size_t oldcapacity = _capacity_by_cap(cap);

    // Set the size shorter or longer than the current size
    // If size is increased, this leaves memory uninitialized

    // Do not operate on outside arrays of literal bytes-arrays
    if (oldcapacity == 0)
        return;

    // Do not exceed the reserved capacity
    if (sz >= oldcapacity)
        sz = oldcapacity - 1;

    if (_is_pointered_by_cap(cap))
    {
        std::size_t oldsz = _l._size;
        _l._size = sz;
        if (zero_initialize && oldsz < sz)
            detail::bytes_zero(
                std::bit_cast<byte_t*,std::size_t>(_l._data)+oldsz,
                sz-oldsz);
        std::bit_cast<byte_t*,std::size_t>(_l._data)[sz] = '\0';
    }
    else
    {
        byte_t *b = reinterpret_cast<_short*>(&_l)->_data;
        std::size_t oldsz = b[_length_byte] >> 3;
        b[_length_byte] = static_cast<byte_t>(sz << 3);
        if (zero_initialize && oldsz < sz)
            detail::bytes_zero(&b[oldsz + _int_offset],sz-oldsz);
        b[sz + _int_offset] = '\0';
    }
}

template <typename Byte>
constexpr void bytes::_construct_from_bytes(
    Byte const *data, std::size_t size)
{
    static_assert(sizeof(Byte) == sizeof(byte_t));

    // This function is to be called from constructors only
    if (size > _intlen_net)
    {
        std::size_t newcap = _size_to_allocate(size);
        byte_t *b = _allocate(newcap);
        detail::bytes_memcpy(b,data,size);
        b[size] = '\0';
        _construct_clear();
        _l._data = std::bit_cast<std::size_t,byte_t*>(b);
        _l._size = size;
        _l._cap = _cap_by_components(newcap,true);
    }
    else
    {
        _short dst { };
        dst._data[_length_byte] =
            static_cast<byte_t>(size << 3);
        if (size)
            detail::bytes_memcpy(dst._data + _int_offset,data,size);
        _l = std::bit_cast<_long,_short>(dst);
    }
}

constexpr void bytes::_construct_a_copy(const bytesval &s)
{
    if (!s._is_allocated())
        // This function will copy the unallocated pointer of a literal
        this->_l = s._l;
    else
        _construct_from_bytes(s._data(),s._l._size);
}

constexpr void bytes::_construct_by_move(bytes &&s) noexcept
{
    _l = s._l;
    s._construct_clear();
}

// Copy constructor
constexpr bytes::bytes(const bytesval &s) {
    _construct_a_copy(s);
}
constexpr bytes::bytes(const bytes &s) : bytesval() {
    _construct_a_copy(s);
}

// Copy assignment operator
constexpr bytes & bytes::operator = (const bytesval &s) {
    if (this != &s)
    {
        _destruct();
        _construct_a_copy(s);
    }
    return *this;
}
constexpr bytes & bytes::operator = (const bytes &s) {
    if (this != &s)
    {
        _destruct();
        _construct_a_copy(s);
    }
    return *this;
}

// Move constructor
constexpr bytes::bytes(bytesval &&s) noexcept {
    bytesval::_construct_by_move(std::move(s));
}

constexpr bytes::bytes(bytes &&s) noexcept {
    bytesval::_construct_by_move(std::move(s));
}

// Move assignment operator
constexpr bytes & bytes::operator = (bytes &&s) {
    if (this != &s)
    {
        _destruct();
        _l = s._l;
        s._construct_clear();
    }
    return *this;
}


// Auxiliary constructors
template<typename Byte>
constexpr bytes bytes::from_c_bytes(Byte const *data, std::size_t size)
{
    static_assert(sizeof(Byte) == sizeof(byte_t));

    bytes s;
    s._construct_from_bytes(data,size);
    return s;
}


constexpr void bytes::reserve(i64 newsize,cig_bool duplicate)
{
    if (newsize._v < 0)         // Nothing to do
        return;
    _reserve(static_cast<std::size_t>(newsize._v),duplicate._v);
}


// if zero_initialize is false and size increases,
// caller must resize ... fill ... set_size even on error
constexpr void bytes::set_size(i64 newsize,cig_bool zero_initialize)
{
    std::size_t sz = _get_size();
    std::size_t newsz =
        newsize._v < 0 ? 0 : static_cast<std::size_t>(newsize._v);
    if (newsz > _get_capacity())
    {
        // resize if filling with zeros is allowed
        if (zero_initialize._v)
        {
            reserve(newsize,true); // This is a duplicate situation
        }
        // otherwise enforce the current size with a terminating 0 char
        // since it is highly likely the caller made an error
        else
        {
            _set_size(sz);
            return;
        }
    }
    // Now set the size
    _set_size(newsz,zero_initialize);
}


constexpr
bytes                           // only ASCII characters converted to lowercase
detail::bytesval::ascii_tolower() const
{
    std::size_t sz = _get_size();
    if (sz <= _intlen_net) // short
    {
        byte_t a[_intlen_net] = { };
        bytes_memcpy_ascii_tolower(a,_data(),sz);
        return bytes::from_c_bytes(a,sz);
    }
    else
    {
        bytes a;
        a._reserve(sz);
        bytes_memcpy_ascii_tolower(a._data(),_data(),sz);
        a._set_size(sz);
        return a;
    }
}


constexpr
bytes                           // only ASCII characters converted to uppercase
detail::bytesval::ascii_toupper() const
{
    std::size_t sz = _get_size();
    if (sz <= _intlen_net) // short
    {
        byte_t a[_intlen_net] = { };
        bytes_memcpy_ascii_toupper(a,_data(),sz);
        return bytes::from_c_bytes(a,sz);
    }
    else
    {
        bytes a;
        a._reserve(sz);
        bytes_memcpy_ascii_toupper(a._data(),_data(),sz);
        a._set_size(sz);
        return a;
    }
}


/**********************************************************************/
/*                                                                    */
/*                         BYTES EXPRESSIONS                          */
/*                                                                    */
/**********************************************************************/


namespace detail {


// Bytes expression class for addition and multiplication operator
struct bytesexpr {
    enum operation {
        plus_bytes_bytes,
        plus_bytes_expr,
        plus_expr_bytes,
        plus_expr_expr,
        plus_bytes_other,
        plus_expr_other,
        plus_other_bytes,
        plus_other_expr,
        mult_bytes,
        mult_expr,
        slice_bytes,
        slice_expr
    };

    union { bytesval const *s; bytesexpr const *e; } _ex1, _ex2;
    operation _op;              // Operation between left and right subtree
    std::size_t size;           // Size of strexpr tree result bytes
    std::variant<std::monostate,bytes,uint64_t,slicespec> aux;

    static constexpr bool op_needs_saved_op1(operation op);
    static constexpr bool op_needs_saved_op2(operation op);
    static constexpr bool op_is_slice(operation op);

    // Copy constructor omitted on purpose
    bytesexpr(bytesexpr &&) noexcept;
    bytesexpr(operation op,bytesval const &s1,bytesval const &s2);
    bytesexpr(operation op,bytesval const &s1,bytesexpr const &s2);
    bytesexpr(operation op,bytesexpr const &s1,bytesval const &s2);
    bytesexpr(operation op,bytesexpr const &s1,bytesexpr const &s2);
    bytesexpr(operation op,bytesval const &s,i64 m);
    bytesexpr(operation op,bytesexpr const &s,i64 m);
    bytesexpr(operation op,bytesval const &s,slicespec spec);
    bytesexpr(operation op,bytesexpr const &s,slicespec spec);
    ~bytesexpr();

    bytesexpr slice(std::optional<i64> start, // Python like semantics
                    std::optional<i64> end = std::nullopt,
                    std::optional<i64> step = std::nullopt) const noexcept;
    
private:

    //
    // Auxiliary functions for leaf nodes
    //

    // Aux: Full copy a bytesval leaf node (not this tree) onto o
    template<typename O>
    static constexpr std::size_t // size copied
    bytesval_evaluate_onto(bytesval const &s,O *o) noexcept;

    // Aux: Slice a bytesval leaf node (not this tree) onto o
    template<typename O>
    static constexpr std::size_t // size filled
    bytesval_slice_onto(bytesval const &s,O *o,slicespec spec) noexcept;

    // Aux: Determine min/max of slice of bytesval leaf node (not this tree)
    static constexpr i64 // minimum byte value; -1 if bytesval is empty
    bytesval_slice_min(bytesval const &s,slicespec spec) noexcept;
    static constexpr i64 // maximum byte value; -1 if bytesval is empty
    bytesval_slice_max(bytesval const &s,slicespec spec) noexcept;
    static constexpr std::pair<i64,i64> // min and max byte v.; -1,-1 if empty
    bytesval_slice_minmax(bytesval const &s,slicespec spec) noexcept;

    //
    // Recursive operations on strexpr trees
    //

    // Copy a subslice of the strexpr tree onto o
    template<typename O>
    constexpr std::size_t       // size filled
    slice_onto(O *o,slicespec spec) const noexcept;

    // Determine min/max of a subslice of the bytesexpr tree
    constexpr i64 // minimum byte value; -1 if bytesval is empty
    slice_min(slicespec spec) const noexcept;
    constexpr i64 // maximum byte value; -1 if bytesval is empty
    slice_max(slicespec spec) const noexcept;
    constexpr std::pair<i64,i64> // min and max byte value; -1,-1 if empty
    slice_minmax(slicespec spec) const noexcept;

    //
    // Top level node operations
    //

public:

    // Copy the bytesexpr tree onto o
    template<typename O>
    constexpr std::size_t       // size written
    evaluate_onto(O *o) const noexcept;

    // Determine minimum and maximum byte value; return -1 for empty bytesexpr
    constexpr i64 min() const noexcept; // -1 if empty
    constexpr i64 max() const noexcept; // -1 if empty
    constexpr std::pair<i64,i64> minmax() const noexcept; // both

    friend struct cig::bytes;
};


// Static aux: Fully copy a bytesval leaf node onto the output o
template <typename O>
constexpr std::size_t           // size copied
bytesexpr::bytesval_evaluate_onto(bytesval const &s,O * o) noexcept
{
    std::size_t sz = s._get_size();
    if (sz)
        detail::bytes_memcpy(o,s._data(),sz);
    return sz;
}


// Static aux: Slice a bytesval leaf node onto the output o
template <typename O>
constexpr std::size_t           // size filled
bytesexpr::bytesval_slice_onto(bytesval const &s,O * CIG_RESTRICT o,
                               slicespec spec) noexcept
{
    std::size_t j = 0;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return 0;               // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        if (o)
        {
            bytesval::byte_t const * CIG_RESTRICT data = s._data();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
                o[j++] = static_cast<O>(data[pos]);
        }
        else // no o
        {
            j = spec.count;
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        if (o)
        {
            bytesval::byte_t const * CIG_RESTRICT data = s._data();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                o[j++] = static_cast<O>(data[pos]);
                pos += ustep;
            }
        }
        else // no o
        {
            j = spec.count;
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        if (o)
        {
            bytesval::byte_t const * CIG_RESTRICT data = s._data();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                o[j++] = static_cast<O>(data[pos]);
                pos -= ustep;
            }
        }
        else // no o
        {
            j = spec.count;
        }
    }

    // At most all bytes may have been placed in o
    cig_assert(j <= s._get_size());

    return j;
}


// Static aux: Determine min of slice of bytesval leaf node (not this tree)
constexpr i64                   // minimum byte value, -1 if slice is empty
bytesexpr::bytesval_slice_min(bytesval const &s,slicespec spec) noexcept
{
    std::size_t j = 0;
    int64_t vmin = i64::max_value;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return -1;              // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t pos = spec.start; pos < endpos; pos++)
        {
            bytesval::byte_t v = data[pos];
            if (vmin > v)
                vmin = v;
            j++;
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t i = 0; i < spec.count; i++)
        {
            bytesval::byte_t v = data[pos];
            pos += ustep;
            if (vmin > v)
                vmin = v;
            j++;
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t i = 0; i < spec.count; i++)
        {
            bytesval::byte_t v = data[pos];
            pos -= ustep;
            if (vmin > v)
                vmin = v;
            j++;
        }
    }

    // At most all bytes may have been examined
    cig_assert(j <= s._get_size());

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return vmin;
}


// Static aux: Determine max of slice of bytesval leaf node (not this tree)
constexpr i64                   // maximum byte value, -1 if slice is empty
bytesexpr::bytesval_slice_max(bytesval const &s,slicespec spec) noexcept
{
    std::size_t j = 0;
    int64_t vmax = -1;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return -1;              // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t pos = spec.start; pos < endpos; pos++)
        {
            bytesval::byte_t v = data[pos];
            if (vmax < v)
                vmax = v;
            j++;
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t i = 0; i < spec.count; i++)
        {
            bytesval::byte_t v = data[pos];
            pos += ustep;
            if (vmax < v)
                vmax = v;
            j++;
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t i = 0; i < spec.count; i++)
        {
            bytesval::byte_t v = data[pos];
            pos -= ustep;
            if (vmax < v)
                vmax = v;
            j++;
        }
    }

    // At most all bytes may have been examined
    cig_assert(j <= s._get_size());

    return vmax;
}


// Static aux: Determine min/max of slice of bytesval leaf node (not this tree)
constexpr
std::pair<i64,i64>              // min,max; -1,-1 if slice is empty
bytesexpr::bytesval_slice_minmax(bytesval const &s,slicespec spec) noexcept
{
    std::size_t j = 0;
    int64_t vmin = i64::max_value;
    int64_t vmax = -1;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return { -1, -1 };      // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t pos = spec.start; pos < endpos; pos++)
        {
            bytesval::byte_t v = data[pos];
            if (vmin > v)
                vmin = v;
            if (vmax < v)
                vmax = v;
            j++;
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t i = 0; i < spec.count; i++)
        {
            bytesval::byte_t v = data[pos];
            pos += ustep;
            if (vmin > v)
                vmin = v;
            if (vmax < v)
                vmax = v;
            j++;
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        bytesval::byte_t const * CIG_RESTRICT data = s._data();
        for (std::size_t i = 0; i < spec.count; i++)
        {
            bytesval::byte_t v = data[pos];
            pos -= ustep;
            if (vmin > v)
                vmin = v;
            if (vmax < v)
                vmax = v;
            j++;
        }
    }

    // At most all bytes may have been examined
    cig_assert(j <= s._get_size());

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return { vmin, vmax };
}


// Copy a subslice of the strexpr tree onto an output o
template<typename O>
constexpr std::size_t           // size filled
bytesexpr::slice_onto(O *o,slicespec spec) const noexcept
{
    std::size_t sz1;
    std::size_t sz2;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = bytesval_slice_onto<O>(*_ex1.s,o,specarr[0]);
            sz2 = bytesval_slice_onto<O>(*_ex2.s,o + sz1,specarr[1]);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = bytesval_slice_onto<O>(*_ex2.s,o,specarr[1]);
            sz1 = bytesval_slice_onto<O>(*_ex1.s,o + sz2,specarr[0]);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case plus_bytes_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = bytesval_slice_onto<O>(*_ex1.s,o,specarr[0]);
            sz2 = _ex2.e->slice_onto<O>(o + sz1,specarr[1]);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = _ex2.e->slice_onto<O>(o,specarr[1]);
            sz1 = bytesval_slice_onto<O>(*_ex1.s,o + sz2,specarr[0]);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case plus_expr_bytes:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = _ex1.e->slice_onto<O>(o,specarr[0]);
            sz2 = bytesval_slice_onto<O>(*_ex2.s,o + sz1,specarr[1]);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = bytesval_slice_onto<O>(*_ex2.s,o,specarr[1]);
            sz1 = _ex1.e->slice_onto<O>(o + sz2,specarr[0]);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = _ex1.e->slice_onto<O>(o,specarr[0]);
            sz2 = _ex2.e->slice_onto<O>(o + sz1,specarr[1]);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = _ex2.e->slice_onto<O>(o,specarr[1]);
            sz1 = _ex1.e->slice_onto<O>(o + sz2,specarr[0]);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case mult_bytes:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                sz2 = bytesval_slice_onto<O>(*_ex1.s,o+sz1,partspec);
                sz1 += sz2;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                sz2 = bytesval_slice_onto<O>(*_ex1.s,o+sz1,partspec);
                sz1 += sz2;
            }
        }
        cig_assert(sz1 <= size);
        return sz1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                sz2 = _ex1.e->slice_onto<O>(o+sz1,partspec);
                sz1 += sz2;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                sz2 = _ex1.e->slice_onto<O>(o+sz1,partspec);
                sz1 += sz2;
            }
        }
        cig_assert(sz1 <= size);
        return sz1;
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            sz1 = bytesval_slice_onto<O>(*_ex1.s,o,cumulated);
            cig_assert(sz1 <= size);
        }
        return sz1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            sz1 = _ex1.e->slice_onto<O>(o,cumulated);
            cig_assert(sz1 <= size);
        }
        return sz1;
    }
    cig_assert(false);          // all cases covered
    return 0;
}


// Determine min of a subslice of the bytesexpr tree
constexpr
i64                             // minimum byte value; -1 if bytesval is empty
bytesexpr::slice_min(slicespec spec) const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = bytesval_slice_min(*_ex1.s,specarr[0]);
            min2 = bytesval_slice_min(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = bytesval_slice_min(*_ex2.s,specarr[1]);
            min1 = bytesval_slice_min(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case plus_bytes_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = bytesval_slice_min(*_ex1.s,specarr[0]);
            min2 = _ex2.e->slice_min(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = _ex2.e->slice_min(specarr[1]);
            min1 = bytesval_slice_min(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case plus_expr_bytes:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = _ex1.e->slice_min(specarr[0]);
            min2 = bytesval_slice_min(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = bytesval_slice_min(*_ex2.s,specarr[1]);
            min1 = _ex1.e->slice_min(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = _ex1.e->slice_min(specarr[0]);
            min2 = _ex2.e->slice_min(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = _ex2.e->slice_min(specarr[1]);
            min1 = _ex1.e->slice_min(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case mult_bytes:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                min2 = bytesval_slice_min(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                min2 = bytesval_slice_min(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        return min1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                min2 = _ex1.e->slice_min(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                min2 = _ex1.e->slice_min(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        return min1;
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            min1 = bytesval_slice_min(*_ex1.s,cumulated);
        }
        return min1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            min1 = _ex1.e->slice_min(cumulated);
        }
        return min1;
    }
    return min1;
}


// Determine max of a subslice of the bytesexpr tree
constexpr
i64                             // maximum byte value; -1 if bytesval is empty
bytesexpr::slice_max(slicespec spec) const noexcept
{
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = bytesval_slice_max(*_ex1.s,specarr[0]);
            max2 = bytesval_slice_max(*_ex2.s,specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = bytesval_slice_max(*_ex2.s,specarr[1]);
            max1 = bytesval_slice_max(*_ex1.s,specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case plus_bytes_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = bytesval_slice_max(*_ex1.s,specarr[0]);
            max2 = _ex2.e->slice_max(specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = _ex2.e->slice_max(specarr[1]);
            max1 = bytesval_slice_max(*_ex1.s,specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case plus_expr_bytes:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = _ex1.e->slice_max(specarr[0]);
            max2 = bytesval_slice_max(*_ex2.s,specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = bytesval_slice_max(*_ex2.s,specarr[1]);
            max1 = _ex1.e->slice_max(specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = _ex1.e->slice_max(specarr[0]);
            max2 = _ex2.e->slice_max(specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = _ex2.e->slice_max(specarr[1]);
            max1 = _ex1.e->slice_max(specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case mult_bytes:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                max2 = bytesval_slice_max(*_ex1.s,partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                max2 = bytesval_slice_max(*_ex1.s,partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return max1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                max2 = _ex1.e->slice_max(partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                max2 = _ex1.e->slice_max(partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return max1;
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            max1 = bytesval_slice_max(*_ex1.s,cumulated);
        }
        return max1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            max1 = _ex1.e->slice_max(cumulated);
        }
        return max1;
    }
    return max1;
}


// Determine min/max of a subslice of the bytesexpr tree
constexpr
std::pair<i64,i64>              // min,max; -1,-1 if slice is empty
bytesexpr::slice_minmax(slicespec spec) const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = bytesval_slice_minmax(*_ex1.s,specarr[0]);
            std::tie(min2,max2) = bytesval_slice_minmax(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = bytesval_slice_minmax(*_ex2.s,specarr[1]);
            std::tie(min1,max1) = bytesval_slice_minmax(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case plus_bytes_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = bytesval_slice_minmax(*_ex1.s,specarr[0]);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            std::tie(min1,max1) = bytesval_slice_minmax(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case plus_expr_bytes:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            std::tie(min2,max2) = bytesval_slice_minmax(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = bytesval_slice_minmax(*_ex2.s,specarr[1]);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case mult_bytes:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                std::tie(min2,max2) = bytesval_slice_minmax(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                std::tie(min2,max2) = bytesval_slice_minmax(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return { min1, max1 };
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                std::tie(min2,max2) = _ex1.e->slice_minmax(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                std::tie(min2,max2) = _ex1.e->slice_minmax(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return { min1, max1 };
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            std::tie(min1,max1) = bytesval_slice_minmax(*_ex1.s,cumulated);
        }
        return { min1, max1 };
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            std::tie(min1,max1) = _ex1.e->slice_minmax(cumulated);
        }
        return { min1, max1 };
    }
    return { min1, max1 };
}


// Copy the bytesexpr tree onto the output o
template<typename O>
constexpr std::size_t           // size copied
bytesexpr::evaluate_onto(O *o) const noexcept
{
    std::size_t sz1;
    std::size_t sz2;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        sz1 = bytesval_evaluate_onto(*_ex1.s,o);
        sz2 = bytesval_evaluate_onto(*_ex2.s,o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;
    case plus_bytes_expr:
    case plus_other_expr:
        sz1 = bytesval_evaluate_onto(*_ex1.s,o);
        sz2 = _ex2.e->evaluate_onto(o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;
    case plus_expr_bytes:
    case plus_expr_other:
        sz1 = _ex1.e->evaluate_onto(o);
        sz2 = bytesval_evaluate_onto(*_ex2.s,o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;
    case plus_expr_expr:
        sz1 = _ex1.e->evaluate_onto(o);
        sz2 = _ex2.e->evaluate_onto(o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;

    case mult_bytes:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                sz1 += bytesval_evaluate_onto(*_ex1.s,o + sz1);
            }
        }
        return sz1;
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                sz1 += _ex1.e->evaluate_onto(o + sz1);
            }
        }
        return sz1;
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            sz1 = bytesval_slice_onto(*_ex1.s,o,specaux);
        }
        return sz1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            sz1 = _ex1.e->slice_onto(o,specaux);
        }
        return sz1;
    }
    return 0;
}


// Determine minimum byte value
constexpr 
i64                             // minimum byte value; -1 if bytesexpr is empty
bytesexpr::min() const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        min1 = _ex1.s->min();
        min2 = _ex2.s->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case plus_bytes_expr:
    case plus_other_expr:
        min1 = _ex1.s->min();
        min2 = _ex2.e->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case plus_expr_bytes:
    case plus_expr_other:
        min1 = _ex1.e->min();
        min2 = _ex2.s->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case plus_expr_expr:
        min1 = _ex1.e->min();
        min2 = _ex2.e->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case mult_bytes:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            min1 = _ex1.s->min();
        }
        return min1;
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            min1 = _ex1.e->min();
        }
        return min1;
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            min1 = bytesval_slice_min(*_ex1.s,specaux);
        }
        return min1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            min1 = _ex1.e->slice_min(specaux);
        }
        return min1;
    }
    return min1;
}


// Determine maximum byte value
constexpr 
i64                             // maximum byte value; -1 if bytesexpr is empty
bytesexpr::max() const noexcept
{
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        max1 = _ex1.s->max();
        max2 = _ex2.s->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case plus_bytes_expr:
    case plus_other_expr:
        max1 = _ex1.s->max();
        max2 = _ex2.e->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case plus_expr_bytes:
    case plus_expr_other:
        max1 = _ex1.e->max();
        max2 = _ex2.s->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case plus_expr_expr:
        max1 = _ex1.e->max();
        max2 = _ex2.e->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case mult_bytes:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            max1 = _ex1.s->max();
        }
        return max1;
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            max1 = _ex1.e->max();
        }
        return max1;
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            max1 = bytesval_slice_max(*_ex1.s,specaux);
        }
        return max1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            max1 = _ex1.e->slice_max(specaux);
        }
        return max1;
    }
    return max1;
}


// Determine minimum and maximum byte value
constexpr 
std::pair<i64,i64>              // min,max; -1,-1 if bytesexpr is empty
bytesexpr::minmax() const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_bytes_bytes:
    case plus_bytes_other:
    case plus_other_bytes:
        std::tie(min1,max1) = _ex1.s->minmax();
        std::tie(min2,max2) = _ex2.s->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case plus_bytes_expr:
    case plus_other_expr:
        std::tie(min1,max1) = _ex1.s->minmax();
        std::tie(min2,max2) = _ex2.e->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case plus_expr_bytes:
    case plus_expr_other:
        std::tie(min1,max1) = _ex1.e->minmax();
        std::tie(min2,max2) = _ex2.s->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case plus_expr_expr:
        std::tie(min1,max1) = _ex1.e->minmax();
        std::tie(min2,max2) = _ex2.e->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case mult_bytes:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return { -1, -1 };
            std::tie(min1,max1) = _ex1.s->minmax();
        }
        return { min1, max1 };
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return { -1, -1 };
            std::tie(min1,max1) = _ex1.e->minmax();
        }
        return { min1, max1 };
    case slice_bytes:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            std::tie(min1,max1) = bytesval_slice_minmax(*_ex1.s,specaux);
        }
        return { min1, max1 };
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specaux);
        }
        return { min1, max1 };
    }
    return { min1, max1 };
}


inline bytesexpr::~bytesexpr()
{
}

inline bytesexpr::bytesexpr(bytesexpr &&e) noexcept :
    _ex1(e._ex1), _ex2(e._ex2), _op(e._op), size(e.size),
    aux(std::move(e.aux))
{
}

constexpr bool bytesexpr::op_needs_saved_op1(bytesexpr::operation op)
{
    return (op == plus_other_bytes ||
            op == plus_other_expr);
}

constexpr bool bytesexpr::op_needs_saved_op2(bytesexpr::operation op)
{
    return (op == plus_bytes_other ||
            op == plus_expr_other);
}

constexpr bool bytesexpr::op_is_slice(bytesexpr::operation op)
{
    return (op == slice_bytes ||
            op == slice_expr);
}

// Constructor from bytesval,bytesval
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesval const &s1,bytesval const &s2) :
    _op(op)
{
    // Some operators require that one of the bytes is stored internally
    if (op_needs_saved_op1(op))
    {
        aux = bytes(s1);
        _ex1.s = std::get_if<bytes>(&aux);
        _ex2.s = &s2;
    }
    else if (op_needs_saved_op2(op))
    {
        _ex1.s = &s1;
        aux = bytes(s2);
        _ex2.s = std::get_if<bytes>(&aux);
    }
    else
    {
        _ex1.s = &s1;
        _ex2.s = &s2;
    }
    std::size_t sz1 = s1._get_size();
    std::size_t sz2 = s2._get_size();
    size = sz1 + sz2;
}

// Constructor from bytesval,bytesexpr
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesval const &s1,bytesexpr const &s2) :
    _op(op)
{
    // Some operators require that one of the bytes is stored internally
    if (op_needs_saved_op1(op))
    {
        aux = bytes(s1);
        _ex1.s = std::get_if<bytes>(&aux);
        _ex2.e = &s2;
    }
    else
    {
        _ex1.s = &s1;
        _ex2.e = &s2;
    }
    std::size_t sz1 = s1._get_size();
    std::size_t sz2 = s2.size;
    size = sz1 + sz2;
}

// Constructor from bytesexpr,bytesval
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesexpr const &s1,bytesval const &s2) :
    _op(op)
{
    // Some operators require that one of the bytes is stored internally
    if (op_needs_saved_op2(op))
    {
        _ex1.e = &s1;
        aux = bytes(s2);
        _ex2.s = std::get_if<bytes>(&aux);
    }
    else
    {
        _ex1.e = &s1;
        _ex2.s = &s2;
    }
    std::size_t sz1 = s1.size;
    std::size_t sz2 = s2._get_size();
    size = sz1 + sz2;
}

// Constructor from bytesexpr,bytesexpr
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesexpr const &s1,bytesexpr const &s2) :
    _op(op)
{
    _ex1.e = &s1;
    _ex2.e = &s2;
    std::size_t sz1 = s1.size;
    std::size_t sz2 = s2.size;
    size = sz1 + sz2;
}

// Constructor from bytesval,number
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesval const &s1,i64 m) :
    _op(op)
{
    _ex1.s = &s1;
    _ex2.s = 0;
    uint64_t multaux = static_cast<uint64_t>(m._v > 0 ? m._v : 0);
    aux = multaux;
    size = s1._get_size() * multaux;
}

// Constructor from bytesexpr,number
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesexpr const &s1,i64 m) :
    _op(op)
{
    _ex1.e = &s1;
    _ex2.s = 0;
    uint64_t multaux = static_cast<uint64_t>(m._v > 0 ? m._v : 0);
    aux = multaux;
    size = s1.size * multaux;
}

// Constructor from bytesval,slicespec
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesval const &s1,slicespec spec) :
    _op(op)
{
    _ex1.s = &s1;
    _ex2.s = 0;
    aux = spec;
    size = spec.count;
}

// Constructor from bytesexpr,slicespec
inline bytesexpr::bytesexpr(
    bytesexpr::operation op,bytesexpr const &s1,slicespec spec) :
    _op(op)
{
    _ex1.e = &s1;
    _ex2.s = 0;
    aux = spec;
    size = spec.count;
}

// Slice member function of bytesval
inline bytesexpr
bytesval::slice(std::optional<i64> start, // Python like semantics
                std::optional<i64> end,
                std::optional<i64> step) const noexcept
{
    std::size_t sz = _get_size();
    // fixme If step is 0, this will not have a value, better handle that here
    slicespec spec = setup_slicespec_like_python(sz,start,end,step).value();
    return bytesexpr(bytesexpr::slice_bytes,*this,spec);
}

// Slice member function of bytesexpr
inline bytesexpr
bytesexpr::slice(std::optional<i64> start, // Python like semantics
                 std::optional<i64> end,
                 std::optional<i64> step) const noexcept
{
    std::size_t sz = size;
    // fixme If step is 0, this will not have a value, better handle that here
    slicespec spec = setup_slicespec_like_python(sz,start,end,step).value();
    return bytesexpr(bytesexpr::slice_expr,*this,spec);
}



} // namespace detail


inline detail::bytesexpr operator + (detail::bytesval const &s1) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_bytes,s1,1); // use bytes * 1 for unary +
}

inline detail::bytesexpr operator + (bytes const &s1) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_bytes,s1,1); // use bytes * 1 for unary +
}

inline detail::bytesexpr operator + (detail::bytesexpr const &s1) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_expr,s1,1); // use bytes * 1 for unary +
}

inline cig_bool operator ! (detail::bytesval const &s1) {
    return s1.is_empty();
}

inline cig_bool operator ! (bytes const &s1) {
    return s1.is_empty();
}

inline cig_bool operator ! (detail::bytesexpr const &s1) {
    return s1.size == 0;
}

inline detail::bytesexpr operator + (detail::bytesval const &s1,detail::bytesval const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_bytes,s1,s2);
}

inline detail::bytesexpr operator + (detail::bytesval const &s1,bytes const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_bytes,s1,s2);
}

inline detail::bytesexpr operator + (bytes const &s1,detail::bytesval const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_bytes,s1,s2);
}

inline detail::bytesexpr operator + (bytes const &s1,bytes const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_bytes,s1,s2);
}

inline detail::bytesexpr operator + (detail::bytesval const &s1,detail::bytesexpr const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_expr,s1,s2);
}

inline detail::bytesexpr operator + (bytes const &s1,detail::bytesexpr const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_expr,s1,s2);
}

inline detail::bytesexpr operator + (detail::bytesexpr const &s1,detail::bytesval const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_expr_bytes,s1,s2);
}

inline detail::bytesexpr operator + (detail::bytesexpr const &s1,bytes const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_expr_bytes,s1,s2);
}

inline detail::bytesexpr operator + (detail::bytesexpr const &s1,detail::bytesexpr const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_expr_expr,s1,s2);
}

template<typename T>
inline detail::bytesexpr operator + (detail::bytesval const &s1,T const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_other,s1,s2.to_bytes());
}

template<typename T>
inline detail::bytesexpr operator + (bytes const &s1,T const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_bytes_other,s1,s2.to_bytes());
}

template<typename T>
inline detail::bytesexpr operator + (detail::bytesexpr const &s1,T const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_expr_other,s1,s2.to_bytes());
}

template<typename T>
inline detail::bytesexpr operator + (T const &s1,detail::bytesval const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_other_bytes,s1.to_bytes(),s2);
}

template<typename T>
inline detail::bytesexpr operator + (T const &s1,bytes const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_other_bytes,s1.to_bytes(),s2);
}

template<typename T>
inline detail::bytesexpr operator + (T const &s1,detail::bytesexpr const &s2) {
    using namespace detail;
    return bytesexpr(bytesexpr::plus_other_expr,s1,s2);
}

inline detail::bytesexpr operator * (detail::bytesval const &s,i64 const &m) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_bytes,s,m._v);
}

inline detail::bytesexpr operator * (bytes const &s,i64 const &m) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_bytes,s,m._v);
}

inline detail::bytesexpr operator * (detail::bytesexpr const &s,i64 const &m) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_expr,s,m._v);
}

inline detail::bytesexpr operator * (i64 const &m,detail::bytesval const &s) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_bytes,s,m._v);
}

inline detail::bytesexpr operator * (i64 const &m,bytes const &s) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_bytes,s,m._v);
}

inline detail::bytesexpr operator * (i64 const &m,detail::bytesexpr const &s) {
    using namespace detail;
    return bytesexpr(bytesexpr::mult_expr,s,m._v);
}

inline bytes & bytes::operator += (const bytesval &s) {
    using namespace detail;

    // Note that *this may be s
    std::size_t sz = _get_size();
    std::size_t szend = sz + s._get_size();

    _reserve(szend,true); // reallocate in powers of 2

    std::size_t szaux = bytesexpr::bytesval_evaluate_onto(s,_data() + sz);
    cig_assert(sz + szaux == szend);

    // Set size and add '\0'
    _set_size(szend);

    return *this;
}

constexpr bytes & bytes::operator *= (i64 m) {

    if (m._v <= 0)
    {
        _destruct();
        _construct_clear();
    }
    else if (m._v > 1)
    {
        std::size_t mv = static_cast<std::size_t>(m._v);
        std::size_t sz = _get_size();
        if (sz)
        {
            std::size_t i;
            _reserve(mv * sz);
            byte_t *data = _data();
            for (i = 1; i < mv; i++)
                detail::bytes_memcpy(data + i * sz,data,sz);
            _set_size(mv * sz);
        }
    }

    return *this;
}

inline bytes::bytes(const detail::bytesexpr &e)
{
    using namespace detail;

    std::size_t sz = e.size;

    _construct_clear();
    _reserve(sz,false);

    std::size_t szaux = e.evaluate_onto(_data());
    cig_assert(szaux == sz);

    // Set size and add '\0'
    _set_size(sz);
}

inline bytes & bytes::operator = (const detail::bytesexpr &e)
{
    using namespace detail;

    // Note that *this may be used in the expression e
    bytes r;
    std::size_t sz = e.size;

    r._reserve(sz,false);

    std::size_t szaux = e.evaluate_onto(r._data());
    cig_assert(szaux == sz);

    // Set size and add '\0'
    r._set_size(sz);

    // Move to *this
    _destruct();
    _l = r._l;
    r._construct_clear();
    return *this;
}

inline bytes & bytes::operator += (const detail::bytesexpr &e)
{
    using namespace detail;

    // Note that *this may be used in the expression e
    std::size_t sz = _get_size();
    std::size_t szend = sz + e.size;

    _reserve(szend,true); // reallocate in powers of 2

    std::size_t szaux = e.evaluate_onto(_data() + sz);
    cig_assert(sz + szaux == szend);

    // Set size and add '\0'
    _set_size(szend);

    return *this;
}


constexpr
std::strong_ordering            // b1 <=> b2
operator <=>(const detail::bytesval &b1,const detail::bytesval &b2) noexcept
{
    return detail::bytes_memcmp(b1._data(),b2._data(),
                                b1._get_size(),b2._get_size());
}


inline
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::bytesval &s1,const detail::bytesexpr &s2) noexcept
{
    return operator <=> (s1,bytes(s2));
}


inline
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::bytesexpr &s1,const detail::bytesval &s2) noexcept
{
    return operator <=> (bytes(s1),s2);
}


inline
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::bytesexpr &s1,const detail::bytesexpr &s2) noexcept
{
    return operator <=> (bytes(s1),bytes(s2));
}


constexpr
bool
operator ==(const detail::bytesval &b1,const detail::bytesval &b2) noexcept
{
    return operator <=>(b1,b2) == 0;
}


inline
bool
operator ==(const detail::bytesval &s1,const detail::bytesexpr &s2) noexcept
{
    return operator <=>(s1,bytes(s2)) == 0;
}


inline
bool
operator ==(const detail::bytesexpr &s1,const detail::bytesval &s2) noexcept
{
    return operator <=>(bytes(s1),s2) == 0;
}


inline
bool
operator ==(const detail::bytesexpr &s1,const detail::bytesexpr &s2) noexcept
{
    return operator <=>(bytes(s1),bytes(s2)) == 0;
}


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_BYTES_INLINE_H_INCLUDED) */
