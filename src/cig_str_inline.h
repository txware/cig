/* cig_str_inline.h
 *
 * Copyright 2022-2024 Claus Fischer
 *
 * Inline functions for str.
 */
#ifndef CIG_STR_INLINE_H_INCLUDED
#define CIG_STR_INLINE_H_INCLUDED

#include "cig_compatibility.h"  // CIG_RESTRICT,std::bit_cast
#include "cig_datatypes.h"
#include "cig_slicespec.h"      // slicespec
#include "cig_assert.h"         // cig_assert,cig_static_assert
#include "cig_allocation.h"     // cig::allocator

#include <cstddef>              // std::size_t
#include <utility>              // std::move
#include <stdexcept>            // std::invalid_argument
#include <type_traits>          // std::make_unsigned
#include <bit>                  // std::bit_cast
#include <compare>              // std::strong_ordering
#include <optional>             // std::optional,std::nullopt
#include <variant>              // std::variant,std::monostate
#include <tuple>                // std::tie

namespace cig {


namespace detail {


// Disable the -Wstrict-aliasing warning for the reinterpret_casts
#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif


//
// strval
//


// Auxiliary functions to compose and decompose the cap value

constexpr bool strval::_is_pointered_by_cap(std::size_t cap) noexcept {
    return (cap & _long_mask) != 0;
}

constexpr bool strval::_is_allocated_by_cap(std::size_t cap) noexcept {
    // Long strings with zero capacity
    // are unallocated external arrays of literals
    return ((cap & _long_mask    ) != 0 &&
            (cap & _capacity_mask) != 0);
}

constexpr strval::_chartype strval::_chartype_by_cap(std::size_t cap) noexcept {
    return static_cast<_chartype>(cap & _chartype_mask);
}

constexpr bool strval::_is_ucs1_by_cap(std::size_t cap) noexcept {
    // this function reports the storage size, not the chartype
    _chartype t = _chartype_by_cap(cap);
    return (t == _chartype_ascii || t == _chartype_ucs1);
}

constexpr bool strval::_is_ucs2_by_cap(std::size_t cap) noexcept {
    return _chartype_by_cap(cap) == _chartype_ucs2;
}

constexpr bool strval::_is_ucs4_by_cap(std::size_t cap) noexcept {
    return _chartype_by_cap(cap) == _chartype_ucs4;
}

constexpr bool strval::_is_ascii_by_cap(std::size_t cap) noexcept {
    return _chartype_by_cap(cap) == _chartype_ascii;
}

constexpr std::size_t strval::_intcapacity_by_chartype(_chartype t) noexcept {
    return                      // returns net capacity
        t == _chartype_ucs4 ? static_cast<std::size_t>(_c32_len_net) :
        t == _chartype_ucs2 ? static_cast<std::size_t>(_c16_len_net) :
        static_cast<std::size_t>(_c8_len_net);
}

constexpr std::size_t strval::_capacity_by_cap(std::size_t cap) noexcept {
    return
        _is_pointered_by_cap(cap) ? (cap & _capacity_mask) :
        _intcapacity_by_chartype(_chartype_by_cap(cap)) + 1; // add '\0'
}

constexpr std::size_t strval::_cap_by_components(
    std::size_t capacity,_chartype chartype,bool is_pointered) noexcept
{
    return
        (capacity /* multiple of 16 */ & _capacity_mask) |
        static_cast<std::size_t>(chartype) |
        (is_pointered ? static_cast<std::size_t>(_long_mask) : 0);
}



// Auxiliary functions

constexpr bool strval::_is_pointered() const noexcept {
    return _is_pointered_by_cap(_l._cap);
}

constexpr bool strval::_is_allocated() const noexcept {
    // Long strings with zero _cap are unallocated external arrays of literals
    return _is_allocated_by_cap(_l._cap);
}

constexpr strval::_chartype strval::_get_chartype() const noexcept {
    return _chartype_by_cap(_l._cap);
}

constexpr bool strval::_is_ucs1() const noexcept {
    // this function reports the storage size, not the chartype
    return _is_ucs1_by_cap(_l._cap);
}

constexpr bool strval::_is_ucs2() const noexcept {
    return _is_ucs2_by_cap(_l._cap);
}

constexpr bool strval::_is_ucs4() const noexcept {
    return _is_ucs4_by_cap(_l._cap);
}

constexpr bool strval::_is_ascii() const noexcept {
    return _is_ascii_by_cap(_l._cap);
}

constexpr strval::ucs1_t *strval::_data_ucs1() noexcept {
    return _is_pointered() ?
        std::bit_cast<ucs1_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short_u8*>(&_l)->c8 + _c8_offset;
}

constexpr strval::ucs2_t *strval::_data_ucs2() noexcept {
    return _is_pointered() ?
        std::bit_cast<ucs2_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short_u16*>(&_l)->c16 + _c16_offset;
}

constexpr strval::ucs4_t *strval::_data_ucs4() noexcept {
    return _is_pointered() ?
        std::bit_cast<ucs4_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short_u32*>(&_l)->c32 + _c32_offset;
}

constexpr const strval::ucs1_t *strval::_data_ucs1() const noexcept {
    return _is_pointered() ?
        std::bit_cast<ucs1_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short_u8 const *>(&_l)->c8 + _c8_offset;
}

constexpr const strval::ucs2_t *strval::_data_ucs2() const noexcept {
    return _is_pointered() ?
        std::bit_cast<ucs2_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short_u16 const *>(&_l)->c16 + _c16_offset;
}

constexpr const strval::ucs4_t *strval::_data_ucs4() const noexcept {
    return _is_pointered() ?
        std::bit_cast<ucs4_t*,std::size_t>(_l._data) :
        reinterpret_cast<_short_u32 const *>(&_l)->c32 + _c32_offset;
}

constexpr std::size_t strval::_size_to_allocate(
    std::size_t size, bool duplicate) noexcept {
    // add at least 1 char for '\0' then round up to multiple of 16
    if (duplicate)              // used for += operator
    {
        std::size_t sz = 16;
        while (sz <= size)
            sz *= 2;
        return sz;
    }
    else
        return (size + (16 - size % 16));
}

// Function capacity returns the net capacity of the string w/o trailing '\0'
constexpr std::size_t strval::_get_capacity() const noexcept {
    return _capacity_by_cap(_l._cap) - 1; // subtract the '\0'
}

// Function size returns the net length of the string w/o trailing '\0'
constexpr std::size_t strval::_get_size() const noexcept {
    return _is_pointered() ? _l._size : ((_l._cap & 0xff) >> 3);
}

constexpr void strval::_construct_clear(_chartype chartype)
{
    _l._cap = chartype;
    _l._size = 0;
    _l._data = 0;
}

constexpr void strval::_construct_by_move(strval &&s) noexcept
{
    _l = s._l;
    s._construct_clear();
}


constexpr i64 strval::capacity() const noexcept {
    return static_cast<int64_t>(_get_capacity());
}

constexpr i64 strval::size() const noexcept {
    return static_cast<int64_t>(_get_size());
}

constexpr cig_bool strval::is_empty() const noexcept {
    return static_cast<int64_t>(_get_size()) == 0;
}

// Function char_at returns the character at idx, or -1
constexpr i64 strval::char_at(i64 idx) const noexcept {
    std::size_t sz = _get_size();
    if (idx._v < 0 || static_cast<std::size_t>(idx._v) >= sz)
        return -1;
    if (_is_ucs4())
    {
        strval::ucs4_t const * CIG_RESTRICT data = _data_ucs4();
        return data[idx._v];
    }
    else if (_is_ucs2())
    {
        strval::ucs2_t const * CIG_RESTRICT data = _data_ucs2();
        return data[idx._v];
    }
    else
    {
        strval::ucs1_t const * CIG_RESTRICT data = _data_ucs1();
        return data[idx._v];
    }
}


constexpr i64 strval::operator [] (i64 idx) const noexcept {
    return char_at(idx);
}


constexpr
i64                             // minimum code point; -1 if strval is empty
strval::min() const noexcept
{
    int64_t vmin = i64::max_value;

    std::size_t endpos = _get_size();
    if (_is_ucs4())
    {
        strval::ucs4_t const * CIG_RESTRICT data = _data_ucs4();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs4_t v = data[pos];
            if (vmin > v)
                vmin = v;
        }
    }
    else if (_is_ucs2())
    {
        strval::ucs2_t const * CIG_RESTRICT data = _data_ucs2();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs2_t v = data[pos];
            if (vmin > v)
                vmin = v;
        }
    }
    else
    {
        strval::ucs1_t const * CIG_RESTRICT data = _data_ucs1();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs1_t v = data[pos];
            if (vmin > v)
                vmin = v;
        }
    }

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return vmin;
}


constexpr
i64                             // maximum code point; -1 if strval is empty
strval::max() const noexcept
{
    int64_t vmax = -1;

    std::size_t endpos = _get_size();
    if (_is_ucs4())
    {
        strval::ucs4_t const * CIG_RESTRICT data = _data_ucs4();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs4_t v = data[pos];
            if (vmax < v)
                vmax = v;
        }
    }
    else if (_is_ucs2())
    {
        strval::ucs2_t const * CIG_RESTRICT data = _data_ucs2();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs2_t v = data[pos];
            if (vmax < v)
                vmax = v;
        }
    }
    else
    {
        strval::ucs1_t const * CIG_RESTRICT data = _data_ucs1();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs1_t v = data[pos];
            if (vmax < v)
                vmax = v;
        }
    }

    return vmax;
}


constexpr
std::pair<i64,i64>              // min,max; -1,-1 if strval is empty
strval::minmax() const noexcept
{
    int64_t vmin = i64::max_value;
    int64_t vmax = -1;

    std::size_t endpos = _get_size();
    if (_is_ucs4())
    {
        strval::ucs4_t const * CIG_RESTRICT data = _data_ucs4();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs4_t v = data[pos];
            if (vmin > v)
                vmin = v;
            if (vmax < v)
                vmax = v;
        }
    }
    else if (_is_ucs2())
    {
        strval::ucs2_t const * CIG_RESTRICT data = _data_ucs2();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs2_t v = data[pos];
            if (vmin > v)
                vmin = v;
            if (vmax < v)
                vmax = v;
        }
    }
    else
    {
        strval::ucs1_t const * CIG_RESTRICT data = _data_ucs1();
        for (std::size_t pos = 0; pos < endpos; pos++)
        {
            strval::ucs1_t v = data[pos];
            if (vmin > v)
                vmin = v;
            if (vmax < v)
                vmax = v;
        }
    }

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return { vmin, vmax };
}


constexpr
bool                            // true if _is_ascii() or min() <= 0x7f
strval::is_pure_ascii() const noexcept
{
    if (_is_ascii())
        return true;
    i64 v = min();
    return v._v <= 0x7f;
}


// Function str_memcpy copies string parts with potentially different base types
template<typename S,typename O,bool do_cast=false>
constexpr void
str_memcpy(
    O * CIG_RESTRICT o,         // [o] output code points
    S const * CIG_RESTRICT s,   // [i] input code points
    std::size_t n               // [i] number of code points
    )
{
    typedef typename std::make_unsigned<S>::type US;
    
    // if (sizeof(O) == sizeof(S))
    // {
    //     std::copy(s,s+n,o);
    //     return;
    // }

    if constexpr (do_cast)
        for (std::size_t i = 0; i < n; i++)
            o[i] = static_cast<O>(static_cast<US>(s[i]));
    else
        for (std::size_t i = 0; i < n; i++)
            o[i] = static_cast<US>(s[i]);
}


// Function str_memcpy_ascii_tolower copies and converts ASCII to lowercase
template<typename S,typename O,bool do_cast=false>
constexpr void
str_memcpy_ascii_tolower(
    O * CIG_RESTRICT o,         // [o] output code points
    S const * CIG_RESTRICT s,   // [i] input code points
    std::size_t n               // [i] number of code points
    )
{
    typedef typename std::make_unsigned<S>::type US;
    
    if constexpr (do_cast)
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = static_cast<O>(u >= 65 && u < 91 ? (u | 0x20) : u);
        }
    }
    else
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = (u >= 65 && u < 91 ? (u | 0x20) : u);
        }
    }
}


// Function str_memcpy_ascii_toupper copies and converts ASCII to uppercase
template<typename S,typename O,bool do_cast=false>
constexpr void
str_memcpy_ascii_toupper(
    O * CIG_RESTRICT o,         // [o] output code points
    S const * CIG_RESTRICT s,   // [i] input code points
    std::size_t n               // [i] number of code points
    )
{
    typedef typename std::make_unsigned<S>::type US;
    constexpr auto mask = static_cast<US>(~static_cast<US>(0x20));

    if constexpr (do_cast)
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = static_cast<O>(u >= 97 && u < 123 ? (u & mask) : u);
        }
    }
    else
    {
        for (std::size_t i = 0; i < n; i++)
        {
            auto u = static_cast<US>(s[i]);
            o[i] = (u >= 97 && u < 123 ? (u & mask) : u);
        }
    }
}


// Function str_memcmp compares strings with potentially different base types
template<typename S1,typename S2>
constexpr std::strong_ordering
str_memcmp(
    S1 const * CIG_RESTRICT s1, // [i] input code points of first string
    S2 const * CIG_RESTRICT s2, // [i] input code points of second string
    std::size_t n1,             // [i] number of code points in first string
    std::size_t n2              // [i] number of code points in second string
    )
{
    typedef typename std::make_unsigned<S1>::type US1;
    typedef typename std::make_unsigned<S2>::type US2;
    std::size_t n = (n1 < n2 ? n1 : n2);
    std::size_t i;

    for (i = 0; i < n; i++)
        if (static_cast<US1>(s1[i]) != static_cast<US2>(s2[i]))
            break;
    if (i < n)
    {
        US1 c1 = static_cast<US1>(s1[i]);
        US2 c2 = static_cast<US2>(s2[i]);
        return
            (c1 < c2 ? std::strong_ordering::less :
             c1 > c2 ? std::strong_ordering::greater :
             std::strong_ordering::equal);
    }
    return
        (n1 < n2 ? std::strong_ordering::less :
         n1 > n2 ? std::strong_ordering::greater :
         std::strong_ordering::equal);
}


// Function str_strlen computes a length of a string up to a zero code point
template<typename S>
constexpr std::size_t
str_strlen(
    S const * CIG_RESTRICT s    // [i] input code points of string
    )
{
    std::size_t i;

    for (i = 0; s[i] != 0; i++)
        ;
    return i;
}


// Function str_zero fills a string with zero-valued code points
template<typename O>
constexpr void
str_zero(
    O * CIG_RESTRICT o,         // [o] output code points
    std::size_t n               // [i] number of code points
    )
{
    std::size_t i;

    for (i = 0; i < n; i++)
        o[i] = 0;
}


} // namespace detail


//
// str
//


// Allocation functions (central place of allocation for str)
inline detail::strval::ucs1_t *
str::_allocate_ucs1(std::size_t capacity) {
    return baseallocator<"ucs1_t">::alloc<ucs1_t>(capacity);
}
inline detail::strval::ucs2_t *
str::_allocate_ucs2(std::size_t capacity) {
    return baseallocator<"ucs2_t">::alloc<ucs2_t>(capacity);
}
inline detail::strval::ucs4_t *
str::_allocate_ucs4(std::size_t capacity) {
    return baseallocator<"ucs4_t">::alloc<ucs4_t>(capacity);
}
inline void str::_delete_ucs1(ucs1_t *ptr,[[maybe_unused]] std::size_t capacity)
{
    baseallocator<"ucs1_t">::free<ucs1_t>(ptr,capacity);
}
inline void str::_delete_ucs2(ucs2_t *ptr,[[maybe_unused]] std::size_t capacity)
{
    baseallocator<"ucs2_t">::free<ucs2_t>(ptr,capacity);
}
inline void str::_delete_ucs4(ucs4_t *ptr,[[maybe_unused]] std::size_t capacity)
{
    baseallocator<"ucs4_t">::free<ucs4_t>(ptr,capacity);
}

// Function _delete_by_cap deletes internal pointer by externally provided cap
constexpr void str::_delete_by_cap(std::size_t cap)
{
    // This function leaves 'uninitialized memory'

    // Nothing to delete
    if (!_is_allocated_by_cap(cap))
        return;

    // The allocated size is capacity+1
    std::size_t oldcapacity = _capacity_by_cap(cap); // with '\0'

    // Determine the chartype
    _chartype chartype = _chartype_by_cap(cap);

    if (chartype == _chartype_ucs4)
    {
        ucs4_t *ucs4 = std::bit_cast<ucs4_t*,std::size_t>(_l._data);
        _delete_ucs4(ucs4,oldcapacity);
    }
    else if (chartype == _chartype_ucs2)
    {
        ucs2_t *ucs2 = std::bit_cast<ucs2_t*,std::size_t>(_l._data);
        _delete_ucs2(ucs2,oldcapacity);
    }
    else
    {
        ucs1_t *ucs1 = std::bit_cast<ucs1_t*,std::size_t>(_l._data);
        _delete_ucs1(ucs1,oldcapacity);
    }
}

// Function _destruct is the named destructor
constexpr void str::_destruct()
{
    // This function leaves 'uninitialized memory'
    _delete_by_cap(_l._cap);
}

// Official destructor
constexpr str::~str() {
    _destruct();
}

// Function _construct_clear is a named constructor
constexpr void str::_construct_clear(_chartype chartype)
{
    strval::_construct_clear(chartype);
}

// Function _construct_by_move is a named move constructor
constexpr void str::_construct_by_move(strval &&s) noexcept
{
    strval::_construct_by_move(std::move(s));
}

// Function _reserve ensures place for capacity and chartype
constexpr void str::_reserve(std::size_t newsize,_chartype chartype,bool duplicate)
{
    std::size_t oldsize = _get_size();
    if (oldsize == 0)
    {
        _destruct();
        _construct_clear(chartype);
    }

    std::size_t const oldcap = _l._cap;
    _chartype oldchartype = _chartype_by_cap(oldcap);
    std::size_t oldcapacity = _capacity_by_cap(oldcap); // with '\0'

    // Note that for external literal arrays, oldcapacity is 0

    // Nothing to do?
    if (newsize + 1 <= oldcapacity && chartype <= oldchartype)
        return;

    // Does it fit in the structure?
    bool old_is_pointered = _is_pointered_by_cap(oldcap);
    if (chartype > oldchartype &&
        newsize <= _intcapacity_by_chartype(chartype) /* net */ &&
        !old_is_pointered)
    {
        if (chartype == _chartype_ucs1)
        {
            // Upgrading from ascii, set the bits but keep other bits
            static_assert(_chartype_ascii == 0);
            _l._cap = oldcap | static_cast<std::size_t>(_chartype_ucs1);
            return;
        }
        else if (chartype == _chartype_ucs2)
        {
            // Upgrading from ascii or ucs1
            _short_u16 dst { };
            _short_u8 src = std::bit_cast<_short_u8,_long>(_l);
            detail::str_memcpy(dst.c16 + _c16_offset,src.c8 + _c8_offset,
                               oldsize);
            dst.c16[_c16_length_byte] = _chartype_ucs2;
            _l = std::bit_cast<_long,_short_u16>(dst);
            return;
        }
        // chartype == _chartype_ucs4
        else if (oldchartype == _chartype_ucs2)
        {
            // Upgrading from ucs2 to ucs4
            _short_u32 dst { };
            _short_u16 src = std::bit_cast<_short_u16,_long>(_l);
            detail::str_memcpy(dst.c32 + _c32_offset,src.c16 + _c16_offset,
                               oldsize);
            dst.c32[_c32_length_byte] = _chartype_ucs4;
            _l = std::bit_cast<_long,_short_u32>(dst);
            return;
        }
        else
        {
            // Upgrading from ascii or ucs1 to ucs4
            _short_u32 dst { };
            _short_u8 src = std::bit_cast<_short_u8,_long>(_l);
            detail::str_memcpy(dst.c32 + _c32_offset,src.c8 + _c8_offset,
                               oldsize);
            dst.c32[_c32_length_byte] = _chartype_ucs4;
            _l = std::bit_cast<_long,_short_u32>(dst);
            return;
        }
    }

    // Do not downgrade chartype
    if (chartype < oldchartype)
        chartype = oldchartype;


    // We need to reallocate
    
    if (newsize + 1 < oldcapacity)
        newsize = oldcapacity - 1;

    std::size_t newcap = _size_to_allocate(newsize,duplicate);
    if (chartype == _chartype_ucs4)
    {
        ucs4_t *ucs4 = _allocate_ucs4(newcap);
        if (oldsize)
        {
            if (oldchartype == _chartype_ucs4)
                detail::str_memcpy<ucs4_t,ucs4_t>(ucs4,_data_ucs4(),oldsize);
            else if (oldchartype == _chartype_ucs2)
                detail::str_memcpy<ucs2_t,ucs4_t>(ucs4,_data_ucs2(),oldsize);
            else
                detail::str_memcpy<ucs1_t,ucs4_t>(ucs4,_data_ucs1(),oldsize);
        }
        ucs4[oldsize] = '\0';
        _delete_by_cap(oldcap);
        _l._data = std::bit_cast<size_t,ucs4_t*>(ucs4);
        _l._size = oldsize;
        _l._cap = _cap_by_components(newcap,chartype,true);
    }
    else if (chartype == _chartype_ucs2)
    {
        ucs2_t *ucs2 = _allocate_ucs2(newcap);
        if (oldsize)
        {
            if (oldchartype == _chartype_ucs2)
                detail::str_memcpy<ucs2_t,ucs2_t>(ucs2,_data_ucs2(),oldsize);
            else
                detail::str_memcpy<ucs1_t,ucs2_t>(ucs2,_data_ucs1(),oldsize);
        }
        ucs2[oldsize] = '\0';
        _delete_by_cap(oldcap);
        _l._data = std::bit_cast<size_t,ucs2_t*>(ucs2);
        _l._size = oldsize;
        _l._cap = _cap_by_components(newcap,chartype,true);
    }
    else // ascii or ucs1
    {
        ucs1_t *ucs1 = _allocate_ucs1(newcap);
        if (oldsize)
            detail::str_memcpy<ucs1_t,ucs1_t>(ucs1,_data_ucs1(),oldsize);
        ucs1[oldsize] = '\0';
        _delete_by_cap(oldcap);
        _l._data = std::bit_cast<size_t,ucs1_t*>(ucs1);
        _l._size = oldsize;
        _l._cap = _cap_by_components(newcap,chartype,true);
    }
}

constexpr void str::_set_size(std::size_t sz,bool zero_initialize)
{
    std::size_t cap = _l._cap;
    std::size_t oldcapacity = _capacity_by_cap(cap);

    // Set the size shorter or longer than the current size
    // If size is increased, this leaves memory uninitialized

    // Do not operate on outside arrays of literal strings
    if (oldcapacity == 0)
        return;

    // Do not exceed the reserved capacity
    if (sz >= oldcapacity)
        sz = oldcapacity - 1;

    switch (cap & _capbits_mask)
    {
    case _chartype_ascii: {
        ucs1_t *c8 = reinterpret_cast<_short_u8*>(&_l)->c8;
        std::size_t oldsz = c8[_c8_length_byte] >> 3;
        c8[_c8_length_byte] = static_cast<ucs1_t>((sz << 3) | _chartype_ascii);
        if (zero_initialize && oldsz < sz)
            detail::str_zero(&c8[oldsz + _c8_offset],sz-oldsz);
        c8[sz + _c8_offset] = '\0';
        break; }
    case _chartype_ucs1: {
        ucs1_t *c8 = reinterpret_cast<_short_u8*>(&_l)->c8;
        std::size_t oldsz = c8[_c8_length_byte] >> 3;
        c8[_c8_length_byte] = static_cast<ucs1_t>((sz << 3) | _chartype_ucs1);
        if (zero_initialize && oldsz < sz)
            detail::str_zero(&c8[oldsz + _c8_offset],sz-oldsz);
        c8[sz + _c8_offset] = '\0';
        break; }
    case _chartype_ucs2: {
        ucs2_t *c16 = reinterpret_cast<_short_u16*>(&_l)->c16;
        std::size_t oldsz = c16[_c16_length_byte] >> 3;
        c16[_c16_length_byte] = static_cast<ucs2_t>((sz << 3) | _chartype_ucs2);
        if (zero_initialize && oldsz < sz)
            detail::str_zero(&c16[oldsz + _c16_offset],sz-oldsz);
        c16[sz + _c16_offset] = '\0';
        break; }
    case _chartype_ucs4: {
        ucs4_t *c32 = reinterpret_cast<_short_u32*>(&_l)->c32;
        std::size_t oldsz = c32[_c32_length_byte] >> 3;
        c32[_c32_length_byte] = static_cast<ucs4_t>((sz << 3) | _chartype_ucs4);
        if (zero_initialize && oldsz < sz)
            detail::str_zero(&c32[oldsz + _c32_offset],sz-oldsz);
        c32[sz + _c32_offset] = '\0';
        break; }
    case static_cast<std::size_t>(_long_mask) | _chartype_ascii:
    case static_cast<std::size_t>(_long_mask) | _chartype_ucs1: {
        std::size_t oldsz = _l._size;
        _l._size = sz;
        if (zero_initialize && oldsz < sz)
            detail::str_zero(std::bit_cast<ucs1_t*,std::size_t>(_l._data)+oldsz,
                             sz-oldsz);
        std::bit_cast<ucs1_t*,std::size_t>(_l._data)[sz] = '\0';
        break; }
    case static_cast<std::size_t>(_long_mask) | _chartype_ucs2: {
        std::size_t oldsz = _l._size;
        _l._size = sz;
        if (zero_initialize && oldsz < sz)
            detail::str_zero(std::bit_cast<ucs2_t*,std::size_t>(_l._data)+oldsz,
                             sz-oldsz);
        std::bit_cast<ucs2_t*,std::size_t>(_l._data)[sz] = '\0';
        break; }
    case static_cast<std::size_t>(_long_mask) | _chartype_ucs4: {
        std::size_t oldsz = _l._size;
        _l._size = sz;
        if (zero_initialize && oldsz < sz)
            detail::str_zero(std::bit_cast<ucs4_t*,std::size_t>(_l._data)+oldsz,
                             sz-oldsz);
        std::bit_cast<ucs4_t*,std::size_t>(_l._data)[sz] = '\0';
        break; }
    }
}

template<typename Char>
constexpr void str::_construct_from_ascii(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs1_t));

    // This function is to be called from constructors only
    if (size > _c8_len_net)
    {
        std::size_t newcap = _size_to_allocate(size);
        ucs1_t *ucs1 = _allocate_ucs1(newcap);
        detail::str_memcpy(ucs1,data,size);
        ucs1[size] = '\0';
        _construct_clear();
        _l._data = std::bit_cast<std::size_t,ucs1_t*>(ucs1);
        _l._size = size;
        _l._cap = _cap_by_components(newcap,_chartype_ascii,true);
    }
    else
    {
        _short_u8 dst { };
        dst.c8[_c8_length_byte] =
            static_cast<ucs1_t>((size << 3) | _chartype_ascii);
        if (size)
            detail::str_memcpy(dst.c8 + _c8_offset,data,size);
        _l = std::bit_cast<_long,_short_u8>(dst);
    }
}


template<typename Char>
constexpr void str::_construct_from_ucs1(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs1_t));

    // This function is to be called from constructors only
    if (size > _c8_len_net)
    {
        std::size_t newcap = _size_to_allocate(size);
        ucs1_t *ucs1 = _allocate_ucs1(newcap);
        detail::str_memcpy(ucs1,data,size);
        ucs1[size] = '\0';
        _construct_clear();
        _l._data = std::bit_cast<std::size_t,ucs1_t*>(ucs1);
        _l._size = size;
        _l._cap = _cap_by_components(newcap,_chartype_ucs1,true);
    }
    else
    {
        _short_u8 dst { };
        dst.c8[_c8_length_byte] =
            static_cast<ucs1_t>((size << 3) | _chartype_ascii);
        if (size)
            detail::str_memcpy(dst.c8 + _c8_offset,data,size);
        _l = std::bit_cast<_long,_short_u8>(dst);
    }
}

template<typename Char>
constexpr void str::_construct_from_ucs2(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs2_t));

    // This function is to be called from constructors only
    if (size > _c16_len_net)
    {
        std::size_t newcap = _size_to_allocate(size);
        ucs2_t *ucs2 = _allocate_ucs2(newcap);
        detail::str_memcpy(ucs2,data,size);
        ucs2[size] = '\0';
        _construct_clear();
        _l._data = std::bit_cast<std::size_t,ucs2_t*>(ucs2);
        _l._size = size;
        _l._cap = _cap_by_components(newcap,_chartype_ucs2,true);
    }
    else
    {
        _short_u16 dst { };
        dst.c16[_c16_length_byte] =
            static_cast<ucs2_t>((size << 3) | _chartype_ascii);
        if (size)
            detail::str_memcpy(dst.c16 + _c16_offset,data,size);
        _l = std::bit_cast<_long,_short_u16>(dst);
    }
}

template<typename Char>
constexpr void str::_construct_from_ucs4(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs4_t));

    // This function is to be called from constructors only
    if (size > _c32_len_net)
    {
        std::size_t newcap = _size_to_allocate(size);
        ucs4_t *ucs4 = _allocate_ucs4(newcap);
        detail::str_memcpy(ucs4,data,size);
        ucs4[size] = '\0';
        _construct_clear();
        _l._data = std::bit_cast<std::size_t,ucs4_t*>(ucs4);
        _l._size = size;
        _l._cap = _cap_by_components(newcap,_chartype_ucs4,true);
    }
    else
    {
        _short_u32 dst { };
        dst.c32[_c32_length_byte] =
            static_cast<ucs4_t>((size << 3) | _chartype_ascii);
        if (size)
            detail::str_memcpy(dst.c32 + _c32_offset,data,size);
        _l = std::bit_cast<_long,_short_u32>(dst);
    }
}

constexpr void str::_construct_a_copy(const strval &s)
{
    if (!s._is_allocated())
        // This function will copy the unallocated pointer of a literal
        this->_l = s._l;
    else if (s._is_ucs4())
        _construct_from_ucs4(s._data_ucs4(),s._l._size);
    else if (s._is_ucs2())
        _construct_from_ucs2(s._data_ucs2(),s._l._size);
    else if (s._is_ascii())
        _construct_from_ascii(s._data_ucs1(),s._l._size);
    else
        _construct_from_ucs1(s._data_ucs1(),s._l._size);
}

constexpr void str::_construct_by_move(str &&s) noexcept
{
    _l = s._l;
    s._construct_clear();
}

// Copy constructor
constexpr str::str(const strval &s) {
    _construct_a_copy(s);
}
constexpr str::str(const str &s) : strval() {
    _construct_a_copy(s);
}

// Copy assignment operator
constexpr str & str::operator = (const strval &s) {
    if (this != &s)
    {
        _destruct();
        _construct_a_copy(s);
    }
    return *this;
}
constexpr str & str::operator = (const str &s) {
    if (this != &s)
    {
        _destruct();
        _construct_a_copy(s);
    }
    return *this;
}

// Move constructor
constexpr str::str(strval &&s) noexcept {
    strval::_construct_by_move(std::move(s));
}

constexpr str::str(str &&s) noexcept {
    strval::_construct_by_move(std::move(s));
}

// Move assignment operator
constexpr str & str::operator = (str &&s) {
    if (this != &s)
    {
        _destruct();
        _construct_by_move(std::move(s));
    }
    return *this;
}


// Auxiliary constructors
template<typename Char>
constexpr str str::from_ascii(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs1_t));

    str s;
    s._construct_from_ascii(data,size);
    return s;
}

template<typename Char>
constexpr str str::from_ascii_c_str(Char const *data)
{
    static_assert(sizeof(Char) == sizeof(ucs1_t));
    std::size_t size = detail::str_strlen(data);
    return from_ascii(data,size);
}

template<typename Char>
constexpr str str::from_ucs1(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs1_t));

    str s;
    s._construct_from_ucs1(data,size);
    return s;
}

template<typename Char>
constexpr str str::from_ucs2(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs2_t));

    str s;
    s._construct_from_ucs2(data,size);
    return s;
}

template<typename Char>
constexpr str str::from_ucs4(Char const *data, std::size_t size)
{
    static_assert(sizeof(Char) == sizeof(ucs4_t));

    str s;
    s._construct_from_ucs4(data,size);
    return s;
}


constexpr void str::reserve(i64 newsize,cig_bool duplicate,_chartype newct)
{
    if (newsize._v < 0)         // Nothing to do
        return;
    _chartype ct = _get_chartype();
    if (ct < newct)
        ct = newct;
    _reserve(static_cast<std::size_t>(newsize._v),ct,duplicate._v);
}


// if zero_initialize is false and size increases,
// caller must resize ... fill ... set_size even on error
constexpr void str::set_size(i64 newsize,cig_bool zero_initialize)
{
    std::size_t sz = _get_size();
    std::size_t newsz =
        newsize._v < 0 ? 0 : static_cast<std::size_t>(newsize._v);
    if (newsz > _get_capacity())
    {
        // resize if filling with zeros is allowed
        if (zero_initialize._v)
        {
            reserve(newsize,true); // This is a duplicate situation
        }
        // otherwise enforce the current size with a terminating 0 char
        // since it is highly likely the caller made an error
        else
        {
            _set_size(sz);
            return;
        }
    }
    // Now set the size
    _set_size(newsz,zero_initialize);
}


constexpr
str                             // only ASCII characters converted to lowercase
detail::strval::ascii_tolower() const
{
    std::size_t sz = _get_size();
    if (_is_ucs4())
    {
        if (sz <= _c32_len_net) // short
        {
            ucs4_t a[_c32_len_net] = { };
            str_memcpy_ascii_tolower(a,_data_ucs4(),sz);
            return str::from_ucs4(a,sz);
        }
        else
        {
            str a;
            a._reserve(sz,_chartype_ucs4);
            str_memcpy_ascii_tolower(a._data_ucs4(),_data_ucs4(),sz);
            a._set_size(sz);
            return a;
        }
    }
    else if (_is_ucs2())
    {
        if (sz <= _c16_len_net) // short
        {
            ucs2_t a[_c16_len_net] = { };
            str_memcpy_ascii_tolower(a,_data_ucs2(),sz);
            return str::from_ucs2(a,sz);
        }
        else
        {
            str a;
            a._reserve(sz,_chartype_ucs2);
            str_memcpy_ascii_tolower(a._data_ucs2(),_data_ucs2(),sz);
            a._set_size(sz);
            return a;
        }
    }
    else
    {
        if (sz <= _c8_len_net)  // short
        {
            ucs1_t a[_c8_len_net] = { };
            str_memcpy_ascii_tolower(a,_data_ucs1(),sz);
            return str::from_ucs1(a,sz);
        }
        else
        {
            str a;
            a._reserve(sz,_chartype_ucs1);
            str_memcpy_ascii_tolower(a._data_ucs1(),_data_ucs1(),sz);
            a._set_size(sz);
            return a;
        }
    }
}


constexpr
str                             // only ASCII characters converted to uppercase
detail::strval::ascii_toupper() const
{
    std::size_t sz = _get_size();
    if (_is_ucs4())
    {
        if (sz <= _c32_len_net) // short
        {
            ucs4_t a[_c32_len_net] = { };
            str_memcpy_ascii_toupper(a,_data_ucs4(),sz);
            return str::from_ucs4(a,sz);
        }
        else
        {
            str a;
            a._reserve(sz,_chartype_ucs4);
            str_memcpy_ascii_toupper(a._data_ucs4(),_data_ucs4(),sz);
            a._set_size(sz);
            return a;
        }
    }
    else if (_is_ucs2())
    {
        if (sz <= _c16_len_net) // short
        {
            ucs2_t a[_c16_len_net] = { };
            str_memcpy_ascii_toupper(a,_data_ucs2(),sz);
            return str::from_ucs2(a,sz);
        }
        else
        {
            str a;
            a._reserve(sz,_chartype_ucs2);
            str_memcpy_ascii_toupper(a._data_ucs2(),_data_ucs2(),sz);
            a._set_size(sz);
            return a;
        }
    }
    else
    {
        if (sz <= _c8_len_net)  // short
        {
            ucs1_t a[_c8_len_net] = { };
            str_memcpy_ascii_toupper(a,_data_ucs1(),sz);
            return str::from_ucs1(a,sz);
        }
        else
        {
            str a;
            a._reserve(sz,_chartype_ucs1);
            str_memcpy_ascii_toupper(a._data_ucs1(),_data_ucs1(),sz);
            a._set_size(sz);
            return a;
        }
    }
}


// Reenable the -Wstrict-aliasing warning
#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif


/**********************************************************************/
/*                                                                    */
/*                         STRING EXPRESSIONS                         */
/*                                                                    */
/**********************************************************************/


namespace detail {


// String expression class for addition and multiplication operator
struct strexpr {
    enum operation {
        plus_str_str,
        plus_str_expr,
        plus_expr_str,
        plus_expr_expr,
        plus_str_other,
        plus_expr_other,
        plus_other_str,
        plus_other_expr,
        mult_str,
        mult_expr,
        slice_str,
        slice_expr
    };

    union { strval const *s; strexpr const *e; } _ex1, _ex2;
    operation _op;              // Operation between left and right subtree
    strval::_chartype chartype; // Max chartype of leaf nodes in strexpr tree
    bool chartype_by_slice;     // Whether chartype may have to be modified
                                // due to slices in strexpr tree
    std::size_t size;           // Size of strexpr tree result string
    std::variant<std::monostate,str,uint64_t,slicespec> aux;

    static constexpr bool op_needs_saved_op1(operation op);
    static constexpr bool op_needs_saved_op2(operation op);
    static constexpr bool op_is_slice(operation op);

    // Copy constructor omitted on purpose
    strexpr(strexpr &&) noexcept;
    strexpr(operation op,strval const &s1,strval const &s2);
    strexpr(operation op,strval const &s1,strexpr const &s2);
    strexpr(operation op,strexpr const &s1,strval const &s2);
    strexpr(operation op,strexpr const &s1,strexpr const &s2);
    strexpr(operation op,strval const &s,i64 m);
    strexpr(operation op,strexpr const &s,i64 m);
    strexpr(operation op,strval const &s,slicespec spec);
    strexpr(operation op,strexpr const &s,slicespec spec);
    ~strexpr();

    strexpr slice(std::optional<i64> start, // Python like semantics
                  std::optional<i64> end = std::nullopt,
                  std::optional<i64> step = std::nullopt) const noexcept;
    
private:

    //
    // Auxiliary functions for leaf nodes
    //

    // Aux: Full copy a strval leaf node (not this tree) onto o
    template<typename O>
    static constexpr std::size_t // size copied
    strval_evaluate_onto(strval const &s,O *o) noexcept;

    // Aux: Slice a strval leaf node (not this tree) onto o
    template<typename O>
    static constexpr std::size_t // size filled
    strval_slice_onto(strval const &s,O *o,slicespec spec,
                      strval::_chartype *ct) noexcept;

    // Aux: Determine min/max of slice of strval leaf node (not this tree)
    static constexpr i64 // minimum code point; -1 if strval is empty
    strval_slice_min(strval const &s,slicespec spec) noexcept;
    static constexpr i64 // maximum code point; -1 if strval is empty
    strval_slice_max(strval const &s,slicespec spec) noexcept;
    static constexpr std::pair<i64,i64> // min and max code pt.; -1,-1 if empty
    strval_slice_minmax(strval const &s,slicespec spec) noexcept;

    //
    // Recursive operations on strexpr trees
    //

    // Copy a subslice of the strexpr tree onto o
    template<typename O>
    constexpr std::size_t       // size filled
    slice_onto(O *o,slicespec spec) const noexcept;

    // Get the chartype of a subslice of the strexpr tree
    constexpr strval::_chartype // chartype for maximum code point
    slice_onto_chartype(slicespec spec) const noexcept;

    // Determine min/max of a subslice of the strexpr tree
    constexpr i64 // minimum code point; -1 if strval is empty
    slice_min(slicespec spec) const noexcept;
    constexpr i64 // maximum code point; -1 if strval is empty
    slice_max(slicespec spec) const noexcept;
    constexpr std::pair<i64,i64> // min and max code point; -1,-1 if empty
    slice_minmax(slicespec spec) const noexcept;

    //
    // Top level node operations
    //

public:

    // Get the chartype of the strexpr tree
    constexpr strval::_chartype get_chartype() const noexcept;

    // Copy the strexpr tree onto o
    template<typename O>
    constexpr std::size_t       // size written
    evaluate_onto(O *o) const noexcept;

    // Determine minimum and maximum code point; return -1 for empty strexpr
    constexpr i64 min() const noexcept; // -1 if empty
    constexpr i64 max() const noexcept; // -1 if empty
    constexpr std::pair<i64,i64> minmax() const noexcept; // both

    friend struct cig::str;
};


// Static aux: Fully copy a strval leaf node onto the output o
template <typename O>
constexpr std::size_t           // size copied
strexpr::strval_evaluate_onto(strval const &s,O * o) noexcept
{
    std::size_t sz = s._get_size();
    if (sz)
    {
        if (s._is_ucs4())
            str_memcpy<strval::ucs4_t,O,true>(o,s._data_ucs4(),sz);
        else if (s._is_ucs2())
            str_memcpy<strval::ucs2_t,O,true>(o,s._data_ucs2(),sz);
        else
            str_memcpy(o,s._data_ucs1(),sz);
    }
    return sz;
}


// Static aux: Slice a strval leaf node onto the output o
template <typename O>
constexpr std::size_t           // size filled
strexpr::strval_slice_onto(strval const &s,O * CIG_RESTRICT o,
                           slicespec spec,strval::_chartype *ct) noexcept
{
    std::size_t j = 0;
    strval::ucs4_t vmax = 0;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return 0;               // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        if (o)
        {
            if (s._is_ucs4())
            {
                strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
                for (std::size_t pos = spec.start; pos < endpos; pos++)
                {
                    strval::ucs4_t v = data[pos];
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
            else if (s._is_ucs2())
            {
                strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
                for (std::size_t pos = spec.start; pos < endpos; pos++)
                {
                    strval::ucs2_t v = data[pos];
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
            else
            {
                strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
                for (std::size_t pos = spec.start; pos < endpos; pos++)
                {
                    strval::ucs1_t v = data[pos];
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
        }
        else // no o
        {
            if (s._is_ucs4())
            {
                strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
                for (std::size_t pos = spec.start; pos < endpos; pos++)
                {
                    strval::ucs4_t v = data[pos];
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
            else if (s._is_ucs2())
            {
                strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
                for (std::size_t pos = spec.start; pos < endpos; pos++)
                {
                    strval::ucs2_t v = data[pos];
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
            else
            {
                strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
                for (std::size_t pos = spec.start; pos < endpos; pos++)
                {
                    strval::ucs1_t v = data[pos];
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        if (o)
        {
            if (s._is_ucs4())
            {
                strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs4_t v = data[pos];
                    pos += ustep;
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
            else if (s._is_ucs2())
            {
                strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs2_t v = data[pos];
                    pos += ustep;
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
            else
            {
                strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs1_t v = data[pos];
                    pos += ustep;
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
        }
        else // no o
        {
            if (s._is_ucs4())
            {
                strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs4_t v = data[pos];
                    pos += ustep;
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
            else if (s._is_ucs2())
            {
                strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs2_t v = data[pos];
                    pos += ustep;
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
            else
            {
                strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs1_t v = data[pos];
                    pos += ustep;
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        if (o)
        {
            if (s._is_ucs4())
            {
                strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs4_t v = data[pos];
                    pos -= ustep;
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
            else if (s._is_ucs2())
            {
                strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs2_t v = data[pos];
                    pos -= ustep;
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
            else
            {
                strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs1_t v = data[pos];
                    pos -= ustep;
                    if (vmax < v)
                        vmax = v;
                    o[j++] = static_cast<O>(v);
                }
            }
        }
        else // no o
        {
            if (s._is_ucs4())
            {
                strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs4_t v = data[pos];
                    pos -= ustep;
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
            else if (s._is_ucs2())
            {
                strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs2_t v = data[pos];
                    pos -= ustep;
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
            else
            {
                strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
                for (std::size_t i = 0; i < spec.count; i++)
                {
                    strval::ucs1_t v = data[pos];
                    pos -= ustep;
                    if (vmax < v)
                        vmax = v;
                    j++;
                }
            }
        }
    }

    // At most all characters may have been placed in o
    cig_assert(j <= s._get_size());

    if (ct)
        *ct = strval::_chartype_by_max_codepoint(vmax);
    return j;
}


// Static aux: Determine min of slice of strval leaf node (not this tree)
constexpr i64                   // minimum code point, -1 if slice is empty
strexpr::strval_slice_min(strval const &s,slicespec spec) noexcept
{
    std::size_t j = 0;
    int64_t vmin = i64::max_value;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return -1;              // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs4_t v = data[pos];
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs2_t v = data[pos];
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs1_t v = data[pos];
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs4_t v = data[pos];
                pos += ustep;
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs2_t v = data[pos];
                pos += ustep;
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs1_t v = data[pos];
                pos += ustep;
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs4_t v = data[pos];
                pos -= ustep;
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs2_t v = data[pos];
                pos -= ustep;
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs1_t v = data[pos];
                pos -= ustep;
                if (vmin > v)
                    vmin = v;
                j++;
            }
        }
    }

    // At most all characters may have been examined
    cig_assert(j <= s._get_size());

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return vmin;
}


// Static aux: Determine max of slice of strval leaf node (not this tree)
constexpr i64                   // maximum code point, -1 if slice is empty
strexpr::strval_slice_max(strval const &s,slicespec spec) noexcept
{
    std::size_t j = 0;
    int64_t vmax = -1;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return -1;              // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs4_t v = data[pos];
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs2_t v = data[pos];
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs1_t v = data[pos];
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs4_t v = data[pos];
                pos += ustep;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs2_t v = data[pos];
                pos += ustep;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs1_t v = data[pos];
                pos += ustep;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs4_t v = data[pos];
                pos -= ustep;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs2_t v = data[pos];
                pos -= ustep;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs1_t v = data[pos];
                pos -= ustep;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
    }

    // At most all characters may have been examined
    cig_assert(j <= s._get_size());

    return vmax;
}


// Static aux: Determine min/max of slice of strval leaf node (not this tree)
constexpr
std::pair<i64,i64>              // min,max; -1,-1 if slice is empty
strexpr::strval_slice_minmax(strval const &s,slicespec spec) noexcept
{
    std::size_t j = 0;
    int64_t vmin = i64::max_value;
    int64_t vmax = -1;

    // the caller guarantees that spec.step is not 0
    if (spec.step == 0)
        return { -1, -1 };      // this is as good as any other error handling

    else if (spec.step == 1)    // special case, very likely
    {
        std::size_t endpos = spec.start + spec.count;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs4_t v = data[pos];
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs2_t v = data[pos];
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t pos = spec.start; pos < endpos; pos++)
            {
                strval::ucs1_t v = data[pos];
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
    }
    else if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step);
        std::size_t pos = spec.start;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs4_t v = data[pos];
                pos += ustep;
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs2_t v = data[pos];
                pos += ustep;
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs1_t v = data[pos];
                pos += ustep;
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
    }
    else // spec.step < 0
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step);
        std::size_t pos = spec.start;
        if (s._is_ucs4())
        {
            strval::ucs4_t const * CIG_RESTRICT data = s._data_ucs4();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs4_t v = data[pos];
                pos -= ustep;
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else if (s._is_ucs2())
        {
            strval::ucs2_t const * CIG_RESTRICT data = s._data_ucs2();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs2_t v = data[pos];
                pos -= ustep;
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
        else
        {
            strval::ucs1_t const * CIG_RESTRICT data = s._data_ucs1();
            for (std::size_t i = 0; i < spec.count; i++)
            {
                strval::ucs1_t v = data[pos];
                pos -= ustep;
                if (vmin > v)
                    vmin = v;
                if (vmax < v)
                    vmax = v;
                j++;
            }
        }
    }

    // At most all characters may have been examined
    cig_assert(j <= s._get_size());

    // We use -1 as exception value
    if (vmin == i64::max_value)
        vmin = -1;

    return { vmin, vmax };
}


// Copy a subslice of the strexpr tree onto an output o
template<typename O>
constexpr std::size_t           // size filled
strexpr::slice_onto(O *o,slicespec spec) const noexcept
{
    std::size_t sz1;
    std::size_t sz2;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = strval_slice_onto<O>(*_ex1.s,o,specarr[0],0);
            sz2 = strval_slice_onto<O>(*_ex2.s,o + sz1,specarr[1],0);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = strval_slice_onto<O>(*_ex2.s,o,specarr[1],0);
            sz1 = strval_slice_onto<O>(*_ex1.s,o + sz2,specarr[0],0);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case plus_str_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = strval_slice_onto<O>(*_ex1.s,o,specarr[0],0);
            sz2 = _ex2.e->slice_onto<O>(o + sz1,specarr[1]);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = _ex2.e->slice_onto<O>(o,specarr[1]);
            sz1 = strval_slice_onto<O>(*_ex1.s,o + sz2,specarr[0],0);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case plus_expr_str:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = _ex1.e->slice_onto<O>(o,specarr[0]);
            sz2 = strval_slice_onto<O>(*_ex2.s,o + sz1,specarr[1],0);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = strval_slice_onto<O>(*_ex2.s,o,specarr[1],0);
            sz1 = _ex1.e->slice_onto<O>(o + sz2,specarr[0]);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz1 = _ex1.e->slice_onto<O>(o,specarr[0]);
            sz2 = _ex2.e->slice_onto<O>(o + sz1,specarr[1]);
            cig_assert(sz1 + sz2 <= size);
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            sz2 = _ex2.e->slice_onto<O>(o,specarr[1]);
            sz1 = _ex1.e->slice_onto<O>(o + sz2,specarr[0]);
            cig_assert(sz1 + sz2 <= size);
        }
        return sz1 + sz2;
    case mult_str:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                sz2 = strval_slice_onto<O>(*_ex1.s,o+sz1,partspec,0);
                sz1 += sz2;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                sz2 = strval_slice_onto<O>(*_ex1.s,o+sz1,partspec,0);
                sz1 += sz2;
            }
        }
        cig_assert(sz1 <= size);
        return sz1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                sz2 = _ex1.e->slice_onto<O>(o+sz1,partspec);
                sz1 += sz2;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            sz1 = 0;
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                sz2 = _ex1.e->slice_onto<O>(o+sz1,partspec);
                sz1 += sz2;
            }
        }
        cig_assert(sz1 <= size);
        return sz1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            sz1 = strval_slice_onto<O>(*_ex1.s,o,cumulated,0);
            cig_assert(sz1 <= size);
        }
        return sz1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            sz1 = _ex1.e->slice_onto<O>(o,cumulated);
            cig_assert(sz1 <= size);
        }
        return sz1;
    }
    cig_assert(false);          // all cases covered
    return 0;
}


// Get the chartype of a subslice of the strexpr tree
constexpr strval::_chartype
strexpr::slice_onto_chartype(slicespec spec) const noexcept
{
    strval::_chartype ct1 = strval::_chartype_ascii;
    strval::_chartype ct2 = strval::_chartype_ascii;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,specarr[0],&ct1);
            strval_slice_onto<strval::ucs4_t>(*_ex2.s,0,specarr[1],&ct2);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            strval_slice_onto<strval::ucs4_t>(*_ex2.s,0,specarr[1],&ct2);
            strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,specarr[0],&ct1);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        return ct1;
    case plus_str_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,specarr[0],&ct1);
            ct2 = _ex2.e->slice_onto_chartype(specarr[1]);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            ct2 = _ex2.e->slice_onto_chartype(specarr[1]);
            strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,specarr[0],&ct1);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        return ct1;
    case plus_expr_str:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            ct1 = _ex1.e->slice_onto_chartype(specarr[0]);
            strval_slice_onto<strval::ucs4_t>(*_ex2.s,0,specarr[1],&ct2);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            strval_slice_onto<strval::ucs4_t>(*_ex2.s,0,specarr[1],&ct2);
            ct1 = _ex1.e->slice_onto_chartype(specarr[0]);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        return ct1;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            ct1 = _ex1.e->slice_onto_chartype(specarr[0]);
            ct2 = _ex2.e->slice_onto_chartype(specarr[1]);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            ct2 = _ex2.e->slice_onto_chartype(specarr[1]);
            ct1 = _ex1.e->slice_onto_chartype(specarr[0]);
            if (ct1 < ct2)
                ct1 = ct2;
        }
        return ct1;
    case mult_str:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,partspec,&ct2);
                if (ct1 < ct2)
                    ct1 = ct2;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,partspec,&ct2);
                if (ct1 < ct2)
                    ct1 = ct2;
            }
        }
        return ct1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                ct2 = _ex1.e->slice_onto_chartype(partspec);
                if (ct1 < ct2)
                    ct1 = ct2;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                ct2 = _ex1.e->slice_onto_chartype(partspec);
                if (ct1 < ct2)
                    ct1 = ct2;
            }
        }
        return ct1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,cumulated,&ct1);
        }
        return ct1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            ct1 = _ex1.e->slice_onto_chartype(cumulated);
        }
        return ct1;
    }
    return ct1;
}


// Determine min of a subslice of the strexpr tree
constexpr
i64                             // minimum code point; -1 if strval is empty
strexpr::slice_min(slicespec spec) const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = strval_slice_min(*_ex1.s,specarr[0]);
            min2 = strval_slice_min(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = strval_slice_min(*_ex2.s,specarr[1]);
            min1 = strval_slice_min(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case plus_str_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = strval_slice_min(*_ex1.s,specarr[0]);
            min2 = _ex2.e->slice_min(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = _ex2.e->slice_min(specarr[1]);
            min1 = strval_slice_min(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case plus_expr_str:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = _ex1.e->slice_min(specarr[0]);
            min2 = strval_slice_min(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = strval_slice_min(*_ex2.s,specarr[1]);
            min1 = _ex1.e->slice_min(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min1 = _ex1.e->slice_min(specarr[0]);
            min2 = _ex2.e->slice_min(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            min2 = _ex2.e->slice_min(specarr[1]);
            min1 = _ex1.e->slice_min(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
        }
        return min1;
    case mult_str:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                min2 = strval_slice_min(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                min2 = strval_slice_min(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        return min1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                min2 = _ex1.e->slice_min(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                min2 = _ex1.e->slice_min(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
            }
        }
        return min1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            min1 = strval_slice_min(*_ex1.s,cumulated);
        }
        return min1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            min1 = _ex1.e->slice_min(cumulated);
        }
        return min1;
    }
    return min1;
}


// Determine max of a subslice of the strexpr tree
constexpr
i64                             // maximum code point; -1 if strval is empty
strexpr::slice_max(slicespec spec) const noexcept
{
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = strval_slice_max(*_ex1.s,specarr[0]);
            max2 = strval_slice_max(*_ex2.s,specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = strval_slice_max(*_ex2.s,specarr[1]);
            max1 = strval_slice_max(*_ex1.s,specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case plus_str_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = strval_slice_max(*_ex1.s,specarr[0]);
            max2 = _ex2.e->slice_max(specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = _ex2.e->slice_max(specarr[1]);
            max1 = strval_slice_max(*_ex1.s,specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case plus_expr_str:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = _ex1.e->slice_max(specarr[0]);
            max2 = strval_slice_max(*_ex2.s,specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = strval_slice_max(*_ex2.s,specarr[1]);
            max1 = _ex1.e->slice_max(specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max1 = _ex1.e->slice_max(specarr[0]);
            max2 = _ex2.e->slice_max(specarr[1]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            max2 = _ex2.e->slice_max(specarr[1]);
            max1 = _ex1.e->slice_max(specarr[0]);
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return max1;
    case mult_str:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                max2 = strval_slice_max(*_ex1.s,partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                max2 = strval_slice_max(*_ex1.s,partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return max1;
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                max2 = _ex1.e->slice_max(partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                max2 = _ex1.e->slice_max(partspec);
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return max1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            max1 = strval_slice_max(*_ex1.s,cumulated);
        }
        return max1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            max1 = _ex1.e->slice_max(cumulated);
        }
        return max1;
    }
    return max1;
}


// Determine min/max of a subslice of the strexpr tree
constexpr
std::pair<i64,i64>              // min,max; -1,-1 if slice is empty
strexpr::slice_minmax(slicespec spec) const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = strval_slice_minmax(*_ex1.s,specarr[0]);
            std::tie(min2,max2) = strval_slice_minmax(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = strval_slice_minmax(*_ex2.s,specarr[1]);
            std::tie(min1,max1) = strval_slice_minmax(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case plus_str_expr:
    case plus_other_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = strval_slice_minmax(*_ex1.s,specarr[0]);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.s->_get_size();
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            std::tie(min1,max1) = strval_slice_minmax(*_ex1.s,specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case plus_expr_str:
    case plus_expr_other:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            std::tie(min2,max2) = strval_slice_minmax(*_ex2.s,specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = strval_slice_minmax(*_ex2.s,specarr[1]);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case plus_expr_expr:
        if (spec.step >= 0)
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        else
        {
            std::size_t firstsz = _ex1.e->size;
            std::array<slicespec,2> specarr = split_slicespec(spec,firstsz);
            std::tie(min2,max2) = _ex2.e->slice_minmax(specarr[1]);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specarr[0]);
            if (min1._v > min2._v && min2._v != -1)
                min1._v = min2._v;
            if (max1._v < max2._v)
                max1._v = max2._v;
        }
        return { min1, max1 };
    case mult_str:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                std::tie(min2,max2) = strval_slice_minmax(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.s->_get_size();
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                std::tie(min2,max2) = strval_slice_minmax(*_ex1.s,partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return { min1, max1 };
    case mult_expr:
        if (spec.step >= 0)
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,i,partsz);
                std::tie(min2,max2) = _ex1.e->slice_minmax(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        else
        {
            std::size_t partsz = _ex1.e->size;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                slicespec partspec = divide_slicespec(spec,multaux-1-i,partsz);
                std::tie(min2,max2) = _ex1.e->slice_minmax(partspec);
                if (min1._v > min2._v && min2._v != -1)
                    min1._v = min2._v;
                if (max1._v < max2._v)
                    max1._v = max2._v;
            }
        }
        return { min1, max1 };
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            std::tie(min1,max1) = strval_slice_minmax(*_ex1.s,cumulated);
        }
        return { min1, max1 };
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            slicespec cumulated = chain_slicespec(spec,specaux);
            std::tie(min1,max1) = _ex1.e->slice_minmax(cumulated);
        }
        return { min1, max1 };
    }
    return { min1, max1 };
}


// Get the chartype of the strexpr tree
constexpr strval::_chartype
strexpr::get_chartype() const noexcept
{
    if (!chartype_by_slice)
        return chartype;

    strval::_chartype ct1 = strval::_chartype_ascii;
    strval::_chartype ct2 = strval::_chartype_ascii;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        ct1 = _ex1.s->_get_chartype();
        ct2 = _ex2.s->_get_chartype();
        return (ct1 > ct2 ? ct1 : ct2);
    case plus_str_expr:
    case plus_other_expr:
        ct1 = _ex1.s->_get_chartype();
        ct2 = _ex2.e->get_chartype();
        return (ct1 > ct2 ? ct1 : ct2);
    case plus_expr_str:
    case plus_expr_other:
        ct1 = _ex1.e->get_chartype();
        ct2 = _ex2.s->_get_chartype();
        return (ct1 > ct2 ? ct1 : ct2);
    case plus_expr_expr:
        ct1 = _ex1.e->get_chartype();
        ct2 = _ex2.e->get_chartype();
        return (ct1 > ct2 ? ct1 : ct2);
    case mult_str:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return strval::_chartype_ascii;
            ct1 = _ex1.s->_get_chartype();
        }
        return ct1;
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return strval::_chartype_ascii;
            ct1 = _ex1.e->get_chartype();
        }
        return ct1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            strval_slice_onto<strval::ucs4_t>(*_ex1.s,0,specaux,&ct1);
        }
        return ct1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            ct1 = _ex1.e->slice_onto_chartype(specaux);
        }
        return ct1;
    }
    return ct1;
}


// Copy the strexpr tree onto the output o
// On toplevel, call get_chartype() first to determine O
template<typename O>
constexpr std::size_t           // size copied
strexpr::evaluate_onto(O *o) const noexcept
{
    std::size_t sz1;
    std::size_t sz2;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        sz1 = strval_evaluate_onto(*_ex1.s,o);
        sz2 = strval_evaluate_onto(*_ex2.s,o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;
    case plus_str_expr:
    case plus_other_expr:
        sz1 = strval_evaluate_onto(*_ex1.s,o);
        sz2 = _ex2.e->evaluate_onto(o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;
    case plus_expr_str:
    case plus_expr_other:
        sz1 = _ex1.e->evaluate_onto(o);
        sz2 = strval_evaluate_onto(*_ex2.s,o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;
    case plus_expr_expr:
        sz1 = _ex1.e->evaluate_onto(o);
        sz2 = _ex2.e->evaluate_onto(o + sz1);
        cig_assert(sz1 + sz2 == size);
        return sz1 + sz2;

    case mult_str:
        if (true)
        {
            sz1 = 0;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                sz1 += strval_evaluate_onto(*_ex1.s,o + sz1);
            }
        }
        return sz1;
    case mult_expr:
        if (true)
        {
            sz1 = 0;
            uint64_t multaux = std::get<uint64_t>(aux);
            for (std::size_t i = 0; i < multaux; i++)
            {
                sz1 += _ex1.e->evaluate_onto(o + sz1);
            }
        }
        return sz1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            sz1 = strval_slice_onto(*_ex1.s,o,specaux,0);
        }
        return sz1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            sz1 = _ex1.e->slice_onto(o,specaux);
        }
        return sz1;
    }
    return 0;
}


// Determine minimum code point
constexpr 
i64                             // minimum code point; -1 if strexpr is empty
strexpr::min() const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        min1 = _ex1.s->min();
        min2 = _ex2.s->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case plus_str_expr:
    case plus_other_expr:
        min1 = _ex1.s->min();
        min2 = _ex2.e->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case plus_expr_str:
    case plus_expr_other:
        min1 = _ex1.e->min();
        min2 = _ex2.s->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case plus_expr_expr:
        min1 = _ex1.e->min();
        min2 = _ex2.e->min();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        return min1;
    case mult_str:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            min1 = _ex1.s->min();
        }
        return min1;
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            min1 = _ex1.e->min();
        }
        return min1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            min1 = strval_slice_min(*_ex1.s,specaux);
        }
        return min1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            min1 = _ex1.e->slice_min(specaux);
        }
        return min1;
    }
    return min1;
}


// Determine maximum code point
constexpr 
i64                             // maximum code point; -1 if strexpr is empty
strexpr::max() const noexcept
{
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        max1 = _ex1.s->max();
        max2 = _ex2.s->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case plus_str_expr:
    case plus_other_expr:
        max1 = _ex1.s->max();
        max2 = _ex2.e->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case plus_expr_str:
    case plus_expr_other:
        max1 = _ex1.e->max();
        max2 = _ex2.s->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case plus_expr_expr:
        max1 = _ex1.e->max();
        max2 = _ex2.e->max();
        if (max1._v < max2._v)
            max1._v = max2._v;
        return max1;
    case mult_str:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            max1 = _ex1.s->max();
        }
        return max1;
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return -1;
            max1 = _ex1.e->max();
        }
        return max1;
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            max1 = strval_slice_max(*_ex1.s,specaux);
        }
        return max1;
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            max1 = _ex1.e->slice_max(specaux);
        }
        return max1;
    }
    return max1;
}


// Determine minimum and maximum code point
constexpr 
std::pair<i64,i64>              // min,max; -1,-1 if strexpr is empty
strexpr::minmax() const noexcept
{
    i64 min1 = -1;
    i64 min2 = -1;
    i64 max1 = -1;
    i64 max2 = -1;
    switch (_op)
    {
    case plus_str_str:
    case plus_str_other:
    case plus_other_str:
        std::tie(min1,max1) = _ex1.s->minmax();
        std::tie(min2,max2) = _ex2.s->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case plus_str_expr:
    case plus_other_expr:
        std::tie(min1,max1) = _ex1.s->minmax();
        std::tie(min2,max2) = _ex2.e->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case plus_expr_str:
    case plus_expr_other:
        std::tie(min1,max1) = _ex1.e->minmax();
        std::tie(min2,max2) = _ex2.s->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case plus_expr_expr:
        std::tie(min1,max1) = _ex1.e->minmax();
        std::tie(min2,max2) = _ex2.e->minmax();
        if (min1._v > min2._v && min2._v != -1)
            min1._v = min2._v;
        if (max1._v < max2._v)
            max1._v = max2._v;
        return { min1, max1 };
    case mult_str:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return { -1, -1 };
            std::tie(min1,max1) = _ex1.s->minmax();
        }
        return { min1, max1 };
    case mult_expr:
        if (true)
        {
            uint64_t multaux = std::get<uint64_t>(aux);
            if (multaux == 0)
                return { -1, -1 };
            std::tie(min1,max1) = _ex1.e->minmax();
        }
        return { min1, max1 };
    case slice_str:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            std::tie(min1,max1) = strval_slice_minmax(*_ex1.s,specaux);
        }
        return { min1, max1 };
    case slice_expr:
        if (true)
        {
            slicespec specaux = std::get<slicespec>(aux);
            std::tie(min1,max1) = _ex1.e->slice_minmax(specaux);
        }
        return { min1, max1 };
    }
    return { min1, max1 };
}


inline strexpr::~strexpr()
{
}

inline strexpr::strexpr(strexpr &&e) noexcept :
    _ex1(e._ex1), _ex2(e._ex2), _op(e._op), chartype(e.chartype), size(e.size),
    aux(std::move(e.aux))
{
}

constexpr bool strexpr::op_needs_saved_op1(strexpr::operation op)
{
    return (op == plus_other_str ||
            op == plus_other_expr);
}

constexpr bool strexpr::op_needs_saved_op2(strexpr::operation op)
{
    return (op == plus_str_other ||
            op == plus_expr_other);
}

constexpr bool strexpr::op_is_slice(strexpr::operation op)
{
    return (op == slice_str ||
            op == slice_expr);
}

// Constructor from strval,strval
inline strexpr::strexpr(
    strexpr::operation op,strval const &s1,strval const &s2) :
    _op(op)
{
    // Some operators require that one of the strings is stored internally
    if (op_needs_saved_op1(op))
    {
        aux = str(s1);
        _ex1.s = std::get_if<str>(&aux);
        _ex2.s = &s2;
    }
    else if (op_needs_saved_op2(op))
    {
        _ex1.s = &s1;
        aux = str(s2);
        _ex2.s = std::get_if<str>(&aux);
    }
    else
    {
        _ex1.s = &s1;
        _ex2.s = &s2;
    }
    std::size_t sz1 = s1._get_size();
    std::size_t sz2 = s2._get_size();
    size = sz1 + sz2;
    strval::_chartype const ct1 =
        sz1 ? s1._get_chartype() : strval::_chartype_ascii;
    strval::_chartype const ct2 =
        sz2 ? s2._get_chartype() : strval::_chartype_ascii;
    chartype = (ct1 > ct2 ? ct1 : ct2);
    chartype_by_slice =
        (chartype != strval::_chartype_ascii) && op_is_slice(op);
}

// Constructor from strval,strexpr
inline strexpr::strexpr(
    strexpr::operation op,strval const &s1,strexpr const &s2) :
    _op(op)
{
    // Some operators require that one of the strings is stored internally
    if (op_needs_saved_op1(op))
    {
        aux = str(s1);
        _ex1.s = std::get_if<str>(&aux);
        _ex2.e = &s2;
    }
    else
    {
        _ex1.s = &s1;
        _ex2.e = &s2;
    }
    std::size_t sz1 = s1._get_size();
    std::size_t sz2 = s2.size;
    size = sz1 + sz2;
    strval::_chartype const ct1 =
        sz1 ? s1._get_chartype() : strval::_chartype_ascii;
    strval::_chartype const ct2 = s2.chartype;
    chartype = (ct1 > ct2 ? ct1 : ct2);
    chartype_by_slice =
        ((chartype != strval::_chartype_ascii) && op_is_slice(op)) ||
        s2.chartype_by_slice; 
}

// Constructor from strexpr,strval
inline strexpr::strexpr(
    strexpr::operation op,strexpr const &s1,strval const &s2) :
    _op(op)
{
    // Some operators require that one of the strings is stored internally
    if (op_needs_saved_op2(op))
    {
        _ex1.e = &s1;
        aux = str(s2);
        _ex2.s = std::get_if<str>(&aux);
    }
    else
    {
        _ex1.e = &s1;
        _ex2.s = &s2;
    }
    std::size_t sz1 = s1.size;
    std::size_t sz2 = s2._get_size();
    size = sz1 + sz2;
    strval::_chartype const ct1 = s1.chartype;
    strval::_chartype const ct2 =
        sz2 ? s2._get_chartype() : strval::_chartype_ascii;
    chartype = (ct1 > ct2 ? ct1 : ct2);
    chartype_by_slice =
        ((chartype != strval::_chartype_ascii) && op_is_slice(op)) ||
        s1.chartype_by_slice;
}

// Constructor from strexpr,strexpr
inline strexpr::strexpr(
    strexpr::operation op,strexpr const &s1,strexpr const &s2) :
    _op(op)
{
    _ex1.e = &s1;
    _ex2.e = &s2;
    std::size_t sz1 = s1.size;
    std::size_t sz2 = s2.size;
    size = sz1 + sz2;
    strval::_chartype const ct1 = s1.chartype;
    strval::_chartype const ct2 = s2.chartype;
    chartype = (ct1 > ct2 ? ct1 : ct2);
    chartype_by_slice =
        ((chartype != strval::_chartype_ascii) && op_is_slice(op)) ||
        s1.chartype_by_slice ||
        s2.chartype_by_slice;
}

// Constructor from strval,number
inline strexpr::strexpr(
    strexpr::operation op,strval const &s1,i64 m) :
    _op(op)
{
    _ex1.s = &s1;
    _ex2.s = 0;
    uint64_t multaux = static_cast<uint64_t>(m._v > 0 ? m._v : 0);
    aux = multaux;
    size = s1._get_size() * multaux;
    chartype = size ? s1._get_chartype() : strval::_chartype_ascii;
    cig_assert(!op_is_slice(op));
    chartype_by_slice = false;
}

// Constructor from strexpr,number
inline strexpr::strexpr(
    strexpr::operation op,strexpr const &s1,i64 m) :
    _op(op)
{
    _ex1.e = &s1;
    _ex2.s = 0;
    uint64_t multaux = static_cast<uint64_t>(m._v > 0 ? m._v : 0);
    aux = multaux;
    size = s1.size * multaux;
    chartype = size ? s1.chartype : strval::_chartype_ascii;
    cig_assert(!op_is_slice(op));
    chartype_by_slice = s1.chartype_by_slice;
}

// Constructor from strval,slicespec
inline strexpr::strexpr(
    strexpr::operation op,strval const &s1,slicespec spec) :
    _op(op)
{
    _ex1.s = &s1;
    _ex2.s = 0;
    aux = spec;
    size = spec.count;
    chartype = size ? s1._get_chartype() : strval::_chartype_ascii;
    chartype_by_slice = (chartype != strval::_chartype_ascii);
}

// Constructor from strexpr,slicespec
inline strexpr::strexpr(
    strexpr::operation op,strexpr const &s1,slicespec spec) :
    _op(op)
{
    _ex1.e = &s1;
    _ex2.s = 0;
    aux = spec;
    size = spec.count;
    chartype = size ? s1.chartype : strval::_chartype_ascii;
    chartype_by_slice = (chartype != strval::_chartype_ascii);
}

// Slice member function of strval
inline strexpr
strval::slice(std::optional<i64> start, // Python like semantics
              std::optional<i64> end,
              std::optional<i64> step) const noexcept
{
    std::size_t sz = _get_size();
    // fixme If step is 0, this will not have a value, better handle that here
    slicespec spec = setup_slicespec_like_python(sz,start,end,step).value();
    return strexpr(strexpr::slice_str,*this,spec);
}

// Slice member function of strexpr
inline strexpr
strexpr::slice(std::optional<i64> start, // Python like semantics
               std::optional<i64> end,
               std::optional<i64> step) const noexcept
{
    std::size_t sz = size;
    // fixme If step is 0, this will not have a value, better handle that here
    slicespec spec = setup_slicespec_like_python(sz,start,end,step).value();
    return strexpr(strexpr::slice_expr,*this,spec);
}



} // namespace detail


inline detail::strexpr operator + (detail::strval const &s1) {
    using namespace detail;
    return strexpr(strexpr::mult_str,s1,1); // use str * 1 for unary +
}

inline detail::strexpr operator + (str const &s1) {
    using namespace detail;
    return strexpr(strexpr::mult_str,s1,1); // use str * 1 for unary +
}

inline detail::strexpr operator + (detail::strexpr const &s1) {
    using namespace detail;
    return strexpr(strexpr::mult_expr,s1,1); // use strexpr * 1 for unary +
}

inline cig_bool operator ! (detail::strval const &s1) {
    return s1.is_empty();
}

inline cig_bool operator ! (str const &s1) {
    return s1.is_empty();
}

inline cig_bool operator ! (detail::strexpr const &s1) {
    return s1.size == 0;
}

inline detail::strexpr operator + (detail::strval const &s1,detail::strval const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_str,s1,s2);
}

inline detail::strexpr operator + (detail::strval const &s1,str const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_str,s1,s2);
}

inline detail::strexpr operator + (str const &s1,detail::strval const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_str,s1,s2);
}

inline detail::strexpr operator + (str const &s1,str const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_str,s1,s2);
}

inline detail::strexpr operator + (detail::strval const &s1,detail::strexpr const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_expr,s1,s2);
}

inline detail::strexpr operator + (str const &s1,detail::strexpr const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_expr,s1,s2);
}

inline detail::strexpr operator + (detail::strexpr const &s1,detail::strval const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_expr_str,s1,s2);
}

inline detail::strexpr operator + (detail::strexpr const &s1,str const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_expr_str,s1,s2);
}

inline detail::strexpr operator + (detail::strexpr const &s1,detail::strexpr const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_expr_expr,s1,s2);
}

template<typename T>
inline detail::strexpr operator + (detail::strval const &s1,T const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_other,s1,s2.to_str());
}

template<typename T>
inline detail::strexpr operator + (str const &s1,T const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_str_other,s1,s2.to_str());
}

template<typename T>
inline detail::strexpr operator + (detail::strexpr const &s1,T const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_expr_other,s1,s2.to_str());
}

template<typename T>
inline detail::strexpr operator + (T const &s1,detail::strval const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_other_str,s1.to_str(),s2);
}

template<typename T>
inline detail::strexpr operator + (T const &s1,str const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_other_str,s1.to_str(),s2);
}

template<typename T>
inline detail::strexpr operator + (T const &s1,detail::strexpr const &s2) {
    using namespace detail;
    return strexpr(strexpr::plus_other_expr,s1,s2);
}

inline detail::strexpr operator * (detail::strval const &s,i64 const &m) {
    using namespace detail;
    return strexpr(strexpr::mult_str,s,m._v);
}

inline detail::strexpr operator * (str const &s,i64 const &m) {
    using namespace detail;
    return strexpr(strexpr::mult_str,s,m._v);
}

inline detail::strexpr operator * (detail::strexpr const &s,i64 const &m) {
    using namespace detail;
    return strexpr(strexpr::mult_expr,s,m._v);
}

inline detail::strexpr operator * (i64 const &m,detail::strval const &s) {
    using namespace detail;
    return strexpr(strexpr::mult_str,s,m._v);
}

inline detail::strexpr operator * (i64 const &m,str const &s) {
    using namespace detail;
    return strexpr(strexpr::mult_str,s,m._v);
}

inline detail::strexpr operator * (i64 const &m,detail::strexpr const &s) {
    using namespace detail;
    return strexpr(strexpr::mult_expr,s,m._v);
}

inline str & str::operator += (const strval &s) {
    using namespace detail;

    // Note that *this may be s
    std::size_t sz = _get_size();
    std::size_t szend = sz + s._get_size();

    _reserve(szend,s._get_chartype(),true); // reallocate in powers of 2

    strval::_chartype chartype = _get_chartype();
    cig_assert(chartype >= s._get_chartype());

    if (chartype == strval::_chartype_ucs4)
    {
        std::size_t szaux = strexpr::strval_evaluate_onto(s,_data_ucs4() + sz);
        cig_assert(sz + szaux == szend);
    }
    else if (chartype == strval::_chartype_ucs2)
    {
        std::size_t szaux = strexpr::strval_evaluate_onto(s,_data_ucs2() + sz);
        cig_assert(sz + szaux == szend);
    }
    else
    {
        std::size_t szaux = strexpr::strval_evaluate_onto(s,_data_ucs1() + sz);
        cig_assert(sz + szaux == szend);
    }

    // Set size and add '\0'
    _set_size(szend);

    return *this;
}

constexpr str & str::operator *= (i64 m) {

    if (m._v <= 0)
    {
        _destruct();
        _construct_clear();
    }
    else if (m._v > 1)
    {
        std::size_t mv = static_cast<std::size_t>(m._v);
        std::size_t sz = _get_size();
        if (sz)
        {
            std::size_t i;
            _reserve(mv * sz,_chartype_ascii);
            if (_is_ucs4())
            {
                ucs4_t *data = _data_ucs4();
                for (i = 1; i < mv; i++)
                    detail::str_memcpy(data + i * sz,data,sz);
            }
            else if (_is_ucs2())
            {
                ucs2_t *data = _data_ucs2();
                for (i = 1; i < mv; i++)
                    detail::str_memcpy(data + i * sz,data,sz);
            }
            else
            {
                ucs1_t *data = _data_ucs1();
                for (i = 1; i < mv; i++)
                    detail::str_memcpy(data + i * sz,data,sz);
            }
            _set_size(mv * sz);
        }
    }

    return *this;
}

inline str::str(const detail::strexpr &e)
{
    using namespace detail;

    std::size_t sz = e.size;
    strval::_chartype chartype = e.get_chartype();

    _construct_clear();
    _reserve(sz,chartype,false);
    cig_assert(_get_chartype() == chartype);

    if (chartype == strval::_chartype_ucs4)
    {
        std::size_t szaux = e.evaluate_onto(_data_ucs4());
        cig_assert(szaux == sz);
    }
    else if (chartype == strval::_chartype_ucs2)
    {
        std::size_t szaux = e.evaluate_onto(_data_ucs2());
        cig_assert(szaux == sz);
    }
    else
    {
        std::size_t szaux = e.evaluate_onto(_data_ucs1());
        cig_assert(szaux == sz);
    }

    // Set size and add '\0'
    _set_size(sz);
}

inline str & str::operator = (const detail::strexpr &e)
{
    using namespace detail;

    // Note that *this may be used in the expression e
    str r;
    std::size_t sz = e.size;
    strval::_chartype chartype = e.get_chartype();

    r._reserve(sz,chartype,false);
    cig_assert(r._get_chartype() == chartype);

    if (chartype == strval::_chartype_ucs4)
    {
        std::size_t szaux = e.evaluate_onto(r._data_ucs4());
        cig_assert(szaux == sz);
    }
    else if (chartype == strval::_chartype_ucs2)
    {
        std::size_t szaux = e.evaluate_onto(r._data_ucs2());
        cig_assert(szaux == sz);
    }
    else
    {
        std::size_t szaux = e.evaluate_onto(r._data_ucs1());
        cig_assert(szaux == sz);
    }

    // Set size and add '\0'
    r._set_size(sz);

    // Move to *this
    _destruct();
    _l = r._l;
    r._construct_clear();
    return *this;
}

inline str & str::operator += (const detail::strexpr &e)
{
    using namespace detail;

    // Note that *this may be used in the expression e
    std::size_t sz = _get_size();
    std::size_t szend = sz + e.size;
    strval::_chartype echartype = e.get_chartype();

    /* Note: If *this is contained in the expression e,
     * the _reserve call might change the chartype of *this
     * and therefore also the chartype of some subexpressions of e
     * that depend on *this.
     * In the above code, however, that does not matter.
     * The chartypes of the subexpressions have already been used
     * when the chartype was accumulated upwards, they are not used
     * any more.
     */
    _reserve(szend,echartype,true); // reallocate in powers of 2

    strval::_chartype chartype = _get_chartype();
    cig_assert(chartype >= echartype);

    if (chartype == strval::_chartype_ucs4)
    {
        std::size_t szaux = e.evaluate_onto(_data_ucs4() + sz);
        cig_assert(sz + szaux == szend);
    }
    else if (chartype == strval::_chartype_ucs2)
    {
        std::size_t szaux = e.evaluate_onto(_data_ucs2() + sz);
        cig_assert(sz + szaux == szend);
    }
    else
    {
        std::size_t szaux = e.evaluate_onto(_data_ucs1() + sz);
        cig_assert(sz + szaux == szend);
    }

    // Set size and add '\0'
    _set_size(szend);

    return *this;
}


constexpr
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::strval &s1,const detail::strval &s2) noexcept
{
    auto chartype1 = s1._get_chartype();
    auto chartype2 = s2._get_chartype();

    // Special code for constexpr evaluation
    if consteval // std::is_constant_evaluated() gives a warning for 'throw'
    {
        // pointered strings can never be constexpr, so not constant evaluated
        if (s1._is_pointered() || s2._is_pointered())
            cig_static_assert(false,"pointered str cannot be constexpr");

        if (chartype1 == str::_chartype_ascii ||
            chartype1 == str::_chartype_ucs1)
        {
            str::_short_u8 c1 =
                std::bit_cast<str::_short_u8,str::_long>(s1._l);
            if (chartype2 == str::_chartype_ascii ||
                chartype2 == str::_chartype_ucs1)
            {
                str::_short_u8 c2 =
                    std::bit_cast<str::_short_u8,str::_long>(s2._l);
                return detail::str_memcmp(c1.c8 + str::_c8_offset,
                                          c2.c8 + str::_c8_offset,
                                          s1._get_size(),s2._get_size());
            }
            else if (chartype2 == str::_chartype_ucs2)
            {
                str::_short_u16 c2 =
                    std::bit_cast<str::_short_u16,str::_long>(s2._l);
                return detail::str_memcmp(c1.c8 + str::_c8_offset,
                                          c2.c16 + str::_c16_offset,
                                          s1._get_size(),s2._get_size());
            }
            else
            {
                cig_assert(chartype2 == str::_chartype_ucs4);
                str::_short_u32 c2 =
                    std::bit_cast<str::_short_u32,str::_long>(s2._l);
                return detail::str_memcmp(c1.c8 + str::_c8_offset,
                                          c2.c32 + str::_c32_offset,
                                          s1._get_size(),s2._get_size());
            }
        }
        else if (chartype1 == str::_chartype_ucs2)
        {
            str::_short_u16 c1 =
                std::bit_cast<str::_short_u16,str::_long>(s1._l);
            if (chartype2 == str::_chartype_ascii ||
                chartype2 == str::_chartype_ucs1)
            {
                str::_short_u8 c2 =
                    std::bit_cast<str::_short_u8,str::_long>(s2._l);
                return detail::str_memcmp(c1.c16 + str::_c16_offset,
                                          c2.c8 + str::_c8_offset,
                                          s1._get_size(),s2._get_size());
            }
            else if (chartype2 == str::_chartype_ucs2)
            {
                str::_short_u16 c2 =
                    std::bit_cast<str::_short_u16,str::_long>(s2._l);
                return detail::str_memcmp(c1.c16 + str::_c16_offset,
                                          c2.c16 + str::_c16_offset,
                                          s1._get_size(),s2._get_size());
            }
            else
            {
                cig_assert(chartype2 == str::_chartype_ucs4);
                str::_short_u32 c2 =
                    std::bit_cast<str::_short_u32,str::_long>(s2._l);
                return detail::str_memcmp(c1.c16 + str::_c16_offset,
                                          c2.c32 + str::_c32_offset,
                                          s1._get_size(),s2._get_size());
            }
        }
        else
        {
            cig_assert(chartype1 == str::_chartype_ucs4);
            str::_short_u32 c1 =
                std::bit_cast<str::_short_u32,str::_long>(s1._l);
            if (chartype2 == str::_chartype_ascii ||
                chartype2 == str::_chartype_ucs1)
            {
                str::_short_u8 c2 =
                    std::bit_cast<str::_short_u8,str::_long>(s2._l);
                return detail::str_memcmp(c1.c32 + str::_c32_offset,
                                          c2.c8 + str::_c8_offset,
                                          s1._get_size(),s2._get_size());
            }
            else if (chartype2 == str::_chartype_ucs2)
            {
                str::_short_u16 c2 =
                    std::bit_cast<str::_short_u16,str::_long>(s2._l);
                return detail::str_memcmp(c1.c32 + str::_c32_offset,
                                          c2.c16 + str::_c16_offset,
                                          s1._get_size(),s2._get_size());
            }
            else
            {
                cig_assert(chartype2 == str::_chartype_ucs4);
                str::_short_u32 c2 =
                    std::bit_cast<str::_short_u32,str::_long>(s2._l);
                return detail::str_memcmp(c1.c32 + str::_c32_offset,
                                          c2.c32 + str::_c32_offset,
                                          s1._get_size(),s2._get_size());
            }
        }
    }

    // General code for non-constexpr evaluation
    else
    {
        if (chartype1 == str::_chartype_ascii ||
            chartype1 == str::_chartype_ucs1)
        {
            if (chartype2 == str::_chartype_ascii ||
                chartype2 == str::_chartype_ucs1)
            {
                return detail::str_memcmp(s1._data_ucs1(),s2._data_ucs1(),
                                          s1._get_size(),s2._get_size());
            }
            else if (chartype2 == str::_chartype_ucs2)
            {
                return detail::str_memcmp(s1._data_ucs1(),s2._data_ucs2(),
                                          s1._get_size(),s2._get_size());
            }
            else
            {
                cig_assert(chartype2 == str::_chartype_ucs4);
                return detail::str_memcmp(s1._data_ucs1(),s2._data_ucs4(),
                                          s1._get_size(),s2._get_size());
            }
        }
        else if (chartype1 == str::_chartype_ucs2)
        {
            if (chartype2 == str::_chartype_ascii ||
                chartype2 == str::_chartype_ucs1)
            {
                return detail::str_memcmp(s1._data_ucs2(),s2._data_ucs1(),
                                          s1._get_size(),s2._get_size());
            }
            else if (chartype2 == str::_chartype_ucs2)
            {
                return detail::str_memcmp(s1._data_ucs2(),s2._data_ucs2(),
                                          s1._get_size(),s2._get_size());
            }
            else
            {
                cig_assert(chartype2 == str::_chartype_ucs4);
                return detail::str_memcmp(s1._data_ucs2(),s2._data_ucs4(),
                                          s1._get_size(),s2._get_size());
            }
        }
        else
        {
            cig_assert(chartype1 == str::_chartype_ucs4);
            if (chartype2 == str::_chartype_ascii ||
                chartype2 == str::_chartype_ucs1)
            {
                return detail::str_memcmp(s1._data_ucs4(),s2._data_ucs1(),
                                          s1._get_size(),s2._get_size());
            }
            else if (chartype2 == str::_chartype_ucs2)
            {
                return detail::str_memcmp(s1._data_ucs4(),s2._data_ucs2(),
                                          s1._get_size(),s2._get_size());
            }
            else
            {
                cig_assert(chartype2 == str::_chartype_ucs4);
                return detail::str_memcmp(s1._data_ucs4(),s2._data_ucs4(),
                                          s1._get_size(),s2._get_size());
            }
        }
    }
}


inline
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::strval &s1,const detail::strexpr &s2) noexcept
{
    return operator <=> (s1,str(s2));
}


inline
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::strexpr &s1,const detail::strval &s2) noexcept
{
    return operator <=> (str(s1),s2);
}


inline
std::strong_ordering            // s1 <=> s2
operator <=>(const detail::strexpr &s1,const detail::strexpr &s2) noexcept
{
    return operator <=> (str(s1),str(s2));
}


constexpr
bool
operator ==(const detail::strval &s1,const detail::strval &s2) noexcept
{
    return operator <=>(s1,s2) == 0;
}


inline
bool
operator ==(const detail::strval &s1,const detail::strexpr &s2) noexcept
{
    return operator <=>(s1,str(s2)) == 0;
}


inline
bool
operator ==(const detail::strexpr &s1,const detail::strval &s2) noexcept
{
    return operator <=>(str(s1),s2) == 0;
}


inline
bool
operator ==(const detail::strexpr &s1,const detail::strexpr &s2) noexcept
{
    return operator <=>(str(s1),str(s2)) == 0;
}


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_STR_INLINE_H_INCLUDED) */
