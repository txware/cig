/* cig_file.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Miscellaneous file tools.
 */
#ifndef CIG_FILE_H_INCLUDED
#define CIG_FILE_H_INCLUDED

#include <utility>              // std::move

#include "cig_datatypes.h"      // str,err
#include "cig_int_inline.h"     // i64 operations

namespace cig {

namespace file {

exp_bytes exp_read_file_into_bytes(str const &filename) noexcept;
exp_str exp_read_file_into_str(str const &filename) noexcept;
bytes read_file_into_bytes(str const &filename) noexcept(false);
str read_file_into_str(str const &filename) noexcept(false);


} // namespace file


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_FILE_H_INCLUDED) */
