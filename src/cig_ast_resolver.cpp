/* cig_ast_resolver.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Abstract Syntax Tree - Reference Resolver
 */
#include "cig_ast_resolver.h"
#include "cig_ast_builtin.h"

namespace cig {

namespace ast {

namespace detail {

using exp_resolved_entity = std::expected<ast_resolved_entity,cig::err>;



/* The code in this file handles both resolution and access determination.

   RESOLUTION AND VISIBILITY

   Resolution is the process by which each qualified name in a program,
   starting from an unqualified name and following a chain of named
   member dereferences, is assigned an entity and a visibility state.

   The entities that are ultimately resolved are types (classes and enums),
   variables (including function parameters), and functions.
   In intermediate states of the member chain, namespaces and the global
   namespace may also be resolved.

   The visibility state is defined by the constants in ast_resolve_quality;
   while most entities have just one corresponding resolve quality, the
   function entities fundef, fundecl, and overload set are combined to
   the FNNAME quality, and the resolve quality for a class is split:
   AST_RQ_CLASS denotes a class definition fully visible, while
   AST_RQ_CLASSFROMDEF denotes a class definition that contains the
   place of lookup, and is therefore only partially visible at the
   place of lookup.

   Visibility is modified by a big distinction: Within a function body,
   all its enclosing classes are fully visible, whereas outside a function
   body, the classes and variables are only visible as specified up to that
   place in the code.

   This distinction must be kept up in order to maintain compatibility with
   C++ and in order to allow data types to incorporate other types by
   value (including the full memory), not just by reference as is the
   case in e.g. Java or Python.

   ACCESS

   Access describes the way in which the runtime code can access an object
   in memory. Access is defined by the constants in ast_access_kind.

   Access is only relevant for entities that need such an object:
   Functions (including class member functions) and variables.
   For data types and namespaces, no runtime access is required.
   
   Access for objects and functions can be in three kinds:
   - Local objects (there are no local functions) are objects that
     exist on a function's memory stack; they are the function
     parameters, return values, function local variables, and
     temporary objects of the function.
   - Global objects and functions are objects and functions that
     exist in the global namespace or directly in another namespace.
     They can be accessed from anywhere in the program.
     These include static members of classes, which are visible
     only through the class but accessible from anywhere.
   - Class member objects and functions are objects and functions that
     are nonstatic members of a class.
     They can only be accessed via an object of the class or from inside
     the class or inside one of its nonstatic nested inner classes.
     When accessing a variable or a function of an outer nested class
     from the inner nested class, all nested classes from the inner
     nested class up to (but not including) the outer must be nonstatic
     classes.
     Nonstatic classes carry an implicit 'outer' pointer to the outer
     class that contains them. Objects of nonstatic classes can only
     be created from a pointer to an object of the static class outside. */



/**********************************************************************/
/*                                                                    */
/*                      RESOLVE UNQUALIFIED NAME                      */
/*                                                                    */
/**********************************************************************/


// Forward declaration
static
exp_resolved_entity
resolve_member_name_in_class(
    ast_node_ptr scope,         // The class scope to search fully
    str name,                   // Member name/unqualified name to lookup up
    cig::source_location sloc   // Location of name for error messages
    );



static
ast_node_ptr                    // Base class of derived class, or null
get_base_class(
    [[maybe_unused]]
    ast_node_ptr derived_class  // The class to get the base for
    )
{
    // This function is reserved for later use
    // when base classes and derived classes is
    // implemented.
    return nullptr;
}


static
exp_resolved_entity
resolve_member_name_in_base_classes(
    ast_node_ptr scope,         // The class of which to search the base classes
    str name,                   // Member name/unqualified name to lookup up
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    // Note that all base classes of a class must be static.
    // Non-static base classes would allow access to the
    // members of their enclosing classes, which is not possible.

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (name.is_empty())
        return makeerr(err::EC_AST_RESOLVE_NAME_IS_EMPTY,sloc);

    auto scope_type = scope->get_node_type();
    cig_assert(scope_type == AST_CLASSDEF);

    ast_node_ptr bscope = get_base_class(scope);
    if (!bscope)
        return ast_resolved_entity(nullptr,AST_RQ_NOTFOUND);

    return resolve_member_name_in_class(bscope,name,sloc);
}


// Try to find a name in a single class
// with inspecting its base classes
// but without expecting its enclosing classes.
static
exp_resolved_entity
resolve_member_name_in_class(
    ast_node_ptr scope,         // The class scope to search fully
    str name,                   // Member name/unqualified name to lookup up
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (name.is_empty())
        return makeerr(err::EC_AST_RESOLVE_NAME_IS_EMPTY,sloc);

    auto scope_type = scope->get_node_type();
    cig_assert(scope_type == AST_CLASSDEF);

    i64 idx = scope->lookup_name(name);
    if (idx != -1)
    {
        cig_assert(idx >= 0);

        // Found the name
        auto nd = scope->get_subnode(idx);
        cig_assert(nd);

        // Classify the name
        ast_node_type node_type = nd->get_node_type();
        if (node_type == AST_NAMESPACE)
            return ast_resolved_entity({ nd, AST_RQ_NAMESPACE });
        else if (node_type == AST_CLASSDECL)
        {
            auto nd2 = nd->get_classdef();
            if (nd2)
                return ast_resolved_entity({ nd2, AST_RQ_CLASS });
            else
                return ast_resolved_entity({ nd, AST_RQ_CLASSDECL });
        }
        else if (node_type == AST_CLASSDEF)
            return ast_resolved_entity({ nd, AST_RQ_CLASS });
        else if (node_type == AST_ENUMDECL)
        {
            auto nd2 = nd->get_classdef();
            if (nd2)
                return ast_resolved_entity({ nd2, AST_RQ_ENUM });
            else
                return ast_resolved_entity({ nd, AST_RQ_ENUMDECL });
        }
        else if (node_type == AST_ENUMDEF)
            return ast_resolved_entity({ nd, AST_RQ_ENUM });
        else if (node_type == AST_ENUMVALUE) // cannot be in class
            return ast_resolved_entity({ nd, AST_RQ_ENUMVALUE });
        else if (node_type == AST_VARIABLE)
            return ast_resolved_entity({ nd, AST_RQ_VARIABLE });
        else if (node_type == AST_OVERLOADSET)
            return ast_resolved_entity({ nd, AST_RQ_FNNAME });
        else
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
    }
    else if (scope_type == AST_CLASSDEF)
    {
        // Look up in all base classes
        auto expre = resolve_member_name_in_base_classes(scope,name,sloc);
        if (!expre)
            return expre;
        auto re = expre.value();
        if (std::get<ast_resolve_quality>(re) != AST_RQ_NOTFOUND)
            return re;          // Found the name in the base class
    }
    return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
}



// Try to find a name in an enum
static
exp_resolved_entity
resolve_member_name_in_enum(
    ast_node_ptr scope,         // The enum scope to search fully
    str name,                   // Member name/unqualified name to lookup up
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (name.is_empty())
        return makeerr(err::EC_AST_RESOLVE_NAME_IS_EMPTY,sloc);

    ast_node_type scope_type = scope->get_node_type();
    cig_assert(scope_type == AST_ENUMDEF);

    i64 idx = scope->lookup_name(name);
    if (idx != -1)
    {
        cig_assert(idx >= 0);

        // Found the name
        auto nd = scope->get_subnode(idx);
        cig_assert(nd);

        // Classify the name
        ast_node_type node_type = nd->get_node_type();
        if (node_type != AST_ENUMVALUE)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
        return ast_resolved_entity({ nd, AST_RQ_ENUMVALUE });
    }
    return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
}



// Try to find a name in a scope
// and all its enclosing scopes
// and base classes if it is a class.
static
exp_resolved_entity             // entity found, or null
resolve_unqualified_name_in_scope(
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    str name,                   // Unqualified name to lookup up
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_node_br_special;
    using enum ast::ast_resolve_quality;

    // This function should not be called iteratively,
    // but always from the point-of-lookup.
    // It does its own iteration through the enclosing scopes.

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (pos_in_scope == -1)
        return makeerr(err::EC_RESOLVER_NO_POS_IN_SCOPE,sloc);
    if (name.is_empty())
        return makeerr(err::EC_AST_RESOLVE_NAME_IS_EMPTY,sloc);

    // Name lookup works very much like C++:
    //
    // All names are looked up from the position where they are declared.
    // That is the position in which the scopes are nested.
    //
    // There are two exceptions:
    //
    // Within a class scope, if the name cannot be found within the
    // class, it will be searched in the base classes of the class,
    // and within the base classes the full class scope will be searched.
    //
    // For searches that start within a function, if the name is not found
    // within the function, the full class scope of all enclosing classes
    // is searched (also with base classes).
    // However, if that does not resolve the name, the outer scopes
    // (which must be namespace scopes) will again be searched from
    // the position where the function definition is in the source code
    // (scope_in_global).

    bool search_full_classes = false;
    bool search_definition_in_full_classes = false;
    auto scope_type = scope->get_node_type();

    // Use pos_in_global only if scope is a namespace
    if (scope_type != AST_NAMESPACE &&
        scope_type != AST_GLOBAL)
        pos_in_global = -1;


    // Loop over the enclosing scopes up to the global namespace
    // This loop iterates over all nested namespaces; however,
    // when considering whether a name is visible at the point
    // of lookup, the pos_in_global is used to check the visibility.
    for (;;)
    {
        i64 idx = scope->lookup_name(name);
        if (idx != -1)
        {
            cig_assert(idx >= 0);

            // The entity (AST node) for the name
            auto nd = scope->get_subnode(idx);
            cig_assert(nd);
            ast_node_type node_type = nd->get_node_type();
            auto global_br = nd->get_global_backref();
            auto gpidx = std::get<i64>(global_br); // -1 unless in namespace

            // Index comparisions are different inside namespace and outside
            bool scope_is_namespace = (scope_type == AST_NAMESPACE ||
                                       scope_type == AST_GLOBAL);

            bool entity_before_resolveloc =
                (scope_is_namespace ?
                 gpidx <= pos_in_global :
                 idx <= pos_in_scope); // incorrect for special pos_in_scope < 0
            bool entity_contains_resolveloc =
                (scope_is_namespace ?
                 gpidx == pos_in_global :
                 idx == pos_in_scope);


            // Note that the special values of pos_in_scope < 0
            // never occur inside a namespace;
            // those are the pseudo-index values for init statements etc.

            // Special child of scope: Init statement
            if (pos_in_scope == AST_BR_INIT) // never in nmspc
            {
                // The init statement does not see variables that
                // it declared itself.
                // Fall through: not found
            }
            // Special child of scope: Iterator of for..of statement
            else if (pos_in_scope == AST_BR_ITERATOR)// never in nmspc
            {
                // The iterator sees only the variables in the init statement,
                // not the variables in the iterator itself.
                i64 n = scope->get_initstmt_variables();

                if (idx < n)
                {
                    if (node_type != AST_VARIABLE)
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

                    return ast_resolved_entity({ nd, AST_RQ_VARIABLE });
                }
                // Else fall through: not found
            }

            else if (

                // All other special children of scope see all variables
                pos_in_scope < 0 || // never in namespace

                // Regular children of scope see entities before them
                entity_before_resolveloc ||

                // Function body sees all entities in class
                (scope_type == AST_CLASSDEF &&
                 search_full_classes))
            {
                // Special child of scope: The entity must be a variable
                if (pos_in_scope < 0) // never in namespace
                {
                    // The entity must be a variable
                    if (node_type != AST_VARIABLE)
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

                    return ast_resolved_entity({ nd, AST_RQ_VARIABLE });
                }

                // Classify the name
                if (node_type == AST_NAMESPACE)
                    return ast_resolved_entity({ nd, AST_RQ_NAMESPACE });
                else if (node_type == AST_CLASSDECL)
                {
                    // Try to use definition instead of declaration
                    auto nd2 = nd->get_classdef();
                    if (nd2)
                    {
                        // Is the full scope to be searched?
                        if (scope_type == AST_CLASSDEF &&
                            search_definition_in_full_classes)
                            return ast_resolved_entity({ nd2, AST_RQ_CLASS });
                        // Is the classdef before place of lookup?
                        if (scope_is_namespace)
                        {
                            ast_node_backref br = nd2->get_global_backref();
                            auto gpidx2 = std::get<i64>(br);
                            if (gpidx2 != -1 && gpidx2 <= pos_in_global)
                            {
                                if (gpidx2 == pos_in_global)
                                    return ast_resolved_entity(
                                        { nd2, AST_RQ_CLASSFROMDEF });
                                else
                                    return ast_resolved_entity(
                                        { nd2, AST_RQ_CLASS });
                            }
                        }
                        else
                        {
                            ast_node_backref br = nd2->get_backref();
                            auto pidx = std::get<i64>(br);
                            if (pidx != -1 && pidx <= pos_in_scope)
                            {
                                if (pidx == pos_in_scope)
                                    return ast_resolved_entity(
                                        { nd2, AST_RQ_CLASSFROMDEF });
                                else
                                    return ast_resolved_entity(
                                        { nd2, AST_RQ_CLASS });
                            }
                        }
                    }
                    // No classdef seen before pos_in_scope
                    return ast_resolved_entity({ nd, AST_RQ_CLASSDECL });
                }
                else if (node_type == AST_CLASSDEF)
                {
                    if (entity_contains_resolveloc)
                        return ast_resolved_entity(
                            { nd, AST_RQ_CLASSFROMDEF });
                    else
                        return ast_resolved_entity(
                            { nd, AST_RQ_CLASS });
                }
                else if (node_type == AST_ENUMDECL)
                {
                    // Try to use definition instead of declaration
                    auto nd2 = nd->get_enumdef();
                    if (nd2)
                    {
                        // Is the full scope to be searched?
                        if (scope_type == AST_CLASSDEF &&
                            search_definition_in_full_classes)
                            return ast_resolved_entity(
                                { nd2, AST_RQ_ENUM });
                        // Is the enumdef before place of lookup?
                        if (scope_is_namespace)
                        {
                            ast_node_backref br = nd2->get_global_backref();
                            auto gpidx2 = std::get<i64>(br);
                            if (gpidx2 != -1 && gpidx2 < pos_in_global)
                                return ast_resolved_entity(
                                    { nd2, AST_RQ_ENUM });
                        }
                        else
                        {
                            ast_node_backref br = nd2->get_backref();
                            auto pidx = std::get<i64>(br);
                            if (pidx != -1 && pidx < pos_in_scope)
                                return ast_resolved_entity(
                                    { nd2, AST_RQ_ENUM });
                        }
                    }
                    // No enumdef seen before pos_in_scope
                    return ast_resolved_entity({ nd, AST_RQ_ENUMDECL });
                }
                else if (node_type == AST_ENUMDEF)
                    return ast_resolved_entity({ nd, AST_RQ_ENUM });
                else if (node_type == AST_ENUMVALUE) // not in class
                    return ast_resolved_entity({ nd, AST_RQ_ENUMVALUE });
                else if (node_type == AST_VARIABLE)
                    return ast_resolved_entity({ nd, AST_RQ_VARIABLE });
                else if (node_type == AST_OVERLOADSET)
                    return ast_resolved_entity({ nd, AST_RQ_FNNAME });
                else
                    return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
            }

            // else not found in scope (declaration too late)
            else
            {
                if (!scope_is_namespace)
                    return makeerr(err::EC_RESOLVED_NAME_DECLARED_LATER,sloc);
            }
        }
        else if (scope_type == AST_FUNBODY)
        {
            // Look up in parameter list
            ast_node_ptr fundef = scope->get_lambda_backref();
            ast_node_ptr parlist = fundef ? fundef->get_funparlist() : nullptr;
            if (parlist)
            {
                i64 paridx = parlist->lookup_name(name);
                if (paridx != -1)
                {
                    cig_assert(paridx >= 0);

                    // Found the name
                    auto nd = parlist->get_subnode(paridx);
                    cig_assert(nd);

                    ast_node_type node_type = nd->get_node_type();
                    if (node_type != AST_FUNPAR)
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

                    return ast_resolved_entity({ nd, AST_RQ_FUNPAR });
                }
            }
        }

        // If not found in this class, perhaps in base classes?
        if (scope_type == AST_CLASSDEF)
        {
            // Look up in all base classes
            auto expre = resolve_member_name_in_base_classes(scope,name,sloc);
            if (!expre)
                return expre;
            auto re = expre.value();
            if (std::get<ast_resolve_quality>(re) != AST_RQ_NOTFOUND)
                return re;      // Found the name in the base class
            // else continue
        }

        // end of search
        if (scope_type == AST_GLOBAL)
            break;

        //
        // Proceed to the parent scope
        //

        // When coming from inside a function body, do some settings
        // to search the full classes
        // and to lookup names from point-of-definition in namespaces.
        // When coming from the parameter list or return type list of
        // a function definition, search the full classes for
        // data type definitions.
        if (scope_type == AST_FUNPARLIST ||
            scope_type == AST_FUNRETLIST ||
            scope_type == AST_FUNBODY)
        {
            ast_node_ptr fundef = scope->get_lambda_backref();
            if (fundef &&
                fundef->get_node_type() == AST_FUNDEF)
            {
                // Set search in full classes when resolving names
                // inside the function body.
                if (scope_type == AST_FUNBODY)
                    search_full_classes = true;
                // Resolve type definitions in full classes when resolving
                // names inside parameter list, return list or function body.
                search_definition_in_full_classes = true;
                // Set the encapsulating namespace and position
                // when leaving the outermost class to resolve
                // in namespace at the place of function definition

                auto fundef_global_br = fundef->get_global_backref();
                auto fundef_gpidx = std::get<i64>(fundef_global_br);
                if (fundef_gpidx != -1)
                    pos_in_global = fundef_gpidx;
            }
        }

        // Get parent scope
        ast_node_ptr anchorednd = scope;
        if (scope_type == AST_FUNPARLIST ||
            scope_type == AST_FUNRETLIST ||
            scope_type == AST_FUNBODY)
        {
            anchorednd = scope->get_lambda_backref();
            if (!anchorednd)
            {
                anchorednd = scope->get_fundecl_backref();
                if (anchorednd)
                    anchorednd = anchorednd->get_lambda_backref();
            }
        }
        if (!anchorednd)
            return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
        ast_node_backref br = anchorednd->get_backref();
        auto parent = std::get<ast_node_ptr>(br);
        auto pidx = std::get<i64>(br);

        if (!parent)
            return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
        if (pidx == -1)
            return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);


        auto parent_scope_type = parent->get_node_type();

        // Remember pos_in_global if scope is a non-namespace in a namespace
        if (scope_type != AST_NAMESPACE &&
            scope_type != AST_GLOBAL &&
            (parent_scope_type == AST_NAMESPACE ||
             parent_scope_type == AST_GLOBAL))
        {
            auto global_br = anchorednd->get_global_backref();
            auto gpidx = std::get<i64>(global_br);

            // Do not override a pos_in_global that has already been set
            // from an AST_FUNBODY
            if (pos_in_global == -1)
                pos_in_global = gpidx;
        }


        // Proceed to the parent
        scope = std::move(parent);
        scope_type = parent_scope_type;
        pos_in_scope = pidx;
    }

    return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
}



/**********************************************************************/
/*                                                                    */
/*                        RESOLVE MEMBER NAME                         */
/*                                                                    */
/**********************************************************************/


/* Note on duplication:

   We need to duplicate the descent from inner scope to outer scope here.

   While it should be possible to do the whole resolution recursively
   in the resolve_subtree function, this is only practically feasible
   if the compound data type, of which we are selecting a member, is
   found at the innermost level.

   However, the language CIG, just like C++, should also allow to specify
   a data type (inside which we are starting a lookup) as a fully qualified
   name, i.e. starting from the global level. And if not fully qualified,
   even partially qualified, starting (redundantly) from an enclosing
   scope must be possible.

   In these cases, the recursion of resolve_subtree will not memorize
   the pos_in_scope for each intermediate member dereference; rather,
   it carries the single scope and pos_in_scope of the subtree top node
   (typeref etc.) through all member lookups to the bottom node, which
   is an unqualitife name or entity link.

   Thus, to follow the member operations inbetween qualified name and
   typeref, we need to re-calculate the pos_in_scope indices on the
   various intermediate nesting levels. This is done by a separate
   function here, which duplicates the recursion of
   resolve_unqualified_name_in_scope above.

   While this leads to quadratic effort in name resolution, we trust
   that the maximum nesting depth of reasonable code will only be
   a few levels, and thus not be a problem. */


static
exp_resolved_entity             // entity found, or null
resolve_member_name(
    ast_node_ptr basend,        // the node containing the member
    ast_resolve_quality basequ, // and the base node's quality
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    str name,                   // Member name to look up
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    // This function should not be called iteratively,
    // but always from the point-of-lookup.
    // It does its own iteration through the enclosing scopes.

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (pos_in_scope == -1)
        return makeerr(err::EC_RESOLVER_NO_POS_IN_SCOPE,sloc);
    if (name.is_empty())
        return makeerr(err::EC_AST_RESOLVE_NAME_IS_EMPTY,sloc);


    // Member lookup differs by resolve-quality (type etc.) of parent node
    switch (basequ)
    {
    case AST_RQ_INVALID:
        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

    case AST_RQ_NOTFOUND:
        return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });

    case AST_RQ_NAMESPACE:
    {
        auto scope_type = scope->get_node_type();

        // Proceed from point-of-lookup through all non-namespace layers
        // outwards until we reach a namespace layer.
        for (;;)
        {
            if (scope_type == AST_NAMESPACE ||
                scope_type == AST_GLOBAL)
                break;

            // Since this function must be called from point-of-lookup,
            // the pos_in_global cannot be set if we are not in a
            // namespace.
            // Everything else is an algorithmic error.
            cig_assert(pos_in_global == -1);

            // When coming from inside a function body,
            // go straight to the point of definition in namespace
            // (unless defined within a class).
            if (scope_type == AST_FUNBODY)
            {
                ast_node_ptr fundef = scope->get_lambda_backref();

                if (fundef)
                {
                    auto fundef_global_br = fundef->get_global_backref();
                    auto fundef_gpidx = std::get<i64>(fundef_global_br);
                    if (fundef_gpidx != -1)
                    {
                        ast_node_backref br = fundef->get_backref();
                        auto parent = std::get<ast_node_ptr>(br);
                        auto pidx = std::get<i64>(br);

                        if (!parent)
                            return makeerr(
                                err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
                        if (pidx == -1)
                            return makeerr(
                                err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);

                        auto parent_scope_type = parent->get_node_type();
                        if (parent_scope_type != AST_NAMESPACE &&
                            parent_scope_type != AST_GLOBAL)
                            return makeerr(
                                err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);

                        // Done with the for loop
                        scope = parent;
                        scope_type = parent_scope_type;
                        pos_in_scope = pidx;
                        pos_in_global = fundef_gpidx;
                        break;
                    }
                }
            }


            //
            // Proceed to the parent scope
            //

            // Get parent scope
            ast_node_ptr anchorednd = scope;
            if (scope_type == AST_FUNPARLIST ||
                scope_type == AST_FUNRETLIST ||
                scope_type == AST_FUNBODY)
            {
                anchorednd = scope->get_lambda_backref();
                if (!anchorednd)
                {
                    anchorednd = scope->get_fundecl_backref();
                    if (anchorednd)
                        anchorednd = anchorednd->get_lambda_backref();
                }
            }
            if (!anchorednd)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
            ast_node_backref br = anchorednd->get_backref();
            auto parent = std::get<ast_node_ptr>(br);
            auto pidx = std::get<i64>(br);

            if (!parent)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
            if (pidx == -1)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);

            auto parent_scope_type = parent->get_node_type();

            // Reached the end if scope is a non-namespace in a namespace
            if (parent_scope_type == AST_NAMESPACE ||
                parent_scope_type == AST_GLOBAL)
            {
                auto global_br = anchorednd->get_global_backref();
                auto gpidx = std::get<i64>(global_br);

                // Done with the for loop
                scope = parent;
                scope_type = parent_scope_type;
                pos_in_scope = pidx;
                pos_in_global = gpidx;
                break;
            }

            // Proceed to the parent
            scope = std::move(parent);
            scope_type = parent_scope_type;
            pos_in_scope = pidx;
            cig_assert(pos_in_global == -1);
        }


        // State: Now scope and pos_in_scope are the innermost namespace
        // in which the point-of-lookup is located.
        // The pos_in_global is set.
        //
        // The basend is the node which should contain the member.
        //
        // The scope and basend have unspecified relations; one could
        // be within the other, or both could be disjunct.
        // Importantly, the pos_in_global and the gpidx of the name
        // at index idx determine the visibility of the name at the
        // point of lookup.

        // Look up name in basend
        i64 idx = basend->lookup_name(name);
        if (idx == -1)
            return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });

        cig_assert(idx >= 0); // basend is a namespace, no negative idx

        // The entity (AST node) for the name
        auto nd = basend->get_subnode(idx);
        cig_assert(nd);
        ast_node_type node_type = nd->get_node_type();
        auto global_br = nd->get_global_backref();
        auto gpidx = std::get<i64>(global_br);
        cig_assert(gpidx != -1); // basend is a namespace

        // Relative position of gpidx and pos_in_global
        bool entity_before_resolveloc = (gpidx <= pos_in_global);
        bool entity_contains_resolveloc = (gpidx == pos_in_global);

        // Member not yet visible?
        if (!entity_before_resolveloc)
            return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });

        // Classify the name
        if (node_type == AST_NAMESPACE)
            return ast_resolved_entity({ nd, AST_RQ_NAMESPACE });
        else if (node_type == AST_CLASSDECL)
        {
            // Try to use definition instead of declaration
            auto nd2 = nd->get_classdef();
            if (nd2)
            {
                // Is the classdef before place of lookup?
                ast_node_backref br = nd2->get_global_backref();
                auto gpidx2 = std::get<i64>(br);
                if (gpidx2 != -1 && gpidx2 <= pos_in_global)
                {
                    if (gpidx2 == pos_in_global)
                        return ast_resolved_entity(
                            { nd2, AST_RQ_CLASSFROMDEF });
                    else
                        return ast_resolved_entity(
                            { nd2, AST_RQ_CLASS });
                }
            }
            // No classdef seen before pos_in_basend
            return ast_resolved_entity({ nd, AST_RQ_CLASSDECL });
        }
        else if (node_type == AST_CLASSDEF)
        {
            if (entity_contains_resolveloc)
                return ast_resolved_entity({ nd, AST_RQ_CLASSFROMDEF });
            else
                return ast_resolved_entity({ nd, AST_RQ_CLASS });
        }
        else if (node_type == AST_ENUMDECL)
        {
            // Try to use definition instead of declaration
            auto nd2 = nd->get_enumdef();
            if (nd2)
            {
                // Is the enumdef before place of lookup?
                ast_node_backref br = nd2->get_global_backref();
                auto gpidx2 = std::get<i64>(br);
                if (gpidx2 != -1 && gpidx2 < pos_in_global)
                    return ast_resolved_entity({ nd2, AST_RQ_ENUM });
            }
            // No enumdef seen before pos_in_basend
            return ast_resolved_entity({ nd, AST_RQ_ENUMDECL });
        }
        else if (node_type == AST_ENUMDEF)
            return ast_resolved_entity({ nd, AST_RQ_ENUM });
        else if (node_type == AST_ENUMVALUE) // cannot be in namespace
            return ast_resolved_entity({ nd, AST_RQ_ENUMVALUE });
        else if (node_type == AST_VARIABLE)
            return ast_resolved_entity({ nd, AST_RQ_VARIABLE });
        else if (node_type == AST_OVERLOADSET)
            return ast_resolved_entity({ nd, AST_RQ_FNNAME });

        // else
        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
    }


    case AST_RQ_CLASSFROMDEF:
    {
        bool search_full_classes = false;
        bool search_definition_in_full_classes = false;
        auto scope_type = scope->get_node_type();

        // The basend must be a classdef for AST_RQ_CLASSFROMDEF
        if (basend->get_node_type() != AST_CLASSDEF)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

        // The scope must not be a namespace
        if (scope_type == AST_GLOBAL ||
            scope_type == AST_NAMESPACE)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

        // Proceed from point-of-lookup through several non-namespace
        // layers until we reach basend.
        for (;;)
        {
            if (scope == basend)
                break;

            // CLASSFROMDEF must find basend before reaching global
            // and before reaching a namespace
            if (scope_type == AST_GLOBAL ||
                scope_type == AST_NAMESPACE)
                return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);


            //
            // Proceed to the parent scope
            //

            // When coming from inside a function body, do some settings
            // to search the full classes
            if (scope_type == AST_FUNPARLIST ||
                scope_type == AST_FUNRETLIST ||
                scope_type == AST_FUNBODY)
            {
                ast_node_ptr fundef = scope->get_lambda_backref();
                if (fundef &&
                    fundef->get_node_type() == AST_FUNDEF)
                {
                    // Set search in full classes when resolving names
                    // inside the function body
                    if (scope_type == AST_FUNBODY)
                        search_full_classes = true;
                    // Resolve type definitions in full classes when resolving
                    // names inside parameter lists, return lists, or fn body.
                    search_definition_in_full_classes = true;
                }
            }

            // Get parent scope
            ast_node_ptr anchorednd = scope;
            if (scope_type == AST_FUNPARLIST ||
                scope_type == AST_FUNRETLIST ||
                scope_type == AST_FUNBODY)
            {
                anchorednd = scope->get_lambda_backref();
                if (!anchorednd)
                {
                    anchorednd = scope->get_fundecl_backref();
                    if (anchorednd)
                        anchorednd = anchorednd->get_lambda_backref();
                }
            }
            if (!anchorednd)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
            ast_node_backref br = anchorednd->get_backref();
            auto parent = std::get<ast_node_ptr>(br);
            auto pidx = std::get<i64>(br);

            if (!parent)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
            if (pidx == -1)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);


            auto parent_scope_type = parent->get_node_type();

            // Proceed to the parent
            scope = std::move(parent);
            scope_type = parent_scope_type;
            pos_in_scope = pidx;
        }

        cig_assert(pos_in_global == -1);
        cig_assert(scope == basend);
        cig_assert(scope_type == AST_CLASSDEF);
        cig_assert(pos_in_scope >= 0); // Since scope is a classdef now


        // State: Now scope is identical to basend and pos_in_scope
        // is set correctly.
        //
        // The pos_in_scope and idx of the name determine the visibility
        // of the name at the point of lookup.


        // Look up name in basend/scope
        i64 idx = basend->lookup_name(name);
        if (idx == -1)
            return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });

        cig_assert(idx >= 0); // basend is a namespace, no negative idx

        // The entity (AST node) for the name
        auto nd = basend->get_subnode(idx);
        cig_assert(nd);
        ast_node_type node_type = nd->get_node_type();

        // Relative position of gpidx and pos_in_global
        bool entity_before_resolveloc = (idx <= pos_in_scope);
        bool entity_contains_resolveloc = (idx == pos_in_scope);

        // Note that the special values of pos_in_scope < 0
        // never occurs inside a class;
        // those are the pseudo-index values for init statements etc.
        if (

            // Regular children of scope see entities before them
            entity_before_resolveloc ||

            // Function body sees all entities in class
            // scope_type == AST_CLASSDEF guaranteed by assert
            search_full_classes)
        {
            // Classify the name
            if (node_type == AST_NAMESPACE)
                return ast_resolved_entity({ nd, AST_RQ_NAMESPACE });
            else if (node_type == AST_CLASSDECL)
            {
                // Try to use definition instead of declaration
                auto nd2 = nd->get_classdef();
                if (nd2)
                {
                    // Is the full scope to be searched?
                    if (search_definition_in_full_classes)
                        return ast_resolved_entity({ nd2, AST_RQ_CLASS });
                    // Is the classdef before place of lookup?
                    ast_node_backref br = nd2->get_backref();
                    auto pidx = std::get<i64>(br);
                    if (pidx != -1 && pidx <= pos_in_scope)
                    {
                        if (pidx == pos_in_scope)
                            return ast_resolved_entity(
                                { nd2, AST_RQ_CLASSFROMDEF });
                        else
                            return ast_resolved_entity(
                                { nd2, AST_RQ_CLASS });
                    }
                }
                // No classdef seen before pos_in_scope
                return ast_resolved_entity({ nd, AST_RQ_CLASSDECL });
            }
            else if (node_type == AST_CLASSDEF)
            {
                if (entity_contains_resolveloc)
                    return ast_resolved_entity({ nd, AST_RQ_CLASSFROMDEF });
                else
                    return ast_resolved_entity({ nd, AST_RQ_CLASS });
            }
            else if (node_type == AST_ENUMDECL)
            {
                // Try to use definition instead of declaration
                auto nd2 = nd->get_enumdef();
                if (nd2)
                {
                    // Is the full scope to be searched?
                    if (search_definition_in_full_classes)
                        return ast_resolved_entity({ nd2, AST_RQ_ENUM });
                    // Is the enumdef before place of lookup?
                    ast_node_backref br = nd2->get_backref();
                    auto pidx = std::get<i64>(br);
                    if (pidx != -1 && pidx < pos_in_scope)
                        return ast_resolved_entity({ nd2, AST_RQ_ENUM });
                }
                // No enumdef seen before pos_in_scope
                return ast_resolved_entity({ nd, AST_RQ_ENUMDECL });
            }
            else if (node_type == AST_ENUMDEF)
                return ast_resolved_entity({ nd, AST_RQ_ENUM });
            else if (node_type == AST_ENUMVALUE) // cannot be in class
                return ast_resolved_entity({ nd, AST_RQ_ENUMVALUE });
            else if (node_type == AST_VARIABLE)
                return ast_resolved_entity({ nd, AST_RQ_VARIABLE });
            else if (node_type == AST_OVERLOADSET)
                return ast_resolved_entity({ nd, AST_RQ_FNNAME });
            else
                return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
        }

        // If not found in this class, perhaps in base classes?

        // Look up in all base classes
        auto expre = resolve_member_name_in_base_classes(scope,name,sloc);
        if (!expre)
            return expre;
        auto re = expre.value();
        if (std::get<ast_resolve_quality>(re) != AST_RQ_NOTFOUND)
            return re;      // Found the name in the base class

        // Not found
        return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
    }


    case AST_RQ_VARIABLE:
    case AST_RQ_FUNPAR:
    {
        ast_node_ptr typeref = basend->get_typeref();
        if (!typeref)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
        auto re = typeref->get_resolved_entity();
        auto typend = std::get<ast_node_ptr>(re);
        auto typequ = std::get<ast_resolve_quality>(re);
        if (!typend)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
        if (typequ == AST_RQ_CLASS)
            return resolve_member_name_in_class(basend,name,sloc);
        else if (typequ == AST_RQ_ENUM)
            return resolve_member_name_in_enum(basend,name,sloc);
        else
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
    }

    case AST_RQ_CLASS:
        return resolve_member_name_in_class(basend,name,sloc);

    case AST_RQ_ENUM:
        return resolve_member_name_in_enum(basend,name,sloc);

    case AST_RQ_CLASSDECL:
        return makeerr(err::EC_CANNOT_RESOLVE_MEMBER_OF_UNDEFINED_CLASS,sloc);

    case AST_RQ_ENUMDECL:
        return makeerr(err::EC_CANNOT_RESOLVE_MEMBER_OF_UNDEFINED_ENUM,sloc);

    case AST_RQ_ENUMVALUE:
        return makeerr(err::EC_CANNOT_RESOLVE_MEMBER_OF_ENUMVALUE,sloc);

    case AST_RQ_FNNAME:
        return makeerr(err::EC_CANNOT_RESOLVE_MEMBER_OF_FUNCTION,sloc);

    case AST_RQ_FUNCALL:
        return makeerr(err::EC_CANNOT_RESOLVE_MEMBER_OF_FUNCALL,sloc);

    }

    return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
}


/**********************************************************************/
/*                                                                    */
/*                        RESOLVE ENTITY LINK                         */
/*                                                                    */
/**********************************************************************/


// For direct entity links, a minimum resolution is required.
//
// First, we must check whether the linked entity is in the same
// tree as the linking node and is visible.
// As an exception, for enum values the enum definition is checked.
//
// Second, if the linked entity is a class definition and the
// linking node is within that class, we need to use the special
// return value AST_RQ_CLASSFROMDEF.
//
// Third, if the class definition is after the linking node, we
// need to return AST_RQ_CLASSDECL and not AST_RQ_NOTFOUND,
// since the code generator can always issue a declaration
// for the later defined class.


static
exp_resolved_entity             // entity found, or null
resolve_entity_link(
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    ast_node_ptr entity,        // Entity to resolve
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (pos_in_scope == -1)
        return makeerr(err::EC_RESOLVER_NO_POS_IN_SCOPE,sloc);
    if (!entity)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_IS_NULL,sloc);

    // Check entity type
    ast_node_type entity_type = entity->get_node_type();

    if (entity_type == AST_GLOBAL ||
        entity_type == AST_NAMESPACE)
        return ast_resolved_entity({ entity, AST_RQ_NAMESPACE });

    if (entity_type != AST_CLASSDECL &&
        entity_type != AST_CLASSDEF &&
        entity_type != AST_ENUMDECL &&
        entity_type != AST_ENUMDEF &&
        entity_type != AST_ENUMVALUE &&
        entity_type != AST_VARIABLE &&
        entity_type != AST_FUNPAR &&
        entity_type != AST_FUNRET &&
        entity_type != AST_OVERLOADSET &&
        entity_type != AST_FUNDECL &&
        entity_type != AST_FUNDEF)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_TYPE,sloc);

    // For enumvalues, replace entity by the enumdef
    // but keep entity_type;
    // thus, entity and entity_type do not match in that case
    auto saved_entity = entity;
    if (entity_type == AST_ENUMVALUE)
    {
        ast_node_backref br = entity->get_backref();
        entity = std::get<ast_node_ptr>(br);
        if (!entity)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
        if (entity->get_node_type() != AST_ENUMDEF)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    }

    // Entity parent scope and index
    ast_node_ptr entity_anchorednd = entity;
    if (entity_type == AST_FUNDEF)
    {
        auto fundecl = scope->get_fundecl_backref();
        if (fundecl)
            entity_anchorednd = fundecl;
    }
    if (!entity_anchorednd)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    ast_node_backref entitybr = entity_anchorednd->get_backref();
    auto entity_parent = std::get<ast_node_ptr>(entitybr);
    if (!entity_parent)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    auto entity_pidx = std::get<i64>(entitybr);

    // Entity global scope and index
    auto entity_global_br = entity->get_global_backref(); // use fundef itself
    auto entity_gpidx = std::get<i64>(entity_global_br);



    // Entity lookup works a lot like name lookup
    // except that the entity need not be found
    // by name.
    // Also, we do not replace the declaration with
    // the definition.


    bool search_full_classes = false;
    auto scope_type = scope->get_node_type();

    // Use pos_in_global only if scope is a namespace
    if (scope_type != AST_NAMESPACE &&
        scope_type != AST_GLOBAL)
        pos_in_global = -1;


    // Loop over the enclosing scopes up to the global namespace
    // This loop iterates over all nested namespaces; however,
    // when considering whether an entity is visible at the point
    // of lookup, the pos_in_global is used to check the visibility.
    for (;;)
    {
        // Is the place of lookup the entity?
        if (scope == entity)
        {
            // Entity must be a class definition
            cig_assert(entity_type == AST_CLASSDEF);

            return ast_resolved_entity({ entity, AST_RQ_CLASSFROMDEF });
        }

        // Is the entity in the same scope as the place of lookup?
        if (scope == entity_parent)
        {
            // Index comparisions are different inside namespace and outside
            bool scope_is_namespace = (scope_type == AST_NAMESPACE ||
                                       scope_type == AST_GLOBAL);

            bool entity_before_resolveloc =
                (scope_is_namespace ?
                 entity_gpidx <= pos_in_global :
                 entity_pidx <= pos_in_scope); // incorrect for special pis < 0
            bool entity_contains_resolveloc =
                (scope_is_namespace ?
                 entity_gpidx == pos_in_global :
                 entity_pidx == pos_in_scope);


            // Note that the special values of pos_in_scope < 0
            // never occur inside a namespace;
            // those are the pseudo-index values for init statements etc.

            // Special child of scope: Init statement
            if (pos_in_scope == AST_BR_INIT) // never in nmspc
            {
                // The init statement does not see variables that
                // it declared itself.
                // Not found here means not reachable
                return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
            }
            // Special child of scope: Iterator of for..of statement
            else if (pos_in_scope == AST_BR_ITERATOR)// never in nmspc
            {
                // The iterator sees only the variables in the init statement,
                // not the variables in the iterator itself.
                i64 n = scope->get_initstmt_variables();

                if (entity_pidx < n)
                {
                    if (entity_type != AST_VARIABLE)
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

                    return ast_resolved_entity({ entity, AST_RQ_VARIABLE });
                }
                // Not found here means not reachable
                return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
            }

            else if (

                // All other special children of scope see all variables
                pos_in_scope < 0 || // never in namespace

                // Regular children of scope see entities before them
                entity_before_resolveloc ||

                // Function body sees all entities in class
                (scope_type == AST_CLASSDEF &&
                 search_full_classes))
            {
                // Special child of scope: The entity must be a variable
                if (pos_in_scope < 0) // never in namespace
                {
                    // The entity must be a variable
                    if (entity_type != AST_VARIABLE)
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

                    return ast_resolved_entity({ entity, AST_RQ_VARIABLE });
                }

                // Classify the entity
                if (entity_type == AST_NAMESPACE) // never happens
                    return ast_resolved_entity({ entity, AST_RQ_NAMESPACE });
                else if (entity_type == AST_CLASSDECL)
                    return ast_resolved_entity({ entity, AST_RQ_CLASSDECL });
                else if (entity_type == AST_CLASSDEF)
                {
                    if (entity_contains_resolveloc)
                        return ast_resolved_entity(
                            { entity, AST_RQ_CLASSFROMDEF });
                    else
                        return ast_resolved_entity(
                            { entity, AST_RQ_CLASS });
                }
                else if (entity_type == AST_ENUMDECL)
                    return ast_resolved_entity({ entity, AST_RQ_ENUMDECL });
                else if (entity_type == AST_ENUMDEF)
                    return ast_resolved_entity({ entity, AST_RQ_ENUM });
                else if (entity_type == AST_ENUMVALUE)
                    // Return original enumvalue, not entity which is an enumdef
                    return ast_resolved_entity(
                        { saved_entity, AST_RQ_ENUMVALUE });
                else if (entity_type == AST_VARIABLE)
                    return ast_resolved_entity({ entity, AST_RQ_VARIABLE });
                else if (entity_type == AST_OVERLOADSET ||
                         entity_type == AST_FUNDECL ||
                         entity_type == AST_FUNDEF)
                    return ast_resolved_entity({ entity, AST_RQ_FNNAME });
                else
                    return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
            }

            // else not found in scope (declaration too late)
            else
            {
                // The code generator can insert an earlier declaration
                if (entity_type == AST_CLASSDECL ||
                    entity_type == AST_CLASSDEF)
                    return ast_resolved_entity({ entity, AST_RQ_CLASSDECL });
                else if (entity_type == AST_ENUMDECL ||
                         entity_type == AST_ENUMDEF)
                    return ast_resolved_entity({ entity, AST_RQ_ENUMDECL });
                else if (entity_type == AST_ENUMVALUE)
                    // Enum value is not reachable
                    return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
                else if (entity_type == AST_FUNDECL ||
                         entity_type == AST_FUNDEF ||
                         entity_type == AST_OVERLOADSET)
                    return ast_resolved_entity({ entity, AST_RQ_FNNAME });
                // Not found here means not reachable
                return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
            }
        }
        else if (scope_type == AST_FUNBODY)
        {
            // Look up in parameter list
            ast_node_ptr fundef = scope->get_lambda_backref();
            ast_node_ptr parlist = fundef ? fundef->get_funparlist() : nullptr;
            if (parlist)
            {
                if (parlist == entity_parent)
                {
                    if (entity_type != AST_FUNPAR)
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);

                    return ast_resolved_entity({ entity, AST_RQ_FUNPAR });
                }
            }
        }

        // If not found in this class, perhaps in base classes?
        if (scope_type == AST_CLASSDEF)
        {
            // Loop over the base classes
            for (ast_node_ptr classdef = get_base_class(scope);
                 classdef;
                 classdef = get_base_class(classdef))
            {
                if (classdef == entity_parent)
                {
                    // Classify the entity
                    if (entity_type == AST_CLASSDECL)
                        return ast_resolved_entity({ entity, AST_RQ_CLASSDECL});
                    else if (entity_type == AST_CLASSDEF)
                        return ast_resolved_entity({ entity, AST_RQ_CLASS });
                    else if (entity_type == AST_ENUMDECL)
                        return ast_resolved_entity({ entity, AST_RQ_ENUMDECL });
                    else if (entity_type == AST_ENUMDEF)
                        return ast_resolved_entity({ entity, AST_RQ_ENUM });
                    else if (entity_type == AST_ENUMVALUE)
                        // Return original enumvalue, not entity (an enumdef)
                        return ast_resolved_entity(
                            { saved_entity, AST_RQ_ENUMVALUE});
                    else if (entity_type == AST_VARIABLE)
                        return ast_resolved_entity({ entity, AST_RQ_VARIABLE });
                    else if (entity_type == AST_OVERLOADSET ||
                             entity_type == AST_FUNDECL ||
                             entity_type == AST_FUNDEF)
                        return ast_resolved_entity({ entity, AST_RQ_FNNAME });
                    else
                        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
                }
            }
        }

        // end of search
        if (scope_type == AST_GLOBAL)
            break;


        //
        // Proceed to the parent scope
        //

        // When coming from inside a function body, do some settings
        // to search the full classes
        // and to lookup entities from point-of-definition in namespaces.
        // When coming from the parameter list or return type list of
        // a function definition, search the full classes for
        // data type definitions.
        if (scope_type == AST_FUNPARLIST ||
            scope_type == AST_FUNRETLIST ||
            scope_type == AST_FUNBODY)
        {
            ast_node_ptr fundef = scope->get_lambda_backref();
            if (fundef &&
                fundef->get_node_type() == AST_FUNDEF)
            {
                // Set search in full classes when resolving entity links
                // inside the function body.
                if (scope_type == AST_FUNBODY)
                    search_full_classes = true;
                // Set the encapsulating namespace and position
                // when leaving the outermost class to resolve
                // in namespace at the place of function definition

                auto fundef_global_br = fundef->get_global_backref();
                auto fundef_gpidx = std::get<i64>(fundef_global_br);
                if (fundef_gpidx != -1)
                    pos_in_global = fundef_gpidx;
            }
        }

        // Get parent scope
        ast_node_ptr anchorednd = scope;
        if (scope_type == AST_FUNPARLIST ||
            scope_type == AST_FUNRETLIST ||
            scope_type == AST_FUNBODY)
        {
            anchorednd = scope->get_lambda_backref();
            if (!anchorednd)
            {
                anchorednd = scope->get_fundecl_backref();
                if (anchorednd)
                    anchorednd = anchorednd->get_lambda_backref();
            }
        }
        if (!anchorednd)
            return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
        ast_node_backref br = anchorednd->get_backref();
        auto parent = std::get<ast_node_ptr>(br);
        auto pidx = std::get<i64>(br);

        if (!parent)
            return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
        if (pidx == -1)
            return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);


        auto parent_scope_type = parent->get_node_type();

        // Remember pos_in_global if scope is a non-namespace in a namespace
        if (scope_type != AST_NAMESPACE &&
            scope_type != AST_GLOBAL &&
            (parent_scope_type == AST_NAMESPACE ||
             parent_scope_type == AST_GLOBAL))
        {
            auto global_br = anchorednd->get_global_backref();
            auto gpidx = std::get<i64>(global_br);

            // Do not override a pos_in_global that has already been set
            // from an AST_FUNBODY
            if (pos_in_global == -1)
                pos_in_global = gpidx;
        }


        // Proceed to the parent
        scope = std::move(parent);
        scope_type = parent_scope_type;
        pos_in_scope = pidx;
    }

    return ast_resolved_entity({ nullptr, AST_RQ_NOTFOUND });
}


static
exp_resolved_entity             // entity found, or null
resolve_member_link(
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    ast_resolved_entity parent_re, // Outer resolved entity
    ast_node_ptr entity,        // Entity to resolve
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (pos_in_scope == -1)
        return makeerr(err::EC_RESOLVER_NO_POS_IN_SCOPE,sloc);
    if (!entity)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_IS_NULL,sloc);

    // Check entity type
    ast_node_type entity_type = entity->get_node_type();

    if (entity_type == AST_GLOBAL ||
        entity_type == AST_NAMESPACE)
        return ast_resolved_entity({ entity, AST_RQ_NAMESPACE });

    if (entity_type != AST_CLASSDECL &&
        entity_type != AST_CLASSDEF &&
        entity_type != AST_ENUMDECL &&
        entity_type != AST_ENUMDEF &&
        entity_type != AST_ENUMVALUE &&
        entity_type != AST_VARIABLE &&
        entity_type != AST_FUNPAR &&
        entity_type != AST_FUNRET &&
        entity_type != AST_OVERLOADSET &&
        entity_type != AST_FUNDECL &&
        entity_type != AST_FUNDEF)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_TYPE,sloc);

    // For enumvalues, replace entity by the enumdef
    // but keep entity_type;
    // thus, entity and entity_type do not match in that case
    auto saved_entity = entity;
    if (entity_type == AST_ENUMVALUE)
    {
        ast_node_backref br = entity->get_backref();
        entity = std::get<ast_node_ptr>(br);
        if (!entity)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
        if (entity->get_node_type() != AST_ENUMDEF)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    }

    // Entity parent scope and index
    ast_node_ptr entity_anchorednd = entity;
    if (entity_type == AST_FUNDEF)
    {
        auto fundecl = scope->get_fundecl_backref();
        if (fundecl)
            entity_anchorednd = fundecl;
    }
    if (!entity_anchorednd)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    ast_node_backref entitybr = entity_anchorednd->get_backref();
    auto entity_parent = std::get<ast_node_ptr>(entitybr);
    if (!entity_parent)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);

    // Check parent relationship with outer resolved entity
    auto parent_re_nd = std::get<ast_node_ptr>(parent_re);
    auto parent_re_qu = std::get<ast_resolve_quality>(parent_re);
    if (parent_re_qu == AST_RQ_VARIABLE)
    {
        // If the parent is a variable, we need to use the data type instead
        auto typeref = parent_re_nd->get_typeref();
        if (!typeref)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_NOCHILD,sloc);
        auto typere = typeref->get_resolved_entity();
        auto typend = std::get<ast_node_ptr>(typere);
        auto typequ = std::get<ast_resolve_quality>(typere);
        if (typequ != AST_RQ_CLASS &&
            typequ != AST_RQ_ENUM)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_NOCHILD,sloc);
        parent_re_nd = typend;
        //.parent_re_qu = AST_RQ_CLASS not necessary
    }
    if (entity_type == AST_ENUMVALUE)
    {
        // The parent is actually in entity
        // If parent_re_nd is the parent, we need not check base classes
        if (entity == parent_re_nd)
            // Return original enumvalue, not entity which is an enumdef
            return ast_resolved_entity({ saved_entity, AST_RQ_ENUMVALUE});
    }
    if (entity_parent != parent_re_nd)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_NOCHILD,sloc);



    // If the parent is a namespace or a class just being defined,
    // repeat the whole lookup as in resolve_entity_link().
    if (parent_re_qu == AST_RQ_NAMESPACE ||
        parent_re_qu == AST_RQ_CLASSFROMDEF)
    {
        return resolve_entity_link(scope,pos_in_scope,pos_in_global,
                                   entity,sloc);
    }

    // Don't proceed if the parent was not well defined
    else if (parent_re_qu == AST_RQ_INVALID ||
             parent_re_qu == AST_RQ_NOTFOUND)
        return ast_resolved_entity({ nullptr, parent_re_qu });

    // All other situations
    if (entity_type == AST_CLASSDECL)
        return ast_resolved_entity({ entity, AST_RQ_CLASSDECL});
    else if (entity_type == AST_CLASSDEF)
        return ast_resolved_entity({ entity, AST_RQ_CLASS });
    else if (entity_type == AST_ENUMDECL)
        return ast_resolved_entity({ entity, AST_RQ_ENUMDECL });
    else if (entity_type == AST_ENUMDEF)
        return ast_resolved_entity({ entity, AST_RQ_ENUM });
    else if (entity_type == AST_ENUMVALUE)
        // Return original enumvalue, not entity which is an enumdef
        return ast_resolved_entity({ saved_entity, AST_RQ_ENUMVALUE});
    else if (entity_type == AST_VARIABLE)
        return ast_resolved_entity({ entity, AST_RQ_VARIABLE });
    else if (entity_type == AST_OVERLOADSET ||
             entity_type == AST_FUNDECL ||
             entity_type == AST_FUNDEF)
        return ast_resolved_entity({ entity, AST_RQ_FNNAME });
    else
        return makeerr(err::EC_RESOLVER_ALGORITHMIC,sloc);
}


/**********************************************************************/
/*                                                                    */
/*                          ACCESS TO ENTITY                          */
/*                                                                    */
/**********************************************************************/


// These functions determine access to the resolved entities.
//
// The program needs to create access to variables or class member
// functions.
//
// The resolver transforms each qualified name or entity link with
// member links into a chain of entity references.
// The chain may contain namespaces, types (classes or enums),
// variables, and function names. (If a function name is present,
// it must be the last element of the chain.)
//
// Access must be created starting from a certain point in the chain
// to its end. The starting point, the first entity that needs to
// have access created, is the first variable or function that is
// followed only by non-static variables or non-static functions,
// not by types or static variables or functions.



// The entity_access structure defines the possible access to the
// resolved entity in the following way:
//
// a) The starting point for the current link in the chain is
//    - the local stack memory (LOCAL_OBJECT)
//    - or the global variable memory (GLOBAL_OBJECT)
//    - or the current class object (CLASS_OBJECT)
//    - or an outer class object (CLASS_OBJECT with outer_level > 0)
//    - or an address in memory (MEMORY_OBJECT) from the previous
//      link in the chain.
//
// b) From the starting point, the offset must be added to arrive at
//    the entity or its pointer.
//
// c) Once the entity or its pointer has been found, a possible
//    dereferencing operation of a pointer leads to the entity.
//
// d) If the previous link in the chain required no dereferencing,
//    the current offset and the previous offset can be combined
//    into a single offset.
//    That is indicated by the offset_with_prev flag.
//    In that case the previous link chain's offsetting operation
//    can be ignored.

struct entity_access {
    using enum ast::ast_access_kind;

    ast_access_kind access_kind = NOT_ACCESSIBLE;
    i64 offset = -1;            // Offset in global/stackmem/object
    bool offset_with_prev = false; // Offset is combined with previous in chain
    i64 outer_level = -1;       // 0 for current class, 1 for outer, 2 ...

    ast_expression_info export_ei(ast_builtin_type bitype) const noexcept
    {
        ast_expression_info ei;
        ei.access_kind = access_kind;
        ei.offset = offset;
        ei.combines_sub = offset_with_prev;
        ei.outer_level = outer_level;
        ei.bitype = bitype;
        return ei;
    }
    void import_ei(const ast_expression_info &ei) noexcept {
        access_kind = ei.access_kind;
        offset = ei.offset;
        offset_with_prev = ei.combines_sub;
        outer_level = ei.outer_level;
    };
};


using exp_entity_access = std::expected<entity_access,cig::err>;

// Determine access
static
exp_entity_access
access_to_first_entity(
    ast_node_ptr scope,         // The scope wherein the access happens
    ast_node_ptr entity,        // First entity in chain to provide access to
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_access_kind;

    // This function should not be called iteratively,
    // but always from the point-of-lookup.
    // It does its own iteration through the enclosing scopes.

    // Paranoia checks
    if (!scope)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (!entity)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_IS_NULL,sloc);
    
    // Check entity type
    ast_node_type entity_type = entity->get_node_type();

    if (entity_type == AST_GLOBAL ||
        entity_type == AST_NAMESPACE ||
        entity_type == AST_CLASSDECL ||
        entity_type == AST_CLASSDEF ||
        entity_type == AST_ENUMDECL ||
        entity_type == AST_ENUMDEF)
        return entity_access({ NO_ACCESS_TYPE_OR_NMSP });

    if (entity_type == AST_ENUMVALUE) // no further checks required
        return entity_access({ ACCESS_CONSTANT });

    if (entity_type != AST_VARIABLE &&
        entity_type != AST_FUNPAR &&
        entity_type != AST_OVERLOADSET &&
        entity_type != AST_FUNDECL &&
        entity_type != AST_FUNDEF)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_NOT_VAR_OR_FUN,sloc);

    // If entity is a fundef to a previous (possibly in-class) fundecl,
    // use the fundecl to determine the class
    if (entity_type == AST_FUNDEF)
    {
        auto fundecl = entity->get_fundecl_backref();
        if (fundecl)
        {
            cig_assert(fundecl->get_node_type() == AST_FUNDECL);
            entity = fundecl;
            entity_type = AST_FUNDECL;
        }
    }

    // Get entity offset
    i64 entity_offset = -1;
    if (entity_type == AST_FUNPAR ||
        entity_type == AST_VARIABLE)
    {
        auto entity_vi = entity->get_variable_info();
        if (entity_vi.offset < 0)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_OFFSET_NOT_SET,sloc);
        entity_offset = entity_vi.offset;
    }

    // Entity parent scope
    ast_node_ptr entity_anchorednd = entity;
    if (entity_type == AST_FUNDEF)
    {
        auto fundecl = scope->get_fundecl_backref();
        if (fundecl)
            entity_anchorednd = fundecl;
    }
    if (!entity_anchorednd)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    ast_node_backref entitybr = entity_anchorednd->get_backref();
    auto entity_parent = std::get<ast_node_ptr>(entitybr);
    if (!entity_parent)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);

    // An entity on the global level is always accessible
    auto entity_global_br = entity_anchorednd->get_global_backref();
    auto entity_gpidx = std::get<i64>(entity_global_br);
    if (entity_gpidx != -1)
    {
        if (entity_type == AST_FUNPAR)
            return makeerr(err::EC_AST_FUNPAR_NOT_IN_FUNPARLIST,sloc);
        else if (entity_type == AST_VARIABLE)
            return entity_access({ GLOBAL_OBJECT, entity_offset });
        else
            return entity_access({ GLOBAL_FUNCTION });
    }


    // Access determination works very much like the access rules in C++,
    // except for the possibility in CIG to access outer classes.
    //
    // All entities are looked up from the position where they are
    // referenced (as qualified names or entitylinks).
    // That is the position in which the scopes are nested;
    // scope, pos_in_scope, and pos_in_global start from the lookup
    // position.
    //
    // Since access is required from inside function bodies, the full
    // class scope is always accessible.
    // Within a class scope, if the entity cannot be found within the
    // class, it will be searched in the base classes of the class,
    // again using the full scope.


    int outer_level = 0;

    // Loop over the enclosing scopes up to the innermost namespace
    for (;;)
    {
        auto scope_type = scope->get_node_type();

        // If entity is directly in scope, we are done
        if (scope == entity_parent)
        {
            if (scope_type == AST_CLASSDEF)
            {
                if (entity_type == AST_FUNPAR)
                    return makeerr(err::EC_AST_FUNPAR_NOT_IN_FUNPARLIST,sloc);
                else if (entity_type == AST_VARIABLE)
                    return entity_access(
                        { CLASS_OBJECT, entity_offset, false, outer_level });
                else
                    return entity_access(
                        { CLASS_FUNCTION, -1, false, outer_level });
            }
            else
            {
                if (entity_type == AST_FUNPAR ||
                    entity_type == AST_VARIABLE)
                    return entity_access({ LOCAL_OBJECT, entity_offset });
                else
                    return entity_access({ LOCAL_OBJECT });
            }
        }

        // If not found in this class, perhaps in base classes?
        if (scope_type == AST_CLASSDEF)
        {
            // Loop over the base classes
            for (ast_node_ptr classdef = get_base_class(scope);
                 classdef;
                 classdef = get_base_class(classdef))
            {
                if (classdef == entity_parent)
                {
                    if (entity_type == AST_FUNPAR)
                        return makeerr(
                            err::EC_AST_FUNPAR_NOT_IN_FUNPARLIST,sloc);
                    else if (entity_type == AST_VARIABLE)
                        return entity_access(
                            { CLASS_OBJECT,
                              entity_offset, false, outer_level });
                    else
                        return entity_access(
                            { CLASS_FUNCTION, -1, false, outer_level });
                }
            }
            // if not found in base classes, continue
        }

        // end of search
        if (scope_type == AST_GLOBAL ||
            scope_type == AST_NAMESPACE)
            break;

        //
        // Proceed to the parent scope
        //

        // From function body, proceed to funparlist
        if (scope_type == AST_FUNBODY)
        {
            // Get the fundef or lambda
            ast_node_ptr lambdabase = scope->get_lambda_backref();
            if (!lambdabase)
                return makeerr(err::EC_AST_RESOLVE_NAME_BAD_BACKREF,sloc);
            auto parent = lambdabase->get_funparlist();
            if (!parent)
                return makeerr(err::EC_AST_LAMBDA_HAS_NO_FUNPARLIST,sloc);
            auto parent_type = parent->get_node_type();
            if (parent_type != AST_FUNPARLIST)
                return makeerr(err::EC_AST_LAMBDA_HAS_NO_FUNPARLIST,sloc);
            scope = parent;
        }

        // From funparlist or funretlist, proceed to enclosing class
        // or namespace
        else if (scope_type == AST_FUNPARLIST ||
                 scope_type == AST_FUNRETLIST)
        {
            ast_node_ptr lambdabase = scope->get_lambda_backref();
            if (lambdabase &&
                lambdabase->get_node_type() == AST_FUNDEF)
            {
                auto fundecl = scope->get_fundecl_backref();
                if (fundecl)
                    lambdabase = fundecl;
            }
                
            ast_node_backref br = lambdabase->get_backref();
            auto parent = std::get<ast_node_ptr>(br);
            if (!parent)
                return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
            scope = parent;
        }

        // From all other scopes, proceed to parent
        else
        {
            ast_node_backref br = scope->get_backref();
            auto parent = std::get<ast_node_ptr>(br);
            if (!parent)
                return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
            scope = parent;
        }
    }

    // We reached a namespace or the global namespace.
    // If the entity were accessible from there, that
    // would have been detected when checking the
    // entity_gpidx.
    // Therefore we can state that the entity is not accessible.
    return entity_access({ NOT_ACCESSIBLE });
}


static
exp_entity_access
access_to_member_entity(
    ast_node_ptr scope,         // The scope wherein the access happens
    ast_node_ptr object,        // The object type/var/par wherein the member is
    entity_access objaccess,    // Access information for parent object
    ast_node_ptr entity,        // Next entity in chain to provide access to
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_access_kind;

    if (!object)
        return makeerr(err::EC_AST_NODE_ARGUMENT_IS_NULL,sloc);
    if (!entity)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_IS_NULL,sloc);

    // Check entity type
    ast_node_type entity_type = entity->get_node_type();

    if (entity_type == AST_GLOBAL ||
        entity_type == AST_NAMESPACE ||
        entity_type == AST_CLASSDECL ||
        entity_type == AST_CLASSDEF ||
        entity_type == AST_ENUMDECL ||
        entity_type == AST_ENUMDEF)
        return entity_access({ NO_ACCESS_TYPE_OR_NMSP });

    if (entity_type == AST_ENUMVALUE) // no further checks required
        return entity_access({ ACCESS_CONSTANT });

    if (entity_type != AST_VARIABLE &&
        entity_type != AST_ENUMVALUE &&
        // entity_type != AST_FUNPAR && // not allowed here
        entity_type != AST_OVERLOADSET &&
        entity_type != AST_FUNDECL &&
        entity_type != AST_FUNDEF)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_NOT_VAR_OR_FUN,sloc);

    // Get entity offset
    i64 entity_offset = -1;
    if (// entity_type == AST_FUNPAR || not allowed here
        entity_type == AST_VARIABLE)
    {
        auto entity_vi = entity->get_variable_info();
        if (entity_vi.offset < 0)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_OFFSET_NOT_SET,sloc);
        entity_offset = entity_vi.offset;
    }

    // Entity parent scope
    ast_node_ptr entity_anchorednd = entity;
    if (entity_type == AST_FUNDEF)
    {
        auto fundecl = scope->get_fundecl_backref();
        if (fundecl)
            entity_anchorednd = fundecl;
    }
    if (!entity_anchorednd)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);
    ast_node_backref entitybr = entity_anchorednd->get_backref();
    auto entity_parent = std::get<ast_node_ptr>(entitybr);
    if (!entity_parent)
        return makeerr(err::EC_AST_RESOLVE_ENTITY_BAD_BACKREF,sloc);

    // Check starting object or variable
    ast_node_type object_type = object->get_node_type();
    if (object_type != AST_CLASSDEF &&
        object_type != AST_VARIABLE &&
        object_type != AST_FUNPAR)
        return makeerr(err::EC_AST_RESOLVE_MEMBER_EXPR_OF_NONOBJECT,sloc);
    

    // Get the object's type node
    if (object_type == AST_VARIABLE ||
        object_type == AST_FUNPAR)
    {
        auto typeref = object->get_typeref();
        if (!typeref)
            return makeerr(err::EC_AST_RESOLVE_MEMBER_EXPR_OF_NONOBJECT,sloc);
        object = typeref->get_ref();
        if (!object)
            return makeerr(err::EC_AST_RESOLVE_MEMBER_EXPR_OF_NONOBJECT,sloc);
        object_type = object->get_node_type();
        if (object_type == AST_ENUMDEF)
            return makeerr(err::EC_AST_RESOLVE_ENTITY_NOCHILD,sloc);
        if (object_type != AST_CLASSDEF)
            return makeerr(err::EC_AST_RESOLVE_MEMBER_EXPR_OF_NONOBJECT,sloc);
    }

    // Verify parent relationship and determine access
    cig_assert(object_type == AST_CLASSDEF);
    for (ast_node_ptr classdef = object;
         classdef;
         classdef = get_base_class(classdef))
    {
        if (classdef == entity_parent)
        {
            if (entity_type == AST_VARIABLE)
            {
                if (true)       // direct member, not a pointer
                {
                    // Combine offset with previous
                    entity_offset += objaccess.offset;
                    return entity_access(
                        { MEMORY_OBJECT,
                          entity_offset, true, objaccess.outer_level });
                }
                else
                {
                    return entity_access(
                        { MEMORY_OBJECT,
                          entity_offset, false, objaccess.outer_level });
                }
            }
            else
                return entity_access(
                    { CLASS_FUNCTION, -1, false, objaccess.outer_level });
        }
    }

    return makeerr(err::EC_AST_RESOLVE_ENTITY_NOCHILD,sloc);
}


/**********************************************************************/
/*                                                                    */
/*                   EXPRESSION AUXILIARY FUNCTIONS                   */
/*                                                                    */
/**********************************************************************/


// Get the expression info of a node in the expression tree.
// All nodes in the expression tree have an internal expression info
// except for variables. Variables are usually placed into the expression
// tree by reference (qualified name or entity link), which has an
// expression info.
// However, within a function body, variables are declared within
// expressions, so they can appear directly in the expression tree.
// In that case, we need to construct the expression info.
// The same is true if a function parameter or function return value
// is examined for its expression info.
static
ast_expression_info
get_ast_node_expression_info(
    ast_node_ptr nd             // variable, funpar, funret or expression node
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_access_kind;

    cig_assert(nd);
    ast_node_type node_type = nd->get_node_type();

    if (node_type == AST_VARIABLE)
    {
        // This function is only called when a variable is
        // examined as part of an expression tree.
        // Such variables are either local or static local (i.e. global).
        // We can deduce the type and address from the variable info.
        ast_variable_info vi = nd->get_variable_info();
        bool is_static = nd->is_static();
        cig_assert(vi.offset >= 0);
        cig_assert(vi.ti.bitype >= 0);

        ast_expression_info ei;
        ei.access_kind = is_static ? GLOBAL_OBJECT : LOCAL_OBJECT;
        ei.offset = vi.offset;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = vi.ti.bitype;
        return ei;
    }
    else if (node_type == AST_FUNPAR ||
             node_type == AST_FUNRET)
    {
        // This function is only called when a funpar/funret is
        // examined as part of an expression tree, perhaps
        // implicitely via a return statement.
        // Such funpar/funret values are always local.
        // We can deduce the type and address from the variable info.
        ast_variable_info vi = nd->get_variable_info();
        cig_assert(vi.offset >= 0);
        cig_assert(vi.ti.bitype >= 0);

        ast_expression_info ei;
        ei.access_kind = LOCAL_OBJECT;
        ei.offset = vi.offset;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = vi.ti.bitype;
        return ei;
    }
    else if (ast_node::ast_node_type_is_expr(node_type))
    {
        auto ei = nd->get_expression_info();
        cig_assert(ei.bitype >= 0);
        return ei;
    }
    else
    {
        // Algorithmic error
        cig_assert(false);
    }
}


// For an expression under nd, get the list length;
// if not a list, return -1
static
i64                             // list length or -1
ast_node_get_list_length(
    ast_node_ptr nd             // variable or expression node
    )
{
    using enum ast::ast_node_type;

    // Note that lists cannot be combined with most operations;
    // the only operation to combine lists is the ternary condition
    // expression.
    // Since conditional expressions must match the lists exactly
    // in each element, we can simply follow the leftmost branch
    // through nested conditionals until we arrive at the list.
    // The list is either an explicit list or a function return
    // value list.
    for (;;)
    {
        if (!nd)
            return -1;
        ast_node_type node_type = nd->get_node_type();
        if (node_type == AST_EXPR_CONDITION)
        {
            // Take the left branch
            nd = nd->get_subexpr2();
            continue;
        }
        if (node_type == AST_EXPR_LIST)
        {
            return nd->get_listsubexpr_count();
        }
        if (node_type == AST_EXPR_FUNCALL)
        {
            // While the subexpr1() of the funcall resolves
            // to the function name (overload set), the
            // funcall itself always resolves to the
            // exact fundef being called.
            auto fundef = nd->get_resolved_node();
            if (!fundef)
                return -1;
            if (fundef->get_node_type() != AST_FUNDEF)
                return -1;
            auto retlist = fundef->get_funretlist();
            if (!retlist ||
                retlist->get_node_type() != AST_FUNRETLIST)
                return -1;
            return retlist->get_subnode_count();
        }
        if (node_type == AST_FUNPARLIST)
        {
            return nd->get_subnode_count();
        }
        if (node_type == AST_FUNRETLIST)
        {
            return nd->get_subnode_count();
        }
        return -1;
    }
}


// For an expression under nd, get the ith list element's expression info
// if not a list, return an empty expression info
static
ast_expression_info
ast_node_get_list_element_ei(
    ast_node_ptr nd,            // variable or expression node
    i64 i                       // 0..length-1
    )
{
    using enum ast::ast_node_type;

    // Note that lists cannot be combined with most operations;
    // the only operation to combine lists is the ternary condition
    // expression.
    // Since conditional expressions must match the lists exactly
    // in each element, we can simply follow the leftmost branch
    // through nested conditionals until we arrive at the list.
    // The list is either an explicit list or a function return
    // value list.
    for (;;)
    {
        if (!nd)
            return ast_expression_info();
        ast_node_type node_type = nd->get_node_type();
        if (node_type == AST_EXPR_CONDITION)
        {
            // Take the left branch
            nd = nd->get_subexpr2();
            continue;
        }
        if (node_type == AST_EXPR_LIST)
        {
            if (i < 0 || i >= nd->get_listsubexpr_count())
                return ast_expression_info();
            auto sub = nd->get_listsubexpr(i);
            if (!sub)
                return ast_expression_info();
            return get_ast_node_expression_info(sub);
        }
        if (node_type == AST_EXPR_FUNCALL)
        {
            // While the subexpr1() of the funcall resolves
            // to the function name (overload set), the
            // funcall itself always resolves to the
            // exact fundef being called.
            auto fundef = nd->get_resolved_node();
            if (!fundef)
                return ast_expression_info();
            if (fundef->get_node_type() != AST_FUNDEF)
                return ast_expression_info();
            auto retlist = fundef->get_funretlist();
            if (!retlist ||
                retlist->get_node_type() != AST_FUNRETLIST)
                return ast_expression_info();
            if (i < 0 || i >= retlist->get_subnode_count())
                return ast_expression_info();
            auto funret = retlist->get_subnode(i);
            if (!funret)
                return ast_expression_info();
            return get_ast_node_expression_info(funret);
        }
        if (node_type == AST_FUNPARLIST)
        {
            if (i < 0 || i >= nd->get_subnode_count())
                return ast_expression_info();
            auto sub = nd->get_subnode(i);
            if (!sub)
                return ast_expression_info();
            return get_ast_node_expression_info(sub);
        }
        if (node_type == AST_FUNRETLIST)
        {
            if (i < 0 || i >= nd->get_subnode_count())
                return ast_expression_info();
            auto sub = nd->get_subnode(i);
            if (!sub)
                return ast_expression_info();
            return get_ast_node_expression_info(sub);
        }
        return ast_expression_info();
    }
}


// For an expression under nd, get the ith list element's source location
// if not a list, return an empty source location info
static
cig::source_location
ast_node_get_list_element_loc(
    ast_node_ptr nd,            // variable or expression node
    i64 i                       // 0..length-1
    )
{
    using enum ast::ast_node_type;

    // Note that lists cannot be combined with most operations;
    // the only operation to combine lists is the ternary condition
    // expression.
    // Since conditional expressions must match the lists exactly
    // in each element, we can simply follow the leftmost branch
    // through nested conditionals until we arrive at the list.
    // The list is either an explicit list or a function return
    // value list.
    for (;;)
    {
        if (!nd)
            return cig::source_location();
        ast_node_type node_type = nd->get_node_type();
        if (node_type == AST_EXPR_CONDITION)
        {
            // Take the left branch
            nd = nd->get_subexpr2();
            continue;
        }
        if (node_type == AST_EXPR_LIST)
        {
            if (i < 0 || i >= nd->get_listsubexpr_count())
                return cig::source_location();
            auto sub = nd->get_listsubexpr(i);
            if (!sub)
                return cig::source_location();
            return sub->get_effectloc();
        }
        if (node_type == AST_EXPR_FUNCALL)
        {
            // While the subexpr1() of the funcall resolves
            // to the function name (overload set), the
            // funcall itself always resolves to the
            // exact fundef being called.
            auto fundef = nd->get_resolved_node();
            if (!fundef)
                return cig::source_location();
            if (fundef->get_node_type() != AST_FUNDEF)
                return cig::source_location();
            auto retlist = fundef->get_funretlist();
            if (!retlist ||
                retlist->get_node_type() != AST_FUNRETLIST)
                return cig::source_location();
            if (i < 0 || i >= retlist->get_subnode_count())
                return cig::source_location();
            auto funret = retlist->get_subnode(i);
            if (!funret)
                return cig::source_location();
            return funret->get_effectloc();
        }
        if (node_type == AST_FUNPARLIST)
        {
            if (i < 0 || i >= nd->get_subnode_count())
                return cig::source_location();
            auto sub = nd->get_subnode(i);
            if (!sub)
                return cig::source_location();
            return sub->get_effectloc();
        }
        if (node_type == AST_FUNRETLIST)
        {
            if (i < 0 || i >= nd->get_subnode_count())
                return cig::source_location();
            auto sub = nd->get_subnode(i);
            if (!sub)
                return cig::source_location();
            return sub->get_effectloc();
        }
        return cig::source_location();
    }
}


static
cig_bool
ast_nodes_have_equal_value_type(
    ast_node_ptr a,
    ast_node_ptr b,
    cig::source_location *sloca,
    cig::source_location *slocb
    )
{
    if (!a || !b)
        return false;
    if (a->is_list())
    {
        if (!b->is_list())
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        i64 na = ast_node_get_list_length(a);
        i64 nb = ast_node_get_list_length(b);
        if (na != nb)
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        for (i64 i = 0; i < na; i += 1)
        {
            auto eia = ast_node_get_list_element_ei(a,i);
            auto eib = ast_node_get_list_element_ei(b,i);
            if (eia.bitype < 0 || eib.bitype < 0)
            {
                if (sloca)
                    *sloca = ast_node_get_list_element_loc(a,i);
                if (slocb)
                    *slocb = ast_node_get_list_element_loc(b,i);
                return false;
            }
            if (eia.bitype != eib.bitype)
            {
                if (sloca)
                    *sloca = ast_node_get_list_element_loc(a,i);
                if (slocb)
                    *slocb = ast_node_get_list_element_loc(b,i);
                return false;
            }
        }
        return true;
    }
    else
    {
        if (b->is_list())
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        auto eia = get_ast_node_expression_info(a);
        auto eib = get_ast_node_expression_info(b);
        if (eia.bitype < 0 || eib.bitype < 0)
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        if (eia.bitype != eib.bitype)
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        return true;
    }
}


static
cig_bool
ast_nodes_have_assignable_value_type(
    ast_node_ptr a,             // assigned to (lhs)
    ast_node_ptr b,             // assigned from (rhs)
    cig::source_location *sloca,
    cig::source_location *slocb
    )
{
    if (!a || !b)
        return false;
    if (a->is_list())
    {
        if (!b->is_list())
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        i64 na = ast_node_get_list_length(a);
        i64 nb = ast_node_get_list_length(b);
        if (na != nb)
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        for (i64 i = 0; i < na; i += 1)
        {
            auto eia = ast_node_get_list_element_ei(a,i);
            auto eib = ast_node_get_list_element_ei(b,i);
            if (!ast_builtin_type_is_builtin(eia.bitype) ||
                !ast_builtin_type_is_builtin(eib.bitype))
            {
                // Note: This also reports assignment of compounds
                // generally as an error.
                // This needs to be changed for compounds if both
                // tags are CIG_COMPOUND_TYPE and both types can
                // be assigned.
                if (sloca)
                    *sloca = ast_node_get_list_element_loc(a,i);
                if (slocb)
                    *slocb = ast_node_get_list_element_loc(b,i);
                return false;
            }
            auto idxa = eia.bitype;
            auto idxb = eib.bitype;
            if (!implicit_cast_possible(idxb,idxa))
            {
                if (sloca)
                    *sloca = ast_node_get_list_element_loc(a,i);
                if (slocb)
                    *slocb = ast_node_get_list_element_loc(b,i);
                return false;
            }
        }
        return true;
    }
    else
    {
        if (b->is_list())
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        auto eia = get_ast_node_expression_info(a);
        auto eib = get_ast_node_expression_info(b);
        if (!ast_builtin_type_is_builtin(eia.bitype) ||
            !ast_builtin_type_is_builtin(eib.bitype))
        {
            // Note: This also reports assignment of compounds
            // generally as an error.
            // This needs to be changed for compounds if both
            // tags are CIG_COMPOUND_TYPE and both types can
            // be assigned.
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        auto idxa = eia.bitype;
        auto idxb = eib.bitype;
        if (!implicit_cast_possible(idxb,idxa))
        {
            if (sloca)
                *sloca = a->get_effectloc();
            if (slocb)
                *slocb = b->get_effectloc();
            return false;
        }
        return true;
    }
}


static
ast_builtin_type                // bitype or AST_BUILTIN_INVALID
resolved_entity_get_object_type_tag(
    ast_node_ptr entity
    )
{
    using enum ast::ast_node_type;

    cig_assert(entity);

    ast_node_type entity_type = entity->get_node_type();

    if (entity_type == AST_VARIABLE ||
        entity_type == AST_FUNPAR ||
        entity_type == AST_FUNRET ||
        entity_type == AST_EXPR_LITERAL ||
        entity_type == AST_ENUMVALUE)
    {
        ast_variable_info vi = entity->get_variable_info();
        cig_assert(vi.ti.bitype >= 0);

        return vi.ti.bitype;
    }
    else
        return AST_BUILTIN_INVALID;
}


/**********************************************************************/
/*                                                                    */
/*                    FUNCTION OVERLOAD RESOLUTION                    */
/*                                                                    */
/**********************************************************************/


// A function argument in overload resolution
struct ovldres_funarg {
    ast_builtin_type bitype = AST_BUILTIN_INVALID;
    // For compound types, info about the class can be added here
    i64 best_match = 100;
};


// Resolve a function that is given by name (overload set)
// against the funcall arguments.
static
exp_ast_node_ptr                // fundef resolved or error
resolve_overloaded_function(
    ast_node_ptr overload_set,  // overload set
    ast_node_ptr argument_expr, // argument expression
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;

    ast_expression_info eione;
    cig::vector<ovldres_funarg> arg;

    if (argument_expr)
        eione = get_ast_node_expression_info(argument_expr);

    if (!overload_set ||
        overload_set->get_node_type() != AST_OVERLOADSET)
        return makeerr(err::EC_FUNCALL_NOT_FROM_OVERLOAD_SET,
                       sloc,
                       overload_set->get_effectloc());
    i64 novld = overload_set->get_overload_count();


    // Prepare the vector of function argument types
    i64 npar;
    if (!argument_expr)
    {
        // no arguments
    }
    else if (eione.bitype == AST_LIST_TYPE)
    {
        npar = ast_node_get_list_length(argument_expr);
        for (i64 i = 0; i < npar; i += 1)
        {
            auto ei = ast_node_get_list_element_ei(argument_expr,i);
            ovldres_funarg fa;
            if (!ast_builtin_type_is_builtin(ei.bitype) &&
                ei.bitype != AST_COMPOUND_TYPE)
                return makeerr(err::EC_FUNARG_NOT_BUILTIN_OR_COMPOUND,
                               ast_node_get_list_element_loc(argument_expr,i));
            fa.bitype = ei.bitype;
            if (fa.bitype == AST_COMPOUND_TYPE)
                return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,
                               ast_node_get_list_element_loc(argument_expr,i));
            arg.push_back(fa);
        }
    }
    else
    {
        npar = 1;
        ovldres_funarg fa;
        if (!ast_builtin_type_is_builtin(eione.bitype) &&
            eione.bitype != AST_COMPOUND_TYPE)
            return makeerr(err::EC_FUNARG_NOT_BUILTIN_OR_COMPOUND,
                           argument_expr->get_effectloc());
        fa.bitype = eione.bitype;
        if (fa.bitype == AST_COMPOUND_TYPE)
            return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,
                           argument_expr->get_effectloc());
        arg.push_back(fa);
    }

    cig_assert(arg.i64_size() == npar);


    // Next, prepare the list of best matches for each arg
    i64 best = -1;              // the best overload
    i64 alternative = -1;       // also the best overload
    bool found_match = false;
    for (i64 iovld = 0; iovld < novld; iovld += 1)
    {
        // Get overload
        ast_node_ptr fundef = overload_set->get_overload(iovld);
        if (!fundef)
            return makeerr(err::EC_AST_NULL_IN_OVERLOAD_LIST,
                           sloc);

        // Get definition for declaration
        if (fundef->get_node_type() == AST_FUNDECL)
        {
            ast_node_ptr fundecl = fundef;
            fundef = fundecl->get_fundef();
            if (!fundef)
                return makeerr(err::EC_FUNDECL_HAS_NO_FUNDEF,
                               sloc,
                               fundecl->get_effectloc());
        }

        // Get parameter list
        ast_node_ptr parlist = fundef->get_funparlist();
        if (!parlist)
            return makeerr(err::EC_AST_LAMBDA_HAS_NO_FUNPARLIST,
                           sloc,
                           fundef->get_effectloc());

        // Compare length of parameter list and argument list
        if (parlist->get_subnode_count() != npar)
            continue;

        // Get the formal function parameters into par
        cig::vector<ast_builtin_type> par;
        for (i64 ipar = 0; ipar < npar; ipar += 1)
        {
            ast_node_ptr funpar = parlist->get_subnode(ipar);
            if (!funpar)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               sloc,
                               parlist->get_effectloc());
            auto vi = funpar->get_variable_info();
            if (!ast_builtin_type_is_builtin(vi.ti.bitype) &&
                vi.ti.bitype != AST_COMPOUND_TYPE)
                return makeerr(err::EC_FUNPAR_NOT_BUILTIN_OR_COMPOUND,
                               sloc,
                               funpar->get_effectloc());
            ast_builtin_type bitype = vi.ti.bitype;
            par.push_back(bitype);
        }

        cig_assert(par.i64_size() == npar);


        // Compare parameters to arguments
        bool possible = true;
        for (i64 i = 0; i < npar; i += 1)
        {
            if (!implicit_cast_possible(arg[i].bitype,par[i]))
                possible = false;
        }

        // Calling the function is impossible
        if (!possible)
            continue;

        // This is a match; whether it is a best one remains to be seen
        found_match = true;

        // Set best match for each arg
        bool not_optimal = false;
        for (i64 i = 0; i < npar; i += 1)
        {
            i64 quality = 100;
            ast_builtin_type fromtype = arg[i].bitype;
            ast_builtin_type totype = par[i];

            if (fromtype == totype)
                quality = 1;
            else if (implicit_cast_possible_preserving_sign(fromtype,totype))
                quality = 2;
            else if (implicit_cast_possible_preserving_integral(fromtype,
                                                                totype))
                quality = 3;
            else // implicit_cast_possible already checked above
                quality = 4;

            // Is the match quality worse than the best so far?
            if (quality > arg[i].best_match)
                not_optimal = true;

            // Is the match quality better than the best so far?
            else if (quality < arg[i].best_match)
            {
                // The champion so far is no more
                best = -1;
                alternative = -1;

                // The new best match
                arg[i].best_match = quality;
            }
        }

        // If we have set the not_optimal flag, we can ignore this overload
        if (not_optimal)
            continue;

        // If we have not set the not_optimal flag, this is a new champion
        if (best == -1)
            best = iovld;
        else if (alternative == -1)
            alternative = iovld;
        // else we can safely discard it,
        // it is not even required for error messages
    }

    // Did we find no match?
    if (!found_match)
        return makeerr(err::EC_OVERLOAD_RESOLUTION_NO_MATCH,
                       sloc);

    // Did we find several matches differing in parameters
    if (best == -1)
        return makeerr(err::EC_OVERLOAD_RESOLUTION_NO_BEST_FOR_ALL_ARGS,
                       sloc);

    // Did we find several best alternatives
    if (alternative != -1)
        return makeerr(err::EC_OVERLOAD_RESOLUTION_AMBIGUOUS_BEST,
                       sloc);

    // Yes, we resolved that function call
    i64 iovld = best;

    // Get overload
    ast_node_ptr fundef = overload_set->get_overload(iovld);
    cig_assert(fundef);

    // Get definition for declaration
    if (fundef->get_node_type() == AST_FUNDECL)
    {
        ast_node_ptr fundecl = fundef;
        fundef = fundecl->get_fundef();
        cig_assert(fundef);
    }

    return fundef;
}


// If no overload resolution is done because the function was
// directly indicated via an entity link to the fundef,
// we must still be able to verify the parameters against the
// funcall arguments.
static
exp_ast_node_ptr                // fundef resolved or error
verify_funcall_fundef(
    ast_node_ptr fundef,        // function declaration or definition
    ast_node_ptr argument_expr, // argument expression
    cig::source_location sloc   // Location of name for error messages
    )
{
    using enum ast::ast_node_type;

    ast_expression_info eione;
    cig::vector<ovldres_funarg> arg;

    if (argument_expr)
        eione = get_ast_node_expression_info(argument_expr);

    if (!fundef ||
        (fundef->get_node_type() != AST_FUNDEF &&
         fundef->get_node_type() != AST_FUNDECL))
        return makeerr(err::EC_FUNCALL_ENTITYLINK_NOT_FUNDEF,
                       sloc,
                       fundef->get_effectloc());

    // Prepare the vector of function argument types
    i64 npar;
    if (!argument_expr)
    {
        // no arguments
    }
    else if (eione.bitype == AST_LIST_TYPE)
    {
        npar = ast_node_get_list_length(argument_expr);
        for (i64 i = 0; i < npar; i += 1)
        {
            auto ei = ast_node_get_list_element_ei(argument_expr,i);
            ovldres_funarg fa;
            if (!ast_builtin_type_is_builtin(ei.bitype) &&
                ei.bitype != AST_COMPOUND_TYPE)
                return makeerr(err::EC_FUNARG_NOT_BUILTIN_OR_COMPOUND,
                               ast_node_get_list_element_loc(argument_expr,i));
            fa.bitype = ei.bitype;
            if (fa.bitype == AST_COMPOUND_TYPE)
                return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,
                               ast_node_get_list_element_loc(argument_expr,i));
            arg.push_back(fa);
        }
    }
    else
    {
        npar = 1;
        ovldres_funarg fa;
        if (!ast_builtin_type_is_builtin(eione.bitype) &&
            eione.bitype != AST_COMPOUND_TYPE)
            return makeerr(err::EC_FUNARG_NOT_BUILTIN_OR_COMPOUND,
                           argument_expr->get_effectloc());
        fa.bitype = eione.bitype;
        if (fa.bitype == AST_COMPOUND_TYPE)
            return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,
                           argument_expr->get_effectloc());
        arg.push_back(fa);
    }

    cig_assert(arg.i64_size() == npar);


    // Get definition for declaration
    if (fundef->get_node_type() == AST_FUNDECL)
    {
        ast_node_ptr fundecl = fundef;
        fundef = fundecl->get_fundef();
        if (!fundef)
            return makeerr(err::EC_FUNDECL_HAS_NO_FUNDEF,
                           sloc,
                           fundecl->get_effectloc());
    }

    // Get parameter list
    ast_node_ptr parlist = fundef->get_funparlist();
    if (!parlist)
        return makeerr(err::EC_AST_LAMBDA_HAS_NO_FUNPARLIST,
                       sloc,
                       fundef->get_effectloc());

    // Compare length of parameter list and argument list
    if (parlist->get_subnode_count() != npar)
        return makeerr(err::EC_FUNCALL_ENTITYLINK_BAD_NPAR,
                       sloc,fundef->get_effectloc());

    // Get the formal function parameters into par
    cig::vector<ast_builtin_type> par;
    for (i64 ipar = 0; ipar < npar; ipar += 1)
    {
        ast_node_ptr funpar = parlist->get_subnode(ipar);
        if (!funpar)
            return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                           sloc,
                           parlist->get_effectloc());
        auto vi = funpar->get_variable_info();
        if (!ast_builtin_type_is_builtin(vi.ti.bitype) &&
            vi.ti.bitype != AST_COMPOUND_TYPE)
            return makeerr(err::EC_FUNPAR_NOT_BUILTIN_OR_COMPOUND,
                           sloc,
                           funpar->get_effectloc());
        ast_builtin_type bitype = vi.ti.bitype;
        if (bitype == AST_COMPOUND_TYPE)
            return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,
                           sloc,
                           funpar->get_effectloc());
        par.push_back(bitype);
    }

    cig_assert(par.i64_size() == npar);


    // Compare parameters to arguments
    bool possible = true;
    for (i64 i = 0; i < npar; i += 1)
    {
        if (!implicit_cast_possible(arg[i].bitype,par[i]))
            possible = false;
    }

    // Calling the function is impossible
    if (!possible)
        return makeerr(err::EC_FUNCALL_ENTITYLINK_BAD_PAR,
                       sloc,fundef->get_effectloc());

    return fundef;
}


/**********************************************************************/
/*                                                                    */
/*                   MISSING RETURN STATEMENT CHECK                   */
/*                                                                    */
/**********************************************************************/


static
exp_none
check_statement_for_missing_returns(
    ast_node_ptr stmt
    )
{
    using enum ast::ast_node_type;

    ast_node_type stmt_type = stmt->get_node_type();
    if (stmt_type == AST_COMPOUNDSTMT)
    {
        // Get subnode count
        i64 n = stmt->get_substmt_count();
        if (n < 1)
            return makeerr(err::EC_MISSING_RETURN_STMT,
                           stmt->get_endloc());
        ast_node_ptr substmt = stmt->get_substmt(n-1);
        if (!substmt)
            return makeerr(err::EC_MISSING_RETURN_STMT,
                           stmt->get_endloc());
        return check_statement_for_missing_returns(substmt);
    }
    if (stmt_type == AST_IFSTMT)
    {
        // Get then statement
        ast_node_ptr thenstmt = stmt->get_thenstmt();
        if (!thenstmt)
            return makeerr(err::EC_MISSING_RETURN_STMT,
                           stmt->get_endloc());
        auto expthen = check_statement_for_missing_returns(thenstmt);
        if (!expthen)
            return expthen;

        ast_node_ptr elsestmt = stmt->get_elsestmt();
        if (!elsestmt)
            return makeerr(err::EC_MISSING_RETURN_STMT,
                           stmt->get_endloc());
        auto expelse = check_statement_for_missing_returns(elsestmt);
        if (!expelse)
            return expelse;
        return exp_none();
    }
    if (stmt_type == AST_RETURNSTMT)
        return exp_none();

    // All loops must have a return statement after them
    return makeerr(err::EC_MISSING_RETURN_STMT,
                   stmt->get_endloc());
}


// Check a function definition for missing return statements
static
exp_none
check_missing_return_statements(
    ast_node_ptr fundef         // function definition
    )
{
    ast_node_ptr funretlist = fundef->get_funretlist();
    if (!funretlist)
        return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNRETLIST,
                       fundef->get_effectloc());
    i64 nret = funretlist->get_subnode_count();

    // No return values: No checks required
    if (nret <= 0)
        return exp_none();

    // Otherwise checks are required
    ast_node_ptr funbody = fundef->get_funbody();
    if (!funbody)
        return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNBODY,
                       fundef->get_effectloc());
    ast_node_ptr cmpstmt = funbody->get_substmt(0);
    if (!cmpstmt)
        return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNBODY,
                       fundef->get_effectloc());

    auto exp = check_statement_for_missing_returns(cmpstmt);
    if (!exp)
        return exp;

    return exp_none();
}


/**********************************************************************/
/*                                                                    */
/*                           MEMORY OFFSETS                           */
/*                                                                    */
/**********************************************************************/


struct memory_offsets {
public:
    i64 offset = 0;             // Current offset
    i64 offset_max = 0;         // Maximum offset
    i64 alignment = 0;          // Maximum alignment

    i64 add_variable(i64 sz,i64 al)
    {
        cig_assert(alignment >= 0);
        cig_assert(offset >= 0);

        // Alignment must be a power of 2
        cig_assert((al & (al - 1)) == 0);

        if (al > 0)
        {
            if ((offset % al) != 0)
            {
                offset -= offset % al;
                offset += al;
            }
            if (alignment < al)
                alignment = al;
        }
        i64 old_offset = offset;
        offset += sz;
        if (offset_max < offset)
            offset_max = offset;
        return old_offset;
    }

    void max_align_offset()
    {
        if (alignment > 0)
        {
            if ((offset % alignment) != 0)
            {
                offset -= offset % alignment;
                offset += alignment;
            }
            if (offset_max < offset)
                offset_max = offset;
        }
    }

    void reset_offset(i64 of)
    {
        offset = of;
    }
};


struct context_offsets {
    memory_offsets *globalconst = 0;
    memory_offsets *global = 0;
    memory_offsets *local = 0;
    ast_node_ptr fundef;
};



// Set the offset of a variable in its scope.
static
exp_none
adjust_variable_info(
    ast_variable_info &vi,      // Variable info with ti set, offset not yet
    memory_offsets &o,          // Current alignment/offset in containing ctxt
    cig::source_location sloc   // Location of name for error messages
    )
{

    i64 al = vi.ti.type_alignment;
    i64 sz = vi.ti.type_size;
    if (al < 0)
        return makeerr(err::EC_RESOLVER_BAD_TYPEINFO_ALIGNMENT,sloc);
    if (sz < 0)
        return makeerr(err::EC_RESOLVER_BAD_TYPEINFO_SIZE,sloc);

    vi.offset = o.add_variable(sz,al);
    return exp_none();
}


// Set the offset of an expression result in its scope.
static
exp_none
adjust_expression_info(
    ast_expression_info &ei,    // Expr. info with bitype set, offset not yet
    memory_offsets &o,          // Current alignment/offset in containing ctxt
    cig::source_location sloc   // Location of evaluation for error messages
    )
{
    if (!ast_builtin_type_is_builtin(ei.bitype) &&
        ei.bitype != AST_COMPOUND_TYPE)
        return makeerr(err::EC_EXPRESSION_NOT_BUILTIN_OR_COMPOUND,sloc);
    if (ei.bitype == AST_COMPOUND_TYPE)
        return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,sloc);

    ast_builtin_type bitype = ei.bitype;
    i64 al = builtin_type_alignment(bitype);
    i64 sz = builtin_type_size(bitype);
    if (al < 0)
        return makeerr(err::EC_RESOLVER_BAD_TYPEINFO_ALIGNMENT,sloc);
    if (sz < 0)
        return makeerr(err::EC_RESOLVER_BAD_TYPEINFO_SIZE,sloc);

    ei.offset = o.add_variable(sz,al);
    return exp_none();
}


/**********************************************************************/
/*                                                                    */
/*                       RESOLVE FULL AST TREE                        */
/*                                                                    */
/**********************************************************************/


// Prototype
static
exp_none
resolve_subtree(
    context_offsets const &o,   // Function variable offsets, or 0
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    ast_node_ptr nd             // root node of subtree
    );


// Resolve a subtree in an expression: Omit resolving variables
static
exp_none
resolve_subexpr(
    context_offsets const &o,   // Function variable offsets, or 0
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    ast_node_ptr nd             // root node of subtree
    )
{
    using enum ast::ast_node_type;

    // Do not resolve variables, they have been resolved
    // in the enclosing compound statement or if,while,...
    if (!nd || nd->get_node_type() == AST_VARIABLE)
        return exp_none();
    return resolve_subtree(o,scope,pos_in_scope,pos_in_global,nd);
}


static
exp_none
resolve_subtree(
    context_offsets const &co,  // Function variable offsets, or 0
    ast_node_ptr scope,         // The scope wherein the lookup starts
    i64 pos_in_scope,           // Position of name usage in scope
    i64 pos_in_global,          // Position in global if scope is nmspc., or -1
    ast_node_ptr nd             // root node of subtree
    )
{
    using enum ast::ast_node_type;
    using enum ast::ast_resolve_quality;
    using enum ast::ast_access_kind;

    // Allow null as a subtree pointer
    if (!nd)
        return exp_none();

    ast_node_type node_type = nd->get_node_type();

    switch (node_type)
    {
    case AST_INVALID:
    case AST_MAX:
    case AST_EXPRMIN:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_STMTMAX:
        return makealgorithmic("Bad node type in resolve_subtree");

    case AST_GLOBAL:
    {
        // For the global namespace,
        // iterate over all namespace descendants anywhere
        // in the sequence in which they were defined.
        i64 n = nd->get_global_subnode_count();

        // Rely on the checks in global_add_builtins():
        // The first AST_BUILTIN_COUNT entries are the builtin types.
        for (i64 i = AST_BUILTIN_COUNT; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_global_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());
            auto sub_node_type = sub->get_node_type();
            // Skip namespaces to avoid duplicate resolving
            if (sub_node_type == AST_NAMESPACE)
                continue;

            // Resolve all other nodes
            ast::ast_node_backref br = sub->get_backref();
            i64 subpos = std::get<i64>(br);
            auto subscope = std::get<ast_node_ptr>(br);
            if (subscope == nullptr || subpos < 0)
                return makealgorithmic(
                    "Bad backreference for node in global subnode list");
            auto exp = resolve_subtree(co,subscope,subpos,i,sub);
            if (!exp)
                return exp;

            // Set global variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (!sub->is_static())
                    return makeerr(err::EC_NAMESPACE_VARIABLE_MUST_BE_STATIC,
                                   nd->get_effectloc());
                ast_variable_info vi = sub->get_variable_info();
                cig_assert(co.global);
                auto expvi = adjust_variable_info(vi,*co.global,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }
        return exp_none();
    }

    case AST_NAMESPACE:
    {
        // For the other namespaces,
        // iterate over all namespace children
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            ast::ast_node_backref global_br = sub->get_backref();
            i64 gpos = std::get<i64>(global_br);
            auto exp = resolve_subtree(co,nd,i,gpos,sub);
            if (!exp)
                return exp;

            // Set global variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (!sub->is_static())
                    return makeerr(err::EC_NAMESPACE_VARIABLE_MUST_BE_STATIC,
                                   nd->get_effectloc());
                ast_variable_info vi = sub->get_variable_info();
                cig_assert(co.global);
                auto expvi = adjust_variable_info(vi,*co.global,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }
        return exp_none();
    }

    case AST_CLASSDECL:
        // Nothing to resolve
        return exp_none();

    case AST_CLASSDEF:
    {
        memory_offsets class_offsets;

        cig_assert(co.local == nullptr);

        // Iterate over all class children
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set class-local and global variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                cig_assert(co.global);
                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,
                                                  sub->is_static() ?
                                                  *co.global :
                                                  class_offsets,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Determine the type tag
        ast_builtin_type bitype;
        if (true)
        {
            // Rely on the checks in global_add_builtins().
            ast::ast_node_backref br = nd->get_backref();
            ast_node_ptr parent = std::get<ast_node_ptr>(br);
            i64 idx = std::get<i64>(br);
            if (parent->get_node_type() == AST_GLOBAL &&
                idx < AST_BUILTIN_COUNT)
                bitype = static_cast<ast_builtin_type>(idx._v);
            else
                bitype = AST_COMPOUND_TYPE;
        }

        // Finish the classdef
        class_offsets.max_align_offset();
        ast_type_info ti;
        ti.type_alignment = class_offsets.alignment;
        ti.type_size = class_offsets.offset;
        ti.bitype = bitype;
        auto expsetti = nd->set_type_info(ti);
        if (!expsetti)
            return expsetti;
        
        return exp_none();
    }

    case AST_ENUMDECL:
        // Nothing to resolve
        return exp_none();

    case AST_ENUMDEF:
    {
        // Iterate over all enum children
        // in the sequence in which they were defined.
        i64 next_value = 0;
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Set the value if the user has not set it
            // fixme better implementation
            if (sub->get_literal().get_literal_type() == literal::LIT_UNDEFINED)
            {
                sub->set_literal(literal(next_value));
                next_value += 1;
            }

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // global-const offset already set
        }

        // Finish the enumdef

        // Note: This assumes a base type of i64
        ast_type_info ti;
        ti.type_alignment = i64::type_alignment;
        ti.type_size = i64::type_size;
        ti.bitype = AST_BUILTIN_I64; // fixme better implementation
        auto expsetti = nd->set_type_info(ti);
        if (!expsetti)
            return expsetti;
        
        return exp_none();
    }

    case AST_VARIABLE:
    {
        auto subnd = nd->get_typeref();
        if (subnd == nullptr)
            return makeerr(err::EC_AST_VARIABLE_HAS_NO_TYPEREF,
                           nd->get_effectloc());

        // Resolve the typeref
        auto exp = resolve_subtree(co,scope,pos_in_scope,pos_in_global,subnd);
        if (!exp)
            return exp;
        auto typere = subnd->get_resolved_entity();
        auto typend = std::get<ast_node_ptr>(typere);
        auto typequ = std::get<ast_resolve_quality>(typere);
        if (!typend ||
            typequ == AST_RQ_INVALID ||
            typequ == AST_RQ_NOTFOUND)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,nd->get_effectloc());

        // Check that the resolved entity is a type
        if (typequ != AST_RQ_CLASS &&
            typequ != AST_RQ_ENUM)
            return makeerr(err::EC_RESOLVED_ENTITY_NOT_A_TYPE,
                           nd->get_effectloc());

        // The variable information
        ast_variable_info vi;
        vi.ti = typend->get_type_info();
        if (vi.ti.type_alignment == -1)
            return makeerr(err::EC_RESOLVER_TYPE_NOT_COMPLETE,
                           subnd->get_effectloc());
        auto expvi = nd->set_variable_info(vi);
        if (!expvi)
            return expvi;
        // Leave the offset open; the caller should set that
        // (We could try to pass the correct context_offsets object
        // and compute the offset here; however, depending on the
        // context variables might have to be treated differently,
        // therefore we leave that to the caller.

        return exp_none();
    }

    case AST_OVERLOADSET:
        // All functions contained herein are resolved from their
        // enclosing namespaces
        return exp_none();

    case AST_FUNDECL:
    {
        cig_assert(co.local == nullptr);

        auto funretlist = nd->get_funretlist();
        if (!funretlist)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNRETLIST,
                           nd->get_effectloc());
        auto exp1 = resolve_subtree(co,nd,AST_BR_FUNRETLIST,-1,
                                    funretlist);
        if (!exp1)
            return exp1;
        auto funparlist = nd->get_funparlist();
        if (!funparlist)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNPARLIST,
                           nd->get_effectloc());
        auto exp2 = resolve_subtree(co,nd,AST_BR_FUNPARLIST,-1,
                                    funparlist);
        if (!exp2)
            return exp2;
        // No function body

        // Skip fundef, it is resolved from its position in its
        // definition-holding namespace
        return exp_none();
    }

    case AST_FUNDEF:
    {
        memory_offsets function_memory;
        context_offsets foffsets = co;
        foffsets.local = &function_memory;
        foffsets.fundef = nd;   // Pass down node for return statements

        cig_assert(co.local == nullptr);

        auto funretlist = nd->get_funretlist();
        if (!funretlist)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNRETLIST,
                           nd->get_effectloc());
        auto exp1 = resolve_subtree(foffsets,nd,AST_BR_FUNRETLIST,-1,
                                    funretlist);
        if (!exp1)
            return exp1;
        auto funparlist = nd->get_funparlist();
        if (!funparlist)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNPARLIST,
                           nd->get_effectloc());
        auto exp2 = resolve_subtree(foffsets,nd,AST_BR_FUNPARLIST,-1,
                                    funparlist);
        if (!exp2)
            return exp2;
        auto funbody = nd->get_funbody();
        if (!funbody)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNBODY,
                           nd->get_effectloc());
        auto exp3 = resolve_subtree(foffsets,nd,AST_BR_FUNBODY,-1,
                                    funbody);
        if (!exp3)
            return exp3;

        // Check for missing return statements
        auto expmissingret = check_missing_return_statements(nd);
        if (!expmissingret)
            return expmissingret;

        // no foffsets for the qualified name subtree
        auto exp4 = resolve_subtree(co,nd,AST_BR_QUNAME,-1,
                                    nd->get_fundef_qualified_name());
        if (!exp4)
            return exp4;

        ast_function_info fi;
        fi.runtime_data_alignment = function_memory.alignment;
        fi.runtime_data_size = function_memory.offset_max;
        auto expfi = nd->set_function_info(fi);
        if (!expfi)
            return expfi;

        return exp_none();
    }

    case AST_LAMBDA:
    {
        memory_offsets function_memory;
        context_offsets foffsets;
        foffsets.local = &function_memory;

        // cig_assert(co.local == nullptr); // lambda can be in regular funbody

        auto funretlist = nd->get_funretlist();
        if (!funretlist)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNRETLIST,
                           nd->get_effectloc());
        auto exp1 = resolve_subtree(foffsets,nd,AST_BR_FUNRETLIST,-1,
                                    funretlist);
        if (!exp1)
            return exp1;
        auto funparlist = nd->get_funparlist();
        if (!funparlist)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNPARLIST,
                           nd->get_effectloc());
        auto exp2 = resolve_subtree(foffsets,nd,AST_BR_FUNPARLIST,-1,
                                    funparlist);
        if (!exp2)
            return exp2;
        auto funbody = nd->get_funbody();
        if (!funbody)
            return makeerr(err::EC_AST_FUNCTION_HAS_NO_FUNBODY,
                           nd->get_effectloc());
        auto exp3 = resolve_subtree(foffsets,nd,AST_BR_FUNBODY,-1,
                                    funbody);
        if (!exp3)
            return exp3;

        // fixme set the function information for the lambda
        return exp_none();
    }

    case AST_TYPEREF:
    {
        auto subnd = nd->get_ref();
        if (!subnd)
            return makeerr(err::EC_AST_TYPEREF_HAS_NO_QUNAME_OR_LINK,
                           nd->get_effectloc());

        // Resolve the subtree
        auto exp = resolve_subtree(co,scope,pos_in_scope,pos_in_global,subnd);
        if (!exp)
            return exp;
        auto typere = subnd->get_resolved_entity();
        auto typend = std::get<ast_node_ptr>(typere);
        auto typequ = std::get<ast_resolve_quality>(typere);
        if (!typend ||
            typequ == AST_RQ_INVALID ||
            typequ == AST_RQ_NOTFOUND)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,nd->get_effectloc());

        // Check that the resolved entity is a type
        if (typequ != AST_RQ_CLASS &&
            typequ != AST_RQ_ENUM)
            return makeerr(err::EC_RESOLVED_ENTITY_NOT_A_TYPE,
                           nd->get_effectloc());
        auto expsetre = nd->set_resolved_entity(typere);
        if (!expsetre)
            return forwarderr(expsetre);

        return exp_none();
    }

    case AST_FUNPAR:
    {
        auto subnd = nd->get_typeref();
        if (subnd == nullptr)
            return makeerr(err::EC_AST_FUNPAR_HAS_NO_TYPEREF,
                           nd->get_effectloc());

        // Resolve the typeref
        auto exp = resolve_subtree(co,scope,pos_in_scope,pos_in_global,subnd);
        if (!exp)
            return exp;
        auto typere = subnd->get_resolved_entity();
        auto typend = std::get<ast_node_ptr>(typere);
        auto typequ = std::get<ast_resolve_quality>(typere);
        if (!typend ||
            typequ == AST_RQ_INVALID ||
            typequ == AST_RQ_NOTFOUND)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,nd->get_effectloc());

        // Check that the resolved entity is a type
        if (typequ != AST_RQ_CLASS &&
            typequ != AST_RQ_ENUM)
            return makeerr(err::EC_RESOLVED_ENTITY_NOT_A_TYPE,
                           nd->get_effectloc());

        // In function definition, co.local is nonnull; in declaration, null
        if (co.local)
        {
            // Set function-local variable offsets
            ast_variable_info vi;
            vi.ti = typend->get_type_info();
            if (vi.ti.type_alignment == -1)
                return makeerr(err::EC_RESOLVER_TYPE_NOT_COMPLETE,
                               subnd->get_effectloc());
            auto expvi = adjust_variable_info(vi,*co.local,nd->get_effectloc());
            if (!expvi)
                return expvi;
            auto expsetvi = nd->set_variable_info(vi);
            if (!expsetvi)
                return expsetvi;
        }

        return exp_none();
    }

    case AST_FUNPARLIST:
    {
        // Iterate over all funpars.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;
        }
        return exp_none();
    }

    case AST_FUNRET:
    {
        auto subnd = nd->get_typeref();
        if (subnd == nullptr)
            return makeerr(err::EC_AST_FUNRET_HAS_NO_TYPEREF,
                           nd->get_effectloc());

        // Resolve the typeref
        auto exp = resolve_subtree(co,scope,pos_in_scope,pos_in_global,subnd);
        if (!exp)
            return exp;
        auto typere = subnd->get_resolved_entity();
        auto typend = std::get<ast_node_ptr>(typere);
        auto typequ = std::get<ast_resolve_quality>(typere);
        if (!typend ||
            typequ == AST_RQ_INVALID ||
            typequ == AST_RQ_NOTFOUND)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,nd->get_effectloc());

        // Check that the resolved entity is a type
        if (typequ != AST_RQ_CLASS &&
            typequ != AST_RQ_ENUM)
            return makeerr(err::EC_RESOLVED_ENTITY_NOT_A_TYPE,
                           nd->get_effectloc());

        // In function definition, co.local is nonnull
        if (co.local)
        {
            // Set function-local variable offsets
            ast_variable_info vi;
            vi.ti = typend->get_type_info();
            if (vi.ti.type_alignment == -1)
                return makeerr(err::EC_RESOLVER_TYPE_NOT_COMPLETE,
                               subnd->get_effectloc());
            auto expvi = adjust_variable_info(vi,*co.local,nd->get_effectloc());
            if (!expvi)
                return expvi;
            auto expsetvi = nd->set_variable_info(vi);
            if (!expsetvi)
                return expsetvi;
        }

        return exp_none();
    }

    case AST_FUNRETLIST:
    {
        // Iterate over all funrets.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;
        }
        return exp_none();
    }

    case AST_FUNBODY:
    {
        // Iterate over the function body compound statement.
        i64 n = nd->get_subnode_count();
        if (n > 1)
            return makealgorithmic("AST node funbody has more than one child");
        ast_node_ptr sub = nd->get_subnode(0);
        if (!sub)
            return makeerr(err::EC_AST_FUNBODY_HAS_NO_COMPOUND_STMT,
                           nd->get_effectloc());

        // Function definitions and lambdas must have co.local set
        cig_assert(co.local != 0);

        // Resolve in the function body
        auto exp = resolve_subtree(co,nd,0,-1,sub);
        if (!exp)
            return exp;
        return exp_none();
    }

    case AST_EXPR_LITERAL:
    case AST_ENUMVALUE:
    {
        literal const & lit = nd->get_literal();
        literal::literal_type t = lit.get_literal_type();
        if (t == literal::LIT_UNDEFINED)
        {
            if (node_type == AST_EXPR_LITERAL)
                return makeerr(err::EC_AST_LITERAL_HAS_NO_VALUE,
                               nd->get_effectloc());
            else
                return makeerr(err::EC_AST_ENUMVALUE_HAS_NO_VALUE,
                               nd->get_effectloc());
        }
        exp_ast_builtin_type expbitype = literal_to_builtin_type(t);
        if (!expbitype)
            return forwarderr(expbitype);
        ast_builtin_type bitype = expbitype.value();
        ast_variable_info vi;
        vi.ti.type_alignment = builtin_type_alignment(bitype);
        vi.ti.type_size = builtin_type_size(bitype);
        vi.ti.bitype = bitype;

        // Set global-const variable offsets
        cig_assert(co.globalconst);
        auto expvi = adjust_variable_info(vi,*co.globalconst,
                                          nd->get_effectloc());
        if (!expvi)
            return expvi;
        auto expsetvi = nd->set_variable_info(vi);
        if (!expsetvi)
            return expsetvi;

        // Create the expression info for literals
        if (node_type == AST_EXPR_LITERAL &&
            co.local)
        {
            // Evaluating the literal means copying it onto the local memory
            // After that, it is a local object
            ast_expression_info ei;
            ei.access_kind = LOCAL_OBJECT;
            ei.combines_sub = false;
            ei.outer_level = 0;
            ei.bitype = bitype;

            auto expei = adjust_expression_info(ei,*co.local,
                                                nd->get_effectloc());

            auto expsetei = nd->set_expression_info(ei);
            if (!expsetei)
                return expsetei;
        }

        return exp_none();
    }

    case AST_EXPR_ENTITYLINK:
    {
        // Resolve the link
        ast_node_ptr link = nd->get_link();
        auto expre =
            resolve_entity_link( // checks link != 0
                scope,pos_in_scope,pos_in_global,
                link,
                nd->get_effectloc());
        // From here on parallel to AST_EXPR_UNQUALNAME
        if (!expre)
            return forwarderr(expre);
        auto re = std::move(expre.value());
        ast_node_ptr entity = std::get<ast_node_ptr>(re);
        if (!entity)
            return makeerr(err::EC_CANNOT_RESOLVE_NAME,nd->get_effectloc());
        auto expsetre = nd->set_resolved_entity(re);
        if (!expsetre)
            return forwarderr(expsetre);

        // Determine access
        auto expaccess = access_to_first_entity(scope,entity,
                                                nd->get_effectloc());
        if (!expaccess)
            return forwarderr(expaccess);
        auto access = std::move(expaccess.value());
        if (access.access_kind == NOT_ACCESSIBLE)
            return makeerr(err::EC_ENTITY_NOT_ACCESSIBLE,nd->get_effectloc());
        ast_builtin_type bitype = resolved_entity_get_object_type_tag(entity);
        ast_expression_info ei =  access.export_ei(bitype);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_UNQUALNAME:
    {
        // Resolve the name
        auto expre =
            resolve_unqualified_name_in_scope(
                scope,pos_in_scope,pos_in_global,
                nd->get_unqualified_name(),
                nd->get_effectloc());
        // From here on parallel to AST_EXPR_ENTITYLINK
        if (!expre)
            return forwarderr(expre);
        auto re = std::move(expre.value());
        ast_node_ptr entity = std::get<ast_node_ptr>(re);
        if (!entity)
            return makeerr(err::EC_CANNOT_RESOLVE_NAME,nd->get_effectloc());
        auto expsetre = nd->set_resolved_entity(re);
        if (!expsetre)
            return forwarderr(expsetre);

        // Determine access
        auto expaccess = access_to_first_entity(scope,entity,
                                                nd->get_effectloc());
        if (!expaccess)
            return forwarderr(expaccess);
        auto access = std::move(expaccess.value());
        if (access.access_kind == NOT_ACCESSIBLE)
            return makeerr(err::EC_ENTITY_NOT_ACCESSIBLE,nd->get_effectloc());
        ast_builtin_type bitype = resolved_entity_get_object_type_tag(entity);
        ast_expression_info ei =  access.export_ei(bitype);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_MEMBERLINK:
    {
        auto subnd = nd->get_subexpr1();

        // Resolve the subtree
        auto exp = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,subnd);
        if (!exp)
            return exp;
        auto basere = subnd->get_resolved_entity();
        auto basend = std::get<ast_node_ptr>(basere);
        auto basequ = std::get<ast_resolve_quality>(basere);
        if (!basend ||
            basequ == AST_RQ_INVALID ||
            basequ == AST_RQ_NOTFOUND)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,nd->get_effectloc());

        // Resolve the member link
        ast_node_ptr link = nd->get_link();
        auto expre =
            resolve_member_link( // checks link != 0
                scope,pos_in_scope,pos_in_global,
                basere,
                link,
                nd->get_effectloc());
        // From here on parallel to AST_EXPR_MEMBER
        if (!expre)
            return forwarderr(expre);
        auto re = std::move(expre.value());
        ast_node_ptr entity = std::get<ast_node_ptr>(re);
        if (!entity)
            return makeerr(err::EC_CANNOT_RESOLVE_NAME,nd->get_effectloc());
        auto expsetre = nd->set_resolved_entity(re);
        if (!expsetre)
            return forwarderr(expsetre);

        // Determine access
        auto baseei = subnd->get_expression_info();
        entity_access baseaccess;
        baseaccess.import_ei(baseei);
        auto expaccess = access_to_member_entity(scope,
                                                 basend,
                                                 baseaccess,
                                                 entity,
                                                 nd->get_effectloc());
        if (!expaccess)
            return forwarderr(expaccess);
        auto access = std::move(expaccess.value());
        if (access.access_kind == NOT_ACCESSIBLE)
            return makeerr(err::EC_ENTITY_NOT_ACCESSIBLE,nd->get_effectloc());
        ast_builtin_type bitype = resolved_entity_get_object_type_tag(entity);
        ast_expression_info ei =  access.export_ei(bitype);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_MEMBER:
    {
        auto subnd = nd->get_subexpr1();

        // Resolve the subtree
        auto exp = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,subnd);
        if (!exp)
            return exp;
        auto basere = subnd->get_resolved_entity();
        auto basend = std::get<ast_node_ptr>(basere);
        auto basequ = std::get<ast_resolve_quality>(basere);
        if (!basend ||
            basequ == AST_RQ_INVALID ||
            basequ == AST_RQ_NOTFOUND)
            return makeerr(err::EC_RESOLVER_ALGORITHMIC,nd->get_effectloc());

        // Resolve the named member
        auto expre =
            resolve_member_name(
                basend,basequ,
                scope,pos_in_scope,pos_in_global,
                nd->get_member_name(),
                nd->get_effectloc());
        // From here on parallel to AST_EXPR_MEMBERLINK
        if (!expre)
            return forwarderr(expre);
        auto re = std::move(expre.value());
        ast_node_ptr entity = std::get<ast_node_ptr>(re);
        if (!entity)
            return makeerr(err::EC_CANNOT_RESOLVE_NAME,nd->get_effectloc());
        auto expsetre = nd->set_resolved_entity(re);
        if (!expsetre)
            return forwarderr(expsetre);

        // Determine access
        auto baseei = subnd->get_expression_info();
        entity_access baseaccess;
        baseaccess.import_ei(baseei);
        auto expaccess = access_to_member_entity(scope,
                                                 basend,
                                                 baseaccess,
                                                 entity,
                                                 nd->get_effectloc());
        if (!expaccess)
            return forwarderr(expaccess);
        auto access = std::move(expaccess.value());
        if (access.access_kind == NOT_ACCESSIBLE)
            return makeerr(err::EC_ENTITY_NOT_ACCESSIBLE,nd->get_effectloc());
        ast_builtin_type bitype = resolved_entity_get_object_type_tag(entity);
        ast_expression_info ei =  access.export_ei(bitype);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_ARRAYDEREF:
    {
        // fixme check that subnodes are not null
        auto exp1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_subexpr1());
        if (!exp1)
            return exp1;
        // fixme check that subnode is a scalar array
        auto exp2 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_subexpr2());
        if (!exp2)
            return exp2;
        // fixme check that subnode is a scalar implicitly convertible to i64
        // fixme set type
        return exp_none();
    }

    case AST_EXPR_SLICE:
    {
        // fixme check that subnodes are not null but some may be
        auto exp1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_subexpr1());
        if (!exp1)
            return exp1;
        // fixme check that subnode is a scalar array
        auto exp2 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_subexpr2());
        if (!exp2)
            return exp2;
        auto exp3 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_subexpr3());
        if (!exp3)
            return exp3;
        auto exp4 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_subexpr4());
        if (!exp4)
            return exp4;
        // fixme check that subnodes are scalar implicitly convertible to i64
        // fixme set type
        return exp_none();
    }

    case AST_EXPR_FUNCALL:
    {
        // Resolve the function name (or function definition via entity link)
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_FUNCALL_HAS_NO_FUNCTION,
                           nd->get_effectloc());
        auto exp1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,subnd1);
        if (!exp1)
            return exp1;
        auto funre = subnd1->get_resolved_entity();
        auto funnd = std::get<ast_node_ptr>(funre);
        auto funqu = std::get<ast_resolve_quality>(funre);
        if (!funnd)
            return makeerr(err::EC_FUNCALL_NOT_A_FNNAME,
                           nd->get_effectloc());
        auto fun_type = funnd->get_node_type();
        ast_node_ptr fundef;

        // Resolve function arguments
        auto subnd2 = nd->get_subexpr2();
        // subnd2 may be nullptr
        auto exp2 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,subnd2);
        if (!exp2)
            return exp2;
        // The resolved data is retrieved by the overload resolution

        // Direct specification by entitylink can be a fundecl/fundef
        if (fun_type == AST_FUNDECL ||
            fun_type == AST_FUNDEF)
        {
            // A function definition can be set directly by entity reference.
            // In that case we should check the arguments here.
            // All arguments must be verified by implicit_cast_possible().

            // A funciton definition is also indicated by AST_RQ_FNNAME.
            if (funqu != AST_RQ_FNNAME)
                return makeerr(err::EC_FUNCALL_NOT_A_FNNAME,
                               nd->get_effectloc());

            // Verify the function definition's paramter list against arguments
            auto exp_fundef = verify_funcall_fundef(funnd,subnd2,
                                                   nd->get_effectloc());
            if (!exp_fundef)
                return forwarderr(exp_fundef);
            fundef = std::move(exp_fundef.value());
        }
        else
        {
            // Typically the function name is given, which points to an
            // overload set.
            // From there, we need to perform overload resolution.
            if (funnd->get_node_type() != AST_OVERLOADSET)
                return makeerr(err::EC_FUNCALL_NOT_A_FNNAME,
                               nd->get_effectloc(),
                               funnd->get_effectloc());
            if (funqu != AST_RQ_FNNAME)
                return makeerr(err::EC_FUNCALL_NOT_A_FNNAME,
                               nd->get_effectloc());

            auto exp_fundef = resolve_overloaded_function(funnd,subnd2,
                                                          nd->get_effectloc());
            if (!exp_fundef)
                return forwarderr(exp_fundef);
            fundef = std::move(exp_fundef.value());
        }

        ast_resolved_entity re = ast_resolved_entity(fundef,AST_RQ_FUNCALL);
        auto expsetre = nd->set_resolved_entity(re);
        if (!expsetre)
            return forwarderr(expsetre);

        ast_node_ptr retlist = fundef->get_funretlist();
        if (!retlist)
            return makeerr(err::EC_AST_LAMBDA_HAS_NO_FUNRETLIST,
                           nd->get_effectloc(),fundef->get_effectloc());
        i64 nret = retlist->get_subnode_count();

        ast_expression_info ei;
        if (nret == 0)
        {
            ei.access_kind = LOCAL_OBJECT;
            ei.offset = -1;
            ei.combines_sub = false;
            ei.outer_level = 0;
            ei.bitype = AST_VOID_TYPE; // Return list is empty
        }
        else if (nret != 1)
        {
            ei.access_kind = LOCAL_OBJECT;
            ei.offset = -1;     // fixme get proper offset and copy returns
            ei.combines_sub = false;
            ei.outer_level = 0;
            ei.bitype = AST_LIST_TYPE;
        }
        else
        {
            auto funret = retlist->get_subnode(0);
            if (!funret)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc(),
                               retlist->get_effectloc());
            auto vi = funret->get_variable_info();
            if (!ast_builtin_type_is_builtin(vi.ti.bitype) &&
                vi.ti.bitype != AST_COMPOUND_TYPE)
                return makeerr(err::EC_FUNRET_NOT_BUILTIN_OR_COMPOUND,
                               nd->get_effectloc(),
                               funret->get_effectloc());
            ast_builtin_type bitype = vi.ti.bitype;
            if (bitype == AST_COMPOUND_TYPE)
                return makeerr(err::EC_COMPOUND_TYPES_NOT_YET_HANDLED,
                               nd->get_effectloc(),
                               funret->get_effectloc());
            ei.access_kind = LOCAL_OBJECT;
            ei.offset = -1;     // fixme get proper offset and copy returns
            ei.combines_sub = false;
            ei.outer_level = 0;
            ei.bitype = bitype;
        }

        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
    {
        // Expression info of first subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_UNARY_HAS_NO_OPERAND,
                           nd->get_effectloc());
        auto expre1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd1);
        if (!expre1)
            return expre1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1) &&
            bitype1 != AST_LIST_TYPE)
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // If the subnode contains a funcall, it might have become a list
        if (bitype1 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                           nd->get_effectloc());

        // Check operation availability
        auto expidx = unary_expression_type(node_type,bitype1,
                                            nd->get_effectloc());
        if (!expidx)
            return forwarderr(expidx);
        ast_builtin_type bitype = expidx.value();

        ast_variable_info vi;
        vi.ti.type_alignment = builtin_type_alignment(bitype);
        vi.ti.type_size = builtin_type_size(bitype);
        vi.ti.bitype = bitype;
        cig_assert(co.local != 0);
        auto expvi = adjust_variable_info(vi,*co.local,nd->get_effectloc());
        if (!expvi)
            return expvi;

        ast_expression_info ei;
        ei.access_kind = LOCAL_OBJECT;
        ei.offset = vi.offset;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = bitype;

        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    {
        // Expression info of first subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_BINARY_HAS_NO_OPERAND1,
                           nd->get_effectloc());
        auto expre1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd1);
        if (!expre1)
            return expre1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1) &&
            bitype1 != AST_LIST_TYPE)
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // If the subnode contains a funcall, it might have become a list
        if (bitype1 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                           nd->get_effectloc());

        // Expression info of second subnode
        auto subnd2 = nd->get_subexpr2();
        if (subnd2 == nullptr)
            return makeerr(err::EC_AST_BINARY_HAS_NO_OPERAND2,
                           nd->get_effectloc());
        auto expre2 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd2);
        if (!expre2)
            return expre2;
        ast_expression_info subei2 = get_ast_node_expression_info(subnd2);

        // Builtin type of second subnode
        ast_builtin_type bitype2 = subei2.bitype;
        if (!ast_builtin_type_is_builtin(bitype2) &&
            bitype2 != AST_LIST_TYPE)
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd2->get_effectloc());

        // If the subnode contains a funcall, it might have become a list
        if (bitype2 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                           nd->get_effectloc());

        // Check operation availability
        auto expidx = binary_expression_type(node_type,bitype1,bitype2,
                                             nd->get_effectloc());
        if (!expidx)
            return forwarderr(expidx);
        ast_builtin_type bitype = expidx.value();

        ast_variable_info vi;
        vi.ti.type_alignment = builtin_type_alignment(bitype);
        vi.ti.type_size = builtin_type_size(bitype);
        vi.ti.bitype = bitype;
        cig_assert(co.local != 0);
        auto expvi = adjust_variable_info(vi,*co.local,nd->get_effectloc());
        if (!expvi)
            return expvi;

        ast_expression_info ei;
        ei.access_kind = LOCAL_OBJECT;
        ei.offset = vi.offset;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = bitype;

        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_CONDITION:
    {
        // Expression info of first subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_TERNARY_HAS_NO_CONDITION,
                           nd->get_effectloc());
        auto expre1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd1);
        if (!expre1)
            return expre1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1))
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // Expression info of second subnode
        auto subnd2 = nd->get_subexpr2();
        if (subnd2 == nullptr)
            return makeerr(err::EC_AST_TERNARY_HAS_NO_LEFT_ALT,
                           nd->get_effectloc());
        auto expre2 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd2);
        if (!expre2)
            return expre2;
        ast_expression_info subei2 = get_ast_node_expression_info(subnd2);

        // Builtin type of second subnode
        ast_builtin_type bitype2 = subei2.bitype;
        if (!ast_builtin_type_is_builtin(bitype2))
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd2->get_effectloc());

        // Expression info of third subnode
        auto subnd3 = nd->get_subexpr3();
        if (subnd3 == nullptr)
            return makeerr(err::EC_AST_TERNARY_HAS_NO_RIGHT_ALT,
                           nd->get_effectloc());
        auto expre3 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd3);
        if (!expre3)
            return expre3;
        ast_expression_info subei3 = get_ast_node_expression_info(subnd3);

        // Builtin type of third subnode
        ast_builtin_type bitype3 = subei3.bitype;
        if (!ast_builtin_type_is_builtin(bitype3))
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd3->get_effectloc());

        // Check that first subexpression is a boolean
        if (bitype1 != AST_BUILTIN_BOOL)
            return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                           nd->get_effectloc());

        // Check for void expression type (function return)
        if (bitype2 == AST_VOID_TYPE || bitype3 == AST_VOID_TYPE)
            return makeerr(err::EC_CONDITION_ALTERNATIVES_VOID,
                           nd->get_effectloc());

        // Check that the expression types on both alternatives match
        if (true)
        {
            cig::source_location sloca;
            cig::source_location slocb;
            if (!ast_nodes_have_equal_value_type(subnd2,subnd3,&sloca,&slocb))
                return makeerr(err::EC_CONDITION_ALTERNATIVES_DIFFER_TYPE,
                               nd->get_effectloc(),
                               sloca);
        }

        // Transfer a list property:
        // We do not have access to the internals of the AST node,
        // therefore we cannot set the X_HAS_LISTVALUE property.
        // But we can set the bitype in the expression info.

        ast_builtin_type bitype = bitype2;


        // Transfer the value type
        if (bitype == AST_LIST_TYPE)
        {
            ast_expression_info ei;
            ei.access_kind = LOCAL_OBJECT;
            ei.offset = -1;
            ei.combines_sub = false;
            ei.outer_level = -1;
            ei.bitype = AST_LIST_TYPE;

            auto expsetei = nd->set_expression_info(ei);
            if (!expsetei)
                return expsetei;
        }
        else
        {
            ast_variable_info vi;
            vi.ti.type_alignment = builtin_type_alignment(bitype);
            vi.ti.type_size = builtin_type_size(bitype);
            vi.ti.bitype = bitype;
            cig_assert(co.local != 0);
            auto expvi = adjust_variable_info(vi,*co.local,nd->get_effectloc());
            if (!expvi)
                return expvi;

            ast_expression_info ei;
            ei.access_kind = LOCAL_OBJECT;
            ei.offset = vi.offset;
            ei.combines_sub = false;
            ei.outer_level = 0;
            ei.bitype = bitype;

            auto expsetei = nd->set_expression_info(ei);
            if (!expsetei)
                return expsetei;
        }
        break;
    }

    case AST_EXPR_LIST:
    {
        // Iterate over all list elements.
        i64 n = nd->get_listsubexpr_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_listsubexpr(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_LISTSUBEXPR_LIST,
                               nd->get_effectloc());

            // Resolve the list element nodes
            auto exp = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                       sub);
            if (!exp)
                return exp;
            ast_expression_info subei = get_ast_node_expression_info(sub);

            // Builtin type of subnode
            ast_builtin_type bitype = subei.bitype;
            if (!ast_builtin_type_is_builtin(bitype) &&
                bitype != AST_LIST_TYPE &&
                bitype != AST_COMPOUND_TYPE)
                return makeerr(err::EC_LISTEL_NOT_BUILTIN_OR_CLASS_TYPE,
                               nd->get_effectloc(),
                               sub->get_effectloc());

            // If the subnode contains a funcall, it might have become a list
            if (bitype == AST_LIST_TYPE)
                return makeerr(err::EC_LISTS_IN_LISTS_NOT_ALLOWED,
                               sub->get_effectloc(),
                               nd->get_startloc());

            // We do not use the type tag otherwise
        }

        // Set the type tag to LIST
        ast_expression_info ei;
        ei.access_kind = LOCAL_OBJECT;
        ei.offset = -1;
        ei.combines_sub = false;
        ei.outer_level = -1;
        ei.bitype = AST_LIST_TYPE;

        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
    {
        // Expression info of first subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_ASSIGN_HAS_NO_LHS,
                           nd->get_effectloc());
        auto expre1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd1);
        if (!expre1)
            return expre1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1) &&
            bitype1 != AST_LIST_TYPE) // fixme compound
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // Expression info of second subnode
        auto subnd2 = nd->get_subexpr2();
        if (subnd2 == nullptr)
            return makeerr(err::EC_AST_ASSIGN_HAS_NO_RHS,
                           nd->get_effectloc());
        auto expre2 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                      subnd2);
        if (!expre2)
            return expre2;
        ast_expression_info subei2 = get_ast_node_expression_info(subnd2);

        // Builtin type of second subnode
        ast_builtin_type bitype2 = subei2.bitype;
        if (!ast_builtin_type_is_builtin(bitype2) &&
            bitype2 != AST_LIST_TYPE) // fixme compound
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd2->get_effectloc());

        // List assignment
        if (bitype1 == AST_LIST_TYPE ||
            bitype2 == AST_LIST_TYPE)
        {
            if (node_type != AST_EXPR_DOT_ASSIGN &&
                node_type != AST_EXPR_ASSIGN)
                return makeerr(err::EC_LIST_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                               nd->get_effectloc());
            if (bitype1 != AST_LIST_TYPE)
                return makeerr(err::EC_CANNOT_ASSIGN_LIST_TO_SCALAR,
                               nd->get_effectloc());
            if (bitype2 != AST_LIST_TYPE)
                return makeerr(err::EC_CANNOT_ASSIGN_SCALAR_TO_LIST,
                               nd->get_effectloc());

            // Compare the lists element by element
            cig::source_location sloca;
            cig::source_location slocb;
            if (!ast_nodes_have_assignable_value_type(subnd1,subnd2,
                                                      &sloca,&slocb))
                return makeerr(err::EC_CANNOT_ASSIGN_FROM_DIFFERENT_TYPE,
                               sloca,slocb);
            // Note: This is based on implicit_cast_possible(), and is a bit
            // inconsistent with the binary_expression_type based check below
        }

        // Scalar assignment
        else
        {
            // Compound types only allowed for simple assignment
            if ((bitype1 == AST_COMPOUND_TYPE ||
                 bitype2 == AST_COMPOUND_TYPE) &&
                node_type != AST_EXPR_DOT_ASSIGN &&
                node_type != AST_EXPR_ASSIGN)
                return makeerr(err::EC_COMPOUND_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                               nd->get_effectloc());

            // Check operation availability
            // Note: This is based on binary_expression_type(), and is a bit
            // inconsistent with the implicit_cast_possible() based check above

            // Note: This is a bit inconsistent with the 
            // based check above
            auto expidx = binary_expression_type(node_type,bitype1,bitype2,
                                                 nd->get_effectloc());
            if (!expidx)
                return forwarderr(expidx);
            ast_builtin_type bitype = expidx.value();

            // All assignments have an expression value of 'void'
            if (bitype != AST_VOID_TYPE)
            {
                if (node_type == AST_EXPR_DOT_ASSIGN ||
                    node_type == AST_EXPR_ASSIGN)
                    return makeerr(err::EC_CANNOT_ASSIGN_FROM_DIFFERENT_TYPE,
                                   subnd1->get_effectloc(),
                                   subnd2->get_effectloc());
                else
                    return makeerr(err::EC_ASSIGNMENT_RESULTTYPE_NOT_VOID,
                                   nd->get_effectloc());
            }
        }

        ast_expression_info ei;
        ei.access_kind = NOT_ACCESSIBLE;
        ei.offset = -1;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = AST_VOID_TYPE;

        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return expsetei;

        return exp_none();
    }

    case AST_COMPOUNDSTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Iterate over all substatements
        // in the sequence in which they were defined.
        i64 n = nd->get_substmt_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_substmt(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBSTMT_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set function-local and global variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                cig_assert(co.global);
                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,
                                                  sub->is_static() ?
                                                  *co.global :
                                                  *co.local,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_RETURNSTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        ast_node_ptr fundef = co.fundef;
        if (!fundef)
            return makealgorithmic("return statement outside fundef");
        ast_node_ptr funretlist = fundef->get_funretlist();
        if (!funretlist)
            return makealgorithmic("fundef has no funretlist");
        i64 nret = funretlist->get_subnode_count();
        if (nret < 0)
            return makealgorithmic("funretlist has negative funret count");


        // Note the return statement need not have an expression
        auto expr = nd->get_expr();
        if (nret == 0 && expr)
            return makeerr(err::EC_RETURNING_EXPRESSION_IN_VOID_FUNCTION,
                           nd->get_effectloc(),
                           funretlist->get_effectloc());
        if (nret > 0 && !expr)
            return makeerr(err::EC_RETURN_WITHOUT_VALUE_IN_NONVOID_FUNCTION,
                           nd->get_effectloc(),
                           funretlist->get_effectloc());
        if (expr)
        {
            auto exp1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                        expr);
            if (!exp1)
                return exp1;
            ast_expression_info exprei = get_ast_node_expression_info(expr);

            // Builtin type of return expression
            ast_builtin_type bitype = exprei.bitype;
            if (!ast_builtin_type_is_builtin(bitype) &&
                bitype != AST_LIST_TYPE) // fixme compound
                return makeerr(err::EC_RETURNVALUE_NOT_BUILTIN_TYPE,
                               nd->get_effectloc(),
                               expr->get_effectloc());

            if (nret > 1 ||
                bitype == AST_LIST_TYPE)
            {
                i64 nval = (bitype == AST_LIST_TYPE ?
                            ast_node_get_list_length(expr) : 1);

                if (nret == 1 && nval > 1)
                    return makeerr(err::EC_CANNOT_RETURN_LIST,
                                   expr->get_effectloc(),
                                   funretlist->get_effectloc());
                if (nret > 1 && nval == 1)
                    return makeerr(err::EC_NEED_TO_RETURN_LIST,
                                   expr->get_effectloc(),
                                   funretlist->get_effectloc());
                if (nret != nval)
                    return makeerr(err::EC_RETURN_LIST_HAS_BAD_LENGTH,
                                   expr->get_effectloc(),
                                   funretlist->get_effectloc());
                cig::source_location sloca;
                cig::source_location slocb;
                if (!ast_nodes_have_assignable_value_type(funretlist,expr,
                                                          &sloca,&slocb))
                    return makeerr(err::EC_CANNOT_RETURN_DIFFERENT_TYPE,
                                   slocb,sloca);
            }
            else
            {
                // Return value and type are both scalar
                cig::source_location sloca;
                cig::source_location slocb;
                cig_assert(nret == 1);
                ast_node_ptr funret = funretlist->get_subnode(0);
                if (!ast_nodes_have_assignable_value_type(funret,expr,
                                                          &sloca,&slocb))
                    return makeerr(err::EC_CANNOT_RETURN_DIFFERENT_TYPE,
                                   slocb,sloca);
            }
        }

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_EXPRSTMT:
    {
        // All variables defined by the expression statement
        // have already been created as separate variable nodes
        // before this expression statement.
        // Remember the offset and restore it after the expression statement.
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Note the expression statement need not have an expression
        auto exp1 = resolve_subexpr(co,scope,pos_in_scope,pos_in_global,
                                    nd->get_expr());
        if (!exp1)
            return exp1;
        // Nothing to do

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_IFSTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Iterate over all variable declarations
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set function-local variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (sub->is_static())
                    return makeerr(err::EC_STATIC_VARIABLE_NOT_ALLOWED_HERE,
                                   nd->get_effectloc());

                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,*co.local,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Resolve init statement
        // Note that the init statement need not be present
        auto expinit = resolve_subtree(co,nd,n,-1,nd->get_initstmt());
        if (!expinit)
            return expinit;

        // Expression info of condition expression
        auto cond = nd->get_condexpr();
        if (cond == nullptr)
            return makeerr(err::EC_AST_IFSTMT_HAS_NO_CONDITION,
                           nd->get_effectloc());
        auto expcond = resolve_subexpr(co,nd,n,-1,cond);
        if (!expcond)
            return expcond;
        ast_expression_info condei = get_ast_node_expression_info(cond);

        // Check that the condition is a bool-valued expression
        i64 cond_type_tag = condei.bitype;
        if (cond_type_tag != AST_BUILTIN_BOOL)
            return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                           cond->get_effectloc());

        // Resolve then statement
        auto thenstmt = nd->get_thenstmt();
        if (thenstmt == nullptr)
            return makeerr(err::EC_AST_IFSTMT_HAS_NO_THENSTMT,
                           nd->get_effectloc());
        auto expthen = resolve_subtree(co,nd,n,-1,thenstmt);
        if (!expthen)
            return expthen;

        // Resolve else statement
        // Note that the else statement need not be present
        auto expelse = resolve_subtree(co,nd,n,-1,nd->get_elsestmt());
        if (!expelse)
            return expelse;

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_WHILESTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Iterate over all substatements
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set function-local variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (sub->is_static())
                    return makeerr(err::EC_STATIC_VARIABLE_NOT_ALLOWED_HERE,
                                   nd->get_effectloc());

                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,*co.local,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Resolve init statement
        // Note that the init statement need not be present
        auto expinit = resolve_subtree(co,nd,n,-1,nd->get_initstmt());
        if (!expinit)
            return expinit;

        // Expression info of condition expression
        auto cond = nd->get_condexpr();
        if (cond == nullptr)
            return makeerr(err::EC_AST_WHILESTMT_HAS_NO_CONDITION,
                           nd->get_effectloc());
        auto expcond = resolve_subexpr(co,nd,n,-1,cond);
        if (!expcond)
            return expcond;
        ast_expression_info condei = get_ast_node_expression_info(cond);

        // Check that the condition is a bool-valued expression
        i64 cond_type_tag = condei.bitype;
        if (cond_type_tag != AST_BUILTIN_BOOL)
            return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                           cond->get_effectloc());

        // Resolve the body statement
        auto bodystmt = nd->get_bodystmt();
        if (bodystmt == nullptr)
            return makeerr(err::EC_AST_WHILESTMT_HAS_NO_BODY,
                           nd->get_effectloc());
        auto expbody = resolve_subtree(co,nd,n,-1,bodystmt);
        if (!expbody)
            return expbody;

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_DOWHILESTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Iterate over all substatements
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set function-local variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (sub->is_static())
                    return makeerr(err::EC_STATIC_VARIABLE_NOT_ALLOWED_HERE,
                                   nd->get_effectloc());

                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,*co.local,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Resolve init statement
        // Note that the init statement need not be present
        auto expinit = resolve_subtree(co,nd,n,-1,nd->get_initstmt());
        if (!expinit)
            return expinit;

        // Resolve the body statement
        auto bodystmt = nd->get_bodystmt();
        if (bodystmt == nullptr)
            return makeerr(err::EC_AST_DOWHILESTMT_HAS_NO_BODY,
                           nd->get_effectloc());
        auto expbody = resolve_subtree(co,nd,n,-1,bodystmt);
        if (!expbody)
            return expbody;

        // Expression info of condition expression
        auto cond = nd->get_condexpr();
        if (cond == nullptr)
            return makeerr(err::EC_AST_DOWHILESTMT_HAS_NO_CONDITION,
                           nd->get_effectloc());
        auto expcond = resolve_subexpr(co,nd,n,-1,cond);
        if (!expcond)
            return expcond;
        ast_expression_info condei = get_ast_node_expression_info(cond);

        // Check that the condition is a bool-valued expression
        i64 cond_type_tag = condei.bitype;
        if (cond_type_tag != AST_BUILTIN_BOOL)
            return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                           cond->get_effectloc());

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_FORSTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Iterate over all substatements
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all childx nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set function-local variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (sub->is_static())
                    return makeerr(err::EC_STATIC_VARIABLE_NOT_ALLOWED_HERE,
                                   nd->get_effectloc());

                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,*co.local,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Resolve init statement
        // Note that the init statement need not be present
        auto expinit = resolve_subtree(co,nd,n,-1,nd->get_initstmt());
        if (!expinit)
            return expinit;

        // Expression info of condition expression
        // Note that the condition need not be present to allow for (;;)
        auto cond = nd->get_condexpr();
        if (cond)
        {
            auto expcond = resolve_subexpr(co,nd,n,-1,cond);
            if (!expcond)
                return expcond;
            ast_expression_info condei = get_ast_node_expression_info(cond);

            // Check that the condition is a bool-valued expression
            i64 cond_type_tag = condei.bitype;
            if (cond_type_tag != AST_BUILTIN_BOOL)
                return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                               cond->get_effectloc());
        }

        // Resolve the iterate expression
        // Note that the iterate expr. need not be present to allow for (;;)
        auto iterateexpr = nd->get_iterateexpr();
        if (iterateexpr)
        {
            auto expiterate = resolve_subexpr(co,nd,n,-1,iterateexpr);
            if (!expiterate)
                return expiterate;
            // The iterate expression can have any expression type,
            // it is ignored
        }

        // Resolve the body statement
        auto bodystmt = nd->get_bodystmt();
        if (bodystmt == nullptr)
            return makeerr(err::EC_AST_DOWHILESTMT_HAS_NO_BODY,
                           nd->get_effectloc());
        auto expbody = resolve_subtree(co,nd,n,-1,bodystmt);
        if (!expbody)
            return expbody;

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_FOROFSTMT:
    {
        // Remember offset before this statement
        cig_assert(co.local);
        i64 offset = co.local->offset;

        // Iterate over all substatements
        // in the sequence in which they were defined.
        i64 n = nd->get_subnode_count();
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST,
                               nd->get_effectloc());

            // Resolve all child nodes
            auto exp = resolve_subtree(co,nd,i,-1,sub);
            if (!exp)
                return exp;

            // Set function-local variable offsets
            if (sub->get_node_type() == AST_VARIABLE)
            {
                if (sub->is_static())
                    return makeerr(err::EC_STATIC_VARIABLE_NOT_ALLOWED_HERE,
                                   nd->get_effectloc());

                ast_variable_info vi = sub->get_variable_info();
                auto expvi = adjust_variable_info(vi,*co.local,
                                                  sub->get_effectloc());
                if (!expvi)
                    return expvi;
                auto expsetvi = sub->set_variable_info(vi);
                if (!expsetvi)
                    return expsetvi;
            }
        }

        // Resolve init statement
        // Note that the init statement need not be present
        i64 ninit = nd->get_initstmt_variables();
        auto expinit = resolve_subtree(co,nd,ninit,-1,nd->get_initstmt());
        if (!expinit)
            return expinit;

        // Resolve the iterator expression
        auto iteratorexpr = nd->get_iteratorexpr();
        if (iteratorexpr == nullptr)
            return makeerr(err::EC_AST_FOROFSTMT_HAS_NO_ITERATOR,
                           nd->get_effectloc());
        auto expiterator = resolve_subexpr(co,nd,n,-1,iteratorexpr);
        if (!expiterator)
            return expiterator;
        [[maybe_unused]] ast_expression_info iteratorei =
            get_ast_node_expression_info(iteratorexpr);

        // Resolve the generator expression
        auto generatorexpr = nd->get_generatorexpr();
        if (generatorexpr == nullptr)
            return makeerr(err::EC_AST_FOROFSTMT_HAS_NO_GENERATOR,
                           nd->get_effectloc());
        auto expgenerator = resolve_subexpr(co,nd,n,-1,generatorexpr);
        if (!expgenerator)
            return expgenerator;
        [[maybe_unused]] ast_expression_info generatorei =
            get_ast_node_expression_info(generatorexpr);
        // fixme check this must be a generator or an array
        // if it is an array, we must construct a generator
        // fixme check congruency of the generator output and iteratorei
        // fixme the generator also delivers the loop-end indication

        // Resolve the body statement
        auto bodystmt = nd->get_bodystmt();
        if (bodystmt == nullptr)
            return makeerr(err::EC_AST_DOWHILESTMT_HAS_NO_BODY,
                           nd->get_effectloc());
        auto expbody = resolve_subtree(co,nd,n,-1,bodystmt);
        if (!expbody)
            return expbody;

        // Restore offset before this statement
        co.local->offset = offset;

        return exp_none();
    }

    case AST_BREAKSTMT:
        // Nothing to resolve
        return exp_none();

    case AST_CONTINUESTMT:
        // Nothing to resolve
        return exp_none();
    }

    return makealgorithmic("Unknown node type in resolve_subtree");
}


} // namespace detail

exp_none
resolve_ast_tree(ast_node_ptr ast_tree) noexcept
{
    using enum ast::ast_node_type;

    if (ast_tree == nullptr)
        return exp_none();

    if (ast_tree->get_node_type() != AST_GLOBAL)
        return makeerr(err::EC_RESOLVE_TREE_ROOT_NOT_GLOBAL,
                       ast_tree->get_effectloc());

    detail::memory_offsets globalconst;
    detail::memory_offsets global;
    detail::context_offsets co;
    co.globalconst = &globalconst;
    co.global = &global;
    co.local = nullptr;         // set for each function

    auto exp = detail::resolve_subtree(co,nullptr,-1,-1,ast_tree);
    if (!exp)
        return forwarderr(exp);

    ast_global_info gi;
    globalconst.max_align_offset();
    global.max_align_offset();
    gi.globalconst_size = globalconst.offset_max;
    gi.globalconst_alignment = globalconst.alignment;
    gi.globalvar_size = global.offset_max;
    gi.globalvar_alignment = global.alignment;
    auto exp_setglobal = ast_tree->set_global_info(gi);
    if (!exp_setglobal)
        return exp_setglobal;
    
    return exp_none();
}





} // namespace ast

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
