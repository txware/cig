/* cig_selftest.cpp
 *
 * Copyright 2022 Claus Fischer
 *
 * Selftest main program for cig.
 */
#include "cig_selftest_expensive.h"
#include "cig_selftest.h"
#include "cig_startup_main.h"

#include <fmt/format.h>

int cig::cig_main([[ maybe_unused ]] int argc,[[ maybe_unused ]] char *argv[])
{
    char const *selftest_error;

    /* Start with a selftest */
    selftest_error = cig::selftest_errorstring();
    if (selftest_error)
    {
	fmt::print(stderr,"Selftest failed:\n{}\n",selftest_error);
	return 1;
    }
    selftest_error = cig::expensive_selftest_errorstring();
    if (selftest_error)
    {
	fmt::print(stderr,"Expensive selftest failed:\n{}\n",selftest_error);
	return 1;
    }
    fmt::print("Selftest successful\n");
    return 0;
}
