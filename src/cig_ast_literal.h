/* cig_ast_literal.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Abstract Syntax Tree - Literals.
 *
 * Note: This file defines the data type to store literals encountered
 *       during parsing the CIG language. For literal operators to create
 *       literals from the C++ sources, see cig_literals.h.
 */
#ifndef CIG_AST_LITERAL_H_INCLUDED
#define CIG_AST_LITERAL_H_INCLUDED

#include "cig_err.h"
#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_vector.h"

#include <variant>              // std::variant,std::monostate

namespace cig {

namespace ast {

typedef std::variant<std::monostate,
                     str,bytes,
                     i64,i32,i16,i8,
                     u64,u32,u16,u8,
                     cig_bool> literal_base;

struct literal : literal_base
{
    enum literal_type {
        LIT_UNDEFINED = 0,
        LIT_STR = 1,
        LIT_BYTES = 2,
        LIT_I64 = 3,
        LIT_I32 = 4,
        LIT_I16 = 5,
        LIT_I8 = 6,
        LIT_U64 = 7,
        LIT_U32 = 8,
        LIT_U16 = 9,
        LIT_U8 = 10,
        LIT_BOOL = 11
    };

    constexpr literal_type get_literal_type() const noexcept
    {
        return static_cast<literal_type>(index());
    }

    // Constructors and assignment from literal
    literal() noexcept = default;
    literal(const literal &) = default;
    literal(literal &&) noexcept = default;
    literal & operator = (const literal &) = default;
    literal & operator = (literal &&) noexcept = default;

    // Constructors and assignment from literal_base
    literal(literal_base const &val) : literal_base(val) { }
    literal(literal_base &&val) : literal_base(std::move(val)) { }
    literal & operator = (literal_base const &val) {
        literal_base::operator = (val);
        return *this;
    }
    literal & operator = (literal_base &&val) {
        literal_base::operator = (std::move(val));
        return *this;
    }

    str to_str() const noexcept; // as in C++ source code
};

using exp_literal = std::expected<literal,err>;


} // namespace ast

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_AST_LITERAL_H_INCLUDED) */
