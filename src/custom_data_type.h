#pragma once
#include <string>
#include <fmt/format.h>

struct custom_data_type
{
    int x;
    std::string y;
};


template<>
struct fmt::formatter<custom_data_type>
{
public:
    constexpr auto parse (format_parse_context & ctx) {
        return ctx.end();
    }
    template <typename FmtContext>
    constexpr auto format(custom_data_type const & d, FmtContext& ctx) const
    {
        return format_to(ctx.out(), "[{}, {}]", d.x, d.y);
    }
};
