/* cig_interpreter_dump.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Interpreter Code Dumper.
 */
#include "cig_interpreter_dump.h"

#include "cig_constexpr_itoa.h"
#include "cig_str_proxy.h"
#include "cig_fmt_formatter.h"
#include "cig_toolbox.h"
#include "cig_interpreter.h"
#include "cig_interpreter_code.h"
#include "cig_str_proxy.h"


namespace cig {

namespace interpreter {


#define INSTRLEN 80
#define COMMENT_COLUMN 48       // Should be a multiple of 8

static
void
dump_instruction(fmt::ostream &out,
                 i64 iinstr,
                 instruction &instr)
{
    opcode op = instr.op;
    opstem stem = opcode_to_opstem(op);
    char ist[INSTRLEN];        // Instruction text to print out
    char arg[INSTRLEN];        // Instruction arguments to print out

    str name_lower = opcode_name_lower(op);
    auto name_lower_utf8 = cig::detail::utf8proxy(name_lower);
    

    // For labels, print a header line
    if (!instr.label.is_empty())
        out.print("        {}:\n",instr.label);


    // Print the main instruction onto itxt,
    // it will be printed out then
    arg[0] = '\0';
    if (op == OP_JMP ||
        op == OP_JMPBREAK ||
        op == OP_JMPCONTINUE ||
        op == OP_JMPRETURN ||
        op == OP_JMPEXCEPT ||
        op == OP_JMPANYUWFLAG ||
        op == OP_JMPNOUWFLAG)
        snprintf(arg,sizeof(arg),"%llu",
                 static_cast<unsigned long long>(instr.arg1._v));
    else if (op == OP_JMPTRUE ||
             op == OP_JMPFALSE)
        snprintf(arg,sizeof(arg),"<%llu> %llu",
                 static_cast<unsigned long long>(instr.arg1._v),
                 static_cast<unsigned long long>(instr.arg2._v));
    else if (stem == OPSTEM_DESTRUCT ||
             stem == OPSTEM_CONSTRUCT)
        snprintf(arg,sizeof(arg),"<%llu>",
                 static_cast<unsigned long long>(instr.arg1._v));

    else if (stem == OPSTEM_COPYLIT)
        snprintf(arg,sizeof(arg),"<c.%llu> <%llu>",
                 static_cast<unsigned long long>(instr.arg1._v),
                 static_cast<unsigned long long>(instr.arg2._v));

    // Combine the instruction and its arguments
    if (strlen(arg))
        snprintf(ist,sizeof(ist),"%6llu  %s %.*s",
                 static_cast<unsigned long long>(iinstr._v),
                 name_lower_utf8.c_str(),INSTRLEN-10,arg);
    else
        snprintf(ist,sizeof(ist),"%6llu  %s",
                 static_cast<unsigned long long>(iinstr._v),
                 name_lower_utf8.c_str());


    // Append blanks to comment
    if (!instr.comment.is_empty())
    {
        int j = static_cast<int>(std::strlen(ist));
        int i = j + 8;          // Count the prepended tab, see below
        int first = 1;          // Add at least one tab
        while (i < COMMENT_COLUMN || first)
        {
            ist[j++] = '\t';
            i += (8 - (i & 0x7));
            first = 0;
        }
        ist[j] = '\0';
    }

    // Print the main instruction
    out.print("\t{}",ist);     // The prepended tab, see above

    // Append the comment
    if (!instr.comment.is_empty())
    {
        out.print("# {}",instr.comment);
    }
    out.print("\n");
}


static
void
dump_code(fmt::ostream &out,
	  ipcode_ptr ipptr)
{
    ipcode const &ip = *ipptr;
    i64 ifn;
    i64 idt;

    for (idt = 0; idt < ip.dt.i64_size(); idt += 1)
    {
        const dtcode &dt = ip.dt[idt];

        out.print("Data type {} (dump_index {}) {}\n",
                  idt,
                  dt.dump_index,
                  dt.fqname);
        out.print("\tsize\t{}\n",dt.type_size);
        out.print("\talign\t{}\n",dt.type_alignment);
        out.print("\n");
    }

    for (ifn = 0; ifn < ip.fn.i64_size(); ifn += 1)
    {
	fncode &fn = *ip.fn[ifn];

	out.print("Function {} (dump_index {}) {}\n",
		  ifn,
		  fn.dump_index,
		  fn.fqname);
        out.print("\tretsize\t\t{}\n",fn.return_list_size);
        out.print("\tretalign\t{}\n",fn.return_list_alignment);
        out.print("\tvarsize\t\t{}\n",fn.retandpar_list_size);
        out.print("\tvaralign\t{}\n",fn.retandpar_list_alignment);
        out.print("\tsize\t\t{}\n",fn.function_size);
        out.print("\talign\t\t{}\n",fn.function_alignment);

        out.print("\n");
        out.print("\tINSTRUCTIONS:\n");

        // Dump the instructions
        if (fn.instr && fn.n_instr > 0)
        {
            instruction *instr = fn.instr;
            i64 n = fn.n_instr;

            for (i64 i = 0; i < n; i += 1)
                dump_instruction(out,i,instr[i._v]);
        }
        out.print("\n");
    }
}


void
dump_interpreter_code(str filename,ipcode_ptr ip)
{
    if (ip == nullptr)
	return;

    // Open the output file
    cig::detail::utf8proxy filename_utf8(filename);
    auto out = fmt::output_file(filename_utf8.c_str());

    dump_code(out,ip);
}




} // namespace interpreter

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
