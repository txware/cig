#include "custom_data_type.h"

#include <fmt/format.h>
#include <string_view>

#include "cig_print.h"
#include "cig_selftest.h"
#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_fmt_formatter.h"
#include "cig_constexpr_itoa.h"
#include "cig_startup_main.h"


int cig::cig_main([[ maybe_unused ]] int argc,[[ maybe_unused ]] char *argv[])
{
    using cig::operator"" _str;
    using cig::operator"" _i64;

    using namespace cig;


    /* Start with a selftest */
    char const *selftest_error = cig::selftest_errorstring();
    if (selftest_error)
    {
        fmt::print("selftest failed: {}\n",selftest_error);
        return 1;
    }

    fmt::print("{} version {}\n\n", PROJECT_NAME, PROJECT_VERSION);

    std::string_view name = "world";
    fmt::print("hello {}!\n", name);

    custom_data_type x{ 4711, "0815"};
    fmt::print("formatting my own custom data type: {}\n", x);

    str os = "Österreich"_str;
    fmt::print("Hello {}\n",os);

    cig::print("First a line to stdout"_str);
    cig::print("Second, a line to stderr ... "_str,
               stderr,cig::print::end(""_str));
    CIG_CONSTEXPR cig::str stringvariable = "stringvar"_str;
    cig::print("Third, a "_str, stringvariable, " with text around it"_str);

    constexpr i64 tendigits = 8001503897_i64;
    constexpr str tenchars = tendigits.to_str();

    // constexpr cig::str tenchars = 1234567890_i64 .to_str();
    cig::print("Then, a ten digit value: "_str,tenchars);

    return 0;
}


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
