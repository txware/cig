/* cig_ast_dump.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Abstract Syntax Tree - AST Tree Dumper
 */
#ifndef CIG_AST_DUMP_H_INCLUDED
#define CIG_AST_DUMP_H_INCLUDED

#include "cig_ast_node.h"

namespace cig {

namespace ast {

void
dump_ast_tree(str filename,ast_node_ptr ast_tree,bool dump_with_index=false);

void
ast_tree_set_dump_indices(ast_node_ptr ast_tree);

} // namespace ast

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_AST_DUMP_H_INCLUDED) */
