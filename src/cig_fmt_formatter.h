/* cig_fmt_formatter.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Formatter for str in C++ fmt library.
 */
#ifndef CIG_FMT_FORMATTER_H_INCLUDED
#define CIG_FMT_FORMATTER_H_INCLUDED

#include "cig_compatibility.h"  // CIG_RESTRICT,std::bit_cast
#include "cig_datatypes.h"
#include "cig_str_proxy.h"
#include "cig_literals.h"

#include <fmt/format.h>
#include <fmt/os.h>

template<>
struct fmt::formatter<cig::str> : formatter<string_view>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::str s, FormatContext& ctx) const
    {
        auto proxy = cig::detail::utf8proxy(s);
        string_view v(proxy.c_str(),proxy.size());
        return formatter<string_view>::format(v, ctx);
    }
};

template<>
struct fmt::formatter<cig::err> : formatter<string_view>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::err e, FormatContext& ctx) const
    {
        cig::str s = e.to_str();
        auto proxy = cig::detail::utf8proxy(s);
        string_view v(proxy.c_str(),proxy.size());
        return formatter<string_view>::format(v, ctx);
    }
};

template<>
struct fmt::formatter<cig::source_location> : formatter<string_view>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::source_location const &sloc, FormatContext& ctx) const
    {
        using namespace cig;
        char aux[100];
        cig::str s;
        if (sloc.file_name() != 0)
        {
            auto exp = cig::str::from_utf8(sloc.file_name());
            if (exp)
                s = exp.value() + ":"_str;
        }
        if (sloc.column())
            snprintf(aux,sizeof(aux),"%lu:%lu",
                     static_cast<unsigned long>(sloc.line()),
                     static_cast<unsigned long>(sloc.column()));
        else
            snprintf(aux,sizeof(aux),"%lu",
                     static_cast<unsigned long>(sloc.line()));
        s += str::from_ascii_c_str(aux);
        auto proxy = cig::detail::utf8proxy(s);
        string_view v(proxy.c_str(),proxy.size());
        return formatter<string_view>::format(v, ctx);
    }
};


template<>
struct fmt::formatter<cig::u8> : formatter<uint8_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::u8 i, FormatContext& ctx) const
    {
        return formatter<uint8_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::u16> : formatter<uint16_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::u16 i, FormatContext& ctx) const
    {
        return formatter<uint16_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::u32> : formatter<uint32_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::u32 i, FormatContext& ctx) const
    {
        return formatter<uint32_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::u64> : formatter<uint64_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::u64 i, FormatContext& ctx) const
    {
        return formatter<uint64_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::i8> : formatter<int8_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::i8 i, FormatContext& ctx) const
    {
        return formatter<int8_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::i16> : formatter<int16_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::i16 i, FormatContext& ctx) const
    {
        return formatter<int16_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::i32> : formatter<int32_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::i32 i, FormatContext& ctx) const
    {
        return formatter<int32_t>::format(i._v, ctx);
    }
};

template<>
struct fmt::formatter<cig::i64> : formatter<int64_t>
{
    // parse is inherited from formatter<string_view>

    template <typename FormatContext>
    auto format(cig::i64 i, FormatContext& ctx) const
    {
        return formatter<int64_t>::format(i._v, ctx);
    }
};



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_FMT_FORMATTER_H_INCLUDED) */
