/* cig_str_functions.cpp
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Runtime non-inline string functions.
 */
#include "cig_datatypes.h"

#include "cig_unicode_conversion.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_asciicharclass.h"


#include <cstdint>              // int64_t ...
#include <cstddef>              // std::size_t

namespace cig {

namespace detail {


//
// auxiliary
//


template<typename S1,typename S2>
static inline
bool
str_equal(
    S1 const * CIG_RESTRICT s1, // [i] input code points of first sequence
    S2 const * CIG_RESTRICT s2, // [i] input code points of second sequence
    int64_t n                   // [i] length of both strings
    )
{
    typedef typename std::make_unsigned<S1>::type US1;
    typedef typename std::make_unsigned<S2>::type US2;
    int64_t i;

    for (i = 0; i < n; i++)
        if (static_cast<US1>(s1[i]) != static_cast<US2>(s2[i]))
            return false;
    return true;
}


template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_sequence(S1 const * CIG_RESTRICT s1,
              S2 const * CIG_RESTRICT s2,
              int64_t sz1,      // length of s1
              int64_t sz2,      // length of s2
              i64 pos,          // start of s1 region
              i64 endpos)       // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos + sz2 > endpos)
        return -1;
    if (sz2 == 0)
        return pos;
    for (int64_t off = pos._v; off <= endpos._v - sz2; off++)
    {
        if (s1[off] == s2[0] && str_equal(s1+off,s2,sz2))
            return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
rfind_sequence(S1 const * CIG_RESTRICT s1,
               S2 const * CIG_RESTRICT s2,
               int64_t sz1,     // length of s1
               int64_t sz2,     // length of s2
               i64 pos,         // start of s1 region
               i64 endpos)      // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos + sz2 > endpos)
        return -1;
    if (sz2 == 0)
        return pos;
    for (int64_t off = endpos._v - sz2; off >= pos._v; off--)
    {
        if (s1[off] == s2[0] && str_equal(s1+off,s2,sz2))
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_char(S1 const * CIG_RESTRICT s1,
          int64_t sz1,          // length of s1
          i64 ch,               // character to find
          i64 pos,              // start of s1 region
          i64 endpos)           // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if (s1[off] == ch)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
rfind_char(S1 const * CIG_RESTRICT s1,
           int64_t sz1,         // length of s1
           i64 ch,              // character to find
           i64 pos,             // start of s1 region
           i64 endpos)          // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if (s1[off] == ch)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_notchar(S1 const * CIG_RESTRICT s1,
             int64_t sz1,       // length of s1
             i64 ch,            // character to find
             i64 pos,           // start of s1 region
             i64 endpos)        // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if (s1[off] != ch)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
rfind_notchar(S1 const * CIG_RESTRICT s1,
              int64_t sz1,      // length of s1
              i64 ch,           // character to find
              i64 pos,          // start of s1 region
              i64 endpos)       // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if (s1[off] != ch)
            return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_firstof_charlist(S1 const * CIG_RESTRICT s1,
                      S2 const * CIG_RESTRICT s2,
                      int64_t sz1, // length of s1
                      std::size_t sz2, // length of s2
                      i64 pos,     // start of s1 region
                      i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        for (std::size_t i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_lastof_charlist(S1 const * CIG_RESTRICT s1,
                     S2 const * CIG_RESTRICT s2,
                     int64_t sz1, // length of s1
                     std::size_t sz2, // length of s2
                     i64 pos,     // start of s1 region
                     i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        for (std::size_t i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_firstnotof_charlist(S1 const * CIG_RESTRICT s1,
                         S2 const * CIG_RESTRICT s2,
                         int64_t sz1, // length of s1
                         std::size_t sz2, // length of s2
                         i64 pos,     // start of s1 region
                         i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        std::size_t i;
        for (i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                break;
        if (i == sz2)
            return off;
    }
    return -1;
}

template<typename S1,typename S2>
static inline
i64                             // position found, or -1
find_lastnotof_charlist(S1 const * CIG_RESTRICT s1,
                        S2 const * CIG_RESTRICT s2,
                        int64_t sz1, // length of s1
                        std::size_t sz2, // length of s2
                        i64 pos,     // start of s1 region
                        i64 endpos)  // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        std::size_t i;
        for (i = 0; i < sz2; i++)
            if (s1[off] == s2[i])
                break;
        if (i == sz2)
            return off;
    }
    return -1;
}


//
// auxiliary for ascii character class
//


template<typename S1>
static inline
i64                             // position found, or -1
find_firstof_acc(S1 const * CIG_RESTRICT s1,
                 int64_t sz1,   // length of s1
                 i64 charclass, // ASCII char class flags, ored
                 i64 pos,       // start of s1 region
                 i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) != 0)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_lastof_acc(S1 const * CIG_RESTRICT s1,
                int64_t sz1,   // length of s1
                i64 charclass, // ASCII char class flags, ored
                i64 pos,       // start of s1 region
                i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) != 0)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_firstnotof_acc(S1 const * CIG_RESTRICT s1,
                    int64_t sz1,   // length of s1
                    i64 charclass, // ASCII char class flags, ored
                    i64 pos,       // start of s1 region
                    i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = pos._v; off < endpos; off++)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) == 0)
            return off;
    }
    return -1;
}

template<typename S1>
static inline
i64                             // position found, or -1
find_lastnotof_acc(S1 const * CIG_RESTRICT s1,
                   int64_t sz1,   // length of s1
                   i64 charclass, // ASCII char class flags, ored
                   i64 pos,       // start of s1 region
                   i64 endpos)    // end of s1 region
{
    if (pos < 0)
        pos = 0;
    if (endpos > sz1)
        endpos = sz1;
    if (pos >= endpos)
        return -1;
    for (int64_t off = endpos._v - 1; off >= pos._v; off--)
    {
        if ((codepoint_to_accmask(s1[off]) & charclass) == 0)
            return off;
    }
    return -1;
}


//
// find
//


i64 strval::find(strval const &sub,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_sequence(_data_ucs4(),sub._data_ucs4(),
                                         sz1,sz2,
                                         pos,sz1);
        case _chartype_ucs2:
            return detail::find_sequence(_data_ucs4(),sub._data_ucs2(),
                                         sz1,sz2,
                                         pos,sz1);
        default:
            return detail::find_sequence(_data_ucs4(),sub._data_ucs1(),
                                         sz1,sz2,
                                         pos,sz1);
        }
    case _chartype_ucs2:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_sequence(_data_ucs2(),sub._data_ucs4(),
                                         sz1,sz2,
                                         pos,sz1);
        case _chartype_ucs2:
            return detail::find_sequence(_data_ucs2(),sub._data_ucs2(),
                                         sz1,sz2,
                                         pos,sz1);
        default:
            return detail::find_sequence(_data_ucs2(),sub._data_ucs1(),
                                         sz1,sz2,
                                         pos,sz1);
        }
    default:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_sequence(_data_ucs1(),sub._data_ucs4(),
                                         sz1,sz2,
                                         pos,sz1);
        case _chartype_ucs2:
            return detail::find_sequence(_data_ucs1(),sub._data_ucs2(),
                                         sz1,sz2,
                                         pos,sz1);
        default:
            return detail::find_sequence(_data_ucs1(),sub._data_ucs1(),
                                         sz1,sz2,
                                         pos,sz1);
        }
    }
}

i64 strval::find(strval const &sub,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_sequence(_data_ucs4(),sub._data_ucs4(),
                                         sz1,sz2,
                                         pos,endpos);
        case _chartype_ucs2:
            return detail::find_sequence(_data_ucs4(),sub._data_ucs2(),
                                         sz1,sz2,
                                         pos,endpos);
        default:
            return detail::find_sequence(_data_ucs4(),sub._data_ucs1(),
                                         sz1,sz2,
                                         pos,endpos);
        }
    case _chartype_ucs2:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_sequence(_data_ucs2(),sub._data_ucs4(),
                                         sz1,sz2,
                                         pos,endpos);
        case _chartype_ucs2:
            return detail::find_sequence(_data_ucs2(),sub._data_ucs2(),
                                         sz1,sz2,
                                         pos,endpos);
        default:
            return detail::find_sequence(_data_ucs2(),sub._data_ucs1(),
                                         sz1,sz2,
                                         pos,endpos);
        }
    default:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_sequence(_data_ucs1(),sub._data_ucs4(),
                                         sz1,sz2,
                                         pos,endpos);
        case _chartype_ucs2:
            return detail::find_sequence(_data_ucs1(),sub._data_ucs2(),
                                         sz1,sz2,
                                         pos,endpos);
        default:
            return detail::find_sequence(_data_ucs1(),sub._data_ucs1(),
                                         sz1,sz2,
                                         pos,endpos);
        }
    }
}

i64 strval::find(i64 singlechar,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_char(_data_ucs4(),sz1,singlechar,
                                 pos,sz1);
    case _chartype_ucs2:
        return detail::find_char(_data_ucs2(),sz1,singlechar,
                                 pos,sz1);
    default:
        return detail::find_char(_data_ucs1(),sz1,singlechar,
                                 pos,sz1);
    }
}

i64 strval::find(i64 singlechar,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_char(_data_ucs4(),sz1,singlechar,
                                 pos,endpos);
    case _chartype_ucs2:
        return detail::find_char(_data_ucs2(),sz1,singlechar,
                                 pos,endpos);
    default:
        return detail::find_char(_data_ucs1(),sz1,singlechar,
                                 pos,endpos);
    }
}


//
// rfind
//


i64 strval::rfind(strval const &sub,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::rfind_sequence(_data_ucs4(),sub._data_ucs4(),
                                          sz1,sz2,
                                          pos,sz1);
        case _chartype_ucs2:
            return detail::rfind_sequence(_data_ucs4(),sub._data_ucs2(),
                                          sz1,sz2,
                                          pos,sz1);
        default:
            return detail::rfind_sequence(_data_ucs4(),sub._data_ucs1(),
                                          sz1,sz2,
                                          pos,sz1);
        }
    case _chartype_ucs2:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::rfind_sequence(_data_ucs2(),sub._data_ucs4(),
                                          sz1,sz2,
                                          pos,sz1);
        case _chartype_ucs2:
            return detail::rfind_sequence(_data_ucs2(),sub._data_ucs2(),
                                          sz1,sz2,
                                          pos,sz1);
        default:
            return detail::rfind_sequence(_data_ucs2(),sub._data_ucs1(),
                                          sz1,sz2,
                                          pos,sz1);
        }
    default:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::rfind_sequence(_data_ucs1(),sub._data_ucs4(),
                                          sz1,sz2,
                                          pos,sz1);
        case _chartype_ucs2:
            return detail::rfind_sequence(_data_ucs1(),sub._data_ucs2(),
                                          sz1,sz2,
                                          pos,sz1);
        default:
            return detail::rfind_sequence(_data_ucs1(),sub._data_ucs1(),
                                          sz1,sz2,
                                          pos,sz1);
        }
    }
}

i64 strval::rfind(strval const &sub,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::rfind_sequence(_data_ucs4(),sub._data_ucs4(),
                                          sz1,sz2,
                                          pos,endpos);
        case _chartype_ucs2:
            return detail::rfind_sequence(_data_ucs4(),sub._data_ucs2(),
                                          sz1,sz2,
                                          pos,endpos);
        default:
            return detail::rfind_sequence(_data_ucs4(),sub._data_ucs1(),
                                          sz1,sz2,
                                          pos,endpos);
        }
    case _chartype_ucs2:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::rfind_sequence(_data_ucs2(),sub._data_ucs4(),
                                          sz1,sz2,
                                          pos,endpos);
        case _chartype_ucs2:
            return detail::rfind_sequence(_data_ucs2(),sub._data_ucs2(),
                                          sz1,sz2,
                                          pos,endpos);
        default:
            return detail::rfind_sequence(_data_ucs2(),sub._data_ucs1(),
                                          sz1,sz2,
                                          pos,endpos);
        }
    default:
        switch (sub._get_chartype())
        {
        case _chartype_ucs4:
            return detail::rfind_sequence(_data_ucs1(),sub._data_ucs4(),
                                          sz1,sz2,
                                          pos,endpos);
        case _chartype_ucs2:
            return detail::rfind_sequence(_data_ucs1(),sub._data_ucs2(),
                                          sz1,sz2,
                                          pos,endpos);
        default:
            return detail::rfind_sequence(_data_ucs1(),sub._data_ucs1(),
                                          sz1,sz2,
                                          pos,endpos);
        }
    }
}

i64 strval::rfind(i64 singlechar,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::rfind_char(_data_ucs4(),sz1,singlechar,
                                  pos,sz1);
    case _chartype_ucs2:
        return detail::rfind_char(_data_ucs2(),sz1,singlechar,
                                  pos,sz1);
    default:
        return detail::rfind_char(_data_ucs1(),sz1,singlechar,
                                  pos,sz1);
    }
}

i64 strval::rfind(i64 singlechar,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::rfind_char(_data_ucs4(),sz1,singlechar,
                                  pos,endpos);
    case _chartype_ucs2:
        return detail::rfind_char(_data_ucs2(),sz1,singlechar,
                                  pos,endpos);
    default:
        return detail::rfind_char(_data_ucs1(),sz1,singlechar,
                                  pos,endpos);
    }
}


//
// find_first_of
//


i64 strval::find_first_of(strval const &charlist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_firstof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_firstof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_firstof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_firstof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_firstof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_firstof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    }
}

i64 strval::find_first_of(strval const &charlist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_firstof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_firstof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_firstof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_firstof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_firstof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_firstof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    }
}

i64 strval::find_first_of(i64 singlechar,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_char(_data_ucs4(),sz1,singlechar,
                                 pos,sz1);
    case _chartype_ucs2:
        return detail::find_char(_data_ucs2(),sz1,singlechar,
                                 pos,sz1);
    default:
        return detail::find_char(_data_ucs1(),sz1,singlechar,
                                 pos,sz1);
    }
}

i64 strval::find_first_of(i64 singlechar,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_char(_data_ucs4(),sz1,singlechar,
                                 pos,endpos);
    case _chartype_ucs2:
        return detail::find_char(_data_ucs2(),sz1,singlechar,
                                 pos,endpos);
    default:
        return detail::find_char(_data_ucs1(),sz1,singlechar,
                                 pos,endpos);
    }
}


//
// find_last_of
//


i64 strval::find_last_of(strval const &charlist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_lastof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_lastof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_lastof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_lastof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_lastof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_lastof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    }
}

i64 strval::find_last_of(strval const &charlist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_lastof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_lastof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_lastof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_lastof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_lastof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_lastof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    }
}

i64 strval::find_last_of(i64 singlechar,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::rfind_char(_data_ucs4(),sz1,singlechar,
                                  pos,sz1);
    case _chartype_ucs2:
        return detail::rfind_char(_data_ucs2(),sz1,singlechar,
                                  pos,sz1);
    default:
        return detail::rfind_char(_data_ucs1(),sz1,singlechar,
                                  pos,sz1);
    }
}

i64 strval::find_last_of(i64 singlechar,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::rfind_char(_data_ucs4(),sz1,singlechar,
                                  pos,endpos);
    case _chartype_ucs2:
        return detail::rfind_char(_data_ucs2(),sz1,singlechar,
                                  pos,endpos);
    default:
        return detail::rfind_char(_data_ucs1(),sz1,singlechar,
                                  pos,endpos);
    }
}


//
// find_first_not_of
//


i64 strval::find_first_not_of(strval const &charlist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstnotof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_firstnotof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_firstnotof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstnotof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_firstnotof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_firstnotof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstnotof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_firstnotof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_firstnotof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    }
}

i64 strval::find_first_not_of(strval const &charlist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstnotof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_firstnotof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_firstnotof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstnotof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_firstnotof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_firstnotof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_firstnotof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_firstnotof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_firstnotof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    }
}

i64 strval::find_first_not_of(i64 singlechar,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_notchar(_data_ucs4(),sz1,singlechar,
                                    pos,sz1);
    case _chartype_ucs2:
        return detail::find_notchar(_data_ucs2(),sz1,singlechar,
                                    pos,sz1);
    default:
        return detail::find_notchar(_data_ucs1(),sz1,singlechar,
                                    pos,sz1);
    }
}

i64 strval::find_first_not_of(i64 singlechar,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_notchar(_data_ucs4(),sz1,singlechar,
                                    pos,endpos);
    case _chartype_ucs2:
        return detail::find_notchar(_data_ucs2(),sz1,singlechar,
                                    pos,endpos);
    default:
        return detail::find_notchar(_data_ucs1(),sz1,singlechar,
                                    pos,endpos);
    }
}


//
// find_last_not_of
//


i64 strval::find_last_not_of(strval const &charlist,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastnotof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_lastnotof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_lastnotof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastnotof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_lastnotof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_lastnotof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastnotof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,sz1);
        case _chartype_ucs2:
            return detail::find_lastnotof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,sz1);
        default:
            return detail::find_lastnotof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,sz1);
        }
    }
}

i64 strval::find_last_not_of(strval const &charlist,i64 pos,i64 endpos)
    const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    std::size_t sz2 =charlist._get_size();
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastnotof_charlist(
                _data_ucs4(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_lastnotof_charlist(
                _data_ucs4(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_lastnotof_charlist(
                _data_ucs4(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    case _chartype_ucs2:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastnotof_charlist(
                _data_ucs2(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_lastnotof_charlist(
                _data_ucs2(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_lastnotof_charlist(
                _data_ucs2(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    default:
        switch (charlist._get_chartype())
        {
        case _chartype_ucs4:
            return detail::find_lastnotof_charlist(
                _data_ucs1(),charlist._data_ucs4(),
                sz1,sz2,
                pos,endpos);
        case _chartype_ucs2:
            return detail::find_lastnotof_charlist(
                _data_ucs1(),charlist._data_ucs2(),
                sz1,sz2,
                pos,endpos);
        default:
            return detail::find_lastnotof_charlist(
                _data_ucs1(),charlist._data_ucs1(),
                sz1,sz2,
                pos,endpos);
        }
    }
}

i64 strval::find_last_not_of(i64 singlechar,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::rfind_notchar(_data_ucs4(),sz1,singlechar,
                                     pos,sz1);
    case _chartype_ucs2:
        return detail::rfind_notchar(_data_ucs2(),sz1,singlechar,
                                     pos,sz1);
    default:
        return detail::rfind_notchar(_data_ucs1(),sz1,singlechar,
                                     pos,sz1);
    }
}

i64 strval::find_last_not_of(i64 singlechar,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::rfind_notchar(_data_ucs4(),sz1,singlechar,
                                     pos,endpos);
    case _chartype_ucs2:
        return detail::rfind_notchar(_data_ucs2(),sz1,singlechar,
                                     pos,endpos);
    default:
        return detail::rfind_notchar(_data_ucs1(),sz1,singlechar,
                                     pos,endpos);
    }
}


//
// find by ascii character class
//


i64 strval::find_first_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_firstof_acc(_data_ucs4(),sz1,accmask,
                                        pos,sz1);
    case _chartype_ucs2:
        return detail::find_firstof_acc(_data_ucs2(),sz1,accmask,
                                        pos,sz1);
    default:
        return detail::find_firstof_acc(_data_ucs1(),sz1,accmask,
                                        pos,sz1);
    }
}

i64 strval::find_first_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_firstof_acc(_data_ucs4(),sz1,accmask,
                                        pos,endpos);
    case _chartype_ucs2:
        return detail::find_firstof_acc(_data_ucs2(),sz1,accmask,
                                        pos,endpos);
    default:
        return detail::find_firstof_acc(_data_ucs1(),sz1,accmask,
                                        pos,endpos);
    }
}

i64 strval::find_last_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_lastof_acc(_data_ucs4(),sz1,accmask,
                                       pos,sz1);
    case _chartype_ucs2:
        return detail::find_lastof_acc(_data_ucs2(),sz1,accmask,
                                       pos,sz1);
    default:
        return detail::find_lastof_acc(_data_ucs1(),sz1,accmask,
                                       pos,sz1);
    }
}

i64 strval::find_last_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_lastof_acc(_data_ucs4(),sz1,accmask,
                                       pos,endpos);
    case _chartype_ucs2:
        return detail::find_lastof_acc(_data_ucs2(),sz1,accmask,
                                       pos,endpos);
    default:
        return detail::find_lastof_acc(_data_ucs1(),sz1,accmask,
                                       pos,endpos);
    }
}

i64 strval::find_first_not_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_firstnotof_acc(_data_ucs4(),sz1,accmask,
                                           pos,sz1);
    case _chartype_ucs2:
        return detail::find_firstnotof_acc(_data_ucs2(),sz1,accmask,
                                           pos,sz1);
    default:
        return detail::find_firstnotof_acc(_data_ucs1(),sz1,accmask,
                                           pos,sz1);
    }
}

i64 strval::find_first_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_firstnotof_acc(_data_ucs4(),sz1,accmask,
                                           pos,endpos);
    case _chartype_ucs2:
        return detail::find_firstnotof_acc(_data_ucs2(),sz1,accmask,
                                           pos,endpos);
    default:
        return detail::find_firstnotof_acc(_data_ucs1(),sz1,accmask,
                                           pos,endpos);
    }
}

i64 strval::find_last_not_in_acc(i64 accmask,i64 pos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_lastnotof_acc(_data_ucs4(),sz1,accmask,
                                          pos,sz1);
    case _chartype_ucs2:
        return detail::find_lastnotof_acc(_data_ucs2(),sz1,accmask,
                                          pos,sz1);
    default:
        return detail::find_lastnotof_acc(_data_ucs1(),sz1,accmask,
                                          pos,sz1);
    }
}

i64 strval::find_last_not_in_acc(i64 accmask,i64 pos,i64 endpos) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    switch (_get_chartype())
    {
    case _chartype_ucs4:
        return detail::find_lastnotof_acc(_data_ucs4(),sz1,accmask,
                                          pos,endpos);
    case _chartype_ucs2:
        return detail::find_lastnotof_acc(_data_ucs2(),sz1,accmask,
                                          pos,endpos);
    default:
        return detail::find_lastnotof_acc(_data_ucs1(),sz1,accmask,
                                          pos,endpos);
    }
}


//
// Specializations
//


bool strval::contains(strval const &sub) const noexcept
{
    return find(sub) != -1;
}

bool strval::starts_with(strval const &sub) const noexcept
{
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    
    return find(sub,0,sz2) != -1;
}

bool strval::ends_with(strval const &sub) const noexcept
{
    int64_t sz1 = static_cast<int64_t>(_get_size());
    int64_t sz2 = static_cast<int64_t>(sub._get_size());
    
    return find(sub,sz1-sz2) != -1;
}





} // namespace detail

} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
