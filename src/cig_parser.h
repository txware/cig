/* cig_parser.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Parser for the CIG language.
 */
#ifndef CIG_PARSER_H_INCLUDED
#define CIG_PARSER_H_INCLUDED

#include "cig_err.h"
#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_vector.h"
#include "cig_ast_node.h"

namespace cig {

namespace parser {


struct parse_error : public cig::err
{
    str name;                   // Name, identifier, additional info

    parse_error() noexcept : err(0) {}

    // From error code
    parse_error(cig::err e) : err(e) { }

    // From location and error code
    parse_error(cig::source_location const &sloc,
                cig::err e) noexcept :
        err(e.get_error_code(),sloc)
    {
    }
    parse_error(cig::source_location const &sloc,
                cig::err::errorcode errcode) noexcept :
        err(errcode,sloc)
    {
    }
    parse_error(cig::source_location const &sloc,
                int errcode) noexcept :
        err(errcode,sloc)
    {
    }

    // From two locations and error code
    parse_error(cig::source_location const &sloc,
                cig::source_location const &prevloc,
                cig::err e) noexcept :
        err(e.get_error_code(),sloc,prevloc)
    {
    }
    parse_error(cig::source_location const &sloc,
                cig::source_location const &prevloc,
                cig::err::errorcode errcode) noexcept :
        err(errcode,sloc,prevloc)
    {
    }

    // From location, error code, and name
    parse_error(cig::source_location const &sloc,
                cig::err::errorcode errcode,
                char const *nm) noexcept :
        err(errcode,sloc)
    {
        name = str::from_ascii(nm,std::strlen(nm));
    }
    str to_str() const {
        if (name.is_empty())
            return err::to_str();
        else
            return err::to_str() + ": "_str + name;
    }
};

inline const parse_error no_parse_error = parse_error();


parse_error
parse_file(ast::ast_node_ptr global, // global namespace
           str const &filename,
           char const *location_file_name,
           i64 tabsize = 8);

} // namespace parser

} // namespace cig


extern "C" char const *cig_parser_selftest();



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_PARSER_H_INCLUDED) */
