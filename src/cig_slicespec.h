/* cig_slicespec.h
 *
 * Copyright 2022-2023 Claus Fischer
 *
 * Auxiliary functions for slice operations.
 */
#ifndef CIG_SLICESPEC_H_INCLUDED
#define CIG_SLICESPEC_H_INCLUDED

#include "cig_datatypes.h"      // i64,err

#include <cstddef>              // std::size_t, std::ptrdiff_t
#include <array>                // std::array
#include <optional>             // std::optional,std::nullopt
#include <limits>               // std::numeric_limits


 
namespace cig {


namespace detail {


struct slicespec { 
    std::size_t count;
    std::size_t start;
    std::ptrdiff_t step;
};


constexpr slicespec empty_slicespec { .count = 0, .start = 0, .step = 1 };


constexpr slicespec
setup_slicespec(
    std::size_t size, 		// size of sequence object (array, str, ...)
    std::ptrdiff_t start,       // start position (first position to use)
    std::ptrdiff_t end,         // end position (excluded)
    std::ptrdiff_t step = 1,    // step size, 0 gives an empty slice
    bool minus_from_end = true, // interpret negative positions from end
    bool limit_wo_step = true)  // limit positions to 0..size-1 w/o adding step
    
{
    // Given a sequence of length size with indices in interval [0..size)
    // this function returns a slicespec where
    // for each i in [0..count)
    // the index start + i * step is in [0..size)

    // For Python style slices, the caller should use
    // the following defaults for start and end:
    // if step > 0:
    //   start =  std::numeric_limits<ptrdiff_t>::min
    //   end   =  std::numeric_limits<ptrdiff_t>::max
    // if step < 0:
    //   start =  std::numeric_limits<ptrdiff_t>::max
    //   end   =  std::numeric_limits<ptrdiff_t>::min

    // For Python style slices, set minus_from_end true
    if (minus_from_end)
    {
        if (start < 0)
            start += static_cast<std::ptrdiff_t>(size);
        if (end < 0)
            end += static_cast<std::ptrdiff_t>(size);
    }

    // For Python style slices, set limit_without_step true
    if (step > 0)
    {
        if (start < 0)
        {
            if (limit_wo_step)
                // Python style
                start = 0;
            else
                // remain contruent modulo step
                start += (-1 - start) / step + 1;
        }
        if (end > static_cast<ptrdiff_t>(size))
            end = static_cast<ptrdiff_t>(size);
        if (end <= start)
            return empty_slicespec;
        std::ptrdiff_t n = (end - start + step - 1) / step;
        return slicespec { .count = static_cast<std::size_t>(n),
                           .start = static_cast<std::size_t>(start),
                           .step = step };
    }
    else if (step < 0)
    {
        if (start >= static_cast<ptrdiff_t>(size))
        {
            if (limit_wo_step)
                // Python style
                start = static_cast<ptrdiff_t>(size) - 1;
            else
                // remain congruent modulo step
                start -= (start - static_cast<ptrdiff_t>(size)) / (-step) + 1;
        }
        if (end < -1)
            end = -1;
        if (end >= start)
            return empty_slicespec;
        std::ptrdiff_t n = (start - end + (-step) - 1) / (-step);
        return slicespec { .count = static_cast<std::size_t>(n),
                           .start = static_cast<std::size_t>(start),
                           .step = step };
    }
    else // step == 0
        return empty_slicespec;
}


constexpr std::array<slicespec,2>
split_slicespec(
    slicespec spec,             // slicespec for full sequence
    std::size_t N)              // size of first part of sequence
{
    if (spec.step > 0)
    {
        std::size_t ustep = static_cast<std::size_t>(spec.step); // unsigned
        if (spec.start >= N)
            return { empty_slicespec, { .count = spec.count,
                                        .start = spec.start - N,
                                        .step = spec.step } };
        else
        {
            std::size_t n1 = (N - spec.start + ustep - 1) / ustep;
            if (n1 >= spec.count)
                return { spec, empty_slicespec };
            else
                return {
                    slicespec { .count = n1,
                                .start = spec.start,
                                .step = spec.step },
                    slicespec { .count = spec.count - n1,
                                .start = spec.start + n1 * ustep - N,
                                .step = spec.step }
                };
        }
    }
    else if (spec.step < 0)
    {
        std::size_t ustep = static_cast<std::size_t>(-spec.step); // unsigned

        if (spec.start < N)
            return { spec, empty_slicespec };
        else
        {
            std::size_t n2 = (spec.start - N) / ustep + 1;
            if (n2 >= spec.count)
                return { empty_slicespec, { .count = spec.count,
                                            .start = spec.start - N,
                                            .step = spec.step } };
                
            else
                return {
                    slicespec { .count = spec.count - n2,
                                .start = spec.start - n2 * ustep,
                                .step = spec.step },
                    slicespec { .count = n2,
                                .start = spec.start - N,
                                .step = spec.step }
                };
        }
    }
    else
        return { empty_slicespec, empty_slicespec };
}


constexpr slicespec
chain_slicespec(
    slicespec upper,            // selects from lower
    slicespec lower)            // selects from sequence
{
    std::size_t start =
        (lower.step >= 0 ?
         lower.start + static_cast<std::size_t>(lower.step) * upper.start :
         lower.start - static_cast<std::size_t>(-lower.step) * upper.start);
    return {
        .count = upper.count,
        .start = start,
        .step = upper.step * lower.step
    };
}


constexpr slicespec
divide_slicespec(
    slicespec spec,             // spec for n parts
    std::size_t i,              // index of part (0-based) for which to return
    std::size_t size)           // size of each part
{
    std::size_t lowerpos = i * size;
    std::size_t upperpos = lowerpos + size;

    // restrict the slicespec to the range [lowerpos..upperpos)
    if (spec.step > 0)
    {
        if (spec.start >= upperpos)
            return empty_slicespec;
        std::size_t ustep = static_cast<std::size_t>(spec.step); // unsigned
        std::size_t nupper = (upperpos - spec.start + ustep - 1) / ustep;
        if (nupper > spec.count)
            nupper = spec.count;
        std::size_t nlower;
        if (spec.start >= lowerpos)
            nlower = 0;
        else
        {
            nlower = (lowerpos - spec.start + ustep - 1) / ustep;
            if (nlower > spec.count)
                nlower = spec.count;
        }
        if (nlower >= nupper)
            return empty_slicespec;
        return { .count = nupper - nlower,
                 .start = spec.start + nlower * ustep - lowerpos,
                 .step = spec.step };
    }
    else if (spec.step < 0)
    {



        if (spec.start < lowerpos)
            return empty_slicespec;

        std::size_t ustep = static_cast<std::size_t>(-spec.step); // unsigned
        std::size_t nlower = (spec.start - lowerpos) / ustep + 1;
        if (nlower > spec.count)
            nlower = spec.count;
        std::size_t nupper;
        if (spec.start < upperpos)
            nupper = 0;
        else
        {
            nupper = (spec.start - upperpos) / ustep + 1;
            if (nupper > spec.count)
                nupper = spec.count;
        }
        if (nlower <= nupper)
            return empty_slicespec;
        return { .count = nlower - nupper,
                 .start = spec.start - nupper * ustep - lowerpos,
                 .step = spec.step };
    }
    else
        return empty_slicespec;
}


constexpr std::expected<slicespec,err>
setup_slicespec_like_python(
    std::size_t size, 		// size of sequence object (array, str, ...)
    std::optional<i64> start,   // start position (first position to use)
    std::optional<i64> end,     // end position (excluded)
    std::optional<i64> step = 1 // step size, 0 gives empty slice
    )
{
    static_assert(sizeof(std::size_t) <= sizeof(int64_t));
    static_assert(sizeof(std::ptrdiff_t) <= sizeof(int64_t));

    constexpr i64 min = i64(std::numeric_limits<std::ptrdiff_t>::min());
    constexpr i64 max = i64(std::numeric_limits<std::ptrdiff_t>::max());

    i64 stepx = step.value_or(i64(1));
    if (stepx._v == 0)
    {
        return empty_slicespec;
    }
    else if (stepx._v > 0)
    {
        i64 startx = start.value_or(min);
        i64 endx = end.value_or(max);
        return setup_slicespec(size,startx._v,endx._v,stepx._v);
    }
    else // stepx < 0
    {
        i64 startx = start.value_or(max);
        i64 endx = end.value_or(min);
        return setup_slicespec(size,startx._v,endx._v,stepx._v);
    }
}
    

} // namespace detail


} // namespace cig


/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_SLICESPEC_H_INCLUDED) */
