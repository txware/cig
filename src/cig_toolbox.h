/* cig_toolbox.h
 *
 * Copyright 2023 Claus Fischer
 *
 * A class with various tools for CIG datatypes.
 */
#ifndef CIG_TOOLBOX_H_INCLUDED
#define CIG_TOOLBOX_H_INCLUDED

#include "cig_err.h"            // type err
#include "cig_datatypes.h"
#include "cig_ast_literal.h"


namespace cig {


struct toolbox {
    
    // Count lines and columns in a str or UTF-8 bytes representing a file
    // Both must be 0-based and a tab advances to next multiple of tabsize
    static void
    count_line_and_column(detail::strval const &s,
                          i64 startpos,i64 endpos,u64 &line,u64 &column,
                          u64 tabsize = 8) noexcept;
    static void
    count_line_and_column(detail::bytesval const &s, // counted as UTF-8 codepts
                          i64 startpos,i64 endpos,u64 &line,u64 &column,
                          u64 tabsize = 8) noexcept;

    // Generalized decoding of str (utf32) from str (utf32 possibly quoted)
    static exp_str
    exp_decode_str_from_str(
        detail::strval const &s,// [i] input string
        i64 startpos,           // [i] start position in s
        i64 &endpos,            // [o] end position or error position in s
        // Behavior configuration
        long delimiter = -1,     // [i] code unit in s to stop at unless escaped
        int stop_if_incomplete=0,// [i] stop parsing before incomplete codepoint
        long n_codepoints_max=-1,// [i] maximum number of codepoints in o
        int quoted_form = 0,     // [i] whether control chars must be quoted
        int with_delimiters = 0, // [i] require delimiters around input
        int zero_terminate = 0   // [i] whether to append a trailing '\0'
    ) noexcept;

    // Generalized decoding of str (utf32) from bytes (utf8 possibly quoted)
    static exp_str
    exp_decode_str_from_bytes(
        detail::bytesval const &b, // [i] input bytes
        i64 startpos,              // [i] start position in b
        i64 &endpos,               // [o] end position or error position in b
        // Behavior configuration
        long delimiter = -1,     // [i] code unit in s to stop at unless escaped
        int stop_if_incomplete=0,// [i] stop parsing before incomplete codepoint
        long n_codepoints_max=-1,// [i] maximum number of codepoints in o
        int quoted_form = 0,     // [i] whether control chars must be quoted
        int with_delimiters = 0, // [i] require delimiters around input
        int zero_terminate = 0   // [i] whether to append a trailing '\0'
    ) noexcept;

    // Generalized decoding of bytes (utf8/raw) from str (utf32 possibly quoted)
    static exp_bytes
    exp_decode_bytes_from_str(
        detail::strval const &s,// [i] input string
        i64 startpos,           // [i] start position in s
        i64 &endpos,            // [o] end position or error position in s
        // Behavior configuration
        long delimiter = -1,     // [i] code unit in s to stop at unless escaped
        int stop_if_incomplete=0,// [i] stop parsing before incomplete codepoint
        long n_codepoints_max=-1,// [i] maximum number of codepoints in o
        int quoted_form = 0,     // [i] whether control chars must be quoted
        int with_delimiters = 0, // [i] require delimiters around input
        int zero_terminate = 0   // [i] whether to append a trailing '\0'
    ) noexcept;

    // Generalized decoding of bytes (utf8/raw) from bytes (utf8 possibly qutd.)
    static exp_bytes
    exp_decode_bytes_from_bytes(
        detail::bytesval const &b, // [i] input bytes
        i64 startpos,              // [i] start position in b
        i64 &endpos,               // [o] end position or error position in b
        // Behavior configuration
        long delimiter = -1,     // [i] code unit in s to stop at unless escaped
        int stop_if_incomplete=0,// [i] stop parsing before incomplete codepoint
        long n_codepoints_max=-1,// [i] maximum number of codepoints in o
        int quoted_form = 0,     // [i] whether control chars must be quoted
        int with_delimiters = 0, // [i] require delimiters around input
        int zero_terminate = 0   // [i] whether to append a trailing '\0'
    ) noexcept;

    // Decode a str in JSON syntax from source str or UTF-8 bytes
    // On error, endpos marks the error position in s
    static exp_str
    exp_decode_json_str(detail::strval const &s,
                        i64 startpos,i64 &endpos) noexcept;
    static exp_str
    exp_decode_json_str(detail::bytesval const &b,
                        i64 startpos,i64 &endpos) noexcept;

    // Decode a str in C syntax from source str or UTF-8 bytes
    // On error, endpos marks the error position in s
    static exp_str
    exp_decode_C_str(detail::strval const &s,
                     i64 startpos,i64 &endpos) noexcept;
    static exp_str
    exp_decode_C_str(detail::bytesval const &b,
                     i64 startpos,i64 &endpos) noexcept;
    
    // Decode a UTF-8 bytes in C syntax from source str or UTF-8 bytes
    // On error, endpos marks the error position in s
    static exp_bytes
    exp_decode_C_bytes_u8(detail::strval const &s,
                          i64 startpos,i64 &endpos) noexcept;
    static exp_bytes
    exp_decode_C_bytes_u8(detail::bytesval const &b,
                          i64 startpos,i64 &endpos) noexcept;
    
    // Decode a raw bytes in C syntax from source str or UTF-8 bytes
    // On error, endpos marks the error position in s
    static exp_bytes
    exp_decode_C_bytes(detail::strval const &s,
                       i64 startpos,i64 &endpos) noexcept;
    static exp_bytes
    exp_decode_C_bytes(detail::bytesval const &b,
                       i64 startpos,i64 &endpos) noexcept;

    // Decode a Unicode code point in C syntax from source str or UTF-8 bytes
    // This is a single-character literal
    // On error, endpos marks the error position in s
    static exp_i64
    exp_decode_quoted_codepoint(detail::strval const &s,
                                i64 startpos,i64 &endpos) noexcept;
    static exp_i64
    exp_decode_quoted_codepoint(detail::bytesval const &b,
                                i64 startpos,i64 &endpos) noexcept;

    static exp_u64
    exp_decode_u64_with_suffix(detail::strval const &s,
                               i64 startpos,i64 &endpos,
                               int &suffix // see implementation for suffix vals
        ) noexcept;

    static exp_u64
    exp_decode_u64_with_suffix(detail::bytesval const &b,
                               i64 startpos,i64 &endpos,
                               int &suffix // see implementation for suffix vals
        ) noexcept;

    static ast::exp_literal
    exp_decode_int_literal(detail::strval const &s,
                           i64 startpos,i64 &endpos) noexcept;

    static ast::exp_literal
    exp_decode_int_literal(detail::bytesval const &b,
                           i64 startpos,i64 &endpos) noexcept;

    // Generalized encoding of str (utf32) to str (utf32 possibly quoted)
    static exp_str
    exp_encode_str_to_str(
        detail::strval const &s,// [i] input string
        // Behavior configuration
        int quote_controls = 0, // [i] 0 not 1 JSON-style \u 2 C-style \ooo
        int quote_escapes = 0,  // [i] whether to quote " and \ and ...
        int quote_nonascii = 0, // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
        long delimiter = -1,    // [i] if nonnegative enclose in this delimiter
        int zero_terminate = 0  // [i] whether to append a trailing '\0'
        );

    // Generalized encoding of str (utf32) to bytes (utf8, possibly quoted)
    static exp_bytes
    exp_encode_str_to_bytes(
        detail::strval const &s,// [i] input string
        // Behavior configuration
        int quote_controls = 0, // [i] 0 not 1 JSON-style \u 2 C-style \ooo
        int quote_escapes = 0,  // [i] whether to quote " and \ and ...
        int quote_nonascii = 0, // [i] 0 not 1 U 2 Uu 3 u surr 4 oU 5 oUu 6 ousg
        long delimiter = -1,    // [i] if nonnegative enclose in this delimiter
        int zero_terminate = 0  // [i] whether to append a trailing '\0'
        );

#if 0
    // Generalized encoding of bytes (utf8/raw) to str (utf32 possibly quoted)
    // is not possible (it's ambiguous).
    // 
    // If the bytes sequence is a fully valid UTF-8 string:
    //   - test with bytes::is_pure_utf8()
    //   - convert with bytes::decode_utf8()
    //
    // Otherwise first encode bytes to bytes, then to str:
    //   - encode with exp_encode_bytes_to_bytes() with quote_nonascii
    //   - convert with bytes::decode_utf8()
    // 
    static exp_str
    exp_encode_bytes_to_str(...);
#endif

    // Generalized encoding of bytes (utf8/raw) to bytes (utf8, possibly quoted)
    static exp_bytes
    exp_encode_bytes_to_bytes(
        detail::bytesval const &b, // [i] input bytes
        // Behavior configuration
        int quote_controls = 0, // [i] 0 not 1 JSON-style \u 2 C-style \ooo
        int quote_escapes = 0,  // [i] whether to quote " and \ and ...
        int quote_nonascii = 0, // [i] 0 keep 1 \U 2 \u 3 \oct 4 latin1 to utf8 
        long delimiter = -1,    // [i] if nonnegative enclose in this delimiter
        int zero_terminate = 0  // [i] whether to append a trailing '\0'
        );


    // Encode a str with JSON syntax into str or UTF-8 bytes
    static exp_str
    exp_encode_json_str(detail::strval const &s,
                        cig_bool ensure_ascii = cig_true) noexcept;
    static exp_bytes
    exp_encode_json_str_to_bytes(detail::strval const &s,
                                 cig_bool ensure_ascii = cig_true) noexcept;

    // Encode a str with C syntax into str or UTF-8 bytes
    // Use with prefix 'u' in CIG source code.
    static exp_str
    exp_encode_C_str(detail::strval const &s,
                     cig_bool ensure_ascii = cig_false) noexcept;
    static exp_bytes
    exp_encode_C_str_to_bytes(detail::strval const &s,
                              cig_bool ensure_ascii = cig_false) noexcept;

    // Encode a bytes with C syntax into str or UTF-8 bytes
    // Use with prefix 'b' in CIG source code.
    static exp_str
    exp_encode_C_bytes(detail::bytesval const &b) noexcept;

    static exp_bytes
    exp_encode_C_bytes_to_bytes(detail::bytesval const &b,
                                cig_bool ensure_ascii = cig_true) noexcept;

    // Encode a bytes with strict UTF-8 content into str or UTF-8 bytes
    // Use with prefix 'u8' in CIG source code.
    static exp_bool
    exp_verify_u8_bytes(detail::bytesval const &b) noexcept;

    static exp_str
    exp_encode_u8_bytes(detail::bytesval const &b) noexcept;

    static exp_bytes
    exp_encode_u8_bytes_to_bytes(detail::bytesval const &b) noexcept;
};



} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_TOOLBOX_H_INCLUDED) */
