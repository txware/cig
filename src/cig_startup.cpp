/* cig_startup.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Startup and termination code for cig_main.
 */
#include "cig_startup_main.h"
#include "cig_startup_filepointer.h"
#include "cig_allocation.h"

namespace cig {

namespace startup {


//
// Filepointer tracker
//


// A singleton
struct filepointer_tracker
{
    detail::filepointer_wrapper dll;
    static filepointer_tracker &get_singleton();

    filepointer_tracker() : dll(0) // no name: no recursive self-registration
    {}
};

filepointer_tracker &filepointer_tracker::get_singleton()
{
    // Object is constructed on first call to get_singleton
    // Object is destructed sometime after main() ends.
    // For the dll, the sequence of destruction is not important,
    // however construction must be done before the allocators.
    static filepointer_tracker tracker;
    return tracker;
}


namespace detail {

void register_filepointer_wrapper(filepointer_wrapper &fpwrapper)
{
    filepointer_tracker &tracker = filepointer_tracker::get_singleton();

    // Do not register the tracker's builtin allocator
    // that just serves as a list head and tail
    if (&fpwrapper == &tracker.dll)
        return;

    fpwrapper.append_to_dll(tracker.dll);
}

} // namespace detail


// Global function to flush all filepointer_wrapper objects
void flush_all_filepointer_wrappers()
{
    filepointer_tracker &tracker = filepointer_tracker::get_singleton();
    detail::filepointer_wrapper *fpwrapper;

    for (fpwrapper = tracker.dll.dll_next;
         fpwrapper != &tracker.dll;
         fpwrapper = fpwrapper->dll_next)
    {
        if (fpwrapper->file_pointer)
            std::fflush(fpwrapper->file_pointer);
    }
}

// Global function to close all filepointer_wrapper objects
void close_all_filepointer_wrappers()
{
    filepointer_tracker &tracker = filepointer_tracker::get_singleton();
    detail::filepointer_wrapper *fpwrapper;

    for (fpwrapper = tracker.dll.dll_next;
         fpwrapper != &tracker.dll;
         fpwrapper = fpwrapper->dll_next)
    {
        if (fpwrapper->file_pointer)
            std::fclose(fpwrapper->file_pointer);
        fpwrapper->file_pointer = 0;
    }
}


} // namespace startup


} // namespace cig


// Wrapper for main program with allocation checker on termination
int main(int argc, char *argv[])
{
    int rv = cig::cig_main(argc,argv);

    if (cig::allocation::check_still_allocated())
    {
        // Allocated memory has not been properly freed
        cig::source_location loc = std::source_location::current();
        fprintf(stderr,"%s:%llu:%llu: "
                "allocate memory not freed at end of program\n",
                loc.file_name(),
                static_cast<unsigned long long>(loc.line()-1),
                static_cast<unsigned long long>(loc.column()-56));
        cig::allocation::endreport(stderr,true);
    }
    else
    {
        // Still do an endreport
        // (this code might be removed or being run on condition
        // of some environment variable.
        cig::allocation::endreport(stderr,true);
    }
    return rv;
}





/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
