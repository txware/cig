/* cig_selftest.h
 *
 * Copyright 2022 Claus Fischer
 *
 * Selftest function for cig.
 */

#ifndef CIG_SELFTEST_H_INCLUDED
#define CIG_SELFTEST_H_INCLUDED

namespace cig {

char const * selftest_errorstring();  // static error string or 0 on success

} /* namespace cig */

/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif	/* !defined(CIG_SELFTEST_H_INCLUDED) */
