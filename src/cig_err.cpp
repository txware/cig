/* cig_err.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Error type with internal cig_errno value.
 */
#include "cig_err.h"

#include "cig_datatypes.h"
#include "cig_str_inline.h"
#include "cig_errno.h"
#include "cig_errorcode.h"
#include <cstring>              // std::strlen
#include <cstdio>               // std::snprintf

namespace cig {


bool err::has_errortag() const noexcept
{
    return cig_errorcode_has_errortag(ecode);
}

char const * err::errortag_c() const noexcept
{
    // A good C function never fails
    return cig_errorcode_errortag(ecode);
}

str err::errortag() const
{
    // Avoid circular creation of errors
    auto error = str::from_utf8(errortag_c());
    if (error.has_value())
        return std::move(std::move(error).value());

    // Failure above probably caused by bad UTF-8, so return plain ASCII
    char buf[200];
    std::snprintf(buf,sizeof(buf),"E%d",ecode);
    return str::from_ascii(buf,std::strlen(buf));
}

char const *err::strerror_c() const noexcept
{
    // A good C function never fails
    return cig_errorcode_strerror(ecode);
}

str err::strerror() const
{
    // Avoid circular creation of errors
    auto error = str::from_utf8(strerror_c());
    if (error.has_value())
        return std::move(std::move(error).value());

    // Failure above probably caused by bad UTF-8, so return plain ASCII
    char buf[200];
    std::snprintf(buf,sizeof(buf),"E%d",ecode);
    return str::from_ascii(buf,std::strlen(buf));
}

str err::to_str() const
{
    char buf[200];

    if (has_errortag())
        std::snprintf(buf,sizeof(buf),"%s (%d): %s%s%s",
                      errortag_c(),ecode,strerror_c(),
                      etext ? ": " : "",
                      etext ? etext : "");
    else
        std::snprintf(buf,sizeof(buf),"%s (%d)%s%s",
                      strerror_c(),ecode,
                      etext ? ": " : "",
                      etext ? etext : "");
    auto error = str::from_utf8(buf);
    if (error.has_value())
        return std::move(std::move(error).value());

    // Failure above probably caused by bad UTF-8, so return plain ASCII
    std::snprintf(buf,sizeof(buf),"E%d",ecode);
    return str::from_ascii(buf,std::strlen(buf));
}


} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
