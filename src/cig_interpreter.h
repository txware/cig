/* cig_interpreter.h
 *
 * Copyright 2023 Claus Fischer
 *
 * Interpreter for the CIG language.
 */
#ifndef CIG_INTERPRETER_H_INCLUDED
#define CIG_INTERPRETER_H_INCLUDED

#include "cig_err.h"
#include "cig_datatypes.h"
#include "cig_literals.h"
#include "cig_str_inline.h"
#include "cig_bytes_inline.h"
#include "cig_int_inline.h"
#include "cig_vector.h"
#include "cig_ast_node.h"
#include "cig_interpreter_ops_and_types.h"


namespace cig {

namespace interpreter {

// Opcode for instruction for the internal interpreter
enum opcode : uint16_t;

// An instruction for the internal interpreter
struct instruction {
    opcode op;                  // Operation code and argument types
    str label;                  // Name of label, or ""
    i64 arg1;                   // Address of argument 1 in stackmem
    i64 arg2;                   // Address of argument 2 in stackmem
    i64 res;                    // Address of result in stackmem
    str comment;                // Comment text (single-line)
};


// Interpreter code for a function
struct fncode {
    i64 fnidx;                  // Function index as stored in AST tree fundefs
    str fqname;                 // Fully qualified name of function
    i64 dump_index;             // Dump index of function, or -1

    i64 return_list_size;       // Size in bytes of return list
    i64 return_list_alignment;  // Alignment in bytes of return list
    i64 retandpar_list_size;    // Size in bytes of return and parameter list
    i64 retandpar_list_alignment;  // Alignment in bytes of return and par list
    i64 function_size;          // Size in bytes of full function variable space
    i64 function_alignment;     // Alignment in bytes of function variable space

    instruction *instr = 0;     // Allocated instruction list, or nullptr
    i64 n_instr;                // Length of allocated instruction list

    ~fncode();
    fncode();
    void *operator new(std::size_t size);
    void operator delete(void *ptr);
};


// Interpreter data for a data type
struct dtcode {
    i64 typeidx;                // Type idx as stored in AST tree class/enumdef
    str fqname;                 // Fully qualified name of datatype
    i64 dump_index;             // Dump index of data type, or -1

    i64 type_alignment = -1;    // Data type alignment
    i64 type_size = -1;         // Data type size
};

// Interpreter code for full interpreter
struct ipcode {
    cig::vector<dtcode> dt;
    cig::vector<std::unique_ptr<fncode> > fn;
    cig::vector<unsigned char> globalconst;
    cig::vector<unsigned char> globalvarinit;
    cig::vector<unsigned char> globalvar;
};

using ipcode_ptr = std::shared_ptr<ipcode>;
using exp_ipcode_ptr = std::expected<ipcode_ptr,cig::err>;


// Unwind flags, see doc/scope_unwinding.txt
enum class unwind_flags : uint64_t {
    UW_ZERO = 0,                // Nothing being processed, regular program flow
    UW_BREAK = 1,               // Break being processed
    UW_CONTINUE = 2,            // Continue being processed
    UW_RETURN = 4,              // Return being processed
    UW_EXCEPTION = 8            // Exception from fncall or illegal instr b.p.
};


// Runtime per-function stackmem
struct fnruntime {
    fncode *fn;                 // Function code
    i64 size;                   // Size of stackmem excluding this structure
    u64 ip;                     // Instruction pointer
    fnruntime *caller;          // Caller function
    fnruntime *called = 0;      // Called function, or nullptr
    unsigned char *globalconst; // Global constants
    unsigned char *globalvar;   // Global variables
    uint64_t uwflags;           // Unwind flags

    void free() noexcept;
    static fnruntime *allocate(i64 size,fnruntime *caller) noexcept;
    unsigned char *to_stackmem() noexcept
    {
        return static_cast<unsigned char *>(static_cast<void*>(this + 1));
    }
    static fnruntime *from_stackmem(unsigned char *stackmem) noexcept
    {
        fnruntime *rt = static_cast<fnruntime *>(static_cast<void*>(stackmem));
        return &rt[-1];
    }
};





exp_ipcode_ptr ast_tree_to_ipcode(
    ast::ast_node_ptr ast_tree,
    bool use_many_labels = true,
    bool compact = true
    );


// Prototypes
str opcode_name(opcode op);
str opcode_name_lower(opcode op);
opstem                          // Unary or binary opstem, or noop
ast_node_type_to_opstem(cig::ast::ast_node_type t);
opstem                          // Stem of the opcode
opcode_to_opstem(opcode op);
oparg                           // First argument of the opcode
opcode_to_oparg1(opcode op);
oparg                           // Second argument of the opcode
opcode_to_oparg2(opcode op);
opcode                          // Opcode or noop
opstem_to_unary_opcode(opstem stem,oparg arg);
opcode                          // Opcode or noop
opstem_to_binary_opcode(opstem stem,oparg arg1,oparg arg2);
oparg                           // Oparg of builtin type, or OPARG_COMPOUND
ast_builtin_type_to_oparg(ast::ast_builtin_type bitype);


void run_function(unsigned char * CIG_RESTRICT stackmem);



} // namespace interpreter

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_INTERPRETER_H_INCLUDED) */
