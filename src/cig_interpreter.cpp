/* cig_interpreter.cpp
 *
 * Copyright 2023-2024 Claus Fischer
 *
 * Interpreter for the CIG language.
 */
#include "cig_interpreter.h"
#include "cig_ast_builtin.h"
#include "cig_ast_iterator.h"
#include "cig_ast_node.h"
#include "cig_interpreter_code.h"
#include "cig_constexpr_itoa.h"
#include "cig_allocation.h"

namespace cig {

namespace interpreter {


// Non-enum (u64) versions of the unwind flags
constexpr auto U64_BREAK     = static_cast<uint64_t>(unwind_flags::UW_BREAK);
constexpr auto U64_CONTINUE  = static_cast<uint64_t>(unwind_flags::UW_CONTINUE);
constexpr auto U64_RETURN    = static_cast<uint64_t>(unwind_flags::UW_RETURN);
constexpr auto U64_EXCEPTION = static_cast<uint64_t>(unwind_flags::UW_EXCEPTION);



/**********************************************************************/
/*                                                                    */
/*                       LITERAL INITIALIZATION                       */
/*                                                                    */
/**********************************************************************/


// Note: The following functions 'copy' without allocation.
//       The 'copied' objects reference the original objects,
//       therefore they are valid only as long as the originals are.


void
pseudocopy(str const &src,unsigned char *dst)
{
    str *d = static_cast<str*>(static_cast<void*>(dst));

    if (src._is_allocated())
    {
        if (src._is_ucs4())
        {
            d->_l._cap =
                str::_long_mask | static_cast<std::size_t>(str::_chartype_ucs4);
            d->_l._size = src._get_size();
            d->_l._data = std::bit_cast<std::size_t,str::ucs4_t*>(
                const_cast<str::ucs4_t *>(src._data_ucs4()));
        }
        else if (src._is_ucs2())
        {
            d->_l._cap =
                str::_long_mask | static_cast<std::size_t>(str::_chartype_ucs2);
            d->_l._size = src._get_size();
            d->_l._data = std::bit_cast<std::size_t,str::ucs2_t*>(
                const_cast<str::ucs2_t *>(src._data_ucs2()));
        }
        else
        {
            d->_l._cap =
                str::_long_mask | static_cast<std::size_t>(
                    src._is_ascii() ?
                    str::_chartype_ascii : str::_chartype_ucs1);
            d->_l._size = src._get_size();
            d->_l._data = std::bit_cast<std::size_t,str::ucs1_t*>(
                const_cast<str::ucs1_t *>(src._data_ucs1()));
        }
    }
    else
        *d = src;
}


void
pseudocopy(bytes const &src,unsigned char *dst)
{
    bytes *d = static_cast<bytes*>(static_cast<void*>(dst));

    if (src._is_allocated())
    {
        d->_l._cap =
            bytes::_long_mask;
        d->_l._size = src._get_size();
        d->_l._data = std::bit_cast<std::size_t,bytes::byte_t*>(
            const_cast<bytes::byte_t *>(src._data()));
    }
    else
        *d = src;
}


void
pseudocopy(const cig::ast::literal &src,unsigned char *dst)
{
    using enum cig::ast::literal::literal_type;

    auto t = src.get_literal_type();
    switch (t)
    {
    case LIT_UNDEFINED:
        cig_assert(false);
    case LIT_STR:
    {
        str const &s = std::get<str>(src);
        pseudocopy(s,dst);
        return;
    }
    case LIT_BYTES:
    {
        bytes const &b = std::get<bytes>(src);
        pseudocopy(b,dst);
        return;
    }
    case LIT_I64:
        *static_cast<i64*>(static_cast<void*>(dst)) = std::get<i64>(src);
        return;
    case LIT_I32:
        *static_cast<i32*>(static_cast<void*>(dst)) = std::get<i32>(src);
        return;
    case LIT_I16:
        *static_cast<i16*>(static_cast<void*>(dst)) = std::get<i16>(src);
        return;
    case LIT_I8:
        *static_cast<i8*>(static_cast<void*>(dst)) = std::get<i8>(src);
        return;
    case LIT_U64:
        *static_cast<u64*>(static_cast<void*>(dst)) = std::get<u64>(src);
        return;
    case LIT_U32:
        *static_cast<u32*>(static_cast<void*>(dst)) = std::get<u32>(src);
        return;
    case LIT_U16:
        *static_cast<u16*>(static_cast<void*>(dst)) = std::get<u16>(src);
        return;
    case LIT_U8:
        *static_cast<u8*>(static_cast<void*>(dst)) = std::get<u8>(src);
        return;
    case LIT_BOOL:
        *static_cast<cig_bool*>(static_cast<void*>(dst)) =
            std::get<cig_bool>(src);
        return;
    }
    cig_assert(false);
}


/**********************************************************************/
/*                                                                    */
/*                      CONSTANTS INITIALIZATION                      */
/*                                                                    */
/**********************************************************************/


static
exp_none
preset_globalconst(ast::ast_node_ptr ast_tree,
                   unsigned char *globalconst,
                   i64 globalconst_len)
{
    using namespace ast;
    using enum ast::ast_node_type;

    if (ast_tree == nullptr)
        return makeerr(err::EC_INTERPRETER_AST_NODE_NULL);

    // Create the mapfunc
    std::function<exp_none(ast_node_ptr nd)> mapfunc =
        [&] (ast_node_ptr nd) -> exp_none {
            if (nd->get_node_type() == AST_ENUMVALUE ||
                nd->get_node_type() == AST_EXPR_LITERAL)
            {
                ast::literal const &lit = nd->get_literal();

                // Check type
                ast::literal::literal_type t = lit.get_literal_type();
                auto exp_bitype = ast::literal_to_builtin_type(t);
                if (!exp_bitype)
                    return forwarderr(exp_bitype);
                auto bitype = exp_bitype.value();
                i64 al = builtin_type_alignment(bitype);
                i64 sz = builtin_type_size(bitype);

                auto vi = nd->get_variable_info();
                i64 offset = vi.offset;
                if (offset + sz > globalconst_len)
                    return makeerr(err::EC_INTERPRETER_BAD_CONSTANT_SIZE,
                                   nd->get_effectloc());
                if (offset % al != 0)
                    return makeerr(err::EC_INTERPRETER_BAD_CONSTANT_ALIGNMENT,
                                   nd->get_effectloc());

                // Copy the literal value to the globalconst memory
                pseudocopy(lit,globalconst + offset._v);
            }
            return exp_none();
        };

    auto exp = ast::detail::ast_node_iterate_once(ast_tree,mapfunc);
    if (!exp)
        return exp;
    return exp_none();
}


/**********************************************************************/
/*                                                                    */
/*                        CONVERSION FUNCTIONS                        */
/*                                                                    */
/**********************************************************************/


oparg                           // Oparg of builtin type, or OPARG_COMPOUND
ast_builtin_type_to_oparg(ast::ast_builtin_type bitype)
{
    using enum ast::ast_builtin_type;

    // Return OPARG_COMPOUND for all non-builtins
    switch (bitype)
    {
    case AST_BUILTIN_INVALID:
        return OPARG_COMPOUND;

    case AST_BUILTIN_BOOL:  return OPARG_BOOL;
    case AST_BUILTIN_U8:    return OPARG_U8;
    case AST_BUILTIN_U16:   return OPARG_U16;
    case AST_BUILTIN_U32:   return OPARG_U32;
    case AST_BUILTIN_U64:   return OPARG_U64;
    case AST_BUILTIN_I8:    return OPARG_I8;
    case AST_BUILTIN_I16:   return OPARG_I16;
    case AST_BUILTIN_I32:   return OPARG_I32;
    case AST_BUILTIN_I64:   return OPARG_I64;
    case AST_BUILTIN_STR:   return OPARG_STR;
    case AST_BUILTIN_BYTES: return OPARG_BYTES;

    case AST_COMPOUND_TYPE:
    case AST_VOID_TYPE:
    case AST_LIST_TYPE:
        return OPARG_COMPOUND;
    }
    return OPARG_COMPOUND;
}


ast::ast_builtin_type           // builtin type or AST_BUILTIN_INVALID
ast_builtin_type_from_oparg(oparg arg)
{
    using enum ast::ast_builtin_type;

    // Return AST_BUILTIN_INVALID for all non-builtins
    if ((arg < static_cast<oparg>(0) || arg >= OPARG_COMPOUND) &&
        arg != OPARG_COMPOUND &&
        arg != OPARG_VOID &&
        arg != OPARG_LIST)
        return AST_BUILTIN_INVALID;

    switch (arg)
    {
    case OPARG_BOOL:      return AST_BUILTIN_BOOL;
    case OPARG_U8:        return AST_BUILTIN_U8;
    case OPARG_U16:       return AST_BUILTIN_U16;
    case OPARG_U32:       return AST_BUILTIN_U32;
    case OPARG_U64:       return AST_BUILTIN_U64;
    case OPARG_I8:        return AST_BUILTIN_I8;
    case OPARG_I16:       return AST_BUILTIN_I16;
    case OPARG_I32:       return AST_BUILTIN_I32;
    case OPARG_I64:       return AST_BUILTIN_I64;
    case OPARG_STR:
    case OPARG_STREXPR:   return AST_BUILTIN_STR;
    case OPARG_BYTES:
    case OPARG_BYTESEXPR: return AST_BUILTIN_BYTES;

    case OPARG_COMPOUND:  return AST_COMPOUND_TYPE;
    case OPARG_VOID:      return AST_VOID_TYPE;
    case OPARG_LIST:      return AST_LIST_TYPE;
    }
    return AST_BUILTIN_INVALID;
}


static
exp_none
verify_oparg_bitype_equivalence(
    oparg arg,
    ast::ast_builtin_type bitype,
    cig::source_location sloc   // Source location
    )
{
    using namespace ast;

    ast_builtin_type bitype2 = ast_builtin_type_from_oparg(arg);
    if (bitype != bitype2)
        return makeerr(err::EC_BUILTIN_TYPE_AND_OPARG_NOT_EQUIVALENT,
                       sloc);
    return exp_none();
}


static
oparg
eitype_to_oparg(ast::ast_ei_type eitype)
{
    return static_cast<oparg>(eitype);
}


opstem                          // Unary or binary opstem, or noop
ast_node_type_to_opstem(cig::ast::ast_node_type t)
{
    using namespace cig::ast;
    using enum cig::ast::ast_node_type;

    switch (t)
    {
    case AST_INVALID:
    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_VARIABLE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:
    case AST_FUNBODY:
    case AST_EXPRMIN:
    case AST_EXPR_LITERAL:
    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_UNQUALNAME:
    case AST_EXPR_MEMBER:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_SLICE:
    case AST_EXPR_FUNCALL:
        return OPSTEM_NOOP;

        // Unary operators
    case AST_EXPR_SIGNPLUS:      return OPSTEM_SIGNPLUS;
    case AST_EXPR_SIGNMINUS:     return OPSTEM_SIGNMINUS;
    case AST_EXPR_BIT_NOT:       return OPSTEM_BIT_NOT;
    case AST_EXPR_LOGICAL_NOT:   return OPSTEM_LOGICAL_NOT;

        // Binary operators
    case AST_EXPR_MULT:          return OPSTEM_MULT;
    case AST_EXPR_DIV:           return OPSTEM_DIV;
    case AST_EXPR_MOD:           return OPSTEM_MOD;
    case AST_EXPR_ADD:           return OPSTEM_ADD;
    case AST_EXPR_SUB:           return OPSTEM_SUB;
    case AST_EXPR_SHIFT_LEFT:    return OPSTEM_SHIFT_LEFT;
    case AST_EXPR_SHIFT_RIGHT:   return OPSTEM_SHIFT_RIGHT;
    case AST_EXPR_LESS:          return OPSTEM_LESS;
    case AST_EXPR_GREATER:       return OPSTEM_GREATER;
    case AST_EXPR_LESS_EQUAL:    return OPSTEM_LESS_EQUAL;
    case AST_EXPR_GREATER_EQUAL: return OPSTEM_GREATER_EQUAL;
    case AST_EXPR_EQUAL:         return OPSTEM_EQUAL;
    case AST_EXPR_NOT_EQUAL:     return OPSTEM_NOT_EQUAL;
    case AST_EXPR_IN:            return OPSTEM_NOOP; // OPSTEM_IN
    case AST_EXPR_AND:           return OPSTEM_AND;
    case AST_EXPR_XOR:           return OPSTEM_XOR;
    case AST_EXPR_OR:            return OPSTEM_OR;
    case AST_EXPR_LOGICAL_AND:   return OPSTEM_LOGICAL_AND;
    case AST_EXPR_LOGICAL_OR:    return OPSTEM_LOGICAL_OR;
    case AST_EXPR_CONDITION:     return OPSTEM_NOOP;
    case AST_EXPR_LIST:          return OPSTEM_NOOP;
    case AST_EXPR_ADD_ASSIGN:    return OPSTEM_ADD_ASSIGN;
    case AST_EXPR_SUB_ASSIGN:    return OPSTEM_SUB_ASSIGN;
    case AST_EXPR_MULT_ASSIGN:   return OPSTEM_MULT_ASSIGN;
    case AST_EXPR_DIV_ASSIGN:    return OPSTEM_DIV_ASSIGN;
    case AST_EXPR_MOD_ASSIGN:    return OPSTEM_MOD_ASSIGN;
    case AST_EXPR_SHL_ASSIGN:    return OPSTEM_SHL_ASSIGN;
    case AST_EXPR_SHR_ASSIGN:    return OPSTEM_SHR_ASSIGN;
    case AST_EXPR_AND_ASSIGN:    return OPSTEM_AND_ASSIGN;
    case AST_EXPR_XOR_ASSIGN:    return OPSTEM_XOR_ASSIGN;
    case AST_EXPR_OR_ASSIGN:     return OPSTEM_OR_ASSIGN;
    case AST_EXPR_DOT_ASSIGN:    return OPSTEM_DOT_ASSIGN;
    case AST_EXPR_ASSIGN:        return OPSTEM_ASSIGN;

    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_COMPOUNDSTMT:
    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_IFSTMT:
    case AST_WHILESTMT:
    case AST_DOWHILESTMT:
    case AST_FORSTMT:
    case AST_FOROFSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
    case AST_STMTMAX:
    case AST_MAX:
        return OPSTEM_NOOP;
    }
    return OPSTEM_NOOP;
}


/**********************************************************************/
/*                                                                    */
/*                            FNCODE CLASS                            */
/*                                                                    */
/**********************************************************************/


// The fncode class

inline
fncode::fncode() {
}

inline
fncode::~fncode() {
    if (instr)
    {
        cig_assert(n_instr > 0);
        allocator<instruction,"fncode.instr">::deallocate_and_destruct(
            instr,
            static_cast<std::size_t>(n_instr._v));
        instr = 0;
        n_instr = 0;
    }
}

inline void *
fncode::operator new([[maybe_unused]] std::size_t size) {
    return cig::allocator<fncode,"fncode">::allocate(1);
}
inline void
fncode::operator delete(void *ptr) {
    cig::allocator<fncode,"fncode">::deallocate(static_cast<fncode*>(ptr),1);
}



/**********************************************************************/
/*                                                                    */
/*                   EXPRESSION AUXILIARY FUNCTIONS                   */
/*                                                                    */
/**********************************************************************/


// Get the expression info of a node in the expression tree.
// All nodes in the expression tree have an internal expression info
// except for variables. Variables are usually placed into the expression
// tree by reference (qualified name or entity link), which has an
// expression info.
// However, within a function body, variables are declared within
// expressions, so they can appear directly in the expression tree.
// In that case, we need to construct the expression info.
// The same is true if a function parameter or function return value
// is examined for its expression info.
static
ast::ast_expression_info
get_ast_node_expression_info(
    ast::ast_node_ptr nd        // variable, funpar, funret or expression node
    )
{
    using namespace ast;
    using enum ast::ast_node_type;
    using enum ast::ast_access_kind;

    cig_assert(nd);
    ast_node_type node_type = nd->get_node_type();

    if (node_type == AST_VARIABLE)
    {
        // This function is only called when a variable is
        // examined as part of an expression tree.
        // Such variables are either local or static local (i.e. global).
        // We can deduce the type and address from the variable info.
        ast_variable_info vi = nd->get_variable_info();
        bool is_static = nd->is_static();
        cig_assert(vi.offset >= 0);
        cig_assert(vi.ti.bitype >= 0);

        ast_expression_info ei;
        ei.access_kind = is_static ? GLOBAL_OBJECT : LOCAL_OBJECT;
        ei.offset = vi.offset;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = vi.ti.bitype;
        if (ast_builtin_type_is_builtin(vi.ti.bitype))
            ei.eitype = ast_builtin_type_to_oparg(vi.ti.bitype);
        else
        {
            cig_assert(vi.ti.bitype == AST_COMPOUND_TYPE);
            ei.eitype = OPARG_COMPOUND;
        }
        return ei;
    }
    else if (node_type == AST_FUNPAR ||
             node_type == AST_FUNRET)
    {
        // This function is only called when a funpar/funret is
        // examined as part of an expression tree, perhaps
        // implicitely via a return statement.
        // Such funpar/funret values are always local.
        // We can deduce the type and address from the variable info.
        ast_variable_info vi = nd->get_variable_info();
        cig_assert(vi.offset >= 0);
        cig_assert(vi.ti.bitype >= 0);

        ast_expression_info ei;
        ei.access_kind = LOCAL_OBJECT;
        ei.offset = vi.offset;
        ei.combines_sub = false;
        ei.outer_level = 0;
        ei.bitype = vi.ti.bitype;
        if (ast_builtin_type_is_builtin(vi.ti.bitype))
            ei.eitype = ast_builtin_type_to_oparg(vi.ti.bitype);
        else
        {
            cig_assert(vi.ti.bitype == AST_COMPOUND_TYPE);
            ei.eitype = OPARG_COMPOUND;
        }
        return ei;
    }
    else if (ast_node::ast_node_type_is_expr(node_type))
    {
        auto ei = nd->get_expression_info();
        cig_assert(ei.bitype >= 0);
        return ei;
    }
    else
    {
        // Algorithmic error
        cig_assert(false);
    }
}


/**********************************************************************/
/*                                                                    */
/*                        INSTRUCTION NODE DLL                        */
/*                                                                    */
/**********************************************************************/


// An instruction as a node in a double linked list

struct instrnode : cig::detail::dll::node<instrnode> {
    instruction instr;

    i64 instr_index = -1;       // instruction index in fncode.instr
    instrnode *jump_target = 0; // for jump instructions only
    bool discard = false;       // instruction can be safely discarded

    inline void *operator new([[maybe_unused]] std::size_t size) {
        return cig::allocator<instrnode,"instrnode">::allocate(1);
    }
    inline void operator delete(void *ptr) {
        cig::allocator<instrnode,"instrnode">::deallocate(
            static_cast<instrnode*>(ptr),1);
    }
};

using instrnode_allocator = allocator<instrnode,"instrnode">;

struct instrnodelist {
    instrnode head;

    // Is the list empty?
    bool is_empty() const noexcept
    {
        return !head.is_in_dll();
    }

    // The first node of the list, or nullptr
    instrnode *get_first() const noexcept
    {
        if (!head.is_in_dll())
            return nullptr;
        return head.dll_next;
    }

    // The last node of the list, or nullptr
    instrnode *get_last() const noexcept
    {
        if (!head.is_in_dll())
            return nullptr;
        return head.dll_prev;
    }

    // Append a node to the list
    void append(instrnode *nd) noexcept
    {
        cig_assert(!nd->is_in_dll());
        nd->append_to_dll(head);
    }

    // Prepend a node to the list
    void prepend(instrnode *nd) noexcept
    {
        cig_assert(!nd->is_in_dll());
        nd->prepend_to_dll(head);
    }

    void append_from_list(instrnodelist &b)
    {
        while (b.head.is_in_dll())
        {
            instrnode *nd = b.head.dll_next;
            nd->withdraw_from_dll();
            nd->append_to_dll(head);
        }
    }

    void prepend_from_list(instrnodelist &b)
    {
        while (b.head.is_in_dll())
        {
            instrnode *nd = b.head.dll_prev;
            nd->withdraw_from_dll();
            nd->prepend_to_dll(head);
        }
    }

    void delete_all_nodes() noexcept
    {
        while (head.is_in_dll())
        {
            instrnode *nd = head.dll_next;
            cig_assert(nd && nd != &head);
            nd->withdraw_from_dll();
            delete nd;
        }
    }
    static instrnode *allocate(opcode op) noexcept
    {
        instrnode *nd = new instrnode();

        cig_assert(nd);
        nd->instr.op = op;
        return nd;
    }

    ~instrnodelist() noexcept {
        delete_all_nodes();
    }
};


// Code generation context
struct instrcontext {

    // The instructions are created from the beginning, and are appended.
    //
    // During code generation for a statement or expression, cleanup code
    // for stack unwinding may be generated in parallel.
    // This code is held in local instruction lists in the code generator;
    // when everything is put together, they are appended here.
    //
    // The list owns all its instrnodes.
    instrnodelist instrlist;

    // Only for expression nodes, the destructor list is built at the same
    // time as the constructor list.
    instrnodelist expr_unwind;

    // Compute maximum offset in function for interpreter
    i64 offset_max;
    i64 alignment;

    // Whether local variable offsets must equal those from resolver
    cig_bool validate_resolver_offsets;

    // Use many labels even though they are not jump targets
    cig_bool use_many_labels = true;
};


// This function is similiar to memory_offsets::add_variable
// in cig_ast_resolver.cpp.
i64                             // offset of new variable
context_add_object(
    instrcontext &context,      // offset_max, alignment
    i64 &offset,                // offset adjusted
    i64 sz,                     // size of variable to add
    i64 al)                     // alignment of variable to add
{
    cig_assert(context.alignment >= 0);
    cig_assert(offset >= 0);

    // Alignment must be a power of 2
    cig_assert((al & (al - 1)) == 0);

    if (al > 0)
    {
        if ((offset % al) != 0)
        {
            offset -= offset % al;
            offset += al;
        }
        if (context.alignment < al)
            context.alignment = al;
    }
    i64 old_offset = offset;
    offset += sz;
    if (context.offset_max < offset)
        context.offset_max = offset;
    return old_offset;
}


// This function is similar to context_offsets::max_align_offset
// in cig_ast_resolver.cpp.
void max_align_offset(
    instrcontext &context       // offset_max, alignment
    )
{
    if (context.alignment > 0)
    {
        if ((context.offset_max % context.alignment) != 0)
        {
            context.offset_max -= context.offset_max % context.alignment;
            context.offset_max += context.alignment;
        }
    }
}


static
void
append_code(
    instrnodelist &anchor,      // List where to append
    instrnodelist &list         // List to append
    )
{
    anchor.append_from_list(list);
}


static
void
prepend_code(
    instrnodelist &anchor,      // List where to prepend
    instrnodelist &list         // List to prepend
    )
{
    anchor.prepend_from_list(list);
}


// Append the unwind code and insert a conditional jump statement
// to the upper level's unwinding code.
// The result returned by this function can be directly returned
// from recursive_instrnode_generator().
static
exp_u64                         // uwflags to be handled by caller or error
unwind_and_handle_flags(
    instrcontext &context,      // Context of interpreter code generation
    instrnodelist &list,        // Code to be appended
    u64 uwflags,                // Unwind flags maybe set by the generated code
    instrnode *unwind_start,    // Start of unwind for the generated code
    cig::source_location sloc   // Source location
    )
{
    if (uwflags != 0)
    {
        instrnode *jump = instrnodelist::allocate(OP_JMPANYUWFLAG);
        jump->jump_target = unwind_start;
        jump->instr.comment =
            "more unwinding after "_str +
            u64(sloc.line()).to_str() +
            ":"_str +
            u64(sloc.column()).to_str();
        list.append(jump);
    }

    append_code(context.instrlist,list);

    return uwflags;
}


/**********************************************************************/
/*                                                                    */
/*                          STATEMENT LABELS                          */
/*                                                                    */
/**********************************************************************/


instrnode *
statement_label(ast::ast_node_ptr stmt,str postfix)
{
    using namespace ast;
    using enum ast::ast_node_type;
    cig::source_location sloc = stmt->get_effectloc();
    u64 line = sloc.line();
    u64 column = sloc.column();

    ast_node_type node_type = stmt->get_node_type();
    str basename = "<stmt>"_str;
    if (node_type == AST_FOROFSTMT)
        basename = "forof"_str;
    else if (node_type == AST_FORSTMT)
        basename = "for"_str;
    else if (node_type == AST_WHILESTMT)
        basename = "while"_str;
    else if (node_type == AST_DOWHILESTMT)
        basename = "dowhile"_str;
    else if (node_type == AST_IFSTMT)
        basename = "if"_str;
    else if (node_type == AST_EXPR_LOGICAL_AND)
        basename = "logical_and"_str;
    else if (node_type == AST_EXPR_LOGICAL_OR)
        basename = "logical_or"_str;
    instrnode *label = instrnodelist::allocate(OP_LABEL);
    label->instr.label = basename + "_"_str +
        line.to_str() + "_"_str +
        column.to_str() + postfix;
    return label;
}


/**********************************************************************/
/*                                                                    */
/*                           LOCAL VARIABLE                           */
/*                                                                    */
/**********************************************************************/


static
exp_u64
variable_offset_verification(
    instrcontext &context,      // Context of interpreter code generation
    i64 &offset,                // Offset in local stack memory (updated)
    ast::ast_node_ptr nd        // Variable node
    )
{
    using namespace ast;
    using enum ast::ast_node_type;

    ast_node_type node_type = nd->get_node_type();

    if (node_type != AST_VARIABLE &&
        node_type != AST_FUNPAR &&
        node_type != AST_FUNRET)
        return makealgorithmic("not a variable");

    auto vi = nd->get_variable_info();
    ast_builtin_type bitype = vi.ti.bitype;

    if (!ast_builtin_type_is_builtin(bitype) &&
        bitype != AST_COMPOUND_TYPE)
        return makeerr(err::EC_VARIABLE_NOT_BUILTIN_OR_COMPOUND,
                       nd->get_effectloc());

    if (bitype == AST_COMPOUND_TYPE)
    {
        return makealgorithmic("fixme compound type not yet implemented");
    }

    else
    {
        i64 al = builtin_type_alignment(bitype);
        i64 sz = builtin_type_size(bitype);
        i64 offset_vi = context_add_object(context,offset,sz,al);
        if (context.validate_resolver_offsets &&
            offset_vi != vi.offset)
            return makeerr(err::EC_INTERPRETER_AST_NODE_INCONSISTENT_OFFSET,
                           nd->get_effectloc());
        // This has increased the 'offset' parameter of this function
    }

    return exp_u64();
}


static
exp_u64
variable_destructor_generator(
    instrnodelist &instrlist,   // Generated instructions
    [[maybe_unused]]
    instrnode *unwind_start,    // Start of unwind for this code
    ast::ast_node_ptr nd        // Variable node to generate code for
    )
{
    using namespace ast;
    using enum ast::ast_node_type;

    ast_node_type node_type = nd->get_node_type();

    if (node_type != AST_VARIABLE &&
        node_type != AST_FUNPAR &&
        node_type != AST_FUNRET)
        return makealgorithmic("not a variable");

    auto vi = nd->get_variable_info();
    auto loc = nd->get_effectloc();
    auto br = nd->get_backref();
    auto idx = std::get<i64>(br);

    oparg arg = ast_builtin_type_to_oparg(vi.ti.bitype);
    if (arg == OPARG_COMPOUND)
        return makealgorithmic("fixme destructor not yet implemented");

    opcode op = opstem_to_unary_opcode(OPSTEM_DESTRUCT,arg);
    if (op == OP_NOOP)
        return makealgorithmic("fixme destructor not yet implemented");
    instrnode *destr = instrnodelist::allocate(op);
    destr->instr.comment =
        (node_type == AST_FUNPAR ? str("par"_str) :
         node_type == AST_FUNRET ? str("ret"_str) :
         str("var"_str)) +
        " "_str +
        (node_type == AST_FUNRET ? idx.to_str() :
         nd->get_name()) +
        " at "_str +
        u64(loc.line()).to_str() +
        ":"_str +
        u64(loc.column()).to_str();
    destr->instr.arg1 = vi.offset;
    instrlist.append(destr);

    // If variable destruction might give an exception,
    // this needs to be handled and a nonzero uwflags
    // needs to be returned.
    return exp_u64();
}


static
exp_u64
variable_constructor_generator(
    instrnodelist &instrlist,   // Generated instructions
    [[maybe_unused]]
    instrnode *unwind_start,    // Start of unwind for this code
    ast::ast_node_ptr nd        // Variable node to generate code for
    )
{
    using namespace ast;
    using enum ast::ast_node_type;

    ast_node_type node_type = nd->get_node_type();

    if (node_type != AST_VARIABLE &&
        node_type != AST_FUNPAR &&
        node_type != AST_FUNRET)
        return makealgorithmic("not a variable");

    auto vi = nd->get_variable_info();
    auto loc = nd->get_effectloc();
    auto br = nd->get_backref();
    auto idx = std::get<i64>(br);

    oparg arg = ast_builtin_type_to_oparg(vi.ti.bitype);
    if (arg == OPARG_COMPOUND)
        return makealgorithmic("fixme destructor not yet implemented");

    opcode op = opstem_to_unary_opcode(OPSTEM_CONSTRUCT,arg);
    if (op == OP_NOOP)
        return makealgorithmic("fixme constructor not yet implemented");
    instrnode *constr = instrnodelist::allocate(op);
    constr->instr.comment =
        (node_type == AST_FUNPAR ? str("par"_str) :
         node_type == AST_FUNRET ? str("ret"_str) :
         str("var"_str)) +
        " "_str +
        (node_type == AST_FUNRET ? idx.to_str() :
         nd->get_name()) +
        " at "_str +
        u64(loc.line()).to_str() +
        ":"_str +
        u64(loc.column()).to_str();
    constr->instr.arg1 = vi.offset;
    instrlist.append(constr);

    // If variable destruction might give an exception,
    // this needs to be handled and a nonzero uwflags
    // needs to be returned.
    return exp_u64();
}


/**********************************************************************/
/*                                                                    */
/*               RECURSIVE FUNDEF INSTRUCTION GENERATOR               */
/*                                                                    */
/**********************************************************************/


/* How code generation works:


   1. Code generation for statements

   Each call of the recursive_instrnode_generator creates code for one
   statement or expression. Let's call it the 'current task' to generate
   this code.

   The regular control flow is like this:

   A) Code before the current task is already in context.instrlist.

   B) Control flow enters 'from the top', i.e. from the end of the
      context.instrlist instructions.

   C) The current task typically consists of
      a) build-up of variables, and various intermediate code
         implementing the logic of the statement/expression, and
         recursive calls to recursive_instrnode_generator to
         implement sub-tasks of the current task,
         all of that heavily intertwined;
      b) cleanup code to 'unwind' the stack, i.e. to destruct
         all temporaries and variables; and
      c) a check whether control flow can proceed linearly after
         cleanup or whether the stack unwinding must immediately
         continue for the outer block.

   D) After cleanup if no further uwinding needs to be done (if no
      exceptions or return/break/continue statements must be handled),
      the control flow leaves the current task 'at the bottom', i.e.
      at the end of context.instrlist.
      The next statement or expression can continue to append to
      context.instrlist.


   2. The onion principle: statements versus expressions

   Code is generated like an onion from outside to inside. The code
   that builds up first needs to be cleaned up last.

   However, what is the outer layer of the onion and what is the inner
   layer is different for statements and expressions.

   For statements, the encapsulating statement (the parent node in the
   AST tree) generates the outer code and the sub-statements (the child
   nodes in the AST tree) generate the inner code.
   In other words, each statement with its sub-statements is self-contained;
   it builds up, then cleans up, and it is done.

   For expressions, it's the other way around. The combining expressions
   (the parent node in the AST tree) generates the inner code and the
   sub-expressions (the child nodes in the AST tree) generate the outer code.
   This is because expressions need to be evaluated from the sub-expression
   up to the combining expression, and their results need to be destroyed
   in the reverse order.

   For this reason, an expression is not self-contained; its constructive
   and its destructive code are at two different places in the instruction
   list, and inbetween them the code for the parent expression is located.
   The expression code generator cannot combine these two parts but needs
   to pass up both of them separately to the caller.

   The code generation context therefore contains a second instruction list
   called expr_unwind, and code generation for each expression adds to both
   the regular instrlist and the expr_unwind list. It appends to instrlist
   and prepends to the expr_unwind list.

   It is the job of the statement containing the expression to merge both
   lists together.


   3. Handling of exceptional situations

   Exceptional situations are: return, break, continue, or an exception
   in a function call or opcode statement (division by zero, bad index
   etc.). See doc/scope_unwinding.txt for an explanation of these
   situations.

   All exceptional situations are handled by leaving the current regular
   code path and jumping to cleanup code. From there, the stack is being
   unwound and all constructed objects are destructed in reverse order.

   Cleanup (stack unwinding) proceeds across several nesting levels
   (e.g. enclosing compound statements) until the location of handling
   is reached.

   This section describes how it is implemented.

   3.1. Runtime versus 'compile-time' handling

   For unwinding, one must make a distinction between runtime handling
   and code-generation handling.

   At runtime, each code part that may create an exceptional situation
   (exception, return, break, continue) must handle it locally.

   3.2. Runtime handling consists of two actions:

   o  Setting the proper bit in the uwflags variable of fnruntime
      (the function's runtime state)

   o  Jumping to the current 'unwind_start' jump target to start
      stack unwinding.

   3.3. Compile-time handling consists of the following actions:

   o  Each recursive call for a substatement or expression is
      passed an 'unwind_start' jump target so that it can
      generate the jump instruction.
      This 'unwind_start' is passed down to the recursive call.

   o  If any generated code may at runtime produce an exceptional
      situation, the code generator must set the uwflags variable
      and return it to the caller. This informs the caller that
      the code just generated may cause such an exceptional situation.
      The 'uwflags' is passed up from the recursive call.

   o  Any exceptional situation that must be handled by the current
      task (e.g. break and continue statements are handled by while,
      for etc.) must generate code.
      Consequently, the bits for these situations handled by the
      current task can be cleared in the uwflags.

   o  At the end of the generated code for a statement or expression,
      before returning to the caller, if the generated code and all
      its sub-codes may have produced an exceptional situation (if
      the uwflags is nonzero), it must append a conditional call to
      the original unwind_start passed to it (JMPANYUWFLAG), so that
      the runtime code will proceed with stack unwinding at the
      enclosing level.


   4. Intermediate lists

   Sometimes the code for the current task needs to be combined from
   several fragments. The fragments can be single instructions that
   need to be created to provide the recursive calls with an unwind_start
   jump target.

   Such code fragments can be held in intermediate lists during code
   generation of a single task, which can then be appended to the
   context.instrlist in order.

   Single instructions should never be held on its own without being
   inserted into a list, since the list destructor will properly free
   the list nodes whereas a single plain C instruction pointer would
   need to be deleted separately.

*/


static
exp_u64                         // Unwind situation flags created by sub-task
recursive_instrnode_generator(
    instrcontext &context,      // Context of interpreter code generation
    i64 offset,                 // Offset of local stack before AST node
    instrnode *unwind_start,    // Start of unwind for this node's context
    ast::ast_node_ptr nd        // Current node to generate code for
    )
{
    using namespace ast;
    using enum ast::ast_node_type;
    using enum ast::ast_access_kind;
    using enum unwind_flags;

    if (nd == nullptr)
        return makeerr(err::EC_INTERPRETER_BAD_AST_TOPNODE);

    ast_node_type node_type = nd->get_node_type();

    // Check the unwind_start pointer
    if (unwind_start == nullptr)
        return makeerr(err::EC_INTERPRETER_BAD_INSTRCONTEXT,
                       nd->get_effectloc());

    // Save the unwind_start on entering this function
    auto unwind_start_saved = unwind_start;

    switch (node_type)
    {
    case AST_INVALID:
    case AST_EXPRMIN:
    case AST_EXPRMAX:
    case AST_STMTMIN:
    case AST_STMTMAX:
    case AST_MAX:
        return makeerr(err::EC_INTERPRETER_ILLEGAL_AST_NODE_TYPE);

    case AST_GLOBAL:
    case AST_NAMESPACE:
    case AST_CLASSDECL:
    case AST_CLASSDEF:
    case AST_ENUMDECL:
    case AST_ENUMDEF:
    case AST_ENUMVALUE:
    case AST_OVERLOADSET:
    case AST_FUNDECL:
    case AST_FUNDEF:
        return makeerr(err::EC_INTERPRETER_AST_NODE_OUTSIDE_FUNCTION,
                       nd->get_effectloc());

    case AST_FUNBODY:
    {
        u64 uwflags;

        // Iterate over the function body compound statement.
        i64 n = nd->get_subnode_count();
        if (n > 1)
            return makealgorithmic("AST node funbody has more than one child");
        ast_node_ptr sub = nd->get_subnode(0);
        if (!sub)
            return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);

        // Resolve in the function body
        auto exp = recursive_instrnode_generator(context,offset,
                                                 unwind_start,sub);
        if (!exp)
            return exp;
        uwflags |= exp.value();
        // The UW_RETURN and UW_EXCEPTION flags are allowed,
        // the others are not.
        if ((uwflags & ~(U64_RETURN | U64_EXCEPTION)) != 0)
            return makeerr(err::EC_UWFLAGS_NOT_HANDLED,
                           nd->get_effectloc());

        return exp_u64(uwflags);
    }

    case AST_EXPR_LITERAL:
    {
        ast::literal const &lit = nd->get_literal();

        // Check type
        ast::literal::literal_type t = lit.get_literal_type();
        auto exp_bitype = ast::literal_to_builtin_type(t);
        if (!exp_bitype)
            return forwarderr(exp_bitype);
        ast::ast_builtin_type bitype = exp_bitype.value();
        oparg arg = ast_builtin_type_to_oparg(bitype);
        if (arg == OPARG_COMPOUND)
            return makeerr(err::EC_BAD_LITERAL_TYPE,
                           nd->get_effectloc());

        // Size and alignment
        i64 sz = builtin_type_size(bitype);
        i64 al = builtin_type_alignment(bitype);

        // This is a temporary, so no resolver offsets validation
        ast_expression_info ei;
        ei.access_kind = LOCAL_OBJECT;
        ei.bitype = bitype;
        ei.eitype = arg;
        ei.offset = context_add_object(context,offset,sz,al);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return forwarderr(expsetei);

        // Generate the code to copy from globalconst at runtime
        auto vi = nd->get_variable_info();
        opcode op = opstem_to_unary_opcode(OPSTEM_COPYLIT,arg);
        if (op == OP_NOOP)
            return makealgorithmic("fixme copy-literal not yet implemented");
        instrnode *copylit = instrnodelist::allocate(op);
        copylit->instr.comment = "literal "_str + lit.to_str();
        copylit->instr.arg1 = vi.offset;
        copylit->instr.arg2 = ei.offset;
        context.instrlist.append(copylit);

#if 1
        // Note: Since pseudocopy creates a model literal in globalconst
        //       that can be memcpy-copied and does not require allocation
        //       (as it references to the allocated string in this AST tree)
        //       a destructor is not required.
        opcode op2 = opstem_to_unary_opcode(OPSTEM_DESTRUCT,arg);
        if (op2 == OP_NOOP)
            return makealgorithmic("fixme destruct not yet implemented");
        instrnode *destrlit = instrnodelist::allocate(op2);
        destrlit->instr.comment = "destruct literal "_str + lit.to_str();
        destrlit->instr.arg1 = ei.offset;
        context.expr_unwind.prepend(destrlit);
#endif
        return exp_u64();
    }

    case AST_VARIABLE:
    {
        // Get type
        ast_variable_info vi = nd->get_variable_info();
        ast_builtin_type bitype = vi.ti.bitype;

        if (!ast_builtin_type_is_builtin(bitype) &&
            bitype != AST_COMPOUND_TYPE)
            return makeerr(err::EC_VARIABLE_NOT_BUILTIN_OR_COMPOUND,
                           nd->get_effectloc());
        if (bitype == AST_COMPOUND_TYPE)
            return makealgorithmic("fixme compound type not yet implemented");
        else
        {
            cig_assert(vi.ti.type_size == builtin_type_size(bitype));
            cig_assert(vi.ti.type_alignment == builtin_type_alignment(bitype));
        }

        // Get expression info
        ast_expression_info ei = get_ast_node_expression_info(nd);

        // Verify the expression info from the resolver
        oparg res = eitype_to_oparg(ei.eitype);
        auto exp_verify =
            verify_oparg_bitype_equivalence(res,bitype,
                                            nd->get_effectloc());
        if (!exp_verify)
            return forwarderr(exp_verify);

        if (ei.access_kind != LOCAL_OBJECT ||
            ei.combines_sub != false ||
            ei.outer_level != 0 ||
            eitype_to_oparg(ei.eitype) != res)
            return makeerr(err::EC_INTERPRETER_EI_INCONSISTENT,
                           nd->get_effectloc());

        // Verification of the variable's offset
        // is done by the statement which owns it.
        // Here, we are just accessing it as if it had
        // been referenced by a qualified name.
        // Therefore its offset should be below the
        // input offset but otherwise we couldn't say much.
        if (vi.offset >= offset)
            return makeerr(err::EC_INTERPRETER_AST_NODE_INCONSISTENT_OFFSET,
                           nd->get_effectloc());

        // Also, no need to set the expression info.
        // We are done.

        return exp_u64();
    }

    case AST_EXPR_UNQUALNAME:
    {
        // Note: Not for every qualified name in the program,
        // the interpreter calls this recursive function for
        // the very first component, the unqualified name.
        //
        // In case the chain of names contains data types (classes),
        // the interpreter code generation starts at the first
        // variable after the last data type.

        auto re = nd->get_resolved_entity();
        auto ei = nd->get_expression_info();

        // Not an object at all?
        if (ei.access_kind == NOT_ACCESSIBLE)
            return makealgorithmic("unqualified name not accessible");
        else if (ei.access_kind == NO_ACCESS_TYPE_OR_NMSP)
            return makealgorithmic("unqualified name is type or namespace");
        else if (ei.access_kind == GLOBAL_FUNCTION)
            return makealgorithmic("unqualified name is global function");
        else if (ei.access_kind == CLASS_FUNCTION)
            return makealgorithmic("unqualified name is this member function");
        else if (ei.access_kind == MEMORY_FUNCTION)
            return makealgorithmic("unqualified name is class object function");


        // Check the result oparg and verify the consistency
        auto bitype = ei.bitype;
        oparg res;
        if (ast_builtin_type_is_builtin(ei.bitype))
            res = ast_builtin_type_to_oparg(bitype);
        else if (ei.bitype == AST_COMPOUND_TYPE)
        {
            // The resolved variable must be in std::get<ast_node_ptr>(re).
            res = OPARG_COMPOUND;
        }
        else
            return makeerr(err::EC_EXPRESSION_NOT_BUILTIN_OR_COMPOUND,
                           nd->get_effectloc());

        // The equivalence is pretty obvious but we still verify it
        auto exp_verify =
            verify_oparg_bitype_equivalence(res,ei.bitype,
                                            nd->get_effectloc());
        if (!exp_verify)
            return forwarderr(exp_verify);

        // Handle local objects
        if (ei.access_kind == LOCAL_OBJECT)
        {
            ei.eitype = res;
        }
        else
        {
            // Note: For non-local objects, the result of an expression
            // (unqualified name, member name, entity link, member link)
            // is either an address in memory or a shared object pointer.
            // This address or the pointer should be copied to local memory
            // and the expression info should hold the local memory
            // address of that plain pointer or shared pointer.

            return makealgorithmic("non-local-object not yet implemented");
        }

        // Set the modified ei
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return forwarderr(expsetei);

        return exp_u64();
    }

    case AST_EXPR_ENTITYLINK:
    case AST_EXPR_MEMBERLINK:
    case AST_EXPR_MEMBER:
    case AST_EXPR_ARRAYDEREF:
    case AST_EXPR_SLICE:
    case AST_EXPR_FUNCALL:
        return exp_u64();

    case AST_EXPR_SIGNPLUS:
    case AST_EXPR_SIGNMINUS:
    case AST_EXPR_BIT_NOT:
    case AST_EXPR_LOGICAL_NOT:
    {
        u64 uwflags;

        // Save the first instruction on the expr_unwind list
        auto saved_expr_unwind_first = context.expr_unwind.get_first();


        // Expression info of subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_UNARY_HAS_NO_OPERAND,
                           nd->get_effectloc());
        auto exp1 = recursive_instrnode_generator(context,offset,
                                                  unwind_start,subnd1);
        if (!exp1)
            return exp1;
        auto uwflags1 = exp1.value();
        uwflags |= uwflags1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1) &&
            bitype1 != AST_LIST_TYPE)
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // If the subnode contains a funcall, it might have become a list
        if (bitype1 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                           nd->get_effectloc());

        // Verify equivalence of first oparg
        oparg arg1 = eitype_to_oparg(subei1.eitype);
        auto exp_verify1 =
            verify_oparg_bitype_equivalence(arg1,bitype1,
                                            subnd1->get_effectloc());
        if (!exp_verify1)
            return forwarderr(exp_verify1);

        // Adjust unwind_start
        if (context.expr_unwind.get_first() != saved_expr_unwind_first)
            unwind_start = context.expr_unwind.get_first();


        // The operation stem
        opstem stem = ast_node_type_to_opstem(node_type);

        // The operation result
        oparg res = oparg_unary_result_type(stem,arg1);
        if (res == OPARG_COMPOUND)
            return makeerr(err::EC_ALGORITHMIC_UNARY_OP_IMPOSSIBLE,
                           nd->get_effectloc());
        auto bitype = ast_builtin_type_from_oparg(res);
        if (!ast_builtin_type_is_builtin(bitype))
            return makeerr(err::EC_ALGORITHMIC_UNARY_OP_IMPOSSIBLE,
                           nd->get_effectloc());
        i64 al = builtin_type_alignment(bitype);
        i64 sz = builtin_type_size(bitype);

        // Verify the expression info from the resolver
        auto ei = nd->get_expression_info();
        auto exp_verify3 =
            verify_oparg_bitype_equivalence(res,ei.bitype,nd->get_effectloc());
        if (!exp_verify3)
            return forwarderr(exp_verify3);

        if (ei.access_kind != LOCAL_OBJECT ||
            ei.combines_sub != false ||
            ei.outer_level != 0)
            return makeerr(err::EC_INTERPRETER_EI_INCONSISTENT,
                           nd->get_effectloc());

        // Set new expression info
        ei.eitype = res;
        ei.offset = context_add_object(context,offset,sz,al);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return forwarderr(expsetei);

        // Generate the code to evaluate the binary expression
        if (subei1.access_kind != LOCAL_OBJECT) // fixme
            return makealgorithmic("non-local operand");

        // The instruction
        opcode op = opstem_to_unary_opcode(stem,arg1);
        if (op == OP_NOOP)
            return makealgorithmic("unary opcode missing");
        auto sloc = nd->get_effectloc();

        auto opinstr = instrnodelist::allocate(op);
        opinstr->instr.arg1 = subei1.offset;
        opinstr->instr.res = ei.offset;
        opinstr->instr.comment =
            str::from_ascii_c_str(opstem_symbol(stem)) +
            " at "_str +
            u64(sloc.line()).to_str() +
            ":"_str +
            u64(sloc.column()).to_str();

        context.instrlist.append(opinstr);

        return uwflags;
    }

    case AST_EXPR_MULT:
    case AST_EXPR_DIV:
    case AST_EXPR_MOD:
    case AST_EXPR_ADD:
    case AST_EXPR_SUB:
    case AST_EXPR_SHIFT_LEFT:
    case AST_EXPR_SHIFT_RIGHT:
    case AST_EXPR_LESS:
    case AST_EXPR_GREATER:
    case AST_EXPR_LESS_EQUAL:
    case AST_EXPR_GREATER_EQUAL:
    case AST_EXPR_EQUAL:
    case AST_EXPR_NOT_EQUAL:
    case AST_EXPR_IN:
    case AST_EXPR_AND:
    case AST_EXPR_XOR:
    case AST_EXPR_OR:
    case AST_EXPR_LOGICAL_AND:
    case AST_EXPR_LOGICAL_OR:
    {
        u64 uwflags;
        bool is_logical_and = (node_type == AST_EXPR_LOGICAL_AND);
        bool is_logical_or  = (node_type == AST_EXPR_LOGICAL_OR);

        // Save the first instruction on the expr_unwind list
        auto saved_expr_unwind_first = context.expr_unwind.get_first();


        // Expression info of first subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_BINARY_HAS_NO_OPERAND1,
                           nd->get_effectloc());
        auto exp1 = recursive_instrnode_generator(context,offset,
                                                  unwind_start,subnd1);
        if (!exp1)
            return exp1;
        auto uwflags1 = exp1.value();
        uwflags |= uwflags1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1) &&
            bitype1 != AST_LIST_TYPE)
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // If the subnode contains a funcall, it might have become a list
        if (bitype1 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                           nd->get_effectloc());

        // Verify equivalence of first oparg
        oparg arg1 = eitype_to_oparg(subei1.eitype);
        auto exp_verify1 =
            verify_oparg_bitype_equivalence(arg1,bitype1,
                                            subnd1->get_effectloc());
        if (!exp_verify1)
            return forwarderr(exp_verify1);

        // Adjust unwind_start
        if (context.expr_unwind.get_first() != saved_expr_unwind_first)
            unwind_start = context.expr_unwind.get_first();


        // For the short-circuiting operators && and ||
        // avoid evaluating the second expression if possible
        instrnodelist skip_arg2_target_list; // hold target intermediately
        instrnode *skip_arg2_unwind = nullptr;
        instrnode *preset_result = nullptr;
        if (is_logical_and || is_logical_or)
        {
            // First argument must be a bool
            if (arg1 != OPARG_BOOL)
                return makeerr(err::EC_LOGICAL_OPERATION_NEEDS_BOOL_ARGUMENTS,
                               nd->get_effectloc());

            // Preset the result with the result of the first argument
            preset_result = instrnodelist::allocate(OP_COPYMEM_BOOL_BOOL);
            preset_result->instr.arg1 = subei1.offset;
            preset_result->instr.comment = "preset with first bool result"_str;
            context.instrlist.append(preset_result);

            // Create a label as the target to skip evaluation of arg2
            auto skip_arg2_target = 
                statement_label(nd,"skipped_second"_str);
            skip_arg2_target_list.append(skip_arg2_target);

            // Create a label as the target to skip unwinding of arg2
            skip_arg2_unwind =
                statement_label(nd,"unwind_first"_str);
            context.expr_unwind.prepend(skip_arg2_unwind);
            unwind_start = skip_arg2_unwind;

            // Create the jump over the evaluation of the second argument
            auto jmpover =
                instrnodelist::allocate(is_logical_and ?
                                        OP_JMPFALSE : OP_JMPTRUE);
            jmpover->instr.comment = (is_logical_and ?
                                      str("short-circuit &&"_str) :
                                      str("short-circuit ||"_str));
            jmpover->jump_target = skip_arg2_target;
            jmpover->instr.arg1 = subei1.offset;
        }


        // Expression info of second subnode
        auto subnd2 = nd->get_subexpr2();
        if (subnd2 == nullptr)
            return makeerr(err::EC_AST_BINARY_HAS_NO_OPERAND2,
                           nd->get_effectloc());
        auto exp2 = recursive_instrnode_generator(context,offset,
                                                  unwind_start,subnd2);
        if (!exp2)
            return exp2;
        auto uwflags2 = exp2.value();
        uwflags |= uwflags2;
        ast_expression_info subei2 = get_ast_node_expression_info(subnd2);

        // Builtin type of second subnode
        ast_builtin_type bitype2 = subei2.bitype;
        if (!ast_builtin_type_is_builtin(bitype2) &&
            bitype2 != AST_LIST_TYPE)
            return makeerr(err::EC_OPERAND_NOT_BUILTIN_TYPE,
                           nd->get_effectloc(),
                           subnd2->get_effectloc());

        // If the subnode contains a funcall, it might have become a list
        if (bitype2 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_OPERATION_IMPOSSIBLE,
                           nd->get_effectloc());

        // Verify equivalence of second oparg
        oparg arg2 = eitype_to_oparg(subei2.eitype);
        auto exp_verify2 =
            verify_oparg_bitype_equivalence(arg2,bitype2,
                                            subnd2->get_effectloc());
        if (!exp_verify2)
            return forwarderr(exp_verify2);

        // Adjust unwind_start
        if (context.expr_unwind.get_first() != saved_expr_unwind_first)
            unwind_start = context.expr_unwind.get_first();


        // The operation stem
        opstem stem = ast_node_type_to_opstem(node_type);

        // The operation result
        oparg res = oparg_binary_result_type(stem,arg1,arg2);
        if (res == OPARG_COMPOUND)
            return makeerr(err::EC_ALGORITHMIC_BINARY_OP_IMPOSSIBLE,
                           nd->get_effectloc());
        auto bitype = ast_builtin_type_from_oparg(res);
        if (!ast_builtin_type_is_builtin(bitype))
            return makeerr(err::EC_ALGORITHMIC_BINARY_OP_IMPOSSIBLE,
                           nd->get_effectloc());
        i64 al = builtin_type_alignment(bitype);
        i64 sz = builtin_type_size(bitype);

        // Verify the expression info from the resolver
        auto ei = nd->get_expression_info();
        auto exp_verify3 =
            verify_oparg_bitype_equivalence(res,ei.bitype,nd->get_effectloc());
        if (!exp_verify3)
            return forwarderr(exp_verify3);

        if (ei.access_kind != LOCAL_OBJECT ||
            ei.combines_sub != false ||
            ei.outer_level != 0)
            return makeerr(err::EC_INTERPRETER_EI_INCONSISTENT,
                           nd->get_effectloc());

        // Set new expression info
        ei.eitype = res;
        ei.offset = context_add_object(context,offset,sz,al);
        auto expsetei = nd->set_expression_info(ei);
        if (!expsetei)
            return forwarderr(expsetei);

        // Generate the code to evaluate the binary expression
        if (subei1.access_kind != LOCAL_OBJECT) // fixme
            return makealgorithmic("non-local operand");
        if (subei2.access_kind != LOCAL_OBJECT) // fixme
            return makealgorithmic("non-local operand");


        // For the short-circuiting operators && and ||
        // avoid evaluating the second expression if possible
        if (is_logical_and || is_logical_or)
        {
            // Second argument must be a bool
            if (arg2 != OPARG_BOOL)
                return makeerr(err::EC_LOGICAL_OPERATION_NEEDS_BOOL_ARGUMENTS,
                               nd->get_effectloc());

            // Set the copy address of the copy instruction
            preset_result->instr.arg2 = ei.offset;

            // If we have evaluated the second argument,
            // copy second result into total result
            auto copy_second = instrnodelist::allocate(OP_COPYOBJ_BOOL_BOOL);
            copy_second->instr.arg1 = subei2.offset;
            copy_second->instr.arg2 = ei.offset;
            context.instrlist.append(copy_second);

            // Insert jump target label
            context.instrlist.append_from_list(skip_arg2_target_list);

            // Create the jump to short-circuit unwinding
            auto jmp_skipuw = instrnodelist::allocate(is_logical_and ?
                                                      OP_JMPFALSE : OP_JMPTRUE);
            jmp_skipuw->instr.comment = (is_logical_and ?
                                         str("short-circuit && unwind"_str) :
                                         str("short-circuit || unwind"_str));
            jmp_skipuw->instr.arg1 = subei1.offset;
            jmp_skipuw->jump_target = skip_arg2_unwind;
            context.expr_unwind.prepend(jmp_skipuw);

            // Return with the uwflags
            return uwflags;
        }


        // The instruction
        opcode op = opstem_to_binary_opcode(stem,arg1,arg2);
        if (op == OP_NOOP)
            return makealgorithmic("binary opcode missing");
        auto sloc = nd->get_effectloc();

        instrnodelist opinstrlist;
        auto opinstr = instrnodelist::allocate(op);
        opinstrlist.append(opinstr);
        opinstr->instr.arg1 = subei1.offset;
        opinstr->instr.arg2 = subei2.offset;
        opinstr->instr.res = ei.offset;
        opinstr->instr.comment =
            str::from_ascii_c_str(opstem_symbol(stem)) +
            " at "_str +
            u64(sloc.line()).to_str() +
            ":"_str +
            u64(sloc.column()).to_str();

        // Prepend a test for division by zero
        u64 uwflags_op;
        if (stem == OPSTEM_DIV || stem == OPSTEM_MOD)
        {
            // Jump over the exception if arg2 is nonzero
            opcode opjmp = opstem_to_unary_opcode(OPSTEM_JMPNONZERO,arg2);
            auto jmpinstr = instrnodelist::allocate(opjmp);
            jmpinstr->jump_target = opinstr;
            jmpinstr->instr.arg1 = subei2.offset;
            jmpinstr->instr.comment =
                "skip exception if divisor is nonzero"_str;
            context.instrlist.append(jmpinstr);

            // Raise an exception
            auto excptinstr = instrnodelist::allocate(OP_NOOP);
            excptinstr->instr.comment =
                "fixme exception division by zero not yet implemented"_str;
            context.instrlist.append(excptinstr);
            auto flaginstr = instrnodelist::allocate(OP_SETEXCEPT);
            flaginstr->instr.comment = "set except flag"_str;
            context.instrlist.append(flaginstr);
            auto uwjmpinstr = instrnodelist::allocate(OP_JMP);
            uwjmpinstr->jump_target = unwind_start;
            uwjmpinstr->instr.comment = "unwind stack"_str;
            context.instrlist.append(uwjmpinstr);

            uwflags |= U64_EXCEPTION;
        }

        context.instrlist.append_from_list(opinstrlist);

        return uwflags;
    }

    case AST_EXPR_CONDITION:
        return exp_u64();       // fixme missing code

    case AST_EXPR_LIST:
        return exp_u64();       // fixme missing code

    case AST_EXPR_ADD_ASSIGN:
    case AST_EXPR_SUB_ASSIGN:
    case AST_EXPR_MULT_ASSIGN:
    case AST_EXPR_DIV_ASSIGN:
    case AST_EXPR_MOD_ASSIGN:
    case AST_EXPR_SHL_ASSIGN:
    case AST_EXPR_SHR_ASSIGN:
    case AST_EXPR_AND_ASSIGN:
    case AST_EXPR_XOR_ASSIGN:
    case AST_EXPR_OR_ASSIGN:
    case AST_EXPR_DOT_ASSIGN:
    case AST_EXPR_ASSIGN:
    {
        u64 uwflags;

        // fixme need to consider list assignments
        // for lists with lvalues on the rhs, save the values
        // in intermediate storage before assigning
        // to achieve atomic assignment

        // Save the first instruction on the expr_unwind list
        auto saved_expr_unwind_first = context.expr_unwind.get_first();

        bool is_simple_assignment = (node_type == AST_EXPR_DOT_ASSIGN ||
                                     node_type == AST_EXPR_ASSIGN);


        // Assignments are evaluated right-to-left


        // Expression info of second subnode
        auto subnd2 = nd->get_subexpr2();
        if (subnd2 == nullptr)
            return makeerr(err::EC_AST_BINARY_HAS_NO_OPERAND2,
                           nd->get_effectloc());
        auto exp2 = recursive_instrnode_generator(context,offset,
                                                  unwind_start,subnd2);
        if (!exp2)
            return exp2;
        auto uwflags2 = exp2.value();
        uwflags |= uwflags2;
        ast_expression_info subei2 = get_ast_node_expression_info(subnd2);

        // Builtin type of second subnode
        ast_builtin_type bitype2 = subei2.bitype;
        if (!ast_builtin_type_is_builtin(bitype2) &&
            bitype2 != AST_COMPOUND_TYPE &&
            bitype2 != AST_LIST_TYPE)
            return makeerr(err::EC_EXPRESSION_NOT_BUILTIN_OR_COMPOUND,
                           nd->get_effectloc(),
                           subnd2->get_effectloc());

        // Lists are only allowed for simple assignment
        if (!is_simple_assignment && bitype2 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                           nd->get_effectloc());

        // Compound types are only allowed for simple assignment
        if (!is_simple_assignment && bitype2 == AST_COMPOUND_TYPE)
            return makeerr(err::EC_COMPOUND_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                           nd->get_effectloc());

        // Verify equivalence of second oparg
        oparg arg2 = eitype_to_oparg(subei2.eitype);
        auto exp_verify2 =
            verify_oparg_bitype_equivalence(arg2,bitype2,
                                            subnd2->get_effectloc());
        if (!exp_verify2)
            return forwarderr(exp_verify2);

        // Adjust unwind_start
        if (context.expr_unwind.get_first() != saved_expr_unwind_first)
            unwind_start = context.expr_unwind.get_first();


        // Expression info of first subnode
        auto subnd1 = nd->get_subexpr1();
        if (subnd1 == nullptr)
            return makeerr(err::EC_AST_BINARY_HAS_NO_OPERAND1,
                           nd->get_effectloc());
        auto exp1 = recursive_instrnode_generator(context,offset,
                                                  unwind_start,subnd1);
        if (!exp1)
            return exp1;
        auto uwflags1 = exp1.value();
        uwflags |= uwflags1;
        ast_expression_info subei1 = get_ast_node_expression_info(subnd1);

        // Builtin type of first subnode
        ast_builtin_type bitype1 = subei1.bitype;
        if (!ast_builtin_type_is_builtin(bitype1) &&
            bitype1 != AST_COMPOUND_TYPE &&
            bitype1 != AST_LIST_TYPE)
            return makeerr(err::EC_EXPRESSION_NOT_BUILTIN_OR_COMPOUND,
                           nd->get_effectloc(),
                           subnd1->get_effectloc());

        // Lists are only allowed for simple assignment
        if (!is_simple_assignment && bitype1 == AST_LIST_TYPE)
            return makeerr(err::EC_LIST_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                           nd->get_effectloc());

        // Compound types are only allowed for simple assignment
        if (!is_simple_assignment && bitype1 == AST_COMPOUND_TYPE)
            return makeerr(err::EC_COMPOUND_SPECIAL_ASSIGNMENT_IMPOSSIBLE,
                           nd->get_effectloc());

        // Verify equivalence of first oparg
        oparg arg1 = eitype_to_oparg(subei1.eitype);
        auto exp_verify1 =
            verify_oparg_bitype_equivalence(arg1,bitype1,
                                            subnd1->get_effectloc());
        if (!exp_verify1)
            return forwarderr(exp_verify1);

        // Adjust unwind_start
        if (context.expr_unwind.get_first() != saved_expr_unwind_first)
            unwind_start = context.expr_unwind.get_first();


        // The operation stem
        opstem stem = ast_node_type_to_opstem(node_type);

        // The operation result
        if (ast_builtin_type_is_builtin(bitype1) &&
            ast_builtin_type_is_builtin(bitype2))
        {
            oparg res = oparg_binary_result_type(stem,arg1,arg2);
            if (res != OPARG_VOID)
                return makeerr(err::EC_ALGORITHMIC_ASSIGNMENT_IMPOSSIBLE,
                               nd->get_effectloc());

            // auto bitype = AST_VOID_TYPE;

            // Verify the expression info from the resolver
            auto ei = nd->get_expression_info();
            auto exp_verify3 =
                verify_oparg_bitype_equivalence(res,ei.bitype,
                                                nd->get_effectloc());
            if (!exp_verify3)
                return forwarderr(exp_verify3);

            if (ei.access_kind != NOT_ACCESSIBLE ||
                ei.offset != -1 || // fixme perhaps needed for temporary storage
                ei.combines_sub != false ||
                ei.outer_level != 0)
                return makeerr(err::EC_INTERPRETER_EI_INCONSISTENT,
                               nd->get_effectloc());

            // Set new expression info
            ei.eitype = res;
            // ei.offset = context_add_object(context,offset,sz,al);
            auto expsetei = nd->set_expression_info(ei);
            if (!expsetei)
                return forwarderr(expsetei);

            // Generate the code to evaluate the binary expression
            if (subei1.access_kind != LOCAL_OBJECT) // fixme
                return makealgorithmic("non-local operand");
            if (subei2.access_kind != LOCAL_OBJECT) // fixme
                return makealgorithmic("non-local operand");

            // The instruction
            opcode op = opstem_to_binary_opcode(stem,arg1,arg2);
            if (op == OP_NOOP)
                return makealgorithmic("binary opcode missing");
            auto sloc = nd->get_effectloc();

            instrnodelist opinstrlist;
            auto opinstr = instrnodelist::allocate(op);
            opinstrlist.append(opinstr);
            opinstr->instr.arg1 = subei1.offset;
            opinstr->instr.arg2 = subei2.offset;
            opinstr->instr.res = ei.offset;
            opinstr->instr.comment =
                str::from_ascii_c_str(opstem_symbol(stem)) +
                " at "_str +
                u64(sloc.line()).to_str() +
                ":"_str +
                u64(sloc.column()).to_str();

            context.instrlist.append_from_list(opinstrlist);
        }
        else
        {
            return makeerr(err::EC_NOT_YET_IMPLEMENTED);
        }

        return uwflags;
    }

    case AST_COMPOUNDSTMT:
    {
        u64 uwflags;
        i64 n = nd->get_substmt_count();
        instrnodelist destrlist; // Regular destruction at end of compound stmt

        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);
            // If this is a variable, generate unwind code
            // and set the new unwind_start
            if (sub->get_node_type() == AST_VARIABLE)
            {
                // Check the variable's offset and type
                auto exp_varoffset =
                    variable_offset_verification(context,offset,sub);
                if (!exp_varoffset)
                    return exp_varoffset;
                // This has updated offset

                instrnodelist list;

                // Create the construction code
                auto exp_varconstr =
                    variable_constructor_generator(list,unwind_start,sub);
                if (!exp_varconstr)
                    return exp_varconstr;
                uwflags |= exp_varconstr.value();
                append_code(context.instrlist,list);

                // Create the destruction code
                auto exp_vardestr =
                    variable_destructor_generator(list,unwind_start,sub);
                if (!exp_vardestr)
                    return exp_vardestr;
                uwflags |= exp_vardestr.value();
                prepend_code(destrlist,list);

                // After the variable has been created,
                // unwinding starts at the head of the destruction list
                if (!destrlist.is_empty())
                    unwind_start = destrlist.get_first();
            }
            // For all non-variables, process the statement
            else
            {
                auto exp =
                    recursive_instrnode_generator(
                        context,offset,unwind_start,sub);
                if (!exp)
                    return exp;
                uwflags |= exp.value();
            }
        }

        // Finally append the destrlist and propagate the uwflags
        return unwind_and_handle_flags(context,destrlist,uwflags,
                                       unwind_start_saved,
                                       nd->get_endloc());
    }

    case AST_IFSTMT:
    {
        u64 uwflags;
        i64 n = nd->get_subnode_count();

        instrnodelist destrlist; // Destruction at end of if stmt

        // Code is generated like an onion from outside to inside
        // Cleanup code is temporarily placed in destrlist.

        // 1. Labels at start and end of statement
        if (context.use_many_labels)
        {
            // A label at the start of the if statement
            auto stmt_label = statement_label(nd,""_str);
            context.instrlist.append(stmt_label);

            // A label at the end of the if statement
            auto end_label = statement_label(nd,"_end"_str);
            destrlist.prepend(end_label);
            // After the end label, a jump for uwflags may be added
        }

        // 2. Initialize and destroy the variables for the init statement
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);
            if (sub->get_node_type() != AST_VARIABLE)
                return makeerr(err::EC_AST_NONVARIABLE_IN_SUBNODE_LIST,
                               sub->get_effectloc());

            // Check the variable's offset and type
            auto exp_varoffset =
                variable_offset_verification(context,offset,sub);
            if (!exp_varoffset)
                return exp_varoffset;
            // This has updated offset

            instrnodelist list;

            // Create the construction code
            auto exp_varconstr =
                variable_constructor_generator(list,unwind_start,sub);
            if (!exp_varconstr)
                return exp_varconstr;
            uwflags |= exp_varconstr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            append_code(context.instrlist,list);

            // Create the destruction code
            auto exp_vardestr =
                variable_destructor_generator(list,unwind_start,sub);
            if (!exp_vardestr)
                return exp_vardestr;
            uwflags |= exp_vardestr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            prepend_code(destrlist,list);

            // After the variable has been created,
            // unwinding starts at the head of the destruction list
            if (!destrlist.is_empty())
                unwind_start = destrlist.get_first();
        }

        // 3. Generate the init statement
        auto init_stmt = nd->get_initstmt();
        if (init_stmt)
        {
            if (context.use_many_labels)
            {
                auto init_stmt_label = statement_label(nd,"_init"_str);
                context.instrlist.append(init_stmt_label);
            }
            auto exp_init =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,init_stmt);
            if (!exp_init)
                return exp_init;
            auto uwflags_init = exp_init.value();

            // Only the UW_EXCEPTION flag may be set by the init statement
            cig_assert((uwflags_init & ~U64_EXCEPTION) == 0);

            uwflags |= uwflags_init;
        }

        // 4. If jumps

        auto then_stmt = nd->get_thenstmt();
        auto else_stmt = nd->get_elsestmt(); // may be nullptr
        if (!then_stmt)
            return makeerr(err::EC_AST_IFSTMT_HAS_NO_THENSTMT,
                           nd->get_effectloc());

        // Create the jump target after the if
        instrnode *after_if = nullptr;
        if (context.use_many_labels || destrlist.is_empty())
        {
            after_if = statement_label(nd,"_after_if"_str);
            destrlist.prepend(after_if);
        }
        else
            after_if = destrlist.get_first();
        // unwind_start = after_loop;

        // Create the label between the then and the else
        instrnodelist else_start_list;
        instrnode *else_start = nullptr;
        if (else_stmt)
        {
            // Create the jump label for the else branch
            // This will be the jump-to label for skipping the then branch
            else_start = statement_label(nd,"_else"_str);
            else_start_list.append(else_start);
        }

        // The jump target after the 'then' branch
        instrnode *after_then = (else_start ? else_start : after_if);

        // 5. Evaluate the condition
        auto condexpr = nd->get_condexpr();
        if (condexpr)
        {
            cig_assert(context.expr_unwind.is_empty());

            // Build up the condition expression
            auto exp_condexpr =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,condexpr);
            if (!exp_condexpr)
                return exp_condexpr;
            auto uwflags_cond = exp_condexpr.value();
            uwflags |= uwflags_cond;

            // Only the UW_EXCEPTION flag may be set by an expression
            cig_assert((uwflags_cond & ~U64_EXCEPTION) == 0);

            // Note: If the destructor of cig_bool would reset it to false,
            //       we would have to save the condition value here
            //       into fnruntime. However, the constructor leaves the
            //       value unchanged, therefore we can rely on the value
            //       remaining in place.


            // Destruct the condition expression
            context.instrlist.append_from_list(context.expr_unwind);

            // Insert a conditional jump statement to the outer
            // unwinding code.
            if (uwflags_cond != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after condition exception"_str;
                context.instrlist.append(jump);
            }


            // Use the condition value for the then jump
            auto ei = condexpr->get_expression_info();
            if (ei.bitype != AST_BUILTIN_BOOL)
                return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                               nd->get_effectloc());
            if (ei.access_kind != LOCAL_OBJECT)
                return makeerr(err::EC_CONDITION_BOOL_IS_NOT_LOCAL,
                               nd->get_effectloc());

            auto condition_jump = instrnodelist::allocate(OP_JMPFALSE);
            condition_jump->instr.comment = "condition not true, skip then"_str;
            condition_jump->jump_target = after_then;
            condition_jump->instr.arg1 = ei.offset;
            context.instrlist.append(condition_jump);
        }
        else
        {
            // If there is no condition expression, the if condition
            // always evaluates as true, which means we need no jump here.
            auto condition_noop = instrnodelist::allocate(OP_NOOP);
            condition_noop->instr.comment = "condition always true"_str;
            context.instrlist.append(condition_noop);
        }

        // 6. Create the 'then' branch
        if (then_stmt)
        {
            // The 'then' start label
            if (context.use_many_labels)
            {
                auto then_stmt_label = statement_label(nd,"_then"_str);
                context.instrlist.append(then_stmt_label);
            }
            auto exp_then =
                recursive_instrnode_generator(context,offset,
                                              after_if,then_stmt);
            if (!exp_then)
                return exp_then;
            auto uwflags_then = exp_then.value();

            // Handle a break, return, or exception (need to leave the loop)
            if (uwflags_then != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after then"_str;
                context.instrlist.append(jump);
            }
            uwflags |= uwflags_then;
        }

        // 7. Create the 'else' branch
        if (else_stmt)
        {
            // After the 'then', jump to the end of if
            instrnode *skip_else =
                instrnodelist::allocate(OP_JMP);
            skip_else->jump_target = after_if;
            skip_else->instr.comment =
                "skip else"_str;
            context.instrlist.append(skip_else);

            // Append the jump target of if
            context.instrlist.append_from_list(else_start_list);

            // The 'else' start label has already been generated
            auto exp_else =
                recursive_instrnode_generator(context,offset,
                                              after_if,else_stmt);
            if (!exp_else)
                return exp_else;
            auto uwflags_else = exp_else.value();

            // Handle a break, return, or exception (need to leave the loop)
            if (uwflags_else != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after else"_str;
                context.instrlist.append(jump);
            }
            uwflags |= uwflags_else;
        }

        // 8. Finally append the destrlist and propagate the uwflags
        //    (This is the outermost layer but depends on the uwflags)
        //    If the uwflags are set, append jump to the saved unwind_start.
        return unwind_and_handle_flags(context,destrlist,uwflags,
                                       unwind_start_saved,
                                       nd->get_endloc());
    }

    case AST_WHILESTMT:
    {
        u64 uwflags;
        i64 n = nd->get_subnode_count();

        instrnodelist destrlist; // Destruction at end of while stmt

        // Code is generated like an onion from outside to inside
        // Cleanup code is temporarily placed in destrlist.

        // 1. Labels at start and end of statement
        if (context.use_many_labels)
        {
            // A label at the start of the while statement
            auto stmt_label = statement_label(nd,""_str);
            context.instrlist.append(stmt_label);

            // A label at the end of the while statement
            auto end_label = statement_label(nd,"_end"_str);
            destrlist.prepend(end_label);
            // After the end label, a jump for uwflags may be added
        }

        // 2. Initialize and destroy the variables for the init statement
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);
            if (sub->get_node_type() != AST_VARIABLE)
                return makeerr(err::EC_AST_NONVARIABLE_IN_SUBNODE_LIST,
                               sub->get_effectloc());

            // Check the variable's offset and type
            auto exp_varoffset =
                variable_offset_verification(context,offset,sub);
            if (!exp_varoffset)
                return exp_varoffset;
            // This has updated offset

            instrnodelist list;

            // Create the construction code
            auto exp_varconstr =
                variable_constructor_generator(list,unwind_start,sub);
            if (!exp_varconstr)
                return exp_varconstr;
            uwflags |= exp_varconstr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            append_code(context.instrlist,list);

            // Create the destruction code
            auto exp_vardestr =
                variable_destructor_generator(list,unwind_start,sub);
            if (!exp_vardestr)
                return exp_vardestr;
            uwflags |= exp_vardestr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            prepend_code(destrlist,list);

            // After the variable has been created,
            // unwinding starts at the head of the destruction list
            if (!destrlist.is_empty())
                unwind_start = destrlist.get_first();
        }

        // 3. Generate the init statement
        auto init_stmt = nd->get_initstmt();
        if (init_stmt)
        {
            if (context.use_many_labels)
            {
                auto init_stmt_label = statement_label(nd,"_init"_str);
                context.instrlist.append(init_stmt_label);
            }
            auto exp_init =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,init_stmt);
            if (!exp_init)
                return exp_init;
            auto uwflags_init = exp_init.value();

            // Only the UW_EXCEPTION flag may be set by the init statement
            cig_assert((uwflags_init & ~U64_EXCEPTION) == 0);

            uwflags |= uwflags_init;
        }

        // 4. Loop jumps

        // Create the jump target after the loop
        instrnode *after_loop = nullptr;
        if (context.use_many_labels || destrlist.is_empty())
        {
            after_loop = statement_label(nd,"_after_loop"_str);
            destrlist.prepend(after_loop);
        }
        else
            after_loop = destrlist.get_first();
        unwind_start = after_loop;

        // Create the jump label for the loop start
        // This will be the jump-to label for loop repetition
        auto loop_start = statement_label(nd,"_loop_start"_str);
        context.instrlist.append(loop_start);

        // Create the repeat jump to start of loop
        instrnode *auxrepeat = instrnodelist::allocate(OP_JMP);
        auxrepeat->jump_target = loop_start;
        destrlist.prepend(auxrepeat);

        // 5. Evaluate the condition
        auto condexpr = nd->get_condexpr();
        if (condexpr)
        {
            cig_assert(context.expr_unwind.is_empty());

            // Build up the condition expression
            auto exp_condexpr =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,condexpr);
            if (!exp_condexpr)
                return exp_condexpr;
            auto uwflags_cond = exp_condexpr.value();
            uwflags |= uwflags_cond;

            // Only the UW_EXCEPTION flag may be set by an expression
            cig_assert((uwflags_cond & ~U64_EXCEPTION) == 0);

            // Note: If the destructor of cig_bool would reset it to false,
            //       we would have to save the condition value here
            //       into fnruntime. However, the constructor leaves the
            //       value unchanged, therefore we can rely on the value
            //       remaining in place.


            // Destruct the condition expression
            context.instrlist.append_from_list(context.expr_unwind);

            // Insert a conditional jump statement to the outer
            // unwinding code.
            if (uwflags_cond != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after condition exception"_str;
                context.instrlist.append(jump);
            }


            // Use the condition value for the loop jump
            auto ei = condexpr->get_expression_info();
            if (ei.bitype != AST_BUILTIN_BOOL)
                return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                               nd->get_effectloc());
            if (ei.access_kind != LOCAL_OBJECT)
                return makeerr(err::EC_CONDITION_BOOL_IS_NOT_LOCAL,
                               nd->get_effectloc());

            auto condition_jump = instrnodelist::allocate(OP_JMPFALSE);
            condition_jump->instr.comment = "condition not true"_str;
            condition_jump->jump_target = after_loop;
            condition_jump->instr.arg1 = ei.offset;
            context.instrlist.append(condition_jump);
        }
        else
        {
            // If there is no condition expression, the while condition
            // always evaluates as true, which means we need no jump here.
            auto condition_noop = instrnodelist::allocate(OP_NOOP);
            condition_noop->instr.comment = "condition always true"_str;
            context.instrlist.append(condition_noop);
        }

        // 6. Generate the while body statement
        auto body_stmt = nd->get_bodystmt();
        if (body_stmt)
        {
            // Create the body-end label
            instrnodelist body_end;
            auto body_end_label = statement_label(nd,"_continue"_str);
            body_end.append(body_end_label);

            // Create the body
            if (context.use_many_labels)
            {
                auto body_stmt_label = statement_label(nd,"_body"_str);
                context.instrlist.append(body_stmt_label);
            }
            auto exp_body =
                recursive_instrnode_generator(context,offset,
                                              body_end_label,body_stmt);
            if (!exp_body)
                return exp_body;
            auto uwflags_body = exp_body.value();

            // Append the body-end label
            context.instrlist.append_from_list(body_end);

            // Handle the continue
            if ((uwflags_body & U64_CONTINUE) != 0)
            {
                auto reset_continue = instrnodelist::allocate(OP_CLEARCONTINUE);
                context.instrlist.append(reset_continue);
                uwflags_body &= ~U64_CONTINUE;
            }

            // Handle a break, return, or exception (need to leave the loop)
            if (uwflags_body != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after while body"_str;
                context.instrlist.append(jump);
            }
            uwflags |= uwflags_body;
        }

        // Note that the repeat jump is at the head of destrlist

        // 7. Handle the break
        if ((uwflags & U64_BREAK) != 0)
        {
            auto reset_break = instrnodelist::allocate(OP_CLEARBREAK);
            destrlist.append(reset_break);
            uwflags &= ~U64_BREAK;
        }

        // 8. Finally append the destrlist and propagate the uwflags
        //    (This is the outermost layer but depends on the uwflags)
        //    If the uwflags are set, append jump to the saved unwind_start.
        return unwind_and_handle_flags(context,destrlist,uwflags,
                                       unwind_start_saved,
                                       nd->get_endloc());
    }

    case AST_DOWHILESTMT:
    {
        u64 uwflags;

        instrnodelist destrlist; // Destruction at end of while stmt

        // Code is generated like an onion from outside to inside
        // Cleanup code is temporarily placed in destrlist.

        // 1. Labels at start and end of statement
        if (context.use_many_labels)
        {
            // A label at the start of the while statement
            auto stmt_label = statement_label(nd,""_str);
            context.instrlist.append(stmt_label);

            // A label at the end of the while statement
            auto end_label = statement_label(nd,"_end"_str);
            destrlist.prepend(end_label);
            // After the end label, a jump for uwflags may be added
        }

        // No init statement in do..while statement

        // 2. Loop jumps

        // Create the jump target after the loop
        instrnode *after_loop = nullptr;
        if (context.use_many_labels || destrlist.is_empty())
        {
            after_loop = statement_label(nd,"_after_loop"_str);
            destrlist.prepend(after_loop);
        }
        else
            after_loop = destrlist.get_first();
        unwind_start = after_loop;

        // Create the jump label for the loop start
        // This will be the jump-to label for loop repetition
        auto loop_start = statement_label(nd,"_loop_start"_str);
        context.instrlist.append(loop_start);

        // // Create the repeat jump to start of loop
        // instrnode *auxrepeat = instrnodelist::allocate(OP_JMP);
        // auxrepeat->jump_target = loop_start;
        // destrlist.prepend(auxrepeat);

        // 3. Generate the while body statement
        auto body_stmt = nd->get_bodystmt();
        if (body_stmt)
        {
            // Create the body-end label
            instrnodelist body_end;
            auto body_end_label = statement_label(nd,"_continue"_str);
            body_end.append(body_end_label);

            // Create the body
            if (context.use_many_labels)
            {
                auto body_stmt_label = statement_label(nd,"_body"_str);
                context.instrlist.append(body_stmt_label);
            }
            auto exp_body =
                recursive_instrnode_generator(context,offset,
                                              body_end_label,body_stmt);
            if (!exp_body)
                return exp_body;
            auto uwflags_body = exp_body.value();

            // Append the body-end label
            context.instrlist.append_from_list(body_end);

            // Handle the continue
            if ((uwflags_body & U64_CONTINUE) != 0)
            {
                auto reset_continue = instrnodelist::allocate(OP_CLEARCONTINUE);
                context.instrlist.append(reset_continue);
                uwflags_body &= ~U64_CONTINUE;
            }

            // Handle a break, return, or exception (need to leave the loop)
            if (uwflags_body != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after while body"_str;
                context.instrlist.append(jump);
            }
            uwflags |= uwflags_body;
        }

        // 4. Evaluate the condition
        auto condexpr = nd->get_condexpr();
        if (condexpr)
        {
            cig_assert(context.expr_unwind.is_empty());

            // Build up the condition expression
            auto exp_condexpr =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,condexpr);
            if (!exp_condexpr)
                return exp_condexpr;
            auto uwflags_cond = exp_condexpr.value();
            uwflags |= uwflags_cond;

            // Only the UW_EXCEPTION flag may be set by an expression
            cig_assert((uwflags_cond & ~U64_EXCEPTION) == 0);

            // Note: If the destructor of cig_bool would reset it to false,
            //       we would have to save the condition value here
            //       into fnruntime. However, the constructor leaves the
            //       value unchanged, therefore we can rely on the value
            //       remaining in place.


            // Destruct the condition expression
            context.instrlist.append_from_list(context.expr_unwind);

            // Insert a conditional jump statement to the outer
            // unwinding code.
            if (uwflags_cond != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after condition exception"_str;
                context.instrlist.append(jump);
            }


            // Use the condition value for the loop jump
            auto ei = condexpr->get_expression_info();
            if (ei.bitype != AST_BUILTIN_BOOL)
                return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                               nd->get_effectloc());
            if (ei.access_kind != LOCAL_OBJECT)
                return makeerr(err::EC_CONDITION_BOOL_IS_NOT_LOCAL,
                               nd->get_effectloc());

            auto condition_jump = instrnodelist::allocate(OP_JMPTRUE);
            condition_jump->instr.comment = "condition true"_str;
            condition_jump->jump_target = loop_start;
            condition_jump->instr.arg1 = ei.offset;
            context.instrlist.append(condition_jump);
        }
        else
        {
            // If there is no condition expression, the while condition
            // always evaluates as true, which means we must jump
            auto condition_jump = instrnodelist::allocate(OP_JMP);
            condition_jump->instr.comment = "condition always true"_str;
            condition_jump->jump_target = loop_start;
            context.instrlist.append(condition_jump);
        }

        // 5. Handle the break
        if ((uwflags & U64_BREAK) != 0)
        {
            auto reset_break = instrnodelist::allocate(OP_CLEARBREAK);
            destrlist.append(reset_break);
            uwflags &= ~U64_BREAK;
        }

        // 6. Finally append the destrlist and propagate the uwflags
        //    (This is the outermost layer but depends on the uwflags)
        //    If the uwflags are set, append jump to the saved unwind_start.
        return unwind_and_handle_flags(context,destrlist,uwflags,
                                       unwind_start_saved,
                                       nd->get_endloc());
    }

    case AST_FORSTMT:
    {
        u64 uwflags;
        i64 n = nd->get_subnode_count();

        instrnodelist destrlist; // Destruction at end of for stmt

        // Code is generated like an onion from outside to inside
        // Cleanup code is temporarily placed in destrlist.

        // 1. Labels at start and end of statement
        if (context.use_many_labels)
        {
            // A label at the start of the for statement
            auto stmt_label = statement_label(nd,""_str);
            context.instrlist.append(stmt_label);

            // A label at the end of the for statement
            auto end_label = statement_label(nd,"_end"_str);
            destrlist.prepend(end_label);
            // After the end label, a jump for uwflags may be added
        }

        // 2. Initialize and destroy the variables for the init statement
        for (i64 i = 0; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);
            if (sub->get_node_type() != AST_VARIABLE)
                return makeerr(err::EC_AST_NONVARIABLE_IN_SUBNODE_LIST,
                               sub->get_effectloc());

            // Check the variable's offset and type
            auto exp_varoffset =
                variable_offset_verification(context,offset,sub);
            if (!exp_varoffset)
                return exp_varoffset;
            // This has updated offset

            instrnodelist list;

            // Create the construction code
            auto exp_varconstr =
                variable_constructor_generator(list,unwind_start,sub);
            if (!exp_varconstr)
                return exp_varconstr;
            uwflags |= exp_varconstr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            append_code(context.instrlist,list);

            // Create the destruction code
            auto exp_vardestr =
                variable_destructor_generator(list,unwind_start,sub);
            if (!exp_vardestr)
                return exp_vardestr;
            uwflags |= exp_vardestr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            prepend_code(destrlist,list);

            // After the variable has been created,
            // unwinding starts at the head of the destruction list
            if (!destrlist.is_empty())
                unwind_start = destrlist.get_first();
        }

        // 3. Generate the init statement
        auto init_stmt = nd->get_initstmt();
        if (init_stmt)
        {
            if (context.use_many_labels)
            {
                auto init_stmt_label = statement_label(nd,"_init"_str);
                context.instrlist.append(init_stmt_label);
            }
            auto exp_init =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,init_stmt);
            if (!exp_init)
                return exp_init;
            auto uwflags_init = exp_init.value();

            // Only the UW_EXCEPTION flag may be set by the init statement
            cig_assert((uwflags_init & ~U64_EXCEPTION) == 0);

            uwflags |= uwflags_init;
        }

        // 4. Loop jumps

        // Create the jump target after the loop
        instrnode *after_loop = nullptr;
        if (context.use_many_labels || destrlist.is_empty())
        {
            after_loop = statement_label(nd,"_after_loop"_str);
            destrlist.prepend(after_loop);
        }
        else
            after_loop = destrlist.get_first();
        unwind_start = after_loop;

        // Create the jump label for the loop start
        // This will be the jump-to label for loop repetition
        auto loop_start = statement_label(nd,"_loop_start"_str);
        context.instrlist.append(loop_start);

        // Create the repeat jump to start of loop
        instrnode *auxrepeat = instrnodelist::allocate(OP_JMP);
        auxrepeat->jump_target = loop_start;
        destrlist.prepend(auxrepeat);

        // 5. Evaluate the condition
        auto condexpr = nd->get_condexpr();
        if (condexpr)
        {
            cig_assert(context.expr_unwind.is_empty());

            // Build up the condition expression
            auto exp_condexpr =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,condexpr);
            if (!exp_condexpr)
                return exp_condexpr;
            auto uwflags_cond = exp_condexpr.value();
            uwflags |= uwflags_cond;

            // Only the UW_EXCEPTION flag may be set by an expression
            cig_assert((uwflags_cond & ~U64_EXCEPTION) == 0);

            // Note: If the destructor of cig_bool would reset it to false,
            //       we would have to save the condition value here
            //       into fnruntime. However, the constructor leaves the
            //       value unchanged, therefore we can rely on the value
            //       remaining in place.


            // Destruct the condition expression
            context.instrlist.append_from_list(context.expr_unwind);

            // Insert a conditional jump statement to the outer
            // unwinding code.
            if (uwflags_cond != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after condition exception"_str;
                context.instrlist.append(jump);
            }


            // Use the condition value for the loop jump
            auto ei = condexpr->get_expression_info();
            if (ei.bitype != AST_BUILTIN_BOOL)
                return makeerr(err::EC_CONDITION_IS_NOT_A_BOOL,
                               nd->get_effectloc());
            if (ei.access_kind != LOCAL_OBJECT)
                return makeerr(err::EC_CONDITION_BOOL_IS_NOT_LOCAL,
                               nd->get_effectloc());

            auto condition_jump = instrnodelist::allocate(OP_JMPFALSE);
            condition_jump->instr.comment = "condition not true"_str;
            condition_jump->jump_target = after_loop;
            condition_jump->instr.arg1 = ei.offset;
            context.instrlist.append(condition_jump);
        }
        else
        {
            // If there is no condition expression, the for condition
            // always evaluates as true, which means we need no jump here.
            auto condition_noop = instrnodelist::allocate(OP_NOOP);
            condition_noop->instr.comment = "condition always true"_str;
            context.instrlist.append(condition_noop);
        }

        // 6. Generate the for body statement
        auto body_stmt = nd->get_bodystmt();
        if (body_stmt)
        {
            // Create the body-end label
            instrnodelist body_end;
            auto body_end_label = statement_label(nd,"_continue"_str);
            body_end.append(body_end_label);

            // Create the body
            if (context.use_many_labels)
            {
                auto body_stmt_label = statement_label(nd,"_body"_str);
                context.instrlist.append(body_stmt_label);
            }
            auto exp_body =
                recursive_instrnode_generator(context,offset,
                                              body_end_label,body_stmt);
            if (!exp_body)
                return exp_body;
            auto uwflags_body = exp_body.value();

            // Append the body-end label
            context.instrlist.append_from_list(body_end);

            // Handle the continue
            if ((uwflags_body & U64_CONTINUE) != 0)
            {
                auto reset_continue = instrnodelist::allocate(OP_CLEARCONTINUE);
                context.instrlist.append(reset_continue);
                uwflags_body &= ~U64_CONTINUE;
            }

            // Handle a break, return, or exception (need to leave the loop)
            if (uwflags_body != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after for body"_str;
                context.instrlist.append(jump);
            }
            uwflags |= uwflags_body;
        }

        // 7. Evaluate the iterate expression
        auto iterateexpr = nd->get_iterateexpr();
        if (iterateexpr)
        {
            // Build up the iterate expression
            auto exp_iterateexpr =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,iterateexpr);
            if (!exp_iterateexpr)
                return exp_iterateexpr;
            auto uwflags_it = exp_iterateexpr.value();

            // Only the UW_EXCEPTION flag may be set by an expression
            cig_assert((uwflags_it & ~U64_EXCEPTION) == 0);

            // Destruct the iterate expression
            context.instrlist.append_from_list(context.expr_unwind);

            // Insert a conditional jump statement to the outer
            // unwinding code.
            if (uwflags_it != 0)
            {
                instrnode *jump =
                    instrnodelist::allocate(OP_JMPANYUWFLAG);
                jump->jump_target = unwind_start;
                jump->instr.comment =
                    "more unwinding after for iterate exception"_str;
                context.instrlist.append(jump);
            }
            uwflags |= uwflags_it;
        }
        else
        {
            // If there is no iterate expression, there is nothing to do here.
        }


        // Note that the repeat jump is at the head of destrlist

        // 8. Handle the break
        if ((uwflags & U64_BREAK) != 0)
        {
            auto reset_break = instrnodelist::allocate(OP_CLEARBREAK);
            destrlist.append(reset_break);
            uwflags &= ~U64_BREAK;
        }

        // 9. Finally append the destrlist and propagate the uwflags
        //    (This is the outermost layer but depends on the uwflags)
        //    If the uwflags are set, append jump to the saved unwind_start.
        return unwind_and_handle_flags(context,destrlist,uwflags,
                                       unwind_start_saved,
                                       nd->get_endloc());
    }

    case AST_FOROFSTMT:
    {
        u64 uwflags;
        i64 n = nd->get_subnode_count();
        i64 ninit = nd->get_initstmt_variables();

        instrnodelist outerdestrlist; // Destruction at end of for..of stmt
        instrnodelist innerdestrlist; // Destruction at end of loop

        // Code is generated like an onion from outside to inside
        // Cleanup code is temporarily placed in innerdestrlist/outerdestrlist.

        // 1. Labels at start and end of statement
        if (context.use_many_labels)
        {
            // A label at the start of the for..of statement
            auto stmt_label = statement_label(nd,""_str);
            context.instrlist.append(stmt_label);

            // A label at the end of the for..of statement
            auto end_label = statement_label(nd,"_end"_str);
            outerdestrlist.prepend(end_label);
            // After the end label, a jump for uwflags may be added
        }

        // 2. Initialize and destroy the variables for the init statement
        for (i64 i = 0; i < ninit; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);
            if (sub->get_node_type() != AST_VARIABLE)
                return makeerr(err::EC_AST_NONVARIABLE_IN_SUBNODE_LIST,
                               sub->get_effectloc());

            // Check the variable's offset and type
            auto exp_varoffset =
                variable_offset_verification(context,offset,sub);
            if (!exp_varoffset)
                return exp_varoffset;
            // This has updated offset

            instrnodelist list;

            // Create the construction code
            auto exp_varconstr =
                variable_constructor_generator(list,unwind_start,sub);
            if (!exp_varconstr)
                return exp_varconstr;
            uwflags |= exp_varconstr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            append_code(context.instrlist,list);

            // Create the destruction code
            auto exp_vardestr =
                variable_destructor_generator(list,unwind_start,sub);
            if (!exp_vardestr)
                return exp_vardestr;
            uwflags |= exp_vardestr.value();
            cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            prepend_code(outerdestrlist,list);

            // After the variable has been created,
            // unwinding starts at the head of the destruction list
            if (!outerdestrlist.is_empty())
                unwind_start = outerdestrlist.get_first();
        }

        // 3. Generate the init statement
        auto init_stmt = nd->get_initstmt();
        if (init_stmt)
        {
            if (context.use_many_labels)
            {
                auto init_stmt_label = statement_label(nd,"_init"_str);
                context.instrlist.append(init_stmt_label);
            }
            auto exp_init =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,init_stmt);
            if (!exp_init)
                return exp_init;
            auto uwflags_init = exp_init.value();

            // Only the UW_EXCEPTION flag may be set by the init statement
            cig_assert((uwflags_init & ~U64_EXCEPTION) == 0);

            uwflags |= uwflags_init;
        }

        // 4. Create and destroy the generator.
        if (context.use_many_labels)
        {
            auto gen_start = statement_label(nd,"_create_generator"_str);
            context.instrlist.append(gen_start);
        }
        u64 uwflags_creategen;

        // Turn off offset validation since we need a temporary
        // object for the generator.
        auto validate_resolver_offsets_save =
            context.validate_resolver_offsets;
        context.validate_resolver_offsets = false;

        // fixme we need first to create an iterator, which holds the
        // state of the iteration and a boolean variable (there's more).
        // There needs to be a destructor generated for the iterator,
        // and it needs to be prepended to the destrlist.
        // It needs to run after the loop_end label.
        auto noop_gen = instrnodelist::allocate(OP_NOOP);
        context.instrlist.append(noop_gen);

        // Destroy the generator
        auto noop_gend = instrnodelist::allocate(OP_NOOP);
        outerdestrlist.prepend(noop_gend);
        if (context.use_many_labels)
        {
            auto gen_destroy = statement_label(nd,"_destroy_generator"_str);
            outerdestrlist.prepend(gen_destroy);
        }

        // Only the UW_EXCEPTION flag may be set by the generator
        cig_assert((uwflags_creategen & ~U64_EXCEPTION) == 0);
        uwflags |= uwflags_creategen;

        // 5. Loop jumps

        // Create the jump target after the loop
        instrnode *after_loop = nullptr;
        if (context.use_many_labels || outerdestrlist.is_empty())
        {
            after_loop = statement_label(nd,"_after_loop"_str);
            outerdestrlist.prepend(after_loop);
        }
        else
            after_loop = outerdestrlist.get_first();
        unwind_start = after_loop;

        // Create the jump label for the loop start
        // This will be the jump-to label for loop repetition
        auto loop_start = statement_label(nd,"_loop_start"_str);
        context.instrlist.append(loop_start);

        // Create the repeat jump to start of loop
        instrnode *auxrepeat = instrnodelist::allocate(OP_JMP);
        auxrepeat->jump_target = loop_start;
        outerdestrlist.prepend(auxrepeat);

        // From here on, we are in inner code;
        // unwind the inner code first

        // Note that if the inner code of the loop generates
        // an exceptional situation, the iterator variables need
        // to be destroyed first; only then can the execution
        // jump around the loop-repeat jump.
        // Therefore we make a distinction here between
        // inner cleanup and outer cleanup, and we insert
        // the handling of break, return, or exception between
        // those two cleanups (see no. 10).

        // 6. Run generator

        // Run the generator once and get the result.
        if (context.use_many_labels)
        {
            auto label = statement_label(nd,"_run_generator"_str);
            context.instrlist.append(label);
        }
        u64 uwflags_rungen;

        // fixme in each iteration we need to run the generator,
        // check the bool and if the end is reached jump to end_loop.
        // If the iterator was successful, we have atomically initialized
        // the iterator variable(s).
        // fixme set the flags in uwflags_rungen
        auto skip_loop = instrnodelist::allocate(OP_JMP);
        skip_loop->instr.comment = "fixme skip loop"_str;
        skip_loop->jump_target = after_loop;
        context.instrlist.append(skip_loop);

        // Only the UW_EXCEPTION flag may be set by the generator
        cig_assert((uwflags_rungen & ~U64_EXCEPTION) == 0);
        uwflags |= uwflags_rungen;

        // De-Initialize the variables for the generator expression
        u64 uwflags_destr_rungen;
        for (i64 i = ninit; i < n; i += 1)
        {
            ast_node_ptr sub = nd->get_subnode(i);
            if (!sub)
                return makeerr(err::EC_AST_NULL_IN_SUBNODE_LIST);
            if (sub->get_node_type() != AST_VARIABLE)
                return makeerr(err::EC_AST_NONVARIABLE_IN_SUBNODE_LIST,
                               sub->get_effectloc());

            // Check the variable's offset and type
            auto exp_varoffset =
                variable_offset_verification(context,offset,sub);
            if (!exp_varoffset)
                return exp_varoffset;
            // This has updated offset

            instrnodelist list;

            // Constructor not required; that is done by run-generator.
            //
            // // Create the construction code
            // auto exp_varconstr =
            //     variable_constructor_generator(list,unwind_start,sub);
            // if (!exp_varconstr)
            //     return exp_varconstr;
            // uwflags |= exp_varconstr.value();
            // cig_assert((uwflags & ~U64_EXCEPTION) == 0);
            // append_code(context.instrlist,list);

            // Create the destruction code
            auto exp_vardestr =
                variable_destructor_generator(list,unwind_start,sub);
            if (!exp_vardestr)
                return exp_vardestr;
            uwflags_destr_rungen |= exp_vardestr.value();
            cig_assert((uwflags_destr_rungen & ~U64_EXCEPTION) == 0);
            prepend_code(innerdestrlist,list);

            // After the variable has been created,
            // unwinding starts at the head of the destruction list
            if (!innerdestrlist.is_empty())
                unwind_start = innerdestrlist.get_first();

            uwflags |= uwflags_destr_rungen;
        }

        // 7. Generate the body statement
        u64 uwflags_body;
        auto body_stmt = nd->get_bodystmt();
        if (body_stmt)
        {
            if (context.use_many_labels)
            {
                auto body_stmt_label = statement_label(nd,"_body"_str);
                context.instrlist.append(body_stmt_label);
            }
            // An unwind start is required for possible continue statements
            if (context.use_many_labels || innerdestrlist.is_empty())
            {
                auto body_end_label = statement_label(nd,"_continue"_str);
                innerdestrlist.prepend(body_end_label);
                unwind_start = body_end_label;
            }
            else
            {
                cig_assert(unwind_start == innerdestrlist.get_first());
            }

            auto exp_body =
                recursive_instrnode_generator(context,offset,
                                              unwind_start,body_stmt);
            if (!exp_body)
                return exp_body;
            uwflags_body |= exp_body.value();
            // The body may contain exceptions, return, break, continue

            // fixme remove
            // Let's assume the body may have a return,
            // break, and continue statement
            uwflags |= U64_RETURN;
            uwflags |= U64_BREAK;
            uwflags |= U64_CONTINUE;

            // Note: This is different from simple for loop.
            // Here, in the case of exceptional situations,
            // we still need to unwind the stack inside the loop.
            // Therefore no jump here to avoid the repeat jump.
            // That jump is added later.
            uwflags |= uwflags_body;
        }

        // 8. Handle the continue
        if ((uwflags & U64_CONTINUE) != 0)
        {
            auto reset_continue = instrnodelist::allocate(OP_CLEARCONTINUE);
            innerdestrlist.append(reset_continue);
            uwflags_body &= ~U64_CONTINUE;
            uwflags &= ~U64_CONTINUE;
        }

        // 9. Append the inner destructor list
        context.instrlist.append_from_list(innerdestrlist);

        // 10. Handle a break, return, or exception (need to leave the loop)
        if (uwflags_rungen != 0 ||
            uwflags_destr_rungen != 0 ||
            uwflags_body != 0)
        {
            instrnode *jump =
                instrnodelist::allocate(OP_JMPANYUWFLAG);
            jump->jump_target = after_loop;
            jump->instr.comment =
                "more unwinding after for body"_str;
            context.instrlist.append(jump);
        }

        // 11. Handle the break
        if ((uwflags & U64_BREAK) != 0)
        {
            auto reset_break = instrnodelist::allocate(OP_CLEARBREAK);
            outerdestrlist.append(reset_break);
            uwflags &= ~U64_BREAK;
        }

        // Restore the previous value of validate_resolver_offsets
        context.validate_resolver_offsets =
            validate_resolver_offsets_save;

        // 12. Finally append the destrlist and propagate the uwflags
        //     (This is the outermost layer but depends on the uwflags)
        //     If the uwflags are set, append jump to the saved unwind_start.
        return unwind_and_handle_flags(context,outerdestrlist,uwflags,
                                       unwind_start_saved,
                                       nd->get_endloc());
    }

    case AST_LAMBDA:
    case AST_TYPEREF:
    case AST_FUNPAR:
    case AST_FUNPARLIST:
    case AST_FUNRET:
    case AST_FUNRETLIST:


    case AST_RETURNSTMT:
    case AST_EXPRSTMT:
    case AST_BREAKSTMT:
    case AST_CONTINUESTMT:
        return exp_u64();       // fixme missing code
    }

    return makeerr(err::EC_INTERPRETER_BAD_AST_TOPNODE,
                   nd->get_effectloc());
}



static
exp_none
function_to_fncode(
    ast::ast_node_ptr fundef,
    ipcode &ip,
    fncode &fnc,
    bool use_many_labels,
    bool compact
    )
{
    // Set function's qualified name, fnidx (index in interpreter)
    // and dump_index (index from ast_dump)
    fnc.fnidx = ip.fn.i64_size();
    fnc.fqname = fundef->create_fqname();
    fnc.dump_index = fundef->dump_index;
    i64 oldfnidx = fundef->get_interpreter_function_index();
    if (oldfnidx != -1)
        return makeerr(err::EC_INTERPRETER_FNCODE_ALREADY_GENERATED,
                       fundef->get_effectloc());
    auto expsetfnidx = fundef->set_interpreter_function_index(fnc.fnidx);
    if (!expsetfnidx)
        return expsetfnidx;

    // Get function body
    ast::ast_node_ptr body = fundef->get_funbody();
    if (!body)
        return makeerr(err::EC_INTERPRETER_NO_FUNBODY,
                       fundef->get_effectloc());

    // Create instruction context
    instrcontext context;
    context.use_many_labels = use_many_labels;

    // Check offsets of return type list
    i64 offset = 0;
    auto funretlist = fundef->get_funretlist();
    if (!funretlist)
        return makeerr(err::EC_INTERPRETER_NO_FUNRETLIST,
                       fundef->get_effectloc());
    i64 nret = funretlist->get_subnode_count();
    for (i64 iret = 0; iret < nret; iret += 1)
    {
        auto funret = funretlist->get_subnode(iret);
        if (!funret)
            return makeerr(err::EC_INTERPRETER_NO_FUNRETLIST,
                           funretlist->get_effectloc());

        ast::ast_variable_info vi = funret->get_variable_info();
        if (vi.ti.type_size < 0 ||
            vi.ti.type_alignment < 0)
            return makeerr(err::EC_INTERPRETER_AST_NODE_NOT_RESOLVED,
                           funret->get_effectloc());

        i64 offset_vi = context_add_object(context,offset,
                                           vi.ti.type_size,
                                           vi.ti.type_alignment);
        if (context.validate_resolver_offsets &&
            offset_vi != vi.offset)
            return makeerr(err::EC_INTERPRETER_AST_NODE_INCONSISTENT_OFFSET,
                           funret->get_effectloc());
    }

    // Adjust full length of return type list to multiple of alignment
    max_align_offset(context);
    fnc.return_list_size = context.offset_max;
    fnc.return_list_alignment = context.alignment;

    // Check offsets of parameter list
    auto funparlist = fundef->get_funparlist();
    if (!funparlist)
        return makeerr(err::EC_INTERPRETER_NO_FUNPARLIST,
                       fundef->get_effectloc());
    i64 npar = funparlist->get_subnode_count();
    for (i64 iret = 0; iret < npar; iret += 1)
    {
        auto funpar = funparlist->get_subnode(iret);
        if (!funpar)
            return makeerr(err::EC_INTERPRETER_NO_FUNPARLIST,
                           funparlist->get_effectloc());

        ast::ast_variable_info vi = funpar->get_variable_info();
        if (vi.ti.type_size < 0 ||
            vi.ti.type_alignment < 0)
            return makeerr(err::EC_INTERPRETER_AST_NODE_NOT_RESOLVED,
                           funpar->get_effectloc());

        i64 offset_vi = context_add_object(context,offset,
                                           vi.ti.type_size,
                                           vi.ti.type_alignment);
        if (context.validate_resolver_offsets &&
            offset_vi != vi.offset)
            return makeerr(err::EC_INTERPRETER_AST_NODE_INCONSISTENT_OFFSET,
                           funpar->get_effectloc());
    }

    // Validate offsets from resolver
    // This is only possible for variables, not for temporaries;
    // and it is not possible where the code generator needs to
    // create temporaries inbetween the variables, e.g. for the
    // generator in a for..of loop.
    // The code generator will turn validation off in those cases.
    // However, we start with validation on.
    context.validate_resolver_offsets = true;

    // Adjust full length of parameter list to multiple of alignment
    max_align_offset(context);
    fnc.retandpar_list_size = context.offset_max;
    fnc.retandpar_list_alignment = context.alignment;

    // Create the code end statement (to pass as unwind_start)
    instrnodelist code_end;
    instrnode *final_end = instrnodelist::allocate(OP_END);
    code_end.append(final_end);

    // Generate the code for the function body
    auto exp = recursive_instrnode_generator(context,offset,
                                             final_end,body);
    if (!exp)
        return forwarderr(exp);

    // Append the end statement
    context.instrlist.append_from_list(code_end);

    // Adjust full length of the variable space (stack space) for the function
    max_align_offset(context);
    fnc.function_size = context.offset_max;
    fnc.function_alignment = context.alignment;

    // Count and number the instructions
    i64 n_instr;
    for (instrnode *nd = context.instrlist.head.dll_next;
         nd != &context.instrlist.head;
         nd = nd->dll_next)
    {
        // Mark labels and noops as discardable
        if ((nd->instr.op == OP_LABEL ||
             nd->instr.op == OP_NOOP) &&
            compact)
            nd->discard = true;
    }
    for (;;)
    {
        // Number all instructions
        n_instr = 0;
        for (instrnode *nd = context.instrlist.head.dll_next;
             nd != &context.instrlist.head;
             nd = nd->dll_next)
        {
            nd->instr_index = n_instr;
            if (!nd->discard)
                n_instr += 1;
        }

        // Should we discard?
        if (!compact)
            break;

        // Discard more instructions
        bool have_discarded = false;
        for (instrnode *nd = context.instrlist.head.dll_next;
             nd != &context.instrlist.head;
             nd = nd->dll_next)
        {
            opstem stem = opcode_to_opstem(nd->instr.op);

            // Already discarded?
            if (nd->discard)
                continue;

            if (stem == OPSTEM_JMP ||
                stem == OPSTEM_JMPBREAK ||
                stem == OPSTEM_JMPCONTINUE ||
                stem == OPSTEM_JMPRETURN ||
                stem == OPSTEM_JMPEXCEPT ||
                stem == OPSTEM_JMPANYUWFLAG ||
                stem == OPSTEM_JMPNOUWFLAG ||
                stem == OPSTEM_JMPTRUE ||
                stem == OPSTEM_JMPFALSE ||
                stem == OPSTEM_JMPZERO ||
                stem == OPSTEM_JMPNONZERO)
            {
                cig_assert(nd->jump_target);

                // Discard a jump if its target is the next instruction
                if (nd->jump_target->instr_index == nd->instr_index ||
                    nd->jump_target->instr_index == nd->instr_index + 1)
                {
                    nd->discard = true;
                    have_discarded = true;
                }
                else
                {

                    // Discard a jump if the next instruction
                    // is an absolute jump and has the same target
                    for (instrnode *nd2 = nd->dll_next;
                         nd2 != &context.instrlist.head;
                         nd2 = nd2->dll_next)
                    {
                        if (nd2->instr_index == nd->instr_index)
                            continue;
                        if (nd2->instr_index > nd->instr_index + 1)
                            break;
                        cig_assert(nd2->instr_index == nd->instr_index + 1);
                        if (nd2->instr.op == OP_JMP &&
                            nd2->jump_target == nd->jump_target)
                        {
                            nd->discard = true;
                            have_discarded = true;
                        }
                    }
                }
            }
        }

        // If we have not discarded any more instructions, we are done
        if (!have_discarded)
            break;
    }

    // Set the jump target index
    for (instrnode *nd = context.instrlist.head.dll_next;
         nd != &context.instrlist.head;
         nd = nd->dll_next)
    {
        opstem stem = opcode_to_opstem(nd->instr.op);

        if (stem == OPSTEM_JMP ||
            stem == OPSTEM_JMPBREAK ||
            stem == OPSTEM_JMPCONTINUE ||
            stem == OPSTEM_JMPRETURN ||
            stem == OPSTEM_JMPEXCEPT ||
            stem == OPSTEM_JMPANYUWFLAG ||
            stem == OPSTEM_JMPNOUWFLAG)
        {
            cig_assert(nd->jump_target != nullptr);
            nd->instr.arg1 = nd->jump_target->instr_index;
        }
        if (stem == OPSTEM_JMPTRUE ||
            stem == OPSTEM_JMPFALSE ||
            stem == OPSTEM_JMPZERO ||
            stem == OPSTEM_JMPNONZERO)
        {
            cig_assert(nd->jump_target != nullptr);
            nd->instr.arg2 = nd->jump_target->instr_index;
        }
    }

    // Convert to instruction list
    if (n_instr > 0)
    {
        instruction *a =
            allocator<instruction,"fncode.instr">::allocate_and_construct(
                static_cast<std::size_t>(n_instr._v));
        fnc.instr = a;
        fnc.n_instr = n_instr;

        for (instrnode *nd = context.instrlist.head.dll_next;
             nd != &context.instrlist.head;
             nd = nd->dll_next)
        {
            i64 i = nd->instr_index;
            // Keep the first label but the last instruction with same index
            if (!a[i._v].label.is_empty())
                nd->instr.label = a[i._v].label;
            a[i._v] = nd->instr;
        }
    }


    return exp_none();
}


static
exp_none
functions_to_fncode(
    ipcode &ip,
    cig::vector<std::unique_ptr<fncode> > &fn,
    ast::ast_node_ptr ast_tree,
    bool use_many_labels,
    bool compact)
{
    using namespace ast;
    using enum ast::ast_node_type;

    if (ast_tree == nullptr)
        return makeerr(err::EC_INTERPRETER_AST_NODE_NULL);

    // Create the mapfunc
    std::function<exp_none(ast_node_ptr nd)> mapfunc =
        [&] (ast_node_ptr nd) -> exp_none {
            if (nd->get_node_type() == AST_FUNDEF)
            {
                std::unique_ptr<fncode> fnc(new fncode);

                auto expfn = function_to_fncode(nd,ip,*fnc,
                                                use_many_labels,
                                                compact);
                if (!expfn)
                    return expfn;

                fn.push_back(std::move(fnc));
            }
            return exp_none();
        };

    auto exp = ast::detail::ast_node_iterate_once(ast_tree,mapfunc);
    if (!exp)
        return exp;
    return exp_none();
}


static
exp_none
datatype_to_dtcode(
    ast::ast_node_ptr datatype, // classdef or enumdef
    ipcode &ip,
    dtcode &dtc)
{
    // Set data type's qualified name, typeidx (index in interpreter)
    // and dump_index (index from ast_dump)
    dtc.typeidx = ip.dt.i64_size();
    dtc.fqname = datatype->create_fqname();
    dtc.dump_index = datatype->dump_index;
    i64 oldtypeidx = datatype->get_interpreter_datatype_index();
    if (oldtypeidx != -1)
        return makeerr(err::EC_INTERPRETER_DTCODE_ALREADY_GENERATED,
                       datatype->get_effectloc());
    auto expsettypeidx = datatype->set_interpreter_datatype_index(dtc.typeidx);
    if (!expsettypeidx)
        return expsettypeidx;

    // Set size and alignment
    ast::ast_type_info ti = datatype->get_type_info();
    dtc.type_size = ti.type_size;
    dtc.type_alignment = ti.type_alignment;

    return exp_none();
}


static
exp_none
datatypes_to_dtcode(
    ipcode &ip,
    cig::vector<dtcode> &dt,
    ast::ast_node_ptr ast_tree)
{
    using namespace ast;
    using enum ast::ast_node_type;

    if (ast_tree == nullptr)
        return makeerr(err::EC_INTERPRETER_AST_NODE_NULL);

    // Create the mapfunc
    std::function<exp_none(ast_node_ptr nd)> mapfunc =
        [&] (ast_node_ptr nd) -> exp_none {
            if (nd->get_node_type() == AST_CLASSDEF ||
                nd->get_node_type() == AST_ENUMDEF)
            {
                dtcode dtc;
                auto expdt = datatype_to_dtcode(nd,ip,dtc);
                if (!expdt)
                    return expdt;

                dt.push_back(std::move(dtc));
            }
            return exp_none();
        };

    auto exp = ast::detail::ast_node_iterate_once(ast_tree,mapfunc);
    if (!exp)
        return exp;
    return exp_none();
}


exp_ipcode_ptr ast_tree_to_ipcode(
    ast::ast_node_ptr ast_tree,
    bool use_many_labels,
    bool compact)
{
    using enum ast::ast_node_type;

    // Check the tree
    if (!ast_tree)
        return makeerr(err::EC_INTERPRETER_AST_ROOT_IS_NULL);
    if (ast_tree->get_node_type() != AST_GLOBAL)
        return makeerr(err::EC_INTERPRETER_AST_ROOT_NOT_GLOBAL,
                       ast_tree->get_effectloc());

    // Has it been resolved?
    ast::ast_global_info gi = ast_tree->get_global_info();
    if (gi.globalconst_size < 0 ||
        gi.globalconst_alignment < 0 ||
        gi.globalvar_size < 0 ||
        gi.globalvar_alignment < 0)
        return makeerr(err::EC_INTERPRETER_AST_TREE_NOT_RESOLVED);

    // Allocate the ipcode
    ipcode_ptr ipptr = std::make_shared<ipcode>();
    cig_assert(ipptr);
    ipcode &ip = *ipptr;

    // Allocate the globalconst and globalvarinit
    std::size_t len_globalconst =
        static_cast<std::size_t>(gi.globalconst_size > 0 ?
                                 gi.globalconst_size._v : 1);
    std::size_t len_globalvar =
        static_cast<std::size_t>(gi.globalvar_size > 0 ?
                                 gi.globalvar_size._v : 1);
    ip.globalconst.reserve(len_globalconst);
    ip.globalconst.resize(len_globalconst,0);
    ip.globalvarinit.reserve(len_globalvar);
    ip.globalvarinit.resize(len_globalvar,0);
    ip.globalvar.reserve(len_globalvar);
    ip.globalvar.resize(len_globalvar,0);

    auto expconst = preset_globalconst(ast_tree,
                                       &ip.globalconst[0],
                                       ip.globalconst.i64_size());
    if (!expconst)
        return forwarderr(expconst);

    auto expdtcode = datatypes_to_dtcode(ip,ip.dt,ast_tree);
    if (!expdtcode)
        return forwarderr(expdtcode);

    auto expfncode = functions_to_fncode(ip,ip.fn,ast_tree,
                                         use_many_labels,compact);
    if (!expfncode)
        return forwarderr(expfncode);

    return ipptr;
}


/**********************************************************************/
/*                                                                    */
/*                            RUNTIME DATA                            */
/*                                                                    */
/**********************************************************************/


fnruntime *fnruntime::allocate(i64 size,fnruntime *caller) noexcept
{
    using enum unwind_flags;

    std::size_t sz = static_cast<std::size_t>(size._v);
    sz += sizeof(fnruntime);
    unsigned char *ptr =
        cig::allocator<unsigned char,"fnruntime">::allocate(sz);
    cig_assert(ptr != nullptr);
    fnruntime *rt = static_cast<fnruntime*>(static_cast<void*>(ptr));

    rt->fn = 0;
    rt->size = size;
    rt->ip = 0;
    rt->caller = caller;
    rt->globalconst = caller->globalconst;
    rt->globalvar = caller->globalvar;
    rt->uwflags = 0;

    // Return a pointer after the end of the fnruntime structure
    return rt + 1;
}


void fnruntime::free() noexcept
{
    std::size_t sz = static_cast<std::size_t>(size._v);
    sz += sizeof(fnruntime);
    unsigned char *ptr = static_cast<unsigned char*>(static_cast<void*>(this));
    cig::allocator<unsigned char,"fnruntime">::deallocate(ptr,sz);
}



} // namespace interpreter

} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
