/* cig_testparser.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Test program for CIG parser.
 */

#include "cig_startup_main.h"
#include "cig_parser.h"
#include "cig_literals.h"
#include "cig_str_proxy.h"
#include "cig_fmt_formatter.h"
#include "cig_ast_node.h"
#include "cig_ast_builtin.h"
#include "cig_ast_resolver.h"
#include "cig_interpreter.h"
#include "cig_interpreter_dump.h"
#include "cig_ast_dump.h"


int cig::cig_main([[maybe_unused]] int argc,
                  [[maybe_unused]] char *argv[])
{
    str program_name = str::from_utf8(argv[0]).value();
    str source_directory;
    str ast_dumpfile;
    str interpreter_dumpfile;
    str cig_file_name;
    bool use_many_labels = false;
    bool compact = true;

    for (int iarg = 1; iarg < argc; )
    {
        str a = str::from_utf8(argv[iarg]).value();

        if (iarg + 2 <= argc &&
            (a == "-ast-dump"_str ||
             a == "--ast-dump"_str))
        {
            ast_dumpfile = str::from_utf8(argv[iarg+1]).value();
            iarg += 2;
        }
        else if (iarg + 2 <= argc &&
                 (a == "-interpreter-dump"_str ||
                  a == "--interpreter-dump"_str))
        {
            interpreter_dumpfile = str::from_utf8(argv[iarg+1]).value();
            iarg += 2;
        }
        else if (iarg + 2 <= argc &&
                 (a == "-dir"_str ||
                  a == "--dir"_str))
        {
            source_directory = str::from_utf8(argv[iarg+1]).value();
            iarg += 2;
        }
        else if (iarg + 1 <= argc &&
                 (a == "-labels"_str ||
                  a == "--use-many-labels"_str))
        {
            use_many_labels = true;
            iarg += 1;
        }
        else if (iarg + 1 <= argc &&
                 (a == "-compact"_str ||
                  a == "--compact"_str))
        {
            compact = true;
            iarg += 1;
        }
        else if (iarg + 1 <= argc &&
                 (a == "-no-compact"_str ||
                  a == "--no-compact"_str))
        {
            compact = false;
            iarg += 1;
        }
        else if (iarg + 1 <= argc)
        {
            cig_file_name = a;
            iarg += 1;
        }
        else
        {
            fmt::print(stderr,
                       "{0}: error: bad commandline argument[{1}]: {2}\n",
                       program_name,iarg,a);
            return 1;
        }
                 
    }

    if (cig_file_name.is_empty())
    {
        fmt::print(stderr,
                   "{0}: error: no source file specified on command line\n",
                   program_name);
        return 1;
    }



    ast::ast_node_ptr global =
        cig::ast::ast_node::make_ast_node(ast::ast_node_type::AST_GLOBAL);

    str sourcepath = source_directory.is_empty() ? cig_file_name :
        source_directory + "/"_str + cig_file_name;
    cig::detail::utf8proxy proxy(sourcepath);

    auto expbuiltin = cig::ast::global_add_builtins(global);
    if (!expbuiltin)
    {
        cig::err const &e = expbuiltin.error();
        fmt::print(stderr,
                   "{0}: error: global_add_builtins: {1}\n",
                   e.sourceloc,e.to_str());
        if (e.prev_sloc.file_name())
            fmt::print(stderr,
                       "{0}: note: previous location referenced\n",
                       e.prev_sloc);
        return 1;
    }

    auto parseerr = cig::parser::parse_file(global,sourcepath,proxy.c_str());
    if (parseerr.is_error())
    {
        fmt::print(stderr,
                   "{0}: error: {1}\n",
                   parseerr.sourceloc,parseerr.to_str());
        if (parseerr.prev_sloc.file_name())
            fmt::print(stderr,
                       "{0}: note: previous location referenced\n",
                       parseerr.prev_sloc);
        return 1;
    }

    auto reserr = cig::ast::resolve_ast_tree(global);
    if (!reserr)
    {
        cig::err e = reserr.error();
        fmt::print(stderr,
                   "{0}: error: {1}\n",
                   e.sourceloc,e.to_str());
        if (e.prev_sloc.file_name())
            fmt::print(stderr,
                       "{0}: note: previous location referenced\n",
                       e.prev_sloc);
        return 1;
    }

    auto reserr2 = cig::ast::resolve_ast_tree(global);
    if (!reserr2)
    {
        cig::err e = reserr.error();
        fmt::print(stderr,
                   "{0}: error: {1} (resolver rerun)\n",
                   e.sourceloc,e.to_str());
        if (e.prev_sloc.file_name())
            fmt::print(stderr,
                       "{0}: note: previous location referenced\n",
                       e.prev_sloc);
        return 1;
    }

    ast_tree_set_dump_indices(global);

    auto expip = cig::interpreter::ast_tree_to_ipcode(global,
                                                      use_many_labels,compact);
    if (!expip)
    {
        cig::err e = expip.error();
        fmt::print(stderr,
                   "{0}: error: {1}\n",
                   e.sourceloc,e.to_str());
        if (e.prev_sloc.file_name())
            fmt::print(stderr,
                       "{0}: note: previous location referenced\n",
                       e.prev_sloc);
        return 1;
    }
    auto ipcode = expip.value();

    if (!ast_dumpfile.is_empty())
        dump_ast_tree(ast_dumpfile,global,true);
    if (!interpreter_dumpfile.is_empty())
        dump_interpreter_code(interpreter_dumpfile,ipcode);

    return 0;
}



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
