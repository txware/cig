/* cig_asciicharclass.h
 *
 * Copyright 2023 Claus Fischer
 *
 * ASCII character class definitions.
 */
#ifndef CIG_ASCIICHARCLASS_H_INCLUDED
#define CIG_ASCIICHARCLASS_H_INCLUDED

#include "cig_int_inline.h"


namespace cig {

// ascii character class definition
enum class acctag : int64_t {
    // Name, value                 Hex      Description
    NUL = 0,                    // 00 '\0'
    CONTROL = 1,                // 01-1f except 09 0a 0d
    HT = 2,                     // 09  \t   Horizontal tab
    LF = 3,                     // 0a  \n   New line (line feed)
    CR = 4,                     // 0d  \r   Carriage return
    SPACE = 5,                  // 20  ' '  Space
    EXCLAM = 6,                 // 21  !    Exclamation mark
    DQUOTE = 7,                 // 22  "    Double quote
    HASH = 8,                   // 23  #    Hash char
    DOLLAR = 9,                 // 24  $    Dollar sign
    PERCNT = 10,                // 25  %    Percent sign
    AMPAND = 11,                // 26  &    Ampersand
    SQUOTE = 12,                // 27  '    Single quote
    OPAREN = 13,                // 28  (    Opening parenthesis
    CPAREN = 14,                // 29  )    Closing parenthesis
    AST = 15,                   // 2a  *    Asterisk
    PLUS = 16,                  // 2b  +    Plus sign
    COMMA = 17,                 // 2c  ,    Comma sign
    MINUS = 18,                 // 2d  -    Minus sign
    DOT = 19,                   // 2e  .    Dot
    SLASH = 20,                 // 2f  /    Slash sign
    DIGIT = 21,                 // 30-39    Digit
    COLON = 22,                 // 3a  :    Colon
    SEMICOL = 23,               // 3b  ;    Semicolon
    LESS = 24,                  // 3c  <    Less sign, left angular bracket
    EQUAL = 25,                 // 3d  =    Equal sign
    GREATER = 26,               // 3e  >    Greater sign, right angular bracket
    QUEST = 27,                 // 3f  ?    Question mark
    ATSIGN = 28,                // 40  @    At sign
    UPPER = 29,                 // 41-5a    Uppercase letters
    OBRACK = 30,                // 5b  [    Opening bracket
    BSLASH = 31,                // 5c  \    Backslash
    CBRACK = 32,                // 5d  ]    Closing bracket
    CARET = 33,                 // 5e  ^    Caret
    USCORE = 34,                // 5f  _    Underscore
    GRAVE = 35,                 // 60  `    Grave, Backtick
    LOWER = 36,                 // 61-7a    Lowercase letters
    OBRACE = 37,                // 7b  {    Opening brace
    VBAR = 38,                  // 7c  |    Vertical bar
    CBRACE = 39,                // 7d  }    Closing brace
    TILDE = 40,                 // 7e  ~    Tilde sign
    DEL = 41,                   // 7f       Delete
    NOASCII = 42                // <0,>7f   anything not ASCII
};


namespace accmask {

constexpr int64_t NUL     = (1ll << static_cast<int64_t>(acctag::NUL    ));
constexpr int64_t CONTROL = (1ll << static_cast<int64_t>(acctag::CONTROL));
constexpr int64_t HT      = (1ll << static_cast<int64_t>(acctag::HT     ));
constexpr int64_t LF      = (1ll << static_cast<int64_t>(acctag::LF     ));
constexpr int64_t CR      = (1ll << static_cast<int64_t>(acctag::CR     ));
constexpr int64_t SPACE   = (1ll << static_cast<int64_t>(acctag::SPACE  ));
constexpr int64_t EXCLAM  = (1ll << static_cast<int64_t>(acctag::EXCLAM ));
constexpr int64_t DQUOTE  = (1ll << static_cast<int64_t>(acctag::DQUOTE ));
constexpr int64_t HASH    = (1ll << static_cast<int64_t>(acctag::HASH   ));
constexpr int64_t DOLLAR  = (1ll << static_cast<int64_t>(acctag::DOLLAR ));
constexpr int64_t PERCNT  = (1ll << static_cast<int64_t>(acctag::PERCNT ));
constexpr int64_t AMPAND  = (1ll << static_cast<int64_t>(acctag::AMPAND ));
constexpr int64_t SQUOTE  = (1ll << static_cast<int64_t>(acctag::SQUOTE ));
constexpr int64_t OPAREN  = (1ll << static_cast<int64_t>(acctag::OPAREN ));
constexpr int64_t CPAREN  = (1ll << static_cast<int64_t>(acctag::CPAREN ));
constexpr int64_t AST     = (1ll << static_cast<int64_t>(acctag::AST    ));
constexpr int64_t PLUS    = (1ll << static_cast<int64_t>(acctag::PLUS   ));
constexpr int64_t COMMA   = (1ll << static_cast<int64_t>(acctag::COMMA  ));
constexpr int64_t MINUS   = (1ll << static_cast<int64_t>(acctag::MINUS  ));
constexpr int64_t DOT     = (1ll << static_cast<int64_t>(acctag::DOT    ));
constexpr int64_t SLASH   = (1ll << static_cast<int64_t>(acctag::SLASH  ));
constexpr int64_t DIGIT   = (1ll << static_cast<int64_t>(acctag::DIGIT  ));
constexpr int64_t COLON   = (1ll << static_cast<int64_t>(acctag::COLON  ));
constexpr int64_t SEMICOL = (1ll << static_cast<int64_t>(acctag::SEMICOL));
constexpr int64_t LESS    = (1ll << static_cast<int64_t>(acctag::LESS   ));
constexpr int64_t EQUAL   = (1ll << static_cast<int64_t>(acctag::EQUAL  ));
constexpr int64_t GREATER = (1ll << static_cast<int64_t>(acctag::GREATER));
constexpr int64_t QUEST   = (1ll << static_cast<int64_t>(acctag::QUEST  ));
constexpr int64_t ATSIGN  = (1ll << static_cast<int64_t>(acctag::ATSIGN ));
constexpr int64_t UPPER   = (1ll << static_cast<int64_t>(acctag::UPPER  ));
constexpr int64_t OBRACK  = (1ll << static_cast<int64_t>(acctag::OBRACK ));
constexpr int64_t BSLASH  = (1ll << static_cast<int64_t>(acctag::BSLASH ));
constexpr int64_t CBRACK  = (1ll << static_cast<int64_t>(acctag::CBRACK ));
constexpr int64_t CARET   = (1ll << static_cast<int64_t>(acctag::CARET  ));
constexpr int64_t USCORE  = (1ll << static_cast<int64_t>(acctag::USCORE ));
constexpr int64_t GRAVE   = (1ll << static_cast<int64_t>(acctag::GRAVE  ));
constexpr int64_t LOWER   = (1ll << static_cast<int64_t>(acctag::LOWER  ));
constexpr int64_t OBRACE  = (1ll << static_cast<int64_t>(acctag::OBRACE ));
constexpr int64_t VBAR    = (1ll << static_cast<int64_t>(acctag::VBAR   ));
constexpr int64_t CBRACE  = (1ll << static_cast<int64_t>(acctag::CBRACE ));
constexpr int64_t TILDE   = (1ll << static_cast<int64_t>(acctag::TILDE  ));
constexpr int64_t DEL     = (1ll << static_cast<int64_t>(acctag::DEL    ));
constexpr int64_t NOASCII = (1ll << static_cast<int64_t>(acctag::NOASCII));

}


template<typename Char>
static inline acctag codepoint_to_acctag_aux(Char c)
{
    if (c < 0)
        return acctag::NOASCII;
    else if (c >= 0x80)
        return acctag::NOASCII;
    else if (c >= 0x40)
    {
        if (c >= 0x60)
        {
            if (c >= 0x7b)
                return static_cast<acctag>(c - 0x7b + 37);
            else
                return acctag::LOWER;
        }
        else
        {
            if (c >= 0x5b)
                return static_cast<acctag>(c - 0x5b + 30);
            else
                return acctag::UPPER;
        }
    }
    else
    {
        if (c >= 0x20)
        {
            if (c >= 0x30)
            {
                if (c >= 0x3a)
                    return static_cast<acctag>(c - 0x3a + 22);
                else
                    return acctag::DIGIT;
            }
            else
                return static_cast<acctag>(c - 0x20 + 5);
        }
        else
        {
            if (c == 0x00)
                return acctag::NUL;
            else if (c == 0x09)
                return acctag::HT;
            else if (c == 0x0a)
                return acctag::LF;
            else if (c == 0x0d)
                return acctag::CR;
            else return acctag::CONTROL;
        }
    }
}


static inline acctag codepoint_to_acctag(i64 c)
{
    return codepoint_to_acctag_aux(c._v);
}


template<typename Char>
static inline i64 codepoint_to_accmask_aux(Char c)
{
    return i64(1) << static_cast<int64_t>(codepoint_to_acctag(c));
}


static inline i64 codepoint_to_accmask(i64 c)
{
    return codepoint_to_accmask_aux(c._v);
}



} // namespace cig



/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
#endif /* !defined(CIG_ASCIICHARCLASS_H_INCLUDED) */
