/* cig_allocation.cpp
 *
 * Copyright 2023 Claus Fischer
 *
 * Allocation functions.
 */
#include "cig_allocation.h"
#include "cig_assert.h"
#include "cig_source_location.h" // cig::source_location
#include "cig_startup_filepointer.h"

#include <cstddef>              // std::size_t
#include <utility>              // std::move
#include <cstdlib>              // std::getenv,std::malloc,std::free
#include <cstdio>               // FILE,std::fopen,std::flush,std::fclose
#include <cstring>              // std::strlen,std::strcpy
#include <new>                  // operator new, operator delete



// Use -DCIG_MALLOC_LOG_ENV='""' to prohibit allocation log altogether.

// Otherwise set the defined environment variable to a filename
// to enable logging.
// The resulting file can be sorted lexicographically by line to find
// non-paired allocations/frees.

#ifndef CIG_MALLOC_LOG_ENV
#define CIG_MALLOC_LOG_ENV "CIG_MALLOC_LOG"
#endif


// Note: the allocation logfile can be sorted by the following command:
#if 0

   cat FILENAME | \
   sort -k 2,2 -k 3,3 -k 1,1rn | \
   awk '{ if ($4 == "free") { ptr=$2; sz=$3; print $0 } else if ($4 == "allocate") { if (ptr != $2) print $0, "LEAKED"; else if (sz != $3) print $0, "BADSIZE"; else print $0; ptr="" } else print $0 }' | \
   sort -k 1,1n | \
   grep 'LEAKED$'

// The script file 'leakcheck' contains a pipeable version of this.

#endif


namespace cig {

namespace allocation {


//
// Allocation tracker (counter for groups of allocations): A singleton
//


// A singleton
struct allocation_tracker
{
    
    detail::allocator_data dll;
    static allocation_tracker &get_singleton() noexcept;

    allocation_tracker() noexcept : dll(0) // 0: no recursive self-registration
    {}
};



// A class to keep track of allocations and frees
allocation_tracker &allocation_tracker::get_singleton() noexcept
{
    // Object is constructed on first call to get_singleton
    // Object is destructed sometime after main() ends.
    // For the dll, the sequence of destruction is not important,
    // however construction must be done before the allocators.
    static allocation_tracker tracker;
    return tracker;
}


namespace detail {

void
register_allocator(allocator_data &alldata) noexcept
{
    allocation_tracker &tracker = allocation_tracker::get_singleton();

    // Do not register the tracker's builtin allocator
    // that just serves as a list head and tail
    if (&alldata == &tracker.dll)
        return;

    cig_assert(alldata.dll_next == &alldata);
    if (!tracker.dll.is_in_dll())
    {
        alldata.dll_next = &tracker.dll;
        alldata.dll_prev = &tracker.dll;
        tracker.dll.dll_next = &alldata;
        tracker.dll.dll_prev = &alldata;
    }
    else
    {
        // Sort alphabetically
        
        detail::allocator_data *p = &tracker.dll;
        detail::allocator_data *n = tracker.dll.dll_next;
        while (n != &tracker.dll && std::strcmp(alldata.name,n->name) > 0)
        {
            p = n;
            n = n->dll_next;
        }
        // Insert between p and n
        p->dll_next = &alldata;
        n->dll_prev = &alldata;
        alldata.dll_next = n;
        alldata.dll_prev = p;
    }
}

}


//
// Allocation logger: A global object
//


struct allocation_logger {
public:
    char *logger_file;        // File name for logger file
    cig::startup::detail::filepointer_wrapper loggerfp {
        "allocation_logger"}; // Logger file pointer
    bool opened_once;         // Whether the logger file has been opened once
    bool do_log;              // Whether logging is activated
    unsigned long long op {}; // Running operation count;

    void close() noexcept
    {
        if (!loggerfp)
            return;
        loggerfp.flush();
        loggerfp.close();
    }

    // At the end of the program the tracker file is closed
    ~allocation_logger() noexcept
    {
        // Close the tracker file
        close();

        // Free the tracker file name to accommodate an external leakage checker
        if (logger_file)
            std::free(static_cast<void *>(logger_file));
        logger_file = NULL;
    }

    allocation_logger() noexcept :
        logger_file(NULL), opened_once(false), do_log(false)
    {
#if defined CIG_MALLOC_LOG_ENV
        if (std::strlen(CIG_MALLOC_LOG_ENV) > 0)
        {
            char *s = std::getenv(CIG_MALLOC_LOG_ENV);
            if (s)
            {
                std::size_t len = std::strlen(s);
                logger_file = static_cast<char *>(std::malloc(len+1));
                if (logger_file)
                {
                    std::strcpy(logger_file,s);
                }

                // Switch on logging
                do_log = true;
            }
        }
#endif
    }

    // Call the flush() function any time to flush the file and close it.
    // It will automatically be reopened on the next call to alloc/free.
    void flush() noexcept
    {
        close();
    }

    void open() noexcept
    {
        if (loggerfp)
            loggerfp.close();
        loggerfp = std::fopen(logger_file,opened_once ? "a" : "w");
        // Ignore any failures here, there is no place to report
        if (loggerfp && !opened_once)
        {
            std::fprintf(loggerfp.file_pointer,
                         "#  Op No %16s %9s   Operation Allocator File+Line\n",
                         "Address","Bytes");
            opened_once = true;
        }
    }

    void ensure_open() noexcept
    {
        if (!loggerfp)
            open();
    }

    void log(bool is_alloc,
             void *ptr,std::size_t size,
             char const *allocatorname,
             char const *file,unsigned long line) noexcept
    {
        if (!do_log)
            return;
        op++;                // Count before checking loggerfp to detect holes
        if (!loggerfp)
        {
            ensure_open();
            if (!loggerfp)
                return;
        }
        std::fprintf(loggerfp.file_pointer,
                     "%8llu %16p %9zu   %-8s  %-9s %s %lu\n",
                     op,
                     ptr,size,
                     is_alloc ? "allocate" : "free",
                     allocatorname,
                     file,line);
    }
};


allocation_logger logger;


// Failing to allocate will return zero
void *
allocate_or_return_zero(
    std::size_t size,
    char const *allocatorname,
    cig::source_location const &loc) noexcept
{
    void *ptr = std::malloc(size);
    logger.log(true,ptr,size,allocatorname,loc.file_name(),loc.line());
    return ptr;
}

// Failing to allocate will abort from cig_assert
void *
allocate(std::size_t size,
         char const *allocatorname,
         cig::source_location const &loc) noexcept
{
    void *ptr = std::malloc(size);
    logger.log(true,ptr,size,allocatorname,loc.file_name(),loc.line());
    cig_assert(ptr != 0);
    return ptr;
}

void
free(void *ptr,
     std::size_t size,
     char const *allocatorname,
     cig::source_location const &loc) noexcept
{
    cig_assert(ptr != 0);
    logger.log(false,ptr,size,allocatorname,loc.file_name(),loc.line());
    std::free(ptr);
}


void flush() noexcept
{
    logger.flush();
}


bool check_still_allocated() noexcept
{
    allocation_tracker &tracker = allocation_tracker::get_singleton();
    detail::allocator_data *alldata;

    bool still_allocated = false;
    for (alldata = tracker.dll.dll_next;
         alldata != &tracker.dll;
         alldata = alldata->dll_next)
    {
        if (alldata->object_alloc != alldata->object_free ||
            alldata->byte_alloc != alldata->byte_free)
            still_allocated = true;
    }
    return still_allocated;
}


void endreport(void *file_pointer,bool force_print) noexcept
{
    allocation_tracker &tracker = allocation_tracker::get_singleton();
    detail::allocator_data *alldata;
    FILE *fp = reinterpret_cast<FILE *>(file_pointer);

    bool do_print = force_print;
    for (alldata = tracker.dll.dll_next;
         alldata != &tracker.dll;
         alldata = alldata->dll_next)
    {
        if (force_print ||
            alldata->object_alloc != alldata->object_free ||
            alldata->byte_alloc != alldata->byte_free)
        {
            do_print = true;
        }
    }

    if (do_print)
    {
        fprintf(fp,"MEMORY LEAKS:%s\n",
                check_still_allocated() ? "" : " NONE");
        fprintf(fp,"%20s %6s %6s %6s %8s %8s %8s\n",
                "object type",
                "object","freed","max",
                "byte","freed","max");
        for (alldata = tracker.dll.dll_next;
             alldata != &tracker.dll;
             alldata = alldata->dll_next)
        {
            if (force_print ||
                alldata->object_alloc != alldata->object_free ||
                alldata->byte_alloc != alldata->byte_free)
            {
                fprintf(fp,"%20s %6lld %6lld %6lld %8lld %8lld %8lld\n",
                        alldata->name,
                        static_cast<long long>(alldata->object_alloc),
                        static_cast<long long>(alldata->object_free),
                        static_cast<long long>(alldata->object_max),
                        static_cast<long long>(alldata->byte_alloc),
                        static_cast<long long>(alldata->byte_free),
                        static_cast<long long>(alldata->byte_max));
            }
        }
    }
}


} // namespace allocation


} // namespace cig






//
// Replacement operator new and delete
//


// These replacement operators are provided to keep track of memory
// allocated with new and delete.
// Unfortunately the C++ standard functions do not keep track of the
// size; there are delete functions defined with size but they are
// apparently not used.
// Therefore we store the size before the allocated space, which
// probably duplicates the behaviour that std::malloc does anyway.
// Also, these operators do not take advantage of reduced alignment
// requirements for small entities.
// However, proper accounting for all objects seems worth it.

// If you do not want this behaviour, simply define this to 0
#define USE_CIG_NEW_OPERATORS 1

#if USE_CIG_NEW_OPERATORS

// Use some overhead to store the originally allocated size
// to allow at least accounting for the number of objects
constexpr std::size_t prepend_size =
    alignof(std::max_align_t) > sizeof(std::size_t) ?
    alignof(std::max_align_t) : sizeof(std::size_t);

// Note that cig::baseallocator<name>::alloc will never return zero;
// on failure it will call cig_abort directly.

[[ nodiscard ]] void* operator new  (std::size_t count)
{
    char *mem = cig::baseallocator<"new">::alloc<char>(prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new[](std::size_t count)
{
    char *mem = cig::baseallocator<"new[]">::alloc<char>(prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new  (std::size_t count,
                                     std::align_val_t)
{
    char *mem = cig::baseallocator<"new">::alloc<char>(prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new[](std::size_t count,
                                     std::align_val_t)
{
    char *mem = cig::baseallocator<"new[]">::alloc<char>(prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new  (std::size_t count,
                                     const std::nothrow_t&) noexcept
{
    char *mem =
        cig::baseallocator<"new_noexcept">::alloc_or_return_zero<char>(
            prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new[](std::size_t count,
                                     const std::nothrow_t&) noexcept
{
    char *mem =
        cig::baseallocator<"new_noexcept[]">::alloc_or_return_zero<char>(
            prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new  (std::size_t count,std::align_val_t,
                                     const std::nothrow_t&) noexcept
{
    char *mem =
        cig::baseallocator<"new_noexcept">::alloc_or_return_zero<char>(
            prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}

[[ nodiscard ]] void* operator new[](std::size_t count,std::align_val_t,
                                     const std::nothrow_t&) noexcept
{
    char *mem =
        cig::baseallocator<"new_noexcept[]">::alloc_or_return_zero<char>(
            prepend_size+count);
    *reinterpret_cast<std::size_t*>(mem) = count;
    return static_cast<void *>(mem+prepend_size);
}



void operator delete  (void* ptr) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new">::free<char>(mem,count+prepend_size);
}

void operator delete[](void* ptr) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new[]">::free<char>(mem,count+prepend_size);
}

void operator delete  (void* ptr,std::align_val_t) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new">::free<char>(mem,count+prepend_size);
}

void operator delete[](void* ptr,std::align_val_t) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new[]">::free<char>(mem,count+prepend_size);
}

void operator delete  (void* ptr,std::size_t sz) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    // auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new">::free<char>(mem,sz+prepend_size);
}

void operator delete[](void* ptr,std::size_t sz) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    // auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new[]">::free<char>(mem,sz+prepend_size);
}

void operator delete  (void* ptr, std::size_t sz,
                       std::align_val_t) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    // auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new">::free<char>(mem,sz+prepend_size);
}

void operator delete[](void* ptr, std::size_t sz,
                       std::align_val_t) noexcept
{
    char *mem = static_cast<char*>(ptr)-prepend_size;
    // auto count = *reinterpret_cast<std::size_t*>(mem);
    cig::baseallocator<"new[]">::free<char>(mem,sz+prepend_size);
}
#endif  // USE_CIG_NEW_OPERATORS

/* ** EMACS **
 * Local Variables:
 * mode: C++
 * c-file-style: "Stroustrup"
 * c-indent-comments-syntactically-p: t
 * c-file-offsets: ((inline-open . 0) (innamespace . 0))
 * indent-tabs-mode: nil
 * End:
 */
