
option(WARNINGS_AS_ERRORS "Treat compiler warnings as errors" FALSE)

macro(_set_flags)
    set(MSVC_WARNINGS
      /W4 # Baseline reasonable warnings
      /w14062 # enumerator 'identifier' in a switch of enum 'enumeration' is
              # not handled
      /w14242 # 'identfier': conversion from 'type1' to 'type1', possible loss
              # of data
      /w14254 # 'operator': conversion from 'type1:field_bits' to
              # 'type2:field_bits', possible loss of data
      /w14263 # 'function': member function does not override any base class
              # virtual member function
      /w14265 # 'classname': class has virtual functions, but destructor is not
              # virtual instances of this class may not be destructed correctly
      /w14266 # 'function': no override available for virtual member function
              # from base 'type'; function is hidden
      /w14287 # 'operator': unsigned/negative constant mismatch
      /we4289 # nonstandard extension used: 'variable': loop control variable
              # declared in the for-loop is used outside the for-loop scope
      /w14296 # 'operator': expression is always 'boolean_value'
      /w14311 # 'variable': pointer truncation from 'type1' to 'type2'
      /w14545 # expression before comma evaluates to a function which is missing
              # an argument list
      /w14546 # function call before comma missing argument list
      /w14547 # 'operator': operator before comma has no effect; expected
              # operator with side-effect
      /w14549 # 'operator': operator before comma has no effect; did you intend
              # 'operator'?
      /w14555 # expression has no effect; expected expression with side- effect
      /w14619 # pragma warning: there is no warning number 'number'
      /w14640 # Enable warning on thread un-safe static member initialization
      /w14826 # Conversion from 'type1' to 'type_2' is sign-extended. This may
              # cause unexpected runtime behavior.
      /w14905 # wide string literal cast to 'LPSTR'
      /w14906 # string literal cast to 'LPWSTR'
      /w14928 # illegal copy-initialization; more than one user-defined
              # conversion has been implicitly applied
      /wd4459 # disable hide globals
      /wd4996 # disable usave functions
      /wd4127 # disable constant expression, use if constexpr
      /permissive- # standards conformance mode for MSVC compiler.
    )

    set(CLANG_C_WARNINGS
      -Wall
      -Wextra # reasonable and standard
      -Wpedantic # warn if non-standard C++ is used
      -Wshadow # warn the user if a variable declaration shadows one from a
               # parent context
      -Wcast-align # warn for potential performance problem casts
      -Wunused # warn on anything being unused
      -Wcast-qual # warn on dropping const or volatile qualifiers
      -Wconversion # warn on type conversions that may lose data
      -Wsign-conversion # warn on sign conversions
      -Wnull-dereference # warn if a null dereference is detected
      -Wdouble-promotion # warn if float is implicit promoted to double
      -Wformat=2 # warn on security issues around functions that format output
                 # (ie printf)
    )

    set(CLANG_CXX_WARNINGS
        ${CLANG_C_WARNINGS}
      -Wnon-virtual-dtor # warn the user if a class with virtual functions has
            # a non-virtual destructor. This helps catch hard to track down
            # memory errors
      -Wold-style-cast # warn for c-style casts
      -Woverloaded-virtual # warn if you overload (not override) a virtual
                           # function
    )

    if (WARNINGS_AS_ERRORS)
        set(CLANG_WARNINGS ${CLANG_WARNINGS} -Werror)
        set(MSVC_WARNINGS ${MSVC_WARNINGS} /WX)
    endif()

    set(GCC_C_WARNINGS
        ${CLANG_C_WARNINGS}
        -Wmisleading-indentation # warn if indentation implies blocks where
            # blocks do not exist
        -Wduplicated-cond # warn if if / else chain has duplicated conditions
        -Wduplicated-branches # warn if if / else branches have duplicated code
        -Wlogical-op # warn about logical operations being used where bitwise
            # were probably wanted
        #-Wuseless-cast # warn if you perform a cast to the same type
    )

    set(GCC_CXX_WARNINGS
      ${CLANG_CXX_WARNINGS}
        -Wmisleading-indentation # warn if indentation implies blocks where
            # blocks do not exist
        -Wduplicated-cond # warn if if / else chain has duplicated conditions
        -Wduplicated-branches # warn if if / else branches have duplicated code
        -Wlogical-op # warn about logical operations being used where bitwise
        #-Wuseless-cast # warn if you perform a cast to the same type
    )


    if(MSVC)
        set(PROJECT_C_WARNINGS ${MSVC_WARNINGS})
        set(PROJECT_CXX_WARNINGS ${MSVC_WARNINGS})
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        set(PROJECT_C_WARNINGS ${CLANG_C_WARNINGS})
        set(PROJECT_CXX_WARNINGS ${CLANG_CXX_WARNINGS})
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
        set(PROJECT_C_WARNINGS ${CLANG_C_WARNINGS})
        set(PROJECT_CXX_WARNINGS ${CLANG_CXX_WARNINGS})
    else()
        set(PROJECT_C_WARNINGS ${GCC_C_WARNINGS})
        set(PROJECT_CXX_WARNINGS ${GCC_CXX_WARNINGS})
    endif()

endmacro()




# Set global compiler warning level
function(set_warnings)
    _set_flags()

    message(STATUS "Setting restrictive compilation warnings")
    message(STATUS "  Treat warnings as errors: ${WARNINGS_AS_ERRORS}")
    message(STATUS "  C Warnign flags: ${PROJECT_C_WARNINGS}")
    message(STATUS "  CXX Warnign flags: ${PROJECT_CXX_WARNINGS}")
    message(STATUS "Setting restrictive compilation warnings - done")

    add_compile_options(
        "$<$<COMPILE_LANGUAGE:C>:${PROJECT_C_WARNINGS}>"
        "$<$<COMPILE_LANGUAGE:CXX>:${PROJECT_CXX_WARNINGS}>")

endfunction()

# Set compiler warning level for a provided CMake target
function(target_set_warnings target scope)
    set(scopes PUBLIC INTERFACE PRIVATE)
    if(NOT scope IN_LIST scopes)
        message(FATAL_ERROR "'scope' argument should be one of ${scopes} ('${scope}' received)")
    endif()

    _set_flags()

    message(STATUS "Setting ${scope} restrictive compilation warnings for '${target}'")
    message(STATUS "  Treat warnings as errors: ${WARNINGS_AS_ERRORS}")
    message(STATUS "  Flags: ${flags}")
    message(STATUS "Setting ${scope} restrictive compilation warnings for '${target}' - done")

    target_compile_options(${target} ${scope}
"$<$<COMPILE_LANGUAGE:C>:${PROJECT_C_WARNINGS}>"
        "$<$<COMPILE_LANGUAGE:CXX>:${PROJECT_CXX_WARNINGS}>")
endfunction()
