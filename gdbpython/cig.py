# To activate this file, put the following GDB commands into ~/.gdbinit
# and adapt the path of python_dir according to your needs.
#
# -------------------------------- .gdbinit --------------------------------
# python
# import glob
# import os
# 
# python_dir = os.path.expanduser("~") + "/my/path/to/cig/gdbpython"
# 
# # Search the python dir for all .py files, and source each
# py_files = glob.glob("%s/*.py" % python_dir)
# for py_file in py_files:
#     gdb.execute('source %s' % py_file)
#     print("loaded ", py_file)
# 
# end
# 
# printf "*** CIG debug-printing initialized ***\n"
# ------------------------------ end .gdbinit ------------------------------


import gdb.printing
import json

class strvalPrinter:
   """Print a CIG strval"""

   def __init__(self, val):
      self.val = val
      self.name = "cig::detail::strval"

   def to_string(self):
      # Endianness
      tp = self.val["_l"].type
      offset = -1
      for f in tp.fields():
         if f.name == "_cap":
            offset = f.bitpos
      if offset == -1:
         return "Endianness cannot be determined"
      elif offset == 0:
         endianness = "little"
      else:
         endianness = "big"
      ptrsize = int(tp.sizeof / 3)

      chartype = ""
      pointered = ""
      data = int(self.val["_l"]["_data"])
      size = int(self.val["_l"]["_size"])
      cap = int(self.val["_l"]["_cap"])
      is_pointered = ((cap & 0x1) == 0x1)
      ct = (cap & 0x6)
      charsz = 0
      if ct == 0:
         chartype = "ascii"
         capacity = 3 * ptrsize - 2
         charsz = 1
      elif ct == 2:
         chartype = "ucs1"
         capacity = 3 * ptrsize - 2
         charsz = 1
      elif ct == 4:
         chartype = "ucs2"
         capacity = int(3 * ptrsize / 2 - 2)
         charsz = 2
      elif ct == 6:
         chartype = "ucs4"
         capacity = int(3 * ptrsize / 4 - 2)
         charsz = 4
      else:
         return "Bad chartype bits"

      if is_pointered:
         pointered = " pointered"
         len = size
         capacity = (cap - (cap & 0xf))

         b = b''
         try:
            inferior = gdb.selected_inferior()
            b = bytes(inferior.read_memory(data,size*charsz))
         except RuntimeError:
            pass
         
      else:
         pointered = " intern"
         len = (cap & 0xff) >> 3
         datab = data.to_bytes(ptrsize,endianness)
         sizeb = size.to_bytes(ptrsize,endianness)
         capb = cap.to_bytes(ptrsize,endianness)
         if endianness == 'little':
            b = capb + sizeb + datab
            b = b[charsz:charsz + len * charsz]
         else:
            b = datab + sizeb + capb
            b = b[0:len * charsz]

      # Decode the string
      try:
          if chartype == 'ascii':
             s = b.decode('ascii')
          elif chartype == 'ucs1':
             s = b.decode('iso-8859-1')
          elif chartype == 'ucs2':
             if endianness == 'little':
                s = b.decode('utf-16le')
             else:
                s = b.decode('utf-16be')
          else:
             if endianness == 'little':
                s = b.decode('utf-32le')
             else:
                s = b.decode('utf-32be')
      except ValueError as e:
         s = "<Cannot decode string, value error>"
         
      return (self.name + " cap " + str(capacity) + " len " + str(len) + " " +
              chartype + pointered + ' ' + json.dumps(s,ensure_ascii=False))

class strPrinter(strvalPrinter):
   '''Print a CIG str'''

   def __init__(self,val):
      super().__init__(val)
      self.name = "cig::str"

class bytesvalPrinter:
   """Print a CIG bytesval"""

   def __init__(self, val):
      self.val = val
      self.name = "cig::detail::bytesval"

   def to_string(self):
      # Endianness
      tp = self.val["_l"].type
      offset = -1
      for f in tp.fields():
         if f.name == "_cap":
            offset = f.bitpos
      if offset == -1:
         return "Endianness cannot be determined"
      elif offset == 0:
         endianness = "little"
      else:
         endianness = "big"
      ptrsize = int(tp.sizeof / 3)

      chartype = ""
      pointered = ""
      data = int(self.val["_l"]["_data"])
      size = int(self.val["_l"]["_size"])
      cap = int(self.val["_l"]["_cap"])
      is_pointered = ((cap & 0x1) == 0x1)
      if is_pointered:
         pointered = " pointered"
         len = size
         capacity = (cap - (cap & 0xf))

         b = b''
         try:
            inferior = gdb.selected_inferior()
            b = bytes(inferior.read_memory(data,size))
         except RuntimeError:
            pass
         
      else:
         pointered = " intern"
         len = (cap & 0xff) >> 3
         capacity = 3 * ptrsize - 2
         datab = data.to_bytes(ptrsize,endianness)
         sizeb = size.to_bytes(ptrsize,endianness)
         capb = cap.to_bytes(ptrsize,endianness)
         if endianness == 'little':
            b = capb + sizeb + datab
            b = b[1:len+1]
         else:
            b = datab + sizeb + capb
            b = b[0:len]

      # Decode the bytes
      try:
         s = b.decode('utf8')
         s = json.dumps(s,ensure_ascii=False)
         s = ' ' + s
      except ValueError as e:
         s = ''
      s = b.hex(' ') + ' ' + s
         
      return (self.name + " cap " + str(capacity) + " len " + str(len) + " " +
              chartype + pointered + ' ' + s)

class bytesPrinter(bytesvalPrinter):
   '''Print a CIG bytes'''

   def __init__(self,val):
      super().__init__(val)
      self.name = "cig::bytes"

class intPrinter:
   '''Print a CIG i8,i16,i32,i64,u8,u16,u32,u64'''

   def __init__(self,val):
      self.val = val

   def to_string(self):
      v = int(self.val["_v"])
      tp = self.val.type
      name = tp.name

      return name + " " + str(v)

class boolPrinter:
   '''Print a CIG bool'''

   def __init__(self,val):
      self.val = val

   def to_string(self):
      v = bool(self.val["_v"])
      tp = self.val.type
      name = tp.name

      return name + " " + str(v)

def build_pretty_printer():
   pp = gdb.printing.RegexpCollectionPrettyPrinter(
   "CIG library")
   pp.add_printer('CIG strval', '^cig::detail::strval$', strvalPrinter)
   pp.add_printer('CIG str', '^cig::str$', strPrinter)
   pp.add_printer('CIG bytesval', '^cig::detail::bytesval$', bytesvalPrinter)
   pp.add_printer('CIG bytes', '^cig::bytes$', bytesPrinter)
   pp.add_printer('CIG int', '^cig::i8$', intPrinter)
   pp.add_printer('CIG int', '^cig::i16$', intPrinter)
   pp.add_printer('CIG int', '^cig::i32$', intPrinter)
   pp.add_printer('CIG int', '^cig::i64$', intPrinter)
   pp.add_printer('CIG int', '^cig::u8$', intPrinter)
   pp.add_printer('CIG int', '^cig::u16$', intPrinter)
   pp.add_printer('CIG int', '^cig::u32$', intPrinter)
   pp.add_printer('CIG int', '^cig::u64$', intPrinter)
   pp.add_printer('CIG bool', '^cig::cig_bool$', boolPrinter)
   return pp

gdb.printing.register_pretty_printer(
    gdb.current_objfile(),
    build_pretty_printer())

print("got it")
