#! /bin/sh


set -xe

if test "z-f" = "z$1"
then set +e
fi

rm -rf build_gcc_debug
mkdir build_gcc_debug
cd build_gcc_debug
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build . -v
ctest
cd ..

rm -rf build_gcc_release
mkdir build_gcc_release
cd build_gcc_release
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build . -v
ctest
cd ..

rm -rf build_clang_debug
mkdir build_clang_debug
cd build_clang_debug
CC=clang CXX=clang++ CFLAGS=-stdlib=libc++ cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build . -v
ctest
cd ..

rm -rf build_clang_release
mkdir build_clang_release
cd build_clang_release
CC=clang CXX=clang++ CFLAGS=-stdlib=libc++ cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build . -v
ctest
cd ..

