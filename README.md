# CIG - Compiler, Interpreter, Generator

CIG is a simple language between Python and C that can be interpreted
or compiled.

## Description

CIG wants to be a simple programming language that can be compiled and
interpreted and that allows to mix both modes eventually.

The compiler and interpreter will be integrated so that the interpreter
can be used for code generation during compilation.

## Plan

* Start with a small procedural language like C
* Strong and static typing
* Syntax ideas from Python
* Native types str and bytes
* Tuples and comma-separated lists
* Follow the path from C to C++
* Avoid complexity, ambiguity, keep syntax simple
* Allow new keywords while staying compatible
* No memory allocation
* Allow binding to native C++

## Authors

* Claus Fischer
* Rainer Sabelka

## License

This project is licensed under the MIT license.

