#include "../src/cig_constexpr_utf8.h"
#include <fmt/format.h>

consteval ptrdiff_t test_count1()
{
    std::array<char, 10> a = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 0 };
    return cig::cig_constexpr_decode_utf8_count(a);
}

consteval ptrdiff_t test_count2()
{
    std::array<unsigned char, 10> a = { 'a', 0xF0, 0x9F, 0x98, 0x81, 'b', 'c', 'd',
        'e', 0 };
    return cig::cig_constexpr_decode_utf8_count(a);
}

consteval ptrdiff_t test_maxcp1()
{
    std::array<char, 10> a = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 0 };
    return cig::cig_constexpr_decode_utf8_maxcp(a);
}

consteval ptrdiff_t test_maxcp2()
{
    std::array<unsigned char, 10> a = { 'a', 0xF0, 0x9F, 0x98, 0x81, 'b',
        'c', 'd', 'e', 0 };
    return cig::cig_constexpr_decode_utf8_maxcp(a);
}

consteval auto test_decode1()
{
    std::array<char, 10> a = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 0 };
    return cig::cig_constexpr_decode_utf8<char16_t, 10>(a);
}

consteval auto test_decode2()
{
    std::array<unsigned char, 10> a = { 'a', 0xF0, 0x9F, 0x98, 0x81, 'b',
        'c', 'd', 'e', 0 };
    return cig::cig_constexpr_decode_utf8<char32_t, 7, unsigned char, 10>(a);
}

static_assert(test_decode1() == std::array<char16_t, 10> {  'a', 'b', 'c',
        'd', 'e', 'f', 'g', 'h', 'i', 0 });
static_assert(test_decode2() == std::array<char32_t, 7> {  'a', 0x1F601,
        'b', 'c', 'd', 'e', 0 });

int main()
{
    auto n1 = test_count1();
    fmt::print("n1 = {}\n", n1);

    auto n2 = test_count2();
    fmt::print("n2 = {}\n", n2);

    auto n3 = test_maxcp1();
    fmt::print("n3 = {}\n", n3);

    auto n4 = test_maxcp2();
    fmt::print("n4 = {}\n", n4);
    return 0;
}
