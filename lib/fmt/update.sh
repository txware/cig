#! /bin/sh

# Use this script to update the fmtlib souce from a local git repo

src=${HOME}/src/fmt

files="CMakeLists.txt
    include/fmt/args.h
    include/fmt/chrono.h
    include/fmt/color.h
    include/fmt/compile.h
    include/fmt/core.h
    include/fmt/format.h
    include/fmt/format-inl.h
    include/fmt/locale.h
    include/fmt/os.h
    include/fmt/ostream.h
    include/fmt/printf.h
    include/fmt/ranges.h
    include/fmt/xchar.h
    README.rst
    ChangeLog.rst
    src/format.cc
    src/fmt.cc
    src/os.cc
    support/cmake/cxx14.cmake
    support/cmake/JoinPaths.cmake
"

for f in $files ; do
    if [ ! -f $f ] || ! cmp ${src}/$f $f >/dev/null ; then
        echo "Updating $f"
        cp ${src}/$f $f || exit 1
    fi
done

echo "Update successful"
